# Slurm Demo Cluster deployment

This is a set of Ansible directives used to create a demo-tier Slurm cluster. Expects the entire cluster to be RedHat, with the sole exception being the DB node, which may be Debian/Ubuntu. The particularly-weird setup done by this codebase is to run MUNGE as the 'slurm' user. Uses Jeff Geerling's (Blessed be His Name) MySQL setup role.
