---
- name: Add nodes in the Slurm cluster to /etc/hosts - Kludge version(TM)
  ansible.builtin.blockinfile:
    dest: /etc/hosts
    block: |
      192.168.56.101 rhel86-master
      192.168.56.102 rhel86-slave1
      192.168.56.103 rhel86-slave2
      192.168.56.104 rhel86-slave3
      192.168.56.105 slurmdb

# This is the more-correct version of the above, but it's worth hard-coding so we don't ALWAYS have to run with full inventories
# - name: Add Slurm cluster nodes to /etc/hosts
#   ansible.builtin.blockinfile:
#     dest: /etc/hosts
#     block: |
#       {% for node in groups['slurm_cluster'] %}
#       {% hostvars[node]['ansible_default_ipv4']['address'] }} {{ node }}
#       {% endfor %}

- name: "Create group for slurm user '{{ slurm_user }}'"
  ansible.builtin.group:
    name: "{{ slurm_user }}"
    state: present
    gid: "{{ slurm_gid }}"

- name: "Create the slurm user '{{ slurm_user }}'"
  ansible.builtin.user:
    name: "{{ slurm_user }}"
    group: "{{ slurm_user }}"
    uid: "{{ slurm_uid }}"
    shell: /sbin/nologin
    state: present

- name: Install Munge
  ansible.builtin.include_tasks: munge_install.yml

- name: Check to see if Slurm is installed on the system
  ansible.builtin.command: "{{ slurm_install_dir }}/sbin/slurmd --version"
  register: slurm_install_check
  changed_when: false
  ignore_errors: true

- name: Include tasks to install Slurm if it's not currently installed
  ansible.builtin.include_tasks: slurm_install.yml
  when: slurm_version not in slurm_install_check.stdout or (SLURM_REINSTALL is defined and SLURM_REINSTALL)

- name: Copy the slurm.conf over to the system
  ansible.builtin.template:
    src: slurm.conf.j2
    dest: "{{ slurm_install_dir }}/etc/slurm.conf"
    owner: "{{ slurm_user }}"
    group: "{{ slurm_user }}"
    mode: 0444
  register: slurm_conf

- name: Copy the cgroup.conf over to the system
  ansible.builtin.copy:
    src: cgroup.conf
    dest: "{{ slurm_install_dir }}/etc/cgroup.conf"
    owner: "{{ slurm_user }}"
    group: "{{ slurm_user }}"
    mode: 0400

- name: Include tasks to start Slurm Accounting service (slurmdbd)
  ansible.builtin.include_tasks: slurmdbd_start.yml
  when: "'slurmdbd' in group_names"

- name: Include tasks to start Slurm Controllers (slurmctld)
  ansible.builtin.include_tasks: slurmctld_start.yml
  when: "'slurm_controllers' in group_names"

- name: Include tasks to start Slurm Workers (slurmd)
  ansible.builtin.include_tasks: slurmd_start.yml
  when: "'slurm_controllers' in group_names or 'slurm_workers' in group_names"
