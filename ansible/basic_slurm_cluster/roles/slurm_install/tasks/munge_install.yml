---
# Built from https://github.com/dun/munge/wiki/Installation-Guide
# and https://wiki.fysik.dtu.dk/Niflheim_system/Slurm_installation/#munge-authentication-service
- name: Install Ubuntu-specific Munge packages
  ansible.builtin.apt:
    pkg:
      - munge
      - libmunge-dev  # Used to build Slurm auth/munge plugin
    state: present
    update_cache: true
    cache_valid_time: 3600
  when: ansible_os_family == "Debian"

- name: Enable the CodeReady repo for access to munge-devel
  community.general.rhsm_repository:
    name: codeready-builder-for-rhel-8-x86_64-rpms
    state: enabled
  when: ansible_os_family == "RedHat"

- name: Install RedHat-specific Munge packages
  ansible.builtin.yum:
    pkg:
      - munge
      - munge-devel  # Used to build Slurm auth/munge plugin
    state: present
  when: ansible_os_family == "RedHat"

# The Munge install process creates the 'munge' user, and does not take direction to not
# https://github/com/dun/munge/blob/master/QUICKSTART
- name: Remove the 'munge' user from the system
  ansible.builtin.user:
    name: munge
    state: absent
    force: true

- name: Ensure Munge directories are owned by the correct user
  ansible.builtin.file:
    path: "{{ item.path }}"
    state: directory
    owner: "{{ slurm_user }}"
    group: "{{ slurm_user }}"
    mode: "{{ item.mode }}"
  with_items:
    - { path: '/etc/munge', mode: '0700' }
    - { path: '/var/lib/munge', mode: '0700' }
    - { path: '/var/log/munge', mode: '0700' }
    - { path: '/run/munge', mode: '0755' }

- name: Copy the munge.key to the system
  ansible.builtin.copy:
    src: munge.key
    dest: /etc/munge/munge.key
    owner: "{{ slurm_user }}"
    group: "{{ slurm_user }}"
    mode: 0400

- name: Copy the Munge Systemd unitfile to the system
  ansible.builtin.template:
    src: munge.service.j2
    dest: /etc/systemd/system/munge.service
    owner: root
    group: root
    mode: 0644

- name: Ensure the Munge service is started and enabled
  ansible.builtin.systemd:
    name: munge
    enabled: true
    state: restarted
    daemon_reload: true
