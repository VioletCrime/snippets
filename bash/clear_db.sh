#!/bin/bash

#set -ex

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

usage(){
    cat <<-EOF
    usage: $PROGNAME [ -h | -x | -s | -c | -r ]
    This program streamlines apache service state management along with the ability to clear the `cs-ace`
    mysql database in the CS10 stack. If no option is specified, apache will be stopped on the management node(s),
    the cs-ace database will be wiped (drop && create), alembic will be invoked to create the DB schema, and then
    apache service will be started again. Only one option should be specified.

    OPTIONS:
        -h --help           Show this help and exit
        -x --stop           Stop the apache service on the management node(s).
        -s --start          Start the apache service on the management node(s).
        -c --clear          Clear the 'cs-ace' database only. No schema reinit, no apache start.
        -n --clearnostart   Stop the apache service, and clear the database, but don't start apache.
        -f --clearnoreinit  Stop the apache service, and clear the database, but don't reinit, and don't start apache.
        -i --reinit         Run the alembic scripts to reinit the cs-ace database.
        -r --restart        Stop and start the apache service on the management node(s).

EOF
}

main() {
    NETWORK='conf'
    MGMT_HOSTS=`cat /etc/hosts | grep 'mgmt-m' | grep $NETWORK | cut -d ' ' -f1`
    MGMT_ONE=`echo $MGMT_HOSTS | cut -d ' ' -f1`
    DB_HOST=`cat /etc/hosts | grep 'db-m1' | grep $NETWORK | cut -d ' ' -f1`

    # Define and parse args
    options=`getopt -o h,x,s,c,n,f,i,r --long help,stop,start,clear,clearnostart,clearnoreinit,reinit,restart -n "$PROGNAME" -- "$@"`
    eval set -- $options
    case $1 in
        -h|--help)
            usage ;;
        -x|--stop)
            stop_apache ;;
        -s|--start)
            start_apache ;;
        -c|--clear)
            clear_db ;;
        -n|--clearnostart)
            stop_apache ; clear_db ; reinit_db ;;
        -f|--clearnoreinit)
            stop_apache ; clear_db ;;
        -i|--reinit)
            reinit_db ;;
        -r|--restart)
            stop_apache ; start_apache ;;
        --)
            stop_apache ; clear_db ; reinit_db ; start_apache ;;
    esac
}

stop_apache() {
    echo "Stopping apache services on the management node(s)."
    for HOST in $MGMT_HOSTS; do
        ssh stack@$HOST "sudo service apache2 stop"
    done
}

clear_db() {
    echo "Wiping the database."
    ssh stack@$DB_HOST "echo 'drop database if exists \`cs-ace\`;' | sudo mysql && echo 'create database \`cs-ace\`;' | sudo mysql"
}

reinit_db() {
    echo "Reinitializing the database."
    PROD_BASELINE_FILE=81f3b0d63cbb_10_0_2_baseline.py
    DEV_BASELINE_FILE=baseline_10_02_tables.py
    ALEMBIC_DIR=/var/lib/cs-ace/alembic
    VERSIONS_DIR=$ALEMBIC_DIR/versions
    ALEMBIC_EXEC=/opt/stack/service/cs-ace/venv/bin/alembic
    ALEMBIC_INI=/etc/cs-ace/alembic.ini

    ssh stack@$MGMT_ONE "sudo mv $VERSIONS_DIR/$PROD_BASELINE_FILE $ALEMBIC_DIR/$PRODUCT_BASELINE_FILE"
    ssh stack@$MGMT_ONE "sudo rm $VERSIONS_DIR/*.pyc"
    if [ -e $PROGDIR/$DEV_BASELINE_FILE ]; then
        echo "  Baseline alembic script ($PROGDIR/$DEV_BASELINE_FILE) was found!"
        scp $PROGDIR/$DEV_BASELINE_FILE stack@$MGMT_ONE:~
        ssh stack@$MGMT_ONE "sudo mv ~/$DEV_BASELINE_FILE $VERSIONS_DIR/$DEV_BASELINE_FILE"
    else
        echo "  Baseline alembic script ($PROGDIR/$DEV_BASELINE_FILE) not found!"
        echo "  Attempting to get it from http://192.168.223.249/html/baseline_10_02_tables.py"
        echo ""
        echo "=====  WGET OUTPUT  ====================="
        set -ex
        ssh stack@$MGMT_ONE "sudo wget http://192.168.223.249/html/baseline_10_02_tables.py -O $VERSIONS_DIR/$DEV_BASELINE_FILE"
        set +ex
        echo "========================================="
        echo ""
    fi
    ssh stack@$MGMT_ONE "sudo $ALEMBIC_EXEC --config $ALEMBIC_INI upgrade head 2>/dev/null"
    ssh stack@$MGMT_ONE "sudo rm $VERSIONS_DIR/$DEV_BASELINE_FILE"
    ssh stack@$MGMT_ONE "sudo rm $VERSIONS_DIR/*.pyc"
    ssh stack@$MGMT_ONE "sudo mv $ALEMBIC_DIR/$PROD_BASELINE_FILE $VERSIONS_DIR/$PROD_BASELINE_FILE"
}

start_apache() {
    echo "Starting apache on the management node(s)."
    for HOST in $MGMT_HOSTS; do
        ssh stack@$HOST "sudo service apache2 start"
    done
}

main $ARGS
