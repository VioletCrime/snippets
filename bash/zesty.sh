#!/bin/bash

for name in $(ls | grep '^test' | grep -v '^test_data' | grep -v 'test_keyfile'); do
    newname=$(echo $name | sed s/^t/z/g)
    mv $name $newname
done
