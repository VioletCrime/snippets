#!/bin/bash

cat <<EOF > /etc/yum.repos.d/dev.repo
[atlas-${atlas.version}]
name=atlas MAT repository for $releasever - $basearch
baseurl=${atlas.version.url}
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fusion
EOF
