# Created using version 8.1.0.0-20140625.085706 
# This file is used by the CLI to install the
# CloudSystem Console on an existing hypervisor-server.
# The product is installed as an instance on this hypervisor-server.
#
# The hypervisor can be ESX 5.0+ or KVM 6.4
# See the install guide included in the release download for more details.
#
# Any field may be wrapped in quotation marks except for those that contain passwords.

[Defaults]
file-version = 9

####################
# Hypervisor values for accessing and managing the hypervisor-server.
#
# type = KVM / ESX
# ip = IPv4 address or fully qualified domain name of the hypervisor-server
# username = administrator user name
# password = administrator password
# cluster = The cluster to use when deploying the appliance (Only used for ESX)
####################
[Hypervisor]
type = KVM
ip = 16.125.104.181
username = root
password = hpvse1
cluster =

####################
# Image file location:
# KVM -- Should be an absolute path to your own directory and image file.
# ESXi -- Should be the name of your template VM or a path of the format:
#         [Datastore] Folder/VM-name.vmx
# SHA1 Checksum -- If the checksum is placed alongside the image and has the
#     same name as the image with an appended '.sha1', the CLI will verify this
#     checksum against the image.
# base-image = Base appliance image file or template name
#     default = KVM = /CloudSystem/images/CS-Base-8.1.0.0.qcow2
#               ESX = CS-Base-8.1.0.0
# sdn-image = SDN appliance image file or template name
#     default = KVM = /CloudSystem/images/CS-SDN-8.1.0.0.qcow2
#               ESX = CS-SDN-8.1.0.0
# net-node-image = Network node image file or template name
#     default = KVM = /CloudSystem/images/CS-NN-8.1.0.0.qcow2
#               ESX = CS-NN-8.1.0.0
# proxy-image = ESX proxy image file or template name (Optional)
#     default = KVM = /CloudSystem/images/CS-Base-8.1.0.0.qcow2
#               ESX = CS-Base-8.1.0.0
# enterprise-image = Enterprise appliance image file or template name (Optional)
#     default = KVM = /CloudSystem/images/CS-Enterprise-8.1.0.0.qcow2
#               ESX = CS-Enterprise-8.1.0.0
# Note if you do not want to use default values for optional keys simply
# comment out the entire line with a '#'
####################
[Images]
base-image = /CloudSystem/iscapp01/images/CS-Base-8.0.2.0.qcow2
sdn-image = /CloudSystem/iscapp01/images/CS-SDN-8.0.2.0.qcow2
net-node-image = /CloudSystem/iscapp01/images/CS-NN-8.0.2.0.qcow2
proxy-image = default
enterprise-image = /CloudSystem/iscapp01/images/CS-Enterprise-8.0.2.0.qcow2


####################
# Appliance Setup values are various values used in the initial setup
# of the appliance.
#
# password = The password to set for the administrator user in the appliance
#            This must be a minimum of 8 characters.
# support-access = Mimics the 'allow support personel to access your system'
#                  question in the ISC GUI.  Values are 'enabled' or 'disabled',
#                  normally set this to 'enabled'.
# glance-size = The size in GBs of the glance disk to create
# security-checking = Will enforce ssh/ssl checking during communication with
#                     the hypervisor.  Values are 'enabled' or 'disabled',
#                     normally set this to enabled if you have valid certificates
#                     or keys to the hypervisor.
####################
[Appliance Setup]
password = serveradmin
support-access = enabled
glance-size = 20
security-checking = enabled


####################
# Network Mapping Section
#
# If you are not installing or upgrading Enterprise, just leave
# enterprise_hostname and enterprise_ip blank.
#
# foundation_hostname = fully qualified host name of the Foundation appliance,
#            also used to form name of the virtual machine instance.
# enterprise_hostname = fully qualified host name of the Enterprise appliance,
#            also used to form name of the virtual machine instance.
# type = static or dhcp
# foundation_ip = foundation appliance IP address [static configuration only]
# enterprise_ip = enterprise appliance IP address [static configuration only]
# gateway = IP address of gateway [static configuration only]
# netmask = subnet mask [static configuration only]
# dns = comma-separated IP addresses of primary and alternate
#       dns server(s) [static configuration only]
# bridge = bridge (KVM) or network (VMware) name
# ** Note! bridges/networks are assumed to have already been created on the hypervisor.
#
####################
[Access Network]
foundation_hostname = p05-iscapp01-CAN.vse.adapps.hp.com
enterprise_hostname = p05-iscapp01-ENT-CAN.vse.adapps.hp.com
type = static
foundation_ip = 16.125.108.81
enterprise_ip = 16.125.108.82
gateway = 16.125.104.1
netmask = 255.255.248.0
dns =
bridge = br-accessnet

[Data Center Management Network]
foundation_hostname = p05-iscapp01.vse.adapps.hp.com
enterprise_hostname = p05-iscapp01-ENT.vse.adapps.hp.com
type = static
foundation_ip = 16.125.104.197
enterprise_ip = 16.125.108.82
gateway = 16.125.104.1
netmask = 255.255.248.0
dns = 16.125.24.19, 16.125.64.12
bridge = br0

[Cloud Management Network]
bridge = mgmt1br

[Cloud Data Trunk]
bridge = cb1br

[External Network]
bridge = br0
