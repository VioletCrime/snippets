#!/bin/bash

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

set -x
echo "Cleaning up previous deployment"
$PROGDIR/clean_host.sh > /dev/null 2>&1 # May return non-0
set -e
$PROGDIR/run_csstart.sh
$PROGDIR/run_csdeploy.sh
$PROGDIR/prep_enterprise.sh

#Simple, right?
