#!/bin/bash
set -ex

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

source $PROGDIR/common_names.sh

$PROGDIR/run_remote_script.sh $MA1_USER@$MA1_HOST $MA1_PW /home/cloudadmin/refresh_enterprise.sh
$PROGDIR/prep_enterprise.sh
