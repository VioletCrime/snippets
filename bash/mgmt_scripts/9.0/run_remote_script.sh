#!/usr/bin/expect -f

# $1 - User@Host
# $2 - Password
# $3 - Script to run (mind the path!)

set arg1 [lindex $argv 0]
set arg2 [lindex $argv 1]
set arg3 [lindex $argv 2]

spawn bash -c "ssh $arg1 sudo bash -c \"$arg3\""
expect {
  -re ".*es.*o.*" {
  exp_send "yes\r"
  exp_continue
  }
  -re ".*sword.*" {
  exp_send "$arg2\r"
  }
}
interact
