#!/bin/bash
set -ex

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

source $PROGDIR/common_names.sh

COMMON_BITS=$(ls $COMMON_BITS_DIR)
for bit in $COMMON_BITS; do
  $PROGDIR/upload_file_to_host.sh $COMMON_BITS_DIR/$bit $MA1_USER@$MA1_HOST:~ $MA1_PW
done

# Upload bits to the bootstrap MA
MA_BITS=$(ls $MA_BITS_DIR)
for bit in $MA_BITS; do
  $PROGDIR/upload_file_to_host.sh $MA_BITS_DIR/$bit $MA1_USER@$MA1_HOST:~ $MA1_PW
done

$PROGDIR/run_remote_script.sh $MA1_USER@$MA1_HOST $MA1_PW /home/cloudadmin/prep_and_run_csdeploy.sh
