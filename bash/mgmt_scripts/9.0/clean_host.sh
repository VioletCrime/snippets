#!/bin/bash

VMS='cloudsystem-cayenne-m04
     cs-mgmt1
     cs-mgmt2
     cs-mgmt-controller
     cs-controller1
     cs-controller2
     cs-swift-controller
     cs-swift-controller1
     cs-swift-controller2
     cs-enterprise-controller
     cs-enterprise1-controller
     cs-enterprise2-controller
     cs-update-controller'

IMAGE_DIR='/CloudSystem/'
for vm in $VMS; do
    virsh destroy $vm
    virsh undefine $vm
    rm -f $IMAGE_DIR/$vm.qcow2 $IMAGE_DIR/$vm.xml
done

rm $IMAGE_DIR/cs-mgmt-controllerdisk1.qcow2
rm $IMAGE_DIR/cs-mgmt1disk1.qcow2
rm $IMAGE_DIR/cs-mgmt2disk1.qcow2
