#!/bin/bash
set -ex

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

EA1_USER="cloudadmin"
EA1_PW="cloudadmin"
EA1_HOST="ea1"
EA_BITS_DIR="$PROGDIR/appliance-bits/ea1"
COMMON_BITS_DIR="$PROGDIR/appliance-bits/common"

COMMON_BITS=$(ls $COMMON_BITS_DIR)
for bit in $COMMON_BITS; do
  $PROGDIR/upload_file_to_host.sh $COMMON_BITS_DIR/$bit $EA1_USER@$EA1_HOST:~ $EA1_PW
done

# Upload bits to the bootstrap MA
EA_BITS=$(ls $EA_BITS_DIR)
for bit in $EA_BITS; do
  $PROGDIR/upload_file_to_host.sh $EA_BITS_DIR/$bit $EA1_USER@$EA1_HOST:~ $EA1_PW
done

$PROGDIR/run_remote_script.sh $EA1_USER@$EA1_HOST $EA1_PW /home/cloudadmin/setup_enterprise.sh
