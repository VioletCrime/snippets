#!/usr/bin/expect -f

# $1 - File to transfer
# $2 - Full destination (host and path)
# $3 - Password to host

set arg1 [lindex $argv 0]
set arg2 [lindex $argv 1]
set arg3 [lindex $argv 2]

spawn bash -c "scp $arg1 $arg2"
expect {
  -re ".*es.*o.*" {
  exp_send "yes\r"
  exp_continue
  }
  -re ".*sword.*" {
  exp_send "$arg3\r"
  }
}
interact
