#!/bin/bash

export OS_USERNAME=admin
export OS_TENANT_NAME=demo
export OS_PASSWORD=unset
export OS_AUTH_URL=http://192.168.0.20:5000/v2.0
export OS_REGION_NAME=RegionOne
export OS_AUTH_TOKEN=$(keystone token-get | grep ' id' | cut -d '|' -f 3 | xargs echo)
