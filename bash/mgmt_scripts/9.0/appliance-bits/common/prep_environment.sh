cp /home/cloudadmin/export_token.sh /usr/local/bin/export_token

echo 'alias cdcsmigrate="cd /opt/stack/venvs/csmigrate/lib/python2.7/site-packages/csmigrate"' >> ~/.bashrc
echo 'alias refresh_enterprise="export_token; echo 'yes' | csdeploy -d -a cs-enterprise; echo 'yes' | csdeploy -d -a cs-enterprise1; echo 'yes' | csdeploy -d -a cs-enterprise2; csdeploy --add_enterprise"' >> ~/.bashrc
echo 'alias refresh_enterprise_non_ha="export_token; echo 'yes' | csdeploy -d -a cs-enterprise; echo 'yes' | csdeploy -d -a cs-enterprise1; echo 'yes' | csdeploy -d -a cs-enterprise2; csdeploy --add_enterprise --disable_ha"' >> ~/.bashrc
echo 'alias l="ls -sahil"' >> ~/.bashrc
echo 'alias sl="ls"' >> ~/.bashrc
echo 'alias ls="ls --color=auto"' >> ~/.bashrc
