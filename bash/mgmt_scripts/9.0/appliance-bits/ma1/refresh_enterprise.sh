#!/bin/bash

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

set -ex

. export_token.sh
echo yes | csdeploy -d -a cs-enterprise
echo yes | csdeploy -d -a cs-enterprise1
echo yes | csdeploy -d -a cs-enterprise2
csdeploy --add_enterprise
