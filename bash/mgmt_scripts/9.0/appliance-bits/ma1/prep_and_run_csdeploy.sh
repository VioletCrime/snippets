#!/bin/bash

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

set -ex

# Check that upstart has completed.
status=$(tail -1 /var/log/upstart/os-collect-config.log)
while [[ $status != *"os-refresh-config:Completed phase migration"* ]]; do
    sleep 5
    status=$(tail -1 /var/log/upstart/os-collect-config.log)
done

MYSQL_DEB=libmysql-java_5.1.32-1_all.deb
MYSQL_DIRS="/boot/cloudsystem/cs-enterprise /boot/cloudsystem/cs-enterprise1 /boot/cloudsystem/cs-enterprise2"

cd $PROGDIR
./prep_environment.sh
. export_token.sh
cshaupdate -t $(echo $OS_AUTH_TOKEN) -v 73,74,75
python update_fti.py fti-settings.json $(echo $OS_AUTH_TOKEN)
for MYSQL_DIR in MYSQL_DIRS; do
    cp $MYSQL_DEB $MYSQL_DIR/$MYSQL_DEB
    chown cloudsystem:cloudsystem $MYSQL_DIR/$MYSQL_DEB
    chmod 644 $MYSQL_DIR/$MYSQL_DEB
done

sleep 10
#csdeploy -m "2501,1575,1800" -s "1202,1523" -1 "192.168.218.37-192.168.218.49" --dc_routes "10.1.0.0/16,192.168.216.1" --can_cidr "192.168.216.0/21" --can_gateway "192.168.216.1" --add_enterprise
#csdeploy -m "2501,1575,1800" -s "1202,1523" -1 "192.168.218.37-192.168.218.49" --clm_cidr "178.20.0.0/21" --dc_routes "10.1.0.0/16,192.168.216.1" --can_cidr "192.168.216.0/21" --can_gateway "192.168.216.1"
csdeploy -m "2501,1575,1800" -s "1202,1523" -1 "192.168.218.37-192.168.218.49" --dc_routes "10.1.0.0/16,192.168.216.1" --can_cidr "192.168.216.0/21" --can_gateway "192.168.216.1"
. export_token.sh
csdeploy --add_enterprise
