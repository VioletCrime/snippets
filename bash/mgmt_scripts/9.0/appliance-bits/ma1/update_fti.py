#!/usr/bin/env python

'''
To get FTI settings, run the following command:
	curl -k -X GET http://localhost:6666/rest/cloudsystem/v1.0/info/fti-settings | python -mjson.tool > tmp.json

Update the json file, then pass it to this script to update the DB.
'''

import json
import requests
import argparse

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Update FTI settings in the DB.')
	parser.add_argument('json_file', metavar='<FILE>',
                   help='The json file with FTI settings content.')
        parser.add_argument('auth_token', help='The auth token required to validate the rest call')
	args = parser.parse_args()

	with open(args.json_file) as f:
		json_content = json.load(f)
	
	url = "http://localhost:6666/rest/cloudsystem/v1.0/info"
	args_dict = {"fti-settings" : json.dumps(json_content)}
	body= json.dumps(args_dict)
	standard_headers = {'Accept': 'application/json', 'Content-Type': 'application/json', 'X-Auth-Token': '{0}'.format(args.auth_token)}
	resp = requests.put(url, headers=standard_headers, data=body)
	print resp.content
