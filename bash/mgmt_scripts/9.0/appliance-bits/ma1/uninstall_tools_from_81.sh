#!/bin/bash

FOUNDATION_IP="192.168.217.15"
ENTERPRISE_IP="192.168.217.16"

ssh root@$FOUNDATION_IP yum erase -y libgssglue libevent keyutils
ssh root@$ENTERPRISE_IP yum erase -y libgssglue libevent keyutils eve-migration graffiti-migration
