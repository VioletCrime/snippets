#!/bin/bash

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

set -ex

cd $PROGDIR
./prep_environment.sh
export OS_USERNAME=admin
export OS_TENANT_NAME=demo
export OS_PASSWORD=unset
export OS_AUTH_URL=http://192.168.0.20:5000/v2.0
export OS_REGION_NAME=RegionOne
auth_token=`keystone token-get | grep ' id' | cut -d '|' -f 3 | xargs echo`

CSMIGRATE_PKG=$(ls | grep 'csmigrate-debs-.*.tar.gz' | sort | head -1)
if [[ -n $CSMIGRATE_PKG ]]; then
    tar -xzvf $CSMIGRATE_PKG
    dpkg -i csmigrate.deb
fi

if [[ -e /home/cloudadmin/migration_vol.tar.gz ]]; then
    mv /home/cloudadmin/migration_vol.tar.gz /mnt
    pushd .
    cd /mnt
    tar -xzvf migration_vol.tar.gz
    popd
fi
