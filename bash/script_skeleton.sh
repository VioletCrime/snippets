#!/usr/bin/env bash

set -e

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

usage() {
        cat <<- EOF
Usage: ./${PROGNAME} OPTIONS...

Options:
  -h, --help      Show this message and exit
EOF
}

parse_args() {
    options=`getopt -o h --long help -n "${PROGNAME}" -- "${@}"`
    eval set -- ${options}
    while true
    do
        case $1 in
            -h|--help)
                usage ; exit 0 ;;
        esac
    done
}

main() {
    parse_args ${ARGS}
}

main
