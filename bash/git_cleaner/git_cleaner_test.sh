#!/usr/bin/env bash

set -e

PROGDIR="$(readlink -m $(dirname ${0}))"

testFullNoLfs() {
    COMMIT_MSG="This is but a test"
    REPO_BASENAME="${TEST_REPO_BASENAME}"
    REPO_CLONE_ADDR="${TEST_REPO_CLONE_ADDR}"
    SUFFIXES_TO_SCRUB="pyc class deb tar.gz jar"
    FILES_TO_SCRUB="secrets.txt id_rsa bin/test_server.bin bin/test_server_win.exe"
    SUFFIXES_TO_LFS=""
    FILES_TO_LFS=""
    LFS_REPO_URL=""
    WORKSPACE="${TEST_DIR}/testFullNoLfs"
    cd "${PROGDIR}/.."
    export_env_and_run
    cd "${WORKSPACE}/${REPO_BASENAME}"
    assert_repo_state
}

testFullSomeLfs() {
    COMMIT_MSG="The ROFLcoptering"
    REPO_BASENAME="${TEST_REPO_BASENAME}"
    REPO_CLONE_ADDR="${TEST_REPO_CLONE_ADDR}"
    SUFFIXES_TO_SCRUB="pyc class deb tar.gz"
    FILES_TO_SCRUB="secrets.txt id_rsa"
    SUFFIXES_TO_LFS="jar"
    FILES_TO_LFS="bin/test_server.bin bin/test_server_win.exe"
    LFS_REPO_URL=""
    WORKSPACE="${TEST_DIR}/testFullSomeLfs"
    cd "${PROGDIR}/.."
    export_env_and_run
    cd "${WORKSPACE}/${REPO_BASENAME}"
    assert_repo_state
}

testFullOnlyLfs() {
    COMMIT_MSG=":ranch-dab: YEETing files to LFS :kekw:"
    REPO_BASENAME="${TEST_REPO_BASENAME}"
    REPO_CLONE_ADDR="${TEST_REPO_CLONE_ADDR}"
    SUFFIXES_TO_SCRUB=""
    FILES_TO_SCRUB=""
    SUFFIXES_TO_LFS="jar pyc class deb tar.gz"
    FILES_TO_LFS="bin/test_server.bin bin/test_server_win.exe secrets.txt id_rsa"
    LFS_REPO_URL=""
    WORKSPACE="${TEST_DIR}/testFullOnlyLfs"
    cd "${PROGDIR}/.."
    export_env_and_run
    cd "${WORKSPACE}/${REPO_BASENAME}"
    assert_repo_state
}

. "${PROGDIR}/.git_cleaner_test_helper.sh"
ensure_requisites  # Do network-enabled setup prior to sourcing the unittest lib which forces fault-tolerance
cd "${PROGDIR}"
if [[ ! -d "shunit2" ]]; then
    git clone https://github.com/kward/shunit2 shunit2
fi
. "${PROGDIR}/shunit2/shunit2"
