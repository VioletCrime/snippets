#!/usr/bin/env bash

# This script is meant to provide a general solution to the concerningly-common task of removing large files
# from Git commit histories. Simply codify your repo details and requirements in the customer-defined vars
# section below, and begin doing test runs.
#
# TESTING NOTES: It is inadviseable to simply jot down the repo details below, and run the script expecting
# a perfect outcome. You'll likely want to run this script multiple times and revise the settings based on
# observations of the resulting repo state. In order to make this as painless as possible, I would suggest-
#
#  * [Ignore if setting LFS_REPO_URL] Do not set REPO_PUSH_ADDR until you're ready to set up a demo repo / 
#      do the full migration. The script will skip the push step if it's empty or undefined, so only set it
#      when 'it's time'.
#
#  * Make a local mirror of the repo you're running against; these clones can be very long-running on really
#      large repos... which this script is designed to run against. Simply clone with --mirror to some place NOT
#      in the WORKSPACE dir, as that gets blown away at the start of every run, and then set REPO_CLONE_ADDR
#      to file://<path_to_clone>, and git will skip network operations entirely.
#
#  * Consider making a tmpfs mount on the WORKSPACE directory- note it will need to be >2x the repo size to
#      be sure to have enough space for a local --mirror clone, a workspace clone, and enough room for things
#      like the analyses output directories. e.g. `sudo mount -t tmpfs -o size=1G tmpfs /tmp/git_cleaner_ws`
#
#  * Pay attention to the postrun analysis output, contained in the /tmp/git_cleaner_ws/repo_analyses dir.
#      These are the highest-value metrics I know of in measuring repo size reduction. Git-sizer is among
#      the most valuable, to the point I almost considered making it a requirement for this script.
#
#
# REQUIREMENTS:
#    - BFG jar is available, its path codified as 'BFG_JAR_PATH' below, and a Java 8 Runtime to execute it.
#          https://rtyley.github.io/bfg-repo-cleaner/
#    - git-lfs    https://www.atlassian.com/git/tutorials/git-lfs
# OPTIONAL - Analysis tools:
#    - git-sizer  https://github.com/github/git-sizer
#
# POST_RUN_CONCERNS:
#    - When --mirror pushing a repo to REPO_PUSH_ADDR, the HEAD branch is set arbitrarily. Go into GitHub and
#          set the default branch, probably to 'main' or 'master'.  https://stackoverflow.com/a/73591617
#

#####  Customer-defined repo cleanup details  #########################################################################

# These values are defined such that they are overrideable by environment variables of the same name. Useful
# for automation, such as testing.

# This script will add commits to every branch; define the commit message here
COMMIT_MSG=${COMMIT_MSG:-"Scrub binaries and large files from the Git object storage."}

# Path to the BFG jar. Needs to be absolue, or relative to '~'. Tested with 1.14.0 (latest)
BFG_JAR_PATH=${BFG_JAR_PATH:-"/usr/share/java/bfg-1.14.0.jar"}

# Name of the repo, used to set up the workspace directory structure.
REPO_BASENAME=${REPO_BASENAME:-""}

# Full address to Git clone from. Can be "git@github...", "https://github...", or even "file://<path>"
REPO_CLONE_ADDR=${REPO_CLONE_ADDR:-""}

# Full address to push the cleaned repo to. Can be "git@github...", "https://github...", or even "file://<path>"
# This should __NEVER__ point to the single-source-of-truth repo we're cleaning up.
# I expect this is obvious to you, and I expect branch protections are in place to keep impact minimal
# should someone make this mistake, but this script makes DESTRUCTIVE and UNRECOVERABLE changes to the commit
# history. The repo we're cleaning should be archived and potentially moved, but never overwritten with the
# cleaned repo this script generates.
REPO_PUSH_ADDR=${REPO_PUSH_ADDR:-""}

# Whitespace-separated list of file extensions to add to .gitignore and scrub from git history.
# May be empty if no suffixes are to be scrubbed.
#SUFFIXES_TO_SCRUB=${SUFFIXES_TO_SCRUB:-"o elf map bin mex t32"} # Example
SUFFIXES_TO_SCRUB=${SUFFIXES_TO_SCRUB:-""}

# Whitespace-separated list of paths (from root of repo) to scrub from git history. May be empty if no files are to
# be scrubbed.
FILES_TO_SCRUB=${FILES_TO_SCRUB:-""}

# Whitespace-separated list of file extensions to add to .gitattributes and migrate to git-lfs. ex. "bin png jpeg".
# May be empty if no suffixes need migrating to LFS.
SUFFIXES_TO_LFS=${SUFFIXES_TO_LFS:-""}

# Whitespace-separated list of paths (from root of repo) to add to .gitattributes and migrate to git-lfs.
# May be empty if no files need migrating to LFS.
FILES_TO_LFS=${FILES_TO_LFS:-""}

# Address of the LFS repo to push LFS objects to. May be empty if no files need migrating to LFS.
LFS_REPO_URL=${LFS_REPO_URL:-""}
#######################################################################################################################

# Runtime variables
WORKSPACE=${WORKSPACE:-"/tmp/git_cleaner_ws"}
LOCAL_MIRROR_NAME="${REPO_BASENAME}_remote"
LOCAL_MIRROR_ADDR="file://${WORKSPACE}/${LOCAL_MIRROR_NAME}"
ANALYSIS_BASEDIR="${WORKSPACE}/repo_analyses"
PROGNAME=$(basename ${0})


check_arguments() {
    if [[ ! ${LFS_REPO_URL} == "" && ${REPO_PUSH_ADDR} == "" ]]; then
        pprint "ERROR: 'LFS_REPO_URL' was set, which requires 'REPO_PUSH_ADDR' be set as well, but it's not."
        exit 1
    fi
}

pprint() {
    printf "\n    [${PROGNAME}] ${@}\n"
}

run() {
    pprint "Running \"${@}\"\n"
    eval "${@}"
}

clean_tip_and_standardize_gitignore() {
    cd ${WORKSPACE}/${REPO_BASENAME}  # Doesn't hurt to be sure
    # Parse the default (HEAD) branch as defined by origin
    main_branch=$(git remote show origin | grep "HEAD branch:" | cut -d':' -f2 | tr -d '[:space:]')
    run "git fetch --all"
    run "git lfs fetch --all"
    run "git checkout ${main_branch}"

    # Start by cleaning the main branch, with the added step of removing files to be scrubbed from tip
    for suffix in ${SUFFIXES_TO_SCRUB}; do
        run "git rm \"*.${suffix}\" || $(which true)"
    done
    for phyle in ${FILES_TO_SCRUB}; do
        run "git rm ${phyle} || $(which true)"
    done
    standardize_configs

    # Iterate over the remaining branches, standardizing configs
    remote_branches=$(git branch -r | grep -v /HEAD | grep -v /${main_branch})
    pprint "Remote branches: \n${remote_branches}"
    for remote_branch in ${remote_branches}; do
        branch_name=$(echo ${remote_branch} | rev | cut -d '/' -f 1 | rev)
        run "git checkout -B ${branch_name} ${remote_branch}"
        standardize_configs
    done
}

# Configure Git accordingly, commit, and push the changes
standardize_configs() {
    # Add suffixes we're scrubbing to .gitignore
    for suffix in ${SUFFIXES_TO_SCRUB}; do
        if [[ ! $(cat .gitignore | grep -F "*.${suffix}" 2>/dev/null) >/dev/null ]]; then
            echo "*.${suffix}" >> .gitignore
        fi
    done
    for file in ${FILES_TO_SCRUB}; do
        if [[ ! $(cat .gitignore | grep -F "${file}" 2>/dev/null) >/dev/null ]]; then
            echo "${file}" >> .gitignore
        fi
    done

    # No idea why `git commit -am` and `git add -A` sometimes misses these, but it does
    run "git add .gitignore || $(which true)"
    run "git add -A --renormalize"
    run "git commit -m \"${COMMIT_MSG} (.gitignore)\""
    run "git push"
}

configure_lfs() {
    cd ${WORKSPACE}/${REPO_BASENAME}
    remote_branches=$(git branch -r | grep -v /HEAD)
    pprint "Remote branches: \n${remote_branches}"
    for remote_branch in ${remote_branches}; do
        branch_name=$(echo ${remote_branch} | rev | cut -d '/' -f 1 | rev)
        run "git checkout -B ${branch_name} ${remote_branch}"
        if [[ ! ${LFS_REPO_URL} == "" ]]; then
            run "git config -f .lfsconfig lfs.url ${LFS_REPO_URL}"
        fi
        # Add suffixes and files we're migrating to LFS to .gitattributes
        for suffix in ${SUFFIXES_TO_LFS}; do
            run "git lfs track \"*.${suffix}\""
        done
        for file in ${FILES_TO_LFS}; do
            run "git lfs track \"${file}\""
        done

        run "git add .lfsconfig || $(which true)"
        run "git add .gitattributes || $(which true)"
        run "git add -A --renormalize"
        run "git commit -m \"${COMMIT_MSG} (lfs-config)\""
        run "git push"
        run "git lfs push --all origin"
    done
}

run_bfg() {
    local to_scrub=${1}
    local bfg_action=${2}
    cd ${WORKSPACE}
    run "java -jar ${BFG_JAR_PATH} ${bfg_action} \"${to_scrub}\" ./${REPO_BASENAME}"
    cd ${REPO_BASENAME}
    run "git reflog expire --expire=now --all"
    run "git gc --prune=now --aggressive"
    run "git push --force --mirror"
}

clone_repo() {
    local clone_flags=${1:-""}
    cd ${WORKSPACE}
    run "rm -rf ./${REPO_BASENAME}"
    run "git clone ${clone_flags} ${LOCAL_MIRROR_ADDR} ${REPO_BASENAME}"
}

do_final_git_push() {
    cd ${WORKSPACE}/${REPO_BASENAME}
    run "git remote add upstream ${REPO_PUSH_ADDR}"
    run "git push --mirror upstream"
}

analyze() {
    OUTPUT_DIR=${1}
    printf "\n    [${PROGNAME}] Running Analysis-\n"
    echo "        Identifying largest files:" | tee ${OUTPUT_DIR}/largest_packfiles.txt
    packfile=$(ls .git/objects/pack/*.idx)
    git verify-pack -v ${packfile} | sort -k 3 -n | tail -10 | while read lfile ; do
        sha=$(echo ${lfile} | awk '{$1=$1};1' | cut -d " " -f1)
        fsize=$(echo ${lfile} | awk '{$1=$1};1' | cut -d " " -f3 | numfmt --to iec --format "%.2f")
        fpath=$(git rev-list --objects --all | grep ${sha})
        echo "${fpath} - (${fsize})" | tee -a ${OUTPUT_DIR}/largest_packfiles.txt
    done
    git lfs ls-files --all > ${OUTPUT_DIR}/lfs-ls-files.txt  # Output very large; obfuscate
    run "ls -shail .git/objects/pack | tee ${OUTPUT_DIR}/packfile_size.txt"
    run "git lfs migrate info --everything | tee ${OUTPUT_DIR}/lfs_migrate_info.txt"
    if [[ $(which git-sizer 2>/dev/null) >/dev/null ]]; then
        run "git-sizer --verbose | tee ${OUTPUT_DIR}/git-sizer.output"
    fi
}

main() {
    # Make sure the inputs are valid
    check_arguments

    # Ensure a fresh workspace to operate in
    run "rm -rf ${WORKSPACE}"
    run "mkdir -p ${WORKSPACE}"
    cd ${WORKSPACE}

    # Large files take a while to xfer, and we have several clones ahead of us. Avoid long network-
    # enabled git operations by instead affecting a local mirror of the repo we're cleaning.
    run "git clone --bare ${REPO_CLONE_ADDR} ${LOCAL_MIRROR_NAME}"
    clone_repo

    # Run initial analysis. Set up dirs for a post-cleanup analysis while we're at it.
    mkdir -p ${ANALYSIS_BASEDIR}/{prerun,postrun}
    cd ${REPO_BASENAME}
    analyze ${ANALYSIS_BASEDIR}/prerun

    # Now that the tip changes have been pushed, run The BFG against fresh mirror clones as required
    if [[ ! ${SUFFIXES_TO_SCRUB} == "" || ! ${FILES_TO_SCRUB} == "" ]]; then
        # BFG does not clean tip of main/master, instead prescribing we clean it ourselves beforehand
        clean_tip_and_standardize_gitignore
        pprint "Scrubbing files and suffixes"
        to_scrub="{"
        for suffix in ${SUFFIXES_TO_SCRUB}; do
            to_scrub="${to_scrub}*.${suffix},"
        done
        for file_to_scrub in ${FILES_TO_SCRUB}; do
            # The BFG doesn't allow paths on files to delete? idk, but account for it
            f=$(echo ${file_to_scrub} | rev | cut -d '/' -f 1 | rev)
            to_scrub="${to_scrub}${f},"
        done
        to_scrub="${to_scrub%,}}"
        clone_repo "--mirror"
        run_bfg "${to_scrub}" "--delete-files"
    fi
    if [[ ! ${SUFFIXES_TO_LFS} == "" || ! ${FILES_TO_LFS} == "" ]]; then
        pprint "Setting up LFS"
        # Once we set lfs.url, we can no longer use file:// addresses- all pushes must be to github,
        # and therefore all clones must be from the same.
        # https://github.com/git-lfs/git-lfs/issues/3893#issuecomment-549459508
        if [[ ! ${LFS_REPO_URL} == "" ]]; then
            clone_repo "--mirror"
            cd ${WORKSPACE}/${REPO_BASENAME}
            run "git remote set-url origin ${REPO_PUSH_ADDR}"
            run "git push --mirror origin"
            LOCAL_MIRROR_ADDR=${REPO_PUSH_ADDR}  # Ensure later 'clone_repo' calls use the non-local upstream
        fi
        clone_repo
        configure_lfs
        to_lfs="{"
        for suffix in ${SUFFIXES_TO_LFS}; do
            to_lfs="${to_lfs}*.${suffix},"
        done
        for file_to_lfs in ${FILES_TO_LFS}; do
            to_lfs="${to_lfs}${file_to_lfs},"
        done
        to_lfs="${to_lfs%,}}"
        clone_repo "--mirror"
        run_bfg "${to_lfs}" "--convert-to-git-lfs"
    fi

    # Clone once more to execute the post-run analysis
    clone_repo
    cd ${REPO_BASENAME}
    analyze ${ANALYSIS_BASEDIR}/postrun

    # If an upstream to push the final result is set, and the LFS workflow hasn't already mirrored to upstream,
    # then mirror now
    if [[ ! ${REPO_PUSH_ADDR} == "" && ${LFS_REPO_URL} == "" ]]; then
        clone_repo "--mirror"
        do_final_git_push
    fi
}

set -e
pushd . > /dev/null
main
popd > /dev/null 2>&1
