#!/usr/bin/env bash

# Figure out if BFG is installed, or if we need to download it. System dir is owned by root,
# so don't attempt to download directly to it, instead download into test dir.
BFG_VERSION=${BFG_VERSION:-"1.14.0"}
default_bfg_path="/tmp/bfg-${BFG_VERSION}.jar"
if [[ -f /usr/share/java/bfg-${BFG_VERSION}.jar ]]; then
    default_bfg_path="/usr/share/java/bfg-${BFG_VERSION}.jar"
fi
readonly BFG_JAR_PATH="${BFG_JAR_PATH:-${default_bfg_path}}"

# Test directory tree definitions
readonly TEST_DIR="/tmp/git_cleaner_test_workspace"  # Base of test dir
readonly TEST_UPSTREAMS_DIR="${TEST_DIR}/bare_repos"  # Dirty 'upstream' repos to run against
readonly TEST_REPO_BASENAME="my_module"
readonly TEST_REPO_CLONE_ADDR="file://${TEST_UPSTREAMS_DIR}/${TEST_REPO_BASENAME}"

# Variable test environment- overrideable by individual tests
COMMIT_MSG=""
REPO_BASENAME="${TEST_REPO_BASENAME}"
REPO_CLONE_ADDR="${TEST_REPO_CLONE_ADDR}"
REPO_PUSH_ADDR=""
SUFFIXES_TO_SCRUB=""
FILES_TO_SCRUB=""
SUFFIXES_TO_LFS=""
FILES_TO_LFS=""
LFS_REPO_URL=""

ensure_requisites() {
    # Ensure Java and The BFG are in place
    if [[ ! $(which java 2>/dev/null) >/dev/null ]]; then
        echo "ERROR: No Java Runtime found! Please install Java version 8 or greater"
        exit 1
    fi
    if [[ ! -e ${BFG_JAR_PATH} ]]; then
        set -e
        curl -o ${BFG_JAR_PATH} https://repo1.maven.org/maven2/com/madgag/bfg/1.14.0/bfg-1.14.0.jar
        set +e
    fi

    # Create the base of the test directory structure.
    if [[ ! -d ${TEST_DIR} ]]; then
        mkdir -p ${TEST_DIR}
    fi
    cd ${TEST_DIR}
    command rm -rf ./*  # Kyle's env implements a recycle-bin; this ignores aliases named 'rm'
    mkdir -p ${TEST_UPSTREAMS_DIR}
    create_dirty_upstream
}

create_dirty_upstream() {
    local repo_name=${1:-"my_module"}
    main_branch=${2:-"master"}
    local builder_dir="${repo_name}_builder"

    # Create the 'upstream' bare repo we want to run tests against as the single-source-of-truth repo
    cd ${TEST_UPSTREAMS_DIR}
    mkdir ${repo_name}
    cd ${repo_name}
    git init --bare
    git symbolic-ref HEAD refs/heads/${main_branch}

    # Clone the empty bare repo, and populate it
    cd ${TEST_DIR}
    git clone "file://${TEST_UPSTREAMS_DIR}/${repo_name}" "${builder_dir}"
    cd ${builder_dir}

    populate_main_branch
    add_python_feature "logging"
    add_python_feature "iam"
    add_python_feature "versioning"
    start_java_rewrite
    add_java_feature "httpserver"
    add_java_feature "api"
    add_java_feature "authentication"

    # Cleanup the repo used to build the bare repo
    cd ${TEST_DIR}
    command rm -rf ${builder_dir}
}

populate_main_branch() {
    # Init commit- just files at the repo root
    create_files "README.md Makefile pyproject.toml poetry.lock secrets.txt id_rsa"
    commit_and_push "${main_branch}" "[init] Add base files"

    # Create the python project across two commits; add tags and pyc files
    mkdir -p src/my_module
    create_files "src/setup.py src/setup.cfg"
    create_files "src/my_module/__init__.py src/my_module/__init__.pyc"
    commit_and_push "${main_branch}" "Create the basic Python module skeleton. Spoopy."
    git tag "v1.0.0"
    git push --tags
    create_files "src/my_module/module.py src/my_module/module.pyc"
    create_files "src/my_module/util.py" "src/my_module/util.pyc"
    commit_and_push "${main_branch}" "Add base logic to my_module"
    git tag "v1.0.1"
    git push --tags

    # Create dist files from the previous tags; commit to Git history, as one does
    mkdir -p dist
    create_files "dist/my-module_1.0.0.tar.gz dist/my-module_1.0.1.tar.gz"
    create_files "dist/my-module_1.0.0-1_amd64.deb dist/my-module_1.0.0-2_amd64.deb"
    create_files "dist/my-module_1.0.1-1_amd64.deb"
    commit_and_push "${main_branch}" "Release v1.0.0 and v1.0.1. Great job, team!"

    # Finally, create a couple of bins that might make sense to keep available alongside the code
    mkdir -p bin
    create_files "bin/bfg-1.14.0.jar bin/test_server.bin bin/test_server_win.exe"
    commit_and_push "${main_branch}" "Add bin files"
}

add_python_feature() {
    local feature_name=${1}
    git checkout -b ${feature_name}
    create_files "src/my_module/${feature_name}.py src/my_module/${feature_name}.pyc"
    commit_and_push "${feature_name}" "Add Python feature '${feature_name}'"
}

start_java_rewrite() {
    git checkout ${main_branch}
    java_dir="src/java"
    java_depdir="${java_dir}/dep"
    mkdir -p ${java_dir}
    mkdir -p ${java_depdir}
    create_files "${java_dir}/Main.java ${java_dir}/Main.class"
    create_files "${java_depdir}/JUnit-2.3.4.jar ${java_depdir}/Log4j-7.6.5.jar"
    commit_and_push "${main_branch}" "Start Java rewrite"
    git tag "v2.0.0"
    git push --tags
}

add_java_feature() {
    local feature_name=${1}
    git checkout ${main_branch}
    git checkout -b ${feature_name}
    create_files "${java_dir}/${feature_name}.java ${java_dir}/${feature_name}.class ${java_depdir}/${feature_name}.jar"
    commit_and_push "${feature_name}" "Add Java feature '${feature_name}"
}

create_files() {
    files=${1}
    for f in ${files}; do
        echo "testfile" > ${f}
    done
}

commit_and_push() {
    local branch=${1}
    local commit_msg=${2}
    git add -A
    git commit -m "${commit_msg}"
    git push -u origin ${branch}
}

# Variables have likely shifted as part of test setup; export the full suite every time
export_env_and_run() {
    export WORKSPACE BFG_JAR_PATH REPO_CLONE_ADDR COMMIT_MSG REPO_BASENAME REPO_PUSH_ADDR \
        SUFFIXES_TO_SCRUB FILES_TO_SCRUB SUFFIXES_TO_LFS FILES_TO_LFS LFS_REPO_URL
    ./scripts/git_cleaner
}

# Assumes CWD is the repo we're running asserts against
assert_repo_state() {
    echo "RUNNING ASSERTS ON ENVIRONMENT:"
    echo "BFG_JAR_PATH: '${BFG_JAR_PATH}'"
    echo "REPO_CLONE_ADDR: '${REPO_CLONE_ADDR}'"
    echo "COMMIT_MSG: '${COMMIT_MSG}'"
    echo "REPO_BASENAME: '${REPO_BASENAME}'"
    echo "REPO_PUSH_ADDR: '${REPO_PUSH_ADDR}'"
    echo "SUFFIXES_TO_SCRUB: '${SUFFIXES_TO_SCRUB}'"
    echo "FILES_TO_SCRUB: '${FILES_TO_SCRUB}'"
    echo "SUFFIXES_TO_LFS: '${SUFFIXES_TO_LFS}'"
    echo "FILES_TO_LFS: '${FILES_TO_LFS}'"
    echo "LFS_REPO_URL: '${LFS_REPO_URL}'"
    echo "WORKSPACE: '${WORKSPACE}'"
    
    # First, let's iterate through the branches, asserting all files should be as-expected
    remote_branches=$(git branch -r | grep -v /HEAD)
    for remote_branch in ${remote_branches}; do
        branch_name=$(echo ${remote_branch} | rev | cut -d '/' -f 1 | rev)
        if [[ $(git branch -l | grep ${branch_name}) > /dev/null ]]; then
            git checkout ${branch_name}
        else
            git checkout -b ${branch_name} ${remote_branch}
        fi
        for suffix in ${SUFFIXES_TO_LFS}; do
            # Assert every suffix is in .gitattributes
            if [[ ! $(cat .gitattributes | grep "${suffix}") ]]; then
                fail "Expected suffix '${suffix}' in .gitattributes, but didn't find it"
            fi
            # Assert every file of the given suffix is tracked by Git-LFS
            files=$(find . -type f -name "*.${suffix}")
            for file in ${files}; do
                f=$(echo ${file} | sed -s 's@^\./@@g')
                if [[ ! $(git lfs ls-files | grep "${f}") ]]; then
                    fail "Expected '${f}' to be tracked by Git LFS, but it isn't"
                fi
            done
        done
        for lfs_file in ${FILES_TO_LFS}; do
            if [[ ! $(cat .gitattributes | grep "${lfs_file}") ]]; then
                fail "Expected file '${lfs_file}' in .gitattributes, but didn't find it"
            fi
            if [[ $(git ls-files | grep "${lfs_file}") && ! $(git lfs ls-files | grep "${lfs_file}") ]]; then
                fail "Expected '${lfs_file}' to be tracked by Git LFS, but it isn't"
            fi
        done
        for suffix in ${SUFFIXES_TO_SCRUB}; do
            # Assert every suffix in .gitignore
            if [[ ! $(cat .gitignore | grep "${suffix}") ]]; then
                fail "Expected '${suffix}' to be included in .gitignore, but it isn't"
            fi
            if [[ $(git ls-files | grep "${suffix}$") ]]; then
                fail "Expected no files with suffix '${suffix}' to be in the commit history."
            fi
        done
        for scrub_file in ${FILES_TO_SCRUB}; do
            if [[ ! $(cat .gitignore | grep "${scrub_file}") ]]; then
                fail "Expected file '${scrub_file}' to be in .gitignore, but it isn't"
            fi
            if [[ $(git ls-files | grep "${scrub_file}") ]]; then
                fail "Expected file '${scrub_file}' was removed from the commit history."
            fi
        done
        if [[ ! ${LFS_REPO_URL} = "" ]]; then
            if [[ ! $(cat .lfsconfig | grep "${LFS_REPO_URL}") ]]; then
                fail "Expected LFS URL '${LFS_REPO_URL}' to be set in .lfsconfig, but it isn't"
            fi
        fi
        if [[ ! $(git log -n1 | grep "${COMMIT_MSG}") ]]; then
            fail "Expected most recent commit to have body '${COMMIT_MSG}', but it isn't"
        fi
    done

}
