#!/usr/bin/env bash

msg() {
    echo ""
    echo ""
    echo -e "\e[36m=========================================================================\e[39m"
    echo -e "\e[36m${@}\e[39m"
    echo ""
}

run() {
    echo -e "\e[91m+ ${@}\e[39m"
    eval ${@}
}

msg "Demonstrate that distbox is not installed"
run "distbox status"

msg "Install Distbox from PW stable branch"
run "wget -O distbox http://pw-jenkins.pw.solidfire.net:8080/view/Ember/job/distbox/job/stable/lastSuccessfulBuild/artifact/build/distbox"
run "python3 ~/distbox install"
run "distbox status"

msg "Update Distbox's upgrade URL to the feature branch containing URL update logic"
run "distbox get_conf distbox.update_stream_url"
run "distbox set_conf distbox.update_stream_url http://pw-jenkins.pw.solidfire.net:8080/view/Ember/job/distbox/job/distbox_in_rtp/lastSuccessfulBuild/artifact/build/distbox"
run "distbox get_conf distbox.update_stream_url"

msg "Run maintenance to pull in the new version of disbox"
run "distbox maintenance"
run "distbox status"
run "distbox get_conf distbox.update_stream_url"
