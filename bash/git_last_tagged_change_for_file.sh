#!/usr/bin/env bash

# This script checks the changes in a specific file between the tags in a git repository.
# It will output the latest tag where the file was changed.

readonly PROGNAME=$(basename ${0})

# Check if the file path is provided as an argument
if [ -z "${1}" ]; then
    echo "Usage: ${PROGNAME} <file_path>"
    exit 1
fi

file_path=${1}

# Get the list of tags sorted by creation date (most recent first)
tags=($(git tag --sort=-creatordate))

# Initialize the latest tag variable
latest_tag=""

# Iterate through the tags and check for changes
for ((i=0; i<${#tags[@]}-1; i++)); do
    tag1=${tags[$i]}
    tag2=${tags[$i+1]}
    
    # Check the diff between the two tags for the specific file
    if git diff --quiet $tag1 $tag2 -- $file_path; then
        continue
    else
        echo "File changed between $tag1 and $tag2"
        latest_tag=$tag1
        break
    fi
done

# Output the result
if [ -z "$latest_tag" ]; then
    echo "No changes found in the file between any tags."
else
    echo "Use tag: $latest_tag"
fi
