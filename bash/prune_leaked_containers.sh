#!/usr/bin/env bash
# Kyle Smith - hiko.san@gmail.com - 1/19/18

set -e

DEBUG=true

# Modify internal delimiter from ' ' to \n, so for-loop iterates over entire line
IFS=$'\n'  

# Get the minimal span of needed container info from docker, remove the column specification row, and the distbox service container from final list.
IMAGES=$(docker ps --format 'table {{.ID}} {{.RunningFor}} {{.Names}}' | grep -v "CONTAINER ID" | grep -v "distbox-sshd") || true  # 'Or true' overcomes grep returning rc=1 in the case that distbox is the only running container

if [[ ${#IMAGES} -eq 0 ]]; then
    echo ""
    echo "=======  $(date) - No docker containers running"
    echo ""
    exit 0
fi


# Debug logging, since this is going straight into 'production' with only manual testing...
echo ""
echo "=======  $(date)  ===================================="
echo "DEBUG: ${DEBUG}"
echo "IMAGES:"
echo "${IMAGES}"
echo ""

for image in ${IMAGES}
do
    CID=$(echo ${image} | cut -d ' ' -f1)
    TVAL=$(echo ${image} | cut -d ' ' -f2)
    TUNIT=$(echo ${image} | cut -d ' ' -f3)
    echo "${CID} has been up for ${TVAL} ${TUNIT}"
    if [[ ${TUNIT} == "hours" && ${TVAL} -ge 5 ]] || [[ ${TUNIT} == "days" && ${TVAL} -ge 1 ]]; then
        if ! $DEBUG; then
            echo "  ${CID} - invoking docker-kill!"
            docker kill ${CID}
        else
            echo "  ${CID} - Skipped kill action (debug mode enabled)"
        fi
    fi
    echo ""
done
echo "======================================================================="

# Probably unnecessary, since this is shell-specific, but in the interest of being overly pedantic...
unset IFS
