#!/usr/bin/env bash

set -e

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

usage() {
    cat <<- EOF
Usage: ./${PROGNAME} [OPTIONS]

OPTIONS:
  -h, --help     Show this message and exit
  -d, --deploy   Deploy flag
  -D, --compose  Use Docker Compose
  -T, --teardown Teardown
EOF
}

parse_args(){
    options=`getopt -o hdDT --long help,deploy,compose,teardown -n "${PROGNAME}" -- "${@}"`
    eval set -- ${options}
    while true
    do
        case $1 in
            -h|--help)
                usage ; exit 0 ;;
            -d|--deploy)
                DEPLOY=true ; shift ;;
            -D|--compose)
                COMPOSE=true ; shift ;;
            -T|--teardown)
                TEARDOWN=true ; shift ;;
            --)
              shift ; break ;;
        esac
    done
}

do_pull(){
    COMPOSE_BINARY=./bin/docker-compose
    if [[ ! -e ${COMPOSE_BINARY} ]]; then
        if ${DEPLOY} && ${COMPOSE} || ${TEARDOWN}; then
            echo "would pull"
        fi
    fi
}

main(){
    local DEPLOY=false
    local COMPOSE=false
    local TEARDOWN=false

    parse_args ${ARGS}

    do_pull
}

main
