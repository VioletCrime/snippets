#!/bin/bash
set -e
#set -x
CHECK_INTERVAL=15
DATEFUNDED=1449900000

get_curr_dollars(){
    CURR_DOLLARS=`curl -s https://www.kickstarter.com/projects/mst3k/bringbackmst3k/ | grep -i 'Project.pledged' | cut -d '$' -f 2 | cut -d '<' -f 1`
}

main(){
    get_curr_dollars
    INIT_DOLLARS=$CURR_DOLLARS
    INIT_DOLLARS_CLEAN=$(echo $INIT_DOLLARS | sed 's/,//g')
    CURR_DOLLARS_CLEAN=$INIT_DOLLARS_CLEAN
    INIT_DATE=$(date +%s)
    TIME_UNTIL_FUNDED=$(( $DATEFUNDED - $INIT_DATE ))
    INIT_MIN_UNTIL_FUNDED=$(( $TIME_UNTIL_FUNDED / 60 ))
    while [[ $TIME_UNTIL_FUNDED -gt 0 ]]; do
        sleep $CHECK_INTERVAL
        LAST_DOLLARS=$CURR_DOLLARS_CLEAN
        get_curr_dollars
        CURR_DOLLARS_CLEAN=$(echo $CURR_DOLLARS | sed 's/,//g')
        DATE_NOW=$(date +%s)
        TIME_UNTIL_FUNDED=$(( $DATEFUNDED - $DATE_NOW ))
        HOURS_UNTIL_FUNDED=$(( $TIME_UNTIL_FUNDED / 3600 ))
        MINS_UNTIL_FUNDED=$(( $(( $TIME_UNTIL_FUNDED - $(( HOURS_UNTIL_FUNDED * 3600 )) )) / 60 ))
        echo "CURRENT FUNDING: \$$CURR_DOLLARS"
        echo "TIME LEFT: $HOURS_UNTIL_FUNDED hours, $MINS_UNTIL_FUNDED minutes."
        DOLL_DIFF=$(( $CURR_DOLLARS_CLEAN - $LAST_DOLLARS ))
        TIME_DELTAS_REMAINING=$(( $TIME_UNTIL_FUNDED / $CHECK_INTERVAL ))
        echo "DOLL_DIFF: \$$DOLL_DIFF"
        echo "TIME_DELTAS_REMAINING: $TIME_DELTAS_REMAINING"
        echo "We've made \$$DOLL_DIFF in the last $CHECK_INTERVAL seconds."
        echo "At this rate, the project will finish with \$$(( $CURR_DOLLARS_CLEAN + $(( $DOLL_DIFF * $TIME_DELTAS_REMAINING )) )) in funding."
        echo ""
        echo "==========================================="
        echo ""

        #INIT_DOLL_DIFF=$(( CURR_DOLLARS_CLEAN - INIT_DOLLARS_CLEAN))
        #MIN_SINCE_START=$(( $(( $DATE_NOW - $INIT_DATE )) / 60 ))
        #if [[ $MIN_SINCE_START -le 0 ]]; then
        #    DPM=$INIT_DOLL_DIFF
        #else
        #    DPM=$(( $INIT_DOLL_DIFF / $MIN_SINCE_START ))
        #fi
        #TOTAL_PROJECTED_AGAINST_DPM=$(( $INIT_DOLLARS_CLEAN + $(( $DPM * INIT_MIN_UNTIL_FUNDED )) ))
        #echo "Since this script has began, we have made \$$INIT_DOLL_DIFF; projecting again from script start, we can expect the project will end with \$$TOTAL_PROJECTED_AGAINST_DPM in funding."
        #echo ""
        #echo "====================================================================================================================================================================================="
        #echo ""
    done
}

main
