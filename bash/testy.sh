#!/bin/bash

for name in $(ls | grep '^zest'); do
    newname=$(echo $name | sed s/^z/t/g)
    mv $name $newname
done
