#!/usr/bin/env python3

import argparse
import sys

def print_args(args):
    print("ARGS: {0}".format(args))


def parse_args(args):
    parser = argparse.ArgumentParser()
    parser.add_argument("-b", "--testbool", help="Testbool", default=False, action='store_true')
    parser.add_argument("-t", "--testhelp", help="Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp Testhelp ", default=False, action='store_true')
    parser.add_argument("-l", "--testlist", help="Testlist", default=[], action='append')
    #args = parser.parse_args(['-b', '-l', 'lol', '-l', 'relf'])
    args = parser.parse_args(args)
    return args

def two_bool(args):
    parser = argparse.ArgumentParser()
    # Turns out the default is order-dependenct, inverse of the first argument.
    # Default can be overridden (but only by the first op?)
    parser.add_argument("-d", "--do-op", dest="do_op", default=True, action="store_true",
                        help="Do the operation")
    parser.add_argument("-s", "--skip-op", dest="do_op", default=False, action="store_false",
                        help="Skip the operation")
    args = parser.parse_args(args)
    print("Do operation? '{0}'\n".format(args.do_op))
    return args


if __name__ == "__main__":
    #print_args(vars(parse_args([])))
    #parse_args(sys.argv[1:])
    print_args(vars(two_bool(sys.argv[1:])))
