#!/usr/bin/env python3

import requests

def try_slack_msg():
    client_id = ""
    client_secret = ""
    token = ""
    webhook_url = "https://hooks.slack.com/services/<redacted>"
    url = "https://slack.com/api/chat.postMessage"
    headers = {"Content-type": "application/json", "Authorization": "Bearer {0}".format(token)}
    msg = "Just anohter test"
    #resp = requests.post(url, headers=headers, data="doesitwork?")
    resp = requests.post(url, headers=headers, json={"channel": "#smitk-test-three", "text": msg})
    print("{0} - {1}".format(resp.status_code, resp.reason))
    print(dir(resp))


if __name__ == "__main__":
    try_slack_msg()
