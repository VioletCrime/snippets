#!/usr/bin/python
# http://www.shiftedup.com/2015/05/07/five-programming-problems-every-software-engineer-should-be-able-to-solve-in-less-than-1-hour
#
# Write a program that outputs all possibilities to put + or - or nothing between
# the numbers 1, 2, ..., 9 (in this order) such that the result is always 100. For
# example: 1 + 2 + 34 - 5 + 67 - 8 + 9 = 100.

from copy import copy


def combine(numA, numB):
    return numA * 10 + numB


def iterate_ops(ops):
    modify_index = 7
    continue_op = True
    while continue_op:
        continue_op = False
        ops[modify_index] += 1
        if ops[modify_index] > 2:
            continue_op = True
            ops[modify_index] = 0
            for index in range(modify_index+1, len(ops)):
                ops[index] = 0
            modify_index -= 1
            if modify_index == -1:
                return ops
    return ops


def get_opslist():
    ops = [0, 0, 0, 0, 0, 0, 0, -1]
    while ops != [2, 2, 2, 2, 2, 2, 2, 2]:
        ops = iterate_ops(ops)
        yield ops


def run(numbers=[1, 2, 3, 4, 5, 6, 7, 8, 9]):
    canonical_numbers = numbers
    for ops in get_opslist():
        numbers = copy(canonical_numbers)
        offset = 0
        for i in range(len(ops)):
            if ops[i] == 2:
                numbers[i-offset] = combine(numbers[i-offset], numbers[i+1-offset])
                numbers.pop(i+1-offset)
                offset += 1
        total = numbers.pop(0)
        opstring = "{0} ".format(total)
        for i in range(len(ops)):
            if ops[i] == 0:
                numB = numbers.pop(0)
                opstring = "{0}+ {1} ".format(opstring, numB)
                total += numB
            elif ops[i] == 1:
                numB = numbers.pop(0)
                opstring = "{0}- {1} ".format(opstring, numB)
                total -= numB
            elif ops[i] == 2:
                continue
            else:
                raise Exception("Op value shouldn't be greater than 2, is {0}.".format(ops[i]))
        if total == 100:
            print "{0}= 100".format(opstring)


if __name__ == "__main__":
    run()
