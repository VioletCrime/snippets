#!/usr/bin/python
# http://www.shiftedup.com/2015/05/07/five-programming-problems-every-software-engineer-should-be-able-to-solve-in-less-than-1-hour
#
# Write a function that given a list of non negative integers, arranges them such
# that they form the largest possible number. For example, given [50, 2, 1, 9], the
# largest formed number is 95021.


def run(numbers=[50, 2, 1, 9]):
    strngs = []
    for number in numbers:
        strngs.append(str(number))
    strngs.sort()
    strngs.reverse()
    result = ''
    for strng in strngs:
        result = "{0}{1}".format(result, strng)
    return result


if __name__ == "__main__":
    print run()
