#!/usr/bin/python
# http://www.shiftedup.com/2015/05/07/five-programming-problems-every-software-engineer-should-be-able-to-solve-in-less-than-1-hour
#
# Write a function that combines two lists by alternatingly taking elements. For
# example: given the two lists [a, b, c] and [1, 2, 3], the function should return [a,
# 1, b, 2, c, 3].

def run(lista=[1, 2, 3], listb=['a', 'b', 'c']):
    lista.reverse()
    listb.reverse()
    result = []
    while len(lista) > 0 or len(listb) > 0:
        try:
            vala = lista.pop()
            result.append(vala)
        except IndexError:
            pass
        try:
            valb = listb.pop()
            result.append(valb)
        except IndexError:
            pass
    return result

if __name__ == "__main__":
    print run()
