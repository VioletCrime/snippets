#!/usr/bin/python
# http://www.shiftedup.com/2015/05/07/five-programming-problems-every-software-engineer-should-be-able-to-solve-in-less-than-1-hour
#
# Write three functions that compute the sum of the numbers in a given list
# using a for-loop, a while-loop, and recursion.

def run_for(lst=[8,4,10,13,6,1]):
    total = 0
    for item in lst:
        total += item
    return total

def run_while(lst=[8,4,10,13,6,1]):
    total = 0
    i = 0
    while i < len(lst):
        total += lst[i]
        i += 1
    return total

def run_recurse(lst=[8,4,10,13,6,1]):
    try:
        val = lst.pop()
    except IndexError:
        return 0
    return val + run_recurse(lst=lst)

def run():
    print "FOR:     {0}".format(run_for())
    print "WHILE:   {0}".format(run_while())
    print "RECURSE: {0}".format(run_recurse())

if __name__ == "__main__":
    run()
