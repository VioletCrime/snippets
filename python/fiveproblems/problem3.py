#!/usr/bin/python
# http://www.shiftedup.com/2015/05/07/five-programming-problems-every-software-engineer-should-be-able-to-solve-in-less-than-1-hour
#
# Write a function that computes the list of the first 100 Fibonacci numbers. By
# definition, the first two numbers in the Fibonacci sequence are 0 and 1, and
# each subsequent number is the sum of the previous two. As an example, here
# are the first 10 Fibonnaci numbers: 0, 1, 1, 2, 3, 5, 8, 13, 21, and 34.


def fibonacci(numA, numB, strng, number):
    next_num = numA + numB
    strng = "{0}, {1}".format(strng, next_num)
    number += 1
    if number >= 100:
        return strng
    return fibonacci(numB, next_num, strng, number)

def run():
    print fibonacci(1, 2, "1, 2", 2)

if __name__ == "__main__":
    run()
