#!/usr/bin/python
# https://projecteuler.net/problem=3
# Largest prime factor
#
# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?

from math import ceil, sqrt

def isPrime(number):
    for num in range(2, int(ceil(sqrt(number)))):
        if number % num == 0:
            return False
    return True

def findFactors(num):
    factors = []
    for possible_factor in range(2, int(ceil(sqrt(num)))):
        if num % possible_factor == 0:
            factors.append(possible_factor)
    return factors

def isolatePrimeFactors(factors):
    prime_factors = []
    for factor in factors:
        if isPrime(factor):
            prime_factors.append(factor)
    return prime_factors

def findLargestValue(values):
    largest = -1
    for value in values:
        if value > largest:
            largest = value
    return largest

def largestPrimeFactor(num=600851475143):
    factors = findFactors(num)
    prime_factors = isolatePrimeFactors(factors)
    return findLargestValue(prime_factors)

if __name__ == "__main__":
    print largestPrimeFactor()
