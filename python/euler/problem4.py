#!/usr/bin/python
# https://projecteuler.net/problem=4
# Largest palindrome product
#
# A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 x 99.
# Find the largest palindrome made from the product of two 3-digit numbers.

def isPalindrome(product):
    string = str(product)
    while len(string) > 0:
        if string[0] != string[-1]:
            return False
        string = string[1:-1]
    return True

def findLargestPalindrome():
    largest_palindrome = 0
    products = {}
    for i in range(100, 1000):
        for j in range(100, i+1):
            products[i*j] = "{0} x {1}".format(i, j)
    for product in products.keys():
        if isPalindrome(product):
            if product > largest_palindrome:
                largest_palindrome = product
    return "{0} = {1}".format(largest_palindrome, products[largest_palindrome])

if __name__ == "__main__":
    print findLargestPalindrome()
