#!/usr/bin/python
# https://projecteuler.net/problem=5
# Smallest multiple
#
# 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?


def isDivisibleByRange(val, num):
    for i in range(1, num+1):
        if val % i != 0:
            return False
    return True

def smallestMultiple(num=20):
    val = num
    while True:
        if not isDivisibleByRange(val, num):
            val += num
        else:
            return val

if __name__ == "__main__":
    print smallestMultiple()
