#!/usr/bin/python
# http://codegolf.stackexchange.com/questions/107323/one-more-lul-and-im-out

import sys


if __name__ == "__main__":
    num = int(sys.argv[1])
    output = "{0}{1}{2}".format('One more "' * num, "One more LUL and I'm out", '" and I\'m out' * num)
    print output
