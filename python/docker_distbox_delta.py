#!/usr/bin/env python3


from datetime import datetime, timedelta
from json import load
from subprocess import run, PIPE

if __name__ == "__main__":
    now = datetime.utcnow()
    with open('/var/distbox/imagedata') as imagedata_file:
        imagedata = load(imagedata_file)
    print("=====  IMAGEDATA  ===================================================")
    for image in imagedata:
        time_delta_string = ""
        pulled_time = datetime.utcfromtimestamp(imagedata[image])
        time_since_pulled_td = now - pulled_time
        time_since_pulled_sec = time_since_pulled_td.total_seconds()
        day_seconds = 60 * 60 * 24
        if time_since_pulled_sec / day_seconds >= 1:
            time_delta_string = "{0}{1} days, ".format(time_delta_string, int(time_since_pulled_sec / day_seconds))
            time_since_pulled_sec = time_since_pulled_sec % day_seconds
        hour_seconds = 60 * 60
        if time_since_pulled_sec / hour_seconds >= 1:
            time_delta_string = "{0}{1} hours, ".format(time_delta_string, int(time_since_pulled_sec / hour_seconds))
            time_since_pulled_sec = time_since_pulled_sec % hour_seconds
        minute_seconds = 60
        if time_since_pulled_sec / minute_seconds >= 1:
            time_delta_string = "{0}{1} minutes, ".format(time_delta_string, int(time_since_pulled_sec / minute_seconds))
            time_since_pulled_sec = time_since_pulled_sec % minute_seconds
        time_delta_string = "{0}{1} seconds".format(time_delta_string, int(time_since_pulled_sec))
        print("{0}  --  pulled {1} ago".format(image, time_delta_string))
    print("\n=====  DOCKER IMAGES  ===============================================")
    docker_images_cmd = run(['docker', 'images', '--format', '{{.Repository}}:{{.Tag}}'], stdout=PIPE)
    docker_images = [image for image in docker_images_cmd.stdout.decode().split('\n') if image != '']
    print("\n".join(docker_images))
    print("\n=====  IMAGES IN DOCKER NOT IN IMAGEDATA FILE  ======================")
    for image in docker_images:
        if image not in imagedata.keys():
            print(image)
    print("") 
