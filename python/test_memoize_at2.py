#!/usr/bin/env python3

import functools
import requests

AT2_USER = 'buildDaemon'
AT2_PASS = 'solidfire'
AT2_API_URL ='https://autotest2.solidfire.net/json-rpc/2.0'

def do_thing():
    thing = get_at2_session()
    thing_two = get_at2_session()

def memoize(func):
    func.cache = None
    @functools.wraps(func)
    def inner_memoize(*args, **kwargs):
        if func.cache:
            print("RETURNING EXISTING")
            return func.cache
        print("MAKING NEW")
        func.cache = func(*args, **kwargs)
    return inner_memoize

@memoize
def get_at2_session():
    """
    """
    session = requests.session()
    session.auth = AT2_USER, AT2_PASS
    r = session.get(AT2_API_URL, verify=False)

    if not r.status_code == 200:
        raise RuntimeError("HTTPError: \n\t{}".format(r))
    return session


if __name__ == "__main__":
    do_thing()
