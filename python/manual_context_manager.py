#!/usr/bin/env python3

class LolText(object):
    def __init__(self, msg):
        self.msg = msg

    def __enter__(self):
        return self

    def __exit__(self, *args):
        print("Exiting, old chap!")

    def printthing(self):
        print(self.msg)

if __name__ == "__main__":
    with LolText("arbitrary message") as l:
        l.printthing()
