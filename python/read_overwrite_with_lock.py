#!/usr/bin/env python3

import datetime
import fcntl
import json

def lock_example(image):
    filename = './example.out'
    # Open the existing / new file
    try:
        f = open(filename, 'r+')
        fcntl.flock(f, fcntl.LOCK_EX)
    except FileNotFoundError:
        f = open(filename, 'w+')
        fcntl.flock(f, fcntl.LOCK_EX)

    # Pull out the datestamp data if it exists
    try:
        image_timestamps = json.loads(f.read())
    except json.JSONDecodeError:
        image_timestamps = {}

    # If the file existed, we've read in the data which we want to overwrite. Set the position to 0 and clear the
    # file. We COULD just re-open with the 'w' flag, but doing so would release the file lock.
    f.seek(0)
    f.truncate()

    # Update / create the image timestamp in the data, save the data, and release the resources.
    image_timestamps[image] = datetime.datetime.now().timestamp()
    f.write(json.dumps(image_timestamps))
    fcntl.flock(f, fcntl.LOCK_UN)
    f.close()

if __name__ == "__main__":
    lock_example('testimagename')
