#!/usr/bin/python

from unittest import TestCase


def create_uri(endpoint, *args):
    # How many arguments does the endpoint string require to correctly format?
    args_required = endpoint.count('{')
    # Figure out the number of arguments we're missing
    num_args_missing = args_required - len(args)
    # Create a list out of the args tuple
    args_list = [args[x] for x in range(len(args))]
    # Append empty strings until we have the minimum number of format args
    while num_args_missing > 0:
        args_list.append('')
        num_args_missing = num_args_missing - 1
    # Run the format, casting the args back to tuple, strip trailing '/', and
    # replace resulting '//' to '/', return result of the format
    return endpoint.format(*tuple(args_list)).rstrip('/').replace('//', '/')


class TestUriCreator(TestCase):
    def test_uri_creator(self):
        endpoint = 'just/{0}/test/{1}'
        output = create_uri(endpoint, 'some', 'endpoint')
        expected = 'just/some/test/endpoint'
        self.assertEquals(expected, output)

        endpoint = 'just/{0}/test'
        output = create_uri(endpoint, 'some')
        expected = 'just/some/test'
        self.assertEquals(expected, output)

        endpoint = 'just/{0}/test/{1}'
        output = create_uri(endpoint, 'some')
        expected = 'just/some/test'
        self.assertEquals(expected, output)

        endpoint = 'just/{0}/test/{1}/thing'
        output = create_uri(endpoint, 'some')
        expected = 'just/some/test/thing'
        self.assertEquals(expected, output)

        endpoint = 'just/{0}/test/{1}/thing/{2}/stuff/'
        output = create_uri(endpoint, 'some')
        expected = 'just/some/test/thing/stuff'
        self.assertEquals(expected, output)

        endpoint = 'just/{0}/test/{1}/thing/{2}/stuff/{3}/test'
        output = create_uri(endpoint, 'some')
        expected = 'just/some/test/thing/stuff/test'
        self.assertEquals(expected, output)

        endpoint = 'just/{0}/test/{1}/thing/{2}/stuff/{3}/test{4}/{5}{6}//{7}////'
        output = create_uri(endpoint, 'some')
        expected = 'just/some/test/thing/stuff/test'
        self.assertEquals(expected, output)

        endpoint = 'icsps/{0}'
        output = create_uri(endpoint)
        expected = 'icsps'
        self.assertEquals(expected, output)

if __name__ == "__main__":
    print create_uri('my/{0}/creator/{1}/socks', 'uri', 'rocks')

