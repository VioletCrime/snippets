#!/usr/bin/python

import argparse
import subprocess
import sys

tempfile = '/tmp/tcpdump_output.txt'
listen_time = '120'


class VRIDOverloadDetector(object):
    def __init__(self, vrids):
        self.vrids = vrids

    def run(self):
        self.raw_output = self.create_sample()  # String
        self.vrid_output = self.grep_for_vrids(self.raw_output)
        self.check_vrids_for_overloading()

    def create_sample(self):
        print "Listening to network traffic for {0} seconds. Please be patient during this time.\n".format(listen_time)
        # Create the output file- best way to limit tcpdump's runtime
        process = subprocess.Popen(['tcpdump', '-G', listen_time, '-W', '1', '-w', tempfile], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = process.communicate()
        # Cat the tcpdump output file to stdout for capture here
        process = subprocess.Popen(['tcpdump', '-r', tempfile], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        data, err = process.communicate()
        # Delete the output file
        process = subprocess.Popen(['rm', '-f', tempfile], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = process.communicate()

        return data

    def grep_for_vrids(self, output):
        results = []
        lines_to_parse = output.split('\n')
        for line in lines_to_parse:
            if 'vrid' in line:
                results.append(line)
        return results

    def check_vrids_for_overloading(self):
       for vrid in vrids:
           entries_with_vrid = []
           ips_using_vrid = []
           for entry in self.vrid_output:
               if 'vrid {0},'.format(vrid) in entry:
                   entries_with_vrid.append(entry)
           for entry in entries_with_vrid:
               ip = entry.split(' IP ')[1].split(' > ')[0]
               ips_using_vrid.append(ip)
           ips_using_vrid = list(set(ips_using_vrid))
           if len(ips_using_vrid) > 1:
               ips_using_vrid.sort()
               print "Detected overloading of VRID {0} by the following IPs:\n{1}\n".format(vrid, "\n".join(ips_using_vrid))


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--vrid', help='Specific VRID to check', required=False)
    return vars(parser.parse_args())


def identify_vrids_to_check(args):
    # Default value- the entire range of valid vrids
    vrids_to_check = range(255)

    # Check to see if a specific vrid to test was specified.
    if args.get('vrid'):
        arg = args['vrid']
        try:
            arg = int(arg)
            if arg > 255 or arg < 0:
                raise Exception()
        except:
            print "ERROR: vrid must be an int between 0 and 255."
            sys.exit(1)
        vrids_to_check = [arg]

    # Return appropriately
    return vrids_to_check


if __name__ == "__main__":
    args = parse_args()
    vrids = identify_vrids_to_check(args)
    detector = VRIDOverloadDetector(vrids=vrids)
    detector.run()
