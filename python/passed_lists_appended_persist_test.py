#!/usr/bin/env python3

def update(a):
    a.append('b')

if __name__ == "__main__":
    a = ['a']
    update(a)
    print(a)
