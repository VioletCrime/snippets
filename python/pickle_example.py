#!/usr/bin/env python

import json
import pickle


class SomeClass(object):
    def __init__(self):
        self.a = 3
        self.b = "refl"
        self.c = {
                  'sumint': 5,
                  'sinstr': 'WAAH',
                  'smdct': {'nint': 7,
                            'nstr': 'something inoccuous'}
        }

    def __repr__(self):
        return json.dumps({'a': self.a, 'b': self.b, 'c': self.c})

if __name__ == "__main__":
    a = SomeClass()
    with open('lol.pickle', 'w') as phyle:
        pickle.dump(a, phyle)
    with open('lol.pickle') as phyle:
        b = pickle.load(phyle)
    print "B: {0}".format(b)


