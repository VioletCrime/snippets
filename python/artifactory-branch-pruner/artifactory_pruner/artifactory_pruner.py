"""
This module defines the ArtifactoryPruner class, which is responsible for
identifying and deleting old artifacts from Artifactory.
"""

import logging
from datetime import datetime, timedelta

import requests

_LOG = logging.getLogger(__name__)

CREATED_FSTR = "%Y-%m-%dT%H:%M:%S.%fZ"


class ArtifactoryPruner:
    """
    Class to identify and delete old artifacts from Artifactory.
    """

    def __init__(self, af_client, repo, path, dry_run, git_branches, open_prs):  # pylint: disable=too-many-arguments
        """
        Instantiate the ArtifactoryPruner class.

        :param ArtifactoryClient af_client: Instance of the ArtifactoryClient class
        :param str repo: Name of the Artifactory repository to prune
        :param str path: Path under the repository to prune
        :param bool dry_run: If True, don't actually delete anything
        :param list[str] git_branches: List of Git branches to retain
        :param list[str] open_prs: List of open PRs to retain
        """
        self.af_client = af_client
        self.repo = repo
        self.path = path
        self.dry_run = dry_run
        self.git_branches = process_git_branches(git_branches)
        self.open_prs = list(filter(None, open_prs))

    def prune_artifacts(self):
        """
        Top-level driving logic to identify deprecated artifacts and delete
        them from Artifactory.
        """
        _LOG.info("There are %s Git branches and %s open PRs to persist", len(self.git_branches), len(self.open_prs))
        # Make the Artifactory calls to generate the list of artifact branches to process
        top_dirs = self.af_client.get_directories_for_repo_path(self.repo, self.path)
        nested_dirs = self.af_client.get_directories_for_repo_path(self.repo, f"{self.path}/*")
        dirs = top_dirs.json()["results"]
        dirs.extend(nested_dirs.json()["results"])

        # Process the list of artifact branches to identify those to delete
        artifact_dirs = identify_leaf_dirs(dirs)
        dangling_dirs = self.identify_dangling_dirs(artifact_dirs)
        old_dirs = identify_old_dirs(dangling_dirs)

        # Delete the crufty artifact branches
        self.delete_dirs(old_dirs)

    def identify_dangling_dirs(self, dirs):
        """
        Given a list of artifact directory objects, as returned by the Artifactory API,
        iterate through them and return only those dirs which are not named for an existing
        branch or open PR.

        :param list[dict] dirs: List of artifact directories to delete from Artifactory
        :rtype: list[dict]
        """
        result = []
        for d in dirs:  # pylint: disable=invalid-name
            # Normal git-branch named path case (non-merge-queue artifacts)
            if "gh-readonly-queue" not in d["path"] and self.git_branches:
                branch_name = d["name"]
                # Case- '/' in branch name, path include branch name fragment
                if d["path"].startswith(f"{self.path}/"):
                    branch_prefix = d["path"].replace(f"{self.path}/", "")
                    branch_name = f"{branch_prefix}/{d['name']}"
                if branch_name not in self.git_branches:
                    result.append(d)
                else:
                    _LOG.info("Skipping %s as it was found in the given list of branches", branch_name)
            # PR-named path case (merge-queue artifacts)
            elif "gh-readonly-queue" in d["path"] and self.open_prs:
                pr_num = d["name"].split("-")[1]
                if pr_num not in self.open_prs:
                    result.append(d)
                else:
                    _LOG.info("Skipping %s as it was found in the given list of open PRs", pr_num)
        return result

    def delete_dirs(self, dirs):
        """
        Given a list of branch directory objects, as returned by the Artifactory API,
        iterate through them and delete them if we're not in dry-run mode

        :param list[dict] dirs: List of artifact directories to delete from Artifactory
        :rtype: list[dict]
        """
        for d in dirs:  # pylint: disable=invalid-name
            uri = f"{d['path']}/{d['name']}"
            if not self.dry_run:
                _LOG.warning("Deleting Artifact URI %s from Artifactory!", uri)
                try:
                    pass
                    #self.af_client.delete(uri)
                except requests.HTTPError:
                    _LOG.error("Failed to delete %s. Continuing.", uri)
            else:
                _LOG.info("[DRY-RUN] Would delete URI from Artifactory- %s", uri)


def process_git_branches(git_branches):
    """
    Given the list of Git branches to retain, process them to include protected
    branches and remove empty elements from the list.

    :param list[str] git_branches: List of Git branches to retain
    :rtype: list[str]
    """
    # Remove empty elements from the list of branches
    git_branches = list(filter(None, git_branches))
    # Ensure we won't be deleting Artifacts for protected branches
    git_branches.extend(["master", "release", "main", "HEAD", "."])
    # Dedupe the list of any previously-existing elements
    git_branches = list(set(git_branches))
    return git_branches


def identify_leaf_dirs(dirs):
    """
    Given a list of artifact directory objects, as returned by the Artifactory API,
    iterate through them and return only leaf dirs. This is important because Artifactory
    returns all directories in a given path, e.g. branch name 'topic/fix-bug' will return
    'topic' and 'topic/fix-bug' as separate results. Without this check, we would very
    likely delete 'topic/*'.

    :param list[dict] dirs: List of artifact directories to delete from Artifactory
    :rtype: list[dict]
    """
    result = []
    # Create a list of all paths described in 'dirs'
    paths = [f"{d['path']}/{d['name']}" for d in dirs]
    # For each entry in 'dirs', count how many times its path appears in the list of paths
    # If it appears only once, it's a leaf directory and we should keep it
    for d in dirs:  # pylint: disable=invalid-name
        d_path = f"{d['path']}/{d['name']}"
        d_list = [p for p in paths if d_path in p]
        if len(d_list) == 1:
            result.append(d)
    return result


def identify_old_dirs(dirs):
    """
    Given a list of artifact directory objects, as returned by the Artifactory API,
    iterate through them and return only those dirs which are older than 2 hours.

    :param list[dict] dirs: List of artifact directories to delete from Artifactory
    :rtype: list[dict]
    """
    result = []
    current_datetime = datetime.now()
    for d in dirs:  # pylint: disable=invalid-name
        branch_ctime = datetime.strptime(d["created"], CREATED_FSTR)
        if (current_datetime - branch_ctime) > timedelta(hours=2):
            result.append(d)
        else:
            _LOG.info("Skipping %s as it was created less than 2 hours ago", d["name"])
    return result
