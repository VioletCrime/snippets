"""
Module used to implement a basic Artifactory API
"""

import logging

import requests

from artifactory_pruner import utils

_LOG = logging.getLogger(__name__)


class ArtifactoryClient:
    """
    Class used to encapsulate Artifactory connection details and serve as a basic API
    """

    def __init__(self, base_url, token):
        """
        Instantiate the ArtifactoryClient class.

        :param str base_url: Base URL of the Artifactory instance to run against
        :param str token: Artifactory token to use to authenticate to Artifactory
        """
        self.base_url = base_url
        self.auth_header = {"Authorization": f"Bearer {token}"}

    def _request(self, method, uri, headers, data):
        """
        Make an HTTP request to Artifactory.

        :param str method: HTTP method to use for the request
        :param str uri: Artifactory URI to request
        :param dict headers: Headers to include in the request
        :param dict data: Data to include in the request
        :rtype: requests.Response
        """
        if not headers:
            headers = {}
        resp = requests.request(
            method=method, url=f"{self.base_url}/{uri}", headers={**self.auth_header, **headers}, data=data, timeout=120
        )
        utils.log_and_raise_for_status(resp)
        return resp

    def get(self, uri, headers=None, data=None):
        """
        Make an HTTP GET request to Artifactory.

        :param str uri: Artifactory URI to request
        :param dict headers: Headers to include in the request
        :param dict data: Data to include in the request
        :rtype: requests.Response
        """
        return self._request("GET", uri, headers, data)

    def put(self, uri, headers=None, data=None):
        """
        Make an HTTP PUT request to Artifactory.

        :param str uri: Artifactory URI to request
        :param dict headers: Headers to include in the request
        :param dict data: Data to include in the request
        :rtype: requests.Response
        """
        return self._request("PUT", uri, headers, data)

    def post(self, uri, headers=None, data=None):
        """
        Make an HTTP POST request to Artifactory.

        :param str uri: Artifactory URI to request
        :param dict headers: Headers to include in the request
        :param dict data: Data to include in the request
        :rtype: requests.Response
        """
        return self._request("POST", uri, headers, data)

    def delete(self, uri, headers=None, data=None):
        """
        Make an HTTP DELETE request to Artifactory.

        :param str uri: Artifactory URI to request
        :param dict headers: Headers to include in the request
        :param dict data: Data to include in the request
        :rtype: requests.Response
        """
        return self._request("DELETE", uri, headers, data)

    def get_items_in_repo_path(self, repo, path, itype):
        """
        Call Artifactory to find all items of type 'itype' in the given repo and path.

        :param str repo: Name of the Artifactory repository to search
        :param str path: Path under the repository to search
        :param str itype: Type of item to search for
        :rtype: requests.Response
        """
        path = path.rstrip("/")  # Trailing '/' breaks the search, I found...
        headers = {"Content-Type": "text/plain"}
        data = f'items.find({{"type":"{itype}","repo":{{"$eq":"{repo}"}}, "path":{{"$match":"{path}"}}}})'
        search_url = "api/search/aql"
        return self.post(search_url, headers, data)

    def get_directories_for_repo_path(self, repo, path):
        """
        Call Artifactory to find all directories in the given repo and path.

        :param str repo: Name of the Artifactory repository to search
        :param str path: Path under the repository to search
        :rtype: requests.Response
        """
        return self.get_items_in_repo_path(repo, path, "folder")

    def delete_artifact(self, uri):
        """
        Call Artifactory to delete the given artifact URI.

        :param str uri: URI of the artifact to delete
        :rtype: requests.Response
        """
        return self.delete(f"api/storage/{uri}")
