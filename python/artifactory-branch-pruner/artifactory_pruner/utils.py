"""
This script is intended to be leveraged to prune artifacts from Artifactory which have been
published under a dir structure which matches the name of a Git branch. Additionally, it
can take a list of open PR numbers, which is useful for projects which use the Github
Read-Only Merge Queue, which creates additional artifact directories under
gh-readonly-queue/main/pr-<PR_NUMBER>-<SHA>. Artifactory paths matching any of the elements
provided as a list of active branches or open PR numbers will be retained.
"""

import json
import logging

import requests

_LOG = logging.getLogger(__name__)


def log_and_raise_for_status(response):
    """Given an HTTP Response object from a requests API call, if the response represents
    a non-2XX status, log its details for debugging.

    :param requests.Response response: The response object returned by requests API calls
    :raises: Any requests exception requests.response.raise_for_status() raises.
    """
    if isinstance(response, requests.Response) and not str(response.status_code).startswith("2"):
        msg = (
            f"Non-200 response returned from {response.url}\n"
            f"{json.dumps(response.json(), sort_keys=True, indent=4)}"
        )
        _LOG.warning(msg)
    response.raise_for_status()
