"""
This module tests the artifactory_pruner.artifactory_pruner module
"""

from datetime import datetime, timedelta

from artifactory_pruner import artifactory_pruner as af_pruner
from artifactory_pruner.artifactory_client import ArtifactoryClient
from artifactory_pruner.artifactory_pruner import ArtifactoryPruner

CURRENT_TIME = datetime.now()


def generate_mock_artifact(path, name, created_delta=timedelta(hours=1)):
    """
    Creates a dict similar to what we get back from Artifactory

    :param str path: Path to the artifact in 'Artifactory' ;)
    :param str name: Name of the artifact
    :param timedelta created_delta: (Default timedelta(hours=1)) Age of the artifact
    :rtype: dict
    """
    created = CURRENT_TIME - created_delta
    return {"path": path, "name": name, "created": created.strftime(af_pruner.CREATED_FSTR)}


def generate_artifact_list(artifacts):
    """
    Given a list of tuples, generate a list of dicts similar to what we get back from Artifactory.
    Tuples should be of types (str, str, timedelta).

    :param list artifacts: List of tuples containing the path, name, and created_delta in that order
    :rtype: list
    """
    result = []
    for artifact in artifacts:
        result.append(generate_mock_artifact(path=artifact[0], name=artifact[1], created_delta=artifact[2]))
    return result


def test_process_git_branches_cleanup():
    """
    Test that process_git_branches removes values that are falsey-evaluated false.
    """
    branches = [None, "", ""]
    result = af_pruner.process_git_branches(branches)
    assert result.count(None) == 0
    assert result.count("") == 0


def test_process_git_branches_protects_common():
    """
    Test that process_git_branches ensures commonly branch names are included,
    thereby implicitly protecting them from deletion.
    """
    branches = [""]
    result = af_pruner.process_git_branches(branches)
    assert result.count("master") == 1
    assert result.count("main") == 1
    assert result.count("HEAD") == 1
    assert result.count("release") == 1
    assert result.count("") == 0


def test_process_git_branches_dedupes():
    """
    Test that process_git_branches removes duplicate values.
    """
    branches = ["master", "master", "master"]
    result = af_pruner.process_git_branches(branches)
    assert result.count("master") == 1


def test_identify_leaf_dirs():
    """
    Test that identify_leaf_dirs returns only the leaf directories from the given list.
    """
    dirs = generate_artifact_list(
        [
            ("arxml-bundles", "hotfix", timedelta(hours=4)),
            ("arxml-bundles/hotfix", "srs-issue", timedelta(hours=4)),
            ("arxml-bundles/hotfix/srs-issue", "1.0.0", timedelta(hours=4)),
            ("arxml-bundles/hotfix/srs-issue", "1.0.1", timedelta(hours=4)),
            ("arxml-bundles", "topic", timedelta(hours=4)),
            ("arxml-bundles/topic", "golang", timedelta(hours=4)),
            ("arxml-bundles/topic/golang", "bestlang", timedelta(hours=4)),
        ]
    )
    result = af_pruner.identify_leaf_dirs(dirs)
    assert len(result) == 3
    names = [x["name"] for x in result]
    assert "1.0.0" in names
    assert "1.0.1" in names
    assert "bestlang" in names


def test_identify_old_dirs():
    """
    Test that identify_old_dirs returns only the directories >= 2 hours old from the given list.
    """
    dirs = generate_artifact_list(
        [
            ("arxml-bundles/hotfix/srs-issue", "1.0.0", timedelta(days=3)),
            ("arxml-bundles/hotfix/srs-issue", "1.0.1", timedelta(days=2)),
            ("arxml-bundles/hotfix/srs-issue", "1.0.2", timedelta(hours=4)),
            ("arxml-bundles/hotfix/srs-issue", "1.0.3", timedelta(hours=2)),
            ("arxml-bundles/hotfix/srs-issue", "1.0.4", timedelta(minutes=119)),
            ("arxml-bundles/hotfix/srs-issue", "1.0.5", timedelta(hours=1)),
        ]
    )
    result = af_pruner.identify_old_dirs(dirs)
    assert len(result) == 4
    names = [x["name"] for x in result]
    assert "1.0.0" in names
    assert "1.0.1" in names
    assert "1.0.2" in names
    assert "1.0.3" in names


def test_identify_dangling_dirs():
    """
    Test that only branches and PRs not given as arguments are returned as being 'dangling' directories
    """
    active_branches = [
        "activate_bees",
        "charlie/bit/my/finger",
        "donatello/underrated",
    ]
    open_prs = ["1234", "8765"]

    af_client = ArtifactoryClient("https://artifactory.example.com", "token_abc123")
    pruner = ArtifactoryPruner(af_client, "vsna-generic-dev-local", "arxml-bundles", True, active_branches, open_prs)
    dirs = generate_artifact_list(
        [
            ("arxml-bundles", "activate_bees", timedelta(days=3)),
            ("arxml-bundles", "beefyond_soy", timedelta(days=2)),
            ("arxml-bundles/charlie/bit/my", "finger", timedelta(hours=4)),
            ("arxml-bundles/donatello", "underrated", timedelta(hours=2)),
            ("arxml-bundles/gh-readonly-queue/main", "pr-1234-abc123", timedelta(hours=2)),
            ("arxml-bundles/gh-readonly-queue/steve", "pr-5678-def456", timedelta(hours=2)),
            ("arxml-bundles/gh-readonly-queue/hoxtix/kek", "pr-8765-112233", timedelta(hours=2)),
            ("arxml-bundles/gh-readonly-queue/HEAD", "pr-4321-998877", timedelta(hours=2)),
            ("arxml-bundles/implement", "soap", timedelta(hours=2)),
            ("arxml-bundles", "monolith", timedelta(hours=2)),
            ("arxml-bundles/rewrite/in/c", "sharp", timedelta(hours=2)),
        ]
    )
    result = pruner.identify_dangling_dirs(dirs)
    assert len(result) == 6
    names = [x["name"] for x in result]
    assert "beefyond_soy" in names
    assert "pr-5678-def456" in names
    assert "pr-4321-998877" in names
    assert "soap" in names
    assert "monolith" in names
    assert "sharp" in names


def test_delete_dirs_honors_dry_run(mocker):
    """
    Test that delete_dirs honors the dry-run flag.
    """
    af_client = ArtifactoryClient("https://artifactory.example.com", "token_abc123")
    af_client.delete = mocker.MagicMock()
    dirs = generate_artifact_list(
        [
            ("arxml-bundles/hotfix/srs-issue", "1.0.0", timedelta(days=3)),
            ("arxml-bundles/hotfix/srs-issue", "1.0.1", timedelta(days=2)),
            ("arxml-bundles/hotfix/srs-issue", "1.0.2", timedelta(hours=4)),
            ("arxml-bundles/hotfix/srs-issue", "1.0.3", timedelta(hours=2)),
        ]
    )
    pruner = ArtifactoryPruner(af_client, "vsna-generic-dev-local", "arxml-bundles", True, [], [])
    pruner.delete_dirs(dirs)  # Dry-run should not call delete_dirs
    assert af_client.delete.call_count == 0
    pruner = ArtifactoryPruner(af_client, "vsna-generic-dev-local", "arxml-bundles", False, [], [])
    pruner.delete_dirs(dirs)  # Dry-run should not call delete_dirs
    assert af_client.delete.call_count == 4
