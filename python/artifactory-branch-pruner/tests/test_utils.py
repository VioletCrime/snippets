"""
This module tests the artifactory_pruner.utils module
"""

import json

import pytest
import requests

import artifactory_pruner
from artifactory_pruner import utils


@pytest.fixture
def warning_logger(mocker):
    """Pytest fixture to mock out the logger"""
    mock_logger = mocker.spy(artifactory_pruner.utils._LOG, "warning")  # pylint: disable=protected-access
    return mock_logger


def test_log_and_raise_for_status(warning_logger):  # pylint: disable=redefined-outer-name
    """Test utils.log_and_raise_for_status"""

    def create_response_obj(url, status_code, content, encoding="utf-8"):
        """Create a minimal requests.Response() object. Required to fully vet the function
        log_and_raise_for_status(), as it does typechecking so as to not inadvertantly break
        other tests which may just mock response objects.

        :param str url: URL to ascribe to the response object
        :param int status_code: HTTP status code to ascribe
        :param dict content: Dictionary to ascribe as the response payload from the server
        :param str encoding: (Default 'utf-8') Encoding to use when ascribing response payload
        :rtype: response.Request"""
        resp = requests.Response()
        resp.encoding = encoding
        resp.status_code = status_code
        resp.url = url
        resp._content = json.dumps(content).encode(encoding)  # pylint: disable=protected-access
        return resp

    url = "https://memes.com/good_guy_greg"

    good_auth_resp = create_response_obj(url, 200, {"Sleeps on your couch": "Makes breakfast"})
    utils.log_and_raise_for_status(good_auth_resp)
    assert warning_logger.call_count == 0

    good_auth_resp = create_response_obj(url, 299, {"Borrows $5": "Pays back $10"})
    utils.log_and_raise_for_status(good_auth_resp)
    assert warning_logger.call_count == 0

    bad_auth_resp = create_response_obj(url, 401, {"reason": "Actually Scumbag Steve"})
    with pytest.raises(requests.exceptions.HTTPError):
        utils.log_and_raise_for_status(bad_auth_resp)
    assert warning_logger.call_count == 1
