#!/usr/bin/env python3

"""
This script is intended to be leveraged to prune artifacts from Artifactory which have been
published under a dir structure which matches the name of a Git branch. Additionally, it
can take a list of open PR numbers, which is useful for projects which use the Github
Read-Only Merge Queue, which creates additional artifact directories under
gh-readonly-queue/main/pr-<PR_NUMBER>-<SHA>. Artifactory paths matching any of the elements
provided as a list of active branches or open PR numbers will be retained.
"""

import argparse
import logging
import os
import sys

from artifactory_pruner.artifactory_client import ArtifactoryClient
from artifactory_pruner.artifactory_pruner import ArtifactoryPruner

_LOG = logging.getLogger(__name__)


def parse_args(args):
    """
    Parse the command-line arguments.

    :param list[str] args: List of command-line arguments
    :rtype: argparse.Namespace
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-a",
        "--artifactory-base-url",
        required=True
        help="Base URL of the Artifactory instance to run against. Should include /artifactory",
    )
    parser.add_argument(
        "-r",
        "--artifactory-repo",
        required=True,
        help="Artifactory repository name containing Git branch artifacts to prune",
    )
    parser.add_argument(
        "-p",
        "--artifactory-path",
        required=True,
        help="Path under the repository containing Git branch artifacts to prune",
    )
    parser.add_argument(
        "-t",
        "--artifactory-token",
        default=os.getenv("ARTIFACTORY_TOKEN"),
        help="Artifactory token to use to authenticate to Artifactory",
    )
    parser.add_argument(
        "-b",
        "--active-branches",
        default="",
        help="List of active branches in Git which should not be pruned. \
              Recommend identifying with \
              `git ls-remote --heads origin | cut -d '/' -f 3- | tr '\n' ' '`",
    )
    parser.add_argument(
        "-o",
        "--open-pr-numbers",
        default="",
        help="List of open PR numbers in Github- recommend identifying with \
              `gh pr list --json number --jq '.[].number' | tr '\n' ' '`",
    )
    parser.add_argument(
        "-l",
        "--log-level",
        default="WARNING",
        choices=["DEBUG", "INFO", "WARNING", "ERROR"],
        help="Runtime logging level",
    )
    parser.add_argument(
        "-d",
        "--dry-run",
        action="store_true",
        help="If specified, don't delete anything, but list what would be deleted",
    )
    args = parser.parse_args(args)
    if not args.artifactory_token:
        raise ValueError(
            "Missing Artifactory token; either export to ARTIFACTORY_TOKEN or pass with --artifactory-token"
        )
    if not (args.active_branches or args.open_pr_numbers):
        raise ValueError("Neither a list of Active Branches nor Open PR Numbers was passed! Nothing to do.")
    return args


def main(parsed_args):
    """
    Instantiates the classes used to interrogate Artifactory and prune artifacts
    before kicking off the process.

    :param parsed_args: The parsed command-line arguments
    """
    numeric_log_level = getattr(logging, parsed_args.log_level.upper(), None)
    logging.basicConfig(level=numeric_log_level)
    af_client = ArtifactoryClient(base_url=parsed_args.artifactory_base_url, token=parsed_args.artifactory_token)
    af_pruner = ArtifactoryPruner(
        af_client=af_client,
        repo=parsed_args.artifactory_repo,
        path=parsed_args.artifactory_path,
        dry_run=parsed_args.dry_run,
        git_branches=parsed_args.active_branches.split(" "),
        open_prs=parsed_args.open_pr_numbers.split(" "),
    )
    af_pruner.prune_artifacts()


if __name__ == "__main__":
    main(parse_args(sys.argv[1:]))
