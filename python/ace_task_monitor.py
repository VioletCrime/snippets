#!/usr/bin/python

# Demonstrates a few cool things:
#  - (Re)Writing a single line of stdout to not fill the terminal with useless test during long-running tasks.
#  - A little float-string formatting magic

def poll_ace_task(task_id):
    timenow = time.time()
    print "Ace Task ID: {0}".format(task_id)
    percent = ace.tasks.get(task_id)['percent_complete']
    sys.stdout.write("{0:.2f} - {1}%".format(time.time() - timenow, percent))
    sys.stdout.flush()
    while percent < 100:
        time.sleep(1)
        percent = ace.tasks.get(task_id)['percent_complete']
        sys.stdout.write("\r                   \r{0:.2f} - {1}%".format(time.time() - timenow, percent))
        sys.stdout.flush()
    print "\n\nRESULT:\n{0}\n".format(ace.tasks.get(task_id))

if __name__ == "__main__":
    import sys
    print "This is not an executable module!"
    print "This module's primary use-case is as a development-assistance method, for use in the python"
    print "    shell, using CS-ACE's virtual environment, with an already-init'd Ace Client object 'ace'"
    sys.exit(1)
