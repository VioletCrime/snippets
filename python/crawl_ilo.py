#!/usr/bin/python

import json
import pprint
import requests
import sys
import time
import warnings

iLo = {'host': '10.1.68.13', 'username': 'csadmin', 'password': 'c@ch@l0t'}
headers = {'Content-Type': 'application/json', 'accepts': 'application/json'}
url = "https://{0}".format(iLo['host'])
base_uri = '/rest/v1'

# Turn off the blasted SSL warnings
warnings.filterwarnings("ignore")

# Log in and get a token to crawl with
login_url = '{0}/rest/v1/Sessions'.format(url)
login_body = {'UserName': iLo['username'], 'Password': iLo['password']}
r = requests.post(login_url, data=json.dumps(login_body), headers=headers, verify=False)
if r.status_code is not 201:
    print "Failed to log in to iLo using: {0}. Exiting.".format(iLo)
    sys.exit(1)

headers['X-Auth-Token'] = r.headers['X-Auth-Token']
auth_location = r.headers['Location']

try:
# Do crawl
    to_visit = [base_uri]
    visited = []
    while len(to_visit) > 0:
        uri = to_visit.pop()
        if 'Logs' in uri or uri in visited:
            continue
        visited.append(uri)
        crawl_url = '{0}{1}'.format(url, uri)
        t1 = time.time()
        resp = requests.get(crawl_url, headers=headers, verify=False)
        t2 = time.time()
        print "\n=========================================================="
        print "URI: {0}. Status Code: {1}".format(uri, resp.status_code)
        print "Time to fulfill request: {0} seconds".format(t2 - t1)
        print ""
        print "Response json:"
        pprint.pprint(resp.json(), indent=2)
        links = resp.json()['links']
        for link in links:
            if link in ['self', 'NextPage']:
                continue
            if type(links[link]) is list:
                for dct in links[link]:
                    to_visit.append(dct['href'])
            else:
                to_visit.append(links[link]['href'])

        #print "\nto_visit: {0}".format(to_visit)

except Exception as exc:
    pprint.pprint("links: {0}".format(links), indent=2)
    pprint.pprint("link: {0}".format(link), indent=2)
    raise exc

finally:
# Log out
    r = requests.delete(auth_location, headers=headers, verify=False)
    if r.status_code is not 200:
        print "Failed to log out of iLo! r.json: {0}".format(r.json())
        sys.exit(3)

    print "Logout successful. Exiting."
