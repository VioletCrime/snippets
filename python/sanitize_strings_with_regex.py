#!/usr/bin/python

import re
import sys

from unittest import TestCase


regexs = [{'match_pattern': '("\S*password\S*": )("\S*")',
           'values_to_ignore': ['"VALUE_SPECIFIED_IN_NO_LOG_PARAMETER"'],
           'sanitized_value': '"***"'},
          {'match_pattern': '(user_password=)(\S*)',
           'values_to_ignore': ['VALUE_HIDDEN'],
           'sanitized_value': '***'},
          {'match_pattern': '(login_token=)(\S*)',
           'values_to_ignore': [],
           'sanitized_value': '***'},
          {'match_pattern': "(replace='password = )(\S*)'",
           'values_to_ignore': [],
           'sanitized_value': "***"},
          {'match_pattern': "( --os-password )(\S*)",
           'values_to_ignore': [],
           'sanitized_value': "***"},
          {'match_pattern': "( keystone_password=)(\S*)",
           'values_to_ignore': [],
           'sanitized_value': "***"}
         ]

def sanitize(line, regexs_to_sanitize=regexs):
    for regex in regexs_to_sanitize:
        matches = re.findall(regex['match_pattern'], line)
        for match in matches:
            if match[1] not in regex.get('values_to_ignore', []):
                str_to_replace = '{0}{1}'.format(match[0], match[1])
                line = re.sub(str_to_replace,
                              '{0}{1}'.format(match[0],
                                              regex.get('sanitized_value', '***')),
                              line)
    return line


class TestSanitize(TestCase):
    def test_keystone_password(self):
        test_string = '''2016-12-12 22:20:20,829 - cs.installer.common.csansible - DEBUG - <10.254.34.197> REMOTE_MODULE monasca_alarm_definition severity=Low expression='directory.size_bytes>2684354560.0' keystone_password=RoCXQSVI9wk monasca_api_url=https://ace-mat-ccp-vip-MON-API-clm:8070/v2.0 keystone_project=admin description='Service log directory consuming more disk than its quota.' keystone_url=https://ace-mat-ccp-vip-KEY-API-clm:5000/v3 keystone_user=logging name='Service Log Directory Size'''
        result_string = '''2016-12-12 22:20:20,829 - cs.installer.common.csansible - DEBUG - <10.254.34.197> REMOTE_MODULE monasca_alarm_definition severity=Low expression='directory.size_bytes>2684354560.0' keystone_password=*** monasca_api_url=https://ace-mat-ccp-vip-MON-API-clm:8070/v2.0 keystone_project=admin description='Service log directory consuming more disk than its quota.' keystone_url=https://ace-mat-ccp-vip-KEY-API-clm:5000/v3 keystone_user=logging name='Service Log Directory Size'''
        self.assertEquals(result_string, sanitize(test_string))

    def test_os_password(self):
        test_string = '''| 8e8e600f-c0b7-11e6-96ef-0050568d84fb | NOV-CAU | start | Disable consoleauth where not needed | Ignored | 9db102a6-c0af-11e6-96ef-0050568d84fb | 2016-12-12 22:08:49 | ace-mat-ccp-cc-m3-clm | {u'cmd': u'nova --os-username nova --os-project-name service --os-password 3KFd0S6j --os-user-domain-name Default --os-project-domain-name Default --os-auth-url https://ace-mat-ccp-vip-KEY-API-clm:5000/v3 --os-endpoint-type internalURL service-disable ace-mat-ccp-cc-m3-clm nova-consoleauth --reason "singleton running elsewhere"', u'end': u'2016-12-12 22:08:48.513036', u'stdout': u'', u'changed': True, u'start': u'2016-12-12 22:08:47.912163', u'delta': u'0:00:00.600873', u'stderr': u'ERROR (ConnectionRefused): Unable to establish connection to https://192.168.0.1:8774/v2.1/', u'rc': 1, 'invocation': {'module_name': u'shell', 'module_complex_args': {}, 'module_args': u'nova --os-username nova --os-project-name service --os-password 3KFd0S6j --os-user-domain-name Default --os-project-domain-name Default --os-auth-url https://ace-mat-ccp-vip-KEY-API-clm:5000/v3 --os-endpoint-type internalURL service-disable ace-mat-ccp-cc-m3-clm nova-consoleauth --reason "singleton running elsewhere"'}, u'warnings': []} |'''
        result_string = '''| 8e8e600f-c0b7-11e6-96ef-0050568d84fb | NOV-CAU | start | Disable consoleauth where not needed | Ignored | 9db102a6-c0af-11e6-96ef-0050568d84fb | 2016-12-12 22:08:49 | ace-mat-ccp-cc-m3-clm | {u'cmd': u'nova --os-username nova --os-project-name service --os-password *** --os-user-domain-name Default --os-project-domain-name Default --os-auth-url https://ace-mat-ccp-vip-KEY-API-clm:5000/v3 --os-endpoint-type internalURL service-disable ace-mat-ccp-cc-m3-clm nova-consoleauth --reason "singleton running elsewhere"', u'end': u'2016-12-12 22:08:48.513036', u'stdout': u'', u'changed': True, u'start': u'2016-12-12 22:08:47.912163', u'delta': u'0:00:00.600873', u'stderr': u'ERROR (ConnectionRefused): Unable to establish connection to https://192.168.0.1:8774/v2.1/', u'rc': 1, 'invocation': {'module_name': u'shell', 'module_complex_args': {}, 'module_args': u'nova --os-username nova --os-project-name service --os-password *** --os-user-domain-name Default --os-project-domain-name Default --os-auth-url https://ace-mat-ccp-vip-KEY-API-clm:5000/v3 --os-endpoint-type internalURL service-disable ace-mat-ccp-cc-m3-clm nova-consoleauth --reason "singleton running elsewhere"'}, u'warnings': []} |'''
        self.assertEquals(result_string, sanitize(test_string))

    def test_user_password_and_login_token(self):
        test_string = '2016-11-07 19:10:46,676 - cs.installer.common.csansible - DEBUG - <10.254.34.203> REMOTE_MODULE keystone_v3 user_name=ceilometer user_password=2ZHJB4M71jmW6eJh user_domain_name="Default" login_token=b4e8bb06f99f44af85ec0721fd09635e endpoint="https://ace-mat-ccp-vip-KEY-API-clm:5000/v3" action="create_user"'
        result_string = '2016-11-07 19:10:46,676 - cs.installer.common.csansible - DEBUG - <10.254.34.203> REMOTE_MODULE keystone_v3 user_name=ceilometer user_password=*** user_domain_name="Default" login_token=*** endpoint="https://ace-mat-ccp-vip-KEY-API-clm:5000/v3" action="create_user"'
        self.assertEquals(result_string, sanitize(test_string))



    def test_replace_password(self):
        #regex = [{'match_pattern': "(replace='password = )(\S*)'", 'values_to_ignore': [], 'sanitized_value': "***"}]

        test_string = " replace='password = 9TIh0Krn'"
        result_string = " replace='password = ***'"
        self.assertEquals(result_string, sanitize(test_string))

        test_string = "2016-12-12 21:51:40,546 - cs.installer.common.csansible - DEBUG - <10.254.34.202> REMOTE_MODULE replace dest=/etc/mysql/debian.cnf regexp='password(.*)' replace='password = 9TIh0Krn'"
        result_string = "2016-12-12 21:51:40,546 - cs.installer.common.csansible - DEBUG - <10.254.34.202> REMOTE_MODULE replace dest=/etc/mysql/debian.cnf regexp='password(.*)' replace='password = ***'"
        self.assertEquals(result_string, sanitize(test_string))

        test_string = "2016-12-12 21:51:40,546 - replace='password = 9TIh0Krn' cs.installer.common.csansible - DEBUG - <10.254.34.202> REMOTE_MODULE replace dest=/etc/mysql/debian.cnf regexp='password(.*)' replace='password = 9TIh0Krn'"
        result_string = "2016-12-12 21:51:40,546 - replace='password = ***' cs.installer.common.csansible - DEBUG - <10.254.34.202> REMOTE_MODULE replace dest=/etc/mysql/debian.cnf regexp='password(.*)' replace='password = ***'"
        self.assertEquals(result_string, sanitize(test_string))


if __name__ == "__main__":
    print sanitize(' '.join(sys.argv[1:]))
