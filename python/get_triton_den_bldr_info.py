#!/usr/bin/python

import argparse
import collections
import json
import paramiko #non-std
import requests #non-std lib
import sys
import threading
import warnings

warnings.filterwarnings("ignore")


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--hostname", help="AT2 instance to interrogate", default="autotest2.solidfire.net")
    parser.add_argument("-u", "--user", help="Username to log into AT2 with", default="buildDaemon")
    parser.add_argument("-p", "--password", help="Password for the AT2 user", default="solidfire")
    parser.add_argument("-o", "--output-file", help="File to write results to", default="")
    return vars(parser.parse_args(sys.argv[1:]))


class Interrogator(object):
    def __init__(self, args):
        self.username = args['user']
        self.password = args['password']
        self.url = "https://{0}".format(args['hostname'])
        self.outfile = args['output_file']
        self.session = requests.session()

    def run(self):
        self.nodelist = self.interrogate_at2()
        #node = self.nodelist[16]
        #print "NODE: {0}\n".format(node.to_json())
        print "Identified {0} Triton nodes from AT2. Note that each node takes anywhere from 30 to 90 seconds :(".format(len(self.nodelist))
        i = 0
        sys.stdout.write("{0}/{1} ({2:.2f}%) nodes completed.".format(i, len(self.nodelist), float(i) / len(self.nodelist) * 100))
        sys.stdout.flush()
        for node in self.nodelist:
            self.interrogate_ssh(node)
            self.interrogate_node_api(node)
            i += 1
            sys.stdout.write("\r{0}/{1} ({2:.2f}%) nodes completed.".format(i, len(self.nodelist), float(i) / len(self.nodelist) * 100))
            sys.stdout.flush()
        print ""

        result = ""
        for node in self.nodelist:
            result += node.output_pretty()
        self.output_result(result)
        #print node.output_pretty()

        #self.output_result(json.dumps(self.raw_nodes[0], indent=4))  # DEBUG OUTPUT ONE
        #self.output_result(json.dumps(self.raw_nodes, indent=4))  # DEBUG OUTPUT ALL
        #self.output_result('\n'.join([r.to_json() for r in self.nodelist]))

    def interrogate_at2(self):
        self.session.auth = requests.auth.HTTPBasicAuth(self.username, self.password)
        r = self.session.post("{0}/json-rpc/1.0/".format(self.url), data=json.dumps({'method': 'ListNodePool'}))
        if r.status_code is not 200:
            raise Exception(r)
        nodes = []
        for node in r.json()['result']['nodes']:
            if node['model'] in ['SF301', 'SF301-NE', 'SF302', 'SF302-NE', 'SF304', 'SF304-NE']:
                nodes.append(node)
        self.raw_nodes = nodes
        triton_nodes = []
        for node in nodes:
            tn = TritonNode()
            tn.add_at2_info(node)
            triton_nodes.append(tn)
        return triton_nodes

    def interrogate_node_api(self, node):
        self.session.auth = requests.auth.HTTPBasicAuth('admin', 'admin')
        try:
            r = self.session.get("https://{0}:442/json-rpc/10.0?method=GetHardwareInfo".format(node.oneGigIP), verify=False)
        except requests.exceptions.ConnectionError as e:
            node.api_nvram_info = "FAILED TO CONNECT: {0}".format(e)
            return
        #print "SC: {0}\nJSON: {1}\n\n".format(r.status_code, r.json())
        if r.status_code is not 200:
            node.api_nvram_info = "ERROR CONNECTING: ERROR {0}".format(r.status_code)
            #raise(r)
        try:
            data = r.json()
        except Exception as e:
            node.api_nvram_info = "ERROR PARSING JSON: Repsonse content: {0}".format(r.content)
            return
        node.boardSerial = data['result']['hardwareInfo']['boardSerial']
        node.chassisSerial = data['result']['hardwareInfo']['chassisSerial']
        if data['result']['hardwareInfo'].get('nvram'):
            for k in data['result']['hardwareInfo']['nvram']:
                node.api_nvram_info[k] = data['result']['hardwareInfo']['nvram'][k]
        else:
            node.api_nvram_info = "NO CARDS REPORTED"

    def interrogate_ssh(self, node):
        try:
            nvmes = run_ssh_cmd(node, "smart-nvdimm --info --all")[1].split('\n')
        except Exception as e:
            node.smart_nvdimm_info = "FAILED TO CONNECT / EXECUTE SSH COMMAND: {0}".format(e)
            return
        nvme_indeces = [i for i, s in enumerate(nvmes) if "###############" in s]
        if len(nvme_indeces) is 0:
            node.smart_nvdimm_info = "NO CARDS REPORTED"
        else:
            nvme_info = []
            i = 0
            while i + 1 < len(nvme_indeces) :
                nvme_info.append(nvmes[nvme_indeces[i]:nvme_indeces[i+1]])
                i += 1
            nvme_info.append(nvmes[nvme_indeces[i]:])
            for nvme in nvme_info:
                nvme_id = nvme[0].split("(--nvid=")[1].split(")")[0]
                node.smart_nvdimm_info[nvme_id] = nvme#[1:]  # TODO

    def output_result(self, result):
        if self.outfile == "":
            print result
        else:
            with open(self.outfile, 'w') as phyle:
                phyle.write(result)




class TritonNode(object):
    def __init__(self):
        # Mostly this function serves as an easy reference for class content
        self.nodename = ''
        self.nodemodel = ''
        self.username = ''
        self.password = ''
        self.oneGigIP = ''
        self.boardSerial = ''
        self.chassisSerial = ''
        self.smart_nvdimm_info = {}
        self.api_nvram_info = {}

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    def get_ssh_connection_info(self):
        return {'username': self.username,
                'password': self.password,
                'hostname': self.oneGigIP}

    def add_at2_info(self, node):
        self.nodename = node['nodeName']
        self.nodemodel = node['model']
        self.username = node['username']
        self.password = node['password']
        self.oneGigIP = node['oneGigIP']

    def output_pretty(self):
        res_dict = collections.OrderedDict()
        res_dict['boardSerial'] = self.boardSerial
        res_dict['chassisSerial'] = self.chassisSerial
        res_dict['smart-nvdimm-info'] = self.smart_nvdimm_info
        res_dict['api-nvram-info'] = self.api_nvram_info
        return "NODE: {0} ({1} - {2}):\n  {3}\n\n".format(self.nodename, self.nodemodel, self.oneGigIP, json.dumps(res_dict, indent=2))


def run_ssh_cmd(node, cmd):
    ssh = paramiko.client.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.save_host_keys('/dev/null')
    ssh.connect(**node.get_ssh_connection_info())
    transport = ssh.get_transport()
    channel = transport.open_session()
    channel.settimeout(20)  # TODO 20?
    channel.exec_command(cmd)
    rc = channel.recv_exit_status()  # Blocks
    stdout = channel.makefile('rb').read()
    stderr = channel.makefile_stderr('rb').read()
    ssh.close()
    return (rc, stdout, stderr)


if __name__ == "__main__":
    Interrogator(parse_args()).run()
