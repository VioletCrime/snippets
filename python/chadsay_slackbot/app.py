#!/usr/bin/env python3

import os
from slack_bolt import App
from slack_bolt.adapter.socket_mode import SocketModeHandler
from slack_sdk.errors import SlackApiError

from chadsay.chad import slackchad

# Initializes your app with your bot token and socket mode handler
app = App(token=os.environ.get("SLACK_BOT_TOKEN"))


@app.event("app_mention")
def event_test(body, say):
    print("Get in, loser, we're doing event things")
    say("Hi there!")


@app.command("/chadsay")
def chadsay(ack, respond, command):
    ack(slackchad(command['text']))


if __name__ == "__main__":
    # Start your app
    handler = SocketModeHandler(app, os.environ.get("SLACK_APP_TOKEN"))
    handler.start()
