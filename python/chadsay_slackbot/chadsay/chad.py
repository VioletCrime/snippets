#!/usr/bin/env python3

import sys

CHAD_LINES = [
    "########################%@@@@@@%%@%%##################*#*",
    "#####################%@@@@@@@%@%%%%%%%%#############*****",
    "####################%%@%%%%%####%%%%@%%%########*********",
    "###################%**#%%####***##@@%%%@@#####***********",
    "###################**##%%%%%#***###%%@@%%*##*************",
    "##################%**%##%%%@%###***#%%@@@%#**************",
    "##################+#%#%@@@%%%##*****%%%%@%***************",
    "#################%@@%+#*@@@%%%%#****#%***#***************",
    "##################@@@%#*#%%%%##*****#%*%#****************",
    "###################%##*##%@%%%######%%*+%%***************",
    "#################++%*#%#%%%@@##%%%#%#%*******************",
    "##################+%%#@@@%%%@@#@@%%%#%#******************",
    "####################*#%###%%@@%@@%%%%%%******************",
    "###################%@+#%##%@@@%@@@%%#%%******************",
    "###################%%#%@@@@@@@@@@@%%%@#******************",
    "###################*%%%%@@@@@%%@@@@@*********************",
    "###################*#%%@@@%%%%@@%##**********************",
    "####################%%%@@@%@@@@######*****#**************",
    "#####################%@@@@%%%%##*####******####**********",
    "#########################==*%%##*###******###########****",
    "#########################=++#%##*###********#%%%%%%%#####",
    "#########################*+*+%%#####*******###%%%%%%%%%%#",
    "#######################*+++**#%%%%######*#*###%%%@%%%%%%%",
    "####################*+**#*=+*##%%%#*#%##%%%%%%%%%%%#%%%%@",
    "##################++**###+=++*%%%#%#%%%####%%#%######%@%%",
    "################+****######*++%%**#*#%%%%%%%%%%%%%#%%%%##",
    "###########**###%%#******#######****##%%%%%#####%#%%%%###",
    "######*******###%#**##*###%%%%%**##%%%%#######%%%%%%%%###",
    "###+++*****######*########%%%%%*#####%#####%##%%%%%%%%###",
    "#=++++***####**##*#########%%%***##%#########%##%%%%%%%%%",
    "==+*+***####**####*##*###%#%%%#*###%########%%%%%%%%%%%%%"
]


def printchad(msg):
    print("\n".join(chadsays(msg)))


def slackchad(msg):
    msg_list = chadsays(msg)
    msg_list.insert(0, "```")
    msg_list.append("```")
    return "\n".join(msg_list)


def chadsays(msg):
    # Automatically format the message to the left side of the chad image, much like cowsay
    if isinstance(msg, str):
        msg = msg.split(" ")
    # Sanitize the message to safeguard against es and double quotes and injection attacks
    # Segment msg into 60-char max lines
    msg_lines = []
    msg_line = ""
    for word in msg:
        if len(msg_line) + len(word) + 1 <= 60:
            msg_line += " " + word
        else:
            msg_lines.append(msg_line)
            msg_line = word
    msg_lines.append(msg_line)
    max_msg_len = max([len(line) for line in msg_lines])
    quote_bubble = [
        "#" * (max_msg_len + 8),
        "##/" + "¯" * (max_msg_len + 2) + "\\##",
    ]
    for line in msg_lines:
        quote_bubble.append(f"##| {line.ljust(max_msg_len)} |##")
    quote_bubble.extend([
        "##\\" + "_" * (max_msg_len + 2) + "/##",
        "#" * (max_msg_len + 8)
    ])
    result = []
    for i in range(min(len(quote_bubble), len(CHAD_LINES))):
        result.append(quote_bubble[i] + CHAD_LINES[i])
    if len(quote_bubble) < len(CHAD_LINES):
        for i in range(len(quote_bubble), len(CHAD_LINES)):
            result.append("#" * (max_msg_len + 8) + CHAD_LINES[i])
    elif len(quote_bubble) > len(CHAD_LINES):
        for i in range(len(CHAD_LINES), len(quote_bubble)):
            result.append(quote_bubble[i] + "#" * 57)
    return result


if __name__ == "__main__":
    if len(sys.argv[1:]) > 0:
        printchad(" ".join(sys.argv[1:]))
    else:
        # Visually test very short message
        printchad("Hello")
        print("")
        # Visually test msg is shorter than Chad
        printchad("Welcome to our Image to ASCII Art page, your go-to destination for turning images into stunning ASCII art creations. Utilize our free online ASCII art generator to easily and quickly convert your photos into text-based art. Ideal for artists, hobbyists, or anyone looking to explore a new form of digital expression. Get started now and bring your images to life in a whole new way!")
        print("")
        # Visually test msg is taller than Chad
        printchad("""Welcome to our Image to ASCII Art page, your go-to destination for turning images into stunning ASCII art creations. Utilize our free online ASCII art generator to easily and quickly convert your photos into text-based art. Ideal for artists, hobbyists, or anyone looking to explore a new form of digital expression. Get started now and bring your images to life in a whole new way! Welcome to our Image to ASCII Art page, your go-to destination for turning images into stunning ASCII art creations. Utilize our free online ASCII art generator to easily and quickly convert your photos into text-based art. Ideal for artists, hobbyists, or anyone looking to explore a new form of digital expression. Get started now and bring your images to life in a whole new way! Welcome to our Image to ASCII Art page, your go-to destination for turning images into stunning ASCII art creations. Utilize our free online ASCII art generator to easily and quickly convert your photos into text-based art. Ideal for artists, hobbyists, or anyone looking to explore a new form of digital expression. Get started now and bring your images to life in a whole new way! Welcome to our Image to ASCII Art page, your go-to destination for turning images into stunning ASCII art creations. Utilize our free online ASCII art generator to easily and quickly convert your photos into text-based art. Ideal for artists, hobbyists, or anyone looking to explore a new form of digital expression. Get started now and bring your images to life in a whole new way! Welcome to our Image to ASCII Art page, your go-to destination for turning images into stunning ASCII art creations. Utilize our free online ASCII art generator to easily and quickly convert your photos into text-based art. Ideal for artists, hobbyists, or anyone looking to explore a new form of digital expression. Get started now and bring your images to life in a whole new way! """)
