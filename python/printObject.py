#!/usr/bin/python

class objectTree(object):
    def __init__(self, obj):
        self.root = obj
        self.children = []

    def add_child(self, child):
        self.children.append(child)


def isIterable(obj):
    try:
        iterator = iter(obj)
        return True
    except TypeError:
        return False


def repr_obj(obj):
    return repr(obj)  # TODO
    #if isinstance(obj, basestring):
        #return obj


def build_tree(obj, treeNode=None, depth=0, max_depth=4):
    if treeNode is None:
        treeNode = objectTree(repr_obj(obj))
    if isinstance(obj, basestring):
        childNode = objectTree("={0}".format(obj))
        treeNode.add_child(childNode)
    elif hasattr(obj, '__dict__'):  # If it is a class
        list_of_attributes = dir(obj)
        for attribute in list_of_attributes:
            pass
    elif isinstance(obj, dict):
        for key in obj.keys():
            pass
    elif isIterable(obj):
        for i in range(len(obj)):
            pass



def print_tree(tree_root):
    pass
