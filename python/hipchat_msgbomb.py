#!/usr/bin/env python3

import requests

if __name__ == "__main__":
    params = {
        "auth_token": "",
        "format": "json",
        "room_id": "Team Chicken Wing",
        "from": "Obama",
        "message": '@ict------',
        "notify": 1,
        "message_format": 'text',
        "color": "purple",
    }
    url = "https://hipchat.ngage.netapp.com/v1/rooms/message"
    resp = requests.post(url, params=params)
