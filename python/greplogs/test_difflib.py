#!/usr/bin/python

import difflib
import random


if __name__ == "__main__":
    with open ('problem_entries.txt') as phyle:
        output = phyle.readlines()

    line_to_test = output[random.randint(0, len(output))]

    print "TESTING LINE:\n{0}\n".format(line_to_test)

    for line in output:
        s = difflib.SequenceMatcher(None, line, line_to_test)
        similarity_ratio = s.ratio()
        print "LINE (below) has a similarity ratio of {0}\n{1}\n".format(similarity_ratio, line)

