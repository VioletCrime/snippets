#!/usr/bin/python

import argparse
import os
import sys


class colors(object):
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def highlight_line(line, key, color=colors.WARNING):
    return line.replace(key, '{0}{1}{2}'.format(color, key, colors.ENDC))

class Grepper(object):
    def __init__(self, args):
        self.debug = args['debug']
        self.key = args['key']  # What is the key we're interested in?
        self.callback_log_file = args['callback_log']
        self.csdeploy_log_files = args['csdeploy_log']
        self.ignore_history = args['ignore_history']
        self.command_history_file = get_history_filename(self.key)

        self.cache_files()
        self.command_history_cleaned = []  # Used to prune bad input from user prompts

        self.yes_responses = ['y', 'yes']
        self.no_responses = ['n', 'no']

    def cache_files(self):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        with open(self.csdeploy_log_files[0]) as phyle:
            self.csdeploy_logs = phyle.readlines()
        for csdeploy_log_file in self.csdeploy_log_files[1:]:
            with open(csdeploy_log_file) as phyle:
                self.csdeploy_logs.extend(phyle.readlines())

        #with open(self.callback_log_file) as phyle:
        #    self.callback_logs = phyle.readlines()

        self.command_history_old = []
        if os.path.isfile(self.command_history_file) and not self.ignore_history:
            with open(self.command_history_file) as phyle:
                self.command_history_old = phyle.readlines()
            self.command_history_old.reverse()
        self.log_debug("---  Leaving '{0}'  -------".format(sys._getframe().f_code.co_name))

    def log_debug(self, msg):
        if self.debug:
            print "    {0}".format(msg)

    def get_cmd_from_history(self, input_validator):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        if len(self.command_history_old) == 0:
            self.log_debug("---  Leaving '{0}' (1) Returning: 'None' -------".format(sys._getframe().f_code.co_name))
            return None
        if len(self.command_history_old) == 1:
            next_cmd = self.command_history_old.pop().replace('\n', '')
            if input_validator(next_cmd):
                self.command_history_cleaned.append(next_cmd)
                self.write_cleaned_history()
                self.log_debug("---  Leaving '{0}' (2) Returning: '{1}' -------".format(sys._getframe().f_code.co_name, next_cmd))
                return next_cmd
            else:
                self.write_cleaned_history()
                self.log_debug("---  Leaving '{0}' (3) Returning: 'None' -------".format(sys._getframe().f_code.co_name))
                return None
        next_cmd = self.command_history_old.pop().replace('\n', '')
        self.log_debug("POPPED NEXT_CMD: {0}. Is valid? {1}".format(next_cmd, input_validator(next_cmd)))
        if input_validator(next_cmd):
            self.command_history_cleaned.append(next_cmd)
            self.log_debug("---  Leaving '{0}' (4) Returning: '{1}' -------".format(sys._getframe().f_code.co_name, next_cmd))
            return next_cmd
        else:
            self.log_debug("---  Leaving '{0}' (5) RECURSE CASE -------".format(sys._getframe().f_code.co_name))
            return self.get_cmd_from_history(input_validator)

    def write_cleaned_history(self):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        with open(self.command_history_file, 'w') as phyle:
            phyle.write('\n'.join(self.command_history_cleaned))
            phyle.write('\n')
        self.log_debug("---  Leaving '{0}'  -------".format(sys._getframe().f_code.co_name))

    def write_cmd_to_history(self, cmd):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        with open(self.command_history_file, 'a') as phyle:
            phyle.write("{0}\n".format(cmd))
        self.log_debug("---  Leaving '{0}'  -------".format(sys._getframe().f_code.co_name))

    def get_int_input(self, query_string):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        history_input = self.get_cmd_from_history(self.is_int_input_acceptable)
        if history_input is not None:
            print "{0}{1}".format(query_string, history_input)
            self.log_debug("---  Leaving '{0}' (1) Returning: '{1}' -------".format(sys._getframe().f_code.co_name, int(history_input)))
            return int(history_input)
        else:
            user_response = self.prompt_user_for_int(query_string)
            self.log_debug("---  Leaving '{0}' (2) Returning: '{1}' -------".format(sys._getframe().f_code.co_name, user_response))
            return user_response

    def prompt_user_for_int(self, query_string):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        response = self._get_input(query_string)
        acceptable = self.is_int_input_acceptable(response)
        while not acceptable:
            response = self._get_input(query_string)
            acceptable = self.is_int_input_acceptable(response)
        self.write_cmd_to_history(response)
        self.log_debug("---  Leaving '{0}' Returning: '{1}' -------".format(sys._getframe().f_code.co_name, int(response)))
        return int(response)

    def is_int_input_acceptable(self, _input):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        try:
            test = int(_input)
            self.log_debug("---  Leaving '{0}' (1) Returning: 'True' -------".format(sys._getframe().f_code.co_name))
            return True
        except Exception as e:
            msg = "Please enter a valid integer (gave '{0}').".format(_input)
            print highlight_line(msg, msg)
            self.log_debug("---  Leaving '{0}' (2) Returning: 'False' -------".format(sys._getframe().f_code.co_name))
            return False

    def get_yes_no_input(self, query_string):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        history_input = self.get_cmd_from_history(self.is_yes_no_acceptable)
        if history_input is not None:
            self.log_debug("---  Leaving '{0}' (1) Returning: '{1}' -------".format(sys._getframe().f_code.co_name, history_input.lower()))
            print "{0}{1}".format(query_string, history_input)
            return history_input.lower()
        else:
            user_response = self.prompt_user_for_yes_no(query_string)
            self.log_debug("---  Leaving '{0}' (2) Returning: '{1}' -------".format(sys._getframe().f_code.co_name, user_response))
            return user_response

    def prompt_user_for_yes_no(self, query_string):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        response = self._get_input(query_string)
        acceptable = self.is_yes_no_acceptable(response)
        while not acceptable:
            self.log_debug("Input {0} is marked as unacceptable".format(response))
            response = self._get_input(query_string)
            acceptable = self.is_yes_no_acceptable(response)
        self.write_cmd_to_history(response.lower())
        return response.lower()

    def is_yes_no_acceptable(self, _input):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        acceptable = _input.lower() in self.yes_responses + self.no_responses
        if not acceptable:
            msg = "Please choose from {0} (gave '{1}').".format(self.yes_responses + self.no_responses, _input)
            print highlight_line(msg, msg)
        return acceptable

    def get_string_input(self, query_string, check_against=None):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        history_input = self.get_cmd_from_history(self.is_string_acceptable)
        if history_input is not None:
            print "{0}{1}".format(query_string, history_input)
            return history_input
        else:
            return self.prompt_user_for_string(query_string, check_against)

    def is_string_acceptable(self, _input, check_against=None):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        if check_against and _input not in check_against:
            msg = "The value you specified ({0}) does not exist in the above string! Try again!"
            print highlight_line(msg.format(_input), _input)
            return False
        elif len(_input) < 3:
            msg = "Input ({0}) not long enough. Try again".format(_input)
            print highlight_line(msg, msg)
            return False
        elif not _input:
            msg = "You didn't enter ANYTHING! Try again!"
            print highlight_line(msg, msg)
            return False
        else:
            return True

    def prompt_user_for_string(self, query_string, check_against=None):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        acceptable = False
        while not acceptable:
            response = self._get_input(query_string)
            if check_against and response not in check_against:
                msg = "The value you specified ({0}) does not exist in the above string! Try again!"
                print highlight_line(msg.format(response), response)
            elif len(response) < 3:
                msg = "Input not long enough. Try again"
                print highlight_line(msg, msg)
            elif not response:
                msg = "You didn't enter ANYTHING! Try again!"
                print highlight_line(msg, msg)
            else:
                acceptable = True
        self.write_cmd_to_history(response)
        return response

    def _get_input(self, query_string):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        acceptable = False
        while not acceptable:
            try:
                response = raw_input(query_string)
                acceptable = True
            except Exception as e:
                print repr(e)
                sys.exit(1)
        self.log_debug("---  Leaving '{0}' returning: '{1}'  -------".format(sys._getframe().f_code.co_name, response))
        return response


    def run(self):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        self. output = "=====================================================================\nKEY: {0}\n\n".format(self.key)
        self.secrets = []
        self.csdeploy_secrets = []
        self.callback_secrets = []

        self.csdeploy_key_mentions = 0
        self.callback_key_mentions = 0

        self.new_csdeploy_log_keys = []
        #self.new_callback_log_keys = []

        self.grep_csdeploy_logs_for_key()
        #self.grep_callback_logs_for_key()

        self.secrets = list(set(self.secrets))

        #print "SECRETS:\n\n"
        #for secret in self.secrets:
        #    print secret
        #print "\n\nAnd now for your regularly scheduled output:"

        for secret in self.secrets:
            self.grep_logs_for_secret(secret)

        return self.output
        self.log_debug("---  Leaving '{0}'  -------".format(sys._getframe().f_code.co_name))

    def grep_csdeploy_logs_for_key(self):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        self.log_debug("len(self.csdeploy_logs): {0}".format(len(self.csdeploy_logs)))
        for line in self.csdeploy_logs:
            if self.key in line:
                yes_no_query = "\n\n{0}\nDoes the above line contain a secret?: "
                if self.get_yes_no_input(yes_no_query.format(highlight_line(line, self.key))) in self.yes_responses:
                    num = self.get_int_input("How many secrets does it contain?: ")
                    for i in range(num):
                        secret_query = "What is secret number {0}?: "
                        secret = self.get_string_input(secret_query.format(i + 1), check_against=line)[:50]
                        self.secrets.append(secret)
                        self.csdeploy_secrets.append(secret)
                self.csdeploy_key_mentions += 1
        self.output += "csdeploy.log* mentions: {0}\n".format(self.csdeploy_key_mentions)
        self.log_debug("---  Leaving '{0}'  -------".format(sys._getframe().f_code.co_name))

    def grep_callback_logs_for_key(self):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        self.log_debug("len(self.callback_logs): {0}".format(len(self.callback_logs)))
        for line in self.callback_logs:
            if self.key in line:
                yes_no_query = "\n\n{0}\nDoes the above line contain a secret?: "
                if self.get_yes_no_input(yes_no_query.format(highlight_line(line, self.key))) in self.yes_responses:
                    num = self.get_int_input("How many secrets does it contain?: ")
                    for i in range(num):
                        secret_query = "What is secret number {0}?: "
                        secret = self.get_string_input(secret_query.format(i + 1), check_against=line)[:50]
                        self.secrets.append(secret)
                        self.csdeploy_secrets.append(secret)
                self.csdeploy_key_mentions += 1
        self.output += "callback_log mentions: {0}\n\nSECRETS:\n".format(self.callback_key_mentions)
        self.log_debug("---  Leaving '{0}'  -------".format(sys._getframe().f_code.co_name))

    def grep_logs_for_secret(self, secret):
        self.log_debug("Entering '{0}'".format(sys._getframe().f_code.co_name))
        self.log_debug("secret: {0}".format(secret))
        csdeploy_secret_mentions = 0
        callback_secret_mentions = 0
        new_keys_csdeploy = []
        new_keys_callback = []

        for line in self.csdeploy_logs:
            if secret in line:
                self.log_debug("Line has secret '{0}'".format(secret))
                yes_no_query = "\n\n{0}\nDoes the above line contain a key to the secret '{1}' differing from '{2}'? "
                yes_no_query = yes_no_query.format(highlight_line(highlight_line(line, secret, colors.FAIL), self.key),
                                                   highlight_line(secret, secret, colors.FAIL),
                                                   highlight_line(self.key, self.key))
                if self.get_yes_no_input(yes_no_query) in self.yes_responses:
                    num = self.get_int_input("How many new keys does it contain?: ")
                    for i in range(num):
                        key_input = self.get_string_input("What is new key number {0}?: ".format(i + 1), check_against=line)
                        new_keys_csdeploy.append("{0}".format(key_input))
                        csdeploy_secret_mentions += 1

        #for line in self.callback_logs:
        #    if secret in line:
        #        yes_no_query = "\n\n{0}\nDoes the above line contain a key to the secret '{1}' differing from '{2}'? "
        #        yes_no_query = yes_no_query.format(highlight_line(highlight_line(line, secret, colors.FAIL), self.key),
        #                                           highlight_line(secret, secret, colors.FAIL),
        #                                           highlight_line(self.key, self.key))
        #        if self.get_yes_no_input(yes_no_query) in self.yes_responses:
        #            num = self.get_int_input("Home many new keys does it contain?: ")
        #            for i in range(num):
        #                key_input = self.get_string_input("What is new key number {0}?: ".format(i + 1), check_against=line)
        #                new_keys_callback.append("{0}".format(key_input))
        #                callback_secret_mentions += 1

        new_keys_csdeploy = list(set(new_keys_csdeploy))
        #new_keys_callback = list(set(new_keys_callback))

        self.output += "\n    {0}\n".format(secret)
        self.output += "        csupgrade.log mentions with a secret (grepping for KEY): {0}\n".format(self.csdeploy_secrets.count(secret))
        self.output += "        csupgrade.log mentions (grepping for SECRET): {0}\n".format(csdeploy_secret_mentions)
        self.output += "        diff: {0}\n".format(abs(self.csdeploy_secrets.count(secret) - csdeploy_secret_mentions))
        if len(new_keys_csdeploy) == 0:
            self.output += "            additional keys: N/A\n"
        else:
            self.output += "            additional keys:\n"
            for new_key_csdeploy in new_keys_csdeploy:
                self.output += "                {0}\n".format(new_key_csdeploy)

        #self.output += "\n        on_any.txt mentions with a secret (grepping for KEY): {0}\n".format(self.callback_secrets.count(secret))
        #self.output += "        on_any.txt mentions (grepping for SECRET): {0}\n".format(callback_secret_mentions)
        #self.output += "        diff: {0}\n".format(abs(self.callback_secrets.count(secret) - callback_secret_mentions))
        #if len(new_keys_callback) == 0:
        #    self.output += "            additional keys: N/A\n"
        #else:
        #    self.output += "            additional keys:\n"
        #    for new_key_callback in new_keys_callback:
        #        self.output += "                {0: <55}     (callback_logs)\n".format(new_key_callback)
        self.log_debug("---  Leaving '{0}'  -------".format(sys._getframe().f_code.co_name))


def remove_history_file(key):
    os.remove(get_history_filename(key))


def undo_last_cmd(key):
    file_to_clean = get_history_filename(key)
    with open(file_to_clean) as phyle:
        cmds = phyle.readlines()
    with open(file_to_clean, 'w') as phyle:
        phyle.writelines(cmds[:-1])


def get_history_filename(key):
    return ".{0}".format(key.replace('"', '\"').replace("'", "\'"))


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-k', '--key', help="The key to grep the logs for.", required=True)
    parser.add_argument('--callback-log', required=False, default='on_any.txt',
                        help="The callback log(s) to grep through.")
    parser.add_argument('--csdeploy-log', required=False, nargs='+',
                        default=['csdeploy.log', 'csdeploy.log.1'],
                        help="The csdeploy log(s) to grep through.")
    parser.add_argument('-i', '--ignore-history', required=False, default=False, action='store_true',
                        help="Ignore inputs from previous run with provided key.")
    parser.add_argument('-r', '--remove-history', required=False, default=False, action='store_true',
                        help="Remove the history for the provided key.")
    parser.add_argument('-u', '--undo-last', required=False, default=False, action='store_true',
                        help="Remove the last command from the history for the provided key.")
    parser.add_argument('-d', '--debug',
                        help="Stuff is broken? Use this option to figure out what",
                        required=False, action='store_true', default=False)
    return vars(parser.parse_args())


if __name__ == "__main__":
    args = parse_args()
    if args['remove_history']:
        remove_history_file(args['key'])
    elif args['undo_last']:
        undo_last_cmd(args['key'])
    else:
        grepper = Grepper(args)
        print grepper.run()
