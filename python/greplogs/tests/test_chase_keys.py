#!/usr/bin/python

import os
from unittest import TestCase

from something_new import Grepper, remove_history_file, undo_last_cmd, get_history_filename

class TestUriCreator(TestCase):
    def setUp(self):
        self.args = {'debug': False,
                     'key': 'testkey:',
                     'ignore_history': False,
                     'csdeploy_log': ['tests/test_csdeploy_log', 'tests/test_csdeploy_log.1'],
                     'callback_log': 'tests/test_callback_log'}
        self.grepper = Grepper(self.args)

    def test_get_history_filename(self):
        self.assertEquals(".lel", get_history_filename("lel"))
        self.assertEquals('.\"password\":', get_history_filename('"password":'))

    def test_undo_last_cmd(self):
        key = 'testkey'
        testlog = ['a', 'b', 'c']
        testfile = get_history_filename(key)
        with open(testfile, 'w') as phyle:
            phyle.write("\n".join(testlog))
        undo_last_cmd(key)
        with open(testfile) as phyle:
            contents = phyle.readlines()
        contents = [content.replace('\n', '') for content in contents]
        self.assertEqual(contents, ['a', 'b'])
        os.remove(testfile)

    def test_remove_history_file(self):
        key = 'testkey'
        testfile = get_history_filename(key)
        with open(testfile, 'w') as phyle:
            phyle.write("useless data")
        self.assertTrue(os.path.isfile(testfile))
        remove_history_file(key)
        self.assertFalse(os.path.isfile(testfile))

    def test_init(self):
        self.assertFalse(self.grepper.debug)
        self.assertEqual(self.args['key'], self.grepper.key)
