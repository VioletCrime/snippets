#!/usr/bin/python

import sys
import pprint
import difflib

class colors(object):
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def highlight_line(line, key, color=colors.WARNING):
    return line.replace(key, '{0}{1}{2}'.format(color, key, colors.ENDC))

if __name__ == "__main__":
    strings = []
    line_match_ratio = 0.65

    with open('csdeploy.log') as phyle:
        csdeploy_logs = phyle.readlines()
    with open('csdeploy.log.1') as phyle:
        csdeploy_logs.extend(phyle.readlines())

    key = sys.argv[1]
    for line in csdeploy_logs:
        if key in line:
            if len(strings) == 0:
                strings.append(line)
            else:
                ratio = 0
                for previous_match in strings:
                    s = difflib.SequenceMatcher(None, line, previous_match)
                    match_ratio = s.real_quick_ratio()
                    #match_ratio = s.ratio()
                    if match_ratio > ratio:
                        ratio = match_ratio
                        if ratio > line_match_ratio:
                            break  # No need to try to find out if other entries are more similar....
                    if ratio <= line_match_ratio:
                        strings.append(line)

    for strng in strings:
        print highlight_line("{0}\n\n=====================================\n".format(strng), key)
