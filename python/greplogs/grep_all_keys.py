#!/usr/bin/python

import commands
import sys

class colors(object):
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def highlight_line(line, key, color=colors.WARNING):
    return line.replace(key, '{0}{1}{2}'.format(color, key, colors.ENDC))

def run(filename):
    for pattern in dq_patterns:
        print "========================================================\n{0}:\n\n".format(pattern.upper())
        cmd = '''grep --color="auto" '{0}' {1}'''.format(pattern, filename)
        print highlight_line(commands.getoutput(cmd), pattern)
        raw_input("Press Enter to continue...")
    for pattern in sq_patterns:
        print "========================================================\n{0}:\n\n".format(pattern.upper())
        cmd = '''grep --color="auto" "{0}" {1}'''.format(pattern, filename)
        print highlight_line(commands.getoutput(cmd), pattern)
        raw_input("Press Enter to continue...")


dq_patterns = [
               '''"appliance-ssh-password": "''',
               '''"attis_encryption_key": "''',
               '''"barbican_admin_password": "''',
               '''"barbican_customer_master_key": "''',
               '''"barbican_kmip_password": "''',
               '''"barbican_service_password": "''',
               '''"barbican_simple_crypto_master_key": "''',
               '''"ceilometer_metering_secret": "''',
               '''"cs-ace_encryption_key": "''',
               '''"dbadmin_user_password": "''',
               '''"erlang_cookie": "''',
               '''"haproxy_stats_password": "''',
               '''"horizon_secret_key": "''',
               '''"influxdb_monasca_api_password": "''',
               '''"influxdb_monasca_persister_password": "''',
               '''"keepalive_vrrp_password": "''',
               '''"key": "ssh-rsa ''',
               '''"keystone_admin_pwd": "''',
               '''"keystone_admin_token": "''',
               '''"keystone_backup_password": "''',
               '''"keystone_ceilometer_password": "''',
               '''"keystone_cinder_password": "''',
               '''"keystone_cinderinternal_password": "''',
               '''"keystone_demo_pwd": "''',
               '''"keystone_designate_password": "''',
               '''"keystone_eon_password": "''',
               '''"keystone_freezer_password": "''',
               '''"keystone_glance_password": "''',
               '''"keystone_glance_check_password": "''',
               '''"keystone_glance_swift_password": "''',
               '''"keystone_heat_password": "''',
               '''"keystone_ironic_password": "''',
               '''"keystone_magnum_password": "''',
               '''"keystone_monasca_agent_password": "''',
               '''"keystone_monasca_password": "''',
               '''"keystone_neutron_password": "''',
               '''"keystone_nova_password": "''',
               '''"keystone_service_password": "''',
               '''"keystone_sirius_password": "''',
               '''"keystone_swift_monitor_password": "''',
               '''"keystone_swift_password": "''',
               '''"keystone_token": "''',
               '''"logging_keystone_password": "''',
               '''"logging_kibana_password": "''',
               '''"metadata_proxy_shared_secret": "''',
               '''"monitor_user_password": "''',
               '''"mysql_admin_password": "''',
               '''"mysql_barbican_password": "''',
               '''"mysql_clustercheck_pwd": "''',
               '''"mysql_designate_password": "''',
               '''"mysql_monasca_api_password": "''',
               '''"mysql_monasca_notifier_password": "''',
               '''"mysql_monasca_thresh_password": "''',
               '''"mysql_powerdns_password": "''',
               '''"mysql_root_pwd": "''',
               '''"mysql_service_pwd": "''',
               '''"mysql_sst_password": "''',
               '''"nova_monasca_password": "''',
               '''"oo-admin-password": "''',
               '''"ops_mon_mdb_password": "''',
               '''"password": "''',
               '''"result": "''',
               '''"rmq_barbican_password": "''',
               '''"rmq_ceilometer_password": "''',
               '''"rmq_designate_password": "''',
               '''"rmq_eon_password": "''',
               '''"rmq_keystone_password": "''',
               '''"rmq_monasca_monitor_password": "''',
               '''"rmq_nova_password": "''',
               '''"rmq_service_password": "''',
               '''"rmq_sirius_password": "''',
               '''"swift_hash_path_prefix": "''',
               '''"swift_hash_path_suffix": "''',
               '''"vertica_agent_cert": "''',
               '''"vertica_agent_key": "''',
               '''"vertica_agent_pem": "''',
               '''"vertica_monasca_api_password": "''',
               '''"vertica_monasca_persister_password": "''',
               '''"vertica_ssh_private_key": "''',
               '''"vcenter-password": "''',
               '''key="ssh-rsa ''',
               '''keystone_password=''',
               '''keystone_token=''',
               '''login_token=''',
               '''user_password=''',
               '''\-\-passphrase ''',
               '''"\-\-passphrase", "''',
               '''clustercheckuser ''',
              ]
sq_patterns = [
               "'u'api-mysql-password': u'",
               "u'api-vertica-password': u'",
               "u'eve-service-password': u'",
               "u'keystone-agent-password': u'",
               "u'keystone-monasca-password': u",
               "u'keystone-password': u'",
               "u'monasca-password': u'",
               "u'monasca-salt-password': u'",
               "u'notification-mysql-password': u'",
               "u'neutron-service-password': u'",
               "u'nova-service-password': u'",
               "u'ops-console-service-password': u'",
               "u'os-db-password': u'",
               "u'password': u'",
               "u'persister-vertica-password': u'",
               "u'rabbit-password': u'",
               "u'thresh-mysql-password': u'",
               "u'vertica-admin-password': u'",
               "u'vertica-monitor-password': u'",
               "replace='password = ",
           ]

if __name__ == "__main__":
    run(sys.argv[1])

