#!/usr/bin/python

import difflib
import re
import sys

line_length_cap = 15000  # Longest acceptable line length
line_match_ratio = 0.70  # How sensitive should the matching be? (0.0 - ignore ANY difference, 1.0 - log ANY difference)
#files = ['csdeploy.log', 'csdeploy.log.1']  # Filenames to process
files = ['csupgrade.log']  # Filenames to process
# Expressions to check for - each entry is passed to re.findall()
regular_expressions = ['(\S*password\S*)', '(\S*(?<!stricthost)(?<!ace-mat-ccp-vip-)key(?!stone)(?!mgr-api)\S*)', '(\S*token\S*)', '(\S*(?<!nova-console)auth(?!/locale)(?!/login)\S*)', '(\S*passphrase\S*)', '(\S*Stack1234567\S*)', '(\S*hpcloud1\S*)']

results = {}  # Global so all functions can see- too lazy to make this a class


def greplog(output):
    # Initialiaze progress monitor values
    output_len = len(output)
    i = 0
    for line in output:  # Iterate through each line we were passed
        # Progress monitor - Outputs to STDERR to bypass likely STDOUT redirects to file
        if i % 500 == 0:
            sys.stderr.write("{0:.4f}%\r".format((float(i) / output_len) * 100))
            sys.stderr.flush()  # Output progress to STDERR, bypassing normal STDOUT redirects.
        i += 1

        # Iterate through the specified expressions
        for expression in regular_expressions:
            # Find all of the matches in the line
            matches = re.findall(expression, line, re.IGNORECASE)
            # Zero matches is returned as None- check for that here
            if matches is not None:
                # Iterate through the matches in the string
                for match in matches:
                    # Try to keep from matching sensitive data
                    # Ex: 'user_password=abc123' becomes 'user_password'
                    if '=' in match:
                        match = match.split('=')[0]
                    # Keep from logging woefully huge entries, safeguard against common false positives.
                    # Room for improvement on this line
                    if len(line) < line_length_cap and not match.startswith('/home/stack/.ansible'):
                        # If our entry is currently unknown to the result dict, create the first entry
                        if results.get(match) is None:
                            results[match] = [line]  # String so we can append later
                        else:
                            # CASE: The result dictionary has our match as a key. Iterate through the key's values
                            # noting how similar the most similar previous entry was. If it's TOO similar, ignore it
                            # If it's not very similar to previously recorded lines, record this one as well
                            ratio = 0
                            for previous_match in results[match]:
                                s = difflib.SequenceMatcher(None, line, previous_match)
                                match_ratio = s.real_quick_ratio()
                                #match_ratio = s.ratio()
                                if match_ratio > ratio:
                                    ratio = match_ratio
                                    if ratio > line_match_ratio:
                                        break  # No need to try to find out if other entries are more similar....
                            if ratio < line_match_ratio:
                                results[match].append("{0} -- {1}".format(ratio, line))


def greplogs():
    for fyle in files:
        with open(fyle) as phyle:
            output = phyle.readlines()
            sys.stderr.write("Computing contents of file: {0}\n".format(fyle))
            sys.stderr.flush()  # Expect STDOUT to be redirected to a file- output status to STDERR
            greplog(output)
    print "=====  POTENTIAL PROBLEM ENTRIES:  ==================================\n"
    print '\n'.join(results)
    print "\n\n=====  EXAMPLES:  ==================================\n"
    for key in results.keys():
        print "'{0}' ({1}) :\n{2}\n".format(key, len(results[key]), '\n'.join(results[key]))


if __name__ == "__main__":
    greplogs()
