import shutil
import sys

class ProgressBar(object):
    def __init__(self, max_val, current_val=0):
        self.current_val = current_val
        self.max_val = max_val
        self.print_progress()

    def iterate_progress(self, val=1):
        self.current_val += val
        self.print_progress()

    def print_progress(self):
        columns = int(shutil.get_terminal_size((80, 20)).columns)
        ratio_completed_str = "{0:.2f}% [{1}/{2}] ".format(float(self.current_val / self.max_val * 100),
                                                           self.current_val, self.max_val)
        progress_bar_len = columns - len(ratio_completed_str) - 2
        bar_tick_value = float(self.max_val) / progress_bar_len
        num_ticks = int(self.current_val / bar_tick_value)
        progress_bar = "[{0}{1}]".format("#"*num_ticks, " "*(progress_bar_len-num_ticks))
        sys.stdout.write("\r{0}{1}".format(ratio_completed_str, progress_bar))
