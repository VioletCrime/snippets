#!/usr/bin/python
import sys, time

def reduce_coins(purse, at_value) :
    if ((purse["pennies"]+(purse["nickles"]*5))+purse["dimes"]*10)-25==24:
        purse["nickles"]-=1
        purse["dimes"]-=2
        purse["quarters"]+=1
    if (purse["pennies"]+(purse["nickles"]*5))-10==9:
        purse["nickles"]-=2
        purse["dimes"]+=1
    if purse["pennies"]-5==4:
        purse["pennies"]-=5
        purse["nickles"]+=1

def main(maximum_value) :
    purse = {"pennies":0, "nickles":0, "dimes":0, "quarters":0}
    at_value = 0
    t1 = time.time()
    while(at_value < maximum_value) :
        at_value += 1
        purse["pennies"] += 1
        reduce_coins(purse, at_value)
    t2 = time.time()
    print "Purse consists of:\n%s pennies, %s nickles, %s dimes, %s quarters.\nSolution took %sms."\
    % (purse["pennies"], purse["nickles"], purse["dimes"], purse["quarters"], t2-t1)

if __name__ == "__main__" :
    main(int(sys.argv[1]))
