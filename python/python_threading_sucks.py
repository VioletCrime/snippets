#!/usr/bin/python

import sys
import threading
import time

class Thing(threading.Thread):
    def __init__(self):
        self.thing = False
        super(Thing, self).__init__()
    def run(self):
        self.thing = True
        print "Thing sleep starting true"
        sys.stdout.flush()
        time.sleep(0.2)
        print "Thing sleep ending, back to false"
        sys.stdout.flush()
        self.thing = False

if __name__ == "__main__":
    t = Thing()
    print "thread starting"
    sys.stdout.flush()
    t.start()
    time.sleep(0.05)
    print "main sleep ending"
    sys.stdout.flush()
    print "t.thing = {0}".format(t.thing)
    sys.stdout.flush()
    t.join()
