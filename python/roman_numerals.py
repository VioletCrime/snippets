#!/usr/bin/python

from unittest import TestCase

def Roman_Numeral(val):
    ret = ''
    numerals = [[1000, 'M'], [900, 'CM'], [500, 'D'], [400, 'CD'], [100, 'C'], [50, 'L'], [40, 'XL'], [10, 'X'], [9, 'IX'], [5, 'V'], [4, 'IV'], [1, 'I']]
    for numeral in numerals:
        while val >= numeral[0]:
            ret += numeral[1]
            val -= numeral[0]
    return ret

class testRomanNumerals(TestCase):
    def test_base_case(self):
        vals_to_test = [[1, 'I'], [2, 'II'], [3, 'III'], [5, 'V'], [10, 'X'], [50, 'L']]
        for vals in vals_to_test:
            self.assertEquals(Roman_Numeral(vals[0]), vals[1])

    def test_more_cases(self):
        vals_to_test =[[6, 'VI'], [20, 'XX'], [30, 'XXX'], [7, 'VII'], [4, 'IV'], [9, 'IX'], [14, 'XIV'], [40, 'XL'], [49, 'XLIX']]
        for vals in vals_to_test:
            self.assertEquals(Roman_Numeral(vals[0]), vals[1])

    def test_even_more_cases(self):
        vals_to_test =[[100, 'C']]
        for vals in vals_to_test:
            self.assertEquals(Roman_Numeral(vals[0]), vals[1])

if __name__ == "__main__":
    print Roman_Numeral(1981)
