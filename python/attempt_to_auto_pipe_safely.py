#!/usr/bin/python

import subprocess

def run_local_popen(cmd):
    cmd_list = cmd.split('|')
    next_cmd = cmd_list[0].strip().split(' ')
    next_popen = subprocess.Popen(next_cmd, stdout=subprocess.PIPE)
    for i in range(1, len(cmd_list)):
        prev_popen = next_popen
        next_cmd = cmd_list[i].strip().split(' ')
        next_popen = subprocess.Popen(next_cmd, stdin=prev_popen.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    result = next_popen.communicate()
    if not next_popen.returncode == 0:
        print "WARNING: command '{0}' failed with exit code: {1}. STDERR: '{2}'".format(cmd, next_popen.returncode, result[1])
    return result[0]

a = run_local_popen("ls sted")
print a

b = run_local_popen("echo 'hi, my name is steve' | sed 's/steve/kyle/g' | sed 's/hi/hello/g'")
print b
