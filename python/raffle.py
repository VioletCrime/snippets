#!/usr/bin/env python3

import random
import time

DONORS = [["Needa Kidney", 11],
          ["Chris Cochrane", 3],
          ["Daniel Suhr", 6],
          ["Arthur", 25],
          ["Cory Winkelhake", 5],
          ["John Sherohman", 15],
          ["Rodeny Surlock", 10]
         ]

class Ticket(object):
    def __init__(self, serial_num, ticket_holder):
        self.serial_number = serial_num
        self.ticket_holder = ticket_holder

def run_raffle():
    tickets = []
    ticket_sn = 1
    for donor in DONORS:
        for i in range(donor[1]):
            tickets.append(Ticket(ticket_sn, donor[0]))
            ticket_sn += 1

    drawn_ticket = tickets[random.randint(0, len(tickets))]
    print("RAFFLE DRAWING WINNER IS...... >drumroll here<")
    time.sleep(1)
    print("   Ticket '{0}', held by '{1}'!".format(drawn_ticket.serial_number, drawn_ticket.ticket_holder))


if __name__ == "__main__":
    run_raffle()
