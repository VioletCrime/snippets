#!/usr/bin/python
# coding: utf-8

import random
import sys

debug = True  # Set to 'True' for debug info


# Simple class for cards used for repr(), mostly
class Card(object):
    def __init__(self, suit, number):
        self.suit = suit
        self.number = number

    def __repr__(self):
        return "{0}{1}".format(self.number, self.suit)


# Create a list of Card objects, a 'deck', if you will, and return it
# I think this is the exact order of a fresh deck; similar at worst...
def build_deck():
    deck = []
    suits = ['♠', '♥', '♦', '♣']
    numbers = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']
    for suit in suits:
        for number in numbers:
            deck.append(Card(suit, number))
    return deck


# Split the deck, creating two smaller decks from it, and return them.
# Optionally, can specify lowest and highest possible cut indecies.
def cut(deck, minIndex=16, maxIndex=46):
    index = random.randint(minIndex, maxIndex)
    deckTwo = deck[index:]
    deck = deck[0:index]
    return deck, deckTwo


# My normal style IRL- cut the deck near center, and try drop cards from the taller stack
# faster than the smaller stack, such that the smaller stack is distributed evenly among
# the larger stack
def stdShuffle(deck):
    deckOne, deckTwo = cut(deck)  # Cut the deck into subdecks
    playBack = ""  # String used to track decisions as they're made. Printed after deck is built.
    leftWins = 0  # Track how many times we drew from the larger deck
    numTransactions = 0  # Track how many random decisions were made before deck order became deterministic
    deck = []  # Resulting deck- we're going to remove the bottom (last) card from subdecks, and add to this.

    while len(deckOne) > 0 and len(deckTwo) > 0:
        numTransactions += 1
        randVal = random.uniform(0, 1)  # Generate random float between 0 and 1

        # Identify the larger and smaller decks (may switch) to identify which subdeck to draw from
        largerDeck = deckOne if len(deckOne) > len(deckTwo) else deckTwo
        smallerDeck = deckTwo if len(deckOne) > len(deckTwo) else deckOne
        playBack += "{0}".format(len(largerDeck))

        # Identify what percentage of total cards to suffle exist in the larger deck. If this 'ratio' is
        # greater than the random number generated above (increasingly likely as disparity in subdeck
        # size increases), then take the next card from the larger deck. Otherwise take from smaller deck.
        ratio = float(len(largerDeck)) / (len(deckOne) + len(deckTwo))
        if debug:
            print "len(largerDeck): {0}\nlen(smallerDeck): {1}\nrandVal: {2}\nratio: {3}\n".format(len(largerDeck), len(smallerDeck), randVal, ratio)
        if ratio > randVal:
            playBack += " << {0}\n".format(len(smallerDeck))
            deck.append(largerDeck[-1])
            largerDeck = largerDeck[:-1]
            leftWins += 1
        else:
            playBack += " >> {0}\n".format(len(smallerDeck))
            deck.append(smallerDeck[-1])
            smallerDeck = smallerDeck[:-1]

        # Semi-arbitrary variable reassignment, used to again determine which subdeck is larger on next loop.
        deckOne = largerDeck
        deckTwo = smallerDeck

    playBack += "Larger deck chosen {0} times of {1} choices made ({2}%).".format(leftWins, numTransactions, (float(leftWins) / numTransactions) * 100)

    # One of the subdecks is exhausted- be sure to dump remaining cards from subdecks to newly shuffled deck
    while len(deckOne) > 0:
        deck.append(deckOne[-1])
        deckOne = deckOne[:-1]
    while len(deckTwo) > 0:
        deck.append(deckTwo[-1])
        deckTwo = deckTwo[:-1]

    if debug:
        print "deckOne:\n{0}\ndeckTwo:\n{1}\nResult:\n{2}\n\n".format(deckOne, deckTwo, deck)
    print playBack

    # List append is analogous to building the deck top-down, IRL behavior is bottom-up. Reverse to correct.
    deck.reverse()

    return deck


def cutAndStack(deck):
    deck, deckTwo = cut(deck)
    deckTwo.extend(deck)
    print "Result Deck (len: {0}):\n{1}\n".format(len(deckTwo), deckTwo)
    return deckTwo


def command_loop():
    loop = True
    deck = build_deck()
    shuffle_logic = stdShuffle
    while loop:
        print ""  # Newline for easier human parsing
        command = raw_input("(S)huffle, (P)rint, (R)eset, (Q)uit: ")
        if command.upper() == "S":
            deck = shuffle_logic(deck)
        if command.upper() == "P":
            print deck
        if command.upper() == "R":
            deck = build_deck()
        if command.upper() == "Q":
            loop = False

if __name__ == "__main__":
    command_loop()
