def recurse(A, i, k):
  n = k-i+1
  if n == 3 :
    print "N = 3, ", A[i:k+1]
    if A[i] > A[i+1] :
      temp = A[i]
      A[i] = A[i+1]
      A[i+1] = temp
    if A[i+1] > A[i+2] :
      temp = A[i+1]
      A[i+1] = A[i+2]
      A[i+2] = temp
    if A[i] > A[i+1] :
      temp = A[i]
      A[i] = A[i+1]
      A[i+1] = temp
      
    return
  else :
    print "Else, ", A[i:k+1]
    third = n/3
    recurse(A, i, k-third)
    recurse(A, i+third, k)
    recurse(A, i, k-third)

A = [9, 7, 4, 1, 8, 2, 3, 5, 6]
recurse(A, 0, 8)
print A
