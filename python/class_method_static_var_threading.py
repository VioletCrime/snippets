#!/usr/bin/python

import threading
import time

class testCls(object):
    flag = True

    @classmethod
    def run(cls):
        cls.flag = False

    @classmethod
    def run_with_wait(cls):
        cls.flag = False
        time.sleep(0.05)
        cls.flag = True

def test_lel():
    l1 = testCls()
    assert testCls.flag
    assert l1.flag
    testCls.flag = False
    assert not testCls.flag
    assert not l1.flag
    l2 = testCls()
    assert not l2.flag
    testCls.flag = True
    assert testCls.flag
    assert l1.flag
    assert l2.flag
    l2.run()
    assert not testCls.flag
    assert not l1.flag
    assert not l2.flag
    l3 = testCls()
    assert not l3.flag
    testCls.flag = True

def test_lel_threading():
    assert testCls.flag
    t1 = threading.Thread(target=testCls.run_with_wait)
    t1.start()
    time.sleep(0.01)
    assert not testCls.flag
    t1.join()
    assert testCls.flag

def test_show_bad_usage():
    assert testCls.flag
    l1 = testCls()
    l1.flag = False
    #assert not testCls.flag  # The way I thought it worked. I was wrong; uncomment and run to see error
    assert testCls.flag


if __name__ == "__main__":
    test_lel()
    test_lel_threading()
    test_show_bad_usage()
