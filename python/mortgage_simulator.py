#!/usr/bin/env python3

import math
import sys

def main():
    print("Type 'exit' at any time to quit.")
    amount = get_input_int("Mortgage amount: $")
    interest_rate = get_input_float("Interest rate: ")
    period = get_input_int("Mortage period (years): ")
    simulator = MortgageSimulator(amount, interest_rate, period)
    simulator.run_simulation()


class MortgageSimulator(object):
    def __init__(self, mortgage_amount, interest_rate, mortgage_period):
        self.mortgage_amount = mortgage_amount
        self.interest_rate = interest_rate
        self.mortgage_period = mortgage_period

        # Calculate monthly interest and payment
        interest_coefficient = self.interest_rate / 100
        self.monthly_interest = interest_coefficient / 12
        monthly_interest_coefficient = 1 + self.monthly_interest
        payment_count = self.mortgage_period * 12
        self.monthly_payment = (self.monthly_interest / (1 - math.pow(monthly_interest_coefficient, -payment_count))) * self.mortgage_amount

    def run_simulation(self):
        print("Monthly payment: ${0:.2f}".format(self.monthly_payment))
        user_resp = get_input("Do you want to overpay any month? Yes/[No]: ")
        if user_resp.lower() in ['y', 'yes', 'true']:
            self.run_overpayment_simulation()
        else:
            self.principle = self.mortgage_amount
            self.interest_paid = 0
            while self.principle > 0:
                payment = self.monthly_payment
                interest = self.principle * self.monthly_interest
                self.interest_paid += interest
                self.principle -= (payment - interest)
            print("Interest paid: {0:.2f}".format(self.interest_paid))
            print("Total cost of mortgage: {0:.2f}".format(self.interest_paid + self.mortgage_amount))

    def run_overpayment_simulation(self):
        principle = self.mortgage_amount
        interest_paid = 0
        payment_input_msg = "How much do you want to pay this month? [${0:.2f}]: $".format(self.monthly_payment * 2)
        month = 0
        while principle > 0:
            month += 1
            print("\nMonth: {0}, Total Principle Remaining: {1:.2f}, Total Interest Paid: {2:.2f}".format(month, principle, interest_paid))
            payment = get_input_float(payment_input_msg, default_val=self.monthly_payment *2 )

            interest = principle * self.monthly_interest
            interest_paid += interest
            principle -= (payment - interest)
            print("Interest paid this month: ${0:.2f}, Principle paid this month: ${1:.2f}".format(interest, payment - interest))
        print("Paid off the loan in {0} months. Interest paid: ${1:.2f}, total cost of mortgage: ${2:.2f}".format(month, interest_paid, interest_paid + self.mortgage_amount))

def get_input(msg, chars_to_sanitize=[',']):
    input_raw = input(msg)
    if input_raw.lower().strip() == "exit":
        sys.exit(0)
    input_trimmed = input_raw.strip()
    for char in chars_to_sanitize:
        input_trimmed = input_trimmed.replace(char, "")
    return input_trimmed


def get_input_int(msg, chars_to_sanitize=[',']):
    while True:
        try:
            return int(get_input(msg, chars_to_sanitize))
        except ValueError:
            print("Error parsing your input! Please enter an integer")


def get_input_float(msg, chars_to_sanitize=[','], default_val=None):
    while True:
        try:
            input_str = get_input(msg, chars_to_sanitize)
            if input_str == '':
                return default_val
            return float(input_str)
        except ValueError:
            print("Error parsing your input! Please enter a float")


if __name__ == "__main__":
    main()
