#!/usr/bin/env python3

class Thing(object):
    def lel(self):
        print(self.kek())

    def kek(self):
        raise NotImplementedError

class Other(Thing):
    def kek(self):
        return "kek"

if __name__ == "__main__":
    o = Other()
    o.lel()
