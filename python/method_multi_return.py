#!/usr/bin/python

def ret_things():
    a = 12
    b = 'lol'
    return a, b

if __name__ == "__main__":
    a, b = ret_things()
    assert a == 12
    assert b == 'lol'
