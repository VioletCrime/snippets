    #include "Sol.h"
    #include <iostream>
#include <vector>


    using namespace std;

    int main() {
        Sol<int> foo;
	Sol<char> bar;

        foo.insert(2);
        foo.insert(5);
        foo.insert(3);

	bar.insert('k');
        bar.insert('c');
        bar.insert('a');
        bar.insert('b');

        // Should print "253"
        for (Sol<int>::iterator it = foo.begin(); it != foo.end(); ++it)
            cout << *it;
        cout << '\n';
	Sol<char>::iterator testit = bar.begin();
	testit++;
	++testit;
	testit++;
	cout << *testit;
	--testit;
	cout << *testit;
	testit--;
	cout << *testit;
	testit--;
	cout << *testit << endl;

cout << "Make it here?\n";

	bool one = testit == bar.begin();
	bool two = testit != bar.begin();
	bool empty = bar.empty();
	bar.clear();
	bool empTwo = bar.empty();

	cout << "\nOne: " << one << ", two: " << two << ".\n\n Empty? " << empty << ", empTwo? " << empTwo << ".\n\n";
	bar.insert('h');
	bar.insert('e');
	Sol<char>::iterator it = bar.insert('y');
	bar.dump();

	cout << "Max size: " << bar.max_size() << ".\n\n";

	cout << "Iterator should point to 'y': " << *it << ".\n\nNext:\n";

	Sol<char> next(bar);
	next.dump();

	Sol<char> test;
	test.insert('K');
	test.insert('e');
	test.insert('e');
	Sol<char>::iterator s1 = test.insert('g');
	test.insert('a');
	test.insert('n');
	test.insert(' ');
	test.insert('i');
	test.insert('s');
	test.insert(' ');
	test.insert('a');
	test.insert('n');
	test.insert(' ');
	test.insert('u');
	test.insert('n');
	test.insert('s');
	test.insert('a');
	test.insert('v');
	test.insert('o');
	test.insert('r');
	test.insert('y');
	test.insert(' ');
	test.insert('b');
	test.insert('e');
	test.insert('d');
	Sol<char>::iterator s2 = test.insert('w');
	test.insert('e');
	test.insert('t');
	test.insert('t');
	test.insert('e');
	test.insert('r');
	test.insert('!');
cout << "test: \n";
	test.dump();
	unsigned int ui = test.count('e');
cout << "Number of 'e's in test: " << ui << ".\n";

cout << "\nCool:\n";

	Sol<char> cool(s1, s2);
	cool.dump();

	vector<int> v;

	for (int i = 1; i < 12; i++){
		v.push_back(i);
	}

	vector<int>::iterator h = v.begin();
	vector<int>::iterator i = v.begin();

	h++;
	h++;

	i++;
	i++;
	i++;
	i++;
	i++;
	i++;

	Sol<int> another(h, i);
	another.dump();

string data = "abcdefGZXY";
Sol<char, 4> carp(data.begin(), data.end());
carp.dump();


        return 0;
    }
