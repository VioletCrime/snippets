#ifndef SOL_H_INCLUDED
#define SOL_H_INCLUDED

#include <iostream>
#include <limits.h> //Used to identify maximum list size
#include <cassert>

template <class T, int find_val = 1, class C = std::less<T> >
class Sol{

	public:
		typedef unsigned int size_type; //typedef'ing class variable types
		typedef T key_type; 
		typedef T value_type; 

	private:

		struct node{ //Used as 'links' in the chain that is the doubly linked list
			T data;
			node *prev;
			node *next;
		};

		node *head, *tail; //The head and tail of the dll
		int find_promote_value;	//How much should find() shift a value?
		C fnctor;

	public:
		class iterator{
			private:
				node *no;
				friend class Sol;
				iterator(node *p) : no(p){
				}
			public:
				iterator() : no(0){
				}

				iterator(const iterator &it) : no(it.no){
				}
				
				~iterator(){
				}

				iterator operator++(){ //Preincrement
					no = no->next;
					return *this;
				}

				iterator operator++(int){ //Postincrement
					iterator temp = *this;
					++*this;
					return temp;
				}

				iterator operator--(){  //Predecrement
					no = no->prev;
					return *this;
				}

				iterator operator--(int){  //Postdecriment
					iterator temp = *this;
					--*this;
					return temp;
				}
		
				T operator*(){
					return no->data;
				}

				T *operator->(){
					return &no->data;
				}

				bool operator==(const iterator &rhs){
					return no == rhs.no;
				}

				bool operator!=(const iterator &rhs){
					return !(*this == rhs);
				}		

				void operator=(const iterator &rhs){
					no = rhs.no;
				}
		};

		Sol() : head(0), tail(0), find_promote_value(find_val){
		}

		Sol(const Sol &rhs) : head(0), tail(0), find_promote_value(rhs.find_promote_value), fnctor(rhs.fnctor){
			node *n = rhs.head;
			while (n != NULL){
				T dat = n->data;
				insert(dat);
				n = n->next;
			}
		}

		template<class inputIterator>
		Sol(inputIterator ita, inputIterator itb){
            if(ita != itb){
            	node *n=new node;
            	n->data = *ita;
            	head=n;
            	tail=n;
            	++ita;
			}
			while (ita != itb){
				node *n = new node;
				n->next = NULL;
				n->prev = tail;
				n->data = *ita;
				tail->next = n;
				tail = n;
				++ita;
			}
std::cout << "\n";
		}

		~Sol(){
			clear();
		}

		Sol<T> operator=(const Sol<T> &rhs){
			clear();
			find_promote_value = rhs.find_promote_value;
			fnctor = rhs.fnctor;
			node *n = rhs.head;
			while (n != NULL){
				T dat = n->data;
				insert(dat);
				n = n->next;
			}
			
			return *this;
		}

		iterator insert(T input){
			node *n = new node;
			n->next = NULL;
			n->prev = tail;
			n->data = input;

			if(!tail){
				head = n;
				tail = n;
			}
			else{
				tail->next = n;
				tail = n;
			}

			return iterator(tail);
		}

		

		void clear(){  
			while (head!=NULL){
				node *n = head;
                head=head->next;
				delete n;
			}
			head = NULL;
			tail = NULL;
		}

		void dump(){
			node *n = head;
			while (n){
				std::cout << n->data << " ";
				n = n->next;
			}
			std::cout << std::endl;
		}

		size_type max_size() const{
			return UINT_MAX;
		}

		size_type size() const{
			size_type result = 0;
			for (node *n = head; n; n= n->next){
				--result;
			}
			return result;
		}

		size_type count(const key_type &k) const{
			size_type result = 0;
			node *n = head;
			while (n){
				if(n->data == k){
					result++;
				}
				n = n->next;
			}
			return result;
		}

		bool empty(){
			return !(size() > 0);
		}

		iterator begin(){
			return iterator(head);
		}

		iterator begin() const{
			const iterator result(NULL);
			return result;
		}

		iterator end(){
			return iterator(NULL);
		}

		iterator end() const{
			const iterator result(NULL);
			return result;
		}


//=================SHIFT OPERATORS ========================/
		Sol<T> operator++(){ //Preincrement
			tail->next = head;
			head->prev = tail;
			head = head->prev;
			tail = tail->prev;
			head->prev = NULL;
			tail->next = NULL;
            
			return *this;
		}

		Sol<T> operator++(int){ //Postincrement
			return ++*this;
		}

		Sol<T> operator+(int j){ //Sol + N
			Sol<T> result(*this);
			for (int i = 0; i < j; i++){
				++result;
			}
			return result;
		}

		Sol<T> operator +=(int j){ // Sol +=n
			*this = *this + j;
			return *this + j;
		}

		Sol<T> operator--(){ //Predecriment
			tail->next = head;
			head->prev = tail;
			head = head->next;
			tail = tail->next;
			head->prev = NULL;
			tail->next = NULL;

			return *this;
		}

		Sol<T> operator--(int){ //Postdecriment
			return --*this;
		}

		Sol<T> operator-(int j){ //Sol - N
			Sol<T> result(*this);
			for (int i = 0; i < j; i++){
				--result;
			}
			return result;
		}

		Sol<T> operator-=(int j){ //Sol -=n
			*this = *this - j;
			return *this - j;
		}
};

template<class T>
Sol<T> operator+(int i, Sol<T> S){ // N + Sol
	return S + i;
}

template<class T>
Sol<T> operator-(int i, Sol<T> S){ //N - Sol
	Sol<T> result = S - i;
	return result;
}

#endif
