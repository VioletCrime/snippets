/*
* sudoku.cc by Kyle Smith
* Attempting to create a sudoku solver, hopefully to be used later
* on android and iOS devices with an OCR text-converter for camera-to-answer use.
* v0.1: basic algorithms, solves all tested normal difficulty, ~2/3 hard difficulty.
*/

#include "sudoku.h"

using namespace std;

/* =========================================CTORS / C-CTORS / DTORS =====================================*/

//Default constructor
sudoku::sudoku(){
}

//Presently unused vector<vector<int> > constructor
sudoku::sudoku(matrix rhs){
	start = rhs;
	solve();
}

//Standard string constructor
sudoku::sudoku(const string fileName){
	ifstream input;
	input.open(fileName.c_str());
	if(input){ //Check to make sure we have opened the file (WEAK ERROR CHECKING!!!)
		for (int i = 0; i < 9; i++){ //Create a 9x9 nested vector of ints, outer vector represents rows
			vector<int> v;
			for (int j = 0; j < 9; j++){ //Read in the 9 ints in a sudoku row
				int q;
				input >> q;
				v.push_back(q);
			}
			start.push_back(v); //Add the row (vector<int>) to the main matrix
		}	
		final = start; //Make a copy to work on, preserving the original for future use.
		input.close();
		solve(); //Last order of business: solve the damn thing.
	}
	else{ //Error 404: Sudoku Not Found!
		cout << "Could not open " << fileName << "." << endl << endl;
	} 
}

//Copy constructor
sudoku::sudoku(const sudoku &rhs) : start(rhs.start), final(rhs.final){
}

//Destructor
sudoku::~sudoku(){
}

/* ========================================SOLVING ALGORITHMS ==========================================*/

//Solving algorithm; progressively tries harder algorithms to solve the puzzle
void sudoku::solve(){
	count_zeros(); //How many empty spaces are in the puzzle?
	build_possible_move_table(); //'Count' algorithm: build a table outlining what numbers can go in each of the empty spaces

	while(zero_count > 0){ //While the puzzle is not solved (breakable if puzzle is sufficiently difficult)
		bool progress_made = false; //Tracks if progress is made in this loop

			while (one_move_possible > 0){ //Are there moves to be written?
				vector<int> v;
				v = single_moves.back(); //Get the move to be made (vector<int> [0 = row] [1 = col] [2 = val])
				single_moves.pop_back(); //Remove the move from the stack of moves to be made
				one_move_possible--; //Update the number of moves remaining to be made
				zero_count--; //One less space to solve
				final[v[0]][v[1]] = v[2]; //Write the value to the 'final' matrix

				for (int z = 0; z < 10; z++){ //Zero out the space's list of possible moves (safety; should not be necessary)
					possible_moves[v[0]][v[1]][z] = 0;
				}
				update_possible_move_table(v);	//Update the possible move table to reflect this write
			}

		if(zero_count > 0){ //If there are still open places on the board, run the 'basic' algorithm
			progress_made = basic_single_move_search(); //Note if progress was made
		}

		if (progress_made == false){
			break; //If progress wasn't made by now, no more progress is possible. Break early.
		}
	}

	if(zero_count > 0){ //We didn't solve the puzzle, print the progress that was made.
		cout << "\nBoard was not completed (" << zero_count <<" empty entries remain). Here is the partially completed puzzle:\n";
		printPartial();
		printStart();
	}
	else{ //We solved the puzzle! Inform the user of the good news, along with the finished table.
		cout << "\nBoard was completed. Here is the solution:\n";
		printFinal();
	}
}

//Initial check: how many spaces are 'empty' ( = 0)?
void sudoku::count_zeros(){ 
	zero_count = 0;
	for (int i = 0; i < 9; i++){
		for (int j = 0; j < 9; j++){
			if (final[i][j] == 0){
				zero_count++;
			}
		}
	}
}

//What is the maximum number of possible moves a single square has?
void sudoku::find_max_moves(){
	for (int i = 0; i < 9; i++){
		for (int j = 0; j < 9; j++){
			if (final[i][j] == 0){
				if (possible_moves[i][j][0] > max_moves_possible){
					max_moves_possible = possible_moves[i][j][0];
				}
			}
		}
	}
}

//Create the possible_moves object, which tracks what numbers can possibly go in each empty space of the table.
void sudoku::build_possible_move_table(){

 	//Set initial values for member variables
	one_move_possible = 0; //Keeps track of how many definitive moves can be made
	max_moves_possible = 0; //Keeps track of the maximum number of possible moves for output formatting

	//Build the possible_moves 3d vector to proper dimensions (9-9-10)
	vector<int> v;
	for (int i = 0; i < 10; i++){//Build vector of possible moves. v[0] represents number of possible numbers, v[1]-v[9] represent the numbers 1-9.
		v.push_back(0); //If v[1] == 0, then a 1 cannot be placed in that spot. If v[1] ==1, then a 1 may possibly be written to the spot.
	}
	vector<vector<int> > vv;
	for (int i = 0; i < 9; i++){ //Place one of these vectors in every place of a row
		vv.push_back(v);
	}
	for (int i = 0; i < 9; i++){ //Push 9 of these rows into the grid.
		possible_moves.push_back(vv);
	}
	//This has created a 3-dimensional array, [row][column][potential moves]


	
	//Fill the possible_moves array with appropriate values
	for (int i = 0; i < 9; i++){
		for (int j = 0; j < 9; j++){ //For each place [row][column] of the grid
			if (start[i][j] == 0){ //If the space is 'empty'

				int q; //Extends scope of 'k', in case only 1 potential number is found

				for (int k = 1; k < 10; k++){ //for numbers 1-9
					if (!counter_connects_to_number(i, j, k)){ //If the space does not 'connect' to the number
						possible_moves[i][j][k] = 1; //Mark that number as a potential move
						q = k; //Extend the scope of 'k'
						possible_moves[i][j][0]++; //Increment the number of potential moves of the space.
					}
				}
				if (possible_moves[i][j][0] == 1){ //If only 1 potential move was found
					one_move_possible++; //Increment the number of moves we can make
					vector<int> v;	//Make a vector with the needed info to make the move [row][col][val]
					v.push_back(i);
					v.push_back(j);
					v.push_back(q);
					single_moves.push_back(v); //Push it onto the stack of moves to be made
				}
				if (possible_moves[i][j][0] > max_moves_possible){ //Update the max_moves_possible variable if applicable
					max_moves_possible = possible_moves[i][j][0];
				}
			}
		}
	}
}

//Whenever a number has been written to the final table, the possible_moves table needs to be updated to eliminate erronious...
//... potential numbers that the written space is 'connected' to, often finding more definitive moves.
void sudoku::update_possible_move_table(vector<int> v){
	int i = v[0], j = v[1], k = v[2];
	
	//First, update the ith row and the jth column
	for (int q = 0; q < 9; q++){
		if (final[i][q] == 0 && q != j){ //For every entry in the ith column, if an entry hasn't been made...
			if (possible_moves[i][q][k] == 1 ){ //If the entry made was a potential for this spot...
				possible_moves[i][q][k] = 0; //Remove it as a potential entry
				possible_moves[i][q][0]--; //Decrement the number of potential moves we can make in this spot
				if (possible_moves[i][q][0] == 1){ //If the number of moves we can make in this spot is 1, we have another definitive move
					one_move_possible++; //Increment the move counter
					possible_moves[i][q][0] = 0;//Mark 0 potential solutions for this spot (avoid flagging this space for multiple writes)
					vector<int> v; //Make the vector used to write the move
					v.push_back(i);
					v.push_back(q);
					for (int y = 1; y < 10; y++){
						if (possible_moves[i][q][y] == 1){ //Find the value to be written		
							v.push_back(y);
							break;
						}
					}
					single_moves.push_back(v); //Add the move to the stack of moves
				}
			}
		}
		if (final[q][j] == 0 && q != i){ //Same as above if{} block, traversing the entries in the jth column.
			if (possible_moves[q][j][k] == 1){
				possible_moves[q][j][k] = 0;
				possible_moves[q][j][0]--;
				if (possible_moves[q][j][0] == 1){
					one_move_possible++;
					possible_moves[q][j][0] = 0;
					vector<int> v;
					v.push_back(q);
					v.push_back(j);
					for (int y = 1; y < 10; y++){
						if (possible_moves[q][j][y] == 1 && q != i){
							v.push_back(y);
							break;
						}
					}
					single_moves.push_back(v);
				}
			}
		}
	}


	//Next, update the subgrid
	int starti, startj;
	if (i < 3) starti = 0; //Define the subgrid to be checked (row)
	else if  (i < 6) starti = 3;
	else starti = 6;
	
	if (j < 3) startj = 0;  //Define the subgrid to be checked (column)
	else if (j < 6) startj = 3;
	else startj = 6;

	for (int q = starti; q < starti + 3; q++){ //For every entry in the subgrid
		for (int r = startj; r < startj + 3; r++){
			if (final[q][r] == 0){ //Same as the blocks above, checking the rows and columns.
				if (possible_moves[q][r][k] == 1){ 
					possible_moves[q][r][k] = 0;
					possible_moves[q][r][0]--;
					if (possible_moves[q][r][0] == 1){
						one_move_possible++;
						possible_moves[q][r][0] = 0;
						vector<int> v;
						v.push_back(q);
						v.push_back(r);
						for (int y = 1; y < 10; y++){
							if (possible_moves[q][r][y] == 1 && (q != i && r != j)){
								v.push_back(y);
								break;
							}
						}
						single_moves.push_back(v);
					}
				}
			}
		}
	}
}



//=====================================COUNTER//
/*
* The 'counter' algorithm checks to see if a given position on the grid is connected (by row, column, or subgrid)...
* to a given number, in order to determine which numbers can potentially be entered into each space. These two ...
* algorithms are the 'bottom rungs' of the algorithm, checking only individual numbers passed by the caller.
*/

//Does [i][j] connect to 'k'?
bool sudoku::counter_connects_to_number(int i, int j, int k){
	bool result = false;
	for (int q = 0; q < 9; q++){ //Check along the ith row and jth column
		if ((final[q][j] == k && q != i) || (final[i][q] == k && q != j)){
			result = true;
			break;
		}
	}
	if (result == false){ //If we didn't find 'k' in the row / column, check the subgrid
		result = counter_subgrid_has_number(i, j, k);
	}
	return result; //True = found 'k'
}

//Does the subgraph [i][j] belongs to contain k?
bool sudoku::counter_subgrid_has_number(int i, int j, int k){
	int starti, startj;
	if (i < 3) starti = 0;//Define the subgrid that needs to be traversed
	else if  (i < 6) starti = 3;
	else starti = 6;
	
	if (j < 3) startj = 0;
	else if (j < 6) startj = 3;
	else startj = 6;

	bool result = false;
	for (int q = starti; q < starti + 3; q++){ //For every entry in the subgrid
		for (int r = startj; r < startj + 3; r++){
			if (final[q][r] == k){ //Check to see if it contains the number we're after.
				result = true;
				break;
			}
		}
	}
	return result;
}



//====================================BASIC//
/*
* The 'basic' algorithm is designed to analyze the potential_moves table to see if there are any potenatial moves that are
* isolated (not connected to other entries which also can place a number 'k'). i.e. if only one place in a row can hold a '1',
* then we know it must be placed there. These four methods implement this search for isolated potential numbers.
*/

//Broad strokes of the algorithm
bool sudoku::basic_single_move_search(){
	bool result = false;
	for (int i = 0; i < 9; i++){ //For each place in the graph
		for (int j = 0; j < 9; j++){
			if(final[i][j] == 0){ // If the entry isn't defined
				for (int k = 1; k < 10; k++){ 
					if (possible_moves[i][j][k] == 1){ //For each potential number for this place
						bool write_it = false; //Base case
						if (!basic_connects_to_number_row(i, j, k)){ //Check the row,
							write_it = true;
						}
						else if(!basic_connects_to_number_column(i, j, k)){ // column,
							write_it = true;
						}
						else if (!basic_connects_to_number_subgrid(i, j, k)){ //and subgrid for other places with this potential.
							write_it = true; //If find one, we have an isolated potential number; 'k' must be entered here
						}
						if (write_it == true){
							vector<int> v; //Same old 'to-be-written' algorithm
							v.push_back(i);
							v.push_back(j);
							v.push_back(k);
							one_move_possible++;
							possible_moves[i][j][0] = 0;
							single_moves.push_back(v);
							result = true; //Indicate the progress was made to the caller.
						}
					}
				}
			}
		}
	}
	return result;
}

//Is there another potential entry 'k' in the ith row?
bool sudoku::basic_connects_to_number_row(int i, int j, int k){
	bool result = false;
	for (int q = 0; q < 9; q++){
		if (final[i][q] == 0){
			if (possible_moves[i][q][k] == 1 && q != j){
				result = true;
				break;
			}
		}
	}
	return result;
}

//Is there another potential entry 'k' in the jth column?
bool sudoku::basic_connects_to_number_column(int i, int j, int k){
	bool result = false;
	for (int q = 0; q < 9; q++){
		if (final [q][j] == 0){
			if (possible_moves[q][j][k] == 1 && q != i){
				result = true;
				break;
			}
		}
	}
	return result;
}

//Is there another potential entry in the subgraph [i][j] belongs to?
bool sudoku::basic_connects_to_number_subgrid(int i, int j, int k){
	int starti, startj;
	if (i < 3) starti = 0;
	else if  (i < 6) starti = 3;
	else starti = 6;
	
	if (j < 3) startj = 0;
	else if (j < 6) startj = 3;
	else startj = 6;
	bool result = false;
	for (int q = starti; q < starti + 3; q++){
		for (int r = startj; r < startj + 3; r++){
			if (final[q][r] == 0){
				if (possible_moves[q][r][k] == 1 && (q != i || r !=j)){
					return true;
				}
			}
		}
	}
//cout << endl << endl;
	return result;
}


/* ========================================= PRINT METHODS ============================================*/

//Print the current progress of the table, make it look pretty.
void sudoku::printStuff(){
	cout << "single_moves.size() = " << single_moves.size() << endl;
}

//Print the original table
void sudoku::printStart(){
	for (int i = 0; i < 9; i++){
		for (int j = 0; j < 9; j++){
			cout << start[i][j] << " ";
		}
		cout << endl;
	}
}

//Print the completed table
void sudoku::printFinal(){
	for (int i = 0; i < 9; i++){
		for (int j = 0; j < 9; j++){
			cout << final[i][j] << " ";
		}
		cout << endl;
	}
}

//Print the paritally completed table
void sudoku::printPartial(){
	find_max_moves();
	int width = (max_moves_possible * 2) + 3;
	for (int i = 0; i < 9; i++){
		for (int j = 0; j < 9; j++){
			if (final[i][j] != 0){
				stringstream ss;
				ss << final[i][j];
				string s = ss.str();
				
				cout << setw(width) << left << s;
			}
			else{
				string s = "(";
				for (int k = 1; k < 10; k++){
					if (possible_moves[i][j][k] == 1){
						stringstream ss;
						ss << k;
						s += ss.str() + ",";
						ss.clear();
					}
				}
				s += ")";
				cout << setw(width) << left << s;
			}
		}
		cout << endl;
	}
}
