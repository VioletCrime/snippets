/*
* sudokutest.cc v0.0 by Kyle Smith
* Meant to test sudoku.cc, by taking in a filename, 
* and instantiating a sudoku object with it.
*/

#include <string>
#include <sstream>
#include "sudoku.h"

using namespace std;

int main(int argc, char* argv[]){
	if (!argc == 2){ //Make sure we have an input file
		cout << "Usage: " << argv[0] << " infile.\n";
	}

	stringstream ss; //Used to convert argv[0] to C++ string
	ss << argv[1];
	string s = ss.str();
	sudoku sud(s);
	//sud.printStuff(); //Uncomment for some runtime variables.
	return 0;
}
