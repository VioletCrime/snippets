/*
* sudoku.h, by Kyle Smith
* Header madness for sudoku.cc
* v0.1 3/30/12
*/

#ifndef SUDOKU_H
#define SUDOKU_H

#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

class sudoku{
	typedef std::vector<std::vector<int> > matrix;
	private:
		matrix start, final, single_moves;
		std::vector<matrix> possible_moves;
		int zero_count, one_move_possible, max_moves_possible;

		void buildMatrix(std::ifstream);
		void solve();
		void count_zeros();
		void find_max_moves();
		void build_possible_move_table();
		void update_possible_move_table(std::vector<int>);

		bool counter_connects_to_number(int, int, int);
		bool counter_subgrid_has_number(int, int, int);

		bool basic_single_move_search();
		bool basic_connects_to_number_row(int, int, int);
		bool basic_connects_to_number_column(int, int, int);
		bool basic_connects_to_number_subgrid(int, int, int);

		void printStart();
		void printFinal();
		void printPartial();
	
	public:
		sudoku();
		sudoku(const std::string);
		sudoku(const matrix);
		sudoku(const sudoku &);
		~sudoku();
		void printStuff(); //Debug method: public for ease of use.
};
#endif
