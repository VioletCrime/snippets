#!/usr/bin/python

class Hour(object) :
    def __init__(self) :
        self.val = 0

    def __repr__(self) :
        return repr(self.val)

    def add(self, amt) :
        self.val += amt

    def sub(self, amt) :
        self.val -= amt

class Minute(object) :
    def __init__(self, hour) :
        self.val = 0
        self.hour = hour

    def __repr__(self) :
        return repr(self.hour) + ":" + repr(self.val)

    def add(self, amt) :
        self.hour.add(amt / 60)
        self.val += amt % 60
        if self.val >= 60 :
            self.hour.add(self.val / 60)
            self.val -= 60 * (self.val / 60)

    def sub(self, amt) :
        self.hour.sub(amt / 60)
        self.val -= amt % 60
        if self.val < 0 :
           self.hour.sub(abs(self.val) / 60)
           self.val += 60 * (abs(self.val) / 60)

class Second(object) :
    def __init__(self, minute) :
        self.val = 0
        self.minute = minute

    def __repr__(self) :
        return repr(self.minute) + ":" + repr(self.val)

    def add(self, amt) :
        self.minute.add(amt / 60)
        self.val += amt % 60
        if self.val >= 60 :
            self.minute.add(self.val / 60)
            self.val -= 60 * (self.val / 60)

    def sub(self, amt) :
        self.minute.sub(amt / 60)
        self.val -= amt % 60
        if self.val < 0 :
            self.minute.sub(abs(self.val) / 60)
            self.val += 60 * (abs(self.val) / 60)

class timeManager(object) :
    def __init__(self) :
        self.hour = Hour()
        self.minute = Minute(self.hour)
        self.second = Second(self.minute)

    def __repr__(self) :
        return repr(self.second)

    def addTime(self, cmd) :
        time = cmd.split(" ")[-1].split(":")
        if len(time) == 3 :
            self.second.add(int(time[2]))
            self.minute.add(int(time[1]))
            self.hour.add(int(time[0]))
        elif len(time) == 2 :
            self.second.add(int(time[1]))
            self.minute.add(int(time[0]))
        elif len(time) == 1 :
            self.second.add(int(time[0]))
        else :
            print "Could not execute " + cmd + ", format unrecognized."

    def subTime(self, cmd) :
        time = cmd.split(" ")[-1].split(":")
        if len(time) == 3 :
            self.second.sub(int(time[2]))
            self.minute.sub(int(time[1]))
            self.hour.sub(int(time[0]))
        elif len(time) == 2 :
            self.second.sub(int(time[1]))
            self.minute.sub(int(time[0]))
        elif len(time) == 1 :
            self.second.sub(int(time[0]))
        else :
            print "Could not execute " + cmd + ", format unrecognized."


def print_commands() :
    print "\n\"add [time]\":  Add time to the running total."
    print "\"sub [time]\":  Subtract time from the running total."
    print "\"help\":        Show this message."
    print "\"exit\":        Exit the program."
    print "\nTime format is as follows: HH:MM:SS  or  MM:SS  or  SS."
    print "Values may be any number of digits; two are used as a conventional example.\n"

if __name__ == "__main__" :
    cmd = ""
    man = timeManager()
    while not cmd.lower() == "exit" :
        print "\nRunning tally: " + repr(man)
        cmd = raw_input("Enter command ('help' for list of commands): ")
        if cmd.lower().split(" ")[0] == "add" :
            man.addTime(cmd)
        elif cmd.lower().split(" ")[0] == "sub" :
            man.subTime(cmd)
        elif cmd.lower().split(" ")[0] == 'help' :
            print_commands()
    exit(0)
