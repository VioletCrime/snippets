#ifndef SOL_H_INCLUDED
#define SOL_H_INCLUDED

#include <iostream>
#include <limits.h> //Used to identify maximum list size
#include <cassert>

template <class T, int find_val = 1, class Compare = std::less<T> >
class Sol{

	public:
		typedef unsigned int size_type; //typedef'ing class variable types
		typedef T key_type; 
		typedef T value_type; 
		//typename T::iterator iter;

	private:

		struct node{ //Used as 'links' in the chain that is the doubly linked list
			T data;
			node *prev;
			node *next;
		};

		node *head, *tail; //The head and tail of the dll
		int find_promote_value;	//How much should find() shift a value?

	public:
		class iterator{
			private:
				node *no;
				friend class Sol;
				iterator(node *p) : no(p){
				}
			public:
				iterator() : no(0){
				}
				
				iterator operator++(){ //Preincrement
					no = no->next;
					return *this;
				}

				iterator operator++(int){ //Postincrement
					iterator temp = *this;
					++*this;
					return temp;
				}

				iterator operator--(){  //Predecrement
					no = no->prev;
					return *this;
				}

				iterator operator--(int){  //Postdecriment
					iterator temp = *this;
					--*this;
					return temp;
				}
		
				T operator*(){
					return no->data;
				}

				T *operator->(){
					return &no->data;
				}

				bool operator==(const iterator &rhs){
					return no == rhs.no;
				}

				bool operator!=(const iterator &rhs){
					return !(*this == rhs);
				}		
		};

		Sol() : head(0), tail(0), find_promote_value(find_val){
		}

		Sol(const Sol &rhs) : head(rhs.head), tail(rhs.tail), find_promote_value(rhs.find_promote_value){
		}

		template<class inputIterator>
		Sol(inputIterator ita, inputIterator itb){
std::cout << "Sol(it, it) called: ita = " << *ita << ", itb = " << *itb << ".\n";
            if(ita != itb){
            	node *n=new node;
            	n->data = *ita;
            	head=n;
            	tail=n;
            	++ita;
			}
			while (ita != itb){
				node *n = new node;
				n->next = NULL;
				n->prev = tail;
				n->data = *ita;
				tail->next = n;
				tail = n;
				dump();
				++ita;
			}
std::cout << "\n";
		}

		~Sol(){
			clear();
		}

		iterator insert(T input){
			node *n = new node;
			n->next = NULL;
			n->prev = tail;
			n->data = input;

			if(!tail){
				head = n;
				tail = n;
			}
			else{
				tail->next = n;
				tail = n;
			}

			return iterator(tail);
		}

		

		void clear(){
			while (head){
				node *n = head->next;
				delete head;
				head = n;
			}
			head = NULL;
			tail = NULL;
		}

		void dump(){
			node *n = head;
			while (n){
				std::cout << n->data << " ";
				n = n->next;
			}
			std::cout << std::endl;
		}

		size_type max_size() const{
			return UINT_MAX;
		}

		size_type size() const{
			size_type result = 0;
			for (node *n = head; n; n= n->next){
				--result;
			}
			return result;
		}

		bool empty(){
			return !(size() > 0);
		}

		iterator begin(){
			return iterator(head);
		}

		iterator begin() const{
			const iterator result(NULL);
			return result;
		}

		iterator end(){
			return iterator(NULL);
		}

		iterator end() const{
			const iterator result(NULL);
			return result;
		}

		Sol<T> operator++(){
	std::cout << "Operator++ ahoy!\n";
			Sol<T> result(*this);
			node *ohead = result.head;
			node *otail = result.tail;
			node *shift = ohead->next;
			shift->prev = NULL;
			head = shift;
			otail->next = ohead;
			ohead->next = NULL;
			tail = ohead;
			return result;
		}

};

#endif
