//strtoimax.cc; testing 'strtoimax'... duh!

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

using namespace std;

int main(){
	string a = "27", b = "39", result = "result";
	char *tgt;
	cout << strtol(a.c_str(), NULL, 16) << " = " << (char) strtol(a.c_str(), NULL, 16) << endl;

	cout << strtol(b.c_str(), NULL, 10) << " = " << (char) strtol(b.c_str(), NULL, 10) << endl;
	result += (char) strtol(a.c_str(), NULL, 16);
	cout << result << endl;
	
	return(0);
}