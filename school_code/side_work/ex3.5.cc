    #include "Sol.h"
    #include <iostream>
    #include <cassert>
    #include <cstring>
    #include <sstream>

    using namespace std;

    template<typename T>
    string cat(const T &con) {
        ostringstream os;
        for (typename T::iterator it = con.begin(); it != con.end(); ++it){
            os << *it;
			cout << "hi " << *it << " ";
		}
        return os.str();
    }

    int main() {
        Sol<int> foo;
        for (int i=1; i<=9; i++)
            foo.insert(i);

cout << "foo: ";
foo.dump(); 

foo++;
cout << "\nfoo++: ";
foo.dump();

++foo;
cout << "\n++foo: ";
foo.dump();

foo--;
cout << "\nfoo--: ";
foo.dump();

--foo;
cout << "\n--foo: ";
foo.dump();

foo + 2;
cout << "\nfoo + 2: ";
foo.dump();

foo = foo + 2;
cout << "\nfoo = foo + 2: ";
foo.dump();

2 + foo;
cout << "\n2 + foo: ";
foo.dump();

foo = 2 + foo;
cout << "\nfoo = 2 + foo: ";
foo.dump();

foo += 2;
cout << "\nfoo += 2: ";
foo.dump();

foo - 2;
cout << "\nfoo - 2: ";
foo.dump();

foo = foo - 2;
cout << "\nfoo = foo - 2: ";
foo.dump();

2 - foo;
cout << "\n2 - foo: ";
foo.dump();

foo = 2 - foo;
cout << "\nfoo = 2 - foo: ";
foo.dump();

foo -= 2;
cout << "\nfoo -= 2: ";
foo.dump();

        return 0;
    }
