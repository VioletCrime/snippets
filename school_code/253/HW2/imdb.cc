/*
*imdb.cc: A simple program used to scrape info from the IMBD (imdb.com)
*Written by Kyle Smith (smithkyl) for CS253, Spring '12
*Started 1/29/12
*/

#include <algorithm> //I don't have OCD
#include <cstring>   //I have CDO
#include <fstream>   //It's like OCD
#include <iostream>  //Only the letters are alphabetical
#include <stdio.h>   //Like they should be
#include <stdlib.h>
#include <string> 
#include <vector>

using namespace std; //Forgot this once... never again.

void fixHTML(string &instr); //Prototypes, ahoy!
void getPages(int &argc, char *argv[], bool &exitEarly); 
string getSubject(int i, char *argv[]);
int numberWorks(int argc, char *argv[]);
void printArgs(int argc, char *argv[], string stuff = "");
void printList(int argc, char *argv[]);
void removeTemps(int leng, char *argv[]);
void sortList(int argc, char *argv[]); //~Prototypes, ahoy!


int main(int argc, char *argv[] ){ ///////////////////////////////////////MAIN////////////////////////////////////////////
    if (argc < 2 || strcmp(argv[1], "-k") == 0){ //If there are no valid arguments...
	cout << "Usage: " << argv[0] << " title ID | movie ID ... [-k]\n"; //...print the usage.
    }
    else{ //If the arguments are satisfactory, we move onto the meat of the program.
	bool exitEarly = false;
	getPages(argc, argv, exitEarly); //First, we get the pages from imdb.com
	if (exitEarly == true){ //If an error is encountered fetching the pages...
		if (strcmp(argv[argc-1], "-k") != 0) {//...and the '-k' flag isn't present...
			removeTemps(argc, argv); //...then remove the temp files.
		}
		return(0);//Either way, we're exiting.
	}
	printList(argc, argv); //Next, call the output method using the sorted list

	if (strcmp(argv[argc - 1], "-k") != 0){ //Finally, if the last argument isn't '-k'...
	    removeTemps(argc, argv); //...remove the temp files.
	}
    }
    
    return 0; //And that's All She Wrote (Angela Lansbury is so secksy)!
}


//Method used to identify HTML entities in a string, and replace them.
void fixHTML(string &instr){ ////////////////////////////////////////////FIX_HTML///////////////////////////////////////////
	size_t finda = instr.find("&#"); //First, find the first instance that needs to be fixed
	string temp; //Create a string we will use to isolate substrings of 'instr'
	while(finda != string::npos){ //While we are finding HTML entities
		temp = instr.substr(finda+2, 3); //Get the 3rd, 4th and 5th chars ('&#x27;' -> 'x27', '&#39;' -> '39;')
		string firstChar = temp.substr(0, 1); //Get the first char of the substring
		if (strcmp(firstChar.c_str(), "x") == 0){ //if the first char is 'x', we know it's a base-16 number
			temp = temp.substr(1,2);//Isolate the two digits we need to convert
			char u = (char) strtol(temp.c_str(), NULL, 16); //Convert the string to an int, and immediatly cast as a char
			string rep = string(1, u); //Finally, express the char as a string
			instr.replace(finda, 6, rep); //insert this new string in place of the 6-char HTM entity
		}
		else{ //if the first char is not 'x', we know it's a base-10 number; almost identical to previous 'if'
			temp = temp.substr(0,2);
			char u = (char) strtol(temp.c_str(), NULL, 10);
			string rep = string(1, u);
			instr.replace(finda, 5, rep);
		}
		finda = instr.find("&#"); //Find the next HTML entity to be fixed, if there is one
	} 
}


//
void getPages(int &argc, char *argv[], bool &exitEarly){/////////////////////////////GET_PAGES//////////////////////////////////////////////
    string syscmd; //Used to construct the command to be passed to system()
    string eyed; //Used to store string version of argv[i]

    //Get the files
    for (int i = 1; i <= numberWorks(argc, argv); i++){//For each argument...
	eyed = string(argv[i]); //... turn the c_string into a 'good' string
	if (argv[i][0] == 't'){//If the argument is the ID of a movie title....
	    syscmd = "wget -nv -o log -O " + eyed + " http://www.imdb.com/title/" + eyed + "/"; //...build the command appropriatly
	    system(syscmd.c_str()); //Call system() with an inferior string structure.
	}
	else if (argv[i][0] == 'n'){//same as above 'if' statement, only for names.
	    syscmd = "wget -nv -o log -O " + eyed + " http://www.imdb.com/name/" + eyed + "/";
	    system(syscmd.c_str());
	}//end elseif
	else{//If the argument is neither a name nor title...
	    if (i != (argc-1)){ //...nor the '-k' operator...
		cerr << argv[i] << " is neither an actor ID nor title ID!\n";//...complain!
		exitEarly = true; //Set flag for early program termination
		continue; //No need to finish this iteration of the for() loop
	    }
	}

	//Check wget's log for an error (This is part of the primary for() loop!)
	ifstream fLog; //Create a file stream
	fLog.open("log"); //Open the log file
	string str; //Used to store each line of the log file
	size_t found; //Location of "ERROR" stored here
	while (fLog.good()){ //While loop traverses entire file
	    getline (fLog, str); //Get the next line
	    found = str.find("ERROR"); //Check the line for "ERROR"
	    if (found != string::npos){ //If "ERROR" is found, all hell breaks loose.

		exitEarly = true;

		string cmd = "rm " + string(argv[i]); //First, prepare to remove the worthless file...
		system(cmd.c_str()); //...and summarily do it

		fLog.close();	//Close the log file so we can start from the beginning
		fLog.open("log"); //Reopen the log file (at the beginnning)
		while (fLog.good()){  //Loop used to output error-containing logs
		    getline (fLog, str); //Get the next line of the file
			if (str.size() > 0){ //Make sure we're not outputting extra empty lines
			   cerr << str << endl; //Let the user know what's up
			}//end if
		}//end while
		for (int j = i; j < argc - 1; j++){ //Fix the argument list for later use.
		    argv[j] = argv[j+1]; //Roll back the appropriate arguments
		}
		i--; //We need to do 'i' again, since there is a new argument in the 'i'th index
		argc--; //There are one fewer arguments.
		argv[argc] = NULL; //Set new breakpoint 
		break; //No need to finish this iteration of the main loop
	    }
	}
	fLog.close(); //Close the file before going onto the next loop iteration
    }//end for
}


//Scans the 'i'th argument's file for the Actor / Actress / Title, and returns it.
string getSubject(int i, char *argv[]){ /////////////////////////////GET_SUBJECT/////////////////////////////////////////
    string result = string(argv[i]); //Create the strign to return; at first, it's used to hold the filename we need
    size_t title; 
    ifstream input;

    input.open(result.c_str()); //open the file in question
    while (input.good()){//While there is more to the file to parse...
	getline (input, result); //Get the next lines...
	title = result.find("<title>");
	if (title != string::npos){ //Until we find the line containing '<title>'
	    result.erase(title - 8, 15); //erase "<title>"
	    title = result.find(" - IMDb"); //Find either ' - IMDb' or 'IMDb - '; delete
	    if (title != string::npos){
		result.erase(title, 7);
	    }
	    title = result.find("IMDb - ");
	    if (title != string::npos){
		result.erase(title, 7);
	    }
	    title = result.find("</title>"); //Finally, erase '</title>'
	    result.erase(title, 8);

	    
	    break;//We've found what we've come for; no need to read the rest of the file.
	}
    }
    input.close(); //Close the input file
    fixHTML(result); //Clean the HTML entities from the resulting string
    return result; //And pass it along to the next sucker
}


//Returns the number of arguments to be processed
int numberWorks(int argc, char *argv[]){ ///////////////////////////////NUMBER_WORKS///////////////////////////////////////
    int result = argc - 1; //Omits 'argv[0]'
    if (strcmp(argv[argc - 1 ], "-k") == 0){//if the last argument is "-k", don't count it
	result--;
    }
    return result; //Simple!
}


//Responsible for outputting the appropraite information to the user
void printList(int argc, char *argv[]){ /////////////////////////////////PRINT_LIST////////////////////////////////////////
    int n = numberWorks(argc, argv); //Get the number of arguments to process
    for(int i = 1; i <= n; i++){ //For each argument... (fairly large block)

	vector<string> outputList; //Declare a vector of strings used to store the data to be emitted
	ifstream input; //Create an ifstream to read from the files with.
	string test;//String used to read lines in from the input files
	size_t find, findTwo, placeHolder; //find and findTwo are used to hold index of substrings, placeHolder searches for sentinel values

	input.open(argv[i]); //Open the file for the ith argument
	if (argv[i][0] == 't'){ //Check the argument to see if it's a title.
	    cout << "Movie: " << getSubject(i, argv) << endl; //Output the title of the movie
	    while (input.good()){ //While there's still info left...
		getline(input, test); //Fetch the next line of the file...
		find = test.find("<table class=\"cast_list\">");//...test it for a specific expression indicating upcoming info we need.
		if(find != string::npos){ //When we find the expression...
		    placeHolder = test.find("</table>"); //Begin looking for our sentinel expression, indicating end of pertinent information
		    while(placeHolder == string::npos){ //Until we haven't found the sentinel expression
			getline(input, test);
			find = test.find("<td class=\"name\">"); //Check each line for a static info tag
			if(find != string::npos){ //Each time we find such a line, do the following
			    string result = "", blah;
			    getline(input, test);
			    find = test.find("    >"); //Find the end of the <a ...> tag
			    findTwo = test.find("</a>"); //And find the beginning of the </a> tag
			    blah = test.substr(find+5, (findTwo - find - 5)); //Extrapolate the position of our info from these
			    result.append(blah + ";"); //Jam the actor name into the result string
			    blah = test.substr(find-11, 9); //Next, isolate the ID of the actor
			    result.append(blah + ";"); //and jam that in as well.
			    for (int k = 0; k < 9; k++){ //Last bit of info comes 9 lines later
				getline (input, test);
			    }
			    find = test.find("    >"); //Check for link tags
			    if (find == string::npos){ //No link tags? It's just the role
				result.append(test); //slam the role on the end. 
				fixHTML(result); //Fix the HTML for the result string
				outputList.push_back(result); //Push that onto the vector
			    }
			    else{ //Has link tags?
				test.erase(0, find+5); //Erase the leading tag
				find = test.find("</a>");
				test.erase(find, 4); //Erase the trailing tag
				result.append(test); //Append to result string
				fixHTML(result); //Fix it's HTML entities
				outputList.push_back(result); //And save for future use
			    }
			}
			placeHolder = test.find("</table>"); //Check for the sentinel expression.
		    }
		    sort (outputList.begin(), outputList.end()); //employ vector's built-in sort function (sweeeeet)
		    int z = outputList.size(); //doesn't seem to like doing this inside of a for loop's parameters
		    for (int l = 0; l < z; l++){ //Output block; can't get simpler than this.
			cout << "Actor: " << outputList[l] << endl;
		    }
		}
	    }
	}
	else{ //If the input isn't a title, it must be a name; exceedingly similar to the previous 'if' block
	    cout << "Actor: " << getSubject(i, argv) << endl;
	    while (input.good()){
		getline(input, test);
		find = test.find("<div id=\"filmo-head-Act");
		if(find != string::npos){
		    placeHolder = test.find("Nikola Tesla is an unsung hero");//'forces' the coming while loop
		    while(placeHolder == string::npos && input.good()){
			getline(input, test);
			find = test.find("<span class=\"year_column\">");
			if(find != string::npos){
			    for (int u = 0; u < 3; u++){
				getline(input, test);
			    }
			    find = test.find("    >");
			    findTwo = test.find("</a>");
			    string result = "", blah;
			    blah = test.substr(find+5, (findTwo- find - 5));
			    result.append(blah + ";");
			    blah = test.substr(find-11, 9);
			    result.append(blah);
		    
			    for (int y = 0; y < 7; y++){
				getline(input, test);
			    }
			    if(test.size() == 0){
				getline(input, test);
			    }
			    find = test.find("    >");
			    if(find == string::npos){
				string work = test.substr(0,1);
				if (work.compare("(") != 0){
					result.append(";" + test);
				}
				fixHTML(result);
				outputList.push_back(result);
			    }
			    else{
				test.erase(0, find+5);
				find = test.find("</a>");
				test.erase(find,4);
				result.append(";" + test);
				fixHTML(result);
				outputList.push_back(result);
			    }
			}
			placeHolder = test.find("<div id=\"filmo-head-"); //Check for the end-of-block condition
		    }
		    sort (outputList.begin(), outputList.end());
		    int z = outputList.size();
		    for (int l = 0; l < z; l++){
			cout << "Movie: " << outputList[l] << endl;
		    }
		}
	    }
	}
	input.close();
    }//End for
}


//Iterates through argv, removing the corresponding temp files, as well as the remaining log file
void removeTemps(int leng, char *argv[]){////////////////////////////////REMOVE_TEMPS//////////////////////////////////////
    string cmd; //As with getPages(), the command we pass to system() starts as a string
    string name; //Used to temporarily hold each argument as a string
    for (int i = 1; i < leng; i++){  //For each argument...
	name = string(argv[i]); //...convert the c_string into a good string...
	cmd = "rm " + name + "*"; //...assemble the command for system...
	system(cmd.c_str()); //...and call system() using that.
    }
	cmd = "rm log"; //Prepare to remove the log file
	system(cmd.c_str()); //Make it so.
}
