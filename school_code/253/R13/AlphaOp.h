#ifndef ALPHA_OP_INCLUDED
#define ALPHA_OP_INCLUDED

template <class Functor>
class AlphaOp { };

template <class T, class Functor>
class LeftPart {
    public:
	LeftPart(const T &value) : value(value) {}
	// Apply the function to the left & right parts:
	T operator>(const T &rhs) {
		return Functor()(value, rhs);
	}
	const T value; 
}; 

// collect the left argument.
template <class T, class Functor>
LeftPart<T,Functor> operator<(const T &value, const AlphaOp<Functor> &) {
	return LeftPart<T,Functor>(value); 
} 
#endif /* ALPHA_OP_INCLUDED */
