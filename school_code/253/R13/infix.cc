#include "AlphaOp.h"

#include <iostream>
#include <string>
using namespace std;

class fryCook {
	public:
		string operator() (string a, string b){
			int r = a.size(), s = b.size();
			if (r > s){
				return a;
			}
			else
				return b;
		}
};

class MyMax {
    public:
        int operator()(int a, int b) {
		return max(a,b);
	}
};

class Purge {
    public:
	// Remove all instances of victim from s:
	string operator()(string s, const string &victim) {
		size_t loc;
		while ((loc = s.find(victim)) != string::npos)
			s.erase(loc, victim.size());
		return s;
	}
};

AlphaOp<MyMax> MAX;
AlphaOp<Purge> zap;
AlphaOp<fryCook> foo;

int main() {
	int a = 1 <MAX> 2;
	cout << "Maximum is " << a << endl;
	int b = 4 <MAX> 3;
	cout << "Maximum is " << b << endl;

	string name = "Barack Hussein Obama";
	string no_middle = name <zap> "Hussein ";
	cout << "Prez: " << no_middle << endl;

	string r = "This sure is big!";
	string s = "This is a lot bigger, though!";
	string result = r <foo> s;
	cout << "The larger string is \"" << result << "\"" << endl;
}
