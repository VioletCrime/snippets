#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
	if (argc != 2) {
		cerr << "usage: " << argv[0] << " infile\n";
		return 1;
	}
	fstream in(argv[1]);
	if (!in) {
		cerr << argv[0] << ": can't open " << argv[1] << endl;
		return 1;
	}

	string s;
	while (getline(in, s)) {
		size_t n;
		if ( (n=s.find("Bush")) != string::npos ){
			in.seekp(n);
			in.write("Gore", 4);
			long pos = in.tellp();
			in.seekg(pos - s.size());
			
		}
	}

	return 0;
}
