#include <iostream>
#include <sstream>

using namespace std;

class Number {
    public:
	Number() : value(0) {}
	Number(int v) : value(v) {}
	Number &operator=(int n) { value = n; return *this; }
	operator int() const { return value; }
    private:
    	int value;
};

istream &operator>>(istream &is, Number &rhs) {
	int n;
	string s;

	is>>n;
    rhs = n;

   	if(!is){
          is.clear();
          is>>s;
		if(s == "one") rhs = 1;
		else if(s == "two") rhs = 2;
		else if(s == "three") rhs = 3;
		else if(s == "four") rhs = 4;
		else if(s == "five") rhs = 5;
		else
           is.setstate(ios::failbit);

	}
	return is;
}


int main() {
	Number n = 666;
	istringstream ss("12 34 three 789 five");

	while (ss >> n)
		cout << n << ' ';
	cout << endl;
}
