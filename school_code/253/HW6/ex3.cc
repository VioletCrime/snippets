    #include "Sol.h"
    #include <iostream>
    #include <cassert>
    #include <cstring>
    #include <sstream>

    using namespace std;

    template<typename T>
    string cat(const T &con) {
        ostringstream os;
        for (typename T::iterator it = con.begin(); it != con.end(); ++it)
            os << *it;
        return os.str();
    }

    int main() {
        Sol<int> foo;
        for (int i=1; i<=9; i++)
            foo.insert(i);

        cout << cat(foo) << '\n';    // Should print 123456789
        cout << cat(foo+3) << '\n';  // Should print 789123456
        cout << cat(foo) << '\n';    // Should print 123456789
        ++foo;
        cout << cat(foo) << '\n';    // Should print 912345678
        foo -= 2;
        cout << cat(foo) << '\n';    // Should print 234567891

        return 0;
    }
