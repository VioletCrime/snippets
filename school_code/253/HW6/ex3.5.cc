    #include "Sol.h"
    #include <iostream>
    #include <cassert>
    #include <cstring>
    #include <sstream>

    using namespace std;

    template<typename T>
    string cat(const T &con) {
        ostringstream os;
        for (typename T::iterator it = con.begin(); it != con.end(); ++it){
            os << *it;
		}
        return os.str();
    }

    int main() {
        Sol<int> foo;
        for (int i=1; i<=9; i++)
            foo.insert(i);
cout << "\nOPERATION (KEY) : YOUR OUTPUT\n\n";
cout << "foo (123456789): " << cat(foo) << endl;

foo++;
cout << "\nfoo++ (912345678): " << cat(foo);

++foo;
cout << "\n++foo (891234567): " << cat(foo);

foo--;
cout << "\nfoo-- (9123456789): " << cat(foo);

--foo;
cout << "\n--foo (123456789): " << cat(foo);

foo + 2;
cout << "\nfoo + 2 (123456789): " << cat(foo);

foo = foo + 2;
cout << "\nfoo = foo + 2 (891234567): " << cat(foo);

2 + foo;
cout << "\n2 + foo (891234567): " << cat(foo);

foo = 2 + foo;
cout << "\nfoo = 2 + foo (678912345): " << cat(foo);

foo += 2;
cout << "\nfoo += 2 (456789123): " << cat(foo);

foo - 2;
cout << "\nfoo - 2 (456789123): " << cat(foo);

foo = foo - 2;
cout << "\nfoo = foo - 2 (678912345): " << cat(foo);

2 - foo;
cout << "\n2 - foo (678912345): " << cat(foo);

foo = 2 - foo;
cout << "\nfoo = 2 - foo (891234567): " << cat(foo);

foo -= 2;
cout << "\nfoo -= 2 (123456789): " << cat(foo) << endl;

        return 0;
    }
