    #include "Sol.h"
    #include <cassert>
    #include <iostream>
    #include <sstream>

    using namespace std;

    // A simple wrapper around a float.
    class Age {
      public:
        Age() : years(0.0) { }
        Age(float y) : years(y) { }
        float years;
    };

    // Note that you CAN'T compare two Age objects using < or ==.
    // This compares two Age objects, but only the integral portion.
    // Therefore, it is NOT the cast that 5.3 compares less than 5.4;
    // they’re both the same as far as we’re concerned.
    class AgeCompare {
      public:
        bool operator()(Age a, Age b) const {
            return int(a.years) < int(b.years);
        }
    };

    int main() {
        Sol<Age, 0, AgeCompare> who;
        who.insert(1.25);   // a baby
        who.insert(54.45);  // Jack
        who.insert(20);     // a typical student

        assert(who.find(1.00) != who.end());   // should succeed
        assert(who.find(1.25) != who.end());   // should succeed
        assert(who.find(1.99) != who.end());   // should succeed
        assert(who.find(2.00) == who.end());   // should fail

        assert(who.find(53.45) == who.end());  // should fail
        assert(who.find(54.00) != who.end());  // should succeed
        assert(who.find(54.45) != who.end());  // should succeed
        assert(who.find(54.78) != who.end());  // should succeed
        assert(who.find(55.78) == who.end());  // should fail

        assert(who.find(19.99) == who.end());  // should fail
        assert(who.find(20.00) != who.end());  // should succeed
        assert(who.find(20.11) != who.end());  // should succeed
        assert(who.find(20.99) != who.end());  // should succeed
        assert(who.find(21.00) == who.end());  // should fail
        assert(who.find(21.01) == who.end());  // should fail

        cout << "Success!  Promotions for everyone!\n";
    }
