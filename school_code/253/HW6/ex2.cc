    #include "Sol.h"
    #include <iostream>
    #include <cassert>
    #include <cstring>
    #include <sstream>

    using namespace std;

    template<typename T>
    string cat(const T &con) {
        ostringstream os;
        for (typename T::iterator it = con.begin(); it != con.end(); ++it)
            os << *it;
        return os.str();
    }

    int main() {
        char data[] = "abcdefghXYZ";
        Sol<char,4> foo(data, data+strlen(data));
        Sol<char,4>::iterator it;

        foo.erase('X');
        cout << cat(foo) << '\n';   // Should print abcdefghYZ
cout << "abcdefghYZ\n\n";
        it = foo.find('Z');
//cout << "hi?" ;
        assert(it != foo.end());
        assert(*it == 'Z');
        cout << cat(foo) << '\n';   // Should print abcdeZfghY
cout << "abcdeZfghY\n\n";
        foo.find('Z');
        cout << cat(foo) << '\n';   // Should print aZbcdefghY
cout << "aZbcdefghY\n\n";
        foo.find('Z');
        cout << cat(foo) << '\n';   // Should print ZabcdefghY
cout << "ZabcdefghY\n\n";
it = foo.find('w');
assert(it == foo.end());


        return 0;
    }
