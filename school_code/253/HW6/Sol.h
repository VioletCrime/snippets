#ifndef SOL_H_INCLUDED
#define SOL_H_INCLUDED

#include <iostream>
#include <limits.h> //Used to identify maximum list size
#include <cassert>

template <class T, int find_val = 1, class C = std::less<T> >
class Sol{

	public:
		typedef unsigned int size_type; //typedef'ing class variable types
		typedef T key_type; 
		typedef T value_type; 

	private:

		struct node{ //Used as 'links' in the chain that is the doubly linked list
			T data;
			node *prev;
			node *next;
		};

		node *head, *tail; //The head and tail of the dll
		int find_promote_value;	//How much should find() shift a value?
		C fnctor;

	public:
		class iterator{ //========================== ITERATOR SUBCLASS ================//
			private:
				node *no;  //The node the iterator points to
				friend class Sol; //besties; let Sol use ctor(node)
				iterator(node *p) : no(p){ //ctor(node)
				}
			public:
				iterator() : no(0){ //default ctor
				}

				iterator(const iterator &it) : no(it.no){ //ctor
				}
				
				~iterator(){ //dtor()
				}

				iterator operator++(){ //Preincrement
					no = no->next;
					return *this;
				}

				iterator operator++(int){ //Postincrement
					iterator temp = *this;
					++*this;
					return temp;
				}

				iterator operator--(){  //Predecrement
					no = no->prev;
					return *this;
				}

				iterator operator--(int){  //Postdecriment
					iterator temp = *this;
					--*this;
					return temp;
				}
		
				T operator*(){ //Indirection
					return no->data;
				}

				T *operator->(){ //whatever the hell this is good for...
					return &no->data;
				}

				bool operator==(const iterator &rhs){ //Equivalency checker
					return no == rhs.no;
				}

				bool operator!=(const iterator &rhs){ //Inequivalency... if that's a word...
					return !(*this == rhs);
				}		

				void operator=(const iterator &rhs){ //Assignment operator
					no = rhs.no;
				}
		}; //============================ END ITERATOR SUBCLASS =========================//


		Sol() : head(0), tail(0), find_promote_value(find_val){ //default ctor
		}

		Sol(const Sol &rhs) : head(0), tail(0), 
find_promote_value(rhs.find_promote_value), fnctor(rhs.fnctor){ //cctor
			node *n = rhs.head; //Get the head of the linked list to copy
			while (n != NULL){ //so long as the node isn't null
				T dat = n->data; //retrieve the node's data
				insert(dat); //insert the data into 'this' linked list
				n = n->next; //increment the node to the next spot.
			}
		}

		template<class inputIterator>
		Sol(inputIterator ita, inputIterator itb){ //Two-iterator ctor
            if(ita != itb){ //Base case insertion (set initial variables)
            	node *n=new node;
            	n->data = *ita;
            	head=n;
            	tail=n;
            	++ita;
			}
			while (ita != itb){ //finish the job
				node *n = new node;
				n->next = NULL;
				n->prev = tail;
				n->data = *ita;
				tail->next = n;
				tail = n;
				++ita;
			}
		}

		~Sol(){ //dtor; actually does something for once!
			clear();
		}

		bool is_equivalent(const T &a, const key_type &b) const{ //THE functor calling method
			bool result = false;
			if (!fnctor(a, b) && !fnctor(b, a)){ //If a is not less than b and b is not...
				result = true;  //... less than a, we know a == b.
			}
			return result; //GOOOOO TELL IT ON THE MOUNTAAAAIN!!!!!
		}

		Sol<T> operator=(const Sol<T> &rhs){ //Assignment operator
			clear(); //Remove the old data first
			find_promote_value = rhs.find_promote_value; //Get the shift value
			fnctor = rhs.fnctor; //Get the functor class
			node *n = rhs.head;  //Starting at the head of the list to be copied
			while (n != NULL){ //Take it to 11!
				T dat = n->data;
				insert(dat); //insert() 'em all.
				n = n->next;
			}
			
			return *this;
		}

		iterator insert(T input){ //Standard input call
			node *n = new node; //You know you're going to have to delete() that, right?
			n->next = NULL; //Set the known values
			n->prev = tail; 
			n->data = input; 

			if(!tail){ //Empty list case
				head = n;
				tail = n;
			}
			else{ //List is not empty
				tail->next = n;
				tail = n;
			}

			return iterator(tail); //In this case, we know it's at the end of the list
		}

		iterator insert(iterator it, const key_type &k){ //Iterator inserter
			node *n = new node; //Make the node to be inserted
			n->data = k;

			int i = 0;
			for (iterator tst = begin(); tst != end(); ++tst){ //How many places in is the iterator?
				if (tst == it){
					break;
				}
				++i;
			}

			node *d = head;
			for (int j = 0; j < i; ++j){ //Get a node object to the iterator's index
				d = d->next;
			}

			d->prev->next = n; //Rearrange pointers around the new node 'n'
			n->prev = d->prev;
			d->prev = n;
			n->next = d;

			if (n->prev == NULL){
				head = n;
			}

			iterator result(n);//Make the resulting iterator
			return result; //RETURN DAT SHIT!
		}

		iterator find(const key_type &k){ //The dreaded find()!
			iterator it = begin(), result = end();
			int i = 0;
			for (; it != end(); ++it){
				if(is_equivalent(*it, k)){
					break;
				}
				i++;
			}
			
			if (it != NULL){
				node *n = head;
				for (int j = 0; j < i; ++j){
					n = n->next;
				}
				i = find_val;
				while ( i > 0 && n->prev != NULL){
					T dat = n->data;
					n->data = n->prev->data;
					n->prev->data = dat;
					n = n->prev;
					i--;
				}
				result = begin();
				while (!is_equivalent(*result, k)){
					++result;
				}
			}
			
			return result;
		}

		void clear(){  //Clear the list
			while (head!=NULL){ //So long as head points to SOMETHING
				node *n = head;
                head=head->next;
				delete n; //Iteratively delete the list
			}
			head = NULL; //Tidying up
			tail = NULL;
		}

		void dump(){ //Debugging output method; output every T seperated by 1 whitespace
			node *n = head;
			while (n){
				std::cout << n->data << " ";
				n = n->next;
			}
			std::cout << std::endl;
		}

		size_type max_size() const{ //How big can we make this bitch?
			return UINT_MAX; //climits.h to the rescue!
		}

		size_type size() const{ //How large is our linked list?
			size_type result = 0;
			for (node *n = head; n; n= n->next){ //iterate through, keepig count
				--result;
			}
			return result;
		}

		void remove (node *&v){ //node which removes a given node from the list
			if (v->next && v->prev){ //Node is neither the head nor tail
				node *p = v->prev;
				node *n = v->next;
				p->next = n;
				n->prev = p;
				delete v;
			}
			else if (v->next){ //Node is the head
				node *n = v->next;
				n->prev = NULL;
				delete v;
				head = n;
			}
			else if (v->prev){ //Node is the tail
				node *p = v->prev;
				p->next = NULL;
				delete v;
				tail = p;
			}
			else { //Node is the ONLY node in the list.
				clear();
			}
		}

		size_type count(const key_type &k) const{ //How many 'k' values i the list?
			size_type result = 0;
			node *n = head;
			while (n){
				if(is_equivalent(n->data, k)){ //call the comparision-functor-calling method
					result++;
				}
				n = n->next;
			}
			return result;
		}

		size_type erase(const key_type &k){ //Erases all instances of 'k' from the list
			size_type result = 0;
			node *n = head;
			while (n){
				node *v = n->next;
				if(is_equivalent(n->data, k)){ 
					remove(n);
					++result;
				}
					n = v;
			}
			return result;
		}

		void erase(iterator it){ //Erases the node the passed iterator points at
			int i = 0;
			for (iterator tst = begin(); tst != end(); ++tst){
				if (tst == it){
					break;
				}
				i++;
			}
			node *n = head;
			for (int j = 0; j < i; j++){
				n = n->next;
			}
			remove(n);
		}

		void erase(iterator ita, iterator itb){ //Erases the half-open range of nodes
			for (; ita != itb; ++ita){
				erase(ita);
			}
		}

		bool empty(){ //Take a guess what this does.
			return !(size() > 0);
		}

		iterator begin(){ //Get an iterator at the start of the list
			return iterator(head);
		}

		iterator begin() const{ //"" const "" "" ""
			const iterator result(head);
			return result;
		}

		iterator end(){ //Get a NULL iterator
			return iterator(NULL);
		}

		iterator end() const{ //"" const NULL ""
			const iterator result(NULL);
			return result;
		}


//=================SHIFT OPERATORS ========================/
		Sol<T> operator++(){ //Preincrement
			tail->next = head;
			head->prev = tail;
			head = head->prev;
			tail = tail->prev;
			head->prev = NULL;
			tail->next = NULL;
            
			return *this;
		}

		Sol<T> operator++(int){ //Postincrement
			return ++*this;
		}

		Sol<T> operator+(int j){ //Sol + N
			Sol<T> result(*this);
			for (int i = 0; i < j; i++){
				++result;
			}
			return result;
		}

		Sol<T> operator +=(int j){ // Sol +=n
			*this = *this + j;
			return *this + j;
		}

		Sol<T> operator--(){ //Predecriment
			tail->next = head;
			head->prev = tail;
			head = head->next;
			tail = tail->next;
			head->prev = NULL;
			tail->next = NULL;

			return *this;
		}

		Sol<T> operator--(int){ //Postdecriment
			return --*this;
		}

		Sol<T> operator-(int j){ //Sol - N
			Sol<T> result(*this);
			for (int i = 0; i < j; i++){
				--result;
			}
			return result;
		}

		Sol<T> operator-=(int j){ //Sol -=n
			*this = *this - j;
			return *this - j;
		}
}; //============================ END CLASS SOL<T> ===================================//

template<class T>
Sol<T> operator+(int i, Sol<T> S){ // N + Sol
	return S + i;
}

template<class T>
Sol<T> operator-(int i, Sol<T> S){ //N - Sol
	Sol<T> result = S - i;
	return result;
}

#endif
