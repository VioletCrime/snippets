#ifndef WINDOW_MANAGER_H
#define WINDOW_MANAGER_H

#include <gtkmm.h>
#include <string>
#include <sstream>
#include "pegBoard.h"
#include "ErrorWindow.h"
#include "peg.h"

class WindowManager : public Gtk::Window{

	public:
		WindowManager();
		~WindowManager();
		
	protected:
		//Class variables
		int first_x, first_y;
		bool first_click;
		std::string num_moves_firstPt;
		pegBoard pb;
  		Gtk::Button reset_button;
		Gtk::Button quit_button;
		peg btn_a2, btn_a3, btn_a4, btn_b2, btn_b3, btn_b4, btn_c0, btn_c1, btn_c2, btn_c3, btn_c4, btn_c5, btn_c6, btn_d0, btn_d1, btn_d2, btn_d3, btn_d4, btn_d5, btn_d6, btn_e0, btn_e1, btn_e2, btn_e3, btn_e4, btn_e5, btn_e6, btn_f2, btn_f3, btn_f4, btn_g2, btn_g3, btn_g4;
		Gtk::HBox h_box;
		Gtk::VBox v_box, main_box;
		Gtk::Grid gryd;
		Gtk::Label lbl;

		//Class methods
		void set_state(peg &, int, int);
 		void on_button_clicked(Glib::ustring);
		void on_quit_clicked();
		void on_reset_clicked();
		void build_gryd();
		void color_board();
		void color_board_i(peg &, int, int);
		
};

#endif 
