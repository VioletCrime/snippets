#include "WindowManager.h"

using namespace std;

WindowManager::WindowManager()
: h_box(false, 25), //Initialize some of the widgets
  v_box(true, 40),
  reset_button(" Reset"),
  quit_button("  Quit  ")
{
	lbl.set_text("Number of pegs on board: 32"); //Initial # pegs output
	btn_d3.set_color("empty"); //Set the center peg to 'empty'
	first_click = false; //Set state
	set_title("Peg Solitaire"); //Window title
	set_border_width(10); //Prettify
	set_resizable(false); //Can't resize

	build_gryd(); //Prepare the grid for packing
	h_box.pack_start(gryd); //pack the grid into h_box
	v_box.pack_start(reset_button); //Pack start and reset buttons in v_box
	v_box.pack_start(quit_button);
	h_box.pack_start(v_box);//Pack v_box into h_box
	main_box.pack_start(h_box);//Pack h_box into main_box
	main_box.pack_start(lbl); //Pack label into main_box
	add(main_box); //Throw main_box at the window

	show_all_children(); //Put it on the main viewer, ensign!

	//Grid button signal handlers
	btn_a2.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "0,2"));
	btn_a3.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "0,3"));
	btn_a4.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "0,4"));
	btn_b2.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "1,2"));
	btn_b3.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "1,3"));
	btn_b4.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "1,4"));
	btn_c0.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "2,0"));
	btn_c1.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "2,1"));
	btn_c2.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "2,2"));
	btn_c3.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "2,3"));
	btn_c4.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "2,4"));
	btn_c5.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "2,5"));
	btn_c6.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "2,6"));
	btn_d0.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "3,0"));
	btn_d1.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "3,1"));
	btn_d2.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "3,2"));
	btn_d3.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "3,3"));
	btn_d4.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "3,4"));
	btn_d5.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "3,5"));
	btn_d6.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "3,6"));
	btn_e0.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "4,0"));
	btn_e1.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "4,1"));
	btn_e2.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "4,2"));
	btn_e3.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "4,3"));
	btn_e4.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "4,4"));
	btn_e5.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "4,5"));
	btn_e6.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "4,6"));
	btn_f2.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "5,2"));
	btn_f3.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "5,3"));
	btn_f4.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "5,4"));
	btn_g2.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "6,2"));
	btn_g3.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "6,3"));
	btn_g4.get_btn().signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
              sigc::mem_fun(*this, &WindowManager::on_button_clicked), "6,4"));
	
	//Quit and Reset button signal handlers
	quit_button.signal_clicked().connect(sigc::mem_fun(*this, &WindowManager::on_quit_clicked));
	reset_button.signal_clicked().connect(sigc::mem_fun(*this, &WindowManager::on_reset_clicked));
}

//Destructor
WindowManager::~WindowManager(){
}

//Reset the board
void WindowManager::on_reset_clicked(){
	first_click = false; //Set state
	lbl.set_text("Number of pegs on board: 32"); //Set # pegs output
	pb.reset(); //Reset the pegBoard object
	color_board(); //color the pegs accordingly
}

//Method for the quit button when clicked
void WindowManager::on_quit_clicked(){
	hide();//adios!
}

//State-dependant method calls
void WindowManager::set_state(peg& p, int i, int j){
	if (!first_click){ //If the first click hasn't been made
		if(pb.prep_move(i, j)){ //Try to color the first peg green
			first_click = true; //Change state
			color_board(); //Color the board to reflect the change
		}
		else{//Initial coloring failed! Complain to user!
			ErrorWindow ew("What were you thinking?", "Error", "Must choose an initial marble");
			Gtk::Main::run(ew);
		}
	}
	else{ //The first click has already been made
		first_click = false; //One way or another, the move ends here.
		if(pb.is_same(i, j)){ //If user clicked on the same space twice, reset w/o complaint
			pb.undo_colors(false); //Undo the coloring (no move made)
			color_board(); //Update peg colors
		}
		else if(!pb.make_move(i, j)){ //Move could not be made: complain!
			pb.undo_colors(false); //Undo the coloring (no move made)
			color_board(); //Update peg colors
			ErrorWindow ew; //Prepare for complaint
			Gtk::Main::run(ew); //LET LOOSE THE JUICE!
		}
		else{ //The move was accepted!
			pb.undo_colors(true); //Undo the coloring (a move was made)
			color_board(); //Update peg colors
			stringstream ss; //Need this to convert into to string
			ss << pb.num_left(); //Put the number of pegs into the stringstream
			lbl.set_text("Number of pegs on board: " + ss.str()); //Build new string to display

			if(pb.num_left() == 1){ //DID WE WIN!?!?!?!?!
				ErrorWindow ew("WINNER!", "Good work!", "You have cleared the board!");
				Gtk::Main::run(ew);
				on_reset_clicked();
			}
		}
	}
}

//tl;dr: Which button was clicked? Call set_state() appropriately
void WindowManager::on_button_clicked(Glib::ustring input){
	if (input == "0,2"){
		set_state(btn_a2, 0, 2);
	}
	else if (input == "0,3"){
		set_state(btn_a3, 0, 3);
	}
	else if (input == "0,4"){
		set_state(btn_a4, 0, 4);
	}
	else if (input == "1,2"){
		set_state(btn_b2, 1, 2);
	}
	else if (input == "1,3"){
		set_state(btn_b3, 1, 3);
	}
	else if (input == "1,4"){
		set_state(btn_b4, 1, 4);
	}
	else if (input == "2,0"){
		set_state(btn_c0, 2, 0);
	}
	else if (input == "2,1"){
		set_state(btn_c1, 2, 1);
	}
	else if (input == "2,2"){
		set_state(btn_c2, 2, 2);
	}
	else if (input == "2,3"){
		set_state(btn_c3, 2, 3);
	}
	else if (input == "2,4"){
		set_state(btn_c4, 2, 4);
	}
	else if (input == "2,5"){
		set_state(btn_c5, 2, 5);
	}
	else if (input == "2,6"){
		set_state(btn_c6, 2, 6);
	}
	else if (input == "3,0"){
		set_state(btn_d0, 3, 0);
	}
	else if (input == "3,1"){
		set_state(btn_d1, 3, 1);
	}
	else if (input == "3,2"){
		set_state(btn_d2, 3, 2);
	}
	else if (input == "3,3"){
		set_state(btn_d3, 3, 3);
	}
	else if (input == "3,4"){
		set_state(btn_d4, 3, 4);
	}
	else if (input == "3,5"){
		set_state(btn_d5, 3, 5);
	}
	else if (input == "3,6"){
		set_state(btn_d6, 3, 6);
	}
	else if (input == "4,0"){
		set_state(btn_e0, 4, 0);
	}
	else if (input == "4,1"){
		set_state(btn_e1, 4, 1);
	}
	else if (input == "4,2"){
		set_state(btn_e2, 4, 2);
	}
	else if (input == "4,3"){
		set_state(btn_e3, 4, 3);
	}
	else if (input == "4,4"){
		set_state(btn_e4, 4, 4);
	}
	else if (input == "4,5"){
		set_state(btn_e5, 4, 5);
	}
	else if (input == "4,6"){
		set_state(btn_e6, 4, 6);
	}
	else if (input == "5,2"){
		set_state(btn_f2, 5, 2);
	}
	else if (input == "5,3"){
		set_state(btn_f3, 5, 3);
	}
	else if (input == "5,4"){
		set_state(btn_f4, 5, 4);
	}
	else if (input == "6,2"){
		set_state(btn_g2, 6, 2);
	}
	else if (input == "6,3"){
		set_state(btn_g3, 6, 3);
	}
	else if (input == "6,4"){
		set_state(btn_g4, 6, 4);
	}
}

//Build the grid; put the correct buttons in the correct place
void WindowManager::build_gryd(){
	gryd.attach(btn_a2.get_wdg(), 2, 0, 1, 1);
	gryd.attach(btn_a3.get_wdg(), 3, 0, 1, 1);
	gryd.attach(btn_a4.get_wdg(), 4, 0, 1, 1);
	gryd.attach(btn_b2.get_wdg(), 2, 1, 1, 1);
	gryd.attach(btn_b3.get_wdg(), 3, 1, 1, 1);
	gryd.attach(btn_b4.get_wdg(), 4, 1, 1, 1);
	gryd.attach(btn_c0.get_wdg(), 0, 2, 1, 1);
	gryd.attach(btn_c1.get_wdg(), 1, 2, 1, 1);
	gryd.attach(btn_c2.get_wdg(), 2, 2, 1, 1);
	gryd.attach(btn_c3.get_wdg(), 3, 2, 1, 1);
	gryd.attach(btn_c4.get_wdg(), 4, 2, 1, 1);
	gryd.attach(btn_c5.get_wdg(), 5, 2, 1, 1);
	gryd.attach(btn_c6.get_wdg(), 6, 2, 1, 1);
	gryd.attach(btn_d0.get_wdg(), 0, 3, 1, 1);
	gryd.attach(btn_d1.get_wdg(), 1, 3, 1, 1);
	gryd.attach(btn_d2.get_wdg(), 2, 3, 1, 1);
	gryd.attach(btn_d3.get_wdg(), 3, 3, 1, 1);
	gryd.attach(btn_d4.get_wdg(), 4, 3, 1, 1);
	gryd.attach(btn_d5.get_wdg(), 5, 3, 1, 1);
	gryd.attach(btn_d6.get_wdg(), 6, 3, 1, 1);
	gryd.attach(btn_e0.get_wdg(), 0, 4, 1, 1);
	gryd.attach(btn_e1.get_wdg(), 1, 4, 1, 1);
	gryd.attach(btn_e2.get_wdg(), 2, 4, 1, 1);
	gryd.attach(btn_e3.get_wdg(), 3, 4, 1, 1);
	gryd.attach(btn_e4.get_wdg(), 4, 4, 1, 1);
	gryd.attach(btn_e5.get_wdg(), 5, 4, 1, 1);
	gryd.attach(btn_e6.get_wdg(), 6, 4, 1, 1);
	gryd.attach(btn_f2.get_wdg(), 2, 5, 1, 1);
	gryd.attach(btn_f3.get_wdg(), 3, 5, 1, 1);
	gryd.attach(btn_f4.get_wdg(), 4, 5, 1, 1);
	gryd.attach(btn_g2.get_wdg(), 2, 6, 1, 1);
	gryd.attach(btn_g3.get_wdg(), 3, 6, 1, 1);
	gryd.attach(btn_g4.get_wdg(), 4, 6, 1, 1);
}

//Call color_board_i for each peg
void WindowManager::color_board(){
	color_board_i(btn_a2, 0, 2);
	color_board_i(btn_a3, 0, 3);
	color_board_i(btn_a4, 0, 4);
	color_board_i(btn_b2, 1, 2);
	color_board_i(btn_b3, 1, 3);
	color_board_i(btn_b4, 1, 4);
	color_board_i(btn_c0, 2, 0);
	color_board_i(btn_c1, 2, 1);
	color_board_i(btn_c2, 2, 2);
	color_board_i(btn_c3, 2, 3);
	color_board_i(btn_c4, 2, 4);
	color_board_i(btn_c5, 2, 5);
	color_board_i(btn_c6, 2, 6);
	color_board_i(btn_d0, 3, 0);
	color_board_i(btn_d1, 3, 1);
	color_board_i(btn_d2, 3, 2);
	color_board_i(btn_d3, 3, 3);
	color_board_i(btn_d4, 3, 4);
	color_board_i(btn_d5, 3, 5);
	color_board_i(btn_d6, 3, 6);
	color_board_i(btn_e0, 4, 0);
	color_board_i(btn_e1, 4, 1);
	color_board_i(btn_e2, 4, 2);
	color_board_i(btn_e3, 4, 3);
	color_board_i(btn_e4, 4, 4);
	color_board_i(btn_e5, 4, 5);
	color_board_i(btn_e6, 4, 6);
	color_board_i(btn_f2, 5, 2);
	color_board_i(btn_f3, 5, 3);
	color_board_i(btn_f4, 5, 4);
	color_board_i(btn_g2, 6, 2);
	color_board_i(btn_g3, 6, 3);
	color_board_i(btn_g4, 6, 4);
}

void WindowManager::color_board_i(peg &p, int i, int j){
	int test = pb.at(i, j); //What 'color' is the peg supposed to be
	if(test == 0){ //Trial and error: 0 = empty
		p.set_color("empty"); //Set the image appropriately
	}
	else if(test == 1){ //1 = black
		p.set_color("black");
	}
	else if(test == 2){ //2 = green
		p.set_color("green");
	}
	else if(test == 3){ // 3 = yellow
		p.set_color("yellow");
	}
	else if(test == 4){ // 4 = red
		p.set_color("red");
	}
}
