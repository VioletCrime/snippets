#ifndef PEG_BOARD_H
#define PEG_BOARD_H

#include <vector>
#include <iostream>

class pegBoard{
	public:
		pegBoard();
		~pegBoard();
		pegBoard(const pegBoard &);
		bool prep_move(int, int);
		bool make_move(int, int);
		bool is_same(int, int);
		void undo_colors(bool);
		void reset();
		int at(int i, int j);
		int num_left();

	protected:
		std::vector<std::vector<int> > board;
		int marbles_remaining, FromX, FromY;
		void build_board();
		bool isLegit(int, int);
		bool legit_move(int, int, int, int);
		void remove_center(int, int, int, int);

};

#endif
