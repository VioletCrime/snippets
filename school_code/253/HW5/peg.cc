#include "peg.h"

using namespace std;

//Default constructor
peg::peg() 
: blk("black.jpeg"), //Initialize color images
  red("red.jpeg"),
  ylw("yellow.jpeg"),
  grn("green.jpeg"),
  empty("empty.jpeg")
{
	set_color("black"); //Initial color should be black
}

//Desctructor
peg::~peg(){
}

//Time for a new color? This is the method FOR YOU!!!
void peg::set_color(string input){
	if (input == "black"){
		p_button.set_image(blk);
	}
	else if (input == "red"){
		p_button.set_image(red);
	}
	else if (input == "yellow"){
		p_button.set_image(ylw);
	}
	else if (input == "green"){
		p_button.set_image(grn);
	}
	else if (input == "empty"){
		p_button.set_image(empty);
	}

}

//Return the button as a 'Button'
Gtk::Button& peg::get_btn(){
	return p_button;
}

//Return the button as a 'Widget' o.O? Stupid GTK...
Gtk::Widget& peg::get_wdg(){
	return p_button;
}

