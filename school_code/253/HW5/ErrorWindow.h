#ifndef ERROR_WINDOW_H
#define ERROR_WINDOW_H

#include<gtkmm.h>
#include<string>

class ErrorWindow : public Gtk::Window{

	public:
		ErrorWindow();
		~ErrorWindow();
		ErrorWindow(std::string, std::string, std::string);

	protected:
		Gtk::Label errLabel;
		Gtk::Frame errFrame;
		Gtk::HBox textBox, ButtonBox;
		Gtk::VBox vBawks;
		Gtk::Button Okies;

		void on_button_Okies();

};
#endif
