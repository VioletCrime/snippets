#include "ErrorWindow.h"

using namespace std;

//Default constructor; the usual suspects (invalid move).
ErrorWindow::ErrorWindow()
: Okies("   OK   "), //Initializers
  errFrame("Error!"), //
  errLabel("The move you have attempted to make is invalid."),
  vBawks(false, 20),
  textBox(false, 20),
  ButtonBox(false, 10)
{
	errFrame.add(errLabel);//Add the label to the frame object
	set_title("What were you thinking?"); //Srsly
	set_border_width(10); //Prettify
	add(vBawks); //cram vBawks into the window
	vBawks.pack_start(textBox); //Add the textBox to vBawks
	textBox.pack_start(errFrame); //Jam the Frame into the text box
	vBawks.pack_start(ButtonBox); //Stuff the button box into vbawks
	ButtonBox.pack_end(Okies, Gtk::PACK_SHRINK); // Add the 'OK' button to the button box

	//OK button signal handler
	Okies.signal_clicked().connect(sigc::mem_fun(*this, &ErrorWindow::on_button_Okies));

	show_all_children(); //SHOW ME THE MONEY!
}

//Destructor
ErrorWindow::~ErrorWindow(){
}

//Custom constructor; passed strings for the title, frame, and message fields
ErrorWindow::ErrorWindow(string title, string frame, string message)
: Okies("  OK  "), //Pretty much as same as default constructor (ctrl-c ctrl-v)
  errFrame(frame),
  errLabel(message),
  vBawks(false, 20),
  textBox(false, 20),
  ButtonBox(false, 10)
{
	errFrame.add(errLabel);
	set_title(title);
	set_border_width(10);
	add(vBawks);
	vBawks.pack_start(textBox);
	textBox.pack_start(errFrame);
	vBawks.pack_start(ButtonBox);
	ButtonBox.pack_end(Okies, Gtk::PACK_SHRINK);

	Okies.signal_clicked().connect(sigc::mem_fun(*this, &ErrorWindow::on_button_Okies));

	show_all_children();
}

//Close the window on 'OK' button clicked
void ErrorWindow::on_button_Okies(){
	hide();
}
