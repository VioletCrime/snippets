#include "pegBoard.h"

using namespace std;

//Default constructor
pegBoard::pegBoard(){
	build_board(); //Make the 'board'
}

//Destructor; nothing to see here...
pegBoard::~pegBoard(){
}

//Copy constructor; never used
pegBoard::pegBoard(const pegBoard &pb) : board(pb.board), marbles_remaining(pb.marbles_remaining){
}

//Reset the board, you say?
void pegBoard::reset(){
	build_board(); //Make the board again...
}

//Builds the vector<vector<int> > that represents the board
void pegBoard::build_board(){ 
	marbles_remaining = 32; //There are 32 pegs, initially
	vector<vector<int> > vv; //Will become 'board' once built
	vector<int> v; //Used to push length-7 vectors onto 'vv'

	for (int i = 0; i < 7; i++){ //Make v 7-indices long
		v.push_back(1);
	}
	for (int i = 0; i < 7; i++){ //Make vv 7-indices deep
		vv.push_back(v);
	}

	board = vv; //There's a new sheriff in town

	board[0][0] = -1; //Cordon off the 2x2 empty blocks in each corner
	board[0][1] = -1; //CTRL-C -> CTRL-V FTW!
	board[0][5] = -1; //Unnecessary code, as it turns out... meh
	board[0][6] = -1;
	board[1][0] = -1;
	board[1][1] = -1;
	board[1][5] = -1;
	board[1][6] = -1;
	board[5][0] = -1;
	board[5][1] = -1;
	board[5][5] = -1;
	board[5][6] = -1;
	board[6][0] = -1;
	board[6][1] = -1;
	board[6][5] = -1;
	board[6][6] = -1;

	board[3][3] = 0; //Set center as the only open position

}

//Is the coordinate part of the board?
bool pegBoard::isLegit(int x, int y){
	bool result = true;
	if(x < 0 || x > 6 || y < 0 || y > 6){ //All values should be between 0 and 6
		result = false;
	}
	else if (x < 2 && y < 2){ //Is it in the top-left blocked area?
		result = false;
	}	
	else if (x > 4 && y < 2){ //How about the top-right?
		result = false;
	}
	else if (x < 2 && y > 4){ //bottom left 4x4?
		result = false;
	}
	else if (x > 4 && y > 4){ //Bottom right Triscut banana?
		result = false;
	}
	return result; //Inform the captain AT ONCE!
}

//First click detected; color the board appropriatly
bool pegBoard::prep_move(int fromX, int fromY){

	bool result = false; //Did the move 'take'?
	FromX = fromX; //Note the initial X coord for future reference
	FromY = fromY; //Ditto of Y coord

	if(board[fromX][fromY] == 1){ //The the initial space a peg?
		result = true; //This block must be entered for the move to 'take'
		board[fromX][fromY] = 2; //Color it green!

		if (isLegit(fromX+2, fromY)){ //Is there a move to the right?
			if (board[fromX + 2][fromY] == 0){ //If there is no peg 2 spaces right...
				if (board[fromX + 1][fromY] == 1){ //...and there IS a peg 1 space right...
					board[fromX + 1][fromY] = 3; //...color the next peg yellow...
					board[fromX + 2][fromY] = 4; //... and the following one red.
				}
			}
		}
		if (isLegit(fromX-2, fromY)){ //Is there a move to the left?
			if (board[fromX - 2][fromY] == 0){
				if (board[fromX - 1][fromY] == 1){
					board[fromX - 1][fromY] = 3;
					board[fromX - 2][fromY] = 4;
				}
			}
		}
		if (isLegit(fromX, fromY + 2)){ //Is there a move above?
			if (board[fromX][fromY + 2] == 0){
				if (board[fromX][fromY + 1] == 1){
					board[fromX][fromY + 1] = 3;
					board[fromX][fromY + 2] = 4;
				}
			}
		}
		if (isLegit(fromX, fromY - 2)){ //How about below?
			if (board[fromX][fromY - 2] == 0){
				if (board[fromX][fromY - 1] == 1){
					board[fromX][fromY - 1] = 3;
					board[fromX][fromY - 2] = 4;
				}
			}
		}
	}
	return result;
}

//Make the move.
bool pegBoard::make_move(int toX, int toY){
	bool result = false; //Did the move take?
	if(isLegit(FromX, FromY) && isLegit(toX, toY) && legit_move(FromX, FromY, toX, toY)){ //Is the move 
		result = true; //If it passed all those tests, the move is good to go
		board[FromX][FromY] = 0;//Empty space where we started
		board[toX][toY] = 1; //Put the peg at the finish line
		remove_center(FromX, FromY, toX, toY); //Empty the space we jumped over
		marbles_remaining--; //One less peg to assassinate
	}
	return result; //Jesus will want to hear about this...
}

//Remove the unnecessary colors
void pegBoard::undo_colors(bool moved){
	if(!moved){ //If a move was not made...
		board[FromX][FromY] = 1; //Put a peg at the start point
	}

	if(isLegit(FromX+1, FromY)){ //Is the slot to the right legit?
		if(board[FromX+1][FromY] == 3){ //Is it colored yellow?
			board[FromX+1][FromY] = 1; //Return it to black
		}
	}

	if(isLegit(FromX+2, FromY)){ //Is the space two slots tot he right legit?
		if(board[FromX+2][FromY] == 4){ //Is it colored red?
			board[FromX+2][FromY] = 0; //Return it to black
		}
	}

	if(isLegit(FromX-1, FromY)){ //1 slot left?
		if(board[FromX-1][FromY] == 3){
			board[FromX-1][FromY] = 1;
		}
	}

	if(isLegit(FromX-2, FromY)){ //2 slots left?
		if(board[FromX-2][FromY] == 4){
			board[FromX-2][FromY] = 0;
		}
	}

	if(isLegit(FromX, FromY+1)){ //1 slot above
		if(board[FromX][FromY+1] == 3){
			board[FromX][FromY+1] = 1;
		}
	}

	if(isLegit(FromX, FromY+2)){ //2 slots above
		if(board[FromX][FromY+2] == 4){
			board[FromX][FromY+2] = 0;
		}
	}

	if(isLegit(FromX, FromY-1)){ //1 slot below?
		if(board[FromX][FromY-1] == 3){
			board[FromX][FromY-1] = 1;
		}
	}

	if(isLegit(FromX, FromY-2)){ //2 slots below?
		if(board[FromX][FromY-2] == 4){
			board[FromX][FromY-2] = 0;
		}
	}
}

//Is the move legal? Return true if so.
bool pegBoard::legit_move(int fX, int fY, int tX, int tY){
	bool result = false; //Default answer...
	if(fX == tX){
		if (fY == tY - 2){//Is the end space 2 above start?
			if(board[tX][tY] == 4){  //Is the end space red?
				result = true; //Yep, it's legit
			}
		}
		else if (fY == tY + 2){//Is the end space 2 below start?
			if(board[tX][tY] == 4){
				result = true;
			}
		}
	}
	else if (fY == tY){
		if (fX == tX - 2){//Is the end space 2 to the right of start?
			if(board[tX][tY] == 4){
				result = true;
			}
		}
		else if (fX == tX + 2){//Is the end space 2 tot he left of start?
			if(board[tX][tY] == 4){
				result = true;
			}
		}
	}
	return result;
}

//Removes the marble that was jumped over
void pegBoard::remove_center(int fX, int fY,int tX,int tY){
	if(fX == tX + 2){//Was the move to the left?
		board[tX+1][tY] = 0; //Remove peg 1 to left
	}
	else if (fX == tX - 2){ //Move was to the right?
		board[tX-1][tY] = 0;
	}
	else if (fY == tY + 2){ //Move was down?
		board[tX][tY+1] = 0;
	}
	else if (fY == tY - 2){ //Move was up?
		board[tX][tY-1] = 0;
	}
}

//Get the value from 'board' at index i, j
int pegBoard::at(int i, int j){
	return board[i][j];
}

//How many pegs / marbles remain?
int pegBoard::num_left(){
	return marbles_remaining;
}

//Did the user click on the same place twice?
bool pegBoard::is_same(int i, int j){
	bool result = false;
	if(i == FromX && j == FromY){
		result = true;
	}
	return result;
}
