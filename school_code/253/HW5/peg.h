#ifndef PEG_H
#define PEG_H

#include <gtkmm.h>
#include <string>

class peg{

	public:
		peg();
		~peg();
		void set_color(std::string);
		Gtk::Button& get_btn();
		Gtk::Widget& get_wdg();

	protected:
		Gtk::Button p_button;
		Gtk::Image blk, red, ylw, grn, empty;

};
#endif
