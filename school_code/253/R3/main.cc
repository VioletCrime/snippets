#include <iostream>

using namespace std;

#include "foo.h"
#include "bar.h"

int main() {
	cout << "Hello from main()\n";
	foo();
	bar();
	return 0;
}
