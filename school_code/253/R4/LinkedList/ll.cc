#include <cstdlib>
#include <iostream>
#include "ll.h" 

using namespace std;

LinkedList::List::LinkedList (unsigned int uiData, Link *pNext) {
	m_uiData = uiData;
	m_pNext = pNext;
}

LinkedList::LinkedList() {
	m_pHead = NULL;
}

bool LinkedList::Insert (unsigned int uiData) {
	Link* new_link = new Link;

	new_link->Initialize (uiData, m_pHead);
	m_pHead = new_link;

	return true;
}

bool LinkedList::Delete (unsigned int *pData) {

	bool ret_val = false;

	if (m_pHead != NULL) {
		Link *temp = m_pHead;
		m_pHead = m_pHead->m_pNext;
		*pData = temp->m_uiData;

		ret_val = true;
	}

	return ret_val;
}

void LinkedList::Print(){
	if (m_pHead != NULL) {
		Link *temp = m_pHead;
		cout << temp->m_uiData << " ";
		while (temp->m_pNext != NULL){
			temp = temp->m_pNext;
			cout << temp->m_uiData << " ";
		}
		cout << endl;
	}
}
