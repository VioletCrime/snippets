#ifndef _LINKED_LIST_H_
#define _LINKED_LIST_H_ 

class LinkedList {
    public:
	LinkedList();
	LinkedList(unsigned int uiData, Link *pNext);
	bool Insert(unsigned int uiData);
	bool Delete(unsigned int *pData);
	void Print();

    private:
	struct Link {
		unsigned int m_uiData;
		Link* m_pNext;
		void Initialize (unsigned int uiData, Link *pNext);
	} *m_pHead;

};

#endif /* _LINKED_LIST_H_ */ 
