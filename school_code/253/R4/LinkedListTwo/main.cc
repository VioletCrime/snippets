#include <iostream>
#include "ll.h" 

using namespace std;

int main() {

  LinkedList list;
  bool done = false;

  while (!done) {
    unsigned int i;
    int option;

    cout << "Choose your operation" << endl;
    cout << "1. insert\t2. delete\t3. print\t4. exit" << endl;

    cin >> option;

    switch (option) {
      case 1:
          cout << "Enter the number to insert" << endl;
          cin >> i;
          list.Insert (i);
        break;
      case 2:
          if (list.Delete (i))
            cout << "Deleted " << i << endl;
          else
           cout << "No numbers in the list" << endl;
        break;
	 case 3:
			list.Print();
			break;
      case 4:
        done = true;
        break;
      default:
        break;
    }
  }

  return 0;
} 
