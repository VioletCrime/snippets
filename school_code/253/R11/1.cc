#include <iostream>
#include <map>
#include <iomanip>
#include <string>

using namespace std;

template <class T>
class BarGraph {
    typedef map<T, unsigned> Con;
    Con data;
  public:
    void operator+=(const T &datum) {
        data[datum]++;
    }
	/*friend std::ostream &operator<<(std::ostream &os, const BarGraph &bg){
		string result;
		for (typename Con::iterator it = bg.data.begin(); it != bg.data.end(); ++it){
			string starz(it->second, '*');
			result += it->first + ' ' + starz;
		}
		return os << result;
	}*/

	Con getData(){
		return data;
	}

    void dump() {
	for (typename Con::iterator it=data.begin(); it!=data.end(); ++it)
	    cout << right << setw(10) << it->first << ' '
		 << string(it->second, '*') << '\n';
        cout << '\n';
    }
};

	template <class T>
	ostream &operator<<(ostream &os, BarGraph<T> &bg){
		os.clear();
		typedef map<T, unsigned> Con;
		Con data = bg.getData();	
		for (typename Con::iterator it = data.begin(); it != data.end(); ++it){
			os << right << setw(10) << it->first << ' ' << string(it->second, '*') << '\n';
		}
		os << '\n';
		return os;
	}

/*template <>
class BarGraph<char> {
    unsigned false_count, true_count;
  public:
    BarGraph() : false_count(0), true_count(0) {
    }
    void operator+=(bool datum) {
	datum ? true_count++ : false_count++;
    }
    void dump() {
        cout << "     false " << string(false_count, '*') << "\n"
                "      true " << string(true_count,  '*') << "\n\n";
    }
};*/

int main() {
	BarGraph<int> alpha;
	alpha += 12;
	alpha += 6;
	alpha += 4;
	alpha += 6;
	cout << alpha;

	BarGraph<double> beta;
	beta += 3.14;
	beta += 2.71828;
	beta += 6.023e23;
	beta += 2.71828;
	beta.dump();

	BarGraph<bool> gamma;
	gamma += false;
	gamma += true;
	gamma += false;
	gamma += true;
	gamma += true;
	gamma.dump();

	BarGraph<char> delta;
	delta += 'G';
	delta += 'e';
	delta += 'o';
	delta += 'f';
	delta += 'f';
	delta.dump();
}
