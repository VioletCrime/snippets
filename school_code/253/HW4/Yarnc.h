#ifndef YARNC_H_INCLUDED
#define YARNC_H_INCLUDED

#include "Yarn.h"
#include <string>

class Yarnc: public Yarn{
public:
	Yarnc();
	Yarnc(std::string &);
	~Yarnc();
	Yarnc(const Yarnc &);

	std::string name();
	void clear();	
	std::string str();
	std::string raw();
	int size();
	
	char &operator[](int) const;
	Yarnc operator%(Yarn);
private:
	static void convert(string);
	static string restore();
	std::string yarnweh;
	int leng;
};

std::ostream &operator<<(std::ostream &, const Yarnc &);

#endif
