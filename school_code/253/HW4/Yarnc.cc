#include "Yarnc.h"

using namespace std;

Yarnc::Yarnc(){
	yarnweh.clear();
	leng = 0;
}

Yarnc::Yarnc(string input){
	convert(input);
	leng = input.size();
}

Yarnc::~Yarnc(){}

Yarnc::Yarnc(const Yarnc &rhs) : yarnweh(rhs.yarnweh), leng(rhs.leng){
}

string name(){
	return "Yarnc";
}

void clear(){
	yarnweh.clear();
	leng = 0;
}

string str(){
	return restore();
}

string raw(){
	return yarnweh;
}

int size(){
	return leng;
}

char &operator[] (int i) const{
	string temp = restore();
	return temp[i];
}

Yarnc operator%(Yarn rhs){
	string temp = rhs.str(), result = "";
	int a, b;
	if (yarnweh.size() > temp.size()){
		a = yarnweh.size();
		b = temp.size();

		for (int i = 0; i < a; i++){
			result += yarnweh[i];
			if (i < b){
				result += temp[i];
			}
		}
	}
	else{
		a = temp.size();
		b = yarnweh.size();
		for (int i = 0; i < a; i++){
			result += temp[i];
			if (i < b){
				result += yarnweh[i];
			}
		}
	}
	return Yarnc(result);
}

void convert(string inpt){
	string result = "";
	for(int i = 0; i < inpt.size()-1; i++){
		if (inpt[i] == inpt[i+1]){
			int j = 2;
			while (inpt[i] == inpt[i+j]){
				j++;
			}
			result = result + "<" + j + ">" +inpt[i];
			i += (j - 1);
			
		}
		else if (inpt[1] == '<'){
			int j = 1;
			while (inpt[i+j] == '<'){
				j++;
			}
			result = result + "<" + j + "><";
			i += (j - 1);
		}
		else{
			result += inpt[i];
		}
	}
	yarnweh = result;
}

string restore(){
	string result = "";
	for(int i = 0; i < yarnweh.size(); i++){
		if(yarnweh[i] == '<'){
			int j = 1;
			while (yarnweh[i + j + 1] != '>'){
				j++;
			}
		}
		else{
			result += yarnweh[i];
		}
	}
}

ostream &operator<< (ostream &stream, const Yarnc &lhs) {
	return stream << lhs.raw();
}
