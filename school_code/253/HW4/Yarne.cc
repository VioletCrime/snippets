#include "Yarne.h"

using namespace std;

Yarne::Yarne(){
	yarnweh.clear();
	leng = 0;
}

Yarne::Yarne(string input){
	convert(input);
	leng = input.size();
}

Yarne::~Yarne(){}

Yarne::Yarne(const Yarne &rhs) : yarnweh(rhs.yarnweh), leng(rhs.leng){
}

string name(){
	return "Yarne";
}

void clear(){
	yarnweh.clear();
	leng = 0;
}

string str(){
	return restore();
}

string raw(){
	return yarnweh;
}

int size(){
	return leng;
}

char &operator[](int i) const{
	string temp = restore();
	return temp[i];
}

Yarne operator%(Yarn rhs){
	string temp = rhs.str(), result = "";
	int a, b;
	if (yarnweh.size() > temp.size()){
		a = yarnweh.size();
		b = temp.size();

		for (int i = 0; i < a; i++){
			result += yarnweh[i];
			if (i < b){
				result += temp[i];
			}
		}
	}
	else{
		a = temp.size();
		b = yarnweh.size();
		for (int i = 0; i < a; i++){
			result += temp[i];
			if (i < b){
				result += yarnweh[i];
			}
		}
	}
	return Yarne(result);
}

void convert(string inpt){
	int y = inpt.size();
	string result = "";
	for (int i = 0; i < y; i++){
		char c = inpt[i];
		char d = c ^ 0x01;
		result += d;
	}
	yarnweh = result;
}

string restore(){
	string result = "";
	for (int i = 0; i < yarnweh.size(); i++){
		char c = yarnweh[i];
		char d = c ^ 0x01;
		result += d;
	}
	return result;
}

ostream &operator<< (ostream &stream, const Yarne &lhs) {
	return stream << lhs.raw();
}
