/*Kyle Smith (cs: smithkyl)
 *Yarnc.cc, written for CS253 Spring '12, HW4
 *version 0.2, 3/19/2012
*/

#include "Yarnc.h"

using namespace std;


//Default constructor
Yarnc::Yarnc(){ 
	yarnweh.clear();
	leng = 0;
}


//Input constructor
Yarnc::Yarnc(const string &input){
	convert(input, yarnweh);
	leng = input.size();
}


//Destructor
Yarnc::~Yarnc(){} 


//Copy constructor
Yarnc::Yarnc(const Yarnc &rhs) : yarnweh(rhs.yarnweh), leng(rhs.leng){ 
}


//Return yarn derived class type: 'Yarnc'
string Yarnc::name() const{ 
	return "Yarnc";
}


//Reset the object to hold nothing
void Yarnc::clear(){ 
	yarnweh.clear();
	leng = 0;
}


//Return the plain-text version of the string member variable
string Yarnc::str() const{ 
	return restore(yarnweh);
}


//Return compressed version of the string
string Yarnc::raw() const{ 
	return yarnweh;
}


//Return size of string
unsigned int Yarnc::size() const{ 
	return leng;
}


//Return index 'i' of the plain-text string
char Yarnc::operator[] (unsigned int i) const { 
	string temp = restore(yarnweh);
	return temp[i];
}


//Interleave two Yarn objects, return object of lhs-type
Yarnc Yarnc::operator%(Yarn &rhs){ 
  string temp = rhs.str(), temp2 = restore(yarnweh), result = "";
  int a = temp.size(), b = temp2.size(), max;
	if (temp2.size() > temp.size()){
	  max = temp2.size();
	}
	else{
	  max = temp.size();
	}
	for (int i = 0; i < max; i++){
	  if (i < a){
	    result += temp2[i];
	  }
	  if (i < b){
	    result += temp[i];
	  }
	}
	return Yarnc(result);
}


//Set 'this' object to hold 'rhs' in compressed form
Yarnc& Yarnc::operator=(const std::string &rhs){
	convert(rhs, yarnweh);
	leng = rhs.size();
	return *this;
}

//Set 'this' object to hold rhs's plain-text string in compressed form
Yarnc& Yarnc::operator=(const Yarn &rhs){
	string temp16 = rhs.str();
	leng = temp16.size();
	convert(temp16, yarnweh);
	return *this;
}


//Convert the input string to a compressed version, set member variable.
void Yarnc::convert(string inpt, string &changeMe){
	string result = "";
	for(unsigned int i = 0; i < inpt.size(); i++){ //For the length of string
	  if (inpt[i] == inpt[i+1]){ //If we see the same letter more than once
	    int j = 2; //placeholder
	    while (inpt[i] == inpt[i+j] && i < inpt.size()){ //count how many times we see the char
				j++;
			}
		stringstream ss; //Used to convert the integer 'j' to a string
		ss << j;
		string tempJ = ss.str(); 

		result += '<' + tempJ + '>' + inpt[i]; //Write the compressed representation
			i += (j - 1);//Modify i past the set of chars
			
		}
	  else if (inpt[i] == '<'){ //If we see a '<'
			int j = 1;
			while (inpt[i+j] == '<' && i < inpt.size()){ //count how many '<'s we see
				j++;
			}

			stringstream ss;
			ss << j;
			string tempJ = ss.str();

			result += '<' + tempJ + "><"; //Write the compressed representation
			i += (j - 1);//Modify 'i' past these '<'s
		}
		else{
		  result += inpt[i]; //If it's just another old letter, just write it
		}
	}
	changeMe = result; //Write the compressed string to the object variable
}


//Return the string in its original state
string Yarnc::restore(const string &useMe){ 
	string result = "";
	for(unsigned int i = 0; i < useMe.size(); i++){ 
	  if(useMe[i] == '<'){ //Look for '<'s
			int j = 1;
			while (useMe[i + j + 1] != '>'){//Count how many chars until '>'
				j++;
			}
			string toNum = useMe.substr(i+1, j); //Isolate number
			int num = atoi(toNum.c_str()); //Convert to int
			for (int k = 0; k < num; k++){ // Write the char n times
			      result += useMe[i+j+2];
			    }
			    i += j + 2; //Modify 'i' past the compression text
			
		}
			  else{ //If there's nothing to see, just write verbatim.
			result += useMe[i];
		}
	}
	return result;
}


//Insert a Yarnc object into the provided ostream
ostream &operator<< (ostream &stream, const Yarnc &lhs) { 
	return stream << lhs.str();
}
