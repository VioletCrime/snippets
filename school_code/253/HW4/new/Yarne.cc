/*Kyle Smith (cs: smithkyl)
 *Yarne.cc, written for CS253 Spring '12, HW4
 *version 0.2, 3/19/2012
*/

#include "Yarne.h"

using namespace std;


//Default constructor
Yarne::Yarne(){
	yarnweh.clear();
	leng = 0;
}


//Input constructor
Yarne::Yarne(const string &input){ 
	convert(input, yarnweh);
	leng = input.size();
}


//Destructor
Yarne::~Yarne(){} 


//Copy constructor
Yarne::Yarne(const Yarne &rhs) : yarnweh(rhs.yarnweh), leng(rhs.leng){ 
}


//Return yarn derived class type: 'Yarne'
string Yarne::name() const { 
	return "Yarne";
}


//Reset the object to hold nothing
void Yarne::clear(){ 
	yarnweh.clear();
	leng = 0;
}


//Return plain-text version of the string member variable
string Yarne::str() const{ 
	return restore(yarnweh);
}


//Return the encrypted version of the string
string Yarne::raw()const { 
	return yarnweh;
}


//Return the size of the string
unsigned int Yarne::size() const { 
	return leng;
}


//Return index 'i' of the plain-text string
char Yarne::operator[](unsigned int i) const { 
	string temp = restore(yarnweh);
	return temp[i];
}


//Interleave two Yarn objects, return object of lhs-type
Yarne Yarne::operator%(Yarn &rhs){
  string temp = rhs.str(), temp2 = restore(yarnweh), result = "";
  int a = temp.size(), b = temp2.size(), max;
	if (temp2.size() > temp.size()){
	  max = temp2.size();
	}
	else{
	  max = temp.size();
	}
	for (int i = 0; i < max; i++){
	  if (i < a){
	    result += temp2[i];
	  }
	  if (i < b){
	    result += temp[i];
	  }
	}
	return Yarne(result);
}


//Set 'this' object to hold 'rhs' in encrypted form
Yarne& Yarne::operator=(const std::string &rhs){
	convert(rhs, yarnweh);
	leng = rhs.size();
	return *this;
}


//Set 'this' object to hold rhs's plain-text string in encrypted form
Yarne& Yarne::operator=(const Yarn &rhs){
	string temp16 = rhs.str();
	leng = temp16.size();
	convert(temp16, yarnweh);
	return *this;
}


//Convert the input string to an encrypted form, set member variable
void Yarne::convert(string inpt, string& changeMe){
	string result = "";
	for (unsigned int i = 0; i < inpt.size(); i++){//For each char in the input string
		char c = inpt[i];
		char d = c ^ 0x01; //Flip its LSB so the Ruskies can't read it!!! o.O
		result += d;
	}
	changeMe = result; //Set it as the object's straing variable
}


//Return string member variable in its plain-text form
string Yarne::restore(const string &useMe){
	string result = "";
	for (unsigned int i = 0; i < useMe.size(); i++){//For each character in a string
		char c = useMe[i];
		char d = c ^ 0x01; //Flip its LSB back to original
		result += d;
	}
	return result; //Return it for further use
}


//Insert a Yarne object into the provided ostream
ostream &operator<< (ostream &stream, const Yarne &lhs) { //ostream handler
	return stream << lhs.str();
}
