/*Kyle Smith (cs: smithkyl)
 *Yarne.h, written for CS253 Spring '12, HW4
 *version 0.2, 3/19/2012
*/

#ifndef YARNE_H_INCLUDED
#define YARNE_H_INCLUDED

#include "Yarn.h"
#include <string>

class Yarne : public Yarn{
public:
	Yarne();
	Yarne(const std::string &);
	~Yarne();
	Yarne(const Yarne &);

	std::string name() const;
	void clear();	
	std::string str() const;
	std::string raw() const;
	unsigned int size() const;
	
	
	char operator[](unsigned int) const;
	Yarne operator%(Yarn &);
	Yarne &operator=(const Yarn &);
      Yarne &operator=(const std::string &);

private:
	static void convert(std::string, std::string &);
	static std::string restore(const std::string &);
	std::string yarnweh;
	unsigned int leng;
};

std::ostream &operator<<(std::ostream &, const Yarne &);

#endif
