#include "Yarne.h"
    #include "Yarnc.h"
    #include <iostream>
    #include <cassert>
    using namespace std;

    int main() {
        Yarne e("Bing");
        Yarn *yp = &e, &yr = e;

        assert(yp->raw() == "Chof");
        assert(yp->str() == "Bing");
        assert(yr[2] == 'n');

        Yarnc c("Yahoo! < Goooooooooooooogle");

        assert(c.raw() == "Yah<2>o! <1>< G<14>ogle");
        assert(c.str() == "Yahoo! < Goooooooooooooogle");
        assert(c.size() == 27);
        assert(c[26] == 'e');

        yp = &e; assert(yp->name() == "Yarne"); assert(yp->size() == 4);
        yp = &c; assert(yp->name() == "Yarnc"); assert(yp->size() == 27);

        e = Yarnc("aallpphhaa");           // assign a Yarnc to a Yarne
        assert(e.str() == "aallpphhaa");
        assert(e.raw() == "``mmqqii``");
        c = Yarne("bbeettaa");		   // assign a Yarne to a Yarnc
        assert(c.str() == "bbeettaa");
        assert(c.raw() == "<2>b<2>e<2>t<2>a");

        e = "Jv safn agaefrBGNIGpormes";
        c = "aai  ielnug o EINN rgamr.";
	Yarne q = e%c;
        cout << q << endl;

    }
