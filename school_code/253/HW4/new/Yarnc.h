/*Kyle Smith (cs: smithkyl)
 *Yarnc.h, written for CS253 Spring '12, HW4
 *version 0.2, 3/19/2012
*/

#ifndef YARNC_H_INCLUDED
#define YARNC_H_INCLUDED

#include "Yarn.h"
#include <string>
#include <sstream>
#include <stdlib.h>

class Yarnc : public Yarn{
public:
	Yarnc();
	Yarnc(const std::string &);
	~Yarnc();
	Yarnc(const Yarnc &);

	std::string name() const;
	void clear();	
	std::string str() const;
	std::string raw() const;
	unsigned int size() const;
	
	char operator[](unsigned int) const;
	Yarnc operator%(Yarn &);
	Yarnc &operator=(const Yarn &);
      Yarnc &operator=(const std::string &);

private:
	static void convert(std::string, std::string &);
	static std::string restore(const std::string &);
	std::string yarnweh;
	unsigned int leng;
};

std::ostream &operator<<(std::ostream &, const Yarnc &);

#endif
