#ifndef YARNE_H_INCLUDED
#define YARNE_H_INCLUDED

#include "Yarn.h"
#include <string>

class Yarne: public Yarn{
public:
	Yarne();
	Yarne(std::string &);
	~Yarne();
	Yarne(const Yarne &);

	std::string name();
	void clear();	
	std::string str();
	std::string raw();
	int size();
	
	char operator[](int) const;
	Yarne &operator%(Yarn);
private:
	static void convert(string);
	static string restore();
	std::string yarnweh;
	int leng;
};

std::ostream &operator<<(std::ostream &, const Yarne &);

#endif
