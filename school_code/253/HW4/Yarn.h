 #ifndef Yarn_h_INCLUDED
    #define Yarn_h_INCLUDED

    #include <string>

    class Yarn {
      public:
        // Default ctor
        // Copy ctor
        // Ctor from string
        virtual Yarn & operator=(const Yarn &) = 0;
        virtual Yarn & operator=(const std::string &) = 0;
        // operator%
        virtual std::string name() const = 0;
        virtual void clear() = 0;
        virtual unsigned int size() const = 0;
        virtual std::string str() const = 0;
        virtual std::string raw() const = 0;
        virtual char operator[](unsigned int) const = 0;
    };

    // std::ostream & operator<<(std::ostream &, const Yarn &); 

    #endif /* Yarn_h_INCLUDED */
