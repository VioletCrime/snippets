#include <iostream>

using namespace std;

class Bar {
    public:
	static void *operator new(size_t) {
	    if (freelist == NULL) {
		char *p = new char[pagesize];
		for (unsigned i=0; i<pagesize/sizeof(Bar); i+=sizeof(Bar)) {
		    thing *t = (thing *) (p+i);
		    t->next = freelist;
		    freelist = t;
		}
	    }
	    void *result = freelist;	// Get the first node
	    freelist = freelist->next;	// Remove it from the free list
	    return result;
	}

	static void operator delete(void *p) {
	    thing *t = (thing *) p;
	    t->next = freelist;
	    freelist = t;
	}

    private:
	int datum;

	struct thing { thing *next; };
	static struct thing *freelist;
	static const int pagesize=8192;

};

struct Bar::thing *Bar::freelist;

int main() {
	for (int h = 0; h < 100000; h++){
	Bar *r = new Bar;
	Bar *s = new Bar;
	cout << "r=" << r << endl;
	cout << "s=" << s << endl;
	delete r;
	delete s;
	}
	return 0;
}
