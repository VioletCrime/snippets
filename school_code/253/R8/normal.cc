#include <iostream>

using namespace std;

class Foo {
    public:
	int datum;
};

int main() {
	Foo *p = new Foo;
	Foo *q = new Foo;
	cout << "p=" << p << " size = " << sizeof(*p) << endl;
	cout << "q=" << q << endl;
	delete p;
	delete q;
	return 0;
}
