/*
*Imdb.cc: A simple class used to scrape info from the IMDb (imdb.com)
*Written by Kyle Smith (smithkyl) for CS253, Spring '12
*Started 2/23/12
*/

#include "Imdb.h"

using namespace std; //Forgot this once... never again.

//Default constructor; creates empty Imdb object.
Imdb::Imdb(){
	dataList.clear();
	errFlag = true;
}

//Internal constructor used to copy Imdb objects
Imdb::Imdb(vector<string> vec, bool flag){
	dataList = vec;
	errFlag = flag;
}

//Public constructor which takes the object's first tag as an argument
Imdb::Imdb(const string &tag){
	dataList.clear();
	errFlag = true;
	read(tag);
}

Imdb::~Imdb(){
	dataList.clear();
}

//Copy constructor
Imdb::Imdb(const Imdb &rhs) : dataList(rhs.dataList), errFlag(rhs.errFlag){
	
}

//Assignment 'operator=' method
Imdb &Imdb::operator=(const Imdb &rhs){
	dataList = rhs.dataList;
	errFlag = rhs.errFlag;
	return *this;
}

//Returns the state of the error flag
Imdb::operator bool() const{
	return errFlag;
}

//Clear method; empties the vector of strings and resets the error flag
void Imdb::clear(){
	dataList.clear();
	errFlag = true;
}

//Returns the number of lines stored by the object's vector of strings
int Imdb::size() const{
	int result = 0;
	if (dataList.size() > 0){
		result = dataList.size() - 1;
	}
	return result;
}

//Returns the string at the 'n+1'th line of the vector of strings
string Imdb::operator[](int n) const{
	return dataList[n+1];
}

//Creates a string containing all of the vector's information; inserts into ostream
std::ostream &operator<< (std::ostream &stream, const Imdb &var) {
	//test();
	string result = "";
	for (int i = 0; i < (int) var.dataList.size(); i++){
		result += var.dataList[i];
		result += "\n";
	}
	return stream << result;
}

//Method used to identify HTML entities in a string, and replace them.
void Imdb::fixHTML(string &instr){
	size_t finda = instr.find("&#"); //First, find the first instance that needs to be fixed
	string temp; //Create a string we will use to isolate substrings of 'instr'
	while(finda != string::npos){ //While we are finding HTML entities
		temp = instr.substr(finda+2, 3); //Get the 3rd, 4th and 5th chars ('&#x27;' -> 'x27', '&#39;' -> '39;')
		string firstChar = temp.substr(0, 1); //Get the first char of the substring
		if (strcmp(firstChar.c_str(), "x") == 0){ //if the first char is 'x', we know it's a base-16 number
			temp = temp.substr(1,2);//Isolate the two digits we need to convert
			char u = (char) strtol(temp.c_str(), NULL, 16); //Convert the string to an int, and immediatly cast as a char
			string rep = string(1, u); //Finally, express the char as a string
			instr.replace(finda, 6, rep); //insert this new string in place of the 6-char HTML entity
		}
		else{ //if the first char is not 'x', we know it's a base-10 number; almost identical to previous 'if'
			temp = temp.substr(0,2);
			char u = (char) strtol(temp.c_str(), NULL, 10);
			string rep = string(1, u);
			instr.replace(finda, 5, rep);
		}
		finda = instr.find("&#"); //Find the next HTML entity to be fixed, if there is one
	} 
}


//Downloads the imdb page associated with the given tag, check for d/l error.
//USES OPAQUE POINTERS!!! Code derived from
//http://stackoverflow.com/questions/9429812/static-function-needing-to-deal-with-members-of-a-c-class
void Imdb::getPage(string tag, void *ptr){
    string syscmd; //Used to construct the command to be passed to system()

    //Get the file
	if (tag[0] == 't'){//If the argument is the ID of a movie title....
	    syscmd = "wget -nv -o log -O " + tag + " http://www.imdb.com/title/" + tag + "/"; //...build the command appropriatly
	    system(syscmd.c_str()); //Call system() with an inferior string structure.
	}
	else if (tag[0] == 'n'){//same as above 'if' statement, only for names.
	    syscmd = "wget -nv -o log -O " + tag + " http://www.imdb.com/name/" + tag + "/";
	    system(syscmd.c_str());
	}//end elseif
	else{//If the argument is neither a name nor title...
		Imdb *point = static_cast<Imdb *>(ptr);
		point->errFlag = false; //...set flag indicating an error
	}

	//Check wget's log for an error
	ifstream fLog;
	fLog.open("log");
	string str;
	size_t found;
	while (fLog.good()){ //While loop traverses entire file
	    getline (fLog, str); 
	    found = str.find("ERROR"); //Check the line for "ERROR"
	    if (found != string::npos){
			Imdb *point = static_cast<Imdb *>(ptr);
			point->errFlag = false;
			string cmd = "rm " + tag; //First, prepare to remove the worthless file...
			system(cmd.c_str()); //...and summarily do it
	    }
	}
	fLog.close(); //Close the file
	system("rm log"); //Delete the log file
}


//Scans the 'i'th argument's file for the Actor / Actress / Title, and returns it.
string Imdb::getSubject(string tag){ 
    string result; //Create the strign to return
	string result2;
    size_t title; 
    ifstream input;

    input.open(tag.c_str()); //open the file in question
    while (input.good()){//While there is more to the file to parse...
		getline (input, result); //Get the next lines...
		title = result.find("<title>");
		if (title != string::npos){ //Until we find the line containing '<title>'
	    	result.erase(title - 8, 15); //erase "<title>"
	    	title = result.find(" - IMDb"); //Find either ' - IMDb' or 'IMDb - '; delete
	    	if (title != string::npos){
				result.erase(title, 7);
	    	}
	    	title = result.find("IMDb - ");
	    	if (title != string::npos){
				result.erase(title, 7);
	    	}
	    	title = result.find("</title>"); //Finally, erase '</title>'
	    	result.erase(title, 8);

	    	break;//We've found what we've come for; no need to read the rest of the file.
		}
    }
    input.close(); //Close the input file
    fixHTML(result); //Clean the HTML entities from the resulting string
	if (tag[0] == 't'){
		result2 = "Movie: " + result;
	}
	else{
		result2 = "Actor: " + result;
	}
    return result2; //And pass it along to the next sucker
}


//Main algorithm; orchestrates file operations and isolates pertinent info to push
//onto the dataList vector
bool Imdb::read(const string &tag){

	getPage(tag, this); //Get the page in question

	if (dataList.size() > 0){//Place the title in dataList[0]
		dataList[0] = getSubject(tag);
	}
	else{//dataList[0] doesn't exist yet
		dataList.push_back(getSubject(tag));
	}

	ifstream input;
	string test;//String used to read lines in from the input files
	size_t find, findTwo, placeHolder; //find and findTwo are used to hold index of substrings, placeHolder searches for sentinel values

	input.open(tag.c_str()); //Open the file
	if (tag[0] == 't'){ //Check the argument to see if it's a title.
	    while (input.good()){ //While there's still info left...
			getline(input, test); //Fetch the next line of the file...
			find = test.find("<table class=\"cast_list\">");//...test it for a specific expression indicating upcoming info we need.
			if(find != string::npos){ //When we find the expression...
		    	placeHolder = test.find("</table>"); //Begin looking for our sentinel expression, indicating end of pertinent information
		    	while(placeHolder == string::npos){ //Until we haven't found the sentinel expression
					getline(input, test);
					find = test.find("<td class=\"name\">"); //Check each line for a static info tag
					if(find != string::npos){ //Each time we find such a line, do the following
			    		string result = "Actor: ", blah;
			    		getline(input, test);
			    		find = test.find("    >"); //Find the end of the <a ...> tag
			    		findTwo = test.find("</a>"); //And find the beginning of the </a> tag
			    		blah = test.substr(find+5, (findTwo - find - 5)); //Extrapolate the position of our info from these
			    		result.append(blah + ";"); //Jam the actor name into the result string
			    		blah = test.substr(find-11, 9); //Next, isolate the ID of the actor
			    		result.append(blah + ";"); //and jam that in as well.
			    		for (int k = 0; k < 9; k++){ //Last bit of info comes 9 lines later
							getline (input, test);
			    		}
			    		find = test.find("    >"); //Check for link tags
			    		if (find == string::npos){ //No link tags? It's just the role
							result.append(test); //slam the role on the end. 
							fixHTML(result); //Fix the HTML for the result string
							dataList.push_back(result); //Push that onto the vector
			    		}
			    		else{ //Has link tags?
							test.erase(0, find+5); //Erase the leading tag
							find = test.find("</a>");
							test.erase(find, 4); //Erase the trailing tag
							result.append(test); //Append to result string
							fixHTML(result); //Fix it's HTML entities
							dataList.push_back(result); //And save for future use
			    		}
					}
					placeHolder = test.find("</table>"); //Check for the sentinel expression.
		    	}
		    	sort (dataList.begin()+1, dataList.end()); //employ vector's built-in sort function (sweeeeet)
			}
	    }
	}
	else{ //If the input isn't a title, it must be a name; exceedingly similar to the previous 'if' block
	    while (input.good()){
			getline(input, test);
			find = test.find("<div id=\"filmo-head-Act");
			if(find != string::npos){
		    	placeHolder = test.find("Nikola Tesla is an unsung hero");//'forces' the coming while loop
		    	while(placeHolder == string::npos && input.good()){
					getline(input, test);
					find = test.find("<span class=\"year_column\">");
					if(find != string::npos){
			    		for (int u = 0; u < 3; u++){
							getline(input, test);
			    		}
			    		find = test.find("    >");
			    		findTwo = test.find("</a>");
			    		string result = "Movie: ", blah;
			    		blah = test.substr(find+5, (findTwo- find - 5));
			    		result.append(blah + ";");
			    		blah = test.substr(find-11, 9);
			    		result.append(blah);
		    
			    		for (int y = 0; y < 7; y++){
							getline(input, test);
			    		}
			    		if(test.size() == 0){
							getline(input, test);
			    		}
			    		find = test.find("    >");
			    		if(find == string::npos){
							string work = test.substr(0,1);
							if (work.compare("(") != 0){
								result.append(";" + test);
							}
							fixHTML(result);
							dataList.push_back(result);
			    		}
			    		else{
							test.erase(0, find+5);
							find = test.find("</a>");
							test.erase(find,4);
							result.append(";" + test);
							fixHTML(result);
							dataList.push_back(result);
			    		}
					}
					placeHolder = test.find("<div id=\"filmo-head-"); //Check for the end-of-block condition
		    	}
		    	sort (dataList.begin(), dataList.end());
			}
	    }
	}
	input.close();
	removeTemp(tag);

	return errFlag;
}


//Iterates through argv, removing the corresponding temp files, as well as the remaining log file
void Imdb::removeTemp(string tag){
	string cmd; //As with getPages(), the command we pass to system() starts as a string
	cmd = "rm -f " + tag + "*"; //...assemble the command for system...
	system(cmd.c_str()); //...and call system() using that.
}
