#ifndef Imdb_h_included
#define Imdb_h_included

#include <algorithm> //I don't have OCD
#include <cstring>   //I have CDO
#include <fstream>   //It's like OCD
#include <iostream>  //Only the letters are alphabetical
#include <stdio.h>   //Like they should be
#include <stdlib.h>
#include <string> 
#include <vector>

//Overview of the Imdb class' functions and variables.
class Imdb {
public:
	Imdb();
	Imdb(std::vector<std::string> vec, bool flag);
	Imdb(const std::string &);
	~Imdb();
	Imdb(const Imdb &);
	Imdb & operator= (const Imdb &);

	bool read(const std::string &);
	operator bool() const;
	void clear();
	int size() const;
	std::string operator[](int) const;

	static void fixHTML(std::string &);
	static void getPage(std::string, void *); 
	static std::string getSubject(std::string);
	static void removeTemp(std::string tag);

	std::vector<std::string> dataList;
	bool errFlag;
};

std::ostream &operator<<(std::ostream &, const Imdb &);

#endif
