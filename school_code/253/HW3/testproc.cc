  #include "Imdb.h"
    #include <iostream>
    #include <cassert>

    using namespace std;

    int main() {
        Imdb im("tt0857265");
        cout << "Test #1: just Sleuth\n" << im;

        im.clear();
        im.read("nm2684512");
        cout << "\nTest #2: just Carmel\n" << im;

        im.read("tt0857265");
        cout << "\nTest #3: Carmel & Sleuth\n" << im;

        const Imdb i2 = im;
        im.clear();
        assert(im.size() == 0);
        assert(im);
        assert(i2.size() == 7);
        assert(i2);

        cout << "\nTest #4: Every other line, no title\n";
        for (int i=0; i<i2.size(); i+=2)
            cout << "== " << i2[i] << '\n';

		cout << "\nTest #5: bogus title\n";
        Imdb bogus("tt9999999");
        assert(bogus==false);

        cout << "\nTest #6: bogus actor\n";
        bogus.clear();
        bogus.read("nm9999999");
        assert(bogus==false);
    }
