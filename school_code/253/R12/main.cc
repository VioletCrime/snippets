#include "hset.h"
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

int main() {
	hset<int> h;

	srand(getpid());

	for (int j=0; j<20; j++)
		h.insert(rand() % 100);
	h.dump();

	cout << "size=" << h.size();
	for (hset<int>::iterator i=h.begin(); i!=h.end(); ++i)
		cout << ' ' << *i;
	cout << endl;

	hset<double> hd;

	hd.insert(1.1);
	hd.insert(1.2);
	hd.insert(1.3);
	hd.insert(1.4);	
	hd.insert(8.5);
	hd.insert(1.6);
	hd.insert(9.1);
	hd.insert(10.2);
	hd.insert(11.3);
	hd.insert(12.4);	
	hd.insert(13.5);
	hd.insert(14.6);

	hset<string> hs;

	hs.insert("alpha");
	hs.insert("beta");
	hs.insert("gamma");
	hs.insert("delta");
	hs.insert("omega");
	hs.dump();

	cout << "size=" << hs.size();
	for (hset<string>::iterator i=hs.begin(); i!=hs.end(); ++i)
		cout << ' ' << *i;
	cout << endl << endl;
	
	hd.dump();

	cout << "hd size = " << hd.size();
	for (hset<double>::iterator i = hd.begin(); i != hd.end(); ++i)
		cout << ' ' << *i;
	cout << endl;
}
