#include <string>

// Some horrible hash functions

class HasherTwo {
    public:
	unsigned operator()(int n) {
		return n % 8;
	}

	unsigned operator()(const std::string &s) {
		unsigned sum = 42;
		for (unsigned i=0; i<s.size(); i++)
			sum += s[i];
		return sum;
	}

	unsigned operator() (double d){
		return d*d*d - (d/3.0);
	}
};

