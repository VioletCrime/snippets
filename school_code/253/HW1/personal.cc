/*
Personal, written by Kyle Smith (ksmitty)
CS253 Spring '12 (Jack Applin)
Started 1/20/2012. Finished 1/24/2012
*/

#include <iostream>
#include <string>

using namespace std; //Can't forget this...

int compute_age (int year){
	return 2012 - year;
}

int main(){
	string name; //Used to store the user's name
	string firstLetter;  //Used to hold the first letter of user's name
	int year; //Stores the year in which the user was born.

	cout << "Your name? "; //Prompt user for his/her name.
	getline (cin, name); //Took a while to figure out 'cin' wouldn't work here.
	firstLetter = name.substr(0,1); //Isolate first character of user's name.
	cout << name << " is a bit much; I'll call you " << firstLetter << ".\nYear of birth? "; //bla, bla...
	cin >> year; //Get user's year of birth.
	int age = compute_age(year); //Calculate user's age
	cout << "You're " << age << ", roughly.\n"; //Make the user feel old
	return 0; //SHAZAM!
}
