/*
 * controller.c: small program used to spawn and employ
 *  instances of calculator.c
 * Written by Kyle Smith (smithkyl) for CS370, Spring '12
 * Started 1/29/12. v0.2.1 2/13/12
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>


int main(int argc, char *argv[]){
	argc--; //Shut up the pedantic compiler warnings
    	pid_t pid;//Create a pid_t object
	char pipArg[sizeof(long)], writeBuf[21]; //Create a couple of buffers

	int shmemID = shmget(IPC_PRIVATE, sizeof(long), IPC_CREAT | 0664); //Allocate some memory; get ID
	char *shared_memory = (char*) shmat(shmemID, NULL, 0); //Attach the memory; get pointer to it
	printf("Controller: Shared Memory %u Created!\n", shmemID);

	memset(shared_memory, '0', 1); //Clear the memory
	memset(shared_memory+1, 0, sizeof(long)-1); //Make it look pretty

   	for (int n = 0; n < 4; n++){ //Makes 4 children

		int fd[2]; //Create an array of 2 ints...
		pipe(fd); //...and turn it into a pipe
 		snprintf(pipArg, sizeof(pipArg), "%d", fd[0]); //Convert the read end into a passable argument
 
   		pid = fork(); //Get 'this' process' pid //Get forked!

		if(pid == -1){ //Failure condition
	  		printf("Controller: fork() failed");	
		}

		else if(pid > 0){   //Parent condition
	    		printf("Controller: forked process %d.\n", pid);

			close(fd[0]); //Close the read-end of the pipe
			snprintf(writeBuf, sizeof(writeBuf), "%d", shmemID); //convert the memory ID into a passable argument
			int nbytes = write(fd[1], (void*) writeBuf, sizeof(writeBuf)); //Shove the argument into the pipe
			printf("Controller: Wrote %d bytes to pipe containing shm ID %s.\n", nbytes, writeBuf);

	    		waitpid(pid, NULL, 0); //Wait for the child to complete it's work

			printf("Controller: %s stored in shared memory.\n", shared_memory);
			memset(shared_memory, '0', 1); //Clear the memory
			memset(shared_memory+1, 0, sizeof(long)-1); //Format the memory to loook pretty
			printf("Controller: Reset shared memory (%s).\n", shared_memory); 
		}

		else if (pid == 0){ //Child condition
			close(fd[1]); //Close the write end of the pipe
	    		execlp("Smith_Kyle_Calculator.out", "Smith_Kyle_Calculator.out", argv[1], argv[2], pipArg, NULL); //Program call
		}
    	} 

	shmdt(shared_memory); //Detach from the shared memory.
	shmctl(shmemID, IPC_RMID, NULL); //Release the memory
	printf("Controller: Exiting.\n"); //Time to ride off into the sunset
    	exit(0); //Great success!
}