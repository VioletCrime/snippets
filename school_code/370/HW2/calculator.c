/*
 * calculator.c: program called by controller (controller.c), which will
 * calculate the remainder of argv[1] / argv[2]]
 * Written by Kyle Smith (smithkyl) for CS370, Spring '12
 * Started 1/29/12. v0.2.1 2/13/12.
 * */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/shm.h>

int main(int argc, char *argv[]){
	argc++; //shut up the pedantic compiler warnings
	pid_t pid = getpid(); //Get the process' pid
	int a = atoi(argv[1]); //Get the first argument
	int b = atoi(argv[2]); //Get the second argument
	int fd = atoi(argv[3]); //Get the pipe
	char tmpMemID[21], memBuff[sizeof(long)]; //Create buffers

	int bytesRead = read(fd, tmpMemID, sizeof(tmpMemID)-1); //Read the memory ID from the pipe to the buffer
	printf("Calculator %d: Read %d bytes containing shm ID %s\n", pid, bytesRead, tmpMemID);
	int shMemID = atoi(tmpMemID); //Convert the ID into a usable int
	char *shared_address = (char*) shmat(shMemID, NULL, 0); //Attach to the shared memory
	printf("Calculator %d: %s stored in shared memory.\n", pid, shared_address);

	int result = a % b; //All that just for this....

	snprintf(memBuff, sizeof(memBuff), "%d", result); //convert the result into the buffer...
	sprintf(shared_address, memBuff); //Write the char[] result to shared memory
	printf("Calculator %d: Wrote result=%s to shared memory.\n", pid, shared_address);
	shmdt(shared_address); //Detach from the shared memory...
	exit(0); //...and that's All She Wrote
}