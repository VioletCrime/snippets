Makefile < all | clean | dev | pack | test >
	all: Compiles the two source files using GCC.
	clean: Removes the compiled programs.
	pack: Prepares the appropriate files for checkin.
	rdev: Same as dev, and runs a simple test case.
	test: Runs 3 test cases. Does not compile the programs.

controller.c int1, int2
	Compiles into 'Smith_Kyle_Controller.out'. This program takes two integer arguments, and passes these integer arguments
	to the Calculator program. Calculator is called 4 times in all, with the same arguments each time, and after these four
	interations of Calculator have terminated, controller will summarily exit.

calculator.c int1, int2
	Compiles into 'Smith_Kyle_Calculator.out'. Will not work without being called by the controller.