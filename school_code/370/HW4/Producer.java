import java.util.Random;


public class Producer extends Thread{
	static Buffer b;
	
	//Set the buffer to be used
	public void prepare(Buffer b_){
		b = b_;
	}
	
	//Moneymaker: Generates the million doubles and throws them at the buffer
	public void oneMRun(){
		Random random = new Random(); //Generates our doubles
		double runningTotal = 0; //Running total value of produced doubles
		
		for (int i = 1; i < 1000001; i++){ //for 1,000,000 iterations
			double d = random.nextDouble() * 100 ; //Get a double
			runningTotal += d; //Add its value to the total
			
			if (i % 100000 == 0){ //Output block
				if (i % 1000000 ==0){
					System.out.println("Producer: Generated " + i / 1000000 + ",000,000 items, Cumulative value of generated items=" + runningTotal);
				}
				else{
					System.out.println("Producer: Generated " + i / 100000 + "00,000 items, Cumulative value of generated items=" + runningTotal);
				}
			}
			b.add(d); //Add it to the buffer
		}
		System.out.println("Producer: Finished generating 1,000,000 items"); //fin
	}
	
	@Override
	public void run() { //Not much to see here. Just call the main method, 'oneMRun()'
		oneMRun();
		
	}
}
