
public class ProducerConsumer {
	public static void main(String args[]){
		//Instantiate the needed classes
		Buffer b = new Buffer();
		Consumer c = new Consumer();
		Producer p = new Producer();
		
		//Pass the buffer to the producer and consumer
		c.prepare(b);
		p.prepare(b);
		
		//Start the producer and consumer threads
		p.start();
		c.start();
		
		//Join() on both the producer and consumer threads
		try { //Eclipse bitched about 'Join throws exceptions'; *sigh*
			p.join();
		} catch (InterruptedException e) {
			System.out.println("ProducerConsumer: p.join() encountered an Interrupted Exception");
			e.printStackTrace();
		}
		try {
			c.join();
		} catch (InterruptedException e) {
			System.out.println("ProducerConsumer: c.join() encountered an Interrupted Exception");
			e.printStackTrace();
		}
		
		//And that's All She Wrote
		System.out.println("Exiting ...");
		
		//b.get_stats();  //Uncomment for # times wait() called by producer and consumer
	}
}
