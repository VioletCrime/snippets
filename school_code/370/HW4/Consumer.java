
public class Consumer extends Thread{
	static Buffer b;

	//Set the buffer to be used
	public void prepare (Buffer b_){
		b = b_;
	}
	
	//Method's tourist attraction: consumes the million random doubles
	public void oneMBurn(){
		double runningTotal = 0; //Running total of consumed doubles
		
		for (int i = 1; i < 1000001; i++){	//Do this bit one million times
			double d = b.get(); //Get the next double in the buffer
			runningTotal += d; //Add its value to the running total
			
			if (i % 100000 == 0){ //Output block
				if (i % 1000000 ==0){
					System.out.println("Consumer: Consumed " + i / 1000000 + ",000,000 items, Cumulative value of consumed items=" + runningTotal);
				}
				else{
					System.out.println("Consumer: Consumed " + i / 100000 + "00,000 items, Cumulative value of consumed items=" + runningTotal);
				}
			}
		}
		System.out.println("Consumer: Finished consuming 1,000,000 items"); //fin
	}
	
	@Override
	public void run(){//Not much to see here. Just call the main method, 'oneMBurn()'
		oneMBurn();
	}
}
