ProducerConsumer by Kyle Smith (ksmitty)
v0.3 4/11/12 for CS370

Problem was solved using 4 classes:

Producer: Uses a Random object to produce a million doubles and add them to the buffer.

Consumer: Gets the million doubles rotated through the buffer.

Buffer: Holds an array of 1000 doubles, and has two methods; one to add values to the array,
and another to retrieve values from the array. All thread-safe programming occurs in the buffer,
causing the Producer and Consumer threads to wait() if the buffer is full/empty, respectively,
and ensuring that the two threads cannot operate on any of Buffer's member variables concurrently.

ProducerConsumer - The 'main' method, if you will. Instantiates a Producer, Consumer, and Buffer,
passing the buffer to the producer and consumer. Next, threads are started on both Producer and
Consumer, and ProducerConsumer waits for the threads to terminate before exiting.