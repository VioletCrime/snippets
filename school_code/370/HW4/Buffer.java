
public class Buffer {
	int bufferSize = 1000; //Set the buffer's size
	double[] buffah = new double[bufferSize]; //The buffer itself
	int in = 0, out = 0; //'pointers' to indices of the buffer
	boolean full = false; //Is the buffer full?
	boolean empty = true; //Is it empty?
	boolean pWaiting = false; //Is the producer waiting?
	boolean cWaiting = false; //How about the consumer?
	
	int a = 0, b = 0; //wait() counters; a = producer, b = consumer
	
	//Adds doubles to the buffer
	public synchronized void add(double d){
		if (full == true){//Is the buffer full?
			pWaiting = true; //If so, flip bool value, and wait()
			a++; //increment wait counter
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		pWaiting = false; //Not waiting anymore!
		
		buffah[in] = d; //Insert the value into the buffer
		in = (in + 1) % bufferSize; //Increment the input pointer (range 0-999)
		if (in == out){ //Did this fill the buffer?
			full = true; //If so, make note of it.
		}
		empty = false; //It sure as hell isn't false after this operation.
		if (cWaiting){
			notify();//If the consumer is waiting, notify it
		}
	}
	
	//Returns doubles from the buffer
	public synchronized double get(){
		if (empty == true){ //Wait if the buffer is empty
			cWaiting = true;
			b++; //Increment wait counter
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		cWaiting = false; //I'm tired of waiting!
		
		double result; //Holds the next double to be consumed
		result = buffah[out]; //Get the next double from the buffer
		out = (out + 1) % bufferSize; //Increment the output pointer
		if (out == in){ //Did this empty the buffer?
			empty = true; //Make note if it did
		}
		full = false; //The buffer certainly isn't full anymore
		if (pWaiting){
			notify(); //If the producer is waiting, get it back to work
		}
		return result; //return the double
	}
	
	public void get_stats(){
		System.out.println("Producer waited " + a + " times, Consumer waited " + b + " times.");
	}
	
}
