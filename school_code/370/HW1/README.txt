Makefile < all | clean | dev | pack | test >
	all: Compiles the two source files using G++.
	clean: Removes the compiled programs.
	dev: Compiles the two source files using G++ using pedantic warning options, and runs a test case.
	pack: Prepares the appropriate files for checkin.
	test: Runs 3 test cases. Does not compile the programs.

controller.c int1, int2
	Compiles into 'Smith_Kyle_Controller.out'. This program takes two integer arguments, and passes these integer arguments
	to the Calculator program. Calculator is called 4 times in all, with the same arguments each time, and after these four
	interations of Calculator have terminated, controller will summarily exit.

calculator.c int1, int2
	Compiles into 'Smith_Kyle_Calculator.out'. The program takes two integer arguments, and calculates the remainer of
	arg1 / arg2. Output is optimized to work with the controller, but it can be employed directly if needed.