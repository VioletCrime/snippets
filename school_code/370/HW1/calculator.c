/*
 * calculator.c: program called by controller (controller.c), which will
 * calculate the remainder of argv[1] / argv[2]]
 * Written by Kyle Smith (smithkyl) for CS370, Spring '12
 * Started 1/29/12. v0.1.3 2/13/12.
 * */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[]){
	argc--; //Shut up the pedantic warnings
	int a = atoi(argv[1]); //Get the first argument
	int b = atoi(argv[2]); //Get the second argument
	pid_t pid = getpid(); //Get the process' pid

	printf("Calculator process [%d]: started.\n", pid);
	printf("Calculator process [%d]: %d %% %d = %d\n", pid, a, b, a%b); //Moneymaker output; calculate result on-the-fly.
	printf("Calculator process [%d]: exiting.\n", pid);
	exit(0);
}
