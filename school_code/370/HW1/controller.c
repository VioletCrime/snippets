/*
 * controller.c: small program used to spawn and employ
 *  instances of calculator.c
 * Written by Kyle Smith (smithkyl) for CS370, Spring '12
 * Started 1/29/12. v0.1.3 2/13/12
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(int argc, char *argv[]){
	argc--; // Shut up the pedantic warnings
	if (strcmp(argv[2], "0") == 0){
		printf("Cannot divide by zero.\n");
		return 1;
	}
    pid_t pid;//Create a pid_t object
    for (int n = 0; n < 4; n++){ //Makes 4 children
    	pid = fork(); //Get 'this' process' pid
	if(pid == -1){	//Failure condition
	  printf("Controller: fork() failed");	
	}
	else if(pid > 0){   //Parent condition
	    printf("Controller: forked process with ID %d.\n", pid);
	    printf("Controller: waiting for process [%d].\n", pid);
	    waitpid(pid, NULL, 0); //Wait for the child to complete it's work
	    printf("Controller: returned from waiting.\n");
	}
	else if (pid == 0){ //Child condition
	    execlp("Smith_Kyle_Calculator.out", "Smith_Kyle_Calculator.out", argv[1], argv[2], NULL); //Program call
	}
    } 
	printf("Controller: exiting.\n"); //Time to ride off into the sunset
    exit(0);
}
