# Search.py, written by Kyle Smith for CS440, Assignment 2

def iterativeDeepeningSearch(startState, actionsF, takeActionF, goalTestF, maxDepth) :
    for depthLimit in range(maxDepth) :
        passState = startState[:]
        solution = depthLimitedSearchHelper(passState, actionsF, takeActionF, goalTestF, depthLimit)
        if solution[0] != "cutoff" :
            return solution
    result = ["cutoff"]
    return result
    

def depthLimitedSearchHelper(state, actionsF, takeActionF, goalTestF, depthLimit) :
    test = goalTestF(state)
    if test == True :
        result = [state]
        return result
    elif depthLimit == 0 :
        result = ["cutoff"]
        return result
    else :
        tempState = state[:]
        cutoff_occurred = False
        actions = actionsF(tempState)
        for action in actions :
            result = depthLimitedSearchHelper(takeActionF(tempState, action), actionsF, takeActionF, goalTestF, depthLimit - 1)
            if result[0] == "cutoff" :
                cutoff_occurred = True
            elif result[0] != "failure" :
                result.insert(0, tempState)
                return result
        if cutoff_occurred == True :
            result = ["cutoff"]
        else :
            result = ["failure"]
        return result
