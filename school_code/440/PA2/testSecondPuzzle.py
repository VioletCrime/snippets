# testSecondPuzzle.py; revisiting the camels problem.

import search

camels = ['r', 'r', 'r', 'r', 'r', 'e', 'l', 'l', 'l', 'l', 'l']
goal = ['l', 'l', 'l', 'l', 'l', 'e', 'r', 'r', 'r', 'r', 'r']

def printState(state) :
    output = ""
    for i in range(11) :
        output += repr(state[i]) + " "
    print output

def actionsF(state) :
    result = []
    for i in range(11) :
        if state[i] == 'e' :
            break
    if (i - 2) >= 0 and state[i - 2] == 'r' :
        result.append('twoLeft')
    if (i - 1) >= 0 and state[i - 1] == 'r' :
        result.append('oneLeft')
    if (i + 1) <= 10 and state[i + 1] == 'l' :
        result.append('oneRight')
    if (i + 2) <= 10 and state[i + 2] == 'l' :
        result.append('twoRight')
    return result

def takeActionF(state, action) :
    result = state[:]
    for i in range(11) :
        if state[i] == 'e' :
            break
    if action == 'twoLeft' :
        result[i] = 'r'
        result[i - 2] = 'e'
    if action == 'oneLeft' :
        result[i] = 'r'
        result[i - 1] = 'e'
    if action == 'oneRight' :
        result[i] = 'l'
        result[i + 1] = 'e'
    if action == 'twoRight' :
        result[i] = 'l'
        result[i + 2] = 'e'
    return result

def goalTestF(state) :
    return state == goal

def printSolution(s) :
    print "Path from"
    printState(camels)
    print "to"
    printState(goal)
    if s[0] == "cutoff" :
        print "was not found due to depth cutoff."
    elif s[0] == "failure" :
        print "is incalculable; search failed."
    else :
        print "  is " + repr(len(s)) + " nodes long:"
        for step in s :
            printState(step)
            print ""

solution = search.iterativeDeepeningSearch(camels, actionsF, takeActionF, goalTestF, 64)
printSolution(solution)
