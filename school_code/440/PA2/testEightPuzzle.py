import search
import eightPuzzle as puz

start = [1, 2, 3, 4, 0, 5, 6, 7, 8]
goal = [1, 2, 3, 4, 5, 8, 6, 0, 7]

def goalTestF(s) :
    return s == goal

solution = search.iterativeDeepeningSearch(start, puz.actionsF, puz.takeActionF, goalTestF, 2)
puz.printResult(start, goal, solution)
solution = search.iterativeDeepeningSearch(start, puz.actionsF, puz.takeActionF, goalTestF, 10)
puz.printResult(start, goal, solution)
