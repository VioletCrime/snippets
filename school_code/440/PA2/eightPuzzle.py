# eightPuzzle.py written by Kyle Smith for CS440, Assignment 2

def printState(state) :
    output = ""
    for j in range (9) :
        output += repr(state[j]) + " "
        if (j + 1) % 3 == 0 and j != 8:
            output += "\n"
    print output

def actionsF(state) :
    result = []
    for i in range(9) :
        if state[i] == 0 :
            break
    if i == 0 :
        result = ['right', 'down']
    elif i == 1 :
        result = ['right', 'left', 'down']
    elif i == 2 :
        result = ['left', 'down']
    elif i == 3 :
        result = ['up', 'right', 'down']
    elif i == 4 :
        result = ['up', 'right', 'left', 'down']
    elif i == 5 :
        result = ['up', 'left', 'down']
    elif i == 6 :
        result = ['up', 'right']
    elif i == 7 :
        result = ['up', 'right', 'left']
    elif i == 8 :
        result = ['up', 'left']
    return result

def takeActionF(state, action) :
    result = state[:]
    for i in range(9) :
        if result[i] == 0 :
            break
    if action == 'up' :
        result[i] = result[i-3]
        result[i-3] = 0
    if action == 'right' :
        result[i] = result[i+1]
        result[i+1] = 0
    if action == 'left' :
        result[i] = result[i-1]
        result[i-1] = 0
    if action == 'down' :
        result[i] = result[i+3]
        result[i+3] = 0
    return result

def goalTestF(state) :
    goalState = [1, 2, 3, 4, 5, 6, 7, 8, 0]
    if state == goalState :
        result = True
    else :
        result = False
    return result

def printResult(startState, goalState, solutionPath) :
    print "Path from"
    printState(startState)
    print "  to"
    printState(goalState)
    if solutionPath[0] == "cutoff" :
        print "was not found due to depth cutoff."
    elif solutionPath[0] == "failure" :
        print "is incalculable; search failed."
    else :
        print "  is " + repr(len(solutionPath)) + " nodes long:"
        for step in solutionPath :
            printState(step)
            print ""
