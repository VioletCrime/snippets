import gameSearch as gs
reload(gs)
import TTT
reload(TTT)
import random
reload(random)

def playGame(game,opponent,depthLimit):
    print game
    while not game.isOver():
        score,move = gs.negamaxIDS(game,depthLimit)
        if move == None :
            print "move is None. Stopping"
            break
        game.makeMove(move)
        print "Player",game.player,"to",move,"for score",score,"\n"
        print game
        if not game.isOver():
            game.changePlayer()
            opponentMove = opponent(game.board)
            game.makeMove(opponentMove)
            print "Player",game.player,"to",move
            print game
            game.changePlayer()

def playGameab(game,opponent,depthLimit):
    print game
    while not game.isOver():
        score,move = gs.negamaxIDSab(game,depthLimit)
        if move == None :
            print "move is None. Stopping"
            break
        game.makeMove(move)
        print "Player",game.player,"to",move,"for score",score,"\n"
        print game
        if not game.isOver():
            game.changePlayer()
            opponentMove = opponent(game.board)
            game.makeMove(opponentMove)
            print "Player",game.player,"to",move
            print game
            game.changePlayer()

def playGameabQuiet(game,opponent,depthLimit):
    while not game.isOver():
        score,move = gs.negamaxIDSab(game,depthLimit)
        if move == None :
            break
        game.makeMove(move)
        if not game.isOver():
            game.changePlayer()
            opponentMove = opponent(game.board)
            game.makeMove(opponentMove)
            game.changePlayer()

def extraCred():
    print "Extra credit function starting! Get comfortable; on the lab machines,"
    print "this took about 3 min."
    nodes = 0
    winners = []
    XWon = 0
    for i in range(100):
        gamez = TTT.TTT()
        playGameabQuiet(gamez, opponentRand,9)
        winners.append(gamez.player)
        nodes += gamez.movesExplored
    ebfz = ebf(nodes/100, 9)
    for i in range(len(winners)):
        if winners[i] == 'X':
            XWon += 1
    print "Over the course of 100 games, X won ", XWon, " of them."
    print "An average of ", nodes/100, " nodes were explored, with an ebf of ", ebfz, "."
    
        

def opponent(board):
    return board.index(' ')

def opponentRand(board):
    moves = []
    for i in range(9):
        if board[i] == ' ':
            moves.append(i)
    i = random.randint(0, len(moves)-1)
    return moves[i]

def ebf(numNodes, depth, precision = 0.01) :
    if depth == 0 :
        return 0
    guess = numNodes / 2.0
    change_amount = numNodes / 2.0
    result = -1
    while abs(result-numNodes) > precision :
        result = calc_ebf(guess, depth)
        if not abs(result-numNodes) < precision :
            change_amount = change_amount / 2
            if numNodes > result :
                guess += change_amount
            else :
                guess -= change_amount
    return guess

def calc_ebf(guess, depth) :
    if guess == 1 :
        guess -= 0.00000000001
    return (1-guess**(depth+1))/(1-guess)

game = TTT.TTT()
gameab = TTT.TTT()
playGame(game,opponent,9)
print "Without alpha-beta pruning, " + repr(game.movesExplored) + " moves explored for ebf of " + repr(ebf(game.movesExplored, 9)) + "."
playGameab(gameab, opponent, 9)
print "With alpha-beta pruning, " + repr(gameab.movesExplored) + " moves explored for ebf of " + repr(ebf(gameab.movesExplored, 9)) + "."
extraCred()
