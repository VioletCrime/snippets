import numpy as np
import matplotlib.pyplot as plt
import random
from copy import copy

######################################################################
def winner(board):
    combos = np.array([' ', ' ', 'a', ' ', ' ', 'b', ' ', ' ', 'c'])
    return (board == combos).all()

def printBoard(board):
    print """
%c|%c|%c
-----
%c|%c|%c
-----
%c|%c|%c""" % tuple(board)
    
def plotOutcomes(outcomes,nGames,game):
    if game==0:
        return
    plt.clf()
    nBins = 100
    nPer = nGames/nBins
    outcomeRows = outcomes.reshape((-1,nPer))
    outcomeRows = outcomeRows[:int(game/float(nPer))+1,:]
    avgs = np.mean(outcomeRows,axis=1)
    xs = np.linspace(nPer,game,len(avgs))
    plt.plot(xs, avgs)
    plt.xlabel('Games')
    plt.ylabel('Steps')
    plt.draw()

def otherPegs(peg):
    result = []
    if peg == 0:
        result = [1, 2]
    elif peg == 1:
        result = [0, 2]
    elif peg == 2:
        result = [0, 1]
    return result

def validMovess(board):
    results = []
    #print "validMovess was passed: " + repr(tuple(board))
    apeg = np.where(board == 'a')[0]
    bpeg = np.where(board == 'b')[0]
    cpeg = np.where(board == 'c')[0]
    apegs = otherPegs(apeg%3)
    bpegs = otherPegs(bpeg%3)
    cpegs = otherPegs(cpeg%3)
    #print "apeg = " + repr(apeg)
    #print "apegs = " + repr(apegs)
    #print "bpeg = " + repr(bpeg)
    #print "bpegs = " + repr(bpegs)
    #print "cpeg = " + repr(cpeg)
    #print "cpegs = " + repr(cpegs)


    #Find where 'a' can move
    if board[apegs[0] + 6] == ' ':
        result = np.copy(board)
        result[apeg] = ' '
        result[apegs[0] + 6] = 'a'
        results.append(result)
    elif board[apegs[0] + 3] == ' ':
        result = np.copy(board)
        result[apeg] = ' '
        result[apegs[0] + 3] = 'a'
        results.append(result)
    else:
        result = np.copy(board)
	result[apeg] = ' '
        result[apegs[0]] = 'a'
        results.append(result)
    #Part II of 'a'
    if board[apegs[1] + 6] == ' ':
        result = np.copy(board)
        result[apeg] = ' '
        result[apegs[1] + 6] = 'a'
        results.append(result)
    elif board[apegs[1] + 3] == ' ':
        result = np.copy(board)
        result[apeg] = ' '
        result[apegs[1] + 3] = 'a'
        results.append(result)
    else:
        result = np.copy(board)
        result[apeg] = ' '
        result[apegs[1]] = 'a'
        results.append(result)

    #Find where 'b' can move
    if board[bpeg[0] - 3] == ' ':
        if board[bpegs[0] + 6] == ' ':
            result = np.copy(board)
            result[bpeg] = ' '
            result[bpegs[0] + 6] = 'b'
            results.append(result)
        elif board[bpegs[0] + 3] == ' ' and board[bpegs[0] + 6] == 'c':
            result = np.copy(board)
            result[bpeg] = ' '
            result[bpegs[0] + 3] = 'b'
            results.append(result)
        #Part deux
        if board[bpegs[1] + 6] == ' ':
            result = np.copy(board)
            result[bpeg] = ' '
            result[bpegs[1] + 6] = 'b'
            results.append(result)
        elif board[bpegs[1] + 3] == ' ' and board[bpegs[1] + 6] == 'c':
            result = np.copy(board)
            result[bpeg] = ' '
            result[bpegs[1] + 3] = 'b'
            results.append(result)
    if board[cpeg[0] - 3] == ' ':
        if board[cpegs[0] + 6] == ' ':
            result = np.copy(board)
            result[cpeg] = ' '
            result[cpegs[0] + 6] = 'c'
            results.append(result)
        if board[cpegs[1] + 6] == ' ':
            result = np.copy(board)
            result[cpeg] = ' '
            result[cpegs[1] + 6] = 'c'
            results.append(result)
    #print "validMovess returns: "
    #for y in results:
    #    printBoard(y)
    return results
        

plt.ion()
nGames = 10000                          # number of games
rho = 0.1                               # learning rate
epsilonExp = 0.999                      # rate of epsilon decay
outcomes = np.zeros(nGames)             # 0 draw, 1 X win, -1 O win
Q = {}                                  # initialize Q dictionary
epsilon = 1.0                           # initial epsilon value
showMoves = False                       # flag to print each board change

for game in range(nGames):              # iterate over multiple games

    epsilon *= epsilonExp
    step = 0
    board = np.array(['a', ' ', ' ', 'b', ' ', ' ', 'c', ' ', ' '])
    done = False
    #testBoard = np.array([' ', ' ', ' ', 'a', ' ', ' ', 'b', 'c', ' '])
    #printBoard(testBoard)
    #possMoves = validMovess(testBoard)
    #print "\nThis board yields the following possible moves:"
    #for brd in possMoves:
    #    print "==================="
    #    printBoard(brd)
    #assert 1 == 0

    while not done:
        step += 1
        if game == nGames - 1:
           printBoard(board)
           print "------"
        validMoves = validMovess(board)
        if np.random.uniform() < epsilon:
            # Random move
            moveInt = random.randint(0, len(validMoves)-1)
            move = validMoves[moveInt]

        else:
            # Greedy move. Collect Q values for valid moves from current board.
            # Select move with highest Q value
            qs = []
            for m in validMoves:
                qs.append(Q.get((tuple(board),tuple(m)), 0))
            move = validMoves[np.argmax(np.asarray(qs))]

        if not Q.has_key((tuple(board),tuple(move))):
            Q[(tuple(board),tuple(move))] = 0

        boardNew = copy(board)
        boardNew = move
        if showMoves:
            printBoard(boardNew)

        if winner(boardNew):
            Q[(tuple(board),tuple(move))] = 1
            done = True
            outcomes[game] = step 
            if game == nGames - 1:
                printBoard(boardNew)
                print "------"

        if step > 1:
            Q[(tuple(boardOld),tuple(moveOld))] += rho * (Q[(tuple(board),tuple(move))] - Q[(tuple(boardOld),tuple(moveOld))])

        boardOld,moveOld = board,move
        board = boardNew

        if game % (nGames/10) == 0 or game == nGames-1:
            plotOutcomes(outcomes,nGames,game)
             
