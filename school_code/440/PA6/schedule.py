# Kyle Smith
# CS440 Fall '12

import minconflicts as mc
import sys

def classes(switch, show=True) :
    vars = ['CS160', 'CS161', 'CS200', 'CS253', 'CS270', 'CS320', 'CS314', 'CS370', 'CS410', 'CS414', 'CS420', 'CS440', 'CS430', 'CS451', 'CS453', 'CS510', 'CS514', 'CS517', 'CS518', 'CS520', 'CS530', 'CS540']
    domains = dict((k, (('CSB130', 9), ('CSB130', 10), ('CSB130', 11), ('CSB130', 12), ('CSB130', 1), ('CSB130', 2), ('CSB130', 3), ('CSB130', 4), ('CSB325', 9), ('CSB325', 10), ('CSB325', 11), ('CSB325', 12), ('CSB325', 1), ('CSB325', 2), ('CSB325', 3), ('CSB325', 4), ('CSB425', 9), ('CSB425', 10), ('CSB425', 11), ('CSB425', 12), ('CSB425', 1), ('CSB425', 2), ('CSB425', 3), ('CSB425', 4))) for k in vars)


    neighbors = {}
    for v in vars:
        neighbors[v] = filter(lambda i: i != v, vars)


    def constraints(A,a,B,b):
        result = True
        if a == b:
            result = False
        elif a[1] == b[1] :
            if A[2] == B[2]:
                result = False
                if A[2] == '1' and B[2] == '1':
                    result = True
        return result

    def display(vars, assignment):
        #print assignment
        classes = ['     ']*24
        for val in assignment:
            if assignment[val] == ('CSB130', 9):
                classes[0] = val
            if assignment[val] == ('CSB325', 9):
                classes[1] = val
            if assignment[val] == ('CSB425', 9):
                classes[2] = val
            if assignment[val] == ('CSB130', 10):
                classes[3] = val
            if assignment[val] == ('CSB325', 10):
                classes[4] = val
            if assignment[val] == ('CSB425', 10):
                classes[5] = val
            if assignment[val] == ('CSB130', 11):
                classes[6] = val
            if assignment[val] == ('CSB325', 11):
                classes[7] = val
            if assignment[val] == ('CSB425', 11):
                classes[8] = val
            if assignment[val] == ('CSB130', 12):
                classes[9] = val
            if assignment[val] == ('CSB325', 12):
                classes[10] = val
            if assignment[val] == ('CSB425', 12):
                classes[11] = val
            if assignment[val] == ('CSB130', 1):
                classes[12] = val
            if assignment[val] == ('CSB325', 1):
                classes[13] = val
            if assignment[val] == ('CSB425', 1):
                classes[14] = val
            if assignment[val] == ('CSB130', 2):
                classes[15] = val
            if assignment[val] == ('CSB325', 2):
                classes[16] = val
            if assignment[val] == ('CSB425', 2):
                classes[17] = val
            if assignment[val] == ('CSB130', 3):
                classes[18] = val
            if assignment[val] == ('CSB325', 3):
                classes[19] = val
            if assignment[val] == ('CSB425', 3):
                classes[20] = val
            if assignment[val] == ('CSB130', 4):
                classes[21] = val
            if assignment[val] == ('CSB325', 4):
                classes[22] = val
            if assignment[val] == ('CSB425', 4):
                classes[23] = val

        print """      CSB 130  CSB 325  CSB 425
-------------------------------
  9    %s    %s    %s
 10    %s    %s    %s
 11    %s    %s    %s
 12    %s    %s    %s
  1    %s    %s    %s
  2    %s    %s    %s
  3    %s    %s    %s
  4    %s    %s    %s""" % tuple(classes)


    def partDeux() :
        num_runs = 1000
        least_conflicts = sys.maxint
        best_solution = None

        def contraintsDeux(A, a, B, b) :
            test1 = a[1] != 9 and a[1] != 12 and a[1] != 4
            test2 = b[1] != 9 and b[1] != 12 and b[1] != 4
            test3 = False
            if A[2] == '1' :
                if a[1] == 1 or a[1] == 2 :
                    test3 = True
            return test1 and test2 and test3

        while num_runs > 0 :
            num_conflicts = 0
            solution = mc.min_conflicts(vars, domains, constraints, neighbors)
            num_runs -= solution[1] + 1
            if solution[0] :
                #print solution[0]
                for var in vars :
                    #print solution[0][var]
                    num_conflicts += mc.nconflicts(var, solution[0][var], solution[0], contraintsDeux, neighbors)
                if num_conflicts < least_conflicts :
                    least_conflicts = num_conflicts
                    print "Best number of conflicts decreased to", least_conflicts
                    best_solution = solution
            else :
                print "No solution found."
        return best_solution

    if switch == 1 :
        solution = mc.min_conflicts(vars, domains, constraints, neighbors)
    else :
        solution = partDeux()

    if show:
        if solution[0]:
            print
            print "Solution took",solution[1],"steps"
            display(vars,solution[0])
        else:
            print "No solution found."

    return solution[1]


if __name__ == "__main__" :
    print "\n=== Part I ==="
    for i in range(3) :
        classes(1)
    print "\n=== Part II ==="
    for i in range(3) :
        classes(0)


"""def constraints(A,a,B,b):


        return A == B or (a != b and A + a != B + b and A - a != B - b)

    def display(vars,assignment):
        "Print the queens and the nconflicts values (for debugging)."
        n = len(vars)
        for val in range(n):
            for var in range(n):
                if assignment.get(var,'') == val: ch ='Q'
                elif (var+val) % 2 == 0: ch = '.'
                else: ch = '-'
                print ch,
            print '    ',
            for var in range(n):
                if assignment.get(var,'') == val: ch ='*'
                else: ch = ' '
                #print str(nconflicts(var, val, assignment))+ch, 
            print        


    solution = mc.min_conflicts(vars, domains, constraints, neighbors)
    if show:
        if solution[0]:
            print
            print "Solution took",solution[1],"steps"
            display(vars,solution[0])
        else:
            print "No solution found."

    return solution[1]

if __name__ == "__main__":
    for i in range(3):
        queens()
"""
