# Search.py, written by Kyle Smith for CS440, Assignment 3

def iterativeDeepeningSearch(startState, actionsF, takeActionF, goalTestF, maxDepth) :
    nodes_expanded = 0
    for depthLimit in range(maxDepth) :
        passState = startState[:]
        solution, depth = depthLimitedSearchHelper(passState, actionsF, takeActionF, goalTestF, depthLimit)
        nodes_expanded += depth
        if solution[0] != "cutoff" :
            return solution, nodes_expanded
    result = ["cutoff"]
    return result, nodes_expanded
    

def depthLimitedSearchHelper(state, actionsF, takeActionF, goalTestF, depthLimit) :
    count = 0
    test = goalTestF(state)
    if test == True :
        result = [state]
        return result, count
    elif depthLimit == 0 :
        result = ["cutoff"]
        return result, count
    else :
        tempState = state[:]
        cutoff_occurred = False
        actions = actionsF(tempState)
        count += len(actions)
        for action in actions :
            result, depth = depthLimitedSearchHelper(takeActionF(tempState, action), actionsF, takeActionF, goalTestF, depthLimit - 1)
            count += depth
            if result[0] == "cutoff" :
                cutoff_occurred = True
            elif result[0] != "failure" :
                result.insert(0, tempState)
                return result, count
        if cutoff_occurred == True :
            result = ["cutoff"]
        else :
            result = ["failure"]
        return result, count


class Node:
    def __init__(self, state, f=0, g=0 ,h=0):
        self.state = state
        self.f = f
        self.g = g
        self.h = h
    def __repr__(self):
        return "Node(" + repr(self.state) + ", f=" + repr(self.f) + \
               ", g=" + repr(self.g) + ", h=" + repr(self.h) + ")"

def RBFS(startState, actionsF, takeActionF, goalTestF, hF):
    h = hF(startState)
    startNode = Node(state=startState, f=0+h, g=0, h=h)
    return RBFSHelper(startNode, actionsF, takeActionF, goalTestF, hF, float('inf'))

def RBFSHelper(parentNode, actionsF, takeActionF, goalTestF, hF, fmax):
    node_count = 0
    if goalTestF(parentNode.state):
        return ([parentNode.state], parentNode.g, node_count)
    ## Construct list of children nodes with f, g, and h values
    actions = actionsF(parentNode.state)
    node_count += len(actions)
    if not actions:
        return ("failure", float('inf'),node_count)
    children = []
    for action in actions:
        (childState,stepCost) = takeActionF(parentNode.state, action)
        h = hF(childState)
        g = parentNode.g + stepCost
        f = max(h+g, parentNode.f)
        childNode = Node(state=childState, f=f, g=g, h=h)
        children.append(childNode)
    while True:
        # find best child
        children.sort(key = lambda n: n.f) # sort by f value
        bestChild = children[0]
        if bestChild.f > fmax:
            return ("failure",bestChild.f,node_count)
        # next lowest f value
        alternativef = children[1].f if len(children) > 1 else float('inf')
        # expand best child, reassign its f value to be returned value
        result,bestChild.f,more_nodes = RBFSHelper(bestChild, actionsF, takeActionF, goalTestF,
                                        hF, min(fmax,alternativef))
        node_count += more_nodes
        if result is not "failure":
            result.insert(0,parentNode.state)
            return (result, bestChild.f,node_count)
