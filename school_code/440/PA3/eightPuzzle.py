# eightPuzzle.py written by Kyle Smith for CS440, Assignment 3

import search

goalStateOne = [1, 2, 3, 4, 0, 5, 6, 7, 8]
goalStateTwo = [1, 2, 3, 4, 5, 8, 6, 0, 7]
goalStateThree = [1, 0, 3, 4, 5, 8, 2, 6, 7]

def printState(state) :
    output = ""
    for j in range (9) :
        output += repr(state[j]) + " "
        if (j + 1) % 3 == 0 and j != 8:
            output += "\n"
    print output

def actionsF(state) :
    result = []
    for i in range(9) :
        if state[i] == 0 :
            break
    if i == 0 :
        result = ['right', 'down']
    elif i == 1 :
        result = ['right', 'left', 'down']
    elif i == 2 :
        result = ['left', 'down']
    elif i == 3 :
        result = ['up', 'right', 'down']
    elif i == 4 :
        result = ['up', 'right', 'left', 'down']
    elif i == 5 :
        result = ['up', 'left', 'down']
    elif i == 6 :
        result = ['up', 'right']
    elif i == 7 :
        result = ['up', 'right', 'left']
    elif i == 8 :
        result = ['up', 'left']
    return result

def actionsHF(state) :
    moves = actionsF(state)
    result = [(i, 1) for i in moves]
    return result

def takeActionF(state, action) :
    result = state[:]
    for i in range(9) :
        if result[i] == 0 :
            break
    if action == 'up' :
        result[i] = result[i-3]
        result[i-3] = 0
    if action == 'right' :
        result[i] = result[i+1]
        result[i+1] = 0
    if action == 'left' :
        result[i] = result[i-1]
        result[i-1] = 0
    if action == 'down' :
        result[i] = result[i+3]
        result[i+3] = 0
    return result

def takeActionHF(state, action) :
    result = state[:]
    for i in range(9) :
        if result[i] == 0 :
            break
    if action == 'up' :
        result[i] = result[i-3]
        result[i-3] = 0
    if action == 'right' :
        result[i] = result[i+1]
        result[i+1] = 0
    if action == 'left' :
        result[i] = result[i-1]
        result[i-1] = 0
    if action == 'down' :
        result[i] = result[i+3]
        result[i+3] = 0
    return (result, 1)




def goalTestOneF(state) :

    if state == goalStateOne :
        result = True
    else :
        result = False
    return result

def goalTestTwoF(state) :

    if state == goalStateTwo :
        result = True
    else :
        result = False
    return result

def goalTestThreeF(state) :

    if state == goalStateThree : 
        result = True
    else :
        result = False
    return result

def printResult(startState, goalState, solutionPath) :
    print "Path from"
    printState(startState)
    print "  to"
    printState(goalState)
    if solutionPath[0] == "cutoff" :
        print "was not found due to depth cutoff."
    elif solutionPath[0] == "failure" :
        print "is incalculable; search failed."
    else :
        print "  is " + repr(len(solutionPath)) + " nodes long:"
        for step in solutionPath :
            printState(step)
            print ""

def h1(state) :
    return 0

def h2One(state) :
    return manhatten(state, goalStateOne, 0)

def h2Two(state) :
    return manhatten(state, goalStateTwo, 0)

def h2Three(state) :
    return manhatten(state, goalStateThree, 0)

def h3One(state) :
    result = 0
    for value in range(9) :
        result += manhatten(state, goalStateOne, value)
    return result

def h3Two(state) :
    result = 0
    for value in range(9) :
        result += manhatten(state, goalStateTwo, value)
    return result

def h3Three(state) :
    result = 0
    for value in range(9) :
        result += manhatten(state, goalStateThree, value)
    return result

def manhatten(state, goalState, value) :
    #Find indecies of the blank states in the start and goal states
    for i in range(9) :
        if state[i] == value :
            break
    for j in range(9) :
        if goalState[j] == value :
            break

    #Convert the indecies into x any y coordinates
    xi = i % 3
    xj = j % 3

    if i >= 0 and i <= 2 :
        yi = 0
    elif i >= 3 and i <= 5 :
        yi = 1
    else:
        yi = 2

    if j >= 0 and j <= 2 :
        yj = 0
    elif j >= 3 and j <= 5 :
        yj = 1
    else:
        yj = 2

    return abs(xi - xj) + abs(yi - yj)

def ebf(numNodes, depth, precision = 0.01) :
    if depth == 0 :
        return 0
    guess = numNodes / 2.0
    change_amount = numNodes / 2.0
    result = -1
    while abs(result-numNodes) > precision :
        result = calc_ebf(guess, depth)
        if not abs(result-numNodes) < precision :
            change_amount = change_amount / 2
            if numNodes > result :
                guess += change_amount
            else :
                guess -= change_amount
    return guess

def calc_ebf(guess, depth) :
    if guess == 1 :
        guess -= 0.00000000001
    return (1-guess**(depth+1))/(1-guess)

if __name__ == "__main__" :
    startState = [1, 2, 3, 4, 0, 5, 6, 7, 8]

    idsOne, idsOneDepth = search.iterativeDeepeningSearch(startState, actionsF, takeActionF, goalTestOneF, 10)
    idsTwo, idsTwoDepth = search.iterativeDeepeningSearch(startState, actionsF, takeActionF, goalTestTwoF, 15)
    idsThree, idsThreeDepth = search.iterativeDeepeningSearch(startState, actionsF, takeActionF, goalTestThreeF, 14)
    a = len(idsOne) - 1
    b = idsOneDepth
    c = ebf(b, a)
    d = len(idsTwo) - 1
    e = idsTwoDepth
    f = ebf(e, d)
    g = len(idsThree) - 1
    h = idsThreeDepth
    i = ebf(h, g)

    (h1One,zero,h1OneNodes) = search.RBFS(startState, actionsF, takeActionHF, goalTestOneF, h1)
    (h1Two,zero,h1TwoNodes) = search.RBFS(startState, actionsF, takeActionHF, goalTestTwoF, h1)
    (h1Three,zero,h1ThreeNodes) = search.RBFS(startState, actionsF, takeActionHF, goalTestThreeF, h1)
    j = h1OneNodes
    k = len(h1One) - 1
    l = ebf(j, k)
    m = h1TwoNodes
    n = len(h1Two) - 1
    o = ebf(m, n)
    p = h1ThreeNodes
    q = len(h1Three) - 1
    r = ebf(p, q)

    (h2One,zero,h2OneNodes) = search.RBFS(startState, actionsF, takeActionHF, goalTestOneF, h2One)
    (h2Two,zero,h2TwoNodes) = search.RBFS(startState, actionsF, takeActionHF, goalTestTwoF, h2Two)
    (h2Three,zero,h2ThreeNodes) = search.RBFS(startState, actionsF, takeActionHF, goalTestThreeF, h2Three)
    aa = h2OneNodes
    bb = len(h2One) - 1
    cc = ebf(aa, bb)
    dd = h2TwoNodes
    ee = len(h2Two) - 1
    ff = ebf(dd, ee)
    gg = h2ThreeNodes
    hh = len(h2Three) - 1
    ii = ebf(gg, hh)

    (h3One,zero,h3OneNodes) = search.RBFS(startState, actionsF, takeActionHF, goalTestOneF, h3One)
    (h3Two,zero,h3TwoNodes) = search.RBFS(startState, actionsF, takeActionHF, goalTestTwoF, h3Two)
    (h3Three,zero,h3ThreeNodes) = search.RBFS(startState, actionsF, takeActionHF, goalTestThreeF, h3Three)
    jj = h3OneNodes
    kk = len(h3One) - 1
    ll = ebf(jj, kk)
    mm = h3TwoNodes
    nn = len(h3Two) - 1
    oo = ebf(mm, nn)
    pp = h3ThreeNodes
    qq = len(h3Three) - 1
    rr = ebf(pp, qq)

    print "       " + repr(goalStateOne) + "     " + repr(goalStateTwo) + "    " + repr(goalStateThree)
    print "Algorithm    Depth  Nodes  EBF              Depth  Nodes  EBF              Depth  Nodes  EBF"
    print "     IDS" + '%8d %6d %6.3f %16d %6d %6.3f %16d %6d %6.3f' % (a, b, c, d, e, f, g, h, i)
    print "    A*h1" + '%8d %6d %6.3f %16d %6d %6.3f %16d %6d %6.3f' % (k, j, l, n, m, o, q, p, r)
    print "    A*h2" + '%8d %6d %6.3f %16d %6d %6.3f %16d %6d %6.3f' % (bb, aa, cc, ee, dd, ff, hh, gg, ii)
    print "    A*h3" + '%8d %6d %6.3f %16d %6d %6.3f %16d %6d %6.3f' % (kk, jj, ll, nn, mm, oo, qq, pp, rr)
