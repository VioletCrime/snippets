# puzzle.py; implements the camels puzzle

import search
reload(search)

init_state = "r r r r e l l l l"
goal_state = "l l l l e r r r r"

def createString(inList) :
    result = ""
    for i in range(8) :
        result += inList[i] + " "
    result += inList[8]
    return result

def successorsf(state) :
    if state is None :
        return None
    result = []
    camels = state.split()
    for i in range(9) : # Find the empty space amongst the camels
        if camels[i] == 'e' :
            if i-2 >= 0 :  
                if camels[i-2] == 'r' :
                    newCamel = camels[:]
                    newCamel[i-2] = 'e'
                    newCamel[i] = 'r'
                    result.append(createString(newCamel))
            if i-1 >= 0 :
                if camels[i-1] == 'r' :
                    newCamel = camels[:]
                    newCamel[i-1] = 'e'
                    newCamel[i] = 'r'
                    result.append(createString(newCamel))
            if i+1 <= 8 :
                if camels[i+1] == 'l' :
                    newCamel = camels[:]
                    newCamel[i+1] = 'e'
                    newCamel[i] = 'l'
                    result.append(createString(newCamel))
            if i+2 <= 8 :
                if camels[i+2] == 'l' :
                    newCamel = camels[:]
                    newCamel[i+2] = 'e'
                    newCamel[i] = 'l'
                    result.append(createString(newCamel))

    # print "successorsf returning: " + 

    if len(result) == 0 :
        return None
    else :
        return result


print "Breadth-first solution of the camels puzzle:", search.breadthFirstSearch(init_state, goal_state, successorsf)
print "Depth-first solution of the camels puzzle:", search.depthFirstSearch(init_state, goal_state, successorsf)
