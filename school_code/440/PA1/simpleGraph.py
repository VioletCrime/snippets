# simpleGraph.py

import search
reload(search)

successors = {'a':  ['b', 'c', 'd'],
              'b':  ['e', 'f', 'g'],
              'c':  ['a', 'h', 'i'],
              'd':  ['j', 'z'],
              'e':  ['k', 'l'],
              'g':  ['m'],
              'k':  ['z']}

def successorsf(state) :
    if state in successors :
        return successors[state]
    else :
        return None

print "Breadth-first"
print "path from a to a is", search.breadthFirstSearch('a','a', successorsf)
print "path from a to m is", search.breadthFirstSearch('a','m', successorsf)
print "path from a to z is", search.breadthFirstSearch('a','z', successorsf)

print "Depth-first"
print "path from a to a is", search.depthFirstSearch('a','a', successorsf)
print "path from a to m is", search.depthFirstSearch('a','m', successorsf)
print "path from a to z is", search.depthFirstSearch('a','z', successorsf)
