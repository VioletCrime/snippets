def breadthFirstSearch(startState, goalState, successorsf) :
    return search(startState, goalState, successorsf, True)

def depthFirstSearch(startState, goalState, successorsf) :
    return search(startState, goalState, successorsf, False)

def search(startState, goalState, successorsf, isBreadth) :
    expanded = {}
    unExpanded = [(startState, None)]
    if goalState == startState :
        result = [startState]
        return result
    while len(unExpanded) > 0 :
        pair = unExpanded.pop()
        expanded[pair[0]] = pair[1]
        connected = successorsf(pair[0])
        if connected is not None :
            for child in connected :
                if child == goalState:
                    result = [pair[0], goalState]
                    parent = expanded[pair[0]]
                    while parent is not None :
                        result.insert(0, parent)
                        parent = expanded[parent]
                    return result
                if not child in expanded :
                    newPair = (child, pair[0])
                    if isBreadth :
                        unExpanded.insert(0, newPair)
                    else :
                        unExpanded.append(newPair) 
