
public class Seat {
	/*
	 * This class maintains information about seats. Specifically, a seat has an identifier 
	 * (a seat is	identified by a row number and a column character, where the character is a letter from A to J), 
	 * and a status which indicates whether the seat is booked or not.
	 * 
	 * 
	 * 
	 * todo
	 * 
	 * 
	 */
	
	private int row;
	private int col;
	private int status; // 0 is empty and one is full no other integers are valid
	
	public Seat(int row, int col, int status){
		setRow(row);
		setCol(col);
		if(status == 1 || status == 0)
			setStatus(status);
		else
			// throw exception;
	}
	
	public int getRow(){
		return row;
	}
	
	public int setRow(row){
		this.row = row;
	}
	
	public int getCol(){
		return col;
	}
	
	public int setCol(col){
		this.col = col;
	}
	
	public int getStatus(){
		return Status;
	}
	
	public int setStatus(status){
		this.status = status;
	}
}
