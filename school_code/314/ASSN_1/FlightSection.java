
public class FlightSection {
	/*
	 * This class maintains information about flight sections. A flight section has a seat class (first, business or economy) and must have at least 1 seat.
	 * A flight section can contain at most 100 rows of seats and at most 10 columns of seats.
	 * 
	 * hasAvailableSeats() returns true iff the section has some seats that are not booked
	 * 
	 * bookSeat() books an available seat
	 * 
	 * createSection(String	air, String flID, int rows, int cols, SeatClass s): Creates a section, of class s, for a flight with identifier flID, associated with an airline, air.
	 * The	section	will contain the input number of rows and columns.
	 * 
	 * todo
	 * 	
	 * 
	 */
	
	private String air;
	private String flID
	private int rows;
	private int cols;
	private SeatClass sc;
	
	public void createSection(String air, String flID, int rows, int cols, SeatClass sc){
		setAir(air);
		setFlID(flID);
		setRows(rows);
		setCols(cols);
		setSc(sc);
	}
	
	public bool hasAvailableSeats()
	
	public Seat bookSeat()
	
	public String getAir() {
		return air;
	}

	public void setAir(String air) {
		this.air = air;
	}
	
	public String getFlID() {
		return flID;
	}

	public void setFlID(String flID) {
		this.flID = flId;
	}
	
	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}
	
	public int getCols() {
		return cols;
	}

	public void setCols(int cols) {
		this.cols = cols;
	}
	
	public SeatClass getSc() {
		return sc;
	}

	public void setSc(int sc) {
		this.sc = sc;
	}
}
