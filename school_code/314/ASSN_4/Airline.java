// The Airline class is responsible for the name of the airline it represents,
// and the flights associated with that airline.
public class Airline extends TravelCompany{	
	
	// Constructor
	public Airline(String airline_name){
		super(airline_name);
	}
	
	// Creates a flight associated with this airline, adds it to 'flightsLL'
	public void createConnection(String air_name, String[] route, int year, int month, int day, String flight_id){
		Flight fly = new Flight(air_name,  route,  year,  month,  day,  flight_id);
		super.connectionsLL.add(fly);
	}
	
	public String toString(){
		String result = "\nAirline: " + getName();
		for(int i = 0; i < connectionsLL.size(); i++){
			result += "\n" + connectionsLL.get(i).toString();
		}
		return result;
	}
	
	public Flight getFlight(String flID){
        Flight result = null;
        for(int index = 0; index < connectionsLL.size(); index++){
                if(connectionsLL.get(index).getConnection_id().equals(flID)){
                        result = (Flight) connectionsLL.get(index);
                        break;
                }
        }
        return result;
	}
}
