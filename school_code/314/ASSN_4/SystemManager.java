import java.util.LinkedList;

public class SystemManager{


	private LinkedList<Airport> airportsLL = new LinkedList<Airport>();
	private LinkedList<Airline> airlinesLL = new LinkedList<Airline>();
	private LinkedList<Seaport> seaportsLL = new LinkedList<Seaport>();
	private LinkedList<Cruiseline> cruiselinesLL = new LinkedList<Cruiseline>();


	public void createAirport(String airport_name){
		if(isValidAirportName(airport_name)){ // If the airport name is valid...
			if(!isAirportInList(airport_name)){ // ... and the name isn't already taken...
				Airport air = new Airport(airport_name);
				airportsLL.add(air); // ... then add the airport.
			}
			else{ // Airport name was taken; inform user.
				System.out.println("Unable to add airport \"" + airport_name + "\": an airport with that name already exists.");
			}
		}
	}

	public void createSeaport(String seaport_name){
		if(isValidAirportName(seaport_name)){ // If the airport name is valid...
			if(!isSeaportInList(seaport_name)){ // ... and the name isn't already taken...
				Seaport sea = new Seaport(seaport_name);
				seaportsLL.add(sea); // ... then add the airport.
			}
			else{ // Airport name was taken; inform user.
				System.out.println("Unable to add seaport \"" + seaport_name + "\": a seaport with that name already exists.");
			}
		}
	}


	public void createAirline(String airline_name){
		if(isValidAirlineName(airline_name)){ // If the airline name is valid...
			if(!isAirlineInList(airline_name)){ // ... and the name isn't already taken...
				Airline air = new Airline(airline_name);
				airlinesLL.add(air); // ... then add the airline.
			}
			else{
				System.out.println("Unable to add airline \"" + airline_name + "\": and airline with that name already exists.");
			}
		}
	}

	public void createCruiseline(String sealine_name){
		if(isValidAirlineName(sealine_name)){ // If the airline name is valid...
			if(!isCruiselineInList(sealine_name)){ // ... and the name isn't already taken...
				Cruiseline air = new Cruiseline(sealine_name);
				cruiselinesLL.add(air); // ... then add the airline.
			}
			else{
				System.out.println("Unable to add cruiseline \"" + sealine_name + "\": a cruiseline with that name already exists.");
			}
		}
	}

	public void createFlight(String airline_name, String[] route, int year, int month, int day, String flight_id){
		boolean good_to_go = true;
		for(int i = 0; i < route.length; i++){
			if(!isAirportInList(route[i])){
				System.out.println("Cannot create flight " + airline_name + " " + flight_id + ": airport \"" + route[i] + "\" does not exist.");
				good_to_go = false;
				break;
			}
		}

		if(route.length < 2){
			System.out.println("Cannot create flight " + airline_name + " " + flight_id + ": specified route is too short.");
			good_to_go = false;
		}

		// Test airline exists
		if(!isAirlineInList(airline_name)){
			System.out.println("Cannot create flight " + airline_name + " " + flight_id + ": airline \"" + airline_name + "\" does not exist.");
			good_to_go = false;
		}

		// Test flight id validity
		if(!isValidID(flight_id)){
			System.out.println("Cannot create flight " + airline_name + " " + flight_id + ": flight_id \"" + flight_id + "\" is not valid.");
			good_to_go = false;
		}

		//Test flight id is not taken
		if(isFlightInList(flight_id, airline_name)){
			System.out.println("Cannot create flight " + airline_name + " " + flight_id + ": flight " + airline_name + " " + flight_id + " already exists");
			good_to_go = false;
		}

		// Test flight date
		if(year <= 2012){
			System.out.println("Cannot create flight " + airline_name + " " + flight_id + ": year (" + year + ") is before 2013");
			good_to_go = false;
		}

		// All tests passed; create the flight.
		if(good_to_go){
			getAirline(airline_name).createConnection(airline_name, route, year, month, day, flight_id);
		}
	}

	public void createCruise(String cruiseline_name, String[] route, int year, int month, int day, String flight_id){
		boolean good_to_go = true;
		for(int i = 0; i < route.length; i++){
			if(!isSeaportInList(route[i])){
				System.out.println("Cannot create cruise " + cruiseline_name + " " + flight_id + ": seaport \"" + route[i] + "\" does not exist.");
				good_to_go = false;
				break;
			}
		}

		if(route.length < 2){
			System.out.println("Cannot create cruise " + cruiseline_name + " " + flight_id + ": specified route is too short.");
			good_to_go = false;
		}

		// Test airline exists
		if(!isCruiselineInList(cruiseline_name)){
			System.out.println("Cannot create cruise " + cruiseline_name + " " + flight_id + ": cruiseline \"" + cruiseline_name + "\" does not exist.");
			good_to_go = false;
		}

		// Test flight id validity
		if(!isValidID(flight_id)){
			System.out.println("Cannot create cruise " + cruiseline_name + " " + flight_id + ": cruise_id \"" + flight_id + "\" is not valid.");
			good_to_go = false;
		}

		//Test flight id is not taken
		if(isCruiseInList(flight_id, cruiseline_name)){
			System.out.println("Cannot create flight " + cruiseline_name + " " + flight_id + ": flight " + cruiseline_name + " " + flight_id + " already exists");
			good_to_go = false;
		}

		// Test flight date
		if(year <= 2012){
			System.out.println("Cannot create flight " + cruiseline_name + " " + flight_id + ": year (" + year + ") is before 2013");
			good_to_go = false;
		}

		// All tests passed; create the flight.
		if(good_to_go){
			getCruiseline(cruiseline_name).createConnection(cruiseline_name, route, year, month, day, flight_id);
		}
	}


	//Creates a section on a given airline flight, provided all tests pass.
	public void createFlightSection(String air_name, String flID, int rows, int cols, SeatClass sc){
		boolean valid_flight_info = true;
		boolean flight_section_test = true;
		//Is the given information valid?
		if(rows > 100 || rows < 1){
			valid_flight_info = false;
			System.out.println("Unable to create section '" + sc + "' on flight " + air_name + " " + flID + ": 'rows' (" + rows + ") out of range (1-100).");
		}
		else if(cols > 10 || cols < 1){
			valid_flight_info = false;
			System.out.println("Unable to create section '" + sc + "' on flight " + air_name + " " + flID + ": 'cols' (" + cols + ") out of range (1-10).");
		}
		else if(!isValidAirlineName(air_name)){
			valid_flight_info = false;
			System.out.println("Unable to create section '" + sc + "' on flight " + air_name + " " + flID + ": \"" + air_name + "\" is not a valid name.");
		}
		else if(!isAirlineInList(air_name)){
			valid_flight_info = false;
			System.out.println("Unable to create section '" + sc + "' on flight " + air_name + " " + flID + ": airline " + air_name + " does not exist.");
		}
		else if(!isValidID(flID)){
			valid_flight_info = false;
			System.out.println("Unable to create section '" + sc + "' on flight " + air_name + " " + flID + ": \"" + flID + "\" is not a valid flight_id.");
		}
		else if(!isFlightInList(flID, air_name)){
			System.out.println("Unable to create section '" + sc + "' on flight " + air_name + " " + flID + ": flight " + flID + " does not exist.");
			valid_flight_info = false;
		}

		if(valid_flight_info){
			Flight flight = getFlight(flID, air_name);
			//for all flight sections
			for(int section=0; section < flight.getFlightsSectionsLL().size(); section++){
				// if the section for that flight already exists test = false
				if(((FlightSection) flight.getFlightsSectionsLL().get(section)).getSection().equals(sc)){
					System.out.println("Unable to create section '" + sc + "' on flight " + air_name + " " + flID + ": section " + sc + " already exists.");
					flight_section_test = false;
				}
			}
			if(flight_section_test){
				flight.createSection(air_name, flID, rows, cols, sc);
			}
		}
	}

	public void createCruiseSection(String air_name, String flID, int rows, int cols, CabinClass cc){
		boolean valid_flight_info = true;
		boolean flight_section_test = true;
		//Is the given information valid?
		if(rows > 100 || rows < 1){
			valid_flight_info = false;
			System.out.println("Unable to create section '" + cc + "' on cruise " + air_name + " " + flID + ": 'rows' (" + rows + ") out of range (1-100).");
		}
		else if(cols > 10 || cols < 1){
			valid_flight_info = false;
			System.out.println("Unable to create section '" + cc + "' on cruise " + air_name + " " + flID + ": 'cols' (" + cols + ") out of range (1-10).");
		}
		else if(!isValidAirlineName(air_name)){
			valid_flight_info = false;
			System.out.println("Unable to create section '" + cc + "' on cruise " + air_name + " " + flID + ": \"" + air_name + "\" is not a valid name.");
		}
		else if(!isCruiselineInList(air_name)){
			valid_flight_info = false;
			System.out.println("Unable to create section '" + cc + "' on cruise " + air_name + " " + flID + ": sealine " + air_name + " does not exist.");
		}
		else if(!isValidID(flID)){
			valid_flight_info = false;
			System.out.println("Unable to create section '" + cc + "' on cruise " + air_name + " " + flID + ": \"" + flID + "\" is not a valid cruise_id.");
		}
		else if(!isCruiseInList(flID, air_name)){
			System.out.println("Unable to create section '" + cc + "' on cruise " + air_name + " " + flID + ": cruise ID " + flID + " does not exist.");
			valid_flight_info = false;
		}

		if(valid_flight_info){
			Cruise flight = getCruise(flID, air_name);
			//for all flight sections
			for(int section=0; section < flight.getFlightsSectionsLL().size(); section++){
				// if the section for that flight already exists test = false
				if(((CabinSection) flight.getFlightsSectionsLL().get(section)).getSection().equals(cc)){
					System.out.println("Unable to create section '" + cc + "' on flight " + air_name + " " + flID + ": section " + cc + " already exists.");
					flight_section_test = false;
				}
			}
			if(flight_section_test){
				flight.createSection(air_name, flID, rows, cols, cc);
			}
		}
	}

	// Finds all available flights between two airports
	public void findAvailableFlights(String orig, String dest){
		// Test the origin and destination strings
		boolean airports_valid = false;
		if(isValidAirportName(orig) && isValidAirportName(dest)){
			if(!isAirportInList(orig)){
				System.out.println("Cannot find flight between " + orig + " and " + dest + ": airport " + orig + " does not exist.");
			}
			else if(!isAirportInList(dest)){
				System.out.println("Cannot find flight between " + orig + " and " + dest + ": airport " + dest + " does not exist.");
			}
			else{
				airports_valid = true;
			}
		}

		if(airports_valid){
			LinkedList<Flight> available_fl = new LinkedList<Flight>();

			// For all airlines
			for(int airline=0; airline<airlinesLL.size(); airline++){
				// For all flights
				for(int flight=0; flight<airlinesLL.get(airline).getConnectionsLL().size(); flight++){
					// If the origin and destination match
					if(airlinesLL.get(airline).getConnectionsLL().get(flight).getRoute()[0].equals(orig) && airlinesLL.get(airline).getConnectionsLL().get(flight).getRoute()[airlinesLL.get(airline).getConnectionsLL().get(flight).getRoute().length - 1].equals(dest)){
						available_fl.add((Flight) airlinesLL.get(airline).getConnectionsLL().get(flight));
					}
				}
			}

			// prints information on available flights
			System.out.println("These are the available flights:\n");
			for(int flight=0; flight<available_fl.size(); flight++){
				System.out.println(available_fl.get(flight));
				System.out.println();
			}
		}
	}

	public void findAvailableCruises(String orig, String dest){
		// Test the origin and destination strings
		boolean airports_valid = false;
		if(isValidAirportName(orig) && isValidAirportName(dest)){
			if(!isSeaportInList(orig)){
				System.out.println("Cannot find cruise between " + orig + " and " + dest + ": seaport " + orig + " does not exist.");
			}
			else if(!isSeaportInList(dest)){
				System.out.println("Cannot find cruise between " + orig + " and " + dest + ": seaport " + dest + " does not exist.");
			}
			else{
				airports_valid = true;
			}
		}

		if(airports_valid){
			LinkedList<Cruise> availableLL = new LinkedList<Cruise>();

			// For all airlines
			for(int cruiseline=0; cruiseline<cruiselinesLL.size(); cruiseline++){
				// For all flights
				for(int cruise=0; cruise<cruiselinesLL.get(cruiseline).getConnectionsLL().size(); cruise++){
					// If the origin and destination match
					if(cruiselinesLL.get(cruiseline).getConnectionsLL().get(cruise).getRoute()[0].equals(orig) && cruiselinesLL.get(cruiseline).getConnectionsLL().get(cruise).getRoute()[cruiselinesLL.get(cruiseline).getConnectionsLL().get(cruise).getRoute().length-1].equals(dest)){
						availableLL.add((Cruise) cruiselinesLL.get(cruiseline).getConnectionsLL().get(cruise));
					}
				}
			}

			// prints information on available flights
			System.out.println("These are the available cruises:\n");
			for(int flight=0; flight<availableLL.size(); flight++){
				System.out.println(availableLL.get(flight));
			}
		}
	}

	//Isolate the flight we need to book the seat on and call that flight's bookSeat() method
	public void bookSeat(String air_name, String flID, SeatClass sc, int row, char col){
		boolean found_airline = false;
		boolean found_flight = false;
		for(int airline=0; airline<airlinesLL.size(); airline++){
			if(airlinesLL.get(airline).getName().equals(air_name)){
				found_airline = true;
				for(int flight=0; flight<airlinesLL.get(airline).getConnectionsLL().size(); flight++){
					if(airlinesLL.get(airline).getConnectionsLL().get(flight).getConnection_id().equals(flID)){
						found_flight = true;
						((Flight) airlinesLL.get(airline).getConnectionsLL().get(flight)).bookSeat(sc, row, col);
					}
				}
			}
		}
		if(!found_airline){
			System.out.println("Unable to book seat " + row + col + " on " + air_name + " flight " + flID + ", airline was not found.");
		}
		else if(!found_flight){
			System.out.println("Unable to book seat " + row + col + " on " + air_name + " flight " + flID + ", flight was not found.");
		}
	}

	public void bookCabin(String air_name, String flID, CabinClass cc, int row, char col){
		boolean found_airline = false;
		boolean found_flight = false;
		for(int cruiseline=0; cruiseline<cruiselinesLL.size(); cruiseline++){
			if(cruiselinesLL.get(cruiseline).getName().equals(air_name)){
				found_airline = true;
				for(int cruise=0; cruise<cruiselinesLL.get(cruiseline).getConnectionsLL().size(); cruise++){
					if(cruiselinesLL.get(cruiseline).getConnectionsLL().get(cruise).getConnection_id().equals(flID)){
						found_flight = true;
						((Cruise) cruiselinesLL.get(cruiseline).getConnectionsLL().get(cruise)).bookSeat(cc, row, col);
					}
				}
			}
		}
		if(!found_airline){
			System.out.println("Unable to book cabin " + row + col + " on " + air_name + " cruise " + flID + ", cruiseline was not found.");
		}
		else if(!found_flight){
			System.out.println("Unable to book cabin " + row + col + " on " + air_name + " cruise " + flID + ", cruise was not found.");
		}
	}

	public void displayAirlineSystemDetails(){
		System.out.println("\nAirports:");
		for(int airport=0; airport<airportsLL.size(); airport++){
			System.out.print(airportsLL.get(airport).getPort_name());
			System.out.print(" ");
		}
		System.out.println("\n");

		System.out.println("Airlines and flight information:");
		// for all airlines
		for(int airline=0; airline<airlinesLL.size(); airline++){
			System.out.println(airlinesLL.get(airline));
		}
	}

	public void displayCruiselineSystemDetails(){
		System.out.println("\nSeaports:");
		for(int seaport=0; seaport<seaportsLL.size(); seaport++){
			System.out.print(seaportsLL.get(seaport).getPort_name());
			System.out.print(" ");
		}
		System.out.println("\n");

		System.out.println("Cruiselines and cruise information:");
		// for all airlines
		for(int cruiseline=0; cruiseline<cruiselinesLL.size(); cruiseline++){
			System.out.println(cruiselinesLL.get(cruiseline));
		}
	}

	// ===========  HELPER METHODS! ===============================================================

	// ================== AIRPORT HELPERS =====================================================

	//Is the passed string a valid airport name? Return true if valid.
	public boolean isValidAirportName(String airport_name){
		boolean result = true;
		//Is it the appropriate length?
		if(airport_name.length() != 3){
			System.out.println("Port identifier \"" + airport_name + "\" invalid: identifier must be three letters long");
			result = false;
		}
		//Is each character a letter?
		for(int character = 0; character < airport_name.length(); character++){
			if(!Character.isLetter(airport_name.charAt(character))){
				System.out.println("Airport identifier \"" + airport_name + "\" invalid: identifier must consist of letters.");
				result = false;
				break;
			}
		}
		return result;
	}

	//Is the passed string the name of an existing airport? Return true if airport name exists in list of airports.
	public boolean isAirportInList(String airport_name){
		boolean result = false;
		for(int airport=0; airport<airportsLL.size(); airport++){
			if(airportsLL.get(airport).getPort_name().equals(airport_name)){
				result = true;
				break;
			}
		}
		return result;
	}

	public boolean isSeaportInList(String seaport_name){
		boolean result = false;
		for(int seaport=0; seaport<seaportsLL.size(); seaport++){
			if(seaportsLL.get(seaport).getPort_name().equals(seaport_name)){
				result = true;
				break;
			}
		}
		return result;
	}

	// Get an airport object from the 3-letter identifier. Return null if not found.
	public Airport getAirport(String airport_name){
		Airport result = null;
		for(int index = 0; index < airportsLL.size(); index++){
			if(airportsLL.get(index).getPort_name().equals(airport_name)){
				result = airportsLL.get(index);
				break;
			}
		}
		return result;
	}


	// ================== AIRLINE HELPERS =====================================================

	// Is Airline name valid? Return true if valid.
	public boolean isValidAirlineName(String airline_name){
		boolean result = true;
		//Is the name the appropriate length?
		if(airline_name.length() > 5 || airline_name.length() < 1){ // Is the name too long?
			System.out.println("Airline name \"" + airline_name + "\" invalid: name must be between 1 and 5 characters.");
			result = false;
		}
		//Is each character a letter?
		for(int character = 0; character < airline_name.length(); character++){
			if(!Character.isLetter(airline_name.charAt(character))){
				System.out.println("Airline name \"" + airline_name + "\" invalid: name must consist of letters.");
				result = false;
			}
		}
		return result;
	}

	// Is Airline name taken? Return true if an airline already has the name.
	public boolean isAirlineInList(String airline_name){
		boolean result = false;
		for(int airline=0; airline<airlinesLL.size(); airline++){
			if(airlinesLL.get(airline).getName().equals(airline_name)){
				result = true;
			}
		}
		return result;
	}

	public boolean isCruiselineInList(String cruiseline_name){
		boolean result = false;
		for(int cruiseline=0; cruiseline<cruiselinesLL.size(); cruiseline++){
			if(cruiselinesLL.get(cruiseline).getName().equals(cruiseline_name)){
				result = true;
			}
		}
		return result;
	}

	//Get Airline object from airline name. Return null if not found.
	public Airline getAirline(String airline_name){
		Airline result = null;
		for(int index = 0; index < airlinesLL.size(); index++){
			if(airlinesLL.get(index).getName().equals(airline_name)){
				result = airlinesLL.get(index);
				break;
			}
		}
		return result;
	}

	public Cruiseline getCruiseline(String cruiseline_name){
		Cruiseline result = null;
		for(int index = 0; index < cruiselinesLL.size(); index++){
			if(cruiselinesLL.get(index).getName().equals(cruiseline_name)){
				result = cruiselinesLL.get(index);
				break;
			}
		}
		return result;
	}


	// ================== FLIGHT HELPERS =====================================================

	// Is flight ID valid? Return true if so.
	public boolean isValidID(String flID){
		boolean result = true;
		// Test length of ID
		if(flID.length() > 6 || flID.length() < 3){
			result = false;
			System.out.println("Flight ID " + flID + " is invalid: ID must be between 3 and 6 characters");
		}
		if(result){
			// Test each character is alphanumeric
			for(int index = 0; index < flID.length(); index++){
				if(!Character.isLetterOrDigit(flID.charAt(index))){
					result = false;
					System.out.println("Flight ID " + flID + " is invalid: characters must be alphanumeric");
					break;
				}
			}
		}
		return result;
	}


	// Does flight exist? Return true if so.
	public boolean isFlightInList(String flID, String airline_name){
		boolean result = false;
		Airline airline = getAirline(airline_name);
		if(airline != null){
			for(int index = 0; index < airline.getConnectionsLL().size(); index++){
				if(airline.getConnectionsLL().get(index).getConnection_id().equals(flID)){
					result = true;
					break;
				}
			}
		}
		return result;
	}

	// Does flight exist? Return true if so.
	public boolean isCruiseInList(String flID, String airline_name){
		boolean result = false;
		Cruiseline airline = getCruiseline(airline_name);
		if(airline != null){
			for(int index = 0; index < airline.getConnectionsLL().size(); index++){
				if(airline.getConnectionsLL().get(index).getConnection_id().equals(flID)){
					result = true;
					break;
				}
			}
		}
		return result;
	}

	// Get Flight object using the flight ID and airline servicing the flight. Null if not found.
	public Flight getFlight(String flID, String airline_name){
		Flight result = null;
		Airline airline = getAirline(airline_name);
		if (airline != null){
			result = airline.getFlight(flID);
		}
		return result;
	}

	// Get Flight object using the flight ID and airline servicing the flight. Null if not found.
	public Cruise getCruise(String flID, String cruiseline_name){
		Cruise result = null;
		Cruiseline cruiseline = getCruiseline(cruiseline_name);
		if (cruiseline != null){
			result = cruiseline.getCruise(flID);
		}
		return result;
	}
}
