import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UI {

	static String password;
	static String username;
	static SystemManager sysMan;

	public static boolean whiteSpace(String s){
		Pattern pattern = Pattern.compile("\\s");
		Matcher matcher = pattern.matcher(s);
		boolean found = matcher.find();
		return found;
	}

	public static void main(String[] args){
		boolean valid = false;
		boolean valid2 = true;
		String password1 = "";
		String password2 = "";
		sysMan = new SystemManager();
		Scanner scanIn = new Scanner(System.in);

		System.out.println("Welcome to the General Booking System.\n");
		while(!valid){
			while(valid2){
				System.out.print("Please enter an administration User Name (without any spaces): ");
				username = scanIn.nextLine();
				valid2 = whiteSpace(username);
				if(valid2){
					System.out.println("User Name is invalid due to white space in the entered User Name.");
				}
			}
			valid2 = true;
			while(valid2){
				System.out.print("Please enter an administration Password (without any spaces): ");
				password1 = scanIn.nextLine();
				valid2 = whiteSpace(username);
				if(valid2){
					System.out.println("Password is invalid due to white space in the entered Password.");
				}
			}
			System.out.print("Please re-enter the administration Password: ");
			password2 = scanIn.nextLine();
			if(password1.equals(password2)){
				password = password1;
				valid = true;
			}
			else System.out.println("Passwords did not match.");
		}
		System.out.println();
		login(scanIn);
	}

	public static void login(Scanner scanIn){
		int option = 9001; //its over 9000 

		loginCommands();
		while(true){
			System.out.print("Please select an option (3 for a list of commands): ");
			try{
				option = Integer.parseInt(scanIn.next());
			}
			catch(NumberFormatException nfe){
				scanIn.nextLine();
				System.out.println("Invalid input, try again:");
				option = 3;
			}
			switch(option){
			case 1: // Login as admin
				adminTerminal(scanIn);
				break;
			case 2: // Login as User
				userTerminal(scanIn);
				break;
			case 3: // Print list of commands (this message)
				loginCommands();
				break;
			case 4: // Exit Program
				exitUI();
				break;
			}
		}
	}

	public static String[] makeRoute(Scanner scanIn, boolean flight){
		LinkedList<String> ll = new LinkedList<String>();
		String temp;
		boolean more = true, loopr;
		float days =0, d=0;
		scanIn.nextLine();

		System.out.print("Please enter an origin: ");
		temp = scanIn.nextLine();
		ll.add(temp);
		while(more){
			System.out.print("Please enter a destination: ");
			temp = scanIn.nextLine();
			if(flight){
				d +=1;
			}
			else{
				System.out.print("How many days will it take to get to this destination: ");
				d = -1;
				while(d < 0){
					try{
						d = Integer.parseInt(scanIn.next());
					}
					catch(NumberFormatException nfe){
						scanIn.nextLine();
						System.out.print("Number of days must be a positive integer, please try again:");
						d = -1;
					}
				}
				System.out.print("");
				scanIn.nextLine();
			}
			if((d+days) > 21){
				System.out.println("A trip can not be longer than 21 days. This destination was not added to the trip.\n");
			}
			else{
				if(ll.contains(temp)){
					if(temp.equals(ll.get(0))){
						days += d;
						ll.add(temp);
						System.out.print("This destination is the same as the origin and is therefore the last destination.\n");
						break;
					}
					else{
						System.out.println("This destination is already in the trip and was not the origin. It was therefore not added to the trip.");
					}
				}
				else{
					days += d;
					ll.add(temp);
				}
			}
			System.out.print("Would you like to add another destination: [ yes(y) | no(n) ]: ");
			loopr = false;
			while (!loopr){
				temp = scanIn.nextLine();
				if(temp.toLowerCase().equals("n") || temp.toLowerCase().equals("no")){
					loopr = true;
					more = false;
				}
				if(temp.toLowerCase().equals("y") || temp.toLowerCase().equals("yes")){
					loopr = true;
					more = true;
				}
				if(!loopr){
					System.out.print("  Input appears invalid. Please try again [ yes(y) | no(n) ]: ");
				}
			}
		}

		String[] route = new String[ll.size()];
		for(int i=0;i<ll.size();i++){
			route[i] = ll.get(i);
		}
		return route;
	}

	public static void adminTerminal(Scanner scanIn){
		int option = 9001; //its over 9000 
		boolean verified = false;
		String entered_username, entered_password, airport, airline, flID, seatClass, seaport, sealine, cabinClass, password1, password2, oldpass;
		int year, month, day, rows, cols;
		boolean loopr;
		String[] route;
		CabinClass cc = null;
		SeatClass sc = null;
		scanIn.nextLine();

		while(!verified){
			System.out.print("Please enter the administration User Name: ");
			entered_username = scanIn.nextLine();
			System.out.print("Please enter the administration Password: ");
			entered_password = scanIn.nextLine();
			if(username.equals(entered_username) && password.equals(entered_password)){
				verified = true;
			}
			else System.out.println("You have entered an invalid User Name or Password.\n\nPlease try again:\n");
		}
		adminCommands();
		while(true){
			System.out.print("Please select an option (14 for a list of commands): ");
			try{
				option = Integer.parseInt(scanIn.next());
			}
			catch(NumberFormatException nfe){
				scanIn.nextLine();
				System.out.println("Invalid input, try again:");
				option = 14;
			}
			switch(option){
			case 1: // Create Airport
				System.out.print("\nPlease enter the 3-character identifier of the new airport: ");
				airport = scanIn.next();
				sysMan.createAirport(airport);
				System.out.println();
				break;
			case 2: // Create Airline
				System.out.print("\nPlease enter the new airline's identifier: ");
				airline = scanIn.next();
				sysMan.createAirline(airline);
				System.out.println();
				break;
			case 3: // Create Flight
				System.out.print("\nPlease enter the identifier of the airline servicing the flight: ");
				airline = scanIn.next();

				route = makeRoute(scanIn, true);
				System.out.println("What is the start date for this trip?");
				System.out.print("Year: ");
				year = -1;
				while(year < 2013){
					try{
						year = Integer.parseInt(scanIn.next());
						scanIn.nextLine();
						if(year < 2013){
							System.out.print("Year must be an integer greater than 2013, try again:");
						}
					}
					catch(NumberFormatException nfe){
						scanIn.nextLine();
						System.out.print("Year must be an integer greater than 2013, try again:");
						year = -1;
					}
				}
				System.out.print("Month: ");
				month = -1;
				while(month < 0){
					try{
						month = Integer.parseInt(scanIn.next());
						scanIn.nextLine();
						if(month >12 || month <1){
							System.out.print("Month must be a positive integer (1-12), try again: ");
							month = -1;
						}
					}
					catch(NumberFormatException nfe){
						scanIn.nextLine();
						System.out.print("Month must be a positive integer (1-12), try again: ");
						month = -1;
					}
				}
				System.out.print("Day: ");
				day = -1;
				while(day < 0){
					try{
						day = Integer.parseInt(scanIn.next());
						scanIn.nextLine();
						if(day > 31 || day < 1){
							System.out.print("Day must be a positive integer (1 - 31), try again: ");
							day = -1;
						}
					}
					catch(NumberFormatException nfe){
						scanIn.nextLine();
						System.out.print("Day must be a positive integer (1 - 31), try again: ");
						day = -1;
					}
				}
				System.out.print("Flight ID: ");
				flID = scanIn.next();
				sysMan.createFlight(airline, route, year, month, day, flID);
				System.out.println();
				break;
			case 4: // Create Flight Section
				System.out.print("\nPlease enter the identifier of the airline servicing the flight: ");
				airline = scanIn.next();
				System.out.print("Flight ID: ");
				flID = scanIn.next();
				System.out.print("Number of rows in the flight section - out of range (1-100): ");
				rows = -1;
				while(rows < 0){
					try{
						rows = Integer.parseInt(scanIn.next());
						scanIn.nextLine();
						if(rows < 1 || rows > 100){
							System.out.print("Rows must be an integer (1-100), try again: ");
							rows = -1;
						}
					}
					catch(NumberFormatException nfe){
						scanIn.nextLine();
						System.out.print("Rows must be an integer (1-100), try again: ");
						rows = -1;
					}
				}
				System.out.print("Number of cols in the flight section - out of range (1-10): ");
				cols = -1;
				while(cols < 0){
					try{
						cols = Integer.parseInt(scanIn.next());
						scanIn.nextLine();
						if(cols < 1 || cols > 10){
							System.out.print("Columns must be an integer (1-10), try again: ");
							cols = -1;
						}
					}
					catch(NumberFormatException nfe){
						scanIn.nextLine();
						System.out.print("Columns must be an integer (1-10), try again: ");
						cols = -1;
					}
				}
				loopr = false;
				System.out.print("Seat Section  [ first(f) | business(b) | economy(e) ]: ");
				while (!loopr){
					seatClass = scanIn.next();
					if(seatClass.toLowerCase().equals("f") || seatClass.toLowerCase().equals("first")){
						loopr = true;
						sc = SeatClass.first;
					}
					else if(seatClass.toLowerCase().equals("b") || seatClass.toLowerCase().equals("business")){
						loopr = true;
						sc = SeatClass.business;
					}
					else if(seatClass.toLowerCase().equals("e") || seatClass.toLowerCase().equals("economy")){
						loopr = true;
						sc = SeatClass.economy;
					}
					if(!loopr){
						System.out.print("  Input appears invalid. Please try again [ f(irst) | b(usiness) | e(conomy) ]: ");
					}
				}
				sysMan.createFlightSection(airline, flID, rows, cols, sc);
				System.out.println();
				break;
			case 5: // Print Airline Subsystem Information
				sysMan.displayAirlineSystemDetails();
				System.out.println();
				break;
			case 6: // Create Seaport
				System.out.print("\nPlease enter the 3-character identifier of the new seaport: ");
				seaport = scanIn.next();
				sysMan.createSeaport(seaport);
				System.out.println();
				break;
			case 7: // Create Cruise line
				System.out.print("\nPlease enter the new Cruise line identifier: ");
				sealine = scanIn.next();
				sysMan.createCruiseline(sealine);
				System.out.println();
				break;
			case 8: // Create Cruise
				System.out.print("\nPlease enter the identifier of the cruise line servicing the cruise: ");
				sealine = scanIn.next();

				route = makeRoute(scanIn, false);
                System.out.println("What is the start date for this trip?");
				System.out.print("Year: ");
				year = -1;
				while(year < 2013){
					try{
						year = Integer.parseInt(scanIn.next());
						scanIn.nextLine();
						if(year < 2013){
							System.out.print("Year must be an integer greater than 2013, try again: ");
						}
					}
					catch(NumberFormatException nfe){
						scanIn.nextLine();
						System.out.print("Year must be an integer greater than 2013, try again: ");
						year = -1;
					}
				}
				System.out.print("Month: ");
				month = -1;
				while(month < 0){
					try{
						month = Integer.parseInt(scanIn.next());
						scanIn.nextLine();
						if(month < 1 || month > 12){
							System.out.print("Month must be a positive integer (1-12), try again: ");
							month = -1;
						}
					}
					catch(NumberFormatException nfe){
						scanIn.nextLine();
						System.out.print("Month must be a positive integer (1-12), try again: ");
						month = -1;
					}
				}
				System.out.print("Day: ");
				day = -1;
				while(day < 0){
					try{
						day = Integer.parseInt(scanIn.next());
						scanIn.nextLine();
						if(day < 1 || day > 31){
							System.out.print("Day must be a positive integer (1-31), try again: ");
							day = -1;
						}
					}
					catch(NumberFormatException nfe){
						scanIn.nextLine();
						System.out.print("Day must be a positive integer (1-31), try again: ");
						day = -1;
					}
				}
				System.out.print("Cruise ID: ");
				flID = scanIn.next();
				sysMan.createCruise(sealine, route, year, month, day, flID);
				System.out.println();
				break;
			case 9: // Create Cabin Section
				System.out.print("\nPlease enter the identifier of the cruise line servicing the cruise: ");
				sealine = scanIn.next();
				System.out.print("Cruise ID: ");
				flID = scanIn.next();
				System.out.print("Number of rows in the cabin section - out of range (1-100): ");
				rows = -1;
				while(rows < 0){
					try{
						rows = Integer.parseInt(scanIn.next());
						scanIn.nextLine();
						if(rows < 1 || rows > 100){
							System.out.print("Row must be a positive integer (1-100), try again: ");
							rows = -1;
						}
					}
					catch(NumberFormatException nfe){
						scanIn.nextLine();
						System.out.print("Row must be a positive integer (1-100), try again: ");
						rows = -1;
					}
				}
				System.out.print("Number of cols in the cabin section - out of range (1-10): ");
				cols = -1;
				while(cols < 0){
					try{
						cols = Integer.parseInt(scanIn.next());
						scanIn.nextLine();
						if(cols < 1 || cols > 10){
							System.out.print("Columns must be a positive integer (1-10), try again: ");
							cols = -1;
						}
					}
					catch(NumberFormatException nfe){
						scanIn.nextLine();
						System.out.print("Columns must be a positive integer (1-10), try again: ");
						cols = -1;
					}
				}
				loopr = false;
				System.out.print("Cabin Section  [ family(f) | deluxe-family(df) | couples(c) | deluxe-couples(dc) ]: ");
				while (!loopr){
					cabinClass = scanIn.next();
					if(cabinClass.toLowerCase().equals("f") || cabinClass.toLowerCase().equals("family")){
						loopr = true;
						cc = CabinClass.family;
					}
					else if(cabinClass.toLowerCase().equals("df") || cabinClass.toLowerCase().equals("deluxe-family")){
						loopr = true;
						cc = CabinClass.deluxe_family;
					}
					else if(cabinClass.toLowerCase().equals("c") || cabinClass.toLowerCase().equals("couples")){
						loopr = true;
						cc = CabinClass.couples;
					}
					else if(cabinClass.toLowerCase().equals("dc") || cabinClass.toLowerCase().equals("deluxe-couples")){
						loopr = true;
						cc = CabinClass.deluxe_couples;
					}
					if(!loopr){
						System.out.print("  Input appears invalid. Please try again [ family(f) | deluxe-family(df) | couples(c) | deluxe-couples(dc) ]: ");
					}
				}
				sysMan.createCruiseSection(sealine, flID, rows, cols, cc);
				System.out.println();
				break;
			case 10: // Print Cruise line Subsystem Information
				sysMan.displayCruiselineSystemDetails();
				System.out.println();
				break;
			case 11: // Reset Administrator password
				password1="";
				password2="";
				boolean valid = true;
				scanIn.nextLine();
				
				System.out.print("Please enter the old administration Password: ");
				oldpass = scanIn.nextLine();
				if(oldpass.equals(password)){
					boolean match = false;
					while(!match){
						while(valid){
							System.out.print("Please enter a new administration Password (without any spaces): ");
							password1 = scanIn.nextLine();
							valid = whiteSpace(password1);
							if(valid){
								System.out.println("Password is invalid due to white space in the entered Password.");
							}
						}
						System.out.print("Please re-enter the new administration Password: ");
						password2 = scanIn.nextLine();
						if(password1.equals(password2)){
							password = password1;
							match = true;
						}
						else System.out.println("Passwords did not match.");
					}
					System.out.println("The administration Password has been changed.\n");
				}
				else {
					System.out.println("The password entered did not match the Administration Password.\n");
				}
				break;
			case 12: // Switch to Customer Interface
				userTerminal(scanIn);
				break;
			case 13: // Return to Login Interface
				login(scanIn);
				break;
			case 14: // Print list of commands (this message)
				adminCommands();
				break;
			case 15: // Exit Program
				exitUI();
				break;
			}
		}
	}

	public static void userTerminal(Scanner scanIn){
		int option = 9001; //its over 9000 
		String orig, dest, airline, flID, seatClass, col, sealine, cabinClass;
		int row;
		char seat;
		boolean loopr;
		SeatClass sc = null;
		CabinClass cc = null;
		scanIn.nextLine();

		userCommands();
		while(true){
			System.out.print("Please select an option (6 for a list of commands): ");
			try{
				option = Integer.parseInt(scanIn.next());
			}
			catch(NumberFormatException nfe){
				scanIn.nextLine();
				System.out.println("Invalid input, try again:");
				option = 14;
			}
			switch(option){
			case 1: // Search Flights
				System.out.print("Please enter the airport of origin: ");
				orig = scanIn.next();
				System.out.print("Destination airport: ");
				dest = scanIn.next();
				sysMan.findAvailableFlights(orig, dest);
				System.out.println();
				break;
			case 2: // Book Flight Seat
				System.out.print("Please enter the name of the airline servicing the flight: ");
				airline = scanIn.next();
				System.out.print("Flight ID: ");
				flID = scanIn.next();
				System.out.print("Row: ");
				row = -1;
				while(row < 0){
					try{
						row = Integer.parseInt(scanIn.next());
						scanIn.nextLine();
						if(row < 1 || row > 100){
							System.out.print("Row must be a positive integer (1-100), try again: ");
							row = -1;
						}
					}
					catch(NumberFormatException nfe){
						scanIn.nextLine();
						System.out.print("Row must be a positive integer (1-100), try again: ");
						row = -1;
					}
				}
				System.out.print("Column [A-J]: ");
				col = scanIn.next();
				seat = col.toUpperCase().charAt(0);
				System.out.print("Seat Section  [ f(irst) | b(usiness) | e(conomy) ]: ");
				loopr = false;
				while (!loopr){
					seatClass = scanIn.next();
					if(seatClass.toLowerCase().equals("f") || seatClass.toLowerCase().equals("first")){
						loopr = true;
						sc = SeatClass.first;
					}
					else if(seatClass.toLowerCase().equals("b") || seatClass.toLowerCase().equals("business")){
						loopr = true;
						sc = SeatClass.business;
					}
					else if(seatClass.toLowerCase().equals("e") || seatClass.toLowerCase().equals("economy")){
						loopr = true;
						sc = SeatClass.economy;
					}
					if(!loopr){
						System.out.print("  Input appears invalid. Please try again [ f(irst) | b(usiness) | e(conomy) ]: ");
					}
				}
				sysMan.bookSeat(airline, flID, sc, row, seat);
				break;
			case 3: // Search Cruises
				System.out.print("Please enter the seaport of origin: ");
				orig = scanIn.next();
				System.out.print("Destination seaport: ");
				dest = scanIn.next();
				sysMan.findAvailableCruises(orig, dest);
				System.out.println();
				break;
			case 4: // Book Trip Cabin
				System.out.print("Please enter the name of the cruise line servicing the cruise: ");
				sealine = scanIn.next();
				System.out.print("Cruise ID: ");
				flID = scanIn.next();
				System.out.print("Row: ");
				row = -1;
				while(row < 0){
					try{
						row = Integer.parseInt(scanIn.next());
						scanIn.nextLine();
						if(row < 1 || row > 100){
							System.out.print("Row must be a positive integer (1-100), try again: ");
							row = -1;
						}
					}
					catch(NumberFormatException nfe){
						scanIn.nextLine();
						System.out.print("Row must be a positive integer (1-100), try again: ");
						row = -1;
					}
				}
				System.out.print("Column [A-J]: ");
				col = scanIn.next();
				seat = col.toUpperCase().charAt(0);
				System.out.print("Seat Section  [ family(f) | deluxe-family(df) | couples(c) | deluxe-couples(dc) ]: ");
				loopr = false;
				while (!loopr){
					cabinClass = scanIn.next();
					if(cabinClass.toLowerCase().equals("f") || cabinClass.toLowerCase().equals("family")){
						loopr = true;
						cc = CabinClass.family;
					}
					else if(cabinClass.toLowerCase().equals("df") || cabinClass.toLowerCase().equals("deluxe-family")){
						loopr = true;
						cc = CabinClass.deluxe_family;
					}
					else if(cabinClass.toLowerCase().equals("c") || cabinClass.toLowerCase().equals("couples")){
						loopr = true;
						cc = CabinClass.couples;
					}
					else if(cabinClass.toLowerCase().equals("dc") || cabinClass.toLowerCase().equals("deluxe-couples")){
						loopr = true;
						cc = CabinClass.deluxe_couples;
					}
					if(!loopr){
						System.out.print("  Input appears invalid. Please try again [ family(f) | deluxe-family(df) | couples(c) | deluxe-couples(dc) ]: ");
					}
				}
				sysMan.bookCabin(sealine, flID, cc, row, seat);
				break;
			case 5: // Return to Login Interface
				System.out.println();
				login(scanIn);
				break;
			case 6: // Print list of commands (this message)
				userCommands();
				break;
			case 7: // Exit Program
				exitUI();
				break;
			}
		}
	}

	public static void exitUI(){
		System.out.println("\nExiting. Thank you for using the Booking System.");
		System.exit(0);
	}

	public static void loginCommands(){
		System.out.println("|===================================================|");
		System.out.println("|  List of Login Commands:                          |");
		System.out.println("|===================================================|");
		System.out.println("|  1. Login as Administrator                        |");
		System.out.println("|  2. Login as User                                 |");
		System.out.println("|  3. Print list of commands (this message)         |");
		System.out.println("|  4. Exit Program                                  |");
		System.out.println("|===================================================|\n");
	}

	public static void adminCommands(){
		System.out.println("|===================================================|");
		System.out.println("|  List of Administrator Commands:                  |");
		System.out.println("|===================================================|");
		System.out.println("|  Airline Management                               |");
		System.out.println("|---------------------------------------------------|");
		System.out.println("|  1. Create Airport                                |");
		System.out.println("|  2. Create Airline                                |");
		System.out.println("|  3. Create Flight                                 |");
		System.out.println("|  4. Create Flight Section                         |");
		System.out.println("|  5. Print Airline Subsystem Information           |");
		System.out.println("|===================================================|");
		System.out.println("|  Cruiseline Management                            |");
		System.out.println("|---------------------------------------------------|");
		System.out.println("|  6. Create Seaport                                |");
		System.out.println("|  7. Create Cruise line                            |");
		System.out.println("|  8. Create Cruise                                 |");
		System.out.println("|  9. Create Cabin Section                          |");
		System.out.println("| 10. Print Cruise line Subsystem Information       |");
		System.out.println("|===================================================|");
		System.out.println("|  System Management                                |");
		System.out.println("|---------------------------------------------------|");
		System.out.println("| 11. Reset Administrator password                  |");
		System.out.println("| 12. Switch to Customer Interface                  |");
		System.out.println("| 13. Return to Login Interface                     |");
		System.out.println("| 14. Print list of commands (this message)         |");
		System.out.println("| 15. Exit Program                                  |");
		System.out.println("|===================================================|\n");
	}

	public static void userCommands(){
		System.out.println("|===================================================|");
		System.out.println("|  List of Customer Commands:                       |");
		System.out.println("|===================================================|");
		System.out.println("|  1. Search Flights                                |");
		System.out.println("|  2. Book Flight Seat                              |");
		System.out.println("|  3. Search Cruises                                |");
		System.out.println("|  4. Book Trip Cabin                               |");
		System.out.println("|---------------------------------------------------|");
		System.out.println("|  5. Return to Login Interface                     |");
		System.out.println("|  6. Print list of commands (this message)         |");
		System.out.println("|  7. Exit Program                                  |");
		System.out.println("|===================================================|");
	}
}
