// The Airline class is responsible for the name of the airline it represents,
// and the flights associated with that airline.
public class Cruiseline extends TravelCompany{	
	
	// Constructor
	public Cruiseline(String trainline_name){
		super(trainline_name);
	}
	
	// Creates a flight associated with this airline, adds it to 'flightsLL'
	public void createConnection(String air_name, String[] route, int year, int month, int day, String flight_id){
		Cruise fly = new Cruise(air_name,  route,  year,  month,  day,  flight_id);
		connectionsLL.add(fly);
	}
	
	public String toString(){
		String result = "\nCruiseline: " + getName();
		for(int i = 0; i < connectionsLL.size(); i++){
			result += "\n" + connectionsLL.get(i).toString();
		}
		return result;
	}
	
	public Cruise getCruise(String flID){
        Cruise result = null;
        for(int index = 0; index < connectionsLL.size(); index++){
                if(connectionsLL.get(index).getConnection_id().equals(flID)){
                        result = (Cruise) connectionsLL.get(index);
                        break;
                }
        }
        return result;
	}
}