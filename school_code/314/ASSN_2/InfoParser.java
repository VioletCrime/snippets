import java.io.*;
import java.util.*;


public class InfoParser {

	private Scanner inny;
	private StringBuilder textBuilder = new StringBuilder();
	private boolean debug = false;


	// creates the text from the file
	public void create_scanner(String _filelocation)throws IOException{
		String NL = System.getProperty("line.separator");
		inny = new Scanner(new FileInputStream(_filelocation));
		try {
			while (inny.hasNextLine()){
				textBuilder.append(inny.nextLine() + NL);
			}
		}
		finally{
			inny.close();
		}
	}


	//Builds airports
	public void build_airports(SystemManager SM, String text){
		if (debug)System.out.println("Entering build_airport");
		String temp;
		String[] airports;

		temp = text.substring(text.indexOf('[')+1, text.indexOf(']')).trim(); // creates the substring of airports
		if (debug)System.out.println("airports sub string : "+temp);
		airports = temp.split(",");
		for (int i = 0; i < airports.length; i++){ // creates all the airports from the sub text string
			if(!airports[i].isEmpty()){
				SM.createAirport(airports[i].trim());
				if (debug)System.out.println("airport "+i+" created airport : "+airports[i].trim());
			}
			else System.out.println("Their were no airports in given file.");
		}
		if (debug)System.out.println("Exiting build_airport");
	}


	// builds section 
	public void build_section(SystemManager SM, String section, String airline, String id){
		if (debug)System.out.println("Entering build_section");
		int rows;
		SeatClass s;
		int seatPrice;
		char sectionType;
		String[] sections;
		String[] temp;
		if(section.length() != 0){
			sections = section.split(",");
			for (int i = 0; i < sections.length; i++){
				sections[i].trim();
				temp = sections[i].split(":");
				if(temp[0].equals("E")) s = SeatClass.economy;
				else if(temp[0].equals("B")) s = SeatClass.business;
				else s = SeatClass.first;
				seatPrice = Integer.parseInt(temp[1]);
				sectionType = temp[2].charAt(0);
				rows = Integer.parseInt(temp[3]);
				if (debug)System.out.println("Creating section with : airline: "+airline+", id: "+id+", rows: "+rows+", s:  "+s+", seatPrice: "+seatPrice+", sectionType: "+sectionType);
				SM.createSection(airline, id, rows, s,  seatPrice, sectionType);
			}
		}
		if (debug)System.out.println("Exiting build_section");
	}


	// builds flight and passes on to sub classes 
	public void build_flight(SystemManager SM, String flight, String airline){
		if (debug)System.out.println("Entering build_flight");
		String orig;
		String dest;
		int	year;
		int	month;
		int	day;
		int hour;
		int minute;
		String id;

		id = flight.substring(0, flight.indexOf('|'));
		flight = flight.substring(flight.indexOf('|')+1).trim();
		year = Integer.parseInt(flight.substring(0, flight.indexOf(',')));
		flight = flight.substring(flight.indexOf(',')+1).trim();
		month = Integer.parseInt(flight.substring(0, flight.indexOf(',')));
		flight = flight.substring(flight.indexOf(',')+1).trim();
		day = Integer.parseInt(flight.substring(0, flight.indexOf(',')));
		flight = flight.substring(flight.indexOf(',')+1).trim();
		hour = Integer.parseInt(flight.substring(0, flight.indexOf(',')));
		flight = flight.substring(flight.indexOf(',')+1).trim();
		minute = Integer.parseInt(flight.substring(0, flight.indexOf('|')));
		flight = flight.substring(flight.indexOf('|')+1).trim();
		orig = flight.substring(0, flight.indexOf('|'));
		flight = flight.substring(flight.indexOf('|')+1).trim();
		dest = flight.substring(0, flight.indexOf('['));
		flight = flight.substring(flight.indexOf('[')+1).trim();

		if (debug)System.out.println("Creating flight with : Airline: "+airline+" orig "+orig+" dest "+dest+" year "+year+" month "+month+" day "+day+" hour "+hour+" minute "+minute+" id "+id);
		SM.createFlight(airline, orig, dest, year, month, day, hour, minute, id);

		if (debug)System.out.println("Section of flight "+id+" sent to build_section : "+ flight);
		build_section(SM, flight, airline, id);

		if (debug)System.out.println("Exiting build_flight");
	}


	// builds airline and passes on to sub classes 
	public void build_airline(SystemManager SM, String air){
		if (debug)System.out.println("Entering build_airline");
		String temp;
		String[] flights;

		temp = air.substring(0, air.indexOf('['));
		SM.createAirline(temp);
		if (debug)System.out.println("created airline : " + temp);
		air = air.substring(air.indexOf('[')+1);
		flights = air.split("]");
		for (int i = 0; i < flights.length; i++){
			if(!flights[0].equals(" ")){
				if(i == 0){
					if (debug)System.out.println("flight["+i+"] sent to build_flight : "+ flights[0]);
					build_flight(SM, flights[0], temp);
				}
				else{
					String flight = flights[i].substring(flights[i].indexOf(',')+2);
					if (debug)System.out.println("flight["+i+"] sent to build_flight : "+ flight);
					build_flight(SM, flight, temp);
				}
			}
		}
		if (debug)System.out.println("Exiting build_airline");
	}


	// splits the airlines so that they can be used by trim airline
	public String[] split_airlines(String text){
		LinkedList<String> result = new LinkedList<String>();
		String[] airlines;
		String[] temp;

		airlines = text.split("(?<=]])");

		for(String item : airlines){
			if(item.charAt(item.indexOf(']')-1) == '['){
				String[] temp2 = item.split("(?<=\\[])");
				for(int i=0;i<temp2.length;i++){
					if(temp2[i].length() > 15){
						temp2[i] += temp2[i+1];
						temp2[i+1] = "";
					}
					if(temp2[i]!="")
						result.add(temp2[i]);
				}
			}
			else result.add(item);
		}

		for (int i = 0; i < result.size(); i++){
			if(result.get(i).charAt(result.get(i).length()-2)=='['){
				result.set(i, result.get(i).substring(1));
				temp = result.get(i).split(",");

				for(int y=0; y<temp.length; y++){
					result.addLast(temp[y]);
				}
				result.removeLast();
				break;
			}
		}
		return result.toArray(airlines);
	}

	// trims airlines and sends them to build airline
	public void trim_airline(SystemManager SM, String text){ 
		if (debug)System.out.println("Entering trim_airline");
		String[] airlines;
		String air;

		airlines = split_airlines(text);

		for (int i = 0; i < airlines.length; i++){ // trims airlines and sends them to build airline
			if(!airlines[i].isEmpty()){
				if(airlines[i].charAt(airlines[i].indexOf('[')) != airlines[i].charAt(airlines[i].length()-1)){
					if(i == 0){
						air = airlines[i].substring(airlines[i].indexOf('{')+1, airlines[i].lastIndexOf(']')+1);
						if (debug)System.out.println("airline["+i+"] sent to build_airline : "+ air);
						build_airline(SM, air);
					}
					else{
						air = airlines[i].substring(airlines[i].indexOf(',')+2, airlines[i].lastIndexOf(']')+1);
						if (debug)System.out.println("airline["+i+"] sent to build_airline : "+ air);
						build_airline(SM, air);
					}
				}
				else{
					air = airlines[i].substring(airlines[i].indexOf(',')+2) + " ]";
					if (debug)System.out.println("airline["+i+"] sent to build_airline : "+ air);
					build_airline(SM, air);
				}
			}
			else {
				System.out.println("Their were no airlines in given file.");
				break;
			}
		}
		if (debug)System.out.println("Exiting trim_airline");
	}


	// reads the text and builds the database
	public void read(SystemManager SM, String _filein)throws IOException{
		try{
			if (debug)System.out.println("Entering read");
			String text;

			create_scanner(_filein);
			text = textBuilder.toString().trim();
			if (debug)System.out.println("text : "+text);
			if (debug)System.out.println("Calling build_airports with : "+text);

			build_airports(SM, text); //Builds airports

			text = text.substring(text.indexOf(']')+1, text.length()-1); // removes airports from text

			if (debug)System.out.println("calling trim_airline with : "+text);
			trim_airline(SM, text); // trims airlines and sends them to build airline
			if (debug)System.out.println("Exiting read");
		}catch(IndexOutOfBoundsException ioe){
			System.out.print("IndexOutOfBoundsException: Could not parse file invalid syntax of input file check your delimiters.");
			SM.airlines.clear();
			SM.airports.clear();
		}catch(NumberFormatException e){
			System.out.print("NumberFormatException: Could not parse file invalid syntax of input file check if you have booked seats in your input file.");
			SM.airlines.clear();
			SM.airports.clear();
		}
	}

	public String outFile_seat(ArrayList<ArrayList<Seat>> seats, String text, int row, int col){
		if (debug)System.out.println("Entering outFile_section");
		if (debug)System.out.println("text in is : "  + text);

		boolean boo = false;

		for(int r=0; r<row; r++){
			for(int c=0; c<col; c++){
				if(seats.get(r).get(c).isBooked()){
					System.out.println("{" + (seats.get(r).get(c).getRow()+1) + seats.get(r).get(c).getCol() + ",");
					text += "{" + (seats.get(r).get(c).getRow()+1) + seats.get(r).get(c).getCol() + ",";
					boo = true;
				}
			}
		}
		if(boo) text = text.substring(0, text.length()-1) + "}";
		if (debug)System.out.println("Exiting outFile_section");
		return text;
	}

	public String outFile_flight(ArrayList <Flight> flights, String text){
		if (debug)System.out.println("Entering outFile_flight");
		if (debug)System.out.println("text in is : "  + text);
		char layout;

		for(int i=0; i<flights.size(); i++){
			text += flights.get(i).getId() + "|" + flights.get(i).getYear() + ", " + flights.get(i).getMonth() + ", " + flights.get(i).getDay() + ", ";
			text += flights.get(i).getHour() + ", " + flights.get(i).getMinute() + "|";
			text += flights.get(i).getOrig() + "|" + flights.get(i).getDest() + "[";
			if(!flights.get(i).getE_Bool()){
				if(flights.get(i).getSection('e').getCols() == 3)
					layout = 'S';
				else if(flights.get(i).getSection('e').getCols() == 4)
					layout = 'M';
				else layout = 'W';
				text += "E" + ":" + flights.get(i).getSection('e').getPrice()+ ":" + layout + ":" + flights.get(i).getSection('e').getRows();
				text = outFile_seat(flights.get(i).getSection('e').getSeats(), text, flights.get(i).getSection('e').getRows(), flights.get(i).getSection('e').getCols());
				text += ",";
			}
			if(!flights.get(i).getB_bool()){
				if(flights.get(i).getSection('b').getCols() == 3)
					layout = 'S';
				else if(flights.get(i).getSection('b').getCols() == 4)
					layout = 'M';
				else layout = 'W';
				text += "B" + ":"+ flights.get(i).getSection('b').getPrice()+ ":" + layout + ":" + flights.get(i).getSection('b').getRows();
				text = outFile_seat(flights.get(i).getSection('b').getSeats(), text, flights.get(i).getSection('b').getRows(), flights.get(i).getSection('b').getCols());
				text += ",";
			}
			if(!flights.get(i).getF_Bool()){
				if(flights.get(i).getSection('f').getCols() == 3)
					layout = 'S';
				else if(flights.get(i).getSection('f').getCols() == 4)
					layout = 'M';
				else layout = 'W';
				text += "F" + ":"+ flights.get(i).getSection('f').getPrice()+ ":" + layout + ":" + flights.get(i).getSection('f').getRows();
				text = outFile_seat(flights.get(i).getSection('f').getSeats(), text, flights.get(i).getSection('f').getRows(), flights.get(i).getSection('f').getCols());
				text += ",";
			}
			text = text.substring(0, text.length()-1) + "], ";
		}
		if(text.charAt(text.length()-1) == '['){
			text += "], ";
		}
		else text = text.substring(0, text.length()-2) + "], ";

		if (debug)System.out.println("text out is : "  + text);
		if (debug)System.out.println("Exiting outFile_flight");
		return text;
	}

	public String outFile_airline(ArrayList <Airline> airlines, String text){
		if (debug)System.out.println("Entering outFile_airline");
		if (debug)System.out.println("text in is : "  + text);
		text += "]{";
		for(int i=0; i<airlines.size(); i++){
			text += airlines.get(i).getName() + "[";
			text = outFile_flight(airlines.get(i).getFlights(), text);
		}
		if (debug)System.out.println("Exiting outFile_airline");
		return text;
	}


	public String outFile_airport(ArrayList <Airport> airports, String text){
		if (debug)System.out.println("Entering outFile_airport");
		if (debug)System.out.println("text in is : "  + text);
		for(int i=0; i<airports.size(); i++){
			text += airports.get(i).getName() + ", ";
		}
		text = text.substring(0, text.length()-2);
		if (debug)System.out.println("Exiting outFile_airport");
		return text;
	}

	public String create_outFile(SystemManager SM){
		if (debug)System.out.println("Entering create_outFile");
		try{
			String text ="["; 
			text = outFile_airport(SM.airports, text);
			text = outFile_airline(SM.airlines, text);

			text = text.substring(0,text.length()-2);
			text += "}";

			if (debug)System.out.println(text);
			if (debug)System.out.println("Exiting create_outFile");
			return text;
		}catch(IndexOutOfBoundsException ioe){
			System.out.print("Could not write file invalid syntax of AMS system database check your structure");
			return "";
		}
	}

	public void write(SystemManager SM, String _fileout){
		if (debug)System.out.println("Entering write");
		try{
			// Create file 
			FileWriter fstream = new FileWriter(_fileout);
			BufferedWriter out = new BufferedWriter(fstream);
			String text_out = create_outFile(SM);
			out.write(text_out);
			//Close the output stream
			out.close();
		}catch (Exception e){//Catch exception if any
			System.err.println("Error: could not create write file");
		}
		if (debug)System.out.println("Exiting write");
	}
}
