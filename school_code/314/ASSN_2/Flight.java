public class Flight {



	private String airlinename, orig, dest, id;
	private int year, month, day, hour, minute;
	private FlightSection first,business,economy; // Seat classes
	private boolean f = true, b = true, e = true; // Booleans for the seat classes
	// A little more explaination about the booleans on the line about this one
	// They are used to check if a type of seat class has already been used however they
	// are programmed a little backwards.  if 'f' is true then a 'first class' section hasn't
	// been made yet.  Latter in the program you will see how they are used but i feel like this
	// is something improtant to explain because of the backwards way I programmed it.


	/**
	 * Constructor for making a flight.
	 */
	Flight(String	airlinename,	String	orig,	String	dest,	int	year,	int	month,	int	day, int hour, int minute,	String	id){
		this.airlinename = airlinename;
		this.orig 		 = orig;
		this.dest 		 = dest;
		this.year 		 = year;
		this.month 		 = month;
		this.day 		 = day;
		this.hour        = hour;
		this.minute      = minute;
		this.id 		 = id;
	}

	/**
	 * 
	 * @return id
	 */
	String getId(){
		return id;
	}

	
	/**
	 * you know what this does
	 */
	public String toString(){
		return airlinename + " Flight "+id+": from "+orig+" to "+dest+" on "+month+"/"+day+"/"+year+" at "+hour+":"+minute;
	}

	/**
	 * For adding sections to flights, we needed to check for what classes the flight already 
	 * contained and if someone tries to make a section that is already there, then it gives an
	 * error. 
	 * @param fs which is the flight section specified to be added
	 */
	public void addSection(FlightSection fs) {
		if(fs.getSection() == SeatClass.first && f){
			first = fs;
			f = false;
		}
		else if(fs.getSection() == SeatClass.business && b){
			business = fs;
			b = false;
		}
		else if(fs.getSection() == SeatClass.economy && e){
			economy = fs;
			e = false;
		}
		else{
			System.out.println("Usage - Adding Flight Section: Seat Class \""+fs.getSection()+"\" already exists on flight \""+id+"\"");
		}
	}
	
	public boolean hasAvailableSeat(SeatClass sc){
		boolean result = false;
		if(sc == SeatClass.first){
			result = first.hasAvailableSeat();
		}
		else if(sc == SeatClass.business){
			result = business.hasAvailableSeat();
		}
		else if(sc == SeatClass.economy){
			result = economy.hasAvailableSeat();
		}
		return result;
	}
	
	public boolean sectionExist(SeatClass sc){
		boolean result = true;
		if(sc == SeatClass.first){
			result = f;
		}
		else if(sc == SeatClass.business){
			result = b;
		}
		else if(sc == SeatClass.economy){
			result = e;
		}
		return !result;
	}
	
	/**
	 * This little helper method just lets us translate from chars to flight sections.
	 * @param c ('f', 'b' or 'e')
	 * @return
	 */
	public FlightSection getSection(char c){
		if( c == 'f')
			return first;
		else if( c == 'b')
			return business;
		else if( c == 'e')
			return economy;
		else
			return null;
	}

	/**
	 * bookSeat just takes the seatClass and (row,col) corridinates and books the 
	 * seat specified.  It is a helper method so there isn't error checking.
	 * @param s is the SeatClass 
	 * @param row 
	 * @param col
	 */
	public void bookSeat(SeatClass s, int row, char col) {
		if(s == SeatClass.first){
			first.bookSeat(row,col);
		}
		else if(s == SeatClass.business){
			business.bookSeat(row,col);
		}
		else{
			economy.bookSeat(row,col);
		}
	}
	
	public void bookSeatByPreference(SeatClass s, int seatPref){
		char sec = 'w';
		if (s == SeatClass.first){
			sec = 'f';
		}
		else if (s == SeatClass.business){
			sec = 'b';
		}
		else if (s == SeatClass.economy){
			sec = 'e';
		}
		FlightSection fc = getSection(sec);
		int[] pref = {0};
		if (seatPref == 0){
			pref = fc.getWindows();
		}
		else if (seatPref == 1){
			pref = fc.getAisles();
		}
		boolean seatFound = false;
		for (int i = 1; i < fc.getRows(); i++){
			for (int j = 0; j < pref.length; j++){
				if (!fc.isSeatEmpty(i, pref[j])){
					fc.bookSeat(i, (char) ((char) pref[j] + 'A'));
					System.out.println("The seat " + i + (char) (pref[j] + 'A') + " has been booked. Enjoy your flight!");
					seatFound = true;
					break;
				}
			}
			if(seatFound){
				break;
			}
		}
		if(!seatFound){
			System.out.println("Sorry; a seat could not be found for you in the " + s + " section of flight " + id);
		}
	}
	
	/**
	 * @return orig
	 */
	public String getOrig(){
		return orig;
	}
	
	/**
	 * @return dest
	 */
	public String getDest(){
		return dest;
	}
	
	public int getDay(){
		return day;
	}
	
	public int getMonth(){
		return month;
	}
	
	public int getYear(){
		return year;
	}

	public int getHour() {
		return hour;
	}

	public int getMinute() {
		return minute;
	}
	
	public boolean getF_Bool() {
		return f;
	}
	
	public boolean getE_Bool() {
		return e;
	}
	
	public boolean getB_bool() {
		return b;
	}
}
