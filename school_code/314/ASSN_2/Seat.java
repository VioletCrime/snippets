public class Seat {

	private boolean booked;
	private int row;
	private char col;

	/**
	 * Class constructor - Creates a seat that is not booked.
	 * also has a row and col identifiers to tell us where the seat
	 * is located within the flight section
	 */
	Seat(int i, char c){
		this.booked = false;
		this.row = i;
		this.col = c;
	}

	/**
	 * Returns the status of the seat. Weather it is booked or not
	 * @return True if the seat is booked. False otherwise
	 */
	public boolean isBooked() {
		return booked;
	}

	/**
	 * Books a seat if it is not already booked
	 * @return True if booked False if seat was already booked 
	 */
	public boolean book(){
		if(booked)
			return false;
		else{
			booked = true;
			return true;
		}
	}

	/**
	 *  you know what this does, but the tabs are for indenting the display
	 */
	public String toString(){
		if(booked){
			return "\t\t\t\t" + (row+1)+""+col+": Booked";
		}
		else{
			return "\t\t\t\t" + (row+1)+""+col+": Available";
		}
	}

	public int getRow(){
		return row;
	}
	
	public char getCol(){
		return col;
	}


}
