import java.util.*;

public class Airline {
	
	public ArrayList <Flight> flights = new ArrayList <Flight>(); //Holds all the flights associated with this airline
	private String name; // Name of the airline
	
	/**
	 * Class constructor creates airline with given name
	 * @param name the name of the airline
	 */
	Airline(String name){
		this.name = name;		
	}
	
	/**
	 *
	 * @return the name of the airline
	 */
	String getName(){
		return name;
	}
	
	/**
	 * Renames the airline
	 * @param name the new name of the airline
	 */
	void setName(String name){
		this.name = name;
	}
	
	/**
	 * Adds a flight to the airline if it doesn't exist already
	 * @param f the flight to add
	 * @return True if the flight was added False if it already exists
	 */
	boolean addFlight(Flight f){
		// Check if the flight alredy exists
		for(int i = 0;i<flights.size();i++){
			if(flights.get(i).getId().equals(f.getId())){
				System.out.println("Error - Creating Flight:  \""+f+"\" already exists in the system.");
				return false;
			}
		}
		
		flights.add(f);
		
		return true;
	}
	
	/**
	 * @return flights
	 */
	ArrayList<Flight> getFlights(){
		return flights;
	}
	
	/**
	 * @return "Airline: <airline name>"
	 */
	public String toString(){
		return "Airline: "+name;
	}

	/**
	 * Searches flights for a flight with the same id
	 * @param flID the id to search for
	 * @return the flight, error otherwise
	 */
	public Flight getFlightByID(String flID) {
		for(int i = 0;i<flights.size();i++){
			if(flights.get(i).getId().equals(flID)){
				return flights.get(i);
			}
		}
		
		System.out.println("Error - Finding Flight: could not find a flight with ID:\"" + flID + "\"");
		return null;
	}
	
	/**
	 * 
	 * @param flID
	 * @return True if flight exists, otherwise false
	 */
	public boolean flightExist(String flID){
		for(int i = 0;i<flights.size();i++){ // Traversing through flights
			if(flights.get(i).getId().equals(flID)){
				return true;
			}
		}
		return false;
	}
	
}
