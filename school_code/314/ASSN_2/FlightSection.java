import java.util.ArrayList;

public abstract class FlightSection {

	private String air, flID;
	private int rows,cols, price;
	private int[] windows, aisles;
	private SeatClass s;
	private ArrayList<ArrayList<Seat>> seats;

	/**
	 * Class constructor
	 * All the seats for this section are generated at this time
	 * @param air  The airline name that is with this flight/section
	 * @param flID The id of the flight this section is in
	 * @param rows The amount of rows for this section (max 100 rows)
	 * @param cols The amount of columns for this section (max 10 columns)
	 * @param s    The type of section this is (first, business, economy)
	 */
	FlightSection(String	air,	String	flID,	int	rows,	int	cols,	SeatClass	s, int price, int[] windows, int[] aisles){
		this.air = air;
		this.flID = flID;
		this.rows = rows;
		this.cols = cols;
		this.price = price;
		this.s = s;
		this.windows = windows;
		this.aisles = aisles;
		this.seats = new ArrayList<ArrayList<Seat>>();
		generateSeats();
	}

	/**
	 * Generates the seats in an arraylist of arraylists where the first arraylist is the rows and the <br>
	 * nested arraylist contains the seats in that row.
	 */
	private void generateSeats(){
		for(int i=0;i!=rows;i++){
			ArrayList<Seat> row = new ArrayList<Seat>();

			char car = 'A';
			for(int j=0;j != cols;j++){
				Seat s = new Seat(i,car);
				row.add(s);
				car++;
			}
			seats.add(row);
		}
	}

	/**
	 * 
	 * @return the type of the section
	 */
	public SeatClass getSection(){
		return s;
	}

	public void setPrice(int prc){
		price = prc;
	}
	
	public int getPrice(){
		return price;
	}
	
	/**
	 * This toString is a little more complicated because it either prints
	 * out ALL of the seats within the flight section ooooooooooor
	 * just shows the ones that are available.  This is all dependant on the 
	 * boolean showall. if showall is true, show all.
	 * @return the available seats out of the total
	 */
	public String toString(boolean showall){

		String total = "" ;

		for(int i=0; i!=rows;i++){
			for(int j=0;j!=cols;j++){
				if(showall)
					total += seats.get(i).get(j).toString()+"\n";	
				else{
					if(!seats.get(i).get(j).isBooked()){
						total += seats.get(i).get(j).toString()+"\n";
					}
				}
			}
		}

		return total;	

	}

	/**
	 * Try's to book a seat at 'row', 'col'. 
	 * Checks that the seat exists first then try's to book
	 * @param row the row of the seat that is trying to be booked
	 * @param col the column of the seat that is trying to be booked
	 */
	public void bookSeat(int row, char col) {
		if(checkSeat(row,col)){
			int i=0;
			if( col == 'A') //convert col to an int
				i = 0;
			else
				i = col - 'A';
			Seat s = seats.get(row-1).get(i);
			if(!s.book())
				System.out.println("Error - Booking Seat: " + this.s + " Seat \"" + row + "" + col +"\" on " + air + " flight " + flID +  " has already been booked");
		}
		else{
			System.out.println("Error - Booking Seat: The seat "+ row + "" + col +" in " + s + " does not exist on " + air + " flight " + flID +"");
		}
	}

	public boolean isSeatEmpty(int row, int col){
		boolean result = false;
		if(checkSeat(row,(char) (col+'A'))){
			Seat s = seats.get(row-1).get(col);
			if(s.isBooked())
				result = true;
		}
		return result;
	}
	
	/**
	 * Checks if a seat is in the valid range of 'rows' and 'cols'
	 * @param row the row the seat is in
	 * @param col the column the seat is in
	 * @return False if the seat is outside the rows or cols. True otherwise
	 */
	private boolean checkSeat(int row, char col) {
		int i = col - 'A';
		if(row < 0 || row > rows){
			return false;
		}
		else if(i < 0 || i > cols){
			return false;
		}
		else
			return true;
	}
	
	public boolean hasAvailableSeat(){
		boolean result = false;
		for(int i = 0; i < rows; i++){
			for(int j = 0; j < cols; j++){
				if(!seats.get(i).get(j).isBooked()){
					result = true;
					break;
				}
				
			}
			if(result){
				break;
			}
		}
		return result;
	}
	
	public int[] getWindows(){
		return windows;
	}
	public int[] getAisles(){
		return aisles;
	}
	public int getRows(){
		return rows;
	}
	public int getCols(){
		return cols;
	}
	public ArrayList<ArrayList<Seat>> getSeats(){
		return seats;
	}
}
