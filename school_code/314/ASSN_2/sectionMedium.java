
public class sectionMedium extends FlightSection{
	public sectionMedium(String air, String flID, int rows, SeatClass s, int price){
		super(air, flID, rows, 4, s, price, getWindowSeats(), getAisleSeats());
	}
	
	public static int[] getWindowSeats(){
		int[] result = new int[2];
		result[0] = 0;
		result[1] = 3;
		return result;
	}
	
	public static int[] getAisleSeats(){
		int[] result = new int[2];
		result[0] = 1;
		result[1] = 2;
		return result;
	}
}


/*
 *    0     1         2     3
 * (           |   |           )
 * ( Seat Seat |   | Seat Seat )
 * (           |   |           )
 * 
 */