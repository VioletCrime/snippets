
public class sectionSmall extends FlightSection{
	
	public sectionSmall(String air, String flID, int rows, SeatClass s, int price){
		super(air, flID, rows, 3, s, price, getWindowSeats(), getAisleSeats());
	}
	
	private static int[] getWindowSeats(){
		int[] result = new int[2];
		result[0] = 0;
		result[1] = 2;
		return result;
	}
	
	private static int[] getAisleSeats(){
		int[] result = new int[2];
		result[0] = 1;
		result[1] = 2;
		return result;
	}
}

/*
 *    0     1          2
 * (           |   |      )
 * ( Seat Seat |   | Seat )
 * (           |   |      )
 * 
 */
