import java.io.IOException;
import java.util.Scanner;

public class UI {
	public static void main(String[] args){
		SystemManager sysMan = new SystemManager();
		InfoParser IP = new InfoParser();
		Scanner scanIn = new Scanner(System.in);
		
		String airport, airline, orig, dest, file_name, cols, flID, sectionType, seatClass, seatPref;
		int year, month, day, hour, minute, rows, price;
		char secType = 'w'; //arbitrary assignment value used to get Eclipse to shut up
		boolean loopr = false;
		SeatClass sc = null;
		
		int command = 99;
		System.out.println("Welcome to the Airport System Manager.");
		printCommands();
		while(command != 0){
			System.out.print("\nPlease select an action ('12' for list of commands): ");
			try{
				command = Integer.parseInt(scanIn.next());
			}
			catch(NumberFormatException nfe){
				scanIn.nextLine();
				System.out.println("Invalid input, try again:");
				command = 12;
			}
			
			switch(command){
			
			case 0: //Exit case
				System.out.println("\nExiting. Thank you for using the Airport Management System.");
				break;
				
			case 1: //Input from file case
				System.out.print("\nPlease enter the name of the AMS file to read from: ");
				file_name = scanIn.next();
				try {
					IP.read(sysMan, file_name);
				} catch (IOException e) {
					System.out.println("Exception caught; File not successfully read.");
				} catch (NumberFormatException n){
					System.out.println("NumberFormatException caught; are you sure the file syntax is correct?");
				}
				
				System.out.println();
				break;
				
			case 2: //Output to file case
				System.out.print("\nPlease enter the name of the AMS file to write to: ");
				file_name = scanIn.next();
				IP.write(sysMan, file_name);
				System.out.println();
				break;
				
			case 3:  //Print System Details case
				sysMan.displaySystemDetails();
				System.out.println();
				break;
				
			case 4: //Create Airport case
				System.out.print("\nPlease enter the 3-character identifier of the new airport: ");
				airport = scanIn.next();
				sysMan.createAirport(airport);
				System.out.println();
				break;
				
			case 5: //Create Airline case
				System.out.print("\nPlease enter the new airline's identifier: ");
				airline = scanIn.next();
				sysMan.createAirline(airline);
				System.out.println();
				break;
				
			case 6: //Create Flight case
				System.out.print("\nPlease enter the identifier of the airline servicing the flight: ");
				airline = scanIn.next();
				System.out.print("Airport of origin: ");
				orig = scanIn.next();
				System.out.print("Destination Airport: ");
				dest = scanIn.next();
				System.out.print("Year: ");
				year = scanIn.nextInt();
				System.out.print("Month: ");
				month = scanIn.nextInt();
				System.out.print("Day: ");
				day = scanIn.nextInt();
				System.out.print("Hour: ");
				hour = scanIn.nextInt();
				System.out.print("Minute: ");
				minute = scanIn.nextInt();
				System.out.print("Flight ID: ");
				flID = scanIn.next();
				sysMan.createFlight(airline, orig, dest, year, month, day, hour, minute, flID);
				System.out.println();
				break;
				
			case 7: //Create Flight Section case
				System.out.print("\nPlease enter the identifier of the airline servicing the flight: ");
				airline = scanIn.next();
				System.out.print("Flight ID: ");
				flID = scanIn.next();
				System.out.print("Number of rows in the section: ");
				rows = scanIn.nextInt();
				loopr = false;
				System.out.print("Seat Section  [ f(irst) | b(usiness) | e(conomy) ]: ");
				while (!loopr){
					seatClass = scanIn.next();
					if(seatClass.toLowerCase().equals("f") || seatClass.toLowerCase().equals("first")){
						loopr = true;
						sc = SeatClass.first;
					}
					else if(seatClass.toLowerCase().equals("b") || seatClass.toLowerCase().equals("business")){
						loopr = true;
						sc = SeatClass.business;
					}
					else if(seatClass.toLowerCase().equals("e") || seatClass.toLowerCase().equals("economy")){
						loopr = true;
						sc = SeatClass.economy;
					}
					if(!loopr){
						System.out.print("  Input appears invalid. Please try again [ f(irst) | b(usiness) | e(conomy) ]: ");
					}
				}
				loopr = false;
				System.out.print("Section Type  [ s(mall) | m(edium) | w(ide) ]: ");
				while (!loopr){
					sectionType = scanIn.next();
					if(sectionType.toLowerCase().equals("s") || sectionType.toLowerCase().equals("small")){
						loopr = true;
						secType = 'S';
					}
					else if(sectionType.toLowerCase().equals("m") || sectionType.toLowerCase().equals("medium")){
						loopr = true;
						secType = 'M';
					}
					else if(sectionType.toLowerCase().equals("w") || sectionType.toLowerCase().equals("wide")){
						loopr = true;
						secType = 'L';
					}
					if(!loopr){
						System.out.print("  Input appears invalid. Please try again [ s(mall) | m(edium) | w(wide) ]: ");
					}
				}
				System.out.print("Price of a seat: ");
				price = scanIn.nextInt();
				sysMan.createSection(airline, flID, rows, sc, price, secType);
				break;
				
			case 8: //Set Section price case
				System.out.print("\nPlease enter the airline: ");
				airline = scanIn.next();
				System.out.print("Flight ID: ");
				flID = scanIn.next();
				loopr = false;
				System.out.print("Seat Section  [ f(irst) | b(usiness) | e(conomy) ]: ");
				while (!loopr){
					seatClass = scanIn.next();
					if(seatClass.toLowerCase().equals("f") || seatClass.toLowerCase().equals("first")){
						loopr = true;
						sc = SeatClass.first;
					}
					else if(seatClass.toLowerCase().equals("b") || seatClass.toLowerCase().equals("business")){
						loopr = true;
						sc = SeatClass.business;
					}
					else if(seatClass.toLowerCase().equals("e") || seatClass.toLowerCase().equals("economy")){
						loopr = true;
						sc = SeatClass.economy;
					}
					if(!loopr){
						System.out.print("  Input appears invalid. Please try again [ f(irst) | b(usiness) | e(conomy) ]: ");
					}
				}
				System.out.print("Price: ");
				price = scanIn.nextInt();
				sysMan.setSectionPrice(airline, flID, sc, price);
				System.out.println();
				break;
				
				
			case 9: //Find a flight case
				System.out.print("Please enter the airport of origin: ");
				orig = scanIn.next();
				System.out.print("Destination airport: ");
				dest = scanIn.next();
				System.out.print("Month: ");
				month = scanIn.nextInt();
				System.out.print("Day: ");
				day = scanIn.nextInt();
				System.out.print("Year: ");
				year = scanIn.nextInt();
				System.out.print("Seat Section  [ f(irst) | b(usiness) | e(conomy) ]: ");
				loopr = false;
				while (!loopr){
					seatClass = scanIn.next();
					if(seatClass.toLowerCase().equals("f") || seatClass.toLowerCase().equals("first")){
						loopr = true;
						sc = SeatClass.first;
					}
					else if(seatClass.toLowerCase().equals("b") || seatClass.toLowerCase().equals("business")){
						loopr = true;
						sc = SeatClass.business;
					}
					else if(seatClass.toLowerCase().equals("e") || seatClass.toLowerCase().equals("economy")){
						loopr = true;
						sc = SeatClass.economy;
					}
					if(!loopr){
						System.out.print("  Input appears invalid. Please try again [ f(irst) | b(usiness) | e(conomy) ]: ");
					}
				}
				sysMan.findAvailableFlights(orig, dest, month, day, year, sc);
				break;
				
			case 10: //Book a specific seat case
				System.out.print("Please enter the name of the airline servicing the flight: ");
				airline = scanIn.next();
				System.out.print("Flight ID: ");
				flID = scanIn.next();
				System.out.print("Row: ");
				rows = scanIn.nextInt();
				System.out.print("Column [A-Z]: ");
				cols = scanIn.next();
				char seat = cols.toUpperCase().charAt(0);
				System.out.print("Seat Section  [ f(irst) | b(usiness) | e(conomy) ]: ");
				loopr = false;
				while (!loopr){
					seatClass = scanIn.next();
					if(seatClass.toLowerCase().equals("f") || seatClass.toLowerCase().equals("first")){
						loopr = true;
						sc = SeatClass.first;
					}
					else if(seatClass.toLowerCase().equals("b") || seatClass.toLowerCase().equals("business")){
						loopr = true;
						sc = SeatClass.business;
					}
					else if(seatClass.toLowerCase().equals("e") || seatClass.toLowerCase().equals("economy")){
						loopr = true;
						sc = SeatClass.economy;
					}
					if(!loopr){
						System.out.print("  Input appears invalid. Please try again [ f(irst) | b(usiness) | e(conomy) ]: ");
					}
				}
				sysMan.bookSeat(airline, flID, sc, rows, seat);
				break;
				
			case 11: //Book a seat by preference case public void bookSeat(String air, String flID, SeatClass s, int seatPref){
				System.out.print("Please enter name of the airline servicing the flight: ");
				airline = scanIn.next();
				System.out.print("Flight ID: ");
				flID = scanIn.next();
				System.out.print("Seat Section  [ f(irst) | b(usiness) | e(conomy) ]: ");
				loopr = false;
				while (!loopr){
					seatClass = scanIn.next();
					if(seatClass.toLowerCase().equals("f") || seatClass.toLowerCase().equals("first")){
						loopr = true;
						sc = SeatClass.first;
					}
					else if(seatClass.toLowerCase().equals("b") || seatClass.toLowerCase().equals("business")){
						loopr = true;
						sc = SeatClass.business;
					}
					else if(seatClass.toLowerCase().equals("e") || seatClass.toLowerCase().equals("economy")){
						loopr = true;
						sc = SeatClass.economy;
					}
					if(!loopr){
						System.out.print("  Input appears invalid. Please try again [ f(irst) | b(usiness) | e(conomy) ]: ");
					}
				}
				System.out.print("Seating Preference [ w(indow) | a(isle) ]: ");
				loopr = false;
				int sp = -1;
				while (!loopr){
					seatPref = scanIn.next();
					if(seatPref.toLowerCase().equals("w") || seatPref.toLowerCase().equals("window")){
						sp = 0;
						loopr = true;
					}
					else if(seatPref.toLowerCase().equals("a") || seatPref.toLowerCase().equals("aisle")){
						sp = 1;
						loopr = true;
					}
					if(!loopr){
						System.out.print("  Input appears invalid. Please try again [ w(indow) | a(aisle) ]: ");
					}
				}
				sysMan.bookSeat(airline,  flID, sc, sp);
				break;
				
			case 12: //Print commands
				printCommands();
				break;
				
			case 2501: //Easter Egg case
				System.out.println("\n\"It can be argued that DNA is nothing more than a program designed to preserve itself.");
				System.out.println("Life has become more complex in the overwhelming sea of information.");
				System.out.println("And life, when organized into species, relies upon genes to be its memory system. ");
				System.out.println("So, man is an individual only because of his intangible memory... and memory cannot be defined, but it defines mankind.");
				System.out.println("The advent of computers, and the subsequent accumulation of incalculable data has given rise");
				System.out.println("to a new system of memory and thought parallel to your own.");
				System.out.println("Humanity has underestimated the consequences of computerization.\" - 2501");
				break;
				
			default: //None of the above case
				System.out.println("Input does not appear to be valid. Try again: ");
				break;
			}
			
		}
	}
	
	public static void printCommands(){
		System.out.println("List of commands available:");
		System.out.println(" 1.   Input system information from AMS file.");
		System.out.println(" 2.   Write system information to AMS file.");
		System.out.println(" 3.   Display Airport System Manager information");
		System.out.println(" 4.   Create Airport.");
		System.out.println(" 5.   Create Airline.");
		System.out.println(" 6.   Create Flight.");
		System.out.println(" 7.   Create Flight Section.");
		System.out.println(" 8.   Set Flight Section price.");
		System.out.println(" 9.   Find a flight.");
		System.out.println(" 10.  Book a seat (specific seat).");
		System.out.println(" 11.  Book a seat (by preference).");
		System.out.println(" 12.  Print this list of commands message");
		System.out.println(" 0.   Exit.");
	}
}
