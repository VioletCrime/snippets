public class Airport {

	private String name;
	
	Airport(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String toString(){
		return "Airport: "+name;
	}
}
