import java.text.SimpleDateFormat;
import java.util.*;


public class SystemManager {

	ArrayList <Airport> airports = new ArrayList <Airport>(); // Storage for airports
	ArrayList <Airline> airlines = new ArrayList <Airline>(); // Storage for airlines


	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////

	/**
	 * Creates	an	airport	object	and	links	it	to	the	SystemManager.	The	airport	will	
	 * have	a	name	(code)	n;	n	must	have	exactly	three	characters.	No	two	airports	can	have	the	same	name
	 * @param name The name of the airport
	 */
	void createAirport(String name){
		//check for valid name
		if(name.length() != 3){	
			System.out.println("Usage - Creating Airport: \""+ name +"\" is an invalid airport.\n" +
					"                          An Airport must be three letters.\n" +
					"                          Example: <DEN> or <LAX>");
		}
		else{
			boolean a = true;
			name = name.toUpperCase();

			//check that all characters are valid
			for(char c: name.toCharArray()){
				if(!Character.isLetter(c)){
					System.out.println("Usage - Creating Airport: \""+ name +"\" is an invalid airport.\n" +
							"                          An Airport must be three letters.\n" +
							"                          Example: <DEN> or <LAX>");
					a = false;
					break;
				}
			}

			//check that it doesn't already exist
			for(int i = 0;i<airports.size();i++){
				if(airports.get(i).getName().equals(name)){
					System.out.println("Error - Creating Airport: \""+name+"\" airport already exists in the system.");
					a = false;
					break;
				}
			}

			//if all checks passed add the airport
			if(a){
				Airport aport = new Airport(name);
				airports.add(aport);
			}
		}	
	}

	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////

	/**
	 * creates an airline based off of the string name, however checks if the name
	 * is acceptable and/or if it already exists.  If the name that is given is lower
	 * case, we just automatically put it to upper case.
	 */
	void createAirline(String name){
		name = name.toUpperCase();
		boolean a = true;
		
		// check if only letters
		for(char c: name.toCharArray()){
			if(!Character.isLetter(c)){
				System.out.println("Usage - Creating Airline: \""+ name +"\" is an invalid airline.\n" +
						"                          An Airline must be only letters.\n" +
						"                          Example: <AMER> or <DELTA>");
				a = false;
				break;
			}
		}
		
		if(name.length() >= 6 || name.length() <= 0){
			System.out.println("Usage - Creating Airline: \""+ name + "\" is an invalid airline.\n" + 
					"                          An Airline must be more then 0 but less then 6 characters.");
			a = false;
		}
		
		// check if already exists
		for(int i = 0;i<airlines.size();i++){
			if(airlines.get(i).getName().equals(name)){
				System.out.println("Error - Creating Airline: \""+name+"\" airline already exists in the system.");
				a = false;
				break;
			}
		}

		// if a == true then there where no errors
		if(a){
			Airline aline = new Airline(name);
			airlines.add(aline);
		}
	}

	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////

	/**
	 * creates flights but checks every param indavidually.  If an error occurs
	 * the flight does not get created.
	 */
	void createFlight(String	airlinename,	String	orig,	String	dest,	int	year,	int	month,	int	day,	int hour, int minute, String	id){
		boolean error=false;

		//These are checked individually(not with else if) so a user can get feedback on all parts of a flight at one time
		if (!airlineExist(airlinename)){

			System.out.println("Error - Creating Flight:  \""+airlinename+"\" airline doesn't exist in the system.");
			error = true;
		}
		if (!airportExist(orig)){

			System.out.println("Error - Creating Flight:  The airport \""+orig+"\" doesn't exist in the system.");
			error = true;
		}
		if (!airportExist(dest)){

			System.out.println("Error - Creating Flight:  The airport \""+dest+"\" doesn't exist in the system.");
			error = true;
		}
		if (!checkdate(year,month,day)){

			System.out.println("Usage - Creating Flight:  The date \""+month+"/"+day+"/"+year+"\" for "+airlinename+" flight "+ id+ " is invalid" +
					         "\n                          All dates must be after December 31, 2012");
			error = true;
		}
		if (!checkValidId(id)){

			System.out.println("Usage - Creating Flight:  The flight id \"" + id + "\" is not a valid id");
			System.out.println("                          A valid id contains letters and numbers only and is three to six characters");
			error = true;
		}
		if(!error){
			for(int i = 0;i<airlines.size();i++){
				if(airlines.get(i).getName().equals(airlinename)){
					Flight f = new Flight(airlinename, orig, dest, year, month, day, hour, minute ,id);
					airlines.get(i).addFlight(f);
					break;
				}
			}
		}
	}

	void createSection(String air, String flID, int rows, SeatClass s, int price, char sectionType){
		boolean error = false;

		//These are checked individually(not with else if) so a user can get feedback on all parts of a flight at one time
		if(!airlineExist(air)){
			System.out.println("Error - Creating Section: The airline \"" + air +"\" does not exsit in the system");
			error = true;
		}
		if(!checkValidId(flID)){
			System.out.println("Usage - Creating Section: The flight id \"" + flID + "\" is not a valid id");
			System.out.println("                         A valid id contains letters and numbers only and is three to six characters");
			error = true;		
		}
		if(rows < 1){
			System.out.println("Error - Creating Section: Cannot have less than one row");
			error = true;
		}
		if(!error) {
			FlightSection fs;
			if(sectionType == 'S'){
				fs = new sectionSmall(air, flID, rows, s, price);
			}
			else if(sectionType == 'M'){
				fs = new sectionMedium(air, flID, rows, s, price);
			}
			else{
				fs = new sectionLarge(air, flID, rows, s, price);
			}
			Airline a = getAirLine(air);
			if(a.flightExist(flID))
				a.getFlightByID(flID).addSection(fs);
			else
				System.out.println("Error - Creating Section: The "+ air + " flight " + flID + " does not exist in the system");
		}
	}

	public void setSectionPrice(String airline, String flID, SeatClass sc, int price){
		boolean error = false;
		
		if(!airlineExist(airline)){
			System.out.println("Error - Setting Section Price: The airline \"" + airline +"\" does not exsit in the system");
		}
		if(!checkValidId(flID)){
			System.out.println("Error - Setting Section Price: The flight id \"" + flID + "\" is not a valid id");
			System.out.println("                         A valid id contains letters and numbers only and is three to six characters");
			error = true;		
		}
		if(!getAirLine(airline).getFlightByID(flID).sectionExist(sc)){
			System.out.println("Error - Setting Section Price: The section \"" + sc + "\" does not exist on flight \"" + airline + flID + "\".");
		}
		if(!error){
			char sec = 'l';
			if(sc == SeatClass.first){
				sec = 'f';
			}
			else if(sc == SeatClass.business){
				sec = 'b';
			}
			else{
				sec = 'e';
			}
			getAirLine(airline).getFlightByID(flID).getSection(sec).setPrice(price);
		}
	}
	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////


	/**
	 * finds the available flights and uses the special toString(bool) that is specified
	 * in flight section to just print the available ones.  First does error checking.
	 */
	void findAvailableFlights(String orig, String dest){
		boolean error = false;

		//These are checked individually(not with else if) so a user can get feedback on all parts of a search 
		if(!airportExist(orig)){
			System.out.println("Error - Flight Search: The airport \"" + orig + "\" does not exist in the system");
			error = true;
		}
		if(!airportExist(dest)){
			System.out.println("Error - Flight Search: The airport \"" + dest + "\" does not exist in the system");
			error = true;
		}

		if(!error){
			System.out.println("Available Flights Found: ");
			for(Airline temp : airlines){
				for(Flight flight : temp.getFlights()){
					if(flight.getOrig().equals(orig) && flight.getDest().equals(dest)){
						System.out.println("\t\t"+flight);
						if(flight.getSection('f')!=null)
						System.out.println("\t\t\tFirst Class:    \n"+flight.getSection('f').toString(false));
						if(flight.getSection('b')!=null)
						System.out.println("\t\t\tBusiness Class: \n"+flight.getSection('b').toString(false));
						if(flight.getSection('c')!=null)
						System.out.println("\t\t\tEconomy Class:  \n"+flight.getSection('c').toString(false));
						
					}
				}
			}
		}

	}
	
	public void findAvailableFlights(String orig, String dest, int month, int day, int year, SeatClass sc){
		boolean error = false;
		char sec = 'w';
		if(sc == SeatClass.first){
			sec = 'f';
		}
		else if(sc == SeatClass.business){
			sec = 'b';
		}
		else if(sc == SeatClass.economy){
			sec = 'e';
		}
		else{
			System.out.print("Error - Flight Search: The Seat Class does not appear valid");
			error = true;
		}
		if(!airportExist(orig)){
			System.out.println("Error - Flight Search: The airport \"" + orig + "\" does not exist in the system");
			error = true;
		}
		if(!airportExist(dest)){
			System.out.println("Error - Flight Search: The airport \"" + dest + "\" does not exist in the system");
			error = true;
		}
		if (!checkdate(year,month,day)){

			System.out.println("Error - Flight Search:  The date \""+month+"/"+day+"/"+year+"\" is invalid.");
			error = true;
		}
		if(!error){
			System.out.println("Available Flights Found: ");
			for(Airline temp : airlines){
				for(Flight flight : temp.getFlights()){
					if(flight.getOrig().equals(orig) && flight.getDest().equals(dest) && flight.getDay() == day && flight.getMonth() == month && flight.getYear() == year && flight.sectionExist(sc) && flight.hasAvailableSeat(sc)){
						System.out.println("\t\t"+flight + " ($" + flight.getSection(sec).getPrice()+")");
						
					}
				}
			}
		}
	}

	/**
	 * Books the seat while checking if all exists.  if any param doesn't exists it sends an 
	 * error.
	 * @param air
	 * @param flid
	 * @param s
	 * @param row
	 * @param col
	 */
	void bookSeat(String	air,	String	flid,	SeatClass	s,	int	row,	char	col){
		if(!airlineExist(air)){
			System.out.println("Error - Booking Seat: The airline " + air + " doesn't exist in the system");
		}
		else{
			Airline a = getAirLine(air);
			if(a.flightExist(flid))
				a.getFlightByID(flid).bookSeat(s,row,col);
			else
				System.out.println("Error - Booking Seat: The " + air + " flight " + flid + " does not exist in the system");

		}
		
	}
	
	public void bookSeat(String air, String flID, SeatClass s, int seatPref){
		if(!airlineExist(air)){
			System.out.println("Error - Booking Seat: The airline" + air + "doesn't exist in the system");
		}
		else{
			Airline a = getAirLine(air);
			if(!a.flightExist(flID)){
				System.out.println("Error - Booking Seat: The flight " + flID + " doesn't exist for " + air);
			}
			else{
				Flight f = a.getFlightByID(flID);
				if(!f.sectionExist(s)){
					System.out.println("Error - Booking Seat: The flight " + flID + " does not have a " + s + " section");
				}
				else{
					f.bookSeatByPreference(s, seatPref);
				}
			}
		}
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * This function is pretty intricate with tabs in different toStrings and prints
	 * out an indented version of everything that is created.
	 */
	void displaySystemDetails(){
		System.out.println("\nAirports:");
		for(int i = 0;i<airports.size();i++){
			System.out.println("\t"+airports.get(i));
		}
		System.out.println("\nAirlines:");
		for(int i = 0;i<airlines.size();i++){
			System.out.println("\t"+airlines.get(i));
			int size = airlines.get(i).flights.size();
			for(int j = 0;j<size;j++){
				System.out.println("\t\t"+airlines.get(i).flights.get(j));
				if(airlines.get(i).flights.get(j).getSection('f')!=null)
				System.out.println("\t\t\tFirst Class ($"+airlines.get(i).flights.get(j).getSection('f').getPrice()+"):    \n"+airlines.get(i).flights.get(j).getSection('f').toString(true));
				if(airlines.get(i).flights.get(j).getSection('b')!=null)
				System.out.println("\t\t\tBusiness Class ($"+airlines.get(i).flights.get(j).getSection('b').getPrice()+"): \n"+airlines.get(i).flights.get(j).getSection('b').toString(true));
				if(airlines.get(i).flights.get(j).getSection('e')!=null)
				System.out.println("\t\t\tEconomy Class ($"+airlines.get(i).flights.get(j).getSection('e').getPrice()+"):  \n"+airlines.get(i).flights.get(j).getSection('e').toString(true));

			}
		}
		System.out.println();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * checks if airline Exists
	 * used in error checking
	 */
	private boolean airlineExist(String name){
		for(int i = 0;i<airlines.size();i++){
			if(airlines.get(i).getName().equals(name)){
				return true;
			}
		}
		return false;
	}

	/**
	 * checks if aiport exists already
	 * used in error checking
	 */
	private boolean airportExist(String name){
		for(int i = 0;i<airports.size();i++){
			if(airports.get(i).getName().equals(name)){

				return true;
			}
		}
		//System.out.println("Usage - Creating Flight: \""+name+"\" airport doesn't exist.");
		return false;
	}

	/**
	 * @param name
	 * @return Airline object specific the the name given.
	 */
	private Airline getAirLine(String name){
		for(int i = 0;i<airlines.size();i++){
			if(airlines.get(i).getName().equals(name)){
				return airlines.get(i);
			}
		}
		return null;
	}

	/**
	 * Checks if id is within 3 - 6 chars that are numbers and letters.
	 * @param id
	 * @return true if valid
	 */
	private boolean checkValidId(String id) {
		if(id.matches("[a-zA-Z0-9 ]*") && id.length()<=6 && id.length()>=3)
			return true;
		return false;
	}

	/**
	 * Checks the date given for flights to make sure that it is valid
	 * and if it is earlier than 2013
	 * @param year
	 * @param month
	 * @param day
	 * @return true if valid
	 */
	private boolean checkdate(int year, int month, int day) {
				
		//found at: http://www.mkyong.com/java/how-to-check-if-date-is-valid-in-java/
		String dateToValidate = day+"/"+month+"/"+year;
		String dateFormat = "dd/MM/yyyy";
		 
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false);
 
		try {
			//if not valid, it will return false
			sdf.parse(dateToValidate);
			 
		} catch (Exception e) {
 
			return false;
		}
		if(year < 2013)
			return false;
 
		return true;

	}

}
