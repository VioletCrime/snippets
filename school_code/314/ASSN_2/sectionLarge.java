
public class sectionLarge extends FlightSection{
	public sectionLarge(String air, String flID, int rows, SeatClass s, int price){
		super(air, flID, rows, 10, s, price, getWindowSeats(), getAisleSeats());
	}
	
	public static int[] getWindowSeats(){
		int[] result = new int[2];
		result[0] = 0;
		result[1] = 9;
		return result;
	}
	
	public static int[] getAisleSeats(){
		int[] result = new int[4];
		result[0] = 2;
		result[1] = 3;
		result[2] = 6;
		result[3] = 7;
		return result;
	}
}

/*
 *    0     1     2       3     4     5     6        7     8     9
 * (                |   |                     |   |                )
 * ( Seat Seat Seat |   | Seat Seat Seat Seat |   | Seat Seat Seat )
 * (                |   |                     |   |                )
 * 
 */