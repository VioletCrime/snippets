/*
* Transposition Encryptor by Kyle Smith
* Written for CS356, Fall 2012
*
* In order to compile this and the sister decryption program,
* simply invoke the make script in this directory using the command
* 'make'. Alternatively, if you wish only to compile this program
* sans the decryptor, run 'make encr'.
*
* When you run this program ('./transposition-encr'), you will be
* prompted for two keys, each of which must contain at least ten
* unique alphabetical characters. Finally, you will be asked for 
* the name of the file whose contents are to be encrypted.
*
* When the program has completed, the encrypted test will be located in
* a file in this same direcory named 'Kyle-Smith-encrypted-str.txt'.
*/

#include <string>
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

//Is the key valid? Return true if so.
bool valid_key(string input){
    bool result = false;
    int number_of_letters = 0;
    vector<char> found_letters;
    for(unsigned int i = 0; i < input.length(); i++){
        if((input[i] >= 'a' && input[i] <='z') || (input[i] >= 'A' && input[i] <= 'Z')){
            bool already_found_letter = false;
            for(unsigned int j = 0; j < found_letters.size(); j++){
                if(found_letters[j] == toupper(input[i]) || found_letters[j] == tolower(input[i])){
                    already_found_letter = true;
                    break;
                }
            }
            if(!already_found_letter){
                found_letters.push_back(input[i]);
                number_of_letters++;
            }
        }
    }
    if(number_of_letters > 9){
        result = true;
    }
    return result;
}

//Return a clean, 10-digit version of the key
vector<char> fixKey(string key){
    vector<char> result;
    for(unsigned int i = 0; i < key.length(); i++){
        if((key[i] >= 'a' && key[i] <='z') || (key[i] >= 'A' && key[i] <= 'Z')){
            bool already_found_letter = false;
            for(unsigned int j = 0; j < result.size(); j++){
                if(result[j] == toupper(key[i]) || result[j] == tolower(key[i])){
                    already_found_letter = true;
                    break;
                }
            }
            if(!already_found_letter){
                result.push_back(key[i]);
            }
            if(result.size() > 9){
                break;
            }
        }
    }
    return result;
}

//Return the order in which to read the columns
vector<unsigned int> get_column_order(vector<char> key){
    vector<unsigned int> result;

    //Copy the key for reference
    vector<char> key_copy;
    for(unsigned int i = 0; i < key.size(); i++){
        key_copy.push_back(key[i]);
    }

    //Bubble sort the key into alphabetical order
    for(unsigned int i = key.size(); i > 0; i--){
        for(unsigned int j = 0; j < i; j++){
            if(j + 1 < key.size()){
                if(toupper(key[j]) > toupper(key[j + 1])){
                    char temp = key[j];
                    key[j] = key[j + 1];
                    key[j + 1] = temp;
                }
            }
        }
    }

    //Ascertain the order in which to read columns
    for(unsigned int i = 0; i < key_copy.size(); i++){
        for(unsigned int j = 0; j < key.size(); j++){
            if(toupper(key_copy[j]) == toupper(key[i])){
                result.push_back(j);
                break;
            }
        }
    }
    return result;
}

//The encryption method... pretty small for all the supporting methods...
vector<char> transpose(vector<char> toCrypt, vector<char> key){
    vector<unsigned int> order = get_column_order(key);
    vector<char> result;
    for(unsigned int i = 0; i < order.size(); i++){
        unsigned int index = order[i];
        while(index < toCrypt.size()){
            result.push_back(toCrypt[index]);
            index += 10;
        }
    }
    return result;
}

//main()... what more is there to say?
int main(){
    string keyOne, keyTwo, input_file;
    vector<char> plaintext, singleTrans, doubleTrans, firstKey, secondKey;

    //Get the first key
    cout << "\nTransposition-Encryptor: Please enter the first 10-character key: ";
    getline(cin, keyOne);
    while(!valid_key(keyOne)){
        cout << "Key is invalid. Please re-enter the first 10-character key: ";
        getline(cin, keyOne);
    }

    //Get the second key
    cout << "\nPlease enter the second 10-character key: ";
    getline(cin, keyTwo);
    while(!valid_key(keyTwo)){
        cout << "Key is invalid. Please re-enter the first 10-character key: ";
        getline(cin, keyTwo);
    }

    //Get the input file
    cout << "\nEnter the name of the file whose contents are to be encrypted: ";
    cin >> input_file;
    ifstream read_file;
    read_file.open(input_file.c_str());
    while(!read_file.is_open()){
         cout << "Could not open the input file. Please re-enter input file: ";
         cin >> input_file;
         read_file.open(input_file.c_str());
    }

    //Read from input file
    while(read_file.good()){
        string input;
        read_file >> input;
        for(unsigned int i = 0; i < input.length(); i++){
            if((input[i] >= 'a' && input[i] <='z') || (input[i] >= 'A' && input[i] <= 'Z')){
                plaintext.push_back(input[i]);
            }
        }
    }

    //Pad if necessary
    unsigned int to_pad = plaintext.size() % 10;
    for(unsigned int i = 0; i < to_pad; i++){
        plaintext.push_back('x');
    }

    //Clean up the keys
    firstKey = fixKey(keyOne);
    secondKey = fixKey(keyTwo);

    //Transposition time!
    singleTrans = transpose(plaintext, firstKey);
    doubleTrans = transpose(singleTrans, secondKey);

    //Output result to file
    string out_string = "";
    for(unsigned int i = 0; i < doubleTrans.size(); i++){
        out_string += doubleTrans[i];
    }
    ofstream outFile;
    outFile.open("Kyle-Smith-encrypted-str.txt");
    outFile << out_string;
    outFile.close();
}
