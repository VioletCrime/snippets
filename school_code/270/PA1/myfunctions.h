/**
 * File:        myfunctions.h
 * Description: Functions used by main in PA1
 * Author(s):   David Alvillar
 * Date:        08/27/2011
 * Modified:    08/29/2011
 *
 * ATTN: Students must not modify this file. The GTA
 * will grade your submission using an unmodified
 * copy of this file.
 **/
#ifndef _MY_FUNCTIONS_H_
#define _MY_FUNCTIONS_H_

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define RADIX_LIMIT_HIGH 36
#define RADIX_LIMIT_LOW 2
#define PLACES_LIMIT 128

//global variable
char radixNNumber[PLACES_LIMIT];

/**
 * Description: 
 * Converts a symbol into its decimal number equivalent (0->0, 1->1, ... 9->9, A->10, 
 * B->11, ..., Z->35).  This is the inverse function of decimalToSymbol.
 * Arguments:
 * symbol - a char ranging between '0' and '9' or between 'A' and 'Z'
 * Postcondition:
 * Returns the decimal equivalent of symbol
 **/
int symbolToDecimal(char symbol);

/**
 * Description:
 * Converts a decimal number into its symbol equivalent (0->0, 1->1, ..., 9->9, 10->A,
 * 11->B, ..., 35->Z).  This is the inverse function of symbolToDecimal.
 * Arguments:
 * decimalNumber - a non-negative integer ranging between 0 and 35
 * Postcondition:
 * Returns the symbol equivalent of decimalNumber
 **/
char decimalToSymbol(int decimalNumber);

/**
 * Description:
 * Converts a number (input as a string) in an indicated radix to 
 * a decimal number
 * Arguments: 
 * radixNNumber - a string representing the non-negative radixN integer to convert,
 * with most significant digit on the left.
 * radixN - a non-negative integer between 2 and 36 indicating the radix of radixNNumber
 * Precondition:
 * Digits of the string radixNNumber are valid for the given radix, radixN
 * Postcondition:
 * Returns the decimal value of radixNNumber as an integer
 **/
int radixNToDecimal(char radixNNumber[], int radixN);

/**
 * Description:
 * Converts a decimal number (output as a string) to the indicated radix
 * Arguments: 
 * decimalNumber - the non-negative integer to convert to radixN
 * radixN - a non-negative integer between 2 and 36 indicating the radix 
 * to convert decimalNumber into
 * result - an uninitialized array of char used to build and return
 * the string representation of decimalNumber in radixN
 * Postcondition:
 * Returns the radixN value of radixNNumber as a string, with the most significant
 * digit on the left.
 **/
char* decimalToRadixN(int decimalNumber, int radixN, char result[]);

/**
 * Description:
 * Converts a number (input as a string) in an indicated radix to another
 * radix (output as a string) 
 * Arguments:
 * radixANumber - a string representing the non-negative radixA integer to convert
 * radixA - a non-negative integer between 2 and 36 indicating the 
 * original radix of radixAnumber
 * radixB - a non-negative integer between 2 and 36 indicating the radix
 * to convert radixANumber into
 * result - an uninitialized array of char used to build and return
 * the string representation of radixANumber in radixB
 * Precondition:
 * Digits of the string radixNNumber are valid for the given radix, radixA
 * Postcondition:
 * Returns the radixB value of radixAnumber as a string
 **/
char* radixAToRadixB(char radixANumber[], int radixA, int radixB, char result[]);

/**
*Description:
*Student-defined method used to implement the 'pow()' function which is peculiarly
*AWOL (even using #include <math.h>). A google search indicates that I must add a
*'-lm' term to the compile command, which seemed out-of-bounds for the assignment.
*base - an integer which is to be raised to a power
*exp - a non-negative integer; the power to which 'base' is to be raised.
*Precondition:
*????
*Postcondition:
*Profit!
**/
//int powa(int base, int exp);
#endif
