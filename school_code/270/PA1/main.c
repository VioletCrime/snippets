/**
 * File:        main.c
 * Description: The main routine for PA1. 
 * Author(s):   David Alvillar
 * Date:        08/27/2011
 * Modified:    08/29/2011
 *
 * ATTN: Students must not modify this file. The GTA
 * will grade your submission using an unmodified
 * copy of this file.
 **/
#include <stdio.h>
#include "myfunctions.h"

void usage() {
  printf("Usage: pa1 number fromRadix toRadix\n");
}

//program entry point
int main(int argc, char **args){
  
  char *radixANumber;
  char *radixBNumber;
  int radixA;
  int radixB;
  
  if(argc != 4){
    usage();
  }
  else{
    radixANumber = args[1];  
    radixA = atoi(args[2]);
    radixB = atoi(args[3]);
    
    //check radix A
    if(radixA < RADIX_LIMIT_LOW || radixA > RADIX_LIMIT_HIGH){
      printf("fromRadix must be between %d and %d\n", RADIX_LIMIT_LOW, RADIX_LIMIT_HIGH);
      return(EXIT_FAILURE);
    }
    //check radix B
    else if(radixB < RADIX_LIMIT_LOW || radixB > RADIX_LIMIT_HIGH){
      printf("toRadix must be between %d and %d\n", RADIX_LIMIT_LOW, RADIX_LIMIT_HIGH);
    }
    //check that every char in radixANumber < radixA 
    for(int i = 0; radixANumber[i] != '\0'; i++){
      if(symbolToDecimal(radixANumber[i]) >= radixA){
	printf("%s is not a valid number in radix %d\n", radixANumber, radixA);
	return(EXIT_FAILURE);
      }
    }
    //run and print the tests
    //part i
    int test1result = radixNToDecimal(radixANumber, radixA);
    printf("Function 1: %s (radix %d) = %d (radix 10)\n", radixANumber, radixA, test1result);
    //part ii
    char *test2result = decimalToRadixN(test1result, radixB, radixNNumber);
    printf("Function 2: %d (radix 10) = %s (radix %d)\n", test1result, test2result, radixB);
    //part iii
    radixBNumber = radixAToRadixB(radixANumber, radixA, radixB, radixNNumber);   
    printf("Function 3: %s (radix %d) = %s (radix %d)\n", radixANumber, radixA, radixBNumber, radixB);
  }

  return(EXIT_SUCCESS);
}
