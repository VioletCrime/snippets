#include "logic.h"

void word_not(lc3_word_t *R, lc3_word_t A) {
  R->bit[15]   = (A.bit[15]   == 1) ? 0: 1;
  R->bit[14]   = (A.bit[14]   == 1) ? 0: 1;
  R->bit[13]   = (A.bit[13]   == 1) ? 0: 1;
  R->bit[12]   = (A.bit[12]   == 1) ? 0: 1;
  R->bit[11]   = (A.bit[11]   == 1) ? 0: 1;
  R->bit[10]   = (A.bit[10]   == 1) ? 0: 1;
  R->bit[9]   = (A.bit[9]   == 1) ? 0: 1;
  R->bit[8]   = (A.bit[8]   == 1) ? 0: 1;
  R->bit[7]   = (A.bit[7]   == 1) ? 0: 1;
  R->bit[6]   = (A.bit[6]   == 1) ? 0: 1;
  R->bit[5]  = (A.bit[5]  == 1) ? 0: 1;
  R->bit[4]  = (A.bit[4]  == 1) ? 0: 1;
  R->bit[3]  = (A.bit[3]  == 1) ? 0: 1;
  R->bit[2]  = (A.bit[2]  == 1) ? 0: 1;
  R->bit[1]  = (A.bit[1]  == 1) ? 0: 1;
  R->bit[0]  = (A.bit[0]  == 1) ? 0: 1;
}


void word_and(lc3_word_t *R, lc3_word_t A, lc3_word_t B) {
    R->bit[15] = ((A.bit[15] == 1) && ((B.bit[15] == 1))) ? 1 : 0;
    R->bit[14] = ((A.bit[14] == 1) && ((B.bit[14] == 1))) ? 1 : 0;
    R->bit[13] = ((A.bit[13] == 1) && ((B.bit[13] == 1))) ? 1 : 0;
    R->bit[12] = ((A.bit[12] == 1) && ((B.bit[12] == 1))) ? 1 : 0;
    R->bit[11] = ((A.bit[11] == 1) && ((B.bit[11] == 1))) ? 1 : 0;
    R->bit[10] = ((A.bit[10] == 1) && ((B.bit[10] == 1))) ? 1 : 0;
    R->bit[9] = ((A.bit[9] == 1) && ((B.bit[9] == 1))) ? 1 : 0;
    R->bit[8] = ((A.bit[8] == 1) && ((B.bit[8] == 1))) ? 1 : 0;
    R->bit[7] = ((A.bit[7] == 1) && ((B.bit[7] == 1))) ? 1 : 0;
    R->bit[6] = ((A.bit[6] == 1) && ((B.bit[6] == 1))) ? 1 : 0;
    R->bit[5] = ((A.bit[5] == 1) && ((B.bit[5] == 1))) ? 1 : 0;
    R->bit[4] = ((A.bit[4] == 1) && ((B.bit[4] == 1))) ? 1 : 0;
    R->bit[3] = ((A.bit[3] == 1) && ((B.bit[3] == 1))) ? 1 : 0;
    R->bit[2] = ((A.bit[2] == 1) && ((B.bit[2] == 1))) ? 1 : 0;
    R->bit[1] = ((A.bit[1] == 1) && ((B.bit[1] == 1))) ? 1 : 0;
    R->bit[0] = ((A.bit[0] == 1) && ((B.bit[0] == 1))) ? 1 : 0;
}


void word_add(lc3_word_t *R, lc3_word_t A, lc3_word_t B) {
  BIT *tw_a = (BIT*)(A.bit);
  BIT *tw_b = (BIT*)(B.bit);
  BIT *tw_r = (BIT*)(R->bit);
  BIT carry = 0;        // carry bit at each addition step
  for (int i=15; i >= 0; i--) {
    if (*(tw_a+i) == 1) {
      if (*(tw_b+i) == 1) {
        // if a == 1, b == 1, c == 1 -> r = 1, c = 1
        // if a == 1, b == 1, c == 0 -> r = 0, c = 1
        *(tw_r+i) = (carry == 1) ? 1 : 0;
        carry = 1;
      }
      else {
        // if a == 1, b == 0, c == 1 -> r = 0, c = 1
        // if a == 1, b == 0, c == 0 -> r = 1, c = 0
        *(tw_r+i) = (carry == 1) ? 0 : 1;
        // carry remains same
      }
    }
    else {
      if (*(tw_b+i) == 1) {
        // if a == 0, b == 1, c == 1 -> r = 0, c = 1
        // if a == 0, b == 1, c == 0 -> r = 1, c = 0
        *(tw_r+i) = (carry == 1) ? 0 : 1;
        // carry remains same
      }
      else {
        // if a == 0, b == 0, c == 1 -> r = 1, c = 0
        // if a == 0, b == 0, c == 0 -> r = 0, c = 0
        *(tw_r+i) = (carry == 1) ? 1 : 0;
        carry = 0;
      }
    }
  }
}


lc3_word_t four_bit_decoder(BIT b3, BIT b2, BIT b1, BIT b0) {
  int count = 0;
  count =  (b0 == 1) ? 1 : 0;
  count += (b1 == 1) ? (1 << 1) : 0;
  count += (b2 == 1) ? (1 << 2) : 0;
  count += (b3 == 1) ? (1 << 3) : 0;
  lc3_word_t decoded = {{0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}};
  switch(count) {
        case 1: decoded.bit[14] = 1;
            break;
        case 2: decoded.bit[13] = 1;
            break;
        case 3: decoded.bit[12] = 1;
            break;
        case 4: decoded.bit[11] = 1;
            break;
        case 5: decoded.bit[10] = 1;
            break;
        case 6: decoded.bit[9] = 1;
            break;
        case 7: decoded.bit[8] = 1;
            break;
        case 8: decoded.bit[7] = 1;
            break;
        case 9: decoded.bit[6] = 1;
            break;
        case 10: decoded.bit[5] = 1;
            break;
        case 11: decoded.bit[4] = 1;
            break;
        case 12: decoded.bit[3] = 1;
            break;
        case 13: decoded.bit[2] = 1;
            break;
        case 14: decoded.bit[1] = 1;
            break;
        case 15: decoded.bit[0] = 1;
            break;
        default: decoded.bit[15] = 1;
  }
  return decoded;
}


lc3_word_t three_bit_decoder(BIT b2, BIT b1, BIT b0) {
  return four_bit_decoder(0, b2, b1, b0);
}
