#ifndef CS270_PA6_LC3_REGISTERS_H_
#define CS270_PA6_LC3_REGISTERS_H_
#include "lc3machinetypes.h"
#include "helper.h"
#include "memorylayout.h"

#define MAX_LABEL_SIZE      128
/**
 * Writes the contents of (argument) "word" to the register specified
 * by "register_name"
 *
 * Prints an error if an incorrect register name is specified
 */
void write_register(lc3_register_names_t register_name, lc3_word_t word);

/**
 * Reads the contents of a register specified by argument "register_name"
 * and returns the value inside that register.
 *
 * Returns a null word if an incorrect register name is specified
 */
lc3_register_t read_register(lc3_register_names_t register_name);

/**
 * Sets (to 1) the status bit specified by the argument "code" in the PSR
 */
void set_status_code(lc3_status_codes_t code);

/**
 * Resets (to 0) the status bit specified by the argument "code" in the PSR
 */
void reset_status_code(lc3_status_codes_t code);

/**
 * Tests whether a particular status bit (specified by the argument "code") is
 * set to 1 or 0
 * 
 * Returns 1 if the status bit is 1 in the PSR, otherwise returns 0
 */
int test_status_code(lc3_status_codes_t code);

/**
 * Writes the contents of MDR register into the location pointed to by MAR
 *
 * Aborts the program (and prints an error) if unable to write to a location
 */
void write_memory();

/**
 * Reads contents of the location pointed to by MAR and stores it into MDR
 *
 * Prints an error message to the console and sets MDR to null word if the
 * location pointed to by MAR is not a location that has been previously
 * written into (i.e., an invalid read).
 */
void read_memory();

// enables some debug information output for memory read and write
void enable_debug_info_output();

// memory manager !DO NOT MODIFY!
extern memory_manager_obj_t memory_manager;

#endif
