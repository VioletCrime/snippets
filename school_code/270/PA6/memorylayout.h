#ifndef CS270_PA6_MEMORY_LAYOUT_H_
#define CS270_PA6_MEMORY_LAYOUT_H_

#include "lc3machinetypes.h"
#include "helper.h"
#include <assert.h>

/**
 * file: memorylayout.c
 * desc:
 *  The data structure and related functions for storing and accessing the
 *  memory of an LC-3 program. The layout is stored as a sorted doubly linked
 *  list.
 */

/**
 * Represents a memory location in the LC-3 machine
 *
 *  label   - points to a symbolic label of the location
 *  address - is the 16-bit address of this location
 *  numeric_address - numeric value of the address for fast lookup
 *  word    - is the data stored at this location
 *  numeric_word    - numeric value of the word for debugging
 */
typedef struct memory_location__ {
  char                    *label;
  lc3_address_t           address;
  uint16_t                numeric_address;
  lc3_word_t              word;
  uint16_t                numeric_word;
} memory_location_t;

/**
 * A doubly linked list which stores the memory layout of a currently
 * executing LC-3 program.
 *
 *  location  - A memory location in the layout (see memory_location_t)
 *  prev_loc  - Points to the previous active memory location
 *  next_loc  - Points to the next active (or empty) memory location
 */
typedef struct memory_layout__ {
  memory_location_t       location;
  struct memory_layout__  *prev_loc;
  struct memory_layout__  *next_loc;
} memory_layout_t;


/**
 * Memory manager object
 *
 *  begin       - always points to the start of the list
 *  end         - always points to end of LC-3 memory
 *  cached      - used for speedy lookups (when possible)
 *  size        - number of active locations in the layout
 *
 *  Note:
 *   You don't need to implement cached lookups, (i.e., ignore "cached")
 *   just go through the doubly linked list from start to end everytime
 *   to find a memory location.
 */
typedef struct memory_manager__ {
  memory_layout_t           *begin;
  memory_layout_t           *end;
  memory_layout_t           *cached;
  int                       size;
} memory_manager_obj_t;


/**
 * Initialize the memory layout and memory manager object
 *
 * Args:
 *  memory manager object
 */
void mm_init(memory_manager_obj_t *mm_obj);

/**
 * Insert a memory location into the layout.
 *
 * The memory location is inserted "in-place" in the layout (i.e., in the
 * sorted order). Note that this function will silently update the word
 * stored at a location if that location already exists in the layout
 *
 * Args:
 *  - memory manager object of the current memory layout
 *  - memory location to be inserted
 * Returns:
 *  0 on success, and a negative value on failure
 */
int mm_sorted_insert(memory_manager_obj_t *mm_obj, memory_location_t mem);

/**
 * Lookup the word stored at a memory location
 *
 * Args:
 *  - memory manager object of the current memory layout
 *  - memory location to be looked up. 
 *    - On success, the word at the specified memory location is returned
 *      implicitly in the arg (i.e., mem->word will be set). The value is
 *      undefined if the call does not succeed. 
 * Returns:
 *  0 on success, and a negative value on failure
 *  
 */
int mm_lookup_word(memory_manager_obj_t *mm_obj, memory_location_t *mem);

/**
 * Get the next program referenced memory location.
 *
 * This function is primarily used to dump the memory used by the program at
 * the end of executing the assembly program. Note that calling this function
 * with interleaved calls to insert will result in unexpected behavior
 *
 * Args:
 *  - memory manager object of the current memory layout
 *  - a memory layout iterator. If the iterator is NULL, the starting location
 *    of the referenced memory is returned (usually the ORIGIN location).
 *    Subsequent calls should pass the previous iterator value to get the
 *    next location.
 * Returns:
 *  memory location (i.e., address, word stored at address ...), implicitly
 *  returns the next location in the layout in the second arg.
 */
memory_location_t mm_next_location(memory_manager_obj_t *mm_obj,
                                   memory_layout_t **iterator);

/**
 * Clear the program memory layout (i.e, remove all memory locations)
 *
 * Note that the simulator never removes individual locations from the layout.
 * That is, once a memory location is addressed by the program, it is stored
 * in the layout permanently until the simulator is reset.
 */
void mm_clear(memory_manager_obj_t *mm_obj);

/**
 * Print the contents of a location to screen
 */
void print_location(memory_location_t location);

#endif
