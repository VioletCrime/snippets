#ifndef CS270_PA6_LC3_MACHINE_TYPES_H_
#define CS270_PA6_LC3_MACHINE_TYPES_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

/**
 * Basic element of the LC-3 machine.
 *
 * BIT takes on values ZERO (0) or ONE (1)
 */
typedef enum {
    ZERO = (uint8_t) 0, ONE = (uint8_t) 1
} BIT;

/**
 * A 16-"BIT" LC-3 word in big-endian format
 */
struct lc3_word_t__ {
    BIT bit[16];
};
typedef struct lc3_word_t__ lc3_word_t;
typedef struct lc3_word_t__ lc3_register_t;
typedef struct lc3_word_t__ lc3_address_t;

/**
 * Symbolic names of the registers.
 */
typedef enum lc3_register_names__ {
    IR, // instruction register
    PC, // program counter
    MAR, // memory address register
    MDR, // memory data register
    PSR, // program status register
    SEXT, // sign extended word register
    R0, // general purpose registers
    R1,
    R2,
    R3,
    R4,
    R5,
    R6,
    R7
} lc3_register_names_t;

/**
 * Symbolic names for status codes.
 *
 * **LC3 machine has additional codes, but we dont support them**
 */
typedef enum lc3_status_codes__ {
    CODE_N,
    CODE_Z,
    CODE_P
} lc3_status_codes_t;

/**
 * Symbolic names of the instructions
 *
 * ** current not in use **
 */
typedef enum lc3_instruction_names__ {
    // supported by the simulator
    ADD,
    AND,
    BR,
    JMP,
    JSR,
    LD,
    LDI,
    LDR,
    LEA,
    NOT,
    RET,
    ST,
    STI,
    STR,
    RESERVED,
    // partial support
    TRAP,
    // not supported
    RTI
} lc3_instruction_names_t;

#define LC3_MEMORY_START_ADDRESS    0x0000
#define LC3_MEMORY_END_ADDRESS      0xFFFF

#endif
