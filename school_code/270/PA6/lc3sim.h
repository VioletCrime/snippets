#ifndef CS270_PA2_LC3SIM_H_
#define CS270_PA2_LC3SIM_H_

#include "logic.h"
#include "lc3machinetypes.h"
#include "lc3registers.h"

/**
 * Sign extends immediate 5-bit value inside an instruction
 *
 * Implicitly reads the instruction from the IR register and stores
 * the result in SEXT register
 */
void sign_extend_imm5();

/**
 * Sign extends the 6-bit offset value inside an instruction
 *
 * Implicitly reads the instruction from the IR register and stores
 * the result in SEXT register
 */
void sign_extend_offset6();

/**
 * Sign extends the 9-bit offset value inside an instruction
 *
 * Implicitly reads the instruction from the IR register and stores
 * the result in SEXT register
 */
void sign_extend_PCOffset9();

/**
 * Sign extends the 11-bit offset value inside an instruction
 *
 * Implicitly reads the instruction from the IR register and stores
 * the result in SEXT register
 */
void sign_extend_PCOffset11();

/**
 * Selects a register (from the register file) based on the three input
 * bits. The bits are decoded to find out which register to select.
 *
 * Returns the selected register's symbolic name
 */
lc3_register_names_t gpr_select(BIT b2, BIT b1, BIT b0);

/**
 * (helper routine) Increment the Program Counter (i.e., add 1 to the PC)
 */
void increment_pc();

/**
 * (helper routine) Fetch the next instruction from memory
 */
void lc3_instruction_fetch();

/**
 * (helper routine) Add the contents of two registers specified by "sr1" 
 * and "sr2" and store the result in register "dr".
 *
 * This method is simply a wrapper around word_add(), but allows the programmer
 * to specify symbolic names of registers rather than a register variable
 */
void register_add(lc3_register_names_t dr, lc3_register_names_t sr1,
                  lc3_register_names_t sr2);

/**
 * (helper routine) Sets the condition codes based on the value inside
 * register identified by "reg".
 */
void setcc(lc3_register_names_t reg);

/**
 * The main routine to fetch, decode, and execute the next LC-3 instruction
 *
 * Returns HALT (integer value 100) when there are no more instructions to
 * execute, -1 on error (unsupported instructions), and 0 on success
 */
#define LC3_SIM_HALT      100
int lc3_evaluate_instruction();

/**
 * Execute the operation specified by the opcode
 */
void lc3_execute_BR();
void lc3_execute_ADD();
void lc3_execute_LD();
void lc3_execute_ST();
void lc3_execute_JSR();
void lc3_execute_AND();
void lc3_execute_LDR();
void lc3_execute_STR();
void lc3_execute_NOT();
void lc3_execute_LDI();
void lc3_execute_STI();
void lc3_execute_JMP();
void lc3_execute_LEA();
#endif
