#ifndef CS270_PA6_INPUT_FILES_PARSER_H_
#define CS270_PA6_INPUT_FILES_PARSER_H_

#include "lc3machinetypes.h"
#include "helper.h"
#include <string.h>
#ifdef __APPLE__
#include <machine/endian.h>
#elif __linux__
#include <endian.h>
#endif

#define MAX_FILENAME_SIZE         256

#define MAX_LINE_SIZE             1024
#define MAX_TOKEN_SIZE            128
// max tokens per line = (max_line_size / max token size)
#define MAX_TOKENS_PER_LINE       8
// max number of args on any lc3sim command
#define MAX_COMMAND_ARGS          2

/**
 * Valid commands in LC-3 simulator commands file
 */
typedef enum sim_commands__ {
  // following are supported by our simulator
  SIMCMD_INVALID,
  SIMCMD_FILE,
  SIMCMD_CONTINUE,
  SIMCMD_MEMORY,
  SIMCMD_QUIT,
  SIMCMD_TRANSLATE,
  SIMCMD_REGISTER,
  SIMCMD_PRINTREGS,

  // following are not implemented and hence not supported by our simulator
  SIMCMD_BREAK,
  SIMCMD_FINISH,
  SIMCMD_STEP,
  SIMCMD_NEXT,
  SIMCMD_DUMP,
  SIMCMD_LIST,
  SIMCMD_HELP,
  SIMCMD_EXECUTE,
  SIMCMD_RESET
} sim_commands_t;

/**
 * Get the next simulator command from the commands file
 * 
 * The command file is what lc3sim from the lc3tool chain accepts. Not all
 * commands are supported, and these are safely ignored. Has very rudimentory
 * error checking, so the user should not use command files with serious errors
 *
 * The first arg is a file stream pointer (to an open file) pointing to the
 * commands file. Note that the file stream pointer must point to an open file.
 * The second arg is a pointer to a command code, and the third arg is a 2-D
 * string array to hold the arguments for certain commands.
 *
 * For example, the "memory" command accepts two arguments that are stored in
 * this array.
 *
 * Returns 0 on success, EOF if the commands file has reached end-of-file, and
 * a negative value on any errors.
 */
int simulator_command_getnext(FILE *cmdsfile, sim_commands_t *command,
                              char cmdargs[][MAX_TOKEN_SIZE]);

/**
 * Get the next lc3 instruction read from an lc3 object file
 *
 * The first argument is a file stream pointer (to an open file) pointing to
 * the object file, the second arg is pointer to an lc3_word.
 *
 * Upon successfully reading the next instruction from the object file, the
 * function implicitly converts the binary instruction to an lc3 word and
 * returns in the second arg.
 *
 * Return 0 on success, EOF if the obj file has reached end-of-file, and
 * a negative value on errors
 */
int objfile_instruction_getnext(FILE *objfile, lc3_word_t *instruction);

/**
 * Get the next symbol and its associated address from the symbol table file
 *
 * First arg is a file stream pointer to the symbol table file, and
 * second arg is a 2-d array to hold the label string and the address
 *
 * Returns 0 on success (and copies the symbol information to the 2nd arg),
 * EOF if the file has reached end-of-file or a negative value on failure
 */
int symbol_table_getnext(FILE *symfile, char label2addr[][MAX_TOKEN_SIZE]);

/**
 * Takes a valid LC-3 obj file name and returns the symbol table file name
 *
 * First arg is the object file name and second arg is a pointer to a string
 *
 * Returns 0 on success and negative number on failure. On success, the symbol
 * table file name is copied into "symfile_name" (caller allocated string).
 */
int generate_symbol_table_filename(const char *objfile_name, char *symfile_name);

#ifdef __APPLE__
// getline support on Mac OS X
ssize_t getline(char **lineptr, size_t *n, FILE *stream);
#endif

/**
 * Validate the LC-3 object file name (i.e., whether it has .obj extension)
 */
int validate_object_filename(const char *objfile_name);

#endif
