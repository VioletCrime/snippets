#include "infilesparser.h"

// static functions
/**
 * Private function to parse a command string and return a code representing
 * that command. See < sim_commands_t > for the codes
 */
static sim_commands_t __simulator_command_str2cmd(char *cmd_string);
/**
 * Function to tokenize an input line into seperate tokens
 *
 * The first arg is the line to be tokenized and the second arg points to
 * a 2-dimentional string array. The number of rows of this array should be
 * at least MAX_TOKENS_PER_LINE size, and the number of columns (i.e., length
 * of each string should be at least MAX_TOKEN_SIZE)
 *
 * Returns 0 on success and negative number on failure
 */
static
int __tokenize(char *line, char tokens[][MAX_TOKEN_SIZE], int *num_tokens);



int simulator_command_getnext(FILE *cmdsfile, sim_commands_t *command,
                              char cmdargs[][MAX_TOKEN_SIZE])
{
  if ((cmdsfile == NULL) || (command == NULL) || (cmdargs == NULL))
    return -1;

  char    *line       = NULL;
  size_t  line_length = MAX_LINE_SIZE;
  char    tokens[MAX_TOKENS_PER_LINE][MAX_TOKEN_SIZE];

  for (;;) {
    int bytes, rv, num_tokens;
    if ((bytes = getline(&line, &line_length, cmdsfile)) == -1)
      return EOF;

    if ((rv = __tokenize(line, tokens, &num_tokens)) < 0) {
      if (line) free(line);
      line = NULL;
      return rv;
    }

    *command = __simulator_command_str2cmd(tokens[0]);
    if (line) free(line);
    line = NULL;
    switch (*command) {
        // these have one arg
      case SIMCMD_FILE:
      case SIMCMD_TRANSLATE:
        strncpy(cmdargs[0], tokens[1], MAX_TOKEN_SIZE);
        return 0;
        // these have two args
      case SIMCMD_MEMORY:
      case SIMCMD_REGISTER:
        strncpy(cmdargs[0], tokens[1], MAX_TOKEN_SIZE);
        strncpy(cmdargs[1], tokens[2], MAX_TOKEN_SIZE);
        return 0;
        // these have no args
      case SIMCMD_CONTINUE:
      case SIMCMD_QUIT:
      case SIMCMD_PRINTREGS:
        return 0;
        // these are not supported
      case SIMCMD_BREAK:
      case SIMCMD_FINISH:
      case SIMCMD_STEP:
      case SIMCMD_NEXT:
      case SIMCMD_DUMP:
      case SIMCMD_LIST:
      case SIMCMD_HELP:
      case SIMCMD_EXECUTE:
      case SIMCMD_RESET:
        info(__func__, "Unsupported command %s ... ignoring", tokens[0]);
        break;
      default:
        dump_error(__func__, "Unrecognized command %s ... ignoring", tokens[0]);
    }
  }
  return -1;
}


static
sim_commands_t __simulator_command_str2cmd(char *cmd_string) {
  if (strcasecmp(cmd_string, "file")      == 0)
    return SIMCMD_FILE;
  if (strcasecmp(cmd_string, "continue")  == 0)
    return SIMCMD_CONTINUE;
  if (strcasecmp(cmd_string, "memory")    == 0)
    return SIMCMD_MEMORY;
  if (strcasecmp(cmd_string, "quit")      == 0)
    return SIMCMD_QUIT;
  if (strcasecmp(cmd_string, "translate") == 0)
    return SIMCMD_TRANSLATE;
  if (strcasecmp(cmd_string, "register")  == 0)
    return SIMCMD_REGISTER;
  if (strcasecmp(cmd_string, "printregs") == 0)
    return SIMCMD_PRINTREGS;
  if (strcasecmp(cmd_string, "break")     == 0)
    return SIMCMD_BREAK;
  if (strcasecmp(cmd_string, "finish")    == 0)
    return SIMCMD_FINISH;
  if (strcasecmp(cmd_string, "step")      == 0)
    return SIMCMD_STEP;
  if (strcasecmp(cmd_string, "next")      == 0)
    return SIMCMD_NEXT;
  if (strcasecmp(cmd_string, "dump")      == 0)
    return SIMCMD_DUMP;
  if (strcasecmp(cmd_string, "list")      == 0)
    return SIMCMD_LIST;
  if (strcasecmp(cmd_string, "help")      == 0)
    return SIMCMD_HELP;
  if (strcasecmp(cmd_string, "execute")   == 0)
    return SIMCMD_EXECUTE;
  if (strcasecmp(cmd_string, "reset")     == 0)
    return SIMCMD_RESET;
  return SIMCMD_INVALID;
}

// adopted from /usr/include/linux/swab.h
#define swap_short(x) (x = (uint16_t)( \
            (((uint16_t)(x) & (uint16_t)0x00ff) << 8) | \
            (((uint16_t)(x) & (uint16_t)0xff00) >> 8)))

int objfile_instruction_getnext(FILE *objfile, lc3_word_t *instruction) {
  if ((objfile == NULL) || (instruction == NULL))
    return -1;

  uint16_t  word;
  int byte = 0;
  if ((byte = fread(&word, 2, 1, objfile)) == 0)
    return EOF;

#if __APPLE__ && BYTE_ORDER == __DARWIN_LITTLE_ENDIAN
  swap_short(word);
#elif __linux__ && BYTE_ORDER == LITTLE_ENDIAN
  swap_short(word);
#endif

  *instruction = convert_numeric2lc3word(word);
  return 0;
}


//int symbol_table_getnext(FILE *symfile, char label2address[][MAX_TOKEN_SIZE])
//{
  //TO BE COMPLETED
//}


int generate_symbol_table_filename(const char *objfile_name, char *symfile_name)
{
  if ((objfile_name == NULL) || (symfile_name == NULL))
    return -1;

  int rv = 0;
  strncpy(symfile_name, objfile_name, MAX_FILENAME_SIZE);
  if ((rv = validate_object_filename(objfile_name)) != 0)
    return rv;

  char *extension = strchr(symfile_name, '.');
  extension++;
  strcpy(extension, "sym");
  return 0;
}


int validate_object_filename(const char *objfile_name) {
  char *extension = NULL;
  if ((extension = strchr(objfile_name, '.')) == NULL) {
    dump_error(__func__, "Invalid object file name %s : missing extension",
               objfile_name);
    return -10;
  }
  //extension++;
  if (!strcasecmp(extension, "obj")) {
    dump_error(__func__, "Invalid object file name %s : wrong extension",
               objfile_name);
    return -11;
  }
  return 0;
}


//static int __tokenize(char *line, char tokens[][MAX_TOKEN_SIZE], int *num_tokens)
//{
  //TO BE COMPLETED
//}


#ifdef __APPLE__
ssize_t getline(char **lineptr, size_t *n, FILE *stream) {
  char    *line       = NULL;
  size_t  line_length = 0;
  if ((line = fgetln(stream, &line_length)) == NULL) {
    *n = 0;
    return -1;
  }
  if (*lineptr == NULL)
    *lineptr = (char*)malloc(line_length+2);
  memcpy(*lineptr, line, line_length);
  *(*lineptr + line_length + 1) = '\0';
  *n = line_length + 1;
  return line_length;
}
#endif

