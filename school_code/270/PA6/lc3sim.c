#include "lc3sim.h"

void sign_extend_imm5() {
	    lc3_word_t i_ = (lc3_word_t) read_register(IR);
    if (i_.bit[11] == 0) {
        lc3_word_t w_ = {{0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, i_.bit[11],
            i_.bit[12], i_.bit[13], i_.bit[14], i_.bit[15]}};
        write_register(SEXT, w_);
    } else {
        lc3_word_t w_ = {{1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, i_.bit[11],
            i_.bit[12], i_.bit[13], i_.bit[14], i_.bit[15]}};
        write_register(SEXT, w_);
    }
}

lc3_register_names_t gpr_select(BIT b2, BIT b1, BIT b0) {
    lc3_word_t decoded_ = three_bit_decoder(b2, b1, b0);
    if (decoded_.bit[15]) return R0;
    if (decoded_.bit[14]) return R1;
    if (decoded_.bit[13]) return R2;
    if (decoded_.bit[12]) return R3;
    if (decoded_.bit[11]) return R4;
    if (decoded_.bit[10]) return R5;
    if (decoded_.bit[9]) return R6;
    if (decoded_.bit[8]) return R7;
    return R7;
}

void register_add(lc3_register_names_t dr, lc3_register_names_t sr1,
        lc3_register_names_t sr2) {
    lc3_word_t wd_;
    word_add(&wd_, (lc3_word_t) read_register(sr1),
            (lc3_word_t) read_register(sr2));
    write_register(dr, wd_);
}

int lc3_evaluate_instruction() {

    lc3_instruction_fetch();

    lc3_word_t ir_ = (lc3_word_t) read_register(IR);
    lc3_word_t op_select_ = four_bit_decoder(ir_.bit[0], ir_.bit[1], ir_.bit[2], ir_.bit[3]);

    // BR
    if (op_select_.bit[15] == 1) {
        lc3_execute_BR();
    }
    // ADD+
    if (op_select_.bit[14] == 1) {
        lc3_execute_ADD();
    }
    // LD+
    if (op_select_.bit[13] == 1) {
        lc3_execute_LD();
    }
    // ST
    if (op_select_.bit[12] == 1) {
        lc3_execute_ST();
    }
    // JSR/JSRR
    if (op_select_.bit[11] == 1) {
        lc3_execute_JSR();
    }
    // AND+
    if (op_select_.bit[10] == 1) {
        lc3_execute_AND();
    }
    // LDR+
    if (op_select_.bit[9] == 1) {
        lc3_execute_LDR();
    }
    // STR
    if (op_select_.bit[8] == 1) {
        lc3_execute_STR();
    }
    // RTI (unsupported)
    if (op_select_.bit[7] == 1) {
        dump_error(__func__, "Unsupported instruction (RTI)... aborting");
        return -1;
    }
    // NOT+
    if (op_select_.bit[6] == 1) {
        lc3_execute_NOT();
    }
    // LDI+
    if (op_select_.bit[5] == 1) {
        lc3_execute_LDI();
    }
    // STI
    if (op_select_.bit[4] == 1) {
        lc3_execute_STI();
    }
    // JMP/RET
    if (op_select_.bit[3] == 1) {
        lc3_execute_JMP();
    }
    // RESERVED
    if (op_select_.bit[2] == 1) {
        dump_error(__func__, "Cannot execute reserved instruction... aborting");
        return -1;
    }
    // LEA+
    if (op_select_.bit[1] == 1) {
        lc3_execute_LEA();
    }
    // TRAP (used only to halt the program)
    if (op_select_.bit[0] == 1) {
        lc3_word_t ir_ = (lc3_word_t) read_register(IR);
        if (ir_.bit[15] && ir_.bit[13] && ir_.bit[10]) // x25
            return LC3_SIM_HALT;
        // unsupported modes
        dump_error(__func__, "Unsupported modes in TRAP instruction... aborting");
        return -1;
    }
    return 0;
}

void lc3_execute_ADD() {
#ifdef DEBUG_L2
    info(__func__, "Executing ADD");
#endif
    lc3_word_t ir_ = (lc3_word_t) read_register(IR);
    lc3_register_names_t dr_ = gpr_select(ir_.bit[4], ir_.bit[5], ir_.bit[6]);
    lc3_register_names_t sr1_ = gpr_select(ir_.bit[7], ir_.bit[8], ir_.bit[9]);
    // no longer using one-bit select mux
    if (ir_.bit[10] == 0) { // register add
        lc3_register_names_t sr2_ = gpr_select(ir_.bit[13], ir_.bit[14], ir_.bit[15]);
        register_add(dr_, sr1_, sr2_);
    } else { // immediate add
        sign_extend_imm5();
        register_add(dr_, sr1_, SEXT);
    }
    setcc(dr_);
}

void lc3_execute_AND() {
#ifdef DEBUG_L2
    info(__func__, "Executing AND");
#endif
    lc3_word_t ir_ = (lc3_word_t) read_register(IR);
    lc3_register_names_t dr_ = gpr_select(ir_.bit[4], ir_.bit[5], ir_.bit[6]);
    lc3_register_names_t sr1_ = gpr_select(ir_.bit[7], ir_.bit[8], ir_.bit[9]);
    lc3_word_t dstw_;
    if (ir_.bit[10] == 0) { // register and
        lc3_register_names_t sr2_ = gpr_select(ir_.bit[13], ir_.bit[14], ir_.bit[15]);
        word_and(&dstw_, (lc3_word_t) read_register(sr1_),
                (lc3_word_t) read_register(sr2_));
        write_register(dr_, dstw_);
    } else { // immediate and
        sign_extend_imm5();
        word_and(&dstw_, (lc3_word_t) read_register(sr1_),
                (lc3_word_t) read_register(SEXT));
        write_register(dr_, dstw_);
    }
    setcc(dr_);
}

void lc3_execute_NOT() {
#ifdef DEBUG_L2
    info(__func__, "Executing NOT");
#endif
    lc3_word_t ir_ = (lc3_word_t) read_register(IR);
    lc3_register_names_t dr_ = gpr_select(ir_.bit[4], ir_.bit[5], ir_.bit[6]);
    lc3_register_names_t sr_ = gpr_select(ir_.bit[7], ir_.bit[8], ir_.bit[9]);
    lc3_word_t dstw_;
    word_not(&dstw_, (lc3_word_t) read_register(sr_));
    write_register(dr_, dstw_);
    setcc(dr_);
}

void lc3_execute_LDR() {
#ifdef DEBUG_L2
    info(__func__, "Executing LDR");
#endif
    lc3_word_t ir_ = (lc3_word_t) read_register(IR);
    lc3_register_names_t dr_ = gpr_select(ir_.bit[4], ir_.bit[5], ir_.bit[6]);
    lc3_register_names_t base_ = gpr_select(ir_.bit[7], ir_.bit[8], ir_.bit[9]);
    sign_extend_offset6();
    lc3_word_t mem_loc;
    word_add(&mem_loc, (lc3_word_t) read_register(base_), (lc3_word_t) read_register(SEXT));
    write_register(MAR, mem_loc);
    read_memory();
    lc3_word_t result = read_register(MDR);
    write_register(dr_, result);
    setcc(dr_);
}

void lc3_execute_STR() {
#ifdef DEBUG_L2
    info(__func__, "Executing STR");
#endif
    lc3_word_t ir_ = (lc3_word_t) read_register(IR);
    lc3_register_names_t sr_ = gpr_select(ir_.bit[4], ir_.bit[5], ir_.bit[6]);
    lc3_register_names_t base_ = gpr_select(ir_.bit[7], ir_.bit[8], ir_.bit[9]);
    sign_extend_offset6();
    lc3_word_t result = (lc3_word_t) read_register(sr_);
    write_register(MDR, result);
    lc3_word_t mem_loc;
    word_add(&mem_loc, (lc3_word_t) read_register(base_), (lc3_word_t) read_register(SEXT));
    write_register(MAR, mem_loc);
    write_memory();
}

void lc3_execute_LDI() {
#ifdef DEBUG_L2
    info(__func__, "Executing LDI");
#endif
     lc3_word_t ir_ = (lc3_word_t) read_register(IR);
     lc3_register_names_t dr_ = gpr_select(ir_.bit[4], ir_.bit[5], ir_.bit[6]);
     sign_extend_PCOffset9();
     lc3_word_t mem_loc;
     word_add(&mem_loc, (lc3_word_t) read_register(PC), (lc3_word_t) read_register(SEXT));
     write_register(MAR, mem_loc);
     read_memory();
     lc3_word_t mem_loc2 = (lc3_word_t) read_register(MDR);
     write_register(MAR, mem_loc2);
     read_memory();
     lc3_word_t result = (lc3_word_t) read_register(MDR);
     write_register(dr_, result);
     setcc(dr_);
}

void lc3_execute_STI() {
#ifdef DEBUG_L2
    info(__func__, "Executing STI");
#endif
     lc3_word_t ir_ = (lc3_word_t) read_register(IR);
     lc3_register_names_t sr_ = gpr_select(ir_.bit[4], ir_.bit[5], ir_.bit[6]);
     sign_extend_PCOffset9();
     lc3_word_t mem_loc;
     word_add(&mem_loc, (lc3_word_t) read_register(PC), (lc3_word_t) read_register(SEXT));
     write_register(MAR, mem_loc);
     read_memory();
     write_register(MAR, (lc3_word_t) read_register(MDR));
     write_register(MDR, (lc3_word_t) read_register(sr_));
     write_memory();
}

void lc3_execute_JMP() {
#ifdef DEBUG_L2
    info(__func__, "Executing JMP");
#endif
    lc3_word_t ir_ = (lc3_word_t) read_register(IR);
    lc3_register_names_t jmp_loc = gpr_select(ir_.bit[7], ir_.bit[8], ir_.bit[9]);
    write_register(PC, (lc3_word_t) read_register(jmp_loc));


}

void lc3_execute_LEA() {
#ifdef DEBUG_L2
    info(__func__, "Executing LEA");
#endif
    lc3_word_t ir_ = (lc3_word_t) read_register(IR);
    lc3_register_names_t dr_ = gpr_select(ir_.bit[4], ir_.bit[5], ir_.bit[6]);
    sign_extend_PCOffset9();
    lc3_word_t mem_loc;
    word_add(&mem_loc, (lc3_word_t) read_register(PC), (lc3_word_t) read_register(SEXT));
    write_register(dr_, mem_loc);
    setcc(dr_);
}

void sign_extend_offset6() {
	lc3_word_t ir_ = (lc3_word_t) read_register(IR);
	if(ir_.bit[10] == 0){
		ir_.bit[9] = 0;
		ir_.bit[8] = 0;
		ir_.bit[7] = 0;
		ir_.bit[6] = 0;
		ir_.bit[5] = 0;
		ir_.bit[4] = 0;
		ir_.bit[3] = 0;
		ir_.bit[2] = 0;
		ir_.bit[1] = 0;
		ir_.bit[0] = 0;
	}
	else{
		ir_.bit[9] = 1;
		ir_.bit[8] = 1;
		ir_.bit[7] = 1;
		ir_.bit[6] = 1;
		ir_.bit[5] = 1;
		ir_.bit[4] = 1;
		ir_.bit[3] = 1;
		ir_.bit[2] = 1;
		ir_.bit[1] = 1;
		ir_.bit[0] = 1;
	}
	write_register(SEXT, ir_);
}

void sign_extend_PCOffset9() {
	lc3_word_t ir_ = (lc3_word_t) read_register(IR);
	if(ir_.bit[7] == 0){
		ir_.bit[6] = 0;
		ir_.bit[5] = 0;
		ir_.bit[4] = 0;
		ir_.bit[3] = 0;
		ir_.bit[2] = 0;
		ir_.bit[1] = 0;
		ir_.bit[0] = 0;
	}
	else{
		ir_.bit[6] = 1;
		ir_.bit[5] = 1;
		ir_.bit[4] = 1;
		ir_.bit[3] = 1;
		ir_.bit[2] = 1;
		ir_.bit[1] = 1;
		ir_.bit[0] = 1;
	}
	write_register(SEXT, ir_);
}

void sign_extend_PCOffset11() {
	lc3_word_t ir_ = (lc3_word_t) read_register(IR);
	if(ir_.bit[5] == 0){
		ir_.bit[4] = 0;
		ir_.bit[3] = 0;
		ir_.bit[2] = 0;
		ir_.bit[1] = 0;
		ir_.bit[0] = 0;
	}
	else{
		ir_.bit[4] = 1;
		ir_.bit[3] = 1;
		ir_.bit[2] = 1;
		ir_.bit[1] = 1;
		ir_.bit[0] = 1;
	}
	write_register(SEXT, ir_);
}

void increment_pc() {
    lc3_word_t pc_ = read_register(PC);
    lc3_word_t one_ = {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}};
    lc3_word_t next_pc;
    word_add(&next_pc, pc_, one_);
    write_register(PC, next_pc);

}

void lc3_instruction_fetch() {
    lc3_word_t PC_ = (lc3_word_t) read_register(PC);
    write_register(MAR, PC_);
    read_memory();
    lc3_word_t ir_ = read_register(MDR);
    write_register(IR, ir_);
    increment_pc();
}

void setcc(lc3_register_names_t reg) {
	reset_status_code(CODE_N);
	reset_status_code(CODE_Z);
	reset_status_code(CODE_P);
	lc3_word_t test = (lc3_word_t) read_register(reg);
	if(test.bit[0] == 1){
		set_status_code(CODE_N);
	}
	else if (test.bit[1] == 0 && test.bit[2] == 0 && test.bit[3] == 0 && test.bit[4] == 0 && test.bit[5] == 0 && test.bit[6] == 0 && test.bit[7] == 0 && test.bit[8] == 0 && test.bit[9] == 0 && test.bit[10] == 0 && test.bit[11] == 0 && test.bit[12] == 0 && test.bit[13] == 0 && test.bit[14] == 0 && test.bit[15] == 0){
		set_status_code(CODE_Z);
	}
	else{
		set_status_code(CODE_P);
	}
}

void lc3_execute_BR() {
#ifdef DEBUG_L2
    info(__func__, "Executing BR");
#endif
    lc3_word_t ir_ = (lc3_word_t) read_register(IR);
    if((ir_.bit[4] == 1 && test_status_code(CODE_N)) || (ir_.bit[5] == 1 && test_status_code(CODE_Z)) || (ir_.bit[6] == 1 && test_status_code(CODE_P))){
    	lc3_word_t pc_ = (lc3_word_t) read_register(PC);
        sign_extend_PCOffset9();
        lc3_word_t new_pc;
        word_add(&new_pc, pc_, (lc3_word_t) read_register(SEXT));
        write_register(PC, new_pc);
    }
}

void lc3_execute_LD() {
#ifdef DEBUG_L2
    info(__func__, "Executing LD");
#endif
     lc3_word_t ir_ = (lc3_word_t) read_register(IR);
     lc3_register_names_t dr_ = gpr_select(ir_.bit[4], ir_.bit[5], ir_.bit[6]);
     sign_extend_PCOffset9();
     lc3_word_t mem_loc;
     word_add(&mem_loc, (lc3_word_t) read_register(PC), (lc3_word_t) read_register(SEXT));
     write_register(MAR, mem_loc);
     read_memory();
     lc3_word_t result = (lc3_word_t) read_register(MDR);
     write_register(dr_, result);
     setcc(dr_);
}

void lc3_execute_ST() {
#ifdef DEBUG_L2
    info(__func__, "Executing ST");
#endif
     lc3_word_t ir_ = (lc3_word_t) read_register(IR);
     lc3_register_names_t sr_ = gpr_select(ir_.bit[4], ir_.bit[5], ir_.bit[6]);
     sign_extend_PCOffset9();
     lc3_word_t mem_loc;
     word_add(&mem_loc, (lc3_word_t) read_register(PC), (lc3_word_t) read_register(SEXT));
     write_register(MAR, mem_loc);
     write_register(MDR, (lc3_word_t) read_register(sr_));
     write_memory();
}

void lc3_execute_JSR() {
    lc3_word_t ir_ = (lc3_word_t) read_register(IR);
    write_register(R7, (lc3_word_t) read_register(PC));

    if (ir_.bit[4] == 0) { // JSRR
#ifdef DEBUG_L2
        info(__func__, "Executing JSR");
#endif
    lc3_register_names_t new_pc = gpr_select(ir_.bit[7], ir_.bit[8], ir_.bit[9]);
    write_register(PC, (lc3_word_t) read_register(new_pc));
    } else {
#ifdef DEBUG_L2
        info(__func__, "Executing JSRR");
#endif
        sign_extend_PCOffset11();
        lc3_word_t new_pc;
        word_add(&new_pc, (lc3_word_t) read_register(PC), (lc3_word_t) read_register(SEXT));
        write_register(PC, new_pc);
    }
}
