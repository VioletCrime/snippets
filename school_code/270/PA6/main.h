#ifndef CS270_PA6_MAIN_H_
#define CS270_PA6_MAIN_H_

#include <stdio.h>
#include "logic.h"
#include "lc3sim.h"
#include "lc3registers.h"
#include "infilesparser.h"
#include "helper.h"
#include "memorylayout.h"
#include "lc3machinetypes.h"

typedef struct address2labelname__ {
  uint16_t  numeric_address;
  char      *label_name;
} address2labelname_t;
#define MAX_ADDRESS2LABELNAME_SIZE    1024

/**
 * Print a short usage message
 */
void usage(int exitvalue);

/**
 * Load the assembly program inside the object file into LC-3 memory and set
 * program counter to the origin address of the program.
 */
int simcmd_load_lc3_program(char *lc3obj_file, memory_manager_obj_t *mmo,
                            address2labelname_t *a2l, size_t *a2l_size);

/**
 * Shows the value of a label and prints the contents.
 * The parameter string_address could be a label or a memory location in hex
 */
int simcmd_translate_address(char *string_address, memory_manager_obj_t *mmo,
                             address2labelname_t *a2l, size_t a2l_size);

/**
 * Sets the contents of memory address ("string_address) to the specified "word"
 * The parameter string_address could be a label or a memory location in hex
 */
int simcmd_set_memory(char *string_address, char* string_word,
                      memory_manager_obj_t *mmo, address2labelname_t *a2l,
                      size_t a2l_size);

/**
 * Sets the value of a register (specified by "register_name") to "string_word"
 * The parameter string_word could be a decimal or a hexadecimal number
 */
int simcmd_set_register(char *register_name, char *string_word);


/**
 * Prints the contents of all registers (including some internal registers)
 */
void simcmd_print_registers();

#endif
