#ifndef CS270_PA6_LOGIC_H_
#define CS270_PA6_LOGIC_H_

#include "lc3machinetypes.h"

/**
 * Description:
 *    Takes in an lc3_word and returns the bitwise negation of that word
 * Args:
 *    pointer to lc3_word - the negated value is stored here
 *    lc3_word            - the value to be negated
 * Output:
 *    The negated value is returned in the first argument
 **/
void word_not(lc3_word_t *R, lc3_word_t A);

/**
 * Description:
 *    Takes two lc3_words and returns the bitwise AND of these words
 **/
void word_and(lc3_word_t *R, lc3_word_t A, lc3_word_t B);

/**
 * Description:
 *    Takes two lc3_words and returns the 2's-complement sum of the words.
 *    Ignore the carry out BIT
 **/
void word_add(lc3_word_t *R, lc3_word_t A, lc3_word_t B);

/**
 * Description:
 *    Takes four input bits, decodes them and returns an lc3_word
 *    (i.e., BITs b0 through b15). 
 **/
lc3_word_t four_bit_decoder(BIT b3, BIT b2, BIT b1, BIT b0);

/**
 * Description:
 *    Takes three input bits, decodes them and returns an lc3_word
 *    (i.e., BITs b0 through b7). Note that the unused bits in the
 *    resulting word are always set to "ZERO"s
 **/
lc3_word_t three_bit_decoder(BIT b2, BIT b1, BIT b0);

#endif
