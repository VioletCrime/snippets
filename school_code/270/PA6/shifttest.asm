;; Main routine
                      .ORIG x3000
MAIN                  ;; setup stack
                      LD    R6, STACKBASE       ; set the stack base
                      ADD   R5, R6, #0          ; set main's frame pointer

                      LD    R0, TOSHIFT         ;
                      LD    R1, AMOUNT          ;

                      ;; push parameters
                      ADD   R6, R6, #-1         ;
                      STR   R1, R6, #0          ; push param 2
                      ADD   R6, R6, #-1         ;
                      STR   R0, R6, #0          ; push param 1
                      JSR   SUB_RightShift      ; call subroutine
                      LDR   R0, R6, #0          ; pop return value
                      ADD   R6, R6, #1          ;

                      ST    R0, SHIFTED         ;

                      ;; unwind stack
                      ADD   R6, R6, #2          ;
                      HALT

;;
;;  Subroutines
;;

;; subroutine: SUBRightShift
;;  desc: Right shift the bits of an input word by the specified number of bits
;;  params:
;;      param 1 (word):   The word to be right shifted
;;      param 2 (amount): The amount to shift
;;  return:
;;      (shifted word):   The right shifted word
                      ;; load input values
SUB_RightShift         
                      ;; setup stack frame
                      ADD   R6, R6, #-1         ; allocate return value
                      ADD   R6, R6, #-1         ;
                      STR   R7, R6, #0          ; push caller's return address (R7)
                      ADD   R6, R6, #-1         ;
                      STR   R5, R6, #0          ; push caller's frame pointer (R5)
                      ADD   R5, R6, #-1         ; set current frame pointer FP = SP - 1

                      ;; get param 1
                      LDR   R0, R5, #4         ; get word to be shifted (note: word has to be > 0)
                      BRz   RightShift_DONE    ; if ( word == 0 ) shifted_word = word
                      ;; get param 2
                      LDR   R1, R5, #5         ; no. of times to right shift input word
                      BRz   RightShift_DONE    ; if ( amount == 0 ) shifted_word = word

                      ;; compute divisor
                      AND   R2, R2, #0          ; divisor (for left shift) will be computed in R2
                      ADD   R2, R2, #1          ;
RightShift_LS_LOOP    ADD   R2, R2, R2          ; r2 <- 2^(amount)
                      BRn   RightShift_ZERO     ; if at any time (divisor < 0) then return 0
                      ADD   R1, R1, #-1         ;
                      BRp   RightShift_LS_LOOP  ;
                      NOT   R2, R2              ;
                      ADD   R2, R2, #1          ; r2 <- -(r2)

                      ;; compute result
                      AND   R3, R3, #0          ; result will be computed in R3
RightShift_RS_LOOP    ADD   R3, R3, #1          ; increment result
                      ADD   R0, R0, R2          ; subtract divisor from word
                      BRzp  RightShift_RS_LOOP  ;
                      ADD   R3, R3, #-1         ; subtract 1 from result
                      STR   R3, R5, #3          ; store result in return location
                      BR    RightShift_RETURN   ;

RightShift_DONE       STR   R0, R5, #3          ;  if (word==0)||(amount==0) word_shifted = word
                      BR    RightShift_RETURN   ;

RightShift_ZERO       AND   R0, R0, #0          ;
                      STR   R0, R5, #3          ; if amount > 15, return 0

RightShift_RETURN     ;; unwind stack
                      ADD   R6, R5, #1          ; reset stack pointer (SP = FP + 1)
                      LDR   R5, R6, #0          ; pop caller's frame pointer
                      ADD   R6, R6, #1          ;
                      LDR   R7, R6, #0          ; pop caller's return address
                      ADD   R6, R6, #1          ;
                      RET

;;
;;  Input/Output and Globals
;;
STACKBASE             .FILL     x4FFF
TOSHIFT               .BLKW     1
AMOUNT                .BLKW     1
SHIFTED               .BLKW     1

                      .END
