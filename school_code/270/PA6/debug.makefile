CC = gcc
LD = gcc

CFLAGS = -g -Wall -std=gnu99 -c -DDEBUG_L0 -DDEBUG_L1 -DDEBUG_L2

OBJS = logic.o lc3sim.o infilesparser.o main.o
LIBS = lc3memlib.a

TARGET = lc3sim-lite

default: $(TARGET)

$(TARGET): $(OBJS)
	$(LD) -g $(OBJS) $(LIBS) -o $(TARGET)

logic.o: logic.c logic.h lc3machinetypes.h
	$(CC) $(CFLAGS) logic.c

lc3sim.o: lc3sim.c lc3sim.h logic.c logic.h helper.h \
    	  lc3machinetypes.h lc3registers.h memorylayout.h
	$(CC) $(CFLAGS) lc3sim.c

infilesparser.o: infilesparser.c infilesparser.h lc3machinetypes.h \
		 helper.h memorylayout.h
	$(CC) $(CFLAGS) infilesparser.c

main.o: main.c main.h lc3sim.c lc3sim.h logic.c logic.h helper.h \
    	lc3machinetypes.h lc3registers.h memorylayout.h infilesparser.h
	$(CC) $(CFLAGS) main.c

clean:
	rm -f *.o

distclean:
	rm -f *.o $(TARGET)

pack:
	cd ../; tar -czf PA6.tar.gz PA6;
