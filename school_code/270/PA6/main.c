/**
 * LC Simulator Lite version
 */
#include "main.h"

// global memory manager object
memory_manager_obj_t  memory_manager;
/**
 * (helper routine) Get the LC-3 location object associated with a
 * memory address
 */
static
memory_location_t __get_location(memory_manager_obj_t *mmo,
                                lc3_address_t address);

int main(int argc, char *argv[]) {
  if (argc < 2) {
    usage(1);
  }

  char  *commands_file = argv[1];
  FILE  *cmdsfile_fp;
  if ((cmdsfile_fp = fopen(commands_file, "r")) == NULL) {
    dump_error(__func__, "Unable to open commands file %s", commands_file);
    exit(1);
  }
  // initialize the memory manager
  mm_init(&memory_manager);
  // address to label name mapping
  size_t              address2label_size = 0;
  address2labelname_t *address2label = (address2labelname_t*)
    malloc(sizeof(address2labelname_t)*MAX_ADDRESS2LABELNAME_SIZE);

  int                 rv = 0;
  char                cmdargs[MAX_TOKENS_PER_LINE][MAX_TOKEN_SIZE];
  sim_commands_t      command;
  // read commands from input file and execute them
  while ((rv = simulator_command_getnext(cmdsfile_fp, &command, cmdargs)) == 0)
  {
    switch (command) {
      // these have one arg
      case SIMCMD_FILE:
        if (simcmd_load_lc3_program(cmdargs[0], &memory_manager,
                                    address2label, &address2label_size) < 0)
        {
          dump_error(__func__, "Unable to initialize program... aborting");
          fclose(cmdsfile_fp);
          free(address2label); // <- dint assign any labels
          exit(1);
        }
        break;
      case SIMCMD_TRANSLATE:
        simcmd_translate_address(cmdargs[0], &memory_manager,
                                 address2label, address2label_size);
        break;
      // these have two args
      case SIMCMD_MEMORY:
        simcmd_set_memory(cmdargs[0], cmdargs[1], &memory_manager,
                          address2label, address2label_size);
        break;
      case SIMCMD_REGISTER:
        simcmd_set_register(cmdargs[0], cmdargs[1]);
        break;
        // these have no args
      case SIMCMD_CONTINUE:
        while ((rv=lc3_evaluate_instruction()) != LC3_SIM_HALT) {
          if (rv < 0) {
            dump_error(__func__,
                "Errors encountered during program execution... stopping");
            fclose(cmdsfile_fp);
            free(address2label);
            mm_clear(&memory_manager); // <- labels are freed here
            exit(1);
          }
        }
#ifdef DEBUG_L2
        info(__func__, "Done executing program");
#endif
        break;
      case SIMCMD_QUIT:
        {
          // dump memory on quit
          memory_layout_t     *iterator = NULL;
          do {
            mm_next_location(&memory_manager, &iterator);
            if (iterator)
              print_location(iterator->location);
          } while (iterator != NULL);
          fclose(cmdsfile_fp);
          exit(0);
        }
      case SIMCMD_PRINTREGS:
        simcmd_print_registers();
        break;
      default:
        info(__func__, "Unrecognized command... ignoring");
    }
  }
  fclose(cmdsfile_fp);
  exit(0);
}


int simcmd_load_lc3_program(char *lc3obj_file, memory_manager_obj_t *mmo,
                            address2labelname_t *a2l, size_t *a2l_size)
{
  int rv = 0;
  if ((rv = validate_object_filename(lc3obj_file)) != 0)
    return rv;

  // step1. read labels from symbol table file
  char  symtable_file[MAX_FILENAME_SIZE];
  FILE  *symfile_fp;
  if ((rv = generate_symbol_table_filename(lc3obj_file, symtable_file)) != 0)
    return rv;

  if ((symfile_fp = fopen(symtable_file, "r")) == NULL) {
    dump_error(__func__, "Unable to open symbol table file %s", symtable_file);
    return -20;
  }
  char  strlabel2addr[2][MAX_TOKEN_SIZE];
  int   next_label = 0;
#ifdef DEBUG_L1
  info(__func__, "Reading symbols from symbol table file %s", symtable_file);
#endif
  while ((rv = symbol_table_getnext(symfile_fp, strlabel2addr)) != EOF) {
    if (rv < 0) {
      dump_error(__func__, "Unable to parse symbol table file");
      fclose(symfile_fp);
      return rv;
    }
    // convert hexadecimal string to short value
    a2l[next_label].numeric_address = 
      (uint16_t)strtol(strlabel2addr[1], NULL, 16);
    if (a2l[next_label].numeric_address == 0) {
      dump_error(__func__, "Unable to convert hex string %s to numeric value",
                 strlabel2addr[1]);
      fclose(symfile_fp);
      return -21;
    }
    a2l[next_label].label_name = strdup(strlabel2addr[0]);
#ifdef DEBUG_L1
    info(__func__, "Got symbol %s associated with address %#.4x",
         a2l[next_label].label_name, a2l[next_label].numeric_address);
#endif
    next_label++;
    // out of space for labels
    assert(next_label <= MAX_ADDRESS2LABELNAME_SIZE);
  }
  *a2l_size = (size_t)next_label;
  fclose(symfile_fp);

#ifdef DEBUG_L1
  info(__func__, "Read %d symbols", next_label);
#endif

  // step2. get origin address from object file and initialize program counter
  FILE  *objfile_fp = NULL;
  if ((objfile_fp = fopen(lc3obj_file, "r")) == NULL) {
    dump_error(__func__, "Unable to open lc3 object file %s", lc3obj_file);
    return -22;
  }
#ifdef DEBUG_L1
  info(__func__, "Reading object code from object file %s", lc3obj_file);
#endif
  lc3_word_t  instruction;
  objfile_instruction_getnext(objfile_fp, &instruction);
  write_register(PC, instruction);

  // step3. load program into memory and assign labels to memory locations
  next_label = 0;
  uint16_t          loader_pc_  = convert_lc3word2numeric(instruction);
#ifdef DEBUG_L1
  info(__func__, ".ORIGIN at %#.4x", loader_pc_);
#endif
  memory_location_t location_;
  while ((rv = objfile_instruction_getnext(objfile_fp, &instruction)) != EOF) {
    if (rv < 0) {
      dump_error(__func__, "Unable to parse object file");
      fclose(objfile_fp);
      return rv;
    }
    location_.address = (lc3_address_t)convert_numeric2lc3word(loader_pc_);
    location_.numeric_address = loader_pc_;
    location_.word    = instruction;
    location_.numeric_word    = convert_lc3word2numeric(instruction);
    if (a2l[next_label].numeric_address == loader_pc_) {
      location_.label = a2l[next_label].label_name;
      next_label++;
    }
    else {
      location_.label = NULL;
    }
    if (a2l[next_label].label_name != NULL)
      assert(a2l[next_label].numeric_address > loader_pc_);
#ifdef DEBUG_L1
    if (location_.label)
      info(__func__, "Loading %#.4x into %#.4x (%s)", location_.numeric_word,
           location_.numeric_address, location_.label);
    else
      info(__func__, "Loading %#.4x into %#.4x",
         location_.numeric_word, location_.numeric_address);
#endif
    if ((rv = mm_sorted_insert(mmo, location_) < 0)) {
      fclose(objfile_fp);
      return -1;
    }
    loader_pc_++;
  }
  fclose(objfile_fp);
  return 0;
}


int simcmd_translate_address(char *string_address, memory_manager_obj_t *mmo,
                             address2labelname_t *a2l, size_t a2l_size)
{
  lc3_address_t address;
  uint16_t      numeric_address;
  if (   (   (string_address[0] == '0')
          && ((string_address[1] == 'x') || (string_address[1] == 'X')))
      || ((string_address[0] == 'x') || (string_address[0] == 'X'))) {
    // string_address is a hex value
    numeric_address = (uint16_t)strtol(string_address, NULL, 0);
    address         = (lc3_address_t)convert_numeric2lc3word(numeric_address);
  }
  else {
    // string_address is a label
    int i = 0;
    for (i = 0; i < a2l_size; i++)
      if (strncmp(a2l[i].label_name, string_address, MAX_LABEL_SIZE) == 0) {
        numeric_address   = a2l[i].numeric_address;
        address     = (lc3_address_t)convert_numeric2lc3word(numeric_address);
        break;
      }
    if (i == a2l_size) {
      dump_error(__func__, "Label not found");
      return -1;
    }
  }
  memory_location_t location_     = __get_location(mmo, address);
  if (location_.label)
    printf("Address %#.4x (%s) has value %#.4x\n", numeric_address,
           location_.label, location_.numeric_word);
  else
    printf("Address %#.4x has value %#.4x\n", numeric_address, 
           location_.numeric_word);
  return 0;
}


int simcmd_set_memory(char *string_address, char *string_word,
                      memory_manager_obj_t *mmo, address2labelname_t *a2l,
                      size_t a2l_size)
{
  lc3_address_t address;
  uint16_t      numeric_address;
  char          *saved_label = NULL;
  if (  (string_address[0] == '0')
      &&((string_address[1] == 'x') || (string_address[1] == 'X'))) {
    // string_address is a hex value
    numeric_address = (uint16_t)strtol(string_address, NULL, 16);
    address         = (lc3_address_t)convert_numeric2lc3word(numeric_address);
  }
  else {
    // string_address is a label
    int i = 0;
    for (i = 0; i < a2l_size; i++)
      if (strncmp(a2l[i].label_name, string_address, MAX_LABEL_SIZE) == 0) {
        numeric_address   = a2l[i].numeric_address;
        address     = (lc3_address_t)convert_numeric2lc3word(numeric_address);
        saved_label = a2l[i].label_name;
        break;
      }
    if (i == a2l_size) {
      dump_error(__func__, "Label not found");
      return -1;
    }
  }
  int           rv            = 0;
  uint16_t      numeric_word  = (uint16_t)strtol(string_word, NULL, 0);
  lc3_word_t    word          = convert_numeric2lc3word(numeric_word);
  memory_location_t location_;
  location_.word      = word;
  location_.address   = address;
  if (saved_label)
    location_.label   = saved_label;
  if ((rv = mm_sorted_insert(mmo, location_) < 0)) {
    return -1;
  }
  else 
  if (saved_label)
    printf("Wrote %#.4x to %#.4x (%s)\n", numeric_word, numeric_address,
           saved_label);
  else
    printf("Wrote %#.4x to %#.4x\n", numeric_word, numeric_address);
  return 0;
}


int simcmd_set_register(char *register_name, char *string_word) {
  uint16_t      numeric_word  = (uint16_t)strtol(string_word, NULL, 0);
  lc3_word_t    word          = convert_numeric2lc3word(numeric_word);
#ifdef DEBUG_L0
  info(__func__, "Attempting to write %#.4x into %s", numeric_word,
       register_name);
#endif
  if (strcmp(register_name, "R0") == 0)
    write_register(R0, word);
  else if (strcmp(register_name, "R1") == 0)
    write_register(R1, word);
  else if (strcmp(register_name, "R2") == 0)
    write_register(R2, word);
  else if (strcmp(register_name, "R3") == 0)
    write_register(R3, word);
  else if (strcmp(register_name, "R4") == 0)
    write_register(R4, word);
  else if (strcmp(register_name, "R5") == 0)
    write_register(R5, word);
  else if (strcmp(register_name, "R6") == 0)
    write_register(R6, word);
  else if (strcmp(register_name, "R7") == 0)
    write_register(R7, word);
  else {
    dump_error(__func__, "Unknown register %s", register_name);
    return -1;
  }
  printf("Wrote %#.4x into %s\n", numeric_word, register_name);
  return 0;
}


void simcmd_print_registers() {
  uint16_t value =0;
  value = convert_lc3word2numeric((lc3_word_t)read_register(IR));
  printf("Register IR   has %#.4x\n", value);

  value = convert_lc3word2numeric((lc3_word_t)read_register(PC));
  printf("Register PC   has %#.4x\n", value);

  value = convert_lc3word2numeric((lc3_word_t)read_register(MAR));
  printf("Register MAR  has %#.4x\n", value);

  value = convert_lc3word2numeric((lc3_word_t)read_register(MDR));
  printf("Register MDR  has %#.4x\n", value);

  value = convert_lc3word2numeric((lc3_word_t)read_register(PSR));
  printf("Register PSR  has %#.4x\n", value);

  value = convert_lc3word2numeric((lc3_word_t)read_register(SEXT));
  printf("Register SEXT has %#.4x\n", value);

  lc3_register_names_t reg;
  int j = 0;
  for (reg = R0, j=0; reg < R7; reg++, j++) {
    value = convert_lc3word2numeric((lc3_word_t)read_register(reg));
    printf("Register R%d   has %#.4x\n", j, value);
  }
}


static
memory_location_t __get_location(memory_manager_obj_t *mmo,
                                lc3_address_t address)
{
  memory_location_t mem;
  mem.address = address;
  mem.numeric_address = convert_lc3word2numeric((lc3_word_t)address);
  int rv = 0;
#ifdef DEBUG_L0
  info(__func__, "Attempting to lookup %#.4x", mem.numeric_address);
#endif
  if ((rv = mm_lookup_word(mmo, &mem)) < 0) {
    if (rv == -1)
      dump_error(__func__, "Lookup from %#.4x failed : read before write",
                 mem.numeric_address);
    else
      dump_error(__func__, "Lookup from %#.4x failed", mem.numeric_address);
    lc3_word_t  nullwd = {{0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}};
    mem.word  = nullwd;
    mem.numeric_word = 0;
  }
#ifdef DEBUG_L0
  info(__func__, "Read %#.4x from %#.4x", mem.numeric_word,
         mem.numeric_address);
#endif
  return mem;
}


void usage(int exitvalue) {
  printf("Usage: lc3sim-lite \"lc3sim commands file\" \n");
  printf("  The commands file must contain a \"file ...obj\" command as the\n");
  printf("  first line, and the symbol table file associated with the .obj\n");
  printf("  must be in the same directory as the .obj file.\n");
  exit(exitvalue);
}

