#ifndef CS270_PA6_HELPER_H_
#define CS270_PA6_HELPER_H_

#include "lc3machinetypes.h"
#include <stdarg.h>
#include <stdio.h>


/**
 * Converts an LC-3 word into a numeric word
 */
uint16_t convert_lc3word2numeric(lc3_word_t w);

/**
 * Converts a numeric word to an LC-3 word
 */
lc3_word_t convert_numeric2lc3word(uint16_t numeric);

/**
 * Convert an LC-3 word into a string
 */
#define LC3_WORD_STRING_SIZE   16
char* convert_lc3word2string(lc3_word_t w, char *ws);

/**
 * Print an error message
 *
 * The first argument is the function name (typically one can get the function
 * name using the __func__ macro), the second argument is an error message
 * formatted in the form of a printf message with variable number of args.
 */
void dump_error(const char* function, const char* msg, ...);

/**
 * Print an information message
 *
 * The first argument is the function name (typically one can get the function
 * name using the __func__ macro), the second argument is an info message
 * formatted in the form of a printf message with variable number of args.
 */
void info(const char* function, const char* msg, ...);
#endif
