#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>

// Function: quadratic
// Description: implements the quadratic equation
// Parameters: int, int, int: coefficients, boolean: return first root if true, second root if false
// Return: One of the roots if true, the other root if false
// Error Avoid division by zero
float quadratic(int coeff1, int coeff2, int coeff3, bool returnFirstRoot);

// Program entry point
int main(){
  
  int a, b, c;
  float r1, r2;
  
  printf ("Quadratic Program\n");
  printf("Enter a: ");
  scanf("%d", &a);
  printf("Enter b: ");
  scanf("%d", &b);
  printf("Enter c: ");
  scanf("%d", &c);
  r1 = quadratic(a, b, c, true);
  r2 = quadratic(a, b, c, false);
  printf("Roots are %3.2f and %3.2f\n", r1, r2);
 
  return EXIT_SUCCESS;
}

float quadratic(int coeff1, int coeff2, int coeff3, bool returnFirstRoot){

  float rootValue = 0.0;

  if (coeff1 == 0){ // Avoid division by zero
    printf("Error: Division by 0 (a = 0)\n");
    exit(EXIT_FAILURE);
  }
  else{ // Implement quadratic equation
    if(returnFirstRoot)
      rootValue = (-coeff2+sqrt((coeff2*coeff2)-(4*coeff1*coeff3)))/(2*coeff1);
    else
      rootValue = (-coeff2-sqrt((coeff2*coeff2)-(4*coeff1*coeff3)))/(2*coeff1);
  }
  return rootValue;
}
