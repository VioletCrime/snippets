/**
 * LC3 Simulator Lite (PA2)
 */
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include "logic.h"

//function prototype
void print_lc3_word_contents(lc3_word_t w);

int main(int argc, char *argv[]) {

    lc3_word_t result;
    lc3_word_t a_value = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1}; // 13
    lc3_word_t b_value = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1}; // 3
    lc3_word_t c_value = {0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; // 1 (floating point)
    lc3_word_t d_value = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; // -2 (floating point)

for (int i = 0; i < 16; i++){
printf("a_value[%d] = %d\n", i, a_value[i]);
}

    // perform unit tests
    // test word_not()
    word_not(&result, &a_value);
    print_lc3_word_contents(result);

    // test word_and()
    //word_and(&result, &a_value, &b_value);
    //print_lc3_word_contents(result);

    // test word_add()
    //word_add(&result, &a_value, &b_value);
    //print_lc3_word_contents(result);

    // test word_flad()
    //word_flad(&result, &a_value, &b_value);
    //print_lc3_word_contents(result);

    // test 4 bit decoder
    //four_bit_decoder(&result, 1, 0, 0, 1);
    //print_lc3_word_contents(result);

    // test 3 bit decoder
    //three_bit_decoder(&result, 1, 0, 0);
    //print_lc3_word_contents(result);

    exit(0);
}

// do not modify

void print_lc3_word_contents(lc3_word_t w) {
    printf("%d %d %d %d  %d %d %d %d  %d %d %d %d  %d %d %d %d\n",
            w[15], w[14], w[13], w[12], w[11], w[10], w[9], w[8],
            w[7], w[6], w[5], w[4], w[3], w[2], w[1], w[0]);
}
