#ifndef CS270_PA2_LOGIC_H_
#define CS270_PA2_LOGIC_H_

#include <stdint.h>

/**
 * Basic element of the LC-3 machine.
 *
 * BIT takes on values ZER0 (0) or ONE (1)
 */
typedef enum {
    ZERO = (uint8_t) 0, ONE = (uint8_t) 1
} BIT;

/**
 * A 16-"BIT" LC-3 word in big-endian format
 */
typedef BIT lc3_word_t[16];

/**
 * Description:
 *  Takes two pointers to an LC-3 words A (i.e., A is of type lc3_word_t*)
 * and R, and writes the negated BITs of A into the LC-3 word pointed to by R.
 * Args:
 *  --> complete the rest
 **/
void word_not(lc3_word_t *R, lc3_word_t *A);

/**
 * Description:
 *  Takes three pointers to the LC-3 words A, B, and R, and writes the
 * bitwise AND of A and B into the LC-3 word pointed to by R.
 * Args:
 *  --> complete the rest
 **/
void word_and(lc3_word_t *R, lc3_word_t *A, lc3_word_t *B);

/**
 * Description:
 * Takes three pointers to the LC-3 words A, B, and R, and writes the bitwise
 * addition of these words into the LC-3 word pointed to by R.
 * A, B, and R are in 2’s complement form.
 * Args:
 *  --> complete the rest
 **/
void word_add(lc3_word_t *R, lc3_word_t *A, lc3_word_t *B);

/**
 * Description:
 * Takes three pointers to the LC-3 words A, B, and R, and writes the 
 * floating-point sum of these words into the LC-3 word pointed to by R.
 * A, B, and R are in 16-bit “half-precision” floating point form.
 * Args:
 *  --> complete the rest
 **/
void word_flad(lc3_word_t *R, lc3_word_t *A, lc3_word_t *B);

/**
 * Description:
 * Takes 4 BITs, decodes them (just like a logic decoder) and returns the
 * result as an int.
 * Args:
 *  --> complete the rest
 **/
int four_bit_decoder(BIT b0, BIT b1, BIT b2, BIT b3);

/**
 * Description:
 * Takes 3 BITs, decodes them and returns the result as an int.
 * Note that the unused bits (bits 8 through 15) in the resulting word
 * must be set to ZERO.
 * Args:
 *  --> complete the rest
 **/
// reuse the four_bit_decoder to implement the three bit decoder
int three_bit_decoder(BIT b0, BIT b1, BIT b2);

#endif
