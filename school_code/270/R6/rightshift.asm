;rightshift.asm 
;PA4 (LC3 Assembly Program 2) by Kyle Smith
;CS270 Fall '11
;Started 10/22/2011

		.ORIG x3000			;Let's get this thing started
;
MAIN	;=======================================================================================
;
;REGISTER USAGE
;R0: Unused	R1: Variable shifter
;R2: Unused	R3: Unused	R4: Unused
;R5: Unused	R6: Unused	R7: RET address
;
		LD 	R1, X			;First, load X into R1...
		ST 	R1, IEEE_EXT		;...then dump it into 'IEEE_EXT'
		JSR	IEEE_EXP2ACTUAL	;Call IEEE_EXP2ACTUAL
		LD	R1, ACTUAL_EXP	;Load the result, ACTUAL_EXP into R1
		ST	R1, A			;And store it for a later method call
		LD 	R1, Y			;Repeat the exact same process above for Y...
		ST	R1, IEEE_EXT
		JSR	IEEE_EXP2ACTUAL
		LD	R1, ACTUAL_EXP
		ST	R1, B			;...storing Y's result in B
;
		JSR	ABS_DIFFERENCE	;Input variables are already loaded; call ABS_DIFF
;
		LD	R1, R_ABS		;Grab the difference calclulated by ABS_DIFF...
		ST	R1, RS_AMOUNT		;...and store it in RS_AMOUNT for the RS method call(s)
;
		JSR	RIGHT_SHIFT_SD	;Call the right-shift function of our choosing
;
		HALT				;And that's all she wrote.
;
;
IEEE_EXP2ACTUAL	;========================================================================
;
;REGISTER USAGE
;R0: Unused	R1: IEEE_EXT -> ACTUAL_EXP
;R2: Unused	R3: Unused	R4: Unused
;R5: Unused	R6: Unused	R7: RET address
;
		LD	R1, IEEE_EXT		;Load input variable 'IEEE_EXT' into R1...
		ADD	R1, R1, #-15		;...subtract the exponent bias of 15...
		ST	R1, ACTUAL_EXP	;...and store it in the output variable 'ACTUAL_EXP'
		RET				;Now where was I...?
;
;
ABS_DIFFERENCE	;========================================================================
;
;REGISTER USAGE
;R0: Unused	R1: A		R2: B	-> -B
;R3: A+(-B)	R4: Unused	R5: TOSHIFT_X/Y -> RS_IN	
;R6: Unused	R7: RET address
;
		LD 	R1, A			;Load A into R1
		LD	R2, B			;Load B into R2
		NOT	R2, R2			;Invert R2...
		ADD	R2, R2, #1		;...and add 1: R2 = -B
		ADD	R3, R1, R2		;R1 + R2 (A+(-B)) -> R3
		BRn	INVERTAAA		;If the result is negative, call a method to invert
		ST	R3, R_ABS		;Otherwise, just store the result in 'R_ABS'
		LD 	R5, TOSHIFT_Y		;Load TOSHIFT_Y (The value with the smaller exponent) into R5...
		ST	R5, RS_IN		;...and put that into RS_* method input variable RS_IN
		RET				;Pass the buck to another method from here on.
;
INVERTAAA	
		NOT	R3, R3			;Invert the result...
		ADD	R3, R3, #1		;...add the single offset...
		ST	R3, R_ABS		;...store in output variable 'R_ABS'...
		LD 	R5, TOSHIFT_X		;Load TOSHIFT_X (The value with the smaller exponent) into R5...
		ST	R5, RS_IN		;...and put that into RS_* method input variable RS_IN
		RET				;...and bugger off for a pint.
;
;
RIGHT_SHIFT_SD	;========================================================================
;
;REGISTER USAGE
;R0: Loop Counter		R1: RS_AMOUNT
;R2: RS_IN -> R_SHIFTED		R3: Unused		R4: Unused
;R5: Unused			R6: Unused		R7: RET address
;
		AND	R0, R0, #0		;Set R0 = 0
		LD	R2, RS_IN		;Load RS_IN into R2...
		ST	R2, R_SHIFTED		;...and place it in the output variable R_SHIFTED
		LD	R1, RS_AMOUNT		;Load RS_AMOUNT into R1
		BRp	OUTER			;If we actually need to shift, head over to the OUTER loop
		RET				;Otherwise, the job did itself ^_^
;
OUTER		LD	R2, R_SHIFTED		;Outer loop. Fetch R_SHIFTED -> R2
;
INNER		ADD	R0, R0, #1		;Inner loop. Inciment loop counter by 1...
		ADD	R2, R2, #-2		;...subtract 2 from the value to be shifted...
		BRp	INNER			;...until it's <= 0
		BRn	NEGA			;If it's <0, we need to subtract one from the loop counter
;
OUTWO		ST	R0, R_SHIFTED		;Save the loop counter (equal to RS_IN / 2) to R_SHIFTED
		AND	R0, R0, #0		;Reset the loop counter.
		ADD	R1, R1, #-1		;Decriment RS_AMOUNT...
		BRp	OUTER			;...if it's still >0, do the whole thing over.
;
		RET				;Time for a nice cuppa tea
;
NEGA		ADD 	R0, R0, #-1		;Subtract one from the loop counter...
		BRnzp	OUTWO			;... and it's back to work!
;
;
RIGHT_SHIFT_LSWA	;========================================================================
;
;REGISTER USAGE
;R0: Loop counter	R1: RS_IN -> R_SHIFTED
;R2: Unused		R3: KILLA	R4: Unused
;R5: Unused		R6: Unused	R7: RET address
;
		LD	R0, RS_AMOUNT		;Load R0 with number of shifts needed
		NOT	R0, R0			;Invert R0
		ADD	R0, R0, #1		;R0 now equals -RS_AMOUNT
		LEA	R3, KILLA		;R3 becomes array pointer for the progressive mask array.
		LD	R1, RS_IN		;Load RS_IN into R1...
		ST	R1, R_SHIFTED		;...store it in R_SHIFTED.
		ADD	R0, R0, #8		;Adding 16 (1/2)
		ADD	R0, R0, #8		;Adding 16 (2/2)
		BRnz	FASTEND			;If it's still negative, the result will be x0000
;
LOOPIT		LD	R1, R_SHIFTED		;Load R_SHIFTED into R1.
		BRn	NEGB			;If it's negative, break off to a specific function
		ADD	R1, R1, R1		;R1 = R_SHIFTED * 2: the trailing zero is automatically added
		ST	R1, R_SHIFTED		;Store it back in R_SHIFTED.
		ADD	R3, R3, #1		;Incriment the mask array pointer
		ADD	R0, R0, #-1		;Decrement the loop counter...
		BRp	LOOPIT			;...and if it's still positive, go again.
		BRnz	FINISH			;If not, call the ending method
;
NEGB		ADD	R1, R1, R1		;R1 ~ R_SHIFTED * 2
		ADD	R1, R1, #1		;Bring the MSB's '1' back to the LSB
		ST	R1, R_SHIFTED		;Store that shit in R_SHIFTED
		ADD	R3, R3, #1		;Incriment the mask array pointer
		ADD	R0, R0, #-1		;Decrement loop counter...
		BRp	LOOPIT			;...and if it's still positive, go again.
		BRnz	FINISH			;If not, call the ending method
;
FASTEND		AND 	R1, R1, #0		;Zero out the damn thing...
		ST	R1, R_SHIFTED		;...save it...
		RET				;...done.	
;
FINISH		LDR	R4, R3, #0
		AND	R1, R1, R4		;Use the mask on our result to knock out incorrect 1's
		ST	R1, R_SHIFTED
		RET				;Saunter off for a brew.
;
;
;===============PROGRAM VARIABLES==========================
;
TOSHIFT_X	.FILL x03FF	;Input value (fract X)
TOSHIFT_Y	.FILL x0008	;Input value (fract Y)
X		.FILL #0	;Input value (exp X)
Y		.FILL #9	;Input value (exp Y)
;
IEEE_EXT	.BLKW 1	;[IEEE_EXP2ACTUAL] Input vairable
ACTUAL_EXP	.BLKW 1	;[IEEE_EXP2ACTUAL] Output variable
;
A		.BLKW 1	;[ABS_DIFFERENCE] Input variable
B		.BLKW 1	;[ABS_DIFFERENCE] Input variable
R_ABS		.BLKW 1	;[ABS_DIFFERENCE] Output variable
;
RS_AMOUNT	.BLKW 1	;[RIGHT_SHIFT_*] Input variable
RS_IN		.BLKW 1	;[RIGHT_SHIFT_*] Input variable
R_SHIFTED	.BLKW 1 ;[RIGHT_SHIFT_*] Output variable
;
KILLA		.FILL x0000	;0000,0000,0000,0000: KILLA masks used to remove appropriate digits. Incriments
		.FILL x0000	;0000,0000,0000,0000: each time the left shift loop iterates
		.FILL x0000	;0000,0000,0000,0000
		.FILL x0000	;0000,0000,0000,0000
		.FILL x0000	;0000,0000,0000,0000
		.FILL x0000	;0000,0000,0000,0000
		.FILL x0000	;0000,0000,0000,0001
		.FILL x0003	;0000,0000,0000,0011	
		.FILL x0007	;0000,0000,0000,0111
		.FILL x000F	;0000,0000,0000,1111
		.FILL x001F	;0000,0000,0001,1111
		.FILL x003F	;0000,0000,0011,1111
		.FILL x007F	;0000,0000,0111,1111
		.FILL x00FF	;0000,0000,1111,1111
		.FILL x01FF	;0000,0001,1111,1111
		.FILL x03FF	;0000,0011,1111,1111
		.FILL x03FF	;0000,0011,1111,1111
		.FILL x03FF	;0000,0011,1111,1111
;
		.END		;All your base are belong to us.
