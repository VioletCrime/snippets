;File:        floatadd.asm
;Description: Floating-point adder
;Author(s):   Kyle Smith
;Date:        11/7/2011

; ********************** BEGIN RESERVED SECTION ********************** 
; You may initialize values here (i.e., change the value of a .FILL
; statement), but you may *not* add or remove instructions or perform
; any edit which changes the address of a label in this section
	
               .ORIG x3000       ; DO NOT change any code before label DATA_END
               BR MAIN          ; jump to beginning of code
DATA_BEGIN     .FILL xFFFF      ; start of provided variables
	
; ************************* SELECT SUBROUTINE ************************  
OPTION         .FILL #2         ; <--initialize this to select which subroutine to execute
                                ; 0 executes program (MY_MAIN) to compute the square of INPUT 
                                ; 1 executes subroutine MULTIPLY
				; 2 executes

; ********************** GLOBAL VARIABLES ********************** 

STACKBASE             .FILL     x4FFF           ; This is where the stack begins
EXPONENTMASK          .FILL     x7C00           ; Use this mask to extract the exponent
SIGNIFICANDMASK       .FILL     x03FF           ; Use this mask to extract the significand
NORMALIZRMASK         .FILL     x0400           ; Use this to add the implicit 1 to the significand
	
; ********************** INPUT/OUTPUT VARIABLES ********************** 
				; vars used by MAIN program
FLOAT_X	       .FILL xC000	; A 16-bit floating point number, for example, -2
FLOAT_Y        .FILL x4580      ; A 16-bit floating point number, for example, 5.5
FLOAT_SUM      .FILL x0		; Calculate and store FLOAT_X + FLOAT_Y here

DATA_END       .FILL xFFFF      ; end of provided variables

; ********************** END RESERVED SECTION ********************** 
;---------you may add more variables or code below this line --------

MAIN           LD    R6, STACKBASE       ; set the stack base
               ADD   R5, R6, #0          ; set main's frame pointer
	       LD  R0,OPTION    ; do a switch on "OPTION: and call ONE of the
               BRz MY_MAIN      ; if 0, execute main	     
               ADD R0,R0,#-1
               BRp CASE234
	       ;;load your arguments here
               JSR SUB_RightShift  ; execute option 1
               BR  DONE
CASE234        ADD R0,R0,#-1
               BRp CASE34
	       ;;load your arguments here
               JSR SUB_AbsDiff	   ; execute option 2
               BR  DONE
CASE34 	       ;;load your arguments here
	       JSR SUB_LessThan    ; execute option 3
	       ;; unwind stack
DONE           ADD   R6, R6, #2    ;
	       HALT

;; Main routine
;;   Adds two 16-bit IEEE floating point numbers
;;   Inputs: "FLOAT_X" and "FLOAT_Y" are two IEEE float values
;;   Output: "FLOAT_SUM" = FLOAT_X + FLOAT_Y
                     
MY_MAIN               ;; setup stack
                      LD    R6, STACKBASE       ; set the stack base
                      ADD   R5, R6, #0          ; set main's frame pointer

                      LD    R0, FLOAT_A         ; load the first float value into R0
                      LD    R1, FLOAT_B         ; load the second float value into R1

                      ;; push parameters
                      ADD   R6, R6, #-1         ;
                      STR   R1, R6, #0          ; push param 2
                      ADD   R6, R6, #-1         ;
                      STR   R0, R6, #0          ; push param 1

                      ;JSR  XXXXXXXXXX          ; call subroutine

                      LDR   R0, R6, #0          ; pop return value
                      ADD   R6, R6, #1          ;
                      ST    R0, FLOAT_SUM       ; store result in float_c

                      ;; unwind stack
                      ADD   R6, R6, #2          ;
                      HALT
               
;;
;;  Subroutines
;;

;; subroutine: SUBRightShift
;;  desc: Right shift the bits of an input word by the specified number of bits
;;  params:
;;      param 1 (word):   The word to be right shifted
;;      param 2 (amount): The amount to shift
;;  return:
;;      (shifted word):   The right shifted word
                      ;; load input values
SUB_RightShift         
                      ;; setup stack frame
                      ADD   R6, R6, #-1         ; allocate return value
                      ADD   R6, R6, #-1         ;
                      STR   R7, R6, #0          ; push caller's return address (R7)
                      ADD   R6, R6, #-1         ;
                      STR   R5, R6, #0          ; push caller's frame pointer (R5)
                      ADD   R5, R6, #-1         ; set current frame pointer FP = SP - 1

                      ;; get param 1
                      LDR   R0, R5, #4         ; get word to be shifted (note: word has to be > 0)
                      BRz   RightShift_DONE    ; if ( word == 0 ) shifted_word = word
                      ;; get param 2
                      LDR   R1, R5, #5         ; no. of times to right shift input word
                      BRz   RightShift_DONE    ; if ( amount == 0 ) shifted_word = word

                      ;; compute divisor
                      AND   R2, R2, #0          ; divisor (for left shift) will be computed in R2
                      ADD   R2, R2, #1          ;
RightShift_LS_LOOP    ADD   R2, R2, R2          ; r2 <- 2^(amount)
                      BRn   RightShift_ZERO     ; if at any time (divisor < 0) then return 0
                      ADD   R1, R1, #-1         ;
                      BRp   RightShift_LS_LOOP  ;
                      NOT   R2, R2              ;
                      ADD   R2, R2, #1          ; r2 <- -(r2)

                      ;; compute result
                      AND   R3, R3, #0          ; result will be computed in R3
RightShift_RS_LOOP    ADD   R3, R3, #1          ; increment result
                      ADD   R0, R0, R2          ; subtract divisor from word
                      BRzp  RightShift_RS_LOOP  ;
                      ADD   R3, R3, #-1         ; subtract 1 from result
                      STR   R3, R5, #3          ; store result in return location
                      BR    RightShift_RETURN   ;

RightShift_DONE       STR   R0, R5, #3          ;  if (word==0)||(amount==0) word_shifted = word
                      BR    RightShift_RETURN   ;

RightShift_ZERO       AND   R0, R0, #0          ;
                      STR   R0, R5, #3          ; if amount > 15, return 0

RightShift_RETURN     ;; unwind stack
                      ADD   R6, R5, #1          ; reset stack pointer (SP = FP + 1)
                      LDR   R5, R6, #0          ; pop caller's frame pointer
                      ADD   R6, R6, #1          ;
                      LDR   R7, R6, #0          ; pop caller's return address
                      ADD   R6, R6, #1          ;
                      RET


;; subroutine: SUB_AbsDiff
;;  desc: Compute the abosulute difference of two input numbers that are in 2's complement form
;;  params:
;;    param 1 (A):  2's complement number
;;    param 2 (B):  2's complement number
;;  return:
;;    (result): Absolute difference of input numbers (i.e., |A-B|)
SUB_AbsDiff
                      ;; setup stack frame
                      ADD   R6, R6, #-1         ; allocate return value
                      ADD   R6, R6, #-1         ;
                      STR   R7, R6, #0          ; push caller's return address (R7)
                      ADD   R6, R6, #-1         ;
                      STR   R5, R6, #0          ; push caller's frame pointer (R5)
                      ADD   R5, R6, #-1         ; set current frame pointer FP = SP - 1

                      ;; get param 1
                      LDR   R0, R5, #4         ; get A 
                      ;; get param 2
                      LDR   R1, R5, #5         ; get B

                      ;; subtract B from A
                      NOT   R1, R1             ;
                      ADD   R1, R1, #1         ; B = -(B)
                      ADD   R0, R0, R1         ;
                      BRzp  AbsDiff_DONE       ; if result is +ve, done
                      NOT   R0, R0             ;
                      ADD   R0, R0, #1         ; otherwise make result +ve

AbsDiff_DONE          STR   R0, R5, #3         ;

AbsDiff_RETURN        ;; unwind stack
                      ADD   R6, R5, #1          ; reset stack pointer (SP = FP + 1)
                      LDR   R5, R6, #0          ; pop caller's frame pointer
                      ADD   R6, R6, #1          ;
                      LDR   R7, R6, #0          ; pop caller's return address
                      ADD   R6, R6, #1          ;
                      RET


;; subroutine: SUB_LessThan
;;  desc: Returns true if input number X is less than input number Y
;;  params:
;;    param 1 (X):  2's complement number
;;    param 2 (Y):  2's complement number
;;  return:
;;    (result): Returns True (1) if X < Y, otherwise returns False (0)
SUB_LessThan
                      ;; setup stack frame
                      ADD   R6, R6, #-1         ; allocate return value
                      ADD   R6, R6, #-1         ;
                      STR   R7, R6, #0          ; push caller's return address (R7)
                      ADD   R6, R6, #-1         ;
                      STR   R5, R6, #0          ; push caller's frame pointer (R5)
                      ADD   R5, R6, #-1         ; set current frame pointer FP = SP - 1

                      ;; get param 1
                      LDR   R0, R5, #4          ; get X
                      ;; get param 2
                      LDR   R1, R5, #5          ; get Y

                      ;; subtract B from A
                      NOT   R1, R1              ;
                      ADD   R1, R1, #1          ; Y = -(Y)
                      ADD   R0, R0, R1          ;
                      BRzp  LessThan_DONE       ; if result is +ve (or zero), then X is not less than Y
                      AND   R0, R0, #0          ;
                      ADD   R0, R0, #1          ; otherwise X < Y
                      STR   R0, R5, #3          ; return 1 (true)
                      BR    LessThan_RETURN     ;

LessThan_DONE         AND   R0, R0, #0          ;
                      STR   R0, R5, #3          ; return 0 (false)

LessThan_RETURN       ;; unwind stack
                      ADD   R6, R5, #1          ; reset stack pointer (SP = FP + 1)
                      LDR   R5, R6, #0          ; pop caller's frame pointer
                      ADD   R6, R6, #1          ;
                      LDR   R7, R6, #0          ; pop caller's return address
                      ADD   R6, R6, #1          ;
                      RET

		      .END
