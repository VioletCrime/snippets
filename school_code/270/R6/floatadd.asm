;floatadd.asm
;PA5 (LC3 Assembly Program 3) by Kyle Smith
;CS270 Fall '11
;Started 11/13/2011 Finished 11/17/2011
;Best read in Vi, or any text editor with tab margins and static character width
;
		.ORIG x3000			;...and we're off...
;
MAIN	;================================================================================================================
;
;REGISTER USAGE
;R0: Temp var		R1: Temp var
;R2: Temp var		R3: Temp var		R4: Temp var
;R5: Frame Pointer	R6: Stack Pointer	R7: RET address
;
;======================================PREPARE VARIABLES=================================================
		LD	R6, STACKROOT		;Set R6 as a pointer to the stack's base
		ADD	R5, R6, #0		;Set R5 as MAIN's frame pointer
		ADD	R6, R6, #-5		;Give R5 some room to play....
;
		LD	R0, FLOAT_X		;Feed FLOAT_X into R0
		LD	R1, FLOAT_Y		;Feed FLOAT_Y into R1
;
		LD	R2, EXPMASK		;Load EXPMASK into R2
		LD 	R3, FRACTMASK		;Load FRACTMASK into R3
;
;		Isolate the exponent terms of our floating point numbers
		AND	R4, R0, R2		;Apply EXPMASK to FLOAT_X, store in R4
		ST	R4, EXP_X		;Dump the result in EXP_X
		AND	R4, R1, R2		;Apply EXPMASK to FLOAT_Y, store in R4 
		ST	R4, EXP_Y		;Dump the result in EXP_Y
;
;		Isolate the sign term of our floating point numbers
		LD	R2, SIGNMASK		;Bring up SIGNMASK
		AND	R4, R2, R0		;Apply SIGNMASK to FLOAT_X, store in R4
		ST	R4, SIGN_X		;Store in SIGN_X
		AND	R4, R2, R1		;Apply SIGNMASK to FLOAT_Y, store in R4
		ST	R4, SIGN_Y		;Store in SIGN_Y
;
;		Isolate the Fractional terms of our floating point numbers, add implicit 1.
		LD	R2, IMPLICIT1		;Load IMPLICIT1 into R2
		AND	R4, R0, R3		;Apply FRACTMASK to FLOAT_X -> R4
		ADD	R4, R4, R2		;Add the implicit 1 to what becomes FRAC_X
		ST	R4, FRAC_X		;Store the result in FRAC_X
		AND	R4, R1, R3		;Apply FRACTMASK to FLOAT_Y -> R4
		ADD	R4, R4, R2		;Add the implicit 1 to what becomes FRAC_Y
		ST	R4, FRAC_Y		;Store the result in FRAC_Y
;
;======================================SHIFT EXPONENTS===================================================
;		Prepare the stack for two calls to R_SHIFT; one for each float.
		AND	R4, R4, #0		;R4 = 0
		ADD	R4, R4, #10		;R4 = 10
		LD	R0, EXP_X		;Load EXP_X into R0
		LD	R1, EXP_Y		;Load EXP_Y into R1
		ADD	R6, R6, #-1		;Decrement the stack pointer
		STR	R4, R6, #0		;Push the amount to shift (10) onto stack
		ADD	R6, R6, #-1		;Decrement the stack pointer
		STR	R0, R6, #0		;Push R0 (EXP_X) onto the stack
		ADD	R6, R6, #-1		;Decrement the stack pointer
		STR	R4, R6, #0		;Push the amount to shift (10) onto stack
		ADD	R6, R6, #-1		;Decrement the stack pointer
		STR	R1, R6, #0		;Push R1 (EXP_Y) onto the stack
;
;		Call R_SHIFT for both FLOAT_X and FLOAT_Y
		JSR 	R_SHIFT			;Call R_SHIFT to shift EXP_Y
		LDR	R0, R6, #0		;Load R_SHIFT's result in R0
		ADD	R6, R6, #3		;Increment the stack pointer(over the first 2 inputs, too)
		ST	R0, AE_Y		;Store the shifted EXP_Y into AE_Y
		JSR	R_SHIFT			;Call R_SHIFT to shift EXP_X
		LDR	R1, R6, #0		;Load R_SHIFT's result in R1
		ADD	R6, R6, #1		;Increment the stack pointer
		ST	R1, AE_X		;Store the shifted EXP_X into AE_X
;
;======================================DETERMINE LARGEST VALUE- ADD===================================
;		Determine which of the exp values is larger
		LD	R0, AE_Y		;Load AE_Y into R0
		NOT	R3, R0			;NOT R0 into R3 (R3 = NOT AE_Y)
		ADD 	R3, R3, #1		;R3 = -R0
		ADD	R4, R3, R1		;R4 = R1 - R0 (R4 = AE_X - AE_Y)
		BRp	ECKS			;Case X > Y
		BRz	NUTN			;Case X ~= Y
		BRn	WHY			;Case X < Y
;
;		X > Y, fiddle accordingly
ECKS		LD	R0, SIGN_X		;Load X's sign bit into R0...
		ST	R0, FLOAT_SUM		;...and store it in FLOAT_SUM
		LD	R0, AE_X		;Load X's exponent bits into R0
		STR	R0, R5, #-1		;Push X's EXP word on the stack
		LD	R0, FRAC_Y		;Load the FRAC to be shifted (Y)
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R4, R6, #0		;Push the amount to shift (R4) onto stack
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R0, R6, #0		;Push the FRAC to be shifted (Y)
		JSR	R_SHIFT			;Take a wicked shift
		LDR	R0, R6, #0		;Load R_SHIFT's result into R0
		LD	R1, FRAC_X		;Load FRAC_X into R1
		STR	R1, R6, #0		;Push FRAC_X onto the stack
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R0, R6, #0		;Push shifted FRAC_Y onto the stack
		JSR	ADDEM			;Call ADDEM to add the two FRAC's
		LDR	R0, R6, #0		;Load ADDEM's result into R0
		ADD	R6, R6, #1		;Increment stack pointer
		BR	AFTERADD		;Onto the next part of the algoritm
;
;		Y < X, herp derp
WHY		LD	R0, SIGN_Y		;Load Y's sign bit into R0...
		ST	R0, FLOAT_SUM		;...and store it in FLOAT_SUM
		LD	R0, AE_Y		;Load Y's exponent bits into R0
		STR	R0, R5, #-1		;Push Y's EXP word onto the stack
		LD	R0, FRAC_X		;Load the FRAC to be shifted (X)
		NOT 	R4, R4			;R4 was negative; calculate absolute value...
		ADD	R4, R4, #1		;...done
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R4, R6, #0		;Push the amount to shift (R4) onto stack
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R0, R6, #0		;Push the FRAC to be shifted (X)
		JSR	R_SHIFT			;Take a wicked shift
		LDR	R0, R6, #0		;Load R_SHIFT's result into R0
		LD	R1, FRAC_Y		;Load FRAC_Y into R1
		STR	R0, R6, #0		;Push shifted FRAC_X onto the stack
		ADD 	R6, R6, #-1		;Decrement stack pointer
		STR	R1, R6, #0		;Push FRAC_Y onto the stack
		JSR 	ADDEM			;Call ADDEM to add the two FRAC's
		LDR	R0, R6, #0		;Load ADDEM's result into R0
		ADD 	R6, R6, #1		;Increment stack pointer
		BR	AFTERADD		;Onto the next part of the algorithm
;
;		X ~= Y, compare FRAC terms
NUTN		LD	R0, FRAC_X		;Load FRAC_X into R0
		LD	R1, FRAC_Y		;Load FRAC_Y into R1
		NOT 	R1, R1			;R1 = ~R1
		ADD	R1, R1, #1		;R1 = -R1
		ADD	R0, R0, R1		;R0 = R0 - R1
		BRp	ECKS			;X > Y
		BRn	WHY			;X < Y
		BRz	ORLY			;X = Y || X = -Y
;
;		There is no difference between the two float's exp and frac fields...
ORLY		LD	R0, SIGN_X		;Load X's sign bit into R0
		BRn	NEG1			;If it's negative, break off to NEG1
		LD	R0, SIGN_Y		;Load Y's sign bit into R0
		BRn	ENDR			;If it's negative NOW, then the 2 float's cancel to 0; end program
		BR 	SENDIT			;Otherwise, we need to add the two numbers.
;
NEG1		LD	R0, SIGN_Y		;Load Y's sign bit into R0
		BRn	SENDIT			;If it's also negative, we need to sum the two floats
		BR 	ENDR			;But if it's not negative, the result will be 0; end program
;
SENDIT		LD	R0, SIGN_X		;Get X's sign bit...
		ST	R0, FLOAT_SUM		;...and store it in FLOAT_SUM
		LD	R0, AE_X		;Get X's EXP bits
		STR	R0, R5, #-1		;Push X's EXP word onto the stack
		LD	R0, FRAC_X		;Load FRAC_X into R0
		LD	R1, FRAC_Y		;Load FRAC_Y into R0
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R0, R6, #0		;Push FRAC_X onto the stack
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R1, R6, #0		;Push FRAC_Y onto the stack
		JSR	ADDEM			;Call ADDEM to add the two FRAC's
		LDR	R0, R6, #0		;Load ADDEM's result into R0
		ADD	R6, R6, #1		;Increment stack pointer
		BR	AFTERADD		;Onto the next part of the algorithm
;
;======================================NORMALIZE RESULT================================================
;		Addition's been done. Normalize the result
AFTERADD	LD	R1, AAMASK		;Load the mask used to remove the 4 MSB's...
		AND	R0, R0, R1		;...and apply the mask to our result from ADDEM
		STR	R0, R5, #0
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R0, R6, #0		;Store our non-normal float sum in the stack for NORML
		JSR	NORML			;Jump to NORML to handle normalization
		LDR	R0, R6, #0		;Load the result given by NORML
		ADD	R6, R6, #1		;Increment stack pointer
		ADD	R0, R0, #0		;Prep for tests
		BRn	NAGN			;If it's negative, act accordingly
		BRz	ZAGN			;If it's zero...
		BRp	PAGN			;If it's positive...
;
NAGN		NOT	R0, R0			;Need to make it positive...
		ADD	R0, R0, #1		;..and now it is
		LDR	R1, R5, #0		;Get the non-normal float sum
		ADD	R6, R6, #1		;Increment stack pointer
		LDR	R2, R5, #-1		;Get the EXP (AE_?) term of the largest float
		ADD	R2, R2, R0		;Modify the EXP term to reflect the normalization
		AND	R4, R4, #0		;R4 = 0
		ADD	R4, R4, #10		;R4 = 10
		STR	R0, R5, #-2		;Store R0 for future use
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R4, R6, #0		;Push 10 onto stack (right shift by this)
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R2, R6, #0		;Push AE_? onto the stack
		JSR	L_SHIFT			;Call L_SHIFT to restore AE_? to the proper range
		LDR	R2, R6, #0		;Get L_SHIFT's result
		ADD	R6, R6, #1		;Increment stack pointer
		LD	R3, FLOAT_SUM		;Get FLOAT_SUM
		ADD	R3, R3, R2		;Add the EXP term to FLOAT_SUM
		ST	R3, FLOAT_SUM		;And put it back where you found it
		LDR	R0, R5, #-2
		LDR	R1, R5, #0
		ADD	R6, R6, #-1
		STR	R0, R6, #0
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R1, R6, #0		;Push the word to shift
		JSR	R_SHIFT			;Call R_SHIFT
		LDR	R0, R6, #0		;Get R_SHIFT's result
		ADD	R6, R6, #1		;Increment stack pointer
		LD	R1, IMPLICIT1		;Get the implicit 1 mask
		NOT	R1, R1			;Negate
		ADD	R1, R1, #1		;Negative
		ADD	R0, R0, R1		;Remove implicit 1 from normalized frac
		LD	R1, FLOAT_SUM		;Get FLOAT_SUM
		ADD	R1, R1, R0		;Add the FRAC term to FLOAT_SUM
		ST	R1, FLOAT_SUM		;Store FLOAT_SUM
		BR	ENDR			;DONE!
		
;
ZAGN		LDR	R1, R5, #0		;Get the float sum: no need to normalize
		LD	R2, IMPLICIT1		;Get the implicit 1 mask
		NOT	R2, R2			;Need it's negative...
		ADD	R2, R2, #1		;...
		ADD	R1, R1, R2		;Remove the implicit 1
		LDR	R2, R5, #-1		;Get the EXP term of the largest float
		LD	R3, FLOAT_SUM		;Get FLOAT_SUM
		ADD	R3, R3, R1		;Add the FRAC term to FLOAT_SUM
		ST	R3, FLOAT_SUM		;Put it back
		AND	R0, R0, #0		;R0 = 0
		ADD	R0, R0, #10		;R0 = 10
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R0, R6, #0		;Push 10 onto the stack
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R2, R6, #0		;Push the AE_? onto the stack
		JSR	L_SHIFT			;Call L_SHIFT to get AE_? back to the proper range
		LDR	R0, R6, #0		;Get L_SHIFT's result
		ADD	R6, R6, #1		;Increment stack pointer
		LD	R3, FLOAT_SUM		;Get FLOAT_SUM again...
		ADD	R3, R3, R0		;Add the EXP term to FLOAT_SUM
		ST	R3, FLOAT_SUM		;Store for future posterity
		BR	ENDR			;ALL DONE!
;
PAGN		LDR	R1, R5, #0		;Get the non-normal float sum
		LDR	R2, R5, #-1		;Get the EXP term of the largest term
		NOT	R4, R0			;R4 = ~R0
		ADD	R4, R4, #1		;R4 = -R0
		ADD	R2, R2, R4		;Modify the EXP term to reflect normalization
		AND	R4, R4, #0		;R4 = 0
		ADD	R4, R4, #10		;R4 = 10
		STR	R0, R5, #-2		;Store R0 for future use
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R4, R6, #0		;Push 10 onto the stack
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R2, R6, #0		;Push the modified EXP term onto the stack
		JSR	L_SHIFT			;Get L_SHIFT to do all the work for us
		LDR	R2, R6, #0		;Get L_SHIFT's result
		ADD	R6, R6, #1		;Increment stack pointer
		LD	R3, FLOAT_SUM		;Get FLOAT_SUM
		ADD	R3, R3, R2		;Add the EXP temp to FLOAT_SUM
		ST	R3, FLOAT_SUM		;And put it back where you found it
		LDR	R0, R5,#-2		;Get the amount to shift by
		LDR	R1, R5, #0		;Get the word to shift 
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R0, R6, #0		;Push the amount to shift by
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R1, R6, #0		;Push the word to shift
		JSR	L_SHIFT			;Call L_SHIFT to do the work
		LDR	R0, R6, #0		;Get L_SHIFT's result
		ADD	R6, R6, #1		;Increment stack pointer
		LD	R1, IMPLICIT1		;Get the implicit 1 mask
		NOT	R1, R1			;Need neg...
		ADD	R1, R1, #1		;...
		ADD	R0, R1, R0		;Remove the implicit 1 from the normalized result
		LD	R1, FLOAT_SUM		;Get FLOAT_SUM
		ADD	R1, R1, R0		;Add the normalized FRAC term to FLOAT_SUM
		ST	R1, FLOAT_SUM		;Store the result for future posterity
		BR	ENDR
;
ENDR		HALT			;STOP! Or my mom will shoot!
;
;======================================END MAIN=======================================================
;
;**********************************PROGRAM VARIABLES***********************************************************
;Centrally located for all your 'being-able-to-access' needs.
FLOAT_X		.FILL x9E00			;Input variable X
FLOAT_Y		.FILL x6040			;Input Variable Y
FLOAT_SUM	.FILL x0000			;Output variable (X+Y)

EXP_X		.FILL x0000			;Stores X's isolated exponent term
AE_X		.FILL x0000			;Stores X's shifted exponent term
FRAC_X		.FILL x0000			;Stores X's isolated fract term
SIGN_X		.FILL x0000			;Stores X's isolated sign bit

EXP_Y		.FILL x0000			;Stores Y's isolated sign bit
AE_Y		.FILL x0000			;Stores Y's shifted exponent term
FRAC_Y		.FILL x0000			;Stores Y's isolated fract term
SIGN_Y		.FILL x0000			;Stores Y's isolated sign bit

STACKROOT	.FILL X4FFF			;Base of the stack

SIGNMASK	.FILL x8000			;Mask used to isolate sign bit
EXPMASK		.FILL x7C00			;Mask used to isolate exponent bits
FRACTMASK	.FILL x03FF			;Mask used to isolate fractional bits
AAMASK		.FILL x0FFF			;Mask used to remove the leading 4 bits of a word
IMPLICIT1	.FILL x0400			;Used to manipulate the implicit 1
;
;		This block of masks is used to determine how to normalize the result
		.FILL x0800			;0000 1000 0000 0000
NMASK		.FILL x0400			;0000 0100 0000 0000 <- No normalization required
		.FILL x0200			;0000 0010 0000 0000
		.FILL x0100			;0000 0001 0000 0000
		.FILL x0080			;0000 0000 1000 0000
		.FILL x0040			;0000 0000 0100 0000
		.FILL x0020			;0000 0000 0010 0000
		.FILL x0010			;0000 0000 0001 0000
		.FILL x0008			;0000 0000 0000 1000
		.FILL x0004			;0000 0000 0000 0100
		.FILL x0002			;0000 0000 0000 0010
		.FILL x0001			;0000 0000 0000 0001
;
;
R_SHIFT ;****************************************************************************************************************
;
;REGISTER USAGE
;R0: Shift value	R1: Shift amount
;R2: Loop counter	R3: Unused		R4: Unused
;R5: Frame Pointer	R6: Stack Pointer	R7: RET address
;
		ADD	R6, R6, #-2		;Decrement stack pointer, leaving room for returned value
		STR	R7, R6, #0		;Push caller's return address (R7) onto stack
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR	R5, R6, #0		;Push caller's frame pointer onto the stack
		ADD	R5, R6, #-1		;Set R_SHIFT's frame pointer
;
		LDR	R0, R5, #4		;Get the value to shift
		STR	R0, R5, #3		;Put the value into the return section of the stack
		AND 	R2, R2, #0		;R2 = 0
		LDR	R1, R5, #5		;Get the amount to shift by
		BRp	OUTER			;If the shift counter is positive, get started...
		BRnz	HEADOUT			;...otherwise, head home
;
OUTER		LDR	R0, R5, #3		;Get the value to shift
;
INNER		ADD	R2, R2, #1		;Increment loop counter by 1
		ADD	R0, R0, #-2		;Subtract 2 from the shift value...
		BRp	INNER			;... until it's <=0
		BRn	NEGA			;If it's < 0, we need to subtract one from the loop counter
;
OUTWO		STR	R2, R5, #3		;Store the running result in the return value spot
		AND	R2, R2, #0		;Reset loop counter
		ADD	R1, R1, #-1		;Decrement shift counter
		BRp	OUTER			;If there's more to shift, do it all over...
		BRnz	HEADOUT			;If all the shifting's been completed, prep for RET
;
NEGA		ADD 	R2, R2, #-1		;Subtract 1 from the loop counter
		BR	OUTWO			;Back to work
;
HEADOUT		ADD	R6, R5, #1		;Reset stack pointer
		LDR	R5, R6, #0		;Load caller's frame pointer
		ADD	R6, R6, #1		;Increment stack pointer
		LDR	R7, R6, #0		;Load caller's return address
		ADD	R6, R6, #1		;Increment stack pointer
		RET				;Make like ET and phone home
;
;
L_SHIFT ;****************************************************************************************************************
;
;REGISTER USAGE
;R0: Sign Bit		R1: FRAC_X
;R2: FRAC_Y		R3: Result		R4: Unused
;R5: Frame Pointer	R6: Stack Pointer	R7: RET address
;
		ADD	R6, R6, #-2		;Decrement stack pointer, leaving room for returned value
		STR	R7, R6, #0		;Push caller's return address (R7) onto stack
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR 	R5, R6, #0		;Push caller's frame pointer onto the stack
		ADD	R5, R6, #-1		;Set L_SHIFT's frame pointer
;
		LDR	R0, R5, #4		;Get the word to shift, place in R0
		LDR	R1, R5, #5		;Get the amount to shift the word by, place in R1
		BRnz	DUSTYTRAIL		;If the amount to shift by is <= 0, no work needs to be done
;
AGAIN		ADD	R0, R0, R0		;R0 * 2 -> R0 (left shift by 1)
		ADD	R1, R1, #-1		;Decrement shift counter
		BRp	AGAIN			;If there's more to be done, do it
;
DUSTYTRAIL	STR	R0, R5, #3		;Once it's been shifted to our liking, store the result appropriately
		ADD	R6, R5, #1		;Reset stack pointer
		LDR	R5, R6, #0		;Restore caller's frame pointer
		ADD	R6, R6, #1		;Increment stack pointer
		LDR	R7, R6, #0		;Restopre caller's return address
		ADD	R6, R6, #1		;Increment stack pointer
		RET				;Home, Jeeves
;
;
ADDEM ;******************************************************************************************************************
;
;REGISTER USAGE
;R0: Sign Bit		R1: FRAC_X
;R2: FRAC_Y		R3: Result		R4: Unused
;R5: Frame Pointer	R6: Stack Pointer	R7: RET address
;
		ADD	R6, R6, #-2		;Decrement stack pointer, leaving room for returned value
		STR	R7, R6, #0		;Push caller's return address (R7) onto stack
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR 	R5, R6, #0		;Push caller's frame pointer onto the stack
		ADD	R5, R6, #-1		;Set ADDEM's frame pointer
;
		LDR	R1, R5, #5		;Load FRAC_X into R1
		LDR	R2, R5, #4		;Load FRAC_Y into R2
;
		LD	R0, SIGN_X		;Load X's sign bit into R0
		BRn	NEGX			;If it's negative, head off to a special subroutine
		LD	R0, SIGN_Y		;Load Y's sign bit into R0
		BRn	NEGY			;And if IT'S negative, jump to a different subroutine
		BR	ADDR			;If both are positive, go to the adder subroutine
;
NEGX		LD	R0, SIGN_Y		;Load Y's sign bit into R0
		BRn	ADDR			;If Y is ALSO negative, we just add the two together
		NOT	R1, R1			;Invert FRAC_X's bits in R1...
		ADD	R1, R1, #1		;...and add 1; R1 = -FRAC_X
		ADD	R3, R1, R2		;R2 - R1 -> R3
		BRn	FIXIT
		STR	R3, R5, #3		;Store the result in the stack's return space
		BR	BACKOUT			;GTFO of Dodge
;
NEGY		NOT	R2, R2			;Invert FRAC_Y's bits in R2...
		ADD	R2, R2, #1		;...and add 1; R2 = -FRAC_Y
		ADD	R3, R1, R2		;R1 - R2 -> R3
		BRn	FIXIT
		STR	R3, R5, #3		;Store the result in the stack's return space
		BR	BACKOUT			;Don't have to go home, but you can't stay here
;
ADDR		ADD	R3, R1, R2		;R1 + R2 -> R3
		STR	R3, R5, #3		;Store the result in the stack's return space
		BR	BACKOUT			;...and boy are my wings tired!
;
FIXIT		NOT	R3, R3			;Negate the sum...
		ADD	R3, R3, #1		;...add 1 to get the absolute value
		STR	R3, R5, #3		;Store that shit
;
BACKOUT		ADD	R6, R5, #1		;Reset stack pointer
		LDR	R5, R6, #0		;Restore caller's frame pointer
		ADD	R6, R6, #1		;Increment stack pointer
		LDR	R7, R6, #0		;Restopre caller's return address
		ADD	R6, R6, #1		;Increment stack pointer
		RET				;RTB
;
;
NORML ;******************************************************************************************************************
;
;REGISTER USAGE
;R0: Word to norm	R1: Mask pointer
;R2: Word tester	R3: Mask incrementer	R4: Modified mask pointer
;R5: Frame Pointer	R6: Stack Pointer	R7: RET address
;
		ADD	R6, R6, #-2		;Decrement stack pointer, leaving room for returned value
		STR	R7, R6, #0		;Push caller's return address (R7) onto stack
		ADD	R6, R6, #-1		;Decrement stack pointer
		STR 	R5, R6, #0		;Push caller's frame pointer onto the stack
		ADD	R5, R6, #-1		;Set L_SHIFT's frame pointer
;
		LDR	R0, R5, #4		;Get the word to normalize
		LEA	R1, NMASK		;Set R1 as the pointer to the block of masks
		AND	R3, R3, #0		;R3 = 0
		ADD	R3, R3, #-1		;R3 = -1 (Location of first mask, relative to R1)
		ADD	R4, R1, R3		;R4 = Mask pointer - 1
;
AGN		LDR	R2, R4, #0		;Get the mask
		NOT	R2, R2			;Invert it..
		ADD	R2, R2, #1		;...making it negative.
		ADD	R2, R0, R2		;Subtract R2 from the word to normalize
		BRzp	BRK			;If the result is positive or zero, we found the leading 1
		ADD	R3, R3, #1		;Otherwise, increment the mask incrementer... odd wording
		ADD	R4, R1, R3		;R4++
		BR	AGN			;Run it again!
;
BRK		STR	R3, R5, #3		;Store the result in the appropriate place in the stack
		ADD	R6, R5, #1		;Reset stack pointer
		LDR	R5, R6, #0		;Restore caller's frame pointer
		ADD	R6, R6, #1		;Increment stack pointer
		LDR	R7, R6, #0		;Restopre caller's return address
		ADD	R6, R6, #1		;Increment stack pointer
		RET				;[insert funny comment here]
;
;
;
		.END				;...and that's all she wrote.
