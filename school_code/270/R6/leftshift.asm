;; File:        leftshift.asm
;; Description: ... Titties and beer, boats and hos.
;; Author:      ... Jack Nasty Black Stroker (Kyle Smith [smithkyl])
;; Date:        ... 10/19/11

;; Function:     main
;; ... fill in description

;;REGISTER USAGE
;R0: Unused
;R1: A [COMPUTE_DIFF]
;R2: B -> -B -> A-B [COMPUTE_DIFF]
;R3: Word to shift (TOSHIFT) [LEFT_SHIFT]
;R4: Loop counter (LS_AMOUNT) [LEFT_SHIFT]
;R5: Unused
;R6: Swap space
;R7: Unused (JSR / Ret)


                .ORIG     x3000
MAIN            AND R1,R1,#0              ; R1 = 0
                JSR COMPUTE_DIFF          ; Call subroutine COMPUTE_DIFF
		LD R6, R_DIFF		;Load the Diff computed...
		ST R6, LS_AMOUNT	;...and store in LS_AMOUNT
                ; ... load value of R_DIFF and store in LS_AMOUNT
                JSR LEFT_SHIFT            ; Call subroutine LEFT_SHIFT
                HALT                      ; Program complete
;
;; Subroutine: COMPUTE_DIFF
;; Input: A, B
COMPUTE_DIFF 	 LD	R1, A		;Load A
		LD	R2, B		;Load B
		NOT	R2, R2		;Negate B
		ADD	R2, R2, #1	;Add one (R2 = -b)
		ADD	R2, R1, R2	;'Subtract' B from A
		ST	R2, R_DIFF	;Store the result in 'R_DIFF'
             	 RET                       ; return from subroutine
;
;
;; Subroutine: LEFT_SHIFT
;; Input:      TOSHIFT, LS_AMOUNT
LEFT_SHIFT    LD R3, TOSHIFT		;Load value to be shifted into R3
		LD R4, LS_AMOUNT	;Load LS_AMOUNT into R4
		BRnz	STOPAA		;Function call to stop program

AGAIN		ADD	R3, R3, R3	;Multiply by 2 (shift left one)
		ADD	R4, R4, #-1	;Decriment loop counter
		BRp	AGAIN		;Do it again if R4 is still pos.
		ST	R3, SHIFTED
              RET                       ; return from subroutine

STOPAA		HALT			;STOP! Or my mom will shoot!
;
;
;; Input variables
TOSHIFT   .FILL         x0004           ; Input word to left shift
A         .FILL         x0008           ; Input variable A, A >= B
B         .FILL         x0006           ; Input variable B, B <= A
LS_AMOUNT .FILL         x0001           ; Input to subroutine LEFT_SHIFT
;
;
;; Output variables
SHIFTED   .BLKW         1               ; Result after shifiting left
R_DIFF    .BLKW         1               ; Result computed by COMPUTE_DIFF
          .END
