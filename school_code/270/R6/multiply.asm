;
; Example assembly program, from textbook page 179
;
        .ORIG     x3050
        LD        R1,FIVE          ; R1 = 5
        LD        R2,NUMBER       ; R2 = NUMBER
		ADD		  R2, R2, #-2	  ; R2 - 2
        AND       R3,R3,#0        ; R3 = 0
;
; inner loop
;
AGAIN   ADD       R3,R3,R2        ; Add NUMBER to R3
        ADD       R1,R1,#-1       ; Decrement loop counter
        BRp       AGAIN           ; Branch is R1 > 0
		ADD 	  FIVE, FIVE, #-5 ; Set FIVE to zero
		ST        NUMBER, R3      ; Store R3 to NUMBER
;
        HALT                      ; Program complete
;
; program variables
;
NUMBER  .BLKW     1               ; Variable
FIVE     .FILL     x0005           ; Constant
;
        .END
