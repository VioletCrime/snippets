#include <math.h>
#include <stdio.h>
#include <stdbool.h>

//Function declaration
float quadratic(float a, float b, float c, bool returnFirstRoot);

//Function: quadratic
//Description: Solves a quadratic equation for one of its two roots.
//Parameters: a, b, and c represent the coefficients of a quadratic equation
//written in its standard form, i.i., ax^2 + bx + c = 0.
//If returnFirstRoot is true, this function returns the first of the two roots. If false, it returns the other.

float quadratic(float a, float b, float c, bool returnFirstRoot){
//avoid division by 0
if (a == 0) return 0.0;

float result = 0.0;
float temp = (b*b-4*a*c);

if (returnFirstRoot == true){
return (-b+sqrt(temp))/(2*a);
}

else{
return (-b-sqrt(temp))/(2*a);
}

return result;
}

int main(){
float a, b, c, root1, root2;
printf ("Quadratic Program\n");
printf("Enter a: ");
scanf("%f", &a);
printf("Enter b: ");
scanf("%f", &b);
printf("Enter c: ");
scanf("%f", &c);
root1 = quadratic(a, b, c, true);
root2 = quadratic(a, b, c, false);
printf("Roots are %3.2f and %3.2f\n", root1, root2);
return 0;
}
