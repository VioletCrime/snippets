;pa3.asm
;PA3 (LC3 Assembly program 1) by Kyle Smith
;CS270 Fall '11
;Started 10/09/2011

	.ORIG	x3000		; IT'S OVER 9000!!!!!!! (12288, to be exact)

;Register usage (MAIN and AGAIN)
;R0 - Value N-3
;R1 - Value N-2
;R2 - Value N-1
;R3 - Unused
;R4 - Unused
;R5 - Loop Counter (N)
;R6 - Pointer to array 'RESULT'
;R7 - Unused

;First order of business 
MAIN	JSR	TESTN		;Function call; set N appropriatly
	LD	R5,N		;Load N into R5 (Loop Counter)
	LEA	R6,RESULT	;Set R6 as a pointer to RESULT
	LDR	R0,R6,#0	;Load RESULT[0] into R0
	LDR	R1,R6,#1	;Load RESULT[1] into R1
	LDR	R2,R6,#2	;Load RESULT[2] into R2
	ADD	R6,R6,#3	;Incriment array pointer by three
	LD	R5,N		;Load N into R5 (Loop Counter)
	BRp	AGAIN		;If the loop counter is positive, enter the loop...
	HALT			;...otherwise, don't!

AGAIN	NOT	R1,R1		;Negate R1- step 1 of 2 to get negative R1
	ADD	R1,R1,#1	;Add 1: R1 is now the negative of what it was
	ADD	R2,R2,R1	;Subtract R1 from R2, store in R2
	ADD	R2,R0,R2	;Add (R2-R1) to R0, store in R2 (next n-1 value)
	STR	R2,R6,#0	;Store the result into RESULT[N]
	LDR	R0,R6,#-2	;Load RESULT[N-2] into R0
	LDR	R1,R6,#-1	;Load RESULT[N-1] into R1
	ADD	R6,R6,#1	;Incriment array pointer
	ADD	R5,R5,#-1	;Decriment loop counter
	BRp	AGAIN		;If the loop control variable > 0, do it again
	HALT			;GAME OVA, BITCHES!!!


;TESTN checks N to see if it is greater than 21
TESTN	LD	R1,N		;Load N into register 1
	ADD	R1,R1,#-11	;Can't subtract 21 all at once, as '-21' can't be represented... 
	ADD	R1,R1,#-10	;... by 5 bits in 2's-compliment binary (offset size)
	BRp	SETN		;Branch to SETN if R1-21 is positive
	RET			;Return to MAIN if N < 22
;
;SETN sets register 2 to '21' and stores that in 'N'
;
SETN	AND	R2,R2,#0	;Zero R2
	ADD	R2,R2,#11	;R2 = 11
	ADD	R2,R2,#10	;R2 = 21
	ST	R2,N		;N = 21
	RET			;Return to MAIN



;Program variables.
N	.FILL x0FFF		;N
;RESULT Array
RESULT	.FILL x0005		;RESULT[0]
	.FILL x0003 		;RESULT[1]
	.FILL x0002 		;RESULT[2]
	.FILL x0000 		;RESULT[3]
	.FILL x0000 		;RESULT[4]
	.FILL x0000 		;RESULT[5]
	.FILL x0000		;RESULT[6]
	.FILL x0000		;RESULT[7]
	.FILL x0000 		;RESULT[8]
	.FILL x0000 		;RESULT[9]
	.FILL x0000 		;RESULT[10]
	.FILL x0000 		;RESULT[11]
	.FILL x0000 		;RESULT[12]
	.FILL x0000 		;RESULT[13]
	.FILL x0000 		;RESULT[14]
	.FILL x0000 		;RESULT[15]
	.FILL x0000 		;RESULT[16]
	.FILL x0000 		;RESULT[17]
	.FILL x0000 		;RESULT[18]
	.FILL x0000 		;RESULT[19]
	.FILL x0000 		;RESULT[20]
	.FILL x0000 		;RESULT[21]
	.FILL x0000 		;RESULT[22]
	.FILL x0000 		;RESULT[23]
;
	.END	;End of the line! You don't have to go home,
		;but you can't stay here!	
