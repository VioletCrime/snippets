/**
 * File:        myfunctions.c
 * Description: --> fill this in
 * Author(s):   --> fill this in
 * Date:        --> fill this in
 **/
#include "myfunctions.h"

//Function I: radixNtoDecimal - Implement the body of this function first
/**
 * Description:
 * Converts a number (input as a string) in an indicated radix to 
 * a decimal number
 * Arguments: 
 * radixNNumber - a string representing the non-negative radixN integer to convert,
 * with most significant digit on the left.
 * radixN - a non-negative integer between 2 and 36 indicating the radix of radixNNumber
 * Precondition:
 * Digits of the string radixNNumber are valid for the given radix, radixN
 * Postcondition:
 * Returns the decimal value of radixNNumber as an integer
 **/
int radixNToDecimal(char radixNNumber[], int radixN){
 
  //-> Implement this method and return the correct value
  return -1;
}

//Function II: decimalToRadixN - Implement the body of this function second
/**
 * Description:
 * Converts a decimal number (output as a string) to the indicated radix
 * Arguments: 
 * decimalNumber - the non-negative integer to convert to radixN
 * radixN - a non-negative integer between 2 and 36 indicating the radix 
 * to convert decimalNumber into
 * result - an uninitialized array of char used to build and return
 * the string representation of decimalNumber in radixN
 * Postcondition:
 * Returns the radixN value of radixNNumber as a string, with the most significant
 * digit on the left.
 **/
char* decimalToRadixN(int decimalNumber, int radixN, char result[]){
  
  //-> Implement this method and return the correct value
  return "not yet implemented";
}

//Function III: radixAtoRadixB - Implement the body of this function third
/**
 * Description:
 * Converts a number (input as a string) in an indicated radix to another
 * radix (output as a string) 
 * Arguments:
 * radixANumber - a string representing the non-negative radixA integer to convert
 * radixA - a non-negative integer between 2 and 36 indicating the 
 * original radix of radixAnumber
 * radixB - a non-negative integer between 2 and 36 indicating the radix
 * to convert radixANumber into
 * result - an uninitialized array of char used to build and return
 * the string representation of radixANumber in radixB
 * Precondition:
 * Digits of the string radixNNumber are valid for the given radix, radixA
 * Postcondition:
 * Returns the radixB value of radixAnumber as a string
 **/
char* radixAToRadixB(char radixANumber[], int radixA, int radixB, char result[]){
  
  //-> Implement this method and return the correct value
  return "not yet implemented";
}

/**The following functions are provided to help you complete the assignment.  Do not alter these functions.**/
/**
 * Description: 
 * Converts a symbol into its decimal number equivalent (0->0, 1->1, ... 9->9, A->10, 
 * B->11, ..., Z->35).  This is the inverse function of decimalToSymbol.
 * Arguments:
 * symbol - a char ranging between '0' and '9' or between 'A' and 'Z'
 * Postcondition:
 * Returns the decimal equivalent of symbol
 **/
int symbolToDecimal(char symbol){
  int decimalNumber = -1;
  if(symbol >= '0' && symbol <= '9'){
    decimalNumber = symbol - '0';
  }
  else if(symbol >= 'A' && symbol <= 'Z'){
    decimalNumber = symbol - 'A' + 10;
  }
  return decimalNumber;
}

/**
 * Description:
 * Converts a decimal number into its symbol equivalent (0->0, 1->1, ..., 9->9, 10->A,
 * 11->B, ..., 35->Z).  This is the inverse function of symbolToDecimal.
 * Arguments:
 * decimalNumber - a non-negative integer ranging between 0 and 35
 * Postcondition:
 * Returns the symbol equivalent of decimalNumber
 **/
char decimalToSymbol(int decimalNumber){
  char symbol = -1;
  if(decimalNumber >= 0 && decimalNumber <= 9){
    symbol = decimalNumber + '0';
  }
  else if(decimalNumber >= 10 && decimalNumber <= 35){
    symbol = decimalNumber + 'A' - 10;
  }
  return symbol;
}
