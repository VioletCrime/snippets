#ifndef R5_POINTERS_H_
#define R5_POINTERS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

/*
 * a function whose input is passed by value
 * - demonstrates how passing by value does not change the original value
 */
void pass_by_value(int var);

/*
 * a function whose input is passed by reference
 * - demonstrates how results can be returned using pass by reference
 */
void pass_by_reference(int *ptr);

/* 
 * print pointer variables and their values
 * - demonstrates the use of single and double pointers
 */
void print_pointers();

/*
 * print the bits of a float variable using pointer manipulation
 * - demonstrates typecasting and pointer magic
 */
void print_float_bits(float f);

/*
 * go through an array of ints using array indices and pointer arithmetic
 * - demonstrates how C arrays are really pointers undeneath.
 * - understand how to staticly and dynamically allocate memory
 */
void arrays_and_pointers(int size);

#define MAX_ARRAY_SIZE 10

/*
 * deliberately cause segfaults and memory corruption
 * - understand why and how these are caused
 */
void memory_corruption();

/*
 * create both static and dynamic array of structs (complex data types)
 * - understand the syntax involved in working with arrays of structs
 */
void array_of_structs(int size);

/* a simple C struct with different type of variables */                                               
typedef struct multivalue_t {
    int   intvalue;
    float floatvalue;
    char  charvalue;
}multivalue_t;

#define MAX_MULTIVALUE_SIZE 32

#endif
