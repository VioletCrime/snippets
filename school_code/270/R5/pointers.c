#include "pointers.h"

int main(int argc, char* argv[]) {
  if (argc < 2) {
    printf("Usage:\n  r5 function_number\n");
    printf("        - valid function_numbers are 0-8\n");     
    exit(EXIT_FAILURE);
  }
  
  int a;
  float flt;
  
  switch(argv[1][0]) {
  case '1':
    a = 10;
    printf("value of a before call: %d\n", a);
    pass_by_value(a);
    printf("value of a after call: %d\n", a);
    break;
  case '2':
    a = 10;
    printf("value of a before call: %d\n", a);
    pass_by_reference(&a);
    printf("value of a after call: %d\n", a);
    break;
  case '3':
    print_pointers();
    break;
  case '4':
    flt = 4.75;
    print_float_bits(flt);
    break;
  case '5':
    arrays_and_pointers(255);
    break;
  case '6':
    memory_corruption();
    break;
  case '7':
    array_of_structs(10);
    break;
  case '8':
    a = 10;
    printf("value of a before call: %d\n", a);
    pass_by_value(a);
    printf("value of a after call: %d\n", a);
    a = 10;
    printf("value of a before call: %d\n", a);
    pass_by_reference(&a);
    printf("value of a after call: %d\n", a);
    print_pointers();
    flt = 4.75;
    print_float_bits(flt);
    arrays_and_pointers(255);
    memory_corruption();
    array_of_structs(10);
    break;
  default:
    printf("Unrecognized function number %s\n", argv[1]);
  }
  
  return(EXIT_SUCCESS);
}

void pass_by_value(int var) { //METHOD 1 *done*
  var = 20;
  printf("value of var inside pass_by_value: %d\n", var);
}

void pass_by_reference(int *ptr) { //METHOD 2 *done*
  /* assign a value to variable pointer to by ptr */
  *ptr = 20;
  printf("value of var inside pass_by_reference: %d\n", *ptr);
}

void print_pointers() { //METHOD 3 *done*
  int var = 10;
  /* get the address of variable "var", assign it to "ptr1", print the address
   * stored inside ptr1 and print the value pointed to by ptr1
   */
  int *ptr1 = &var;
  printf("address of var: 0x%X  value at address: %d\n", ptr1, *ptr1);
  
  /* get and print the address of "ptr1" (not the address inside ptr1!!) */
  printf("address of ptr1: 0x%X\n", &ptr1);
  
  /* assign the address of "ptr1" to another pointer (double pointer) */
  int **ptr2 = &ptr1;
  
  /* modify the value of var using ptr1 and print the new value */
  *ptr1 = 20;
  printf("writing to var using pointer (ptr1), var is now: %d\n", *ptr1);
  
  /* modify the value inside var using ptr2 and print the new value */
  **ptr2 = 30;
  printf("writing to var using double pointer (ptr2), var is now: %d\n", **ptr2);
  
  /* print the size of each of these variables using sizeof() function */
  printf("size of var: %d, size of ptr1: %d, size of ptr2: %d\n", sizeof(var), sizeof(ptr1), sizeof(ptr2));
}

void print_float_bits(float f) {//METHOD 4 *done*
  int bits = 0;
  //int *addy = (int *) &f;
  /*
   * get the address of float variable "f", typecast to an integer pointer,
   * deference it and assign to the integer variable "bits"
   */
  bits = (int *) &f;
  
  for (unsigned int mask = (1 << 31); mask > 0; mask = mask >> 1)
    (bits & mask) ? printf("1") : printf("0");
  printf("\n");
}


void arrays_and_pointers(int size) {//METHOD 5
  
  /* static array of ints */
  int static_array[MAX_ARRAY_SIZE];
  
  /* use array index operator [] to access elements */
  for (int i = 0; i < MAX_ARRAY_SIZE; i++){
    static_array[i] = i;
	
	}
  
  /* now write to static_array using pointer arithmetic */
  for (int i = 0; i < MAX_ARRAY_SIZE; i++) {
    *(static_array + i) = i;
    printf("value at *(static_array + i): %d\n", *(static_array + i));
  }
  
  /* convert static_array of ints to array of bytes */
  char *bytes = (char*)static_array;
  // -> what will this statement print?
  printf("value at bytes[4]: %d\n", bytes[4]);
  
  bytes[5] = 1;
  // -> what will this statement print?
  printf("value at static_array[1]: %d\n", static_array[1]);
  
  /* dynamically allocated array of ints */
  /* !! Note: malloc allocates in bytes !! */
  int *dyn_array = (int *)malloc(16*sizeof(int));
  
  /* initialize the elements using index operator */
  for (int i = 0; i < 16; i++){
		dyn_array[i] = i;
	}
  
  /* initialize the elements using pointer arithmetic */
  for (int i = 0; i < 16; i++){
	*(dyn_array + i) = i;
  }
}

void memory_corruption() { //METHOD 6
  int a_array[MAX_ARRAY_SIZE];
  int b_array[MAX_ARRAY_SIZE];
  
  /* stack corruption ... core dump */
  for (int i = MAX_ARRAY_SIZE; i >= 0; i++)
    a_array[i] = i;
  
  /* memory corruption ... overwritten values */
  for (int i = 0; i < 32; i++)
    b_array[i] = i;
  
  printf("A array has: ");
  for (int i = 0; i < MAX_ARRAY_SIZE; i++)
    printf("%d ", a_array[i]);
  printf("\n");
}

void array_of_structs(int size) {//METHOD 7
  
  printf("size of multivalue_t: %d\n", (int)sizeof(multivalue_t));
  
  /* static array of multivalue_t structs */
  multivalue_t mvarray_s[MAX_MULTIVALUE_SIZE];
  
  for (int i = 0; i < MAX_MULTIVALUE_SIZE; i++) {
    /* assign values to member variables */
    mvarray_s[i].intvalue    = i;
    mvarray_s[i].floatvalue  = (float) 1/(i+1);
    mvarray_s[i].charvalue   = '0' + i;
    printf("mvarray_s[%d]: %d, %f, %c\n", i, mvarray_s[i].intvalue, mvarray_s[i].floatvalue, mvarray_s[i].charvalue);
  }
  
  /* now create a dynamic array using the size parameter */
  multivalue_t *mvarray_d = (multivalue_t *)malloc(size*sizeof(multivalue_t));
  for (int i = 0; i < size; i++) {
    /* assign values to member variables */
    mvarray_d[i].intvalue    = i;
    mvarray_d[i].floatvalue  = (float) 1/(i+1);
    mvarray_d[i].charvalue   = '0' + i;
    printf("mvarray_d[%d]: %d, %f, %c\n", i, mvarray_d[i].intvalue, mvarray_d[i].floatvalue, mvarray_d[i].charvalue);
 
 }
	free(mvarray_d);
}
