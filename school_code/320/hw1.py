
"""
Assignment 1 - heaps

Kyle Smith. CS320, Spring '11, Asa Ben-Hur. 1/22/12 - 1/27/12
"""
import math

class Heap (object) :
    """
    A comment describing the class that you are implementing
    """
    def __init__(self) :
        """
        This is the constructor
        """
	self.heap = []

        pass  # the keyword "pass" means do nothing

    def __len__(self) :
        """
        This method should return the number of items in the heap
        """
        return len(self.heap)

    def __repr__(self) :
        """
        This method returns a string representation of a heap instance.
        Useful for both debugging purposes, and for providing the user
        with information about the instance.
        """
        width = max([len(str(item)) for item in self.heap])
        if width%2==1:
            width+=1
        rows=math.floor(math.log(len(self),2))
        output = []
        length=math.pow(2,math.floor(math.log(len(self),2)))*(width+2)
        for i in range(len(self)):
            s='%'+str(length/2-(width+2)/2)+'s %'+str(width)+'s '+'%'+str(length/2-(width+2)/2)+'s'
            print s
            output.append(s % ('',self.heap[i],'') )
            if math.floor(math.log(i+2,2))==math.log(i+2,2):
                output.append("\n")
                length=length/2

        return ''.join(output)

    def find_min(self) :
        """
        return the smallest element in the heap
        """
	return self.heap[0]
        """raise NotImplemented"""
    def insert(self, item) :
        """
        insert item into the heap
        """
	self.heap.append(item)
	self.heapify_up()

        """raise NotImplemented"""
    def delete(self) :
        """
        delete the smallest element from the heap
        """
	self.heap[0:1] = []
	self.heapify_down()
	"""raise NotImplemented"""

    def heapify_up(self) :
        """
        performs a heapify-up operation that starts at the last element in the heap
        """
        self.recursive_heapify_up(len(self) - 1)

    def recursive_heapify_up(self, i) :
        """
        recursively heapify-up
        """
	if (i > 0) :
	    if (self.heap[i] < self.heap[self.parent(i)]) :
		temp = self.heap[i]
		self.heap[i] = self.heap[self.parent(i)]
		self.heap[self.parent(i)] = temp
	    if (self.parent(i) > 0) :
		self.recursive_heapify_up(self.parent(i))
        """raise NotImplemented"""

    def heapify_down(self) :
        """
        Perform heapify-down starting from the root of the tree
        """
        self.recursive_heapify_down(0)

    def recursive_heapify_down(self, i) :
        """
        recursively heapify-down starting from a given node
        """
	if (self.has_left(i)) :
	    if (self.heap[i] > self.heap[self.left_child(i)]) :
		temp = self.heap[i]
		self.heap[i] = self.heap[self.left_child(i)]
		self.heap[self.left_child(i)] = temp
	    self.recursive_heapify_down(self.left_child(i))
    
	if (self.has_right(i)) :
	    if (self.heap[i] > self.heap[self.right_child(i)]) :
		temp = self.heap[i]
		self.heap[i] = self.heap[self.right_child(i)]
		self.heap[self.right_child(i)] = temp
	    self.recursive_heapify_down(self.right_child(i))
	    
        """raise NotImplemented"""
    
    def parent(self, i) :
	"""
	Returns the parent of index 'i'
	"""
	return (i-1)/2

    def left_child(self, i) :
	"""
	Returns the left child of index 'i'
	"""
	return 2*(i+1)

    def right_child(self, i) :
	"""
	Returns the right child of index 'i'
	"""
	return 2*i + 1

    def has_left(self, i) :
	"""
	Returns true/false representing whether an index 'i' has a left child
	"""
	return (self.left_child(i) < len(self))

    def has_right(self, i) :
	"""
	Returns true/false representing whether an index 'i' has a right child
	"""
	return (self.right_child(i) < len(self))

def heap_sort(in_list) :
    """
    returns a sorted version of the list given as input; sorting is performed
    using the heap-sort algorithm
    Usage:
    out_list = heap_sort(in_list)
    in_list should be unchanged as a result of the operation
    """

    if type(in_list) != list :
        raise ValueError, "expecting a list as input"

    out_list = []
    h2 = Heap()
    for stuff in in_list :
	h2.insert(stuff)
    while (len(h2) > 0) :
	out_list.append(h2.find_min())
	h2.delete()
    return out_list

if __name__ == '__main__' :

    items = [3, 5, 8, 1, 4, 9, 10, 2, 6, 7, 11]
    h = Heap()
    for item in items :
        print 'adding ', item
        h.insert(item)
    h.delete()
    print h
    print heap_sort(items)
