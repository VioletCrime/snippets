import os
from Queue import Queue

class graph (object) :
	"""
	A class that represents an undirected graph as an adjacency list
	"""

	gList = [] #The adjacency list
	names = [] #Becomes the keys of a dictionary
	indices = [] #Becomes the keys values
	i = 0 #Placed in indices[] every time a new node is added to the adj list
	dikt = {} #The dictionary; declared here for favorable scope


   	def add_edge(self, edge1, edge2) : #Adds nodes to g; updates dikt[] lists
		founda = 0 #Faux boolean value
		foundb = 0 #"" ""


		for l in self.gList : #First, iterate through the adj list
			if l[0][0] == edge1 : #Look for the first node in the adj list
				if l.count(edge2) == 0 : # If you find the node, and edge2 isn't there
					l.append(edge2) #... then append the 2nd edge
				founda = 1 #Set the flag to indicate edge1 was found

			if l[0][0] == edge2 : #Same as previous block
				if l.count(edge1) == 0 :
					l.append(edge1)
				foundb = 1

		if founda == 0 or foundb == 0 : # If one of the edges doesn't exist in the list:

			if founda == 0 :
				k = [edge1, 0] #Make a temporary list; will not be appended! 0 = 'no color'
				self.gList.append([k]) #Append the list to the adjacency list
				self.names.append(edge1) #Add the edge's name to the list of dict keys
				self.indices.append(self.i) #Add the corresponding key's value
				self.i += 1 #Prepare 'i' for the next key to represent

			if foundb == 0 : #Same as above for the second list
				k = [edge2, 0]
				self.gList.append([k])
				self.names.append(edge2)
				self.indices.append(self.i)
				self.i += 1

			for l in self.gList : #Finally, we have to add the other edge to the new entry
				if (l[0][0] == edge1 and founda == 0) : #If we didn't find edge1 before...
					l.append(edge2) #...append edge2 to edge1's new list
				if (l[0][0] == edge2 and foundb == 0) : #ditto for edge2
					l.append(edge1)


	def print_list(self) : #Debugging method: can't get a whole lot simpler than this
		print self.gList


	def colorBase(self) : #Control method for the recursive method 'color()'
		self.dikt = dict(zip(self.names, self.indices)) #Create the dictionary
		result = True #Set the default value of result to true

		for l in self.gList : #Iterate through the list, ensuring all nodes are checked in unconnected graphs [O(n)]
			if result == False : #If at any time, result is 'False'...
				break; #...our job is done
			if l[0][1] == 0 : #Otherwise, if the node hasn't been colored... 
				result = self.color(self.dikt[l[0][0]], 1) #...call 'color' with the index and color to use (1)
				
		return result #Holla back!


	def color(self, index, clr) : #Recurisev method with colors all nodes in a (sub?)graph
		result = True #Set default truth value

		if self.gList[index][0][1] != 0 : #If the node has been colored...
			if self.gList[index][0][1] != clr : #...and the color it is doesn't match what we're supposed to color it... 
				result = False #... we have our answer

		else : #If the node has NOT been colored...
			self.gList[index][0][1] = clr #...assign it the appropriate color...
			for node in range(1, len(self.gList[index])) : #...and recurse on every node it is connected to [O(m)]

				if result == False : #If any of the recursive calls return 'false'...
					break #...may as well clock out for the day

				if clr == 1 : #Making sure we're passing the correct color
					result = self.color(self.dikt[self.gList[index][node]], 2) #Recurse with next node's index and color

				if clr == 2 :
					result = self.color(self.dikt[self.gList[index][node]], 1)

		return result

def is_bipartite(g) :
	return g.colorBase() #Could have just implemented colorBase's algorithm here, but meh, what's ~40B of overhead?


def load_dot(file_name) :
	g = graph()
	if not os.path.isfile(file_name) :
		raise ValueError, 'file does not exist at ' + file_name
	file_handle = open(file_name)
	file_handle.readline()
	for line in file_handle :
		if line.find(';') > 0 :
			tokens = line[:line.find(';')].split('--')
		else :
			tokens = line.split('--')
		if len(tokens) < 2 : continue
		node1 = tokens[0].strip()
		node2 = tokens[1].strip()
		g.add_edge(node1, node2)
	#g.print_list()
	return g

if __name__ == '__main__' :

	g = load_dot('test.dot')
	print 'the graph is bipartite?', is_bipartite(g)
