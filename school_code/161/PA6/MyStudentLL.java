import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/** Complete the implementation of this class using a linked list to store the
 * data
 **/

public class MyStudentLL implements StudentCollectionIF{

	Node head = new Node();
	int size = 0;
	
	//Base (and only) constructor
	public MyStudentLL() {
	}

	//returns MyStudent object at specified index
	public StudentIF get(int index) {
		if (index >= size || index < 0){//index out of range test
			return null;
		}
		StudentIF result = null;
		int i = 0;
		Node tmp = head;
		while(i < index){//keeps setting the node to be returned until the actual index is reached. Sloppy, but effective.
			tmp = tmp.getNext();
			i++;
		}
		result = tmp.getData();
		return result;
	}

	//Sums all student scores and returns the average. Simple stuff.
	public double getAveScore() {
		if (size == 0){//Null pointer test...
			return 0;
		}
		StudentIF temp = null;
		double sum = 0;
		int i = 0;
		for (Node curr = head; curr != null; curr = curr.getNext()){
			temp = curr.getData();
			sum += temp.getScore();
			i++;
		}
		return sum / i;
	}

	//Goes through the list looking for 'id', returns the student it represents
	public StudentIF getStudent(int id) {
		if (size == 0){//No null pointer issues here, boss!
			return null;
		}
		for (Node curr = head; curr != null; curr = curr.getNext()){
			if(curr.getData().getId() == id){
				return curr.getData();
			}
		}
		return null;
	}

	//Goes through all the scores, returning the highest (nowai?)
	public double getTopScore() {
		if(size == 0){//Averting null pointer exceptions...
			return 0;
		}
		double max = 0;
		for(Node curr = head; curr != null; curr = curr.getNext()){
			if (curr.getData().getScore() > max){
				max = curr.getData().getScore();
			}
		}
		return max;
	}

	//A play on words, combining 'insert' and 'sort', the 'insort' method's job is to... well...
	public boolean insort(StudentIF s) {
		Node temp = new Node(s); //Create the node to be inserted
		if (size == 0){//If the size is 0; this node defaults to the first entry. Set 'head'.
			head = temp;
			size++;//Bookkeeping
			return true;
		}	
		for (Node curr = head; curr != null; curr = curr.getNext()){//Test loop; if s.ID already exists in the list, discard s
			if (curr.getData().getId() == s.getId()){
				return false;
			}
		}
		if (s.compareTo(head.getData()) < 0){//Comparing the head case separate from the giant for loop (previous node placeholder not required here)
			temp.setNext(head);
			head = temp;
			size++;
			return true;
		}
		Node ph = head; //placeholder for previous node; entering the for loop, it is the head.
		for (Node curr = head.getNext(); curr != null; curr = curr.getNext()){
			if (s.compareTo(curr.getData()) == 0){//Redundant check; should never eval to true.
				return false;
			}
			if (s.compareTo(curr.getData()) < 0){//if new Node comes before current node, set placeholder.next to new, and new.next to current.
				ph.setNext(temp);
				temp.setNext(curr);
				size++;
				return true;
			}
			ph = curr; //Setting the placeholder as the current Node before incrementing current node forward one.
		}
		ph.setNext(temp);//If the for loop has exited w/o a return, then 's' must come at the end of the list
		size++;
		return true;
	}

	//This method's job is to take a string, interpret it, and create a MyStudent to return.
	public StudentIF makeStudent(String studentData) {
	
		//Split the string, note the name, parse the ID, and create the grades array (initialize as empty).
		String[] temp = studentData.split(" ");
		String tempn = temp[0];
		int tempID = Integer.parseInt(temp[1]);
		int[] tempi = {};
	
		//Because 'Assign6' creates students differently (doesn't pass grade info), this 'if' test determines whether or not 
		//grade data was included. Its body creates the 'grades' array if the info was passed.This problem is also solvable by
		//simply creating another constructor for MyStudent which takes only 'name' and 'ID' as parameters, and calling appropriately here.
		if (temp.length > 2){
			String[] tempg = temp[2].split(",");
			tempi = new int[tempg.length];
			int i;
			for (i = 0; i < tempg.length; i++){
				tempi[i] = Integer.parseInt(tempg[i]);
			}
		}
	
		MyStudent result = new MyStudent(tempn, tempID, tempi);//Finally, create a student based on this info,
		return result;//and return it to the caller
	}

	//Opens files and imports the students it contains.
	public void open(String fileName) throws FileNotFoundException {
		Scanner input = null;
		File inny = new File(fileName);
		if(inny.exists()){
			try{
				input = new Scanner(inny);
			}
			catch (FileNotFoundException e){
			}
			String temp = null;
			StudentIF tmp = null;
			while (input.hasNextLine()){//For each line in the file,
				temp = input.nextLine();//copy is,
				tmp = makeStudent(temp);//use it to call 'makeStudent' method,
				insort(tmp);//and send the result of 'makeStudent' to insort for adding to the AL
			}
			input.close();
		}
	}

	//Remove a student based on their ID
	public boolean remove(int id) {
		if (size == 0){//Safeguarding against null pointer exceptions...
			return false;
		}
		if(head.getData().getId() == id){//If it's the head, set the head to be the next Node
			head = head.getNext();
			size--;//Record accordingly...
			return true;
		}
		Node temp = head; //previous node placeholder
		for(Node curr = head.getNext(); curr != null; curr = curr.getNext()){//For loop traverses the linked list
			if (curr.getData().getId() == id){//if we found out man...
				temp.setNext(curr.getNext());//...set the previous Node to redirect to the Node after this one.
				size--;//Bookkeeping
				return true;//Bagged 'em and tagged 'em.
			}
			temp = curr;//advancing the placeholder (will be previous node in next loop)
		}
		return false;//Which way did he go, boss? Which way did he go?
	}

	//Interesting block of code. First, it creates the output string, then decides where to output it.
	public void save(String fileName) throws FileNotFoundException {
		String result = "";
		if (size != 0){//Safeguarding against null pointer exceptions
		
			for (Node curr = head; curr != null; curr = curr.getNext()){ //Cycle through all Nodes and add their toString to the resulting string
				result = result + curr.getData().toString();
				if (curr.getNext() != null){
					result = result + "\n";
				}
			}
			if(fileName == null){//If no filename was given, put it on screen
				System.out.println(result);
			}
			else{//If a filename was given, yadda yadda
				try {  //This output block is effectively copy-pasted from http://www.codecodex.com/wiki/Save_a_string_to_a_file
					PrintWriter out = new PrintWriter(new FileWriter(fileName));
					out.print(result);
					out.close();
				} catch (IOException e) {
				}
			}		
		}	
	}

	//Gee, mister, it's so big!
	public int size() {
		return size;
	}
}