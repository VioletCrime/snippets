/** This represents the public methods of a student. You
 * will have two classes that utilize this interface. The
 * class <code>MyStudent</code> will <code>implement</code>
 * this interface. The class <code>MyHonorStudent</code> will
 * extend <code>MyStudent</code> and thus inherit the
 * interface. The <code>MyHonorStudent</code> class will be in the next
 * assignment.
 * <p>
 * Students have a name (need not be unique), an ID (unique)
 * and a set of grades (ints). Students hava a variable 
 * number of grades (ints) up to <code>maxNumGrades</code>. These
 * should be stored in an int array.
 */

public interface StudentIF {
	
  /** Used to distinguish a normal student from a xcel student.
   */
  public static final String HonorIdentifier = "!";

  /** No student will have more individual grades that
   * this number. They may, however, have fewer.
   */
  public static final int maxNumGrades = 10;

  /** Return the students name. For out examples, Students
   * will have only a single name.
   * @return the name of the student
   */
  public String getName();

  /** Each Student has a unique ID.
   * 
   * @return the ID of this student
   */
  public int getId();

  /** Determine the score for this student. For ordinary
   * students, the score is simply the average of the
   * grades of this student object. Note that the number may
   * vary from student to student.
   * @return the score for this student
   */
  public double getScore();

  /** Add a new grade to the list of grades.
   * @param newGrade - the grade to add
   * @return - true if the grade was added, false if
   * this grade exceeds the maximum number of grades.
   */
  public boolean addGrade (int newGrade);
	
  /** Produce a string representation of a student. The
   * format is name followed by a single blank, the id
   * followed by a single blank, a comma separated list
   * of scores (e.g. 85,93,98,77). If there are <b>no</b> scores,
   * include a value of -1.
   * <p>For example: <code>Kirk 1701 7,8</code>
   * <p>For example: <code>Spock 1941 -1</code>
   */
  public String toString();
	
  /** This method allows one to order students by name.
   * If the names match, compare them based on the ID.
   * 
   * @param other - the student to compare to 
   * <code>this</code>
   * @return - 0 if the two are the same, a value < 0
   * if <code>this</code> comes before <code>other</code>,
   * or a number > 0 if <code>this</code> comes after
   * <code>other</code>.
   */
  public int compareTo(StudentIF other);

  /** compare two students for equality. Students are equal
   * only if their ID's match.
   * 
   * @param obj - another student (may be null or not a StudentIF).
   * @return - true if they have the same ID, false otherwise
   */
  public boolean equals (Object obj);
}