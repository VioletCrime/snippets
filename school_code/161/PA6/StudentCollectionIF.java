import java.io.FileNotFoundException;

/** This interface will define the interaction between the
 * driver and the two collections you will implement. You
 * will implement <code>MyStudentAL</code> which uses an
 * <code>ArrayList</code> to store the student information.
 * You will also implement <code>MyStudentLL</code> which will
 * use a linked list to store the student data.
 */
public interface StudentCollectionIF {
  /** Add a Student to the list of students. Students
   * <b>may</b> have the same name, but <b>must</b> have
   * unique IDs. When a student is added using this
   * method, the list <b>must</b> be maintained in 
   * the order defined by the <code>compareTo()</code>
   * method. Students are inserted in the list.
   * Hence, then name <code>insort</code> (for insertion sort).
   * 
   * @param s the student to add
   * @return true if the student was added, false
   * if the student was not added. A student with a
   * duplicate ID will cause the add to fail and thus
   * return a false.
   */
  public boolean insort(StudentIF s);

  /** Remove the student with this ID.
   * @param id the id of the student to remove
   * @return true if the student was removed, false if
   * a student with this id does not exist.
   */
  public boolean remove(int id);
  
  /** Calculate the average score of all students
   * @return - the average score of all the students
   */
  public double getAveScore();

  /** get the student with the given id.
   * @param id - the id of the student
   * @return - the student or <code>null</code> if
   * the student does not exist.
   */
  public StudentIF getStudent (int id);

  /** get the Ith student in the collection.
   * @param index - the index of the student
   * @return - the student or <code>null</code> if
   * the index is out of range.
   */
  public StudentIF get (int index);

  /** Calculate the maximum score of all students
   * @return - the maximum score of all the students
   */
  public double getTopScore();

  /** Create a new <code>StudentIF</code> object from the string data.
   * In this assignment, the String will contain a name, an ID and an
   * optional list of grades (csv list of itegers). The lack of grades
   * may be indicated by nothing, or the value -1.
   * @param studentData - contains data for one student
   * @return an instance of a type that implements <code>StudentIF</code>.
   */
  public StudentIF makeStudent (String studentData);

  /** Populate a student collection from data stored in
   * a file. Each line of the file contains data for a
   * single student. 
   * @param fileName - the name of the file containing
   * the data. If the file does not exist the collection
   * is empty.
   */
  public void open (String fileName) throws FileNotFoundException;

  /** Save the student data to a file.
   * @param fileName - name of the file to save the data in.
   * If the <code>fileName</code> is <code>null</code>
   * print to <code>System.out</code>
   */
  public void save (String fileName)throws FileNotFoundException;

  /** Return the number of students in this collection */
  public int size();
}