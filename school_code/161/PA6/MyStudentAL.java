import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/** Complete the implementation of this class using a <code>ArrayList</code>
 * to store the data */

public class MyStudentAL implements StudentCollectionIF {
	
        ArrayList<StudentIF> students = new ArrayList<StudentIF>();//The ArrayList for the class.
        
        //Base (and only) constructor
        public MyStudentAL() {
        }

        //WAY simpler than the linked list implementation
        public StudentIF get(int index) {
        	if(index < 0 || index >= students.size()){//Test for 'index out of bounds exception'
        		return null;
        	}
        	return students.get(index);
        }

        //Sums all student scores and returns the average. Simple stuff.
        public double getAveScore() {
        	int i, k = 0;
        	double sum = 0;
        	for (i = 0; i < students.size(); i++){
        		sum += students.get(i).getScore();
        		k++;
        	}	
        	if (k == 0){
        		return 0;
        	}
        	return sum / k;
        }

        //Goes through all ArrayList iterations until student with 'id' is found; returns
        public StudentIF getStudent(int id) {
        	StudentIF tmp = null;
        	int i;
        	for (i = 0; i < students.size(); i++){
        		if(id == students.get(i).getId()){
        			tmp = students.get(i);
        			break;//no need to look further; may as well save processing time
        		}
        	}
        	return tmp;
        }

        //Goes through all the scores, returning the highest (nowai?)
        public double getTopScore() {
        	int i;
        	double result = 0;
        	for (i = 0; i < students.size(); i++){
        		if (students.get(i).getScore() > result){
        			result = students.get(i).getScore();
        		}
        	}
        	return result;
        }

        //A play on words, combining 'insert' and 'sort', the 'insort' method's job is to... well...
        public boolean insort(StudentIF s) {
        	if(students.size() == 0){//If no students yet exist, no sorting required; just add it to index 0.
        		students.add(0, s);
        		return true;
        	}
        	int i, tmp = s.getId();
        	for (i = 0; i < students.size(); i++){//Preliminary test to check ID's. If a duplicate is found, do not add.
        		if(tmp == students.get(i).getId()){
        			return false;
        		}
        	}
        	for (i = 0; i < students.size(); i++){
        		if (students.get(i).compareTo(s) == 0){//A redundant test which should never pass
        			return false;
        		}
        		if (students.get(i).compareTo(s) > 0){//kinda hard to read; if s < students[i](compare method), put s before students[i]
        			students.add(i, s);
        			return true;
        		}
        	}
        	students.add(i, s);//If the loop ended and 's' was never added nor discarded, it's place MUST be at the end; add it there.
        	return true;
        }

        //This method's job is to take a string, interpret it, and create a MyStudent to return.
        public StudentIF makeStudent(String studentData) {
	
        	//Split the string, note the name, parse the ID, and create the grades array (initialize as empty).
        	String[] temp = studentData.split(" ");
        	String tempn = temp[0];
        	int tempID = Integer.parseInt(temp[1]);
        	int[] tempi = {};
	
        	//Because 'Assign6' creates students differently (doesn't pass grade info), this 'if' test determines whether or not 
        	//grade data was included. Its body creates the 'grades' array if the info was passed. This problem is also solvable by
        	//simply creating another constructor for MyStudent which takes only 'name' and 'ID' as parameters, and calling appropriately here.
        	if (temp.length > 2){
        		String[] tempg = temp[2].split(",");
        		tempi = new int[tempg.length];
        		int i;
        		for (i = 0; i < tempg.length; i++){
        			tempi[i] = Integer.parseInt(tempg[i]);
        		}
        	}
	
        	MyStudent result = new MyStudent(tempn, tempID, tempi); //Finally, create a student based on this info,
        	return result;//and return it to the caller
        }

        //Opens files and imports the students it contains.
        public void open(String fileName) throws FileNotFoundException {
        	Scanner input = null;
        	File inny = new File(fileName);
        	if (inny.exists()){
        		try{
        			input = new Scanner(inny);
        		}
        		catch (FileNotFoundException e){
        		}
        		String temp = null;
        		StudentIF tmp = null;
        		while (input.hasNextLine()){//For each line in the file, 
        			temp = input.nextLine();//copy it,
        			tmp = makeStudent(temp);//use it to call 'makeStudent' method,
        			insort(tmp);//and send the result of 'makeStudent' to insort for adding to the AL
        		}
        		input.close();
        	}
        }

        //Remove students based on their ID. Simple, thanks to the ArrayList class
        public boolean remove(int id) {
        	int i;
        	for (i = 0; i < students.size(); i++){
        		if (students.get(i).getId() == id){
        			students.remove(i);
        			return true;
        		}
        	}
        	return false;//Which way did he go, boss? Which way did he go?
        }

        //Interesting block of code. First, it creates the output string, then decides where to output it.
        public void save(String fileName) throws FileNotFoundException {
        	String result = "";
        	int i;
        	for (i = 0; i < students.size(); i++){//Cycle through all students and add their toString to the resulting string
        		result = result + students.get(i).toString();
        		if (i != students.size() - 1){
        			result = result + "\n";
        		}
        	}
        	if(fileName == null){ //If no filename was given, put it on screen
        		System.out.println(result);
        	}
        	else{//If a filename was given, yadda yadda
        		try {  //This output block is effectively copy-pasted from http://www.codecodex.com/wiki/Save_a_string_to_a_file
        			PrintWriter out = new PrintWriter(new FileWriter(fileName));
        			out.print(result);
        			out.close();
        		} catch (IOException e) {
        		}
		   	}	
        }

        //Gee, mister, it's so big!
        public int size() {
        	return students.size();
        }
}