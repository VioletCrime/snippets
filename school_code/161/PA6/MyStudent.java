//Look ma! No imports!

//This class defines the object 'MyStudent', which implements the interface 'StudentIF'
//The program will create data structures of 'MyStudents'.
public class MyStudent implements StudentIF, java.lang.Comparable<StudentIF> {
	
	//Class variables. Private because, why not?
	private String name;
	private int ID;
	private int[] grades;
	
	//Base Constructor
	public MyStudent(){
		
	}
	//Typical constructor. Self-explanatory stuff.
	public MyStudent(String n, int eyeD, int[] gradez){
		name = n;
		ID = eyeD;
		grades = gradez;
	}

	//Used to add grades to the grade int[] array. This is
	//accomplished by creating a new array of 1 additional size,
	//copying the old array, and adding the new grade at the end.
	public boolean addGrade(int newGrade) {
		if (this.grades.length >= maxNumGrades){//Test to ensure max. grades is not exceeded.
			return false;
		}
		int i;
		int[] temp = new int[grades.length + 1];
		for (i = 0; i < grades.length; i++){//Array copy loop
			temp[i] = grades[i];
		}
		temp[grades.length] = newGrade; //Adding new grade to the end
		grades = temp; //Reassigning the grades array to the new correct array
		return true;
	}

	//Semi-complex block of code used to determine whether this.MyStudent comes before or after 'other'
	//in the data structures. -n for before, +n for after, 0 means the two are identically named and ID'd
	public int compareTo(StudentIF other) {
		if (other == null){ //this. comes before the end of linked lists...
			return -1;
		}
		if(this.name.equals(other.getName()) && this.ID == other.getId()){// equivalent case
			return 0;
		}
		else if (this.name.equals(other.getName())){//Same names; sort by ID numeric value
			if (this.ID > other.getId()){
				return 1;
			}
			else return -1;
		}
		else{ //The real workhorse; take 2 names and compare them
			String tmp1 = this.name.toLowerCase(); //making lower-case storage strings of both names...
			String tmp2 = other.getName().toLowerCase();
			int k;
			if (tmp1.length() > tmp2.length()){//assigning the length of the shorter string as the loop sentinel.
				k = tmp2.length();
			}
			else{
				k = tmp1.length();
			}
			char a1, a2;
			int i;
			for (i = 0; i < k; i++){//The muscle. Tests every pair of chars until a discrepancy is found
				if (i == k - 1){
					if (tmp2.length() > tmp1.length()){ //If the names differ only by length, the shortest is first
						return -1;
					}
					else{
						return 1;
					}
				}
				a1 = tmp1.charAt(i);//Getting chars to test
				a2 = tmp2.charAt(i);
				if (a1 > a2){//testing...
					return 1;
				}
				else if (a1 < a2){
					return -1;
				}//If they are the same, for loop starts again on the next pair of chars
			}
		}
		return 0;//Used for method completeness, though this should never be read in the program.
	}

	//Where are your papers?
	public int getId() {
		return this.ID;
	}

	//Guess what THIS one does...
	public String getName() {
		return this.name;
	}

	//Calculates and returns the student's score (sum grades / n)
	public double getScore() {
		if (grades.length == 0){
			return 0;
		}
		double result = 0;
		int i;
		for (i = 0; i < grades.length; i++){
			result += grades[i];
		}
		return result / grades.length;
	}
	
	//String output method. Format: 'Name ID grade1,grade2,...,gradeN'
	public String toString(){
		String result = name + " " + ID + " ";
		if(grades.length > 0){//If there is data in 'grades', the for loop will output it...
			int i;
			for (i = 0; i < grades.length; i++){
				result = result + grades[i];
				if (i < grades.length-1){
					result = result + ",";
				}
			}
		}
		else{ //...otherwise, it will be represented by a '-1' in the output.
			result = result + "-1";
		}
		
		return result;
	}
}