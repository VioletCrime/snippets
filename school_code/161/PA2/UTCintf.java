/** This interface defines the methods for a class you will implement 
 * (<code>MyUTC)</code>. This interface serves to separate the specification
 * and documentation of the methods you will implement from the actual
 * implementations. Normally all of this would be in your class. However,
 * by breaking it out, your code is not "cluttered" with all this information.
 * <p>
 * Author: Fritz Sieker
 *
 */

public interface UTCintf {

   /** Day of week of time 0 (1/1/1970 was a Wednesday). */
   public static final int dowOfZero = 3;

   /** Number of seconds in a day. It is good practice to declare constants */
   public static final long secPerDay = 60 * 60 * 24;

   /** Initial format for toString(). May be changed by setFormat.*/
   public static final String stdFormat = "%w %b, %d, %Y is day %j";

   /** Short names (3 chars) of days.*/
   public static final String[] dayNames = {"Sun", "Mon", "Tue", "Wed", "Thu",
                                       "Fri", "Sat" };

   /** Short names (3 chars) of months.*/
   public static final String[] months =
      {"", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep",
       "Oct", "Nov", "Dec"};

   /** Number of days in each month. Note that they array contains 13 elemnts.
    * This allows using the month number (1-12), directly without having to
    * offset it by 1 to correspond to the java indices that start at 0.
    */
   public static final int[] daysInMonth =
       {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

   /** A helper method to determine the day of the year.
    * @param month - the month (1 .. 12)
    * @param day - the day of the month (1 .. 31)
    * @param leapYear - true if the year is a leap year.
    * @return The day of the year for this day (1 .. 366). Note that you can use
    * the <code>daysUnitlMonth()</code> to do most of work. Return -1 if either
    * the day or month is invalid.
    */
   public abstract int dayOfYear (int month, int day, boolean leapYear);
   
   /** Extract the day of year from the valStr and return it. If the format is
    * dayOfYear/year, you can get the day of year directly.
    * if the format is month/day/year, use dayOfYear to convert month and
    * day into the day of the year.
    * @param valStr - the value to parse
    * @param leapYear - is this a leap year or not
    * @return - the day of the year, or -1 if the format is bad or there are
    * bad values.
    */
   public abstract int dayOfYear (String valStr, boolean leapYear);
   
   /** Determines if value fits in an <code>int</code>
    * @param value - value to be checked
    * @return true if the value fits in an <code>int</code>, false otherwise
    */
   public abstract boolean fitsInInt (long value);
   
   /** Convert a string like 15:30 or 2:35:56 to seconds since midnight
    * @param val - the string to convert
    * @return - the number of seconds since midnight, or -1 if the values
    * are not valid. String.split() is useful here.
    */
   public abstract int hmsStrToSec (String val);

   /** Determine if the specified year is a leap year or
    * not. If a year is divisible by 4 it is a leap year. However, a year
    * divisble by 100 is not a leap year unless it is also divisible by
    * 400.
    * 
    * @param year
    * @return true if the year <b>is</b> a leap year, false otherwise
    */
   public abstract boolean isLeapYear (int year);
   
   /** Determine if a daynumber is valid for a particular year
    * @param dayOfYear - the day to test
    * @param leapYear - whether this is a leap year
    * @return true if the dayOfYear is valid, false otherwise.
    */ 
   public abstract boolean isValidDayOfYear (int dayOfYear, boolean leapYear);
   
   /** Determine if an hour value is valid
    * @param hour - the value to be tested
    * @return true if the value is valid, flase otherwise
    */
   public abstract boolean isValidHour (int hour);
   
   /** Determine if the val is a valid minute or second
    * @param val - the value to be tested
    * @return - true if the value is valid, false otherwise
    */
   public abstract boolean isValidMinOrSec (int val);

   /** Determine if the specified month is a valid value.
    * @param month - the value to test
    * @return true if is valid, false otherwise.
    */
   public abstract boolean isValidMonth (int month);
   
   /* Determine if both the month and day are a valid combination
    * @param month - the month to test
    * @param day - the day to test
    * @param leapYear - is it a leap year or not
    * @return true if the month is valid and the day is a valid day for that
    * month. Othewise, return false.
    */
   public abstract boolean isValidMonthDay (int month, int day, boolean leapYear);
   
   /** Extract the year from the valStr and return it. Formats include
    * <code>month/day/year</code> and <code>dayOfYear/year</code>
    * @param valStr - the value to check
    * @return - the year, or -1 if the format is bad
    */
   public abstract int getYearFromStr (String valStr);
   
   /** Set the default format used by the
    * <code>toString()</code> method to convert an object to a string.
    * @param format - string of codes to use in formatting. See the
    * <code>toString(format)</code> for details. Once <code>setFormat()</code>
    * is used, that format is used for <b>ALL</b> formats until it is changed.
    */
   public abstract void setFormat (String format);

   /** A helper method to determine the number of days until
    * a month. Useful for converting a day of month into a day of year and
    * visa versa.
    * @param month - the month in question (1 .. 12)
    * @param leapYear - true if the year is a leap year.
    * @return how many days occur before the beginning of this month. That
    * value and the value of the day in the month is equal to the day of
    * the year (a value between 1 and 366). Return -1 for an invalid month.
    */
   public abstract int daysUntilMonth (int month, boolean leapYear);

   /** A helper method to determine the month from a day of the year
    * @param dayOfYear - the day of the year (1 .. 366)
    * @param leapYear - true if the year is a leap year
    * @return the month this day occurs in (1 .. 12) or -1 if the
    * dayOfYear is invalid.
    */
   public abstract int getMonth (int dayOfYear, boolean leapYear);

   /** A helper method to determine the day of a month from a
    * day of the year and the month,
    * @param dayOfYear - the day of the year (1 .. 366)
    * @param month - the month containing the day of the year (1 .. 12)
    * @param leapYear - true if the year is a leap year
    * @return the day of the month (1 .. 31) or -1 if either the month or
    * dayOfYear is invalid or the dayOfYear does not fall in the
    * specified month.
    */
   public abstract int getDay (int dayOfYear, int month, boolean leapYear);
   
   /** A helper method to compute the number of days from the
    *  beginning of "time" to the beginning of the given year. Suppose you
    *  are given a date in the form month/day/year. You can use this method to
    *  compute the days up to the beginning of the year. Note that the value
    *  may be negative. Then you may other method you have written to compute
    *  the portion of the year represented by the month and day.
    * @param year - the year to use
    * @return the number of days up to the end of the previous year.
    */

   public abstract int daysSince1970 (int year);
   /** Determine if two UTC's represent the same instant in
    * time. If <code>other</code> is <code>null</code>, return
    * <code>false</code>. Invalid dates are never equal.
    * @param other - an object (may be a <codeMyUTC</code> or something else)
    * @return - true if they represent the same day in time, false otherwise.
    */
   public boolean equals (Object other);

   /** Determine the number of <b>seconds</b> between
    * <code>this</code> and the specified MyUTC. If <code>other</code> is
    * <code>null</code>,
    * or invalid, treat it as a date with a 0 for <code>secondsSince1970</code>.
    * @param other - a second date (either before or after this date).
    * @return - difference in seconds, may be positive or negative. Why is it
    * declared <code>long</code>?
    */
   public abstract long diff (MyUTC other);

    /** <b>Implement</b> Create a new <code>MyUTC</code> object that is
    * before/after <code>this</code> UTC. For example
    * <code>date1.newUTC("-21d")</code> returns a <code>MyUTC</code> that is 21
    * days earlier than <code>date1</code>. It is
    * the opposite operation of the <code>diff()</code> method.
    * @param offset - a string containing the difference between two
    * dates. May be positive of negative. The suffix of the string may be
    * the letter <code>d</code> or the letter <code>h</code> representing
    * a difference in days or hours. If there is no suffix, assume days.
    * @return a <code>MyUTC</code> object (could be invalid). If there are any
    * errors in the <code>offset</code> return the caller.
    */
   public abstract MyUTC newUTC (String offset);
   
   /** Return a String representing this instant in time in
    * the specified format. If the date is not valid return <code>"invalid"</code>.
    * @param format - defines what information to print and in what order. The
    * format controls the output. The interpreted sequences are:
    * <ul>
    * <li><code>%a</code> abbreviated weekday name (e.g. Sun)</li>
    * <li><code>%b</code> abbreviated month name (e.g. Jan)</li>
    * <li><code>%d</code> day of month (e.g. 21)</li>
    * <li><code>%H</code> hour (00 .. 23)</li>
    * <li><code>%j</code> day of year (1 .. 366)</li>
    * <li><code>%m</code> month (01 .. 12)</li>
    * <li><code>%M</code> minute (00 .. 59)</li>
    * <li><code>%s</code> seconds since 1970-01-01 00:00:00 UTC</li>
    * <li><code>%S</code> second (00 .. 59)</li>
    * <li><code>%w</code> day of week (0 .. 6) 0 is Sunday</li>
    * <li>All other characters are added directly to the output. So,
    * if the format is "cs161 %a %H:%M:$S 75 minutes", the output would look
    * like * <code>cs161 Tue 12:30:00 75 minutes</code>.
    * </li>
    * One approach to this is to compute <b>every</b> portion of the "time"
    * and then combine those that are actually specified in the format.
    *
    */
   public abstract String toString (String format);
}