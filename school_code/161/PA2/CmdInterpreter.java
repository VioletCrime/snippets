import java.util.Arrays;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.NoSuchElementException;

/**
 * This is a base class for several assignments you will write. Do <b>NOT</b> 
 * change anything in this file. When grading, a different version of this
 * file <b>MAY</b> be used, so any changes you make would be lost.
 * <p>
 * In  writing your code, you may be tempted to use <code>System.out.println()
 * </code> to determine what is happening. As an alternative, the method
 * <code>trace()</code> is provided. The nice thing about <code>trace()</code>
 * in comparison to <code>System.out.println()</code> is that <code>trace()
 * </code> <b>only</b> prints when debug is turn <b>on</b>. You can turn
 * debug on and off by using the <code>debug</code> command in the program.
 * Thus, you can use it, but not need to change your program in any way before
 * turning it in. If you use <code>System.out.println()</code> you <b>MUST</b>
 * either comment out the lines or remove them before you submit your code
 * for grading.
 * <p>
 * Author: Fritz Sieker
 */
public class CmdInterpreter {
	
  /** A class to help end processing that is only used internally by
   * CmdInterpreter.
   */
  private static class StopProcessing extends RuntimeException {
    private static final long serialVersionUID = 1L; // keep Eclipse happy

    public StopProcessing (String msg) {
      super(msg);
    }
  }
  
  /** Any non-zero value turns debugging <b>on</b> */
  protected static int debug = 0;
  
  /** Print a message if the debug value is non-zero
   * @param msg - the message to be printed
   */
  public static void trace (String msg) {
    if (debug != 0) {
      System.out.println(msg);
    }
  }
  
  /** Print a message and an array if the debug value is non-zero
   * @param msg - the message to be printed
   * @param a - the array to be printed
   */
  public static void trace (String msg, int[] a) {
    trace (msg + " " + Arrays.toString(a));
  }
  
  /** Display a brief help summary for commands implemented by this class*/
  public void showHelp() {
    System.out.println("CmdInterpreter commands:");
    System.out.println("  debug <int>");
    System.out.println("  echo <messagee>");
    System.out.println("  exit");
    System.out.println("  help");
    System.out.println("  input <filename>");
    System.out.println();
  }

  /** Filter input for some special cases. 
   * @param input - the original string
   * @return The string is trimmed. 
   * If it is the String <code>"empty!"</code>, return <code>""</code>.
   * If it is the String <code>"null!"</code>, return <code>null</code>.
   * Otherwise, return the trimmed String. Override this for more special
   * conditions.
   */
  protected String filter (String input) {
    input = input.trim();

    if ("null!".equals(input))
      input = null;

    else if ("empty!".equals(input))
      input = "";

    return input;
  }

  /** Retrieve a String from the scanner, filtering it for convenience
   * @param scanner - retrieve String from this Scanner.
   * @return the filtered String (possibly <code>null</code>).
   */
  public String getStringOrNull (Scanner scanner) {
    String result = null;
    if (scanner.hasNext()) {
      result = filter(scanner.next());
    }
    return result;
  }
  
  /** Get an array of <code>String</code> separated by commas from the scanner.
   * @param scanner - source of the array
   * @return an array (possibly <code>null</code>, containing the
   * filtered values.
   */
  public String[] getStrArray (Scanner scanner) {
    String[] result = null;
    String cslString = getStringOrNull(scanner);

    if (cslString != null) {
      result = cslString.split(",");
      for (int i = 0; i < result.length; i++) {
        result[i] = filter(result[i]);
      }
    }
    
    return result;
  }
  
  /** Get an array of <code>int</code> separated by commas from the scanner.
   * @param scanner - source of the array
   * @return an array (possibly <code>null</code>, containing the values
   */
  public int[] getIntArray (Scanner scanner) {
    return strArrayToIntArray(getStrArray(scanner));
  }

  /** Convert an array of <code>String</code> to an arraof of <code>int</code>.
   * @param strResult - array of Strings to be converted to ints
   * @return Each String is converted to an int
   */
  public int[] strArrayToIntArray (String[] strResult) {
    int[] result = null;

    if (strResult != null) {
      result = new int[strResult.length];
      for (int i = 0; i < result.length; i++) {
        result[i] = Integer.parseInt(strResult[i]);
      }
    }

    return result;
  }
  
  /** Process one command.
   * @param cmd - the command to process
   * @param params - a string containing the parameter(s) (if any) 
   */
  public void processOneCommand (String cmd, String params)
    throws FileNotFoundException, NoSuchElementException
  {
	Scanner paramScanner = new Scanner(params);
	
	if (cmd.equals("debug")) { 
	  debug = paramScanner.nextInt();
	}
	else if (cmd.equals("echo")) {
	  System.out.println(params);
	 }
	else if (cmd.equals("exit")) {
	  throw new StopProcessing ("exit command");
	 }
	else if (cmd.equals("help") || cmd.equals("?")) {
	  showHelp();
	}
	else if (cmd.equals("input")) {
	  String fileName = paramScanner.next().trim();
	  processCmdsFromFile(fileName); 
	}
	else {
	  System.err.println("Unknown cmd: " + cmd + " " + params);
	}
  }

  /** Command interpreter to test code. Process the commands where
   * each command is contained on a separate line. Empty lines and
   * lines beginning with # are ignores
   * @param scanner - the source of the commands 
   */
  public void processCommands (Scanner scanner) {
	while (scanner.hasNext()) {
	  String line = scanner.nextLine().trim();
	  
	  if ((line == null) || (line.length() == 0) || line.startsWith("#"))
		continue; // ignore blank lines and lines starting with #
	  
	  Scanner cs    = new Scanner(line);
	  String cmd    = cs.next().toLowerCase();
	  String params = (cs.hasNext() ? cs.nextLine().trim() : "");
	  cs.close();
	  
	  try {
		processOneCommand(cmd, params);
	  }
	  catch (StopProcessing  sp) {
		break; 
	  }
	  catch (NoSuchElementException nsee) {
		System.err.println("syntax error: " + line);
		System.err.flush();
	  }
	  catch (Throwable t) {
		t.printStackTrace();
	  }
	}
	
	scanner.close();
  }
  
  /** Process commands from a file.
   * @param fileName - name of file containing commands
   */
  public void processCmdsFromFile (String fileName) {
	try {
	  processCommands(new Scanner(new File(fileName)));
	}
	catch (FileNotFoundException fnfe) {
	  System.err.println("can not input commands from file '" + fileName + "'");
	  System.err.flush();
	}
  }
  
  /** Process commands from <code>System.in</code>. */
  public void processCommands() {
	processCommands(new Scanner(System.in));  
  }
  
  /** Provide a main() like method that can be inherited and/or overridden.
   * 
   * @param args - arguments to process. If <code>args</code> is
   * <code>null</code> or of length 0, commands are taken from <code>System.in</code>.
   * If <code>args.length</code> > 0, the <code>args</code> are assumed to be one or more
   * file to be input. 
   */
  public void ooMain (String[] args) {
	if ((args == null) || (args.length == 0)) {
	  System.out.println("Enter commands:");
	  processCommands();
	}
	else for (int i = 0; i < args.length; i++) {
	  processCmdsFromFile(args[i]);
	}
  }
  
  public static void main (String[] args) {
	CmdInterpreter ci = new CmdInterpreter();
	ci.ooMain(args);
  }
}