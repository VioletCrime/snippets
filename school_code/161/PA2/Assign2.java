import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Scanner;

/** This is the driver class.
 *  <b>Don't</b> change anything in this class. It 
 *  will create MyUTC objects, and call MyUTC
 *  methods.<p>
 *  Author: Fritz Sieker
 */

public class Assign2 extends CmdInterpreter {

   /** A dummy value used to call helper methods */
   private UTCintf utc = new MyUTC();
	
   public Assign2() { // just for completeness
	 super();
   }

   /** This method prints out a brief summary of the
    * commands offered by this class. 
    */
   public void showHelp() {
     super.showHelp();
     System.out.println("Assign2 commands:");
     System.out.println("  calc <dateSpec> <offset> date at offset from another date");
     System.out.println("  date <dateSpec> [format]");
     System.out.println("  diff <dateSpec> <dateSpec>");
     System.out.println("  equals <dateSpec> <dateSpec>");
     System.out.println("  format <dateFormat>");
     System.out.println();
     System.out.println("Helper commands for Assign2:");
     System.out.println("  day <dayOfYear> <month> <year> - determine day in month");
     System.out.println("  dum <month> <year> - number of days to beginning of month in this year");
     System.out.println("  duy <year> - number of days 1970 until year");
     System.out.println("  doy <month> <day> <year> - determine day of year");
     System.out.println("  doy <month/day> <year> - determine day of year from string");
     System.out.println("  fits <value>");
     System.out.println("  hms <hour[:min[:sec]]> - convert to sec");
     System.out.println("  hour <val> - check for valid hour");
     System.out.println("  leap <year> - determine if yeap year");
     System.out.println("  min <val> - check for valid min or sec");
     System.out.println("  month <val> - check if month valid ");
     System.out.println("  month <dayOfYear> <year> - determine what month day of year is in");
     System.out.println("  valid <dayOfYear> <year> - check if day year valid ");
     System.out.println("  valid <month> <day> <year> - check if combo valid ");
     System.out.println("  year <month/day/year | doy/year> - extract year from string");
     System.out.println();
     System.out.println("  <dateSpec> means a date may be specified by:");
     System.out.println("     1. secSince1970 -or-");
     System.out.println("     2. dayofYear/year -or-");
     System.out.println("     3. month/dayOfMonth/year");
     System.out.println("        2&3 optionally followed by @hour[:min[:sec]]");
     System.out.println();
   }

   /** This method contains code to exercise some of the methods you will implement
    * in the <code>MyUTC</code> class. The commands are:
    * <ul>
    * <li>calc           - test <code>MyUTC.newUTC()</code></li>
    * <li>date           - test constructor and <code>MyUTC.toString(format)</code></li>
    * <li>day            - test <code>MyUTC.getDay()</code></li>
    * <li>diff           - test <code>MyUTC.diff()</code></li>
    * <li>doy            - test <code>MyUTC.dayOfYear()</code> -or-
    *                           <code>MyUTC.getDayOfYear()</code></li>
    * <li>dum            - test <code>MyUTC.daysUntilMonth()</code></li>
    * <li>duy            - test <code>MyUTC.daysSince1970()</code></li>
    * <li>equals         - test <code>MyUTC.equals()</code></li>
    * <li>fits           - test <code>MyUTC.fitsInInt()</code></li>
    * <li>format         - test <code>MyUTC.setFormat()</code></li>
    * <li>getdoy         - test <code>MyUTC.getDayOfYear()</code></li>
    * <li>hour           - test <code>MyUTC.isValidHour()</code></li>
    * <li>leap           - test <code>MyUTC.isLeapYear()</code></li>
    * <li>min            - test <code>MyUTC.isValidMinOrSec()</code></li>
    * <li>month          - test <code>MyUTC.getMonth()</code> -or -
    *                           <code>MyUTC.isValidMonth()</code></li>
    * <li>valid          - test <code>MyUTC.isValidDayOfYear()</code> -or-
    *                           <code>MyUTC.isValidMonthDayr()</code></li>
    * <li>year           - test <code>MyUTC.getYearFromStr()</code></li>
    * </ul>
    */
   public void processOneCommand (String cmd, String params)
     throws FileNotFoundException, NoSuchElementException
   {
     Scanner paramScanner = new Scanner(params);
	 
     if (cmd.equals("calc")) {
       MyUTC d1 = createDate(paramScanner);
       if (d1 != null)
  	   d1 = d1.newUTC(paramScanner.next());
       System.out.println(d1);
     }
     else if (cmd.equals("date")) {
       MyUTC date = createDate(paramScanner);
       if (paramScanner.hasNext()) {
         String fmt = paramScanner.nextLine().trim();
         System.out.println(((date == null) ? date : date.toString(fmt)));
       }
       else {
         System.out.println(date);
       }
     }
     else if (cmd.equals("day")) {
       int dayOfYear = paramScanner.nextInt();
       int month = paramScanner.nextInt();
       int year  = paramScanner.nextInt();
       boolean leap = utc.isLeapYear(year);
       System.out.println(utc.getDay(dayOfYear, month, leap));
     }
     else if (cmd.equals("diff")) {
       MyUTC d1 = createDate(paramScanner);
       MyUTC d2 = createDate(paramScanner);
       System.out.println(((d1 == null) ? d1 : d1.diff(d2)));
     }
     else if (cmd.equals("doy")) {
       if (paramScanner.hasNextInt()) { // calling dayOfYear(int,int,boolean)
         int month = paramScanner.nextInt();
         int day = paramScanner.nextInt();
         int year  = paramScanner.nextInt();
         boolean leap = utc.isLeapYear(year);
         System.out.println(utc.dayOfYear(month, day, leap));
       }
       else {
         String monthDay = paramScanner.next();
         int year  = paramScanner.nextInt();
         boolean leap = utc.isLeapYear(year);
         System.out.println(utc.dayOfYear(monthDay, leap));
       }
     }
     else if (cmd.equals("dum")) {
       int month = paramScanner.nextInt();
       int year  = paramScanner.nextInt();
       boolean leap = utc.isLeapYear(year);
       System.out.println(utc.daysUntilMonth(month, leap));
     }
     else if (cmd.equals("duy")) {
       System.out.println(utc.daysSince1970(paramScanner.nextInt()));
     }
     else if (cmd.equals("equals")) {
       MyUTC d1 = createDate(paramScanner);
       MyUTC d2 = createDate(paramScanner);
       System.out.println(((d1 == null) ? false : d1.equals(d2)));
     }
     else if (cmd.equals("fits")) {
       System.out.println(utc.fitsInInt(paramScanner.nextLong()));
     }
     else if (cmd.equals("format")) {
       String fmt = paramScanner.nextLine().trim();
       utc.setFormat(fmt);
     }
     else if (cmd.equals("hms")) {
       System.out.println(utc.hmsStrToSec(paramScanner.nextLine()));
     }
     else if (cmd.equals("hour")) {
       System.out.println(utc.isValidHour(paramScanner.nextInt()));
     }
     else if (cmd.equals("leap")) {
       System.out.println(utc.isLeapYear(paramScanner.nextInt()));
     }
     else if (cmd.equals("min")) {
       System.out.println(utc.isValidMinOrSec(paramScanner.nextInt()));
     }
     else if (cmd.equals("month")) {
       int val1 = paramScanner.nextInt();
       if (! paramScanner.hasNextInt()) {
         System.out.println(utc.isValidMonth(val1));
       }
       else {
         int     year = paramScanner.nextInt();
         boolean leap = utc.isLeapYear(year);
         System.out.println(utc.getMonth(val1, leap));
       }
     }
     else if (cmd.equals("valid")) {
       int i1 = paramScanner.nextInt();
       int i2 = paramScanner.nextInt();
       if (! paramScanner.hasNextInt()) {
    	 System.out.println(utc.isValidDayOfYear(i1, utc.isLeapYear(i2)));
       }
       else {
    	 boolean leap = utc.isLeapYear(paramScanner.nextInt());
    	 System.out.println(utc.isValidMonthDay(i1, i2, leap));
       }
    	 
     }
     else if (cmd.equals("year")) {
       System.out.println(utc.getYearFromStr(paramScanner.nextLine()));
     }
     else {
       super.processOneCommand(cmd, params);
     }
	 
     paramScanner.close();
   }
   
   /** A helper method to create dates, depending on input to command
    * @param ps - the Scanner to get input from
    * @return a <code>MyUtc</code> object or <code>null</code>.
    */
   private MyUTC createDate(Scanner ps) {
	 MyUTC  date = null;
	 String dstr = this.getStringOrNull(ps);
	 
	 if (dstr != null) {
	   date = new MyUTC(dstr);
	 }
	 
	 return date;
   }

   public static void main (String[] args) {
     Assign2 a2 = new Assign2();
     a2.ooMain(args);
   }
}
