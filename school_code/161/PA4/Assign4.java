public class Assign4 {
	public static void main(String[] args){
		Cities cities = new Cities();
		try{
			cities.addFromFile(args[0]);
		}
		catch(Throwable th){
			System.out.println("File not found.");
			System.exit(0);
		}
		System.out.println(cities);
		if (cities.size() > 0){
			City a = cities.get(0);
			int i = 1;
			for (i = 1; i < cities.size(); i++){
				City b = cities.get(i);
				System.out.print("Distance between " + a + " and " + b + " is");
				System.out.println(String.format("%5.2f",a.distanceBetween(b)));
			}
		}
	}
}