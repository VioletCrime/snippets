
public class City{

	private String name;
	private double x, y;
	
	public City(String inName, double ecks, double why){
		name = inName;
		x = ecks;
		y = why;
	}
	
	public String getName(){
		return this.name;
	}
	
	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;
	}
	public String toString(){
		String result = "";
		result = result + name + "(" + String.format("%.2f", x) + "," + String.format("%.2f", y) + ")";
		return result;
	}
	public double distanceBetween(City other){
		if (this == null || other == null){
			return 0;
		}
		double result;
		double x1 = this.x, x2 = other.getX(), y1 = this.y, y2 = other.getY();
		result = Math.sqrt(Math.pow(x1-x2,2) + Math.pow(y1-y2,2)); //Taken directly from the assignment 4 description
		return result;
	}
}
