import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;


public class Cities {
	ArrayList<City> Ctz = new ArrayList<City>();
	
	public void addFromFile(String filename)throws FileNotFoundException{
		Scanner input = null;
		File inny = new File(filename);
		try{
			input = new Scanner(inny);
		}
		catch (FileNotFoundException e){
			throw new Error();
		}
		int i = 0;
		while (input.hasNextLine()){
			String tmpName = input.next();
			double tmpX = Double.parseDouble(input.next());
			double tmpY = Double.parseDouble(input.next());
			City temp = new City(tmpName, tmpX, tmpY);
			Ctz.add(i, temp);
			i++;
			input.nextLine();
		}
		input.close();
	}
	
	public int size(){
		return Ctz.size();
	}
	
	public City get(int i){
		if (i < 0 || i > Ctz.size()){
			return null;
		}
		return Ctz.get(i);
	}
	public String toString(){
		String result = "           City Name    X     Y \n-------------------- ------ ------\n";
		int i = 0, j = Ctz.size();
		for (i = 0; i < j; i++){
			City tmpCity = Ctz.get(i);
			result = result + String.format("%20s", tmpCity.getName());
			result = result + String.format("%7.2f", tmpCity.getX());
			result = result + String.format("%7.2f", tmpCity.getY());
			result = result + "\n";
		}
		return result;
	}
	
}
