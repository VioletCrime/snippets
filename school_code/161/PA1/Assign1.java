import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.NoSuchElementException;
import java.io.FileNotFoundException;

/** This class provides code for testing the classes <code>MyXXX.java</code>
 * that you will complete. Do <b>NOT</b> change anything or add anything
 * to this class
 */

public abstract class Assign1 extends CmdInterpreter
{
  /** Determine if the denominator evenly divides the numerator
   * @param numerator - <b>any</b> integer
   * @param denominator <b>any</b> integer
   * @return <code>true</code> if <code>denominator</code> evenly divides
   * <code>numerator</code>. Otherwise, return <code>false</code>.
   */
  public abstract boolean divides (int numerator, int denominator);

  /** Find all elements of the list that are evenly divisible by the denominator
   * @param list - an array of integer values
   * @param denominator <b>any</b> integer
   * @return a list of those elements that are evenly divisible. The values
   * in the returned list <b>must</b> be in the same order as the values occur
   * in the parameter <code>list</code>.
   */
  public abstract int[] findMultiples (int[] list, int denominator);

  /** Count the number of words and the number of lines in a file.
   * If the file does not exist, print 0 for both values. The output should be
   * the number of lines followed by a blank followed by the number of words
   * followed by a newline. To check you answer, you may use the <code>wc</code>
   * command from a terminal shell.
   * @param fileName - the name of the file to process.
   */
  public abstract void wordCount (String fileName);

  /** Merge two sorted lists to produce a sorted list containing all the values
   * from both lists. Duplicates are allowed. You <b>must</b> handle
   * <b>all</b> possible values for the lists, including empty/null lists.
   * You many not use any <code>java</code> sort methods.
   * @param list1 - an array of sorted values
   * @param list2 - another array of values
   * @return a sorted list containing the elements from both lists.
   */
  public abstract int[] merge (int[] list1, int[] list2);

  /** Given a list of names, print out all combinations of the names taken
   * three at a time. Names <b>must</b> occur in the same order that they
   * appear in the list. So, if the list contains the names <code>Kennedy,
   * Johnson, Nixon, Ford</code>, you program prints:
   * <p><pre>
   * [Kennedy, Johnson, Nixon]
   * [Kennedy, Johnson, Ford]
   * [Kennedy, Nixon, Ford]
   * [Johnson, Nixon, Ford]
   * </pre>
   * <p>Put the values in an <code>array</code> and then use the
   * <code>Arrays.toString()</code> method to print the results, one per line.
   * @param list - a list of name.
   */
  public abstract void trio (String[] list);

  /** Display a list of commands useful for testing the classes you will
   * complete for this assignment.
   * The commands are:
   * <ul>
   * <li><b>div</b> Determine if one value evenly divides another
   * <li><b>merge</b> merge two ordered lists
   * <li><b>multiples</b> find values divisible by another value
   * <li><b>trio</b> find combinations of three names
   * <li><b>wc</b> find the number of lines and words in a file.
   * </ul>
   */
   public void showHelp() {
     super.showHelp();
     System.out.println("AssignmentX commands:");
     System.out.println(" div <numerator> <denominator>");
     System.out.println(" merge <csvListOfValues> <csvListOfValues>");
     System.out.println(" multiples <csvListOfValues> <denominator>");
     System.out.println(" trio <csvListOfNames>");
     System.out.println(" wc <fileName>");
     System.out.println();
   }
   
   /** Process one command for this assignment.*/
   public void processOneCommand (String cmd, String params)
       throws FileNotFoundException, NoSuchElementException
   {
     Scanner scan = new Scanner(params);
     
     if (cmd.equals("div")) {
       System.out.println(divides(scan.nextInt(), scan.nextInt()));
     }
     
     else if (cmd.equals("merge")) {
       System.out.println(Arrays.toString(merge(getIntArray(scan),
                                                getIntArray(scan))));
     }
 
     else if (cmd.equals("multiples")) {
       System.out.println(Arrays.toString(findMultiples(getIntArray(scan),
                                          scan.nextInt())));
     }
     
     else if (cmd.equals("trio")) {
       trio(getStrArray(scan));
     }
     
     else if (cmd.equals("wc")) {
       wordCount(scan.next());
     }

     else { // See if super class can handle it
       super.processOneCommand(cmd, params);
     }

     scan.close();
   }
}
