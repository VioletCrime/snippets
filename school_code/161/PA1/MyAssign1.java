import java.util.Arrays;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

/** This class contains code that you will complete. */

public class MyAssign1 extends Assign1
{
   // This class does not compile because it has unimplemented abstract methods.
   // Use the IDE to automatically create the method stubs for you!

   public static void main (String[] args) {
     MyAssign1 a1 = new MyAssign1();
     a1.ooMain(args);
   }

@Override
public boolean divides(int numerator, int denominator) {
	if(denominator == 0){ //Testing for attempt to divide by zero
		return false;
	}
	else if(numerator % denominator == 0) //If a clean division, return true; else false
		return true;
	else
		return false;
}

@Override
public int[] findMultiples(int[] list, int denominator) {
	if (list == null){
		int[] i = new int[0];
		return i;
	}
	if(denominator == 0){ //Testing for attempt to divide by zero
		System.out.println("Denominator cannot be zero.");
		return null;
	}
	int i = 0;
	int j = 0;
	//testing for length of result array
	for (i = 0; i < list.length; i++){
		if(list[i] % denominator == 0)
			j++;
	}
	//declaring result array
	int[] results = new int[j];
	j = 0;
	//test again (rather sloppy method, if I'm honest...), recording appropriate numerators for impending return.
	for (i = 0; i < list.length; i++){
		if (list[i] % denominator == 0){
			results[j] = list[i];
			j++;
		}
	}
	return results;
}

@Override
public void wordCount(String fileName) {
	File infile = new File(fileName);
	Scanner inny = null;
	try{
	inny = new Scanner(infile);
	}
	catch(FileNotFoundException e){
		System.out.println("0 0");
		return;
	}
	int i = 0, j = 0;
	
	while (inny.hasNextLine()){
		String innput = inny.nextLine();
		String[] testbed = innput.split("[ ]");
		if (innput.length() == 0)
			i-= 1;
		i += testbed.length;
		j++;
	}
	
	System.out.println(j + " " + i);
}

@Override
public int[] merge(int[] list1, int[] list2) {
	if (list1 == null && list2 == null)
		return null;
	else if (list1 == null)
		return list2;
	else if (list2 == null)
		return list1;
	int len1 = list1.length;
	int len2 = list2.length;
	int[] list3 = new int[len1 + len2];
	if (len1 == 0 && len2 == 0)
		return list3;
	else if (len1 == 0)
		return list2;
	else if (len2 == 0)
		return list1;
	
	int i, j, k; //i = list1 placeholder, j = list2 placeholder, k = list3 placeholder
	for (i = 0, j = 0, k = 0; k < list3.length; k++){
		if (i == len1){
			list3[k] = list2[j];
			j++;
		}
		else if (j == len2){
			list3[k] = list1[i];
			i++;
		}
		else if (list1[i] < list2[j]){
			list3[k] = list1[i];
			i++;
		}
		else{
			list3[k] = list2[j];
			j++;
		}
	}
	return list3;
}

@Override
public void trio(String[] list) {
	if (list == null)
		return;
	int len = list.length;
	int i, j, k;
	for (i = 0; i < len - 2; i++){
		for (j = (i + 1); j < len - 1; j++){
			for (k = (j + 1); k < len; k++){
				System.out.println("[" + list[i] + ", " + list[j] + ", " + list[k] + "] ");
			}
		}
	}
}
}