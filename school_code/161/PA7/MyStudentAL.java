import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Iterator;

/** Complete the implementation of this class using a <code>ArrayList</code>
 * to store the data */

public class MyStudentAL extends MyAbstractCollection {
	
        ArrayList<StudentIF> students = new ArrayList<StudentIF>();//The ArrayList for the class.
        
        //Base (and only) constructor
        public MyStudentAL() {
        }

        //WAY simpler than the linked list implementation
        public StudentIF get(int index) {
        	if(index < 0 || index >= students.size()){//Test for 'index out of bounds exception'
        		return null;
        	}
        	return students.get(index);
        }

        //A play on words, combining 'insert' and 'sort', the 'insort' method's job is to... well...
        public boolean insort(StudentIF s) {
        	if(students.size() == 0){//If no students yet exist, no sorting required; just add it to index 0.
        		students.add(0, s);
        		return true;
        	}
        	int i, tmp = s.getId();
        	for (i = 0; i < students.size(); i++){//Preliminary test to check ID's. If a duplicate is found, do not add.
        		if(tmp == students.get(i).getId()){
        			return false;
        		}
        	}
        	for (i = 0; i < students.size(); i++){
        		if (students.get(i).compareTo(s) == 0){//A redundant test which should never pass
        			return false;
        		}
        		if (students.get(i).compareTo(s) > 0){//kinda hard to read; if s < students[i](compare method), put s before students[i]
        			students.add(i, s);
        			return true;
        		}
        	}
        	students.add(i, s);//If the loop ended and 's' was never added nor discarded, it's place MUST be at the end; add it there.
        	return true;
        }


        //Remove students based on their ID. Simple, thanks to the ArrayList class
        public boolean remove(int id) {
        	int i;
        	for (i = 0; i < students.size(); i++){
        		if (students.get(i).getId() == id){
        			students.remove(i);
        			return true;
        		}
        	}
        	return false;//Which way did he go, boss? Which way did he go?
        }

        //Gee, mister, it's so big!
        public int size() {
        	return students.size();
        }

		public void clear() {
			students.clear();			
		}

		public Iterator<StudentIF> iterator() {
			return students.iterator();
		}
		
}