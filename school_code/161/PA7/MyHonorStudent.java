/** Complete the implementation of this class. This class represents an honor
 * student. An honor student <code>isa</code> an <code>MyStudent</code> object
 * with one additional facet. Honor students do a project in addition
 * to their other grades. The project is given a letter grade (A, B, C, D).
 * Their score is calculated by taking 90% of the the average, then adding
 * 10 points for an A, 6 points for a B, and 3 points for a C
 * <p>
 * To differentiate an Honor Student from a regular student in an external
 * representation, an honor students name is preceded by an <code>!</code>.
 * See the constant in the <code>StudentIF</code>. However, the <code>!</code>
 * is <b>not</b> part of the name. Additionally, the honor
 * student's project score will be part of the data (following the list of
 * integer grades). The <code>toString()</code> method will need to accommodate
 * these differences. Most of the work can be done in the parent class.
 **/

public class MyHonorStudent extends MyStudent {

	char projScore;
	String HId = HonorIdentifier;
	
	public MyHonorStudent(String n, int eyeD, int[] gradez, char pScore){
		super(n, eyeD, gradez);
		projScore = pScore;
	}
	public void setProjScore (String score) {
		projScore = score.charAt(0);
	}
  
	public double getScore() {
		int[] temp = getGrades();
		double result = 0;
		if(temp.length != 0){
			for(int i = 0; i < temp.length; i++){
				result += temp[i];
			}
			result = result * .9 / temp.length;
		}
		switch (projScore){
	  		case('A'): result += 10;
	  		break;
	  		case('B'): result += 6;
	  		break;
	  		case('C'): result += 3;
	  		break;
		}
		return result;
	}
  
	public String toString() {
		String result = StudentIF.HonorIdentifier + getName() + " " + getId() + " ";
		int[] temp = getGrades();
		if(temp.length > 0){
			for (int i = 0; i < temp.length; i++){
				result = result + temp[i];
				if (i < temp.length - 1){
					result = result + ",";
				}
			}
		}
		else{
			result = result + "-1";
		}
		if (projScore != 'Q'){
			result = result + " " + projScore;
		}
		return result;
	}
}