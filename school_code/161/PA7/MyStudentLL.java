import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Scanner;

/** Complete the implementation of this class using a linked list to store the
 * data
 **/

public class MyStudentLL extends MyAbstractCollection{

	Node head = new Node();
	int size = 0;
	
	//Base (and only) constructor
	public MyStudentLL() {
	}

	//returns MyStudent object at specified index
	public StudentIF get(int index) {
		if (index >= size || index < 0){//index out of range test
			return null;
		}
		StudentIF result = null;
		int i = 0;
		Node tmp = head;
		while(i < index){//keeps setting the node to be returned until the actual index is reached. Sloppy, but effective.
			tmp = tmp.getNext();
			i++;
		}
		result = tmp.getData();
		return result;
	}

	//A play on words, combining 'insert' and 'sort', the 'insort' method's job is to... well...
	public boolean insort(StudentIF s) {
		Node temp = new Node(s); //Create the node to be inserted
		if (size == 0){//If the size is 0; this node defaults to the first entry. Set 'head'.
			head = temp;
			size++;//Bookkeeping
			return true;
		}	
		for (Node curr = head; curr != null; curr = curr.getNext()){//Test loop; if s.ID already exists in the list, discard s
			if (curr.getData().getId() == s.getId()){
				return false;
			}
		}
		if (s.compareTo(head.getData()) < 0){//Comparing the head case separate from the giant for loop (previous node placeholder not required here)
			temp.setNext(head);
			head = temp;
			size++;
			return true;
		}
		Node ph = head; //placeholder for previous node; entering the for loop, it is the head.
		for (Node curr = head.getNext(); curr != null; curr = curr.getNext()){
			if (s.compareTo(curr.getData()) == 0){//Redundant check; should never eval to true.
				return false;
			}
			if (s.compareTo(curr.getData()) < 0){//if new Node comes before current node, set placeholder.next to new, and new.next to current.
				ph.setNext(temp);
				temp.setNext(curr);
				size++;
				return true;
			}
			ph = curr; //Setting the placeholder as the current Node before incrementing current node forward one.
		}
		ph.setNext(temp);//If the for loop has exited w/o a return, then 's' must come at the end of the list
		size++;
		return true;
	}


	//Remove a student based on their ID
	public boolean remove(int id) {
		if (size == 0){//Safeguarding against null pointer exceptions...
			return false;
		}
		if(head.getData().getId() == id){//If it's the head, set the head to be the next Node
			head = head.getNext();
			size--;//Record accordingly...
			return true;
		}
		Node temp = head; //previous node placeholder
		for(Node curr = head.getNext(); curr != null; curr = curr.getNext()){//For loop traverses the linked list
			if (curr.getData().getId() == id){//if we found out man...
				temp.setNext(curr.getNext());//...set the previous Node to redirect to the Node after this one.
				size--;//Bookkeeping
				return true;//Bagged 'em and tagged 'em.
			}
			temp = curr;//advancing the placeholder (will be previous node in next loop)
		}
		return false;//Which way did he go, boss? Which way did he go?
	}

	//Gee, mister, it's so big!
	public int size() {
		return size;
	}

	public void clear() {
		head = null;
		size = 0;
		
	}

	public Iterator<StudentIF> iterator() {
		return new LLIterator(head);
	}
	
//=======Inner class iterator===========
	public class LLIterator implements Iterator<StudentIF>{
		int plc = 0;
		Node place = null;
		
		public LLIterator (Node head){
			place = head;
		}

		public boolean hasNext() {
			boolean result = false;
			Node temp = place.getNext();
			if(temp != null)
				result = true;
			return result;
		}

		public StudentIF next() {
			if(plc == 0){
				plc++;
				return place.getData();
			}
			place = place.getNext();
			plc++;
			return place.getData();
		}

		public void remove() {			
		}
	}
}