import java.util.ArrayList;
import java.util.Scanner;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.io.FileNotFoundException;

/** This class provides code for testing the classes <code>MyXXX.java</code>
 * that you will complete. Do <b>NOT</b> change anything or add anything
 * to this class
 * <br>
 * Fritz Sieker
 */

public class Assign7 extends Assign6
{
   /** Display a list of commands useful for testing the classes you will
    * complete for this assignment. 
    * Note that <b>most</b> of the commands are processed by the 
    * <code>Assign6.processCmd()</code> method. The commands are:
    * <ul>
    * <li><b>clear</b> remove everything from the collection</li>
    * <li><b>iterate</b> print the students using an iterator</li>
    * <li><b>proj</b> set the project score of an honor student</li>
    * <li><b>score</b> print score of a student using toScore()</li>
    * </ul>
    */
   public void showHelp() {
     super.showHelp();
     System.out.println("Assignment 7 commands:");
     System.out.println(" clear - remove the contents of the collection");
     System.out.println(" iterate - use the iterator code");
     System.out.println(" proj <ID> <projScore>");
     System.out.println(" score <ID> print result of getScore() method");
     System.out.println();
   }
   
   /** Process one command for this assignment.*/
   public void processOneCommand (String cmd, String params)
       throws FileNotFoundException, NoSuchElementException
   {
     Scanner scan = new Scanner(params);
     
     if (cmd.equals("clear")) {
       sci.clear();
     } 
     else if (cmd.equals("iterate")) {
       for (Iterator<StudentIF> iter = sci.iterator(); iter.hasNext(); ) {
         StudentIF s = iter.next();
         System.out.println(s);
       }
     }
     
     else if (cmd.equals("proj")) {
       StudentIF s = sci.getStudent(scan.nextInt());
       if (s instanceof MyHonorStudent) {
         MyHonorStudent hs = (MyHonorStudent) s;
         hs.setProjScore(scan.next().trim());
       }
       System.out.println(s);
     }
     
     else if (cmd.equals("score")) {
       StudentIF s = sci.getStudent(scan.nextInt());
       System.out.println(s.getScore());
     }
     
     else {
       super.processOneCommand(cmd, params);
     }

     scan.close();
   }

   public static void main (String[] args) {
     Assign7 assign = new Assign7();
     assign.ooMain(args);
   }
}