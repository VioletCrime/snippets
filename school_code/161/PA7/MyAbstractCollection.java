import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Scanner;

/** This  class will be the parent class to both your <code>MyStudentAL</code>
 * and <code>MyStudentLL</code> classes. It will provide much of the
 * functionality currently in the two existing classes. It will leave 
 * unimplemented  those methods which can <b>only</b> be implemented by
 * the subclasses. That is why this class is <code>abstract</code>.
 * <p>
 * Specifically, you <b>must</b> implement these methods in this class and
 * <b>remove</b> them form your <code>MyStudentAL</code> and
 * <code>MyStudentAL</code> classes.
 * <br>
 * <ul>
 * <li><code>getAveScore()</code></li>
 * <li><code>getTopScore()</code></li>
 * <li><code>makeStudent()</code></li>
 * <li><code>open()</code></li>
 * <li><code>save()</code></li>
 * <li><code>getStudent()</code></li>
 * <li><code>remove()</code> - optional - this is <b>hard</b>.</li>
 * </ul>
 * <br>
 * These method can take advantage of the <code>iterator()</code> method that
 * is now part of <code>StudentCollectionIF</code>.
 */
public abstract class MyAbstractCollection implements StudentCollectionIF,
                                                      Iterable<StudentIF>
{
	
	public double getAveScore(){
		double result = 0;
		int n = 0;
		Iterator<StudentIF> rm = iterator();
		while(rm.hasNext()){
			StudentIF temp = rm.next();
			if(temp.getScore() != 0){
				result += temp.getScore();
			}
			n++;
		}
		if (n != 0){
			result = result / n;
		}
		
		return result;
	}
	
	public StudentIF getStudent(int id){
		StudentIF result = null, temp = null;
		Iterator<StudentIF> rm = iterator();
		while(rm.hasNext()){
			temp = rm.next();
			if(temp.getId() == id){
				result = temp;
				break;
			}
		}
		
		return result;
	}
	
	public double getTopScore(){
		double result = 0;
		Iterator<StudentIF> rm = iterator();
		while(rm.hasNext()){
			StudentIF temp = rm.next();
			if (temp.getScore() > result){
				result = temp.getScore();
			}
		}
		
		return result;
	}
	
	//This method's job is to take a string, interpret it, and create a MyStudent to return.
    public StudentIF makeStudent(String studentData) {

    	//Split the string, note the name, parse the ID, and create the grades array (initialize as empty).
    	String[] temp = studentData.split(" ");
    	String tempn = temp[0];
    	int tempID = Integer.parseInt(temp[1]);
    	int[] tempi = {};

    	if(tempn.substring(0, StudentIF.HonorIdentifier.length()).equals(StudentIF.HonorIdentifier)){
    		tempn = tempn.substring(StudentIF.HonorIdentifier.length(), tempn.length());
    		char tempp = 'Q';
    		if(temp.length > 3){
				tempp = temp[3].charAt(0);
				String[] tempg = temp[2].split(",");
	    		tempi = new int[tempg.length];
	    		int i;
	    		for (i = 0; i < tempg.length; i++){
	    			tempi[i] = Integer.parseInt(tempg[i]);
	    		}
	    		MyHonorStudent result = new MyHonorStudent(tempn, tempID, tempi, tempp);
    			return result;
			}
    		else if(temp.length > 2){
    			String[] tempg = temp[2].split(",");
    			if(tempg.length == 1 && (tempg[0].equals("A") || tempg[0].equals("B") || tempg[0].equals("C") || tempg[0].equals("D") || tempg[0].equals("F"))){
    				tempp = tempg[0].charAt(0);
    				MyHonorStudent result = new MyHonorStudent(tempn, tempID, tempi, tempp);
    				return result;
    			}
        		tempi = new int[tempg.length];
        		int i;
        		for (i = 0; i < tempg.length; i++){
        			tempi[i] = Integer.parseInt(tempg[i]);
        		}
        		MyHonorStudent result = new MyHonorStudent(tempn, tempID, tempi, tempp);
    			return result;
    		}
    		else{
    			MyHonorStudent result = new MyHonorStudent(tempn, tempID, tempi, tempp);
    			return result;
    		}
    	}
    	
    	//Because 'Assign6' creates students differently (doesn't pass grade info), this 'if' test determines whether or not 
    	//grade data was included. Its body creates the 'grades' array if the info was passed. This problem is also solvable by
    	//simply creating another constructor for MyStudent which takes only 'name' and 'ID' as parameters, and calling appropriately here.
    	if (temp.length > 2){
    		String[] tempg = temp[2].split(",");
    		tempi = new int[tempg.length];
    		int i;
    		for (i = 0; i < tempg.length; i++){
    			tempi[i] = Integer.parseInt(tempg[i]);
    		}
    	}
    	
    	MyStudent result = new MyStudent(tempn, tempID, tempi); //Finally, create a student based on this info,
    	return result;//and return it to the caller
    }
    
    
  //Opens files and imports the students they contain.
    public void open(String fileName) throws FileNotFoundException {
    	Scanner input = null;
    	File inny = new File(fileName);
    	if (inny.exists()){
    		clear();
    		try{
    			input = new Scanner(inny);
    		}
    		catch (FileNotFoundException e){
    		}
    		String temp = null;
    		StudentIF tmp = null;
    		while (input.hasNextLine()){//For each line in the file, 
    			temp = input.nextLine();//copy it,
    			tmp = makeStudent(temp);//use it to call 'makeStudent' method,
    			insort(tmp);//and send the result of 'makeStudent' to insort for adding to the AL
    		}
    		input.close();
    	}
    }
    
    public void save(String fileName) throws FileNotFoundException {
		String result = "";
		Iterator<StudentIF> rm = iterator();
		while(rm.hasNext()){
			result = result + rm.next().toString();
			if(rm.hasNext()){
				result = result + "\n";
			}
		}
		if(fileName == null){//If no filename was given, put it on screen
			System.out.println(result);
		}
		else{//If a filename was given, yadda yadda
			try {  //This output block is effectively copy-pasted from http://www.codecodex.com/wiki/Save_a_string_to_a_file
				PrintWriter out = new PrintWriter(new FileWriter(fileName));
				out.print(result);
				out.close();
			} catch (IOException e) {
			}
		}			
	}
	
	
}