//This class defines the Node objects which are used to build a linked list.

public class Node {
	
	//All the method really needs to hold is a particular student and the next assigned Node instance.
	private StudentIF data;
	private Node next;
	
	//Base constructor only ever called to create 'head'
	public Node(){
		data = null;
		next = null;
	}
	
	//Typical constructor employed by 'insort' method.
	public Node(StudentIF d){
		data = d;
		next = null;
	}
	
	
	//Get / set methods; pretty simple stuff.
	public void setNext(Node n){
		next = n;
	}
	public void setData(StudentIF s){
		data = s;
	}
	public Node getNext(){
		return next;
	}
	public StudentIF getData(){
		return data;
	}
}
