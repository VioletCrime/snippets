import java.util.ArrayList;
import java.util.Scanner;
import java.util.NoSuchElementException;
import java.io.FileNotFoundException;

/** This class provides code for testing the classes <code>MyXXX.java</code>
 * that you will complete. Do <b>NOT</b> change anything or add anything
 * to this class
 * <br>
 * Fritz Sieker
 */

public class Assign6 extends CmdInterpreter
{
   private final boolean newVersion = true;

   protected StudentCollectionIF sci;
   
   private void addScores (Scanner scan, StudentIF s) {
     int[] scores = this.getIntArray(scan);
     if (scores != null) {
       for (int i = 0; i < scores.length; i++) {
          boolean added = s.addGrade(scores[i]);
          CmdInterpreter.trace(scores[i] + " " + added);
       }
     }
   }
   
   /** A method to construct a StudentIF from a String. You may want something
    * similar for your <code>StudentCollectionIF.open()</code> method.
    */
   private StudentIF makeStudent (Scanner scan) {
     StudentIF s = null;
     if (newVersion) {
       if (scan.hasNextLine())
         s = sci.makeStudent(scan.nextLine().trim());
     }
     else if (scan.hasNext()) {
       String name = scan.next();
       String IDStr = scan.next();
       s = sci.makeStudent(name + " " + IDStr);
       addScores(scan, s);
     }
     return s;
   }
   
   private StudentIF simpleMakeStudent(Scanner scan) { // used by equal/compto
     StudentIF s = null;

     if (scan.hasNext()) {
       String name = scan.next();
       String IDStr = scan.next();
       s = sci.makeStudent(name + " " + IDStr);
     }
     return s;
   }
   
   /** Display a list of commands useful for testing the classes you will
    * complete for this assignment. Many of these commands only make sense
    * after a <code>MyUniverse</code> object has been created and populated.
    * The commands are:
    * <ul>
    * <li><b>add</b> create a new student and add to list</li>
    * <li><b>avg</b> print the average of the class</li>
    * <li><b>array</b> use <code>MyStudentAL</code> to store data</li>
    * <li><b>compto</b> exercise <code>compareTo()</code></li>
    * <li><b>equal</b> exercise <code>equals()</code></li>
    * <li><b>get</b> fetch a student by ID</li>
    * <li><b>grades</b> add one or more grades to a student</li>
    * <li><b>ith</b> get a student by the position in the list</li>
    * <li><b>link</b> use <code>MyStudentLL</code> to store data</li>
    * <li><b>open</b> populate the list from a file</li>
    * <li><b>rm</b> remove a student given the ID</li>
    * <li><b>save</b> store the list in a file</li>
    * <li><b>size</b> print the number of students in the collection</li>
    * <li><b>student</b> create a student, without storing</li>
    * <li><b>top</b> print the top score</li>
    * </ul>
    */
   public void showHelp() {
     super.showHelp();
     System.out.println("Assignment 6 commands:");
     System.out.println(" add [!]name id [[grade[,grade,...]] projScore]");
     System.out.println(" avg");
     System.out.println(" array");
     System.out.println(" equal <name1> <id1> [<name2> <id2>]");
     System.out.println(" compto <name1> <id1> <name2> <id2>]");
     System.out.println(" get <ID>");
     System.out.println(" grades <id> grade[[,grade,grade] [projGrade]");
     System.out.println(" ith <index>");
     System.out.println(" link");
     System.out.println(" open [fileName]");
     System.out.println(" print");
     System.out.println(" proj <ID> <projGrade>");
     System.out.println(" rm <ID>");
     System.out.println(" save [fileName]");
     System.out.println(" size");
     System.out.println(" student [!]name id [[grade[,grade,...]] projScore]");
     System.out.println(" top");
     System.out.println();
   }
   
   /** Process one command for this assignment.*/
   public void processOneCommand (String cmd, String params)
       throws FileNotFoundException, NoSuchElementException
   {
     Scanner scan = new Scanner(params);
     
     if (cmd.equals("add")) {
       StudentIF s = makeStudent(scan);
       System.out.println(sci.insort(s));
     }
     
     else if (cmd.equals("array")) {
       sci = new MyStudentAL();
     }
     
     else if (cmd.equals("avg")) {
       System.out.println(sci.getAveScore());
     }
     
     else if (cmd.equals("equal")) {
       StudentIF s1 = simpleMakeStudent(scan);
       System.out.println(s1.equals(simpleMakeStudent(scan)));
     }
     
     else if (cmd.equals("compto")) {
       StudentIF s1 = simpleMakeStudent(scan);
       StudentIF s2 = simpleMakeStudent(scan);
       int comp = s1.compareTo(s2);
       System.out.println((comp > 0) ? 1 : ((comp < 0) ? -1 : 0));
     }
     
     else if (cmd.equals("get")) {
       System.out.println(sci.getStudent(scan.nextInt()));
     }
     
     else if (cmd.equals("grades")) {
       StudentIF s = sci.getStudent(scan.nextInt());
       addScores(scan, s);
     }

     else if (cmd.equals("ith")) {
       System.out.println(sci.get(scan.nextInt()));
     }
     
     else if (cmd.equals("link")) {
      sci = new MyStudentLL();
     }

     else if (cmd.equals("print")) {
       int n = sci.size();
       for (int i = 0; i < n; i++)
         System.out.println(sci.get(i));
     }

     else if (cmd.equals("open")) {
       sci.open(this.getStringOrNull(scan));
     }
     
     else if (cmd.equals("rm")) {
       System.out.println(sci.remove(scan.nextInt()));
     }

     else if (cmd.equals("save")) {
       sci.save(this.getStringOrNull(scan));
     }

     else if (cmd.equals("size")) {
       System.out.println(sci.size());
     }

     else if (cmd.equals("student")) {
       StudentIF s = makeStudent(scan);
       System.out.println(s);
     }
     
     else if (cmd.equals("top")) {
       System.out.println(sci.getTopScore());
     }

     else {
       super.processOneCommand(cmd, params);
     }

     scan.close();
   }

   public static void main (String[] args) {
     Assign6 assign = new Assign6();
     assign.ooMain(args);
   }
}