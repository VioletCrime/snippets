import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;


public class Cities {
	
	//Default City ArrayList
	ArrayList<City> Ctz = new ArrayList<City>();
	
	//Calculation workspaces, all used for Assignment 5 calculations; ill-named for readability... =-/
	ArrayList<City> temp = new ArrayList<City>();//Used as an iteration testing object for Traveling salesman problem
	ArrayList<City> temp2 = new ArrayList<City>();//Used to hold best-permutation-yet for Traveling salesman problem
	
	ArrayList<City> temp3 = new ArrayList<City>();//Used to hold subsets (n,k) for testing for Compact Subset problem
	ArrayList<City> tempPi = new ArrayList<City>();//Used to hold subsets (k,2) for testing for Compact Subset problem
	ArrayList<City> temp4 = new ArrayList<City>();//Used to hold best-subset-yet for Compact Subset problem
	
	//Assignment 5 private member variables. They are set damn high in order to ensure the 'lower-than' tests succeed the first iteration.
	private static double shortest = 1000000000;
	private static double avgDist = 1000000000;	
	
	//Used to add city's to the result Cities objects w/o file
	public void add(int i, City N){
		Ctz.add(i, N);
	}
	
	//Assignment 4/5 requirement to add Cities via file.
	public void addFromFile(String filename)throws FileNotFoundException{
		Scanner input = null;
		File inny = new File(filename);
		try{
			input = new Scanner(inny);
		}
		catch (FileNotFoundException e){
			throw new Error();
		}
		int i = 0;
		while (input.hasNextLine()){
			String tmpName = input.next();
			double tmpX = Double.parseDouble(input.next());
			double tmpY = Double.parseDouble(input.next());
			City temp = new City(tmpName, tmpX, tmpY);
			Ctz.add(i, temp);
			i++;
			input.nextLine();
		}
		input.close();
	}
	
	//Personal method used for A4, carrying to A5; pretty simple
	public int size(){
		return Ctz.size();
	}
	
	//Returns the city found in Ctz object at index i
	public City get(int i){
		if (i < 0 || i > Ctz.size()){
			return null;
		}
		return Ctz.get(i);
	}
	
	//The toString for Cities objects
	public String toString(){
		String result = "           City Name    X     Y \n-------------------- ------ ------\n";
		int i = 0, j = Ctz.size();
		for (i = 0; i < j; i++){
			City tmpCity = Ctz.get(i);
			result = result + String.format("%20s", tmpCity.getName());
			result = result + String.format("%7.2f", tmpCity.getX());
			result = result + String.format("%7.2f", tmpCity.getY());
			result = result + "\n";
		}
		return result;
	}
	
	
	
	
	
	//Assignment 5 insanity starts here.
	
	
	//!!!Compact Subsets Problem
	
	public Cities findMostCompactSubset(int subsetSize){
		Cities result = new Cities();//Create Cities object that is to be returned
		for (int i = 0; i < subsetSize; i++){//Initialize temp3 and temp4; 99% of the reason is to ensure the proper size
			temp3.add(i, Ctz.get(i));
			temp4.add(i, Ctz.get(i));
		}
		tempPi.add(0, Ctz.get(0));
		tempPi.add(1, Ctz.get(1));
		ArrayList<int[]> subsets = makeSubsets(Ctz.size(), subsetSize); //Creating an ArrayList which contains all possible subsets of size 'k', calculated by called method.
		calcAvs(subsets, subsetSize);//main submethod method call; rabbit hole goes deeper than it should, but it works...
		if(temp4.size() > 0){
			for(int i = 0; i < subsetSize; i++){//Copy the tightest subset to the object to be returned.
				result.add(i, temp4.get(i));
			}
		}
		return result; //Take a guess what this does...
	}
	
	//Prepares variables and objects for the main subset creation method
	public ArrayList<int[]> makeSubsets(int size, int subsetSize){
		ArrayList<int[]> result = new ArrayList<int[]>();
		int[] subset = new int[subsetSize];
		makeSubsetsNao(size, subsetSize, 0, 0, subset, result);
		return result;
	}
	
	//Subset creation method; effectively copied directly from Recitation 8. What an algorithm!
	public void makeSubsetsNao(int size, int sss, int position, int low, int[] subset, ArrayList<int[]> result){
		int i;
		for (i = low; i <= size-sss+position; i++){
			subset[position] = i;
			if (position >= sss - 1){
				result.add(copyArray(subset));
			}
			else{
				makeSubsetsNao(size, sss, position + 1, i + 1, subset, result);
			}
		}
	}
	
	//Helper method for makeSubsetsNao method; creates a copy of the incoming int array and returns the copy.
	public int[] copyArray(int[] orig){
		int[] copy = new int[orig.length];
		int i;
		for(i = 0; i < orig.length; i++){
			copy[i] = orig[i];
		}
		
		return copy;
	}
	
	//Odd little fucker; calculates all subset avg dist's using only a couple nested loops. Hard to read.
	public void calcAvs(ArrayList<int[]> subsets, int subsetSize){
		int i, j, k, m; //All loop control variables... sheesh
		ArrayList<int[]> subsubs = makeSubsets(subsetSize, 2); //Generating all possible subsets of (n,2)
		
		for (i = 0; i < subsets.size(); i ++){ //This loop goes through every subset of the user-defined length
			int[] subseta = subsets.get(i); //Copying this iteration's particular subset
			double total = 0; //We want this to be zero going into the calculation loop, resetting here
			for (k = 0; k < subseta.length; k++){ //Setting temp3 to the prescribed subset
				temp3.set(k, Ctz.get(subseta[k]));
			}
			
			for (j = 0; j < subsubs.size(); j++){ //Goes through every iteration of (user-defined 'k', 2) subsets to calculate individual distances
				int[] subsuba = subsubs.get(j); //Getting this iterations pair of cities
				tempPi.set(0, temp3.get(subsuba[0]));//Setting city 1
				tempPi.set(1, temp3.get(subsuba[1]));//Setting city 2
				City tempC = tempPi.get(0); //Making the method call next line easier to write / read
				total += tempC.distanceBetween(tempPi.get(1)); //Summing the distances of each iteration for the macro-subset
				
			}
			total = total / subsubs.size();//now that we have the total distances between the cities, we average here
			if (total < avgDist){ //If it sets a record, log it.
				Cities.avgDist = total;
				for (m = 0; m < temp3.size(); m++){//Set temp4 to hold this subset to be copied to the resulting Cities object once calculations are complete
					temp4.set(m, temp3.get(m));
				}
			}
		}		
	}
	
	public double getMinAvgDistance(){
		return Cities.avgDist;//Method simply returns the private value which represents the average distance between the cities in the most compact subset.
	}
	
	//!!!!End Compact Subsets Problem
	
	
	//!!!!Traveling Salesman.
	
	//Initializes variable and values submethods require to work properly, and calls 'em. Returns the resulting Cities object.
	public Cities findShortestRoute(){
		Cities result = new Cities();//Create the object to be returned
		temp = new ArrayList<City>(); //initialize the temp ArrayList
		int i;
		for (i = 0; i < Ctz.size(); i++){ //Copy Ctz to both temp 1 and 2; the second just to ensure the correct number of iterations.
			temp.add(Ctz.get(i));
			temp2.add(Ctz.get(i));
		}
		srPermute(0); //Call srPermute function
		
		for (i = 0; i < temp.size(); i++){//temp2 holds the best route at this point; copy its contents to object 'result'
			result.add(i, temp2.get(i));
		}
		return result;
	}
	
	//Permutation algorithm. What a mindfuck. Copied almost verbatim from recitation 9
	public void srPermute(int from){
		int i;
		
		if (from == temp.size()-1){ //If the length is correct, calculate the path of this iteration.
			calcShort(temp); //Calculation method call, passing iteration
		}
		else{ //If the length isn't there quite yet, add another layer
			for (i = from; i < temp.size(); i++){
				swap(from, i); //Swap temp contents
				srPermute(from + 1); //Recursive call that makes this all work
				swap(i, from); //Swap contents back
			}
		}
	}
	
	//Calculates the route length of each of the permutations; records best.
	public void calcShort(ArrayList<City> temp){
		City a = temp.get(0);
		int i = 0, j = temp.size();
		double sum = a.distanceBetween(temp.get(temp.size() -1));//initialize sum as the distance between the last and first cities (harder for loop to do)
		for (i = 0; i < j - 1; i++){//sum the remainder of distances in order
			a = temp.get(i);
			sum += a.distanceBetween(temp.get(i + 1)); //Heavy lifter
		}
		if (sum < shortest){//If this is a record-setter, take note
			Cities.shortest = sum;
			for (i = 0; i < j; i++){
				temp2.set(i, temp.get(i));
			}
		}
		
	}
	
	//Creates a temp city and rearrange the files in temp using it.
	public void swap(int from, int i){
		City tmp = temp.get(from);
		temp.set(from,temp.get(i));
        temp.set(i,tmp);
		
	}
	
	//Easy method used to return the private variable that represents the shortest possible route's distance.
	public double getMinRouteDistance(){
		return Cities.shortest;
	}
	
	//!!!!End Traveling Salesman.
	
	
}//End File
