
public class City{

	private String name;
	private double x, y;
	
	public City(String inName, double ecks, double why){
		name = inName;
		x = ecks;
		y = why;
	}
	
	public String getName(){
		return this.name;
	}
	
	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;
	}
	public String toString(){
		String result = "";
		result = result + name + "(" + String.format("%.2f", x) + "," + String.format("%.2f", y) + ")";
		return result;
	}
	public double distanceBetween(City other){
		return  Math.sqrt(Math.pow(x - other.x,2) + Math.pow(y - other.y,2));//Taken directly from the assignment 4 solution
	}
}
