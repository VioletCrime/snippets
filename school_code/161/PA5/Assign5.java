import java.util.*;

public class Assign5 {

   public static void main(String args[]) {
      if (args.length < 2) {
         System.out.println("Usage: java Assign5  .");
         System.out.println("  Runs out of memory for colorado.cities. Try smallcolorado.cities.");
         System.exit(1);
      }
      String filename = args[0];
      int subsetSize = Integer.parseInt(args[1]);

      Cities cities = new Cities();
      try {
         cities.addFromFile(filename);
      } catch (java.io.FileNotFoundException e) {
         System.out.println("File " + filename + " not found.");
         System.exit(1);
      }

      System.out.println(cities);

      Cities mostCompactSubset = cities.findMostCompactSubset(subsetSize);
      double avgDistance = mostCompactSubset.getMinAvgDistance();

      System.out.println("Most compact subset, with average distance between cities of " + String.format("%.2f",avgDistance) + ", is:");
      System.out.println(mostCompactSubset);

      Cities shortest = cities.findShortestRoute();
      double minRouteDistance = shortest.getMinRouteDistance();
         System.out.println("Shortest path, with distance of " + String.format("%.2f",minRouteDistance) + ", is");
      System.out.println(shortest);
   }
}