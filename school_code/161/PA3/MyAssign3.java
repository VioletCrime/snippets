import java.io.File;
import java.util.ArrayList;


/** You will complete these public method in this class. They are:
 * <ul>
 * <li><code>repeat()</code></li>
 * <li><code>reverse()</code></li>
 * <li><code>list()</code></li>
 * <li><code>contains()</code></li>
 * <li><code>intersect()</code></li>
 * <li><code>diamond()</code></li>
 * <li><code>directory()</code></li>
 * </ul>
 * <p>It is likely that you will write additional helper methods to complete
 * the work. <b>No java loop constructs are allowed in this file! All work
 * must be done using recursion</b>. Nor may you add additional classes with
 * methods that use loops. You may <b>not</b> use the <code>ArrayList.contains()
 * </code> method.
 */
public class MyAssign3 extends Assign3
{
  private String indentStr = " "; // change to "-" for debugging directory

  public MyAssign3() { // just for completeness
    super();
  }

  public String repeat (String value, int count) {
	  if (value == null){
		  return "null";
	  }
	  String result = "";
	  if (count <= 0){
		  return result;
	  }
	  count--;
	  result += value + repeat(value, count);
	  return result;
  }

  public String reverse (String value) {
	  if (value == null){
		  return "null";
	  }
	  if (value.length() == 1){
		  return value;
	  }
	  char temp = value.charAt(value.length() -1);
	  String result = temp + reverse(value.substring(0, value.length() -1));
	  return result;
  }

  public void list (String[] list) {
	  if (list != null){
	  int max = list.length;
		printr(list, 0, max);
	  }
  }
  public void printr(String[] list, int i, int max){
		if (i < max){
			System.out.println(list[i]);
			printr(list, i + 1, max);
		}
  }

  public boolean contains (String[] list, String value) {
	  if (list == null){
		  return false;
	  }
	  if (value == null){
		  return false;
	  }
	  int max = list.length;
	  boolean result = stationary(list, value, max, 0);
	  return result;
  }
  public static boolean containsTwo (String[] list, String value) {
	  int max = list.length;
	  boolean result = stationary(list, value, max, 0);
	  return result;
  }
  public static boolean stationary(String[] list, String value, int max, int i){
		boolean result = false;
		if (i + 1 < max)
			if(stationary(list, value, max, i + 1)){
				result = true;
				return true;
			}
		if(value.equals(list[i])){
			result = true;
			return true;
		}
		
		return result;
	}
   
  public ArrayList<String> intersect(String[] list1, String[] list2) {
	  ArrayList<String> result = new ArrayList<String>();
	  if(list1 == null || list2 == null || list1.length == 0 || list2.length == 0){
		  return result;
	  }
		int aSize = list1.length, bSize = list2.length;
		halfway(list1, list2, aSize, bSize, 0, result);
		return result;
	}
	public static void halfway(String[] a, String[] b, int aSize, int bSize, int i, ArrayList<String> result){
		if (i < aSize){
			if (containsTwo(b, a[i])){
				result.add(a[i]);
			}
			if (i + 1 < aSize){
				halfway(a, b, aSize, bSize, i + 1, result);
			}
		}	
	}
  
  public void diamond (int size) {
	  if (size > 0){
		  tear(size, 0);
		  build(size, 1);
	  }
  }
  public void tear(int size, int i){
	  if (i <= size){
		  String result = printStar(size - i) + printBlank(i);
		  System.out.println(result + " " + reverse(result));
		  tear(size, i + 1);
	  }
  }
  public void build(int size, int i){
	  if (i <= size){
		  String result = printStar(i) + printBlank(size - i);
		  System.out.println(result + " " + reverse(result));
		  build(size, i + 1);
	  }
  }
  public String printStar(int count){
	  if (count == 0){
		  return "";
	  }
	  String result = "*" + printStar(count - 1);
	  return result;
  }
  public String printBlank(int blanks){
	  if (blanks == 0){
		  return "";
	  }
	  String result = " " + printBlank(blanks - 1);
	  return result;
  }
  
  
  
  
  public void directory (String fileName, int indent, int maxLevel) {
	  if (fileName != null){	  
		  File f = new File(fileName);
		  if(!f.exists()){
			  System.out.println(f + ": No such file or directory");
		  }
		  else if(f.isFile()){
			  System.out.println(f.getName());
		  }
		  else if (f.isDirectory()){
			  printDir(f, 0);
			  if(maxLevel > 0){
				  lista(f, indent, maxLevel, 1, 0);
			  }
		  }
	  }
	 else{
		  System.out.println("null: No such file or directory");
	  }
  }
  public void lista(File f, int indent, int maxLevel, int cLevel, int cItem){
	  File[] chillun = f.listFiles();
	  if (chillun.length != 0){
		  if (chillun[cItem].isDirectory()){
			  printDir(chillun[cItem], indent * cLevel);
			  if(cLevel + 1 < maxLevel){
				  lista(chillun[cItem], indent, maxLevel, cLevel + 1, 0);
			  }
		  }
		  if(chillun[cItem].isFile()){
			  printFile(chillun[cItem], indent * cLevel);
		  }
		  if (cItem + 1 < chillun.length){
			  lista(f, indent, maxLevel, cLevel, cItem + 1);
	 	}
	  }
  }
	  
  public void printDir(File x, int i){
	  System.out.println(moreBlanks(i) + x.getName() + "/");
  }
  public void printFile(File x, int i){
	  System.out.println(moreBlanks(i) + x.getName());
  }
  public String moreBlanks(int i){
	  String result ="";
	  if (i == 0){
		  return result;
	  }
	  result = " " + moreBlanks(i - 1);
	  return result;
  }
  
  
  
  public static void main (String[] args) {
    MyAssign3 ma3 = new MyAssign3();
    ma3.ooMain(args);
  }
}