/**
 * CS 200 Colorado State University, Fall 2011
 * This is the base class for PA5. 
 * DO NOT MODYFY main(). Grading will be based on main() of this class.
 * For more information, http://www.cs.colostate.edu/~cs200/Fall11/assignments/PA5/PA5.html
 */
import java.util.LinkedList;
public class PA5{
    String input_file;
    NewsFeed myNewsFeed;
    InformationParser ip;

    public PA5(String _input_file){
		ip = new InformationParser(_input_file);	
		myNewsFeed = ip.readMembers();

    }


    public void printNewsFeed(String memberID, int begin, int count){	
		myNewsFeed.doIt1(memberID, begin, count);
   }

    
    public void printFullThreadOfNewsFeed(String memberID, int begin, int count){
    	myNewsFeed.doIt2(memberID, begin, count);
    }
    

    //NOTE: DO NOT MODIFY main().
    public static void main(String args[]){
		if (args.length < 5){
			System.out.println("Please enter your input file and command.");
			System.out.println("e.g. java PA5 [input_file][command1][command2][command3]");
		}else{
	    
			String input_file = args[0];
			String cmd = args[1];
	    

			PA5 pa5 = new PA5(input_file);
			int type_size = 0;
		
			if (cmd.equals("printNewsFeed")){
				pa5.printNewsFeed(args[2],Integer.parseInt(args[3]), Integer.parseInt(args[4]));
			}else if(cmd.equals("printFullThreadOfNewsFeed")){
				pa5.printFullThreadOfNewsFeed(args[2],Integer.parseInt(args[3]), Integer.parseInt(args[4]));
			}
	
	    
		}	
    }
}