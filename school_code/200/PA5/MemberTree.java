/**
 * CS 200 Colorado State University, Fall 2011
 */

public class MemberTree{
    private MemberNode root = null;

    public MemberTree(){//No need to initialize 'root' here: it will be taken care of on the first call of 'insertMember'
    }

    public MemberNode getRoot(){
		return root;
    }

    public void setRoot(MemberNode _root){
    	root = _root;
    }
	
    public void insertMember(MemberNode newMember){//Only tests and works for the base case (root is null); calls 'insert' otherwise.
    	if (root == null){//If root is null...
    		root = newMember; //...the new member becomes the root node.
    	}
    	else{//Otherwise we need to employ a recursively-calling function to traverse the tree looking for the appropriate spot for newmember
    		insert(newMember, root);
    	}
    }
    //Extension of 'insertMember'. Recursively traverses the tree looking for the correct spot to place newMember.
    public void insert(MemberNode newMember, MemberNode currentMember){
    	if (newMember.getItem().getUserID().compareTo(currentMember.getItem().getUserID()) < 0){//If 'new' comes before 'current'...
    		if (!currentMember.hasLeft()){//...and 'current' has no left child...
    			currentMember.setLeftChild(newMember);//...set newMember to be the left child...
    			return;//...and return.
    		}
    		else{//But if current has a left child...
    			insert (newMember, currentMember.getLeftChild());//...recursively call 'insert' with the appropriate MemberNode.
    		}
    	}
    	
    	if (newMember.getItem().getUserID().compareTo(currentMember.getItem().getUserID()) > 0){//Same as the above block, going to the right.
    		if (!currentMember.hasRight()){
    			currentMember.setRightChild(newMember);
    			return;
    		}
    		else{
    			insert (newMember, currentMember.getRightChild());
    		}
    	}
    }

    //Kingpin function; employs 2-4 recursive function calls to implement the removeNode algorithm
    public MemberNode removeMember(String searchKey){
		MemberNode save = retrieveMemberInfo(searchKey, root); //First, find and save the node to be whacked in order to save the children.
		kill(searchKey, root);//Next, we go in and remove the MemberNode.
		
		if (save.hasLeft()){//Finally, if the MemberNode had children, we reinsert them into the tree.
			reinsert(save.getLeftChild());
		}
		if (save.hasRight()){
			reinsert(save.getRightChild());
		}
		return save;//Return the member we whacked.
    }

	    
    public MemberNode retrieveMemberInfo(String searchKey, MemberNode compnode){
		if (compnode.getItem().getUserID().equals(searchKey)){//If 'compnode' is the one we're looking for...
			return compnode;//...return it.
		}
		if (searchKey.compareTo(compnode.getItem().getUserID()) < 0){//Otherwise, if what we're looking for is to the left...
			return retrieveMemberInfo(searchKey, compnode.getLeftChild());//...left we shall go.
		}
		if (searchKey.compareTo(compnode.getItem().getUserID()) > 0){//Same as above, to the right this time.
			return retrieveMemberInfo(searchKey, compnode.getRightChild());
		}
		return null;//base case to keep Ecplise happy; unreachable.
    }
    
    public void kill(String searchKey, MemberNode compnode){//The executioner function; nullifies the proper Node.
    	if (compnode.getLeftChild().getItem().getUserID().equals(searchKey)){//If the left child is the marked Node...
    		compnode.setLeftChild(null);//...set it's parent Node's left child to null...
    		return;//...and bugger off to the bar for a pint.
    	}
    	if (compnode.getRightChild().getItem().getUserID().equals(searchKey)){//Same as above, going right this time.
    		compnode.setRightChild(null);
    		return;
    	}
    	if (searchKey.compareTo(compnode.getItem().getUserID()) < 0){//If the children aren't what we need to find...
			kill(searchKey, compnode.getLeftChild());//recursively work down to the left or right, appropriately.
		}
    	if (searchKey.compareTo(compnode.getItem().getUserID()) > 0){
    		kill(searchKey, compnode.getRightChild());
    	}
    }
    
    public void reinsert (MemberNode m){//Used to reintegrate children of nullified parents back into society (the tree).
    	insert(m, root);//First, insert the parent node...
    	if(m.hasLeft()){//...then recursively insert its left children...
    		reinsert(m.getLeftChild());
    	}
    	if(m.hasRight()){//...followed by its right children.
    		reinsert(m.getRightChild());
    	}
    }

    
//===============================================OUTPUT METHODS========================================================//
    
    //InOrder Traverse  
    public void printMemberTreeInOrder(){//Method-handling function for 'memberTreeInOrder(MemberNode)'
    	memberTreeInOrder(root);
    	System.out.println();
    }
    
    public void memberTreeInOrder(MemberNode start){	
    	if(start.hasLeft()){ //First we go as far left as possible...
    		memberTreeInOrder(start.getLeftChild());
    	}
    	System.out.print(start.getItem().getUserID() + " "); //...then print the parent node...
    	if(start.hasRight()){//...then we go as far right as possible.
    		memberTreeInOrder(start.getRightChild());
    	}
    }

    //PreOrder Traverse
    public void printMemberTreePreOrder(){//Method-handling function for 'memberTreePreOrder(MemberNode)'
    	memberTreePreOrder(root);
    	System.out.println();
    }
    
    public void memberTreePreOrder(MemberNode start){
    	System.out.print(start.getItem().getUserID() + " ");//Print the parent node first...
    	if(start.hasLeft()){//...then, if the node has a left child...
    		memberTreePreOrder(start.getLeftChild());//...travel to it, printing the subtree out.
    	}
    	if(start.hasRight()){//Ditto for right subtrees.
    		memberTreePreOrder(start.getRightChild());
    	}
    }

    //PostOrder Traverse
    public void printMemberTreePostOrder(){//Method-handling function for 'memberTreePostOrder(MemberNode)'
    	memberTreePostOrder(root);
    	System.out.println();
    }
    
    public void memberTreePostOrder(MemberNode start){
    	if(start.hasLeft()){//Start all the way to the left...
    		memberTreePostOrder(start.getLeftChild());
    	}
    	if(start.hasRight()){//...working as far right as possible...
    		memberTreePostOrder(start.getRightChild());
    	}
    	System.out.print(start.getItem().getUserID() + " ");//...printing the parent nodes only after all children have printed.
    }
     
   
} //End MemberTree class