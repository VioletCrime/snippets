import java.io.*;
import java.util.*;

// Modify your InformationParser class
public class InformationParser{

	Scanner inny = null;
	String[] temp = null;
	
    public InformationParser(String _filelocation){
    	inny = null; //Input scanner
		File inStuff = new File(_filelocation); //File to input
		
		
		try{ //Trying to hand the file to the input scanner. doesn't seem to properly catch =-/
			inny = new Scanner(inStuff);
		}
		catch (FileNotFoundException e){
			System.out.println("File not found.");
		}
    }
    public NewsFeed readMembers(){//Magic. Held together with hope and duct tape. Seriously.
    	
		NewsFeed tallBoy = new NewsFeed(); //Initialize a new member tree to return.
		temp = inny.nextLine().trim().split(" "); //Get the first line before entering the main loop
		//prntStrng(temp);
		
		while (inny.hasNextLine()){ //While we have not hit the end of file...
			Member dude = new Member(temp[1], temp[2], temp[3]); //We only get here when the current line holds member information; make the object.
			EdgeStack es = new EdgeStack(); //Initialize a EdgeStack for this member.
			temp = inny.nextLine().trim().split(" ");//Get the next line
			//prntStrng(temp);
			
			while (temp[0].equals("friend")){//Short loop used to input Friend information
				dude.addFriend(temp[1], Integer.parseInt(temp[2])); //Add a new friend to this member's list, using given information.
				temp = inny.nextLine().trim().split(" ");//Get the next line
				//prntStrng(temp);
			}
			
			if (temp.length != 1){ //If the line isn't empty...
				while (temp.length != 1){ //...then while successful lines aren't empty...
					Edge tempE = new Edge(temp[1], Integer.parseInt(temp[2])); //...make a new edge...
					temp = inny.nextLine().trim().split(" "); //...and get the next line housing the PostingObject data.
					//prntStrng(temp);
					NFObject from;
					if (Integer.parseInt(temp[2]) == 0){//If it's a 'visit' or 'like', we call a special NFObject constructor.
						from = new NFObject(temp[1], 0, temp[3], Long.parseLong(temp[4]));
					}
					else if (Integer.parseInt(temp[2]) == 5){
						from = new NFObject(temp[1], Integer.parseInt(temp[2]), temp[3], Long.parseLong(temp[4]));
					}
					else{ //If it's not either a 'visit' or 'like', call the usual NFObject constructor
						from = new NFObject(temp[1], Integer.parseInt(temp[2]), temp[3], temp[4], Long.parseLong(temp[5]));
					}
					tempE.setPostingObject(from); //Set the NFObject 'from' as edge's PostingObject
					temp = inny.nextLine().trim().split(" "); //Get the next line; we already know this has to be TargetObject.
					//prntStrng(temp);
					NFObject to = new NFObject(temp[1], Integer.parseInt(temp[2]), temp[3], temp[4], Long.parseLong(temp[5])); //Simple constructor call
					tempE.setTargetObject(to); 
					//tempE.calcScore(); //Calculate the Edge's *individual* score. Cumulative score is calculated by Member.bigCalc()
					es.push(tempE);//Add edge to the edgestack
					temp = inny.nextLine().trim().split(" ");
					//prntStrng(temp);
				}
				dude.setEdgeStack(es);//Attribute the edgestack to member 'dude'
				//dude.bigCalc();//Sum the scores of all related edges for each edge
				//dude.sortScore();//Sort the edgestack by score.
				temp = inny.nextLine().trim().split(" "); //Get the next line
				//prntStrng(temp);
				while (temp.length == 1){//If the next line is empty, get the next one, testing for EOF
					if (inny.hasNextLine()){
						temp = inny.nextLine().trim().split(" ");
						//prntStrng(temp);
					}
					else if (!inny.hasNextLine()){ //If it's the end of the file, break the cycle.
						break;
					}
				}
			}
			else{//same as the while(if-else) just above; apparently never entered, but seems I would want this one here?
				while (temp.length == 1){
					if (inny.hasNextLine()){
						temp = inny.nextLine().trim().split(" ");
						//prntStrng(temp);
					}
					else if (!inny.hasNextLine()){
						break;
					}
				}
			}
			MemberNode dudeNode = new MemberNode(dude, null, null); //Once the member is made, put the member in a MemberNode...
			tallBoy.insertMember(dudeNode); //... and insert it into the tree.
		}
		//tallBoy.hugeCalc();
		return tallBoy; //All done!
    }
    
    public void prntStrng(String[] in){ //Method used for debugging the readMembers method; prints the current input line (String[] temp, above)
    	for(int i = 0; i < in.length; i++){
    		System.out.print(in[i] + " ");
    	}
    	//System.out.println();
    	System.out.println("(" + temp.length + ")");;
    	//prntStrng(temp);  <----- uncomment and use above as necessary. DELETE WHEN DONE FUKKR! O.O
    }
        
}