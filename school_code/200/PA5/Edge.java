/**
 * CS 200 Colorado State University, Fall 2011
 */

public class Edge {

	private String edgeID;
	private int type;
	private NFObject postingObject;
	private NFObject targetObject;
	private int edgerank = 0;
	
	//constructor
	public Edge(String _edgeID, int _type){
		edgeID = _edgeID;
		type = _type;
	}
	
	public String getEdgeID(){
		return edgeID;
	}
	
	public int getEdgeType(){
		return type;
	}
	
	public NFObject getPostingObject(){
		return postingObject;
	}
	
	public NFObject getTargetObject(){
		return targetObject;
	}
		
	public void setType(int _type){
		type = _type;
	}
	
	public void setPostingObject(NFObject posting){
		postingObject = posting;
	}

	public void setTargetObject(NFObject target){
		targetObject = target;
	}
	
	public int getEdgeRank(){
		return edgerank;
	}
	
	public void setEdgeRank(int a){
		edgerank = a;
	}
	
	public int getWeight(){//Calculates the edge's individual 'edgerank' score.
		int weight = 0; 
		if (postingObject.getType() == 0) //Calculate 'weight' for the 'score' calculation
			weight = 1;
		else if(postingObject.getType() == 1)
			weight = 4;
		else if(postingObject.getType() == 2)
			weight = 3;
		else if(postingObject.getType() == 4)
			weight = 4;
		else if(postingObject.getType() == 5)
			weight = 2;
		//edgerank = 3 * postingObject.getTimeDecay() * weight; //Affinity (3 for PA2) * timeDecay * weight
		return weight;
	}
}