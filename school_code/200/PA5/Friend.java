
public class Friend {
	String ID;
	int affinity;
	
	public Friend(String eyeD, int Affinty){
		ID = eyeD;
		affinity = Affinty;
	}
	
	public String getID(){
		return ID;
	}
	
	public int getAffinity(){
		return affinity;
	}
	
	public void setAffinity(int in){
		affinity = in;
	}
	
	public void setID(String in){
		ID = in;
	}
}