member eh0721 Earnest Hemingway
friend bettyfriedan 3
friend remerson 4
friend nhawth 6
friend ar3090 6
edge 1001 4
postingobject 1002 1 eh0721 Message_for_Object1002 758
targetobject 56 3 jambs http://www.cs200classmatebook.org/~jmbs 76823
edge 1003 4
postingobject 1004 4 eh0721 Message_for_Object1004 755
targetobject 1 3 eh0721 http://www.cs200classmatebook.org/~eh0721 88960
edge 1008 4
postingobject 1009 4 eh0721 Message_for_Object1008 699
targetobject 1 3 eh0721 http://www.cs200classmatebook.org/~eh0721 88960
edge 1009 4
postingobject 1010 1 eh0721 Message_for_Object1102 697
targetobject 3 3 ar3090 http://www.cs200classmatebook.org/~ar3090 97869
edge 1188 1
postingobject 1189 0 eh0721 682
targetobject 1186 4 bettyfriedan Message_for_Object1186 685

member jambs James Baldwin
friend bettyfriedan 8
friend remerson 7
friend mtwain 4
edge 1190 4
postingobject 1191 4 jambs Message_for_Object1191 680
targetobject 56 3 jambs http://www.cs200classmatebook.org/~jmbs 76823
edge 1191 3
postingobject 1192 2 jambs Comment_for_Object1192 678
targetobject 1186 4 bettyfriedan Message_for_Object1186 685
edge 1091 4
postingobject 1092 1 jambs Wall-posting_for_Object1092 280
targetobject 50 3 bettyfriedan http://www.cs200classmatebook.org/~bettyfriedan 76990
edge 1197 3
postingobject 1198 2 jambs Comment_for_Object1198 556
targetobject 1291 4 remerson Message_for_Object1291 670
edge 1298 3
postingobject 1299 2 jambs Comment_for_Object1299 512
targetobject 1291 4 remerson Message_for_Object1291 670

member bettyfriedan Betty Friedan
friend eh0721 4
friend dh3136 9
friend jambs 7
friend remerson 1
friend mtwain 4
edge 1185 4
postingobject 1186 4 bettyfriedan Message_for_Object1186 685
targetobject 50 3 bettyfriedan http://www.cs200classmatebook.org/~bettyfriedan 76990
edge 2001 2
postingobject 2002 5 bettyfriedan 675
targetobject 1191 4 jambs Message_for_Object1191 680
edge 1040 2
postingobject 1041 5 bettyfriedan 200
targetobject 1009 4 eh0721 Message_for_Object1008 699

member dh3136 David Halberstam
friend bettyfriedan 9
edge 1499 4
postingobject 1500 4 dh3136 Message_for_Object1500 500
targetobject 36 3 db3136 http://www.cs200classmatebook.org/~dh3136 77000
edge 1561 4
postingobject 1562 1 dh3136 Message_for_Object1562 449
targetobject 50 3 bettyfriedan http://www.cs200classmatebook.org/~bettyfriedan 76990
edge 1566 4
postingobject 1566 1 dh3136 Message_for_Objec1566 423
targetobject 50 3 bettyfriedan http://www.cs200classmatebook.org/~bettyfriedan 76990
edge 2588 4
postingobject 2589 1 dh3136 Message_for_Objec2589 299
targetobject 50 3 bettyfriedan http://www.cs200classmatebook.org/~bettyfriedan 76990

member remerson Ralph Emerson
friend eh0721 9
friend jambs 5
friend bettyfriedan 4
friend nhawth 4
edge 1290 4
postingobject 1291 4 remerson Message_for_Object1291 670
targetobject 140 3 remerson http://www.cs200classmatebook.org/~remerson 56433
edge 1845 3
postingobject 1846 2 remerson Comment_for_Object1846 655
targetobject 1191 4 jambs Message_for_Object1191 680
edge 2110 4
postingobject 2111 4 remerson Message_for_Object2111 320
targetobject 140 3 remerson http://www.cs200classmatebook.org/~remerson 56433
edge 2130 4
postingobject 2131 4 remerson Message_for_Object2131 311
targetobject 140 3 remerson http://www.cs200classmatebook.org/~remerson 56433

member nhawth Nathaniel Hawthorne
friend eh0721 6
friend remerson 5
friend 1372William 5
edge 1380 4
postingobject 1381 4 nhawth Message_for_Object1381 665
targetobject 89 3 nhawth http://www.cs200classmatebook.org/~nhawth 66543

member mtwain Mark Twain
friend jambs 9
friend bettyfriedan 6
friend 1372William 4
edge 1846 3
postingobject 1847 2 mtwain Comment_for_Object1847 656
targetobject 1191 4 jambs Message_for_Object1191 680
edge 1198 4
postingobject 1199 4 mtwain Message_for_Object1199 702
targetobject 45 3 mtwain http://www.cs200classmatebook.org/~mtwain 87799
edge 2099 4
postingobject 2100 4 mtwain Message_for_Object2100 367
targetobject 45 3 mtwain http://www.cs200classmatebook.org/~mtwain 87799
edge 2456 4
postingobject 2457 4 mtwain Message_for_Object2457 344
targetobject 45 3 mtwain http://www.cs200classmatebook.org/~mtwain 87799
edge 2787 4
postingobject 2788 4 mtwain Message_for_Object2788 75
targetobject 45 3 mtwain http://www.cs200classmatebook.org/~mtwain 87799
edge 1193 2
postingobject 1194 5 mtwain 680
targetobject 1186 4 bettyfriedan Message_for_Object1186 685
edge 1788 1
postingobject 1789 0 mtwain 120
targetobject 1092 1 jambs Wall-posting_for_Object1092 280
edge 2003 2
postingobject 2004 5 mtwain 120
targetobject 1191 4 jambs Message_for_Object1191 680

member 1372William William Faulkner
friend mtwain 5
friend nhawth 4
edge 1652 1
postingobject 1653 0 1372William 370
targetobject 1381 4 nhawth Message_for_Object1381 665
edge 1682 1
postingobject 1683 0 1372William 200
targetobject 2457 4 mtwain Message_for_Object2457 344

member ar3090 Ayn Rand
friend eh0721 8
friend willr 7
friend lf8203 6
edge 1898 4
postingobject 1899 4 ar3090 Message_for_Object1898 598
targetobject 3 3 ar3090 http://www.cs200classmatebook.org/~ar3090 97869

member epyle Ernie Pyle
friend willr 7
friend theodore789 6
friend theodore1 8
edge 1379 4
postingobject 1380 4 epyle Message_for_Object1380 623
targetobject 98 3 epyle http://www.cs200classmatebook.org/~epyle 88790
edge 2468 4
postingobject 2469 4 epyle Message_for_Object2469 553
targetobject 98 3 epyle http://www.cs200classmatebook.org/~epyle 88790
edge 2479 4
postingobject 2480 4 epyle Message_for_Object2480 426
targetobject 98 3 epyle http://www.cs200classmatebook.org/~epyle 88790
edge 2501 4
postingobject 2502 4 epyle Message_for_Object2502 411
targetobject 98 3 epyle http://www.cs200classmatebook.org/~epyle 88790
edge 2566 4
postingobject 2567 4 epyle Message_for_Object2567 401
targetobject 98 3 epyle http://www.cs200classmatebook.org/~epyle 88790

member sinclair Upton Sinclair
friend theodore789 6
friend willr 7

member theodore789 Theodore Roosevelt
friend epyle 5
friend sinclair 7
edge 1670 4
postingobject 1671 4 theodore789 Message_for_Object1671 776
targetobject 13 3 theodore789 http://www.cs200classmatebook.org/~theodore789 91211
edge 1674 4
postingobject 1675 4 theodore789 Message_for_Object1675 773
targetobject 13 3 theodore789 http://www.cs200classmatebook.org/~theodore789 91211
edge 1871 4
postingobject 1872 4 theodore789 Message_for_Object1872 509
targetobject 13 3 theodore789 http://www.cs200classmatebook.org/~theodore789 91211
edge 1873 4
postingobject 1874 4 theodore789 Message_for_Object1874 501
targetobject 13 3 theodore789 http://www.cs200classmatebook.org/~theodore789 91211
edge 2630 3
postingobject 2631 2 theodore789 Comment_for_Object2631 398
targetobject 2502 4 epyle Message_for_Object2502 411 

member theodore1 Theodore Dreiser
friend epyle 9
friend lf8203 6
edge 2629 3
postingobject 2630 2 theodore1 Comment_for_Object2630 399
targetobject 2502 4 epyle Message_for_Object2502 411 

member willr Will Rogers
friend ar3090 7
friend sinclair 5
friend epyle 6
friend lf8203 7
edge 2599 3
postingobject 2600 2 willr Comment_for_Object2600 400
targetobject 2502 4 epyle Message_for_Object2502 411 
edge 2889 4
postingobject 2890 4 willr Message_for_Object2890 356
targetobject 760 3 willr http://www.cs200classmatebook.org/~willr 12999
edge 4680 4
	 postingobject 4681 4 willr Message_for_Objec4681 98
targetobject 760 3 willr http://www.cs200classmatebook.org/~willr 12999

member lf8203 Langston Hughes
friend ar3090 8
friend theodore1 8
friend willr 8
edge 1498 4
postingobject 1499 4 lf8203 Message_for_Object1499 578
targetobject 340 3 lf8203 http://www.cs200classmatebook.org/~lf8203 23800
edge 1513 4
postingobject 1514 4 lf8203 Message_for_Object1514 432
targetobject 760 3 lf8203 http://www.cs200classmatebook.org/~lf8203 23800





