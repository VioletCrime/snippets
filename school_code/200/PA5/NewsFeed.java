/**
 * CS 200 Colorado State University, Fall 2011
 */


import java.util.*;
public class NewsFeed extends MemberInfo{

	Member dude;
	EdgeStack dudeFriends;
	String[] Friends;
	LinkedList<Edge> friendEdges;
	LinkedList<Edge> edgeList;	
	String eyeD;
	int runningTotal = 0;

	public NewsFeed(){
	}

	// Using this method is optional. Based on your class structure, design your own 
	// methods and classes.
	// memberID: viewer's ID
	// begin: index of the first item retrieved from the sorted list.
	// count: number of consecutive items retrieved from the sorted list.
	// For more description, http://www.cs.colostate.edu/~cs200/Fall11/assignments/PA5/PA5.html
	/*public LinkedList <NFObject> generateNewsFeed(String memberID, int begin, int count){
			// add your code here
    }*/

	public void doIt1(String memberID, int begin, int count){
		dude = retrieveMemberInfo(memberID, getRoot()).getItem();
		dudeFriends = dude.getEdgeStack();
		Friends = dude.getListOfMyFriends();
		eyeD = memberID;
		friendEdges = getFriendsEdges(memberID);
		EdgeStack interim = new EdgeStack();
		hugeCalc();

		if (dudeFriends != null){
			while (!dudeFriends.isEmpty()){
				Edge temp = dudeFriends.pop();
				if(temp.getEdgeType() == 4 && !isPresent(temp, interim)){
					interim.push(temp);
				}
			}
		}

		if(friendEdges != null){
			while (!friendEdges.isEmpty()){
				Edge temp = friendEdges.pop();
				if(temp.getEdgeType() == 4 && !isPresent(temp, interim)){
					interim.push(temp);
				}
				else if (temp.getEdgeType() == 3 && !isPresent(temp, interim)){
					EdgeStack newES = retrieveMemberInfo(temp.getTargetObject().getCreator(), getRoot()).getItem().getEdgeStack().klone();
					if(newES != null){
						while(!newES.isEmpty()){
							Edge temp47 = newES.pop();
							if(temp47.getPostingObject().getObjectID().equals(temp.getTargetObject().getObjectID())){
								interim.push(temp47);
								break;
							}
						}
					}
				}
			}
		}

		interim = dude.sortScore(interim);
		edgeList = toList(interim);

		int i = 0;
		int j = 0;
		while (i < edgeList.size()){
			Edge temp = edgeList.get(i);
			if (i < begin){
				i++;
				continue;
			}

			else if(temp.getEdgeType() == 3 && j < count){
				NFObject tempNFO = temp.getPostingObject();
				String bod = tempNFO.getBody();
				if(bod == null){
					System.out.println(tempNFO.getObjectID() + " " + tempNFO.getType() + " " + tempNFO.getCreator() + " " + tempNFO.getTimestamp() + " " + temp.getEdgeRank());
				}
				else{
					System.out.println(tempNFO.getObjectID() + " " + tempNFO.getType() + " " + tempNFO.getCreator() + " " + bod + " " + tempNFO.getTimestamp() + " " + temp.getEdgeRank());
				}
			}

			else if(temp.getEdgeType() == 4 && j < count){
				NFObject tempNFO = temp.getPostingObject();
				String bod = tempNFO.getBody();
				if(bod == null){
					System.out.println(tempNFO.getObjectID() + " " + tempNFO.getType() + " " + tempNFO.getCreator() + " " + tempNFO.getTimestamp() + " " + temp.getEdgeRank());
				}
				else{
					System.out.println(tempNFO.getObjectID() + " " + tempNFO.getType() + " " + tempNFO.getCreator() + " " + bod + " " + tempNFO.getTimestamp() + " " + temp.getEdgeRank());
				}
			}
			i++;
			j++;
		}
	}

	public void doIt2(String memberID, int begin, int count){
		dude = retrieveMemberInfo(memberID, getRoot()).getItem();
		dudeFriends = dude.getEdgeStack();
		Friends = dude.getListOfMyFriends();
		eyeD = memberID;
		friendEdges = getFriendsEdges(memberID);
		EdgeStack interim = new EdgeStack();
		hugeCalc();

		if (dudeFriends != null){
			while (!dudeFriends.isEmpty()){
				Edge temp = dudeFriends.pop();
				if(temp.getEdgeType() == 4 && !isPresent(temp, interim)){
					interim.push(temp);
				}
			}
		}

		if(friendEdges != null){
			while (!friendEdges.isEmpty()){
				Edge temp = friendEdges.pop();
				if(temp.getEdgeType() == 4 && !isPresent(temp, interim)){
					interim.push(temp);
				}
				else if (temp.getEdgeType() == 3 && !isPresent(temp, interim)){
					EdgeStack newES = retrieveMemberInfo(temp.getTargetObject().getCreator(), getRoot()).getItem().getEdgeStack().klone();
					if(newES != null){
						while(!newES.isEmpty()){
							Edge temp47 = newES.pop();
							if(temp47.getPostingObject().getObjectID().equals(temp.getTargetObject().getObjectID())){
								interim.push(temp47);
								break;
							}
						}
					}
				}
			}
		}

		interim = dude.sortScore(interim);
		edgeList = toList(interim);

		int i = 0;
		int j = 0;
		while (i < edgeList.size()){
			Edge temp = edgeList.get(i);
			if (i < begin){
				i++;
				continue;
			}

			else if(temp.getEdgeType() == 3 && j < count){
				NFObject tempNFO = temp.getPostingObject();
				String bod = tempNFO.getBody();
				if(bod == null){
					System.out.println(tempNFO.getObjectID() + " " + tempNFO.getType() + " " + tempNFO.getCreator() + " " + tempNFO.getTimestamp() + " " + temp.getEdgeRank());
				}
				else{
					System.out.println(tempNFO.getObjectID() + " " + tempNFO.getType() + " " + tempNFO.getCreator() + " " + bod + " " + tempNFO.getTimestamp() + " " + temp.getEdgeRank());
				}
			}

			else if(temp.getEdgeType() == 4 && j < count){
				NFObject tempNFO = temp.getPostingObject();
				String bod = tempNFO.getBody();
				if(bod == null){
					System.out.println(tempNFO.getObjectID() + " " + tempNFO.getType() + " " + tempNFO.getCreator() + " " + tempNFO.getTimestamp() + " " + temp.getEdgeRank());
				}
				else{
					System.out.println(tempNFO.getObjectID() + " " + tempNFO.getType() + " " + tempNFO.getCreator() + " " + bod + " " + tempNFO.getTimestamp() + " " + temp.getEdgeRank());
				}
			}
			printStupidComments(getRoot(), temp.getPostingObject().getObjectID());
			i++;
			j++;
		}
	}

	public boolean isPresent(Edge test, EdgeStack testStack){
		boolean result = false;
		String ID;
		if(test.getEdgeType() == 3){
			ID = test.getTargetObject().getObjectID();
			EdgeStack testStak = testStack.klone();
			while (!testStak.isEmpty()){
				Edge temp = testStak.pop();
				if(ID.equals(temp.getPostingObject().getObjectID()) || ID.equals(temp.getTargetObject().getObjectID())){
					result = true;
					break;
				}
			}
		}



		return result;
	}

	/*    public EdgeStack stackReverse(EdgeStack input){
    	EdgeStack result = new EdgeStack();

    	while(!input.isEmpty()){
    		Edge temp = input.pop();
    		result.push(temp);
    	}

    	return result;
    }*/

	public LinkedList<Edge> toList(EdgeStack input){
		LinkedList<Edge> result = new LinkedList<Edge>();

		while(!input.isEmpty()){
			Edge temp = input.pop();
			result.push(temp);
		}

		return result;
	}

	/*   public boolean isTargetingFriend(Edge input, String[] friends){
    	boolean result = false;
    	String temp = input.getTargetObject().getCreator();
    	for(int i = 0; i < friends.length; i++){
    		if(friends[i].equals(temp)){
    			result = true;
    			break;
    		}
    	}

    	return result;
    }*/

	public void hugeCalc(){
		recurseOne(getRoot());
		recurseTwo(getRoot());

	}

	public void recurseOne(MemberNode current){
		if(current.hasLeft()){ //First we go as far left as possible...
			recurseOne(current.getLeftChild());
		}
		calcOne(current.getItem()); //...then print the parent node...
		if(current.hasRight()){//...then we go as far right as possible.
			recurseOne(current.getRightChild());
		}
	}

	public void recurseTwo(MemberNode current){
		if(current.hasLeft()){ //First we go as far left as possible...
			recurseTwo(current.getLeftChild());
		}
		EdgeStack tempStack = calcTwo(current.getItem()); //...then print the parent node...
		current.getItem().setEdgeStack(tempStack);
		if(current.hasRight()){//...then we go as far right as possible.
			recurseTwo(current.getRightChild());
		}
	}

	public void calcOne(Member dude){

		int affinity = 1;
		if (dude.getUserID().equals(eyeD)){
			affinity = 3;
		}
		else{

			for (int i = 0; i < Friends.length; i += 2){
				if (dude.getUserID().equals(Friends[i])){
					affinity = Integer.parseInt(Friends[i + 1]);
				}
			}
		}
		if(dude.getEdgeStack() != null){
			EdgeStack tempES = dude.getEdgeStack().klone();

			while(!tempES.isEmpty()){
				Edge tempEdge = tempES.pop();
				int weight = tempEdge.getWeight();
				int timeDecay = tempEdge.getPostingObject().getTimeDecay();
				int result = affinity * weight * timeDecay;
				tempEdge.setEdgeRank(result);
			}
		}


	}

	public EdgeStack calcTwo(Member dude){
		EdgeStack result = new EdgeStack();
		if(dude.getEdgeStack() != null){
			EdgeStack tempES = dude.getEdgeStack().klone();
			while(!tempES.isEmpty()){
				Edge eTemp = tempES.pop();
				runningTotal = eTemp.getEdgeRank();
				String focus = eTemp.getPostingObject().getObjectID();
				traverseTree(focus, getRoot());
				eTemp.setEdgeRank(runningTotal);
				result.push(eTemp);
			}
		}
		return result;
	}

	public void traverseTree(String focus, MemberNode current){
		if(current.hasLeft()){ //First we go as far left as possible...
			traverseTree(focus, current.getLeftChild());
		}
		if (current.getItem().getEdgeStack() != null){
			EdgeStack tempES2 = current.getItem().getEdgeStack().klone();
			while(!tempES2.isEmpty()){
				Edge tempE = tempES2.pop();
				if(tempE.getTargetObject().getObjectID().equals(focus)){
					runningTotal += tempE.getEdgeRank();
				}
			}
		}
		if(current.hasRight()){//...then we go as far right as possible.
			traverseTree(focus, current.getRightChild());
		}
	}
	
	public void printStupidComments(MemberNode current, String focus){
		if(current.hasLeft()){ //First we go as far left as possible...
			printStupidComments(current.getLeftChild(), focus);
		}
		if (current.getItem().getEdgeStack() != null){
			EdgeStack tempES2 = current.getItem().getEdgeStack().klone();
			while(!tempES2.isEmpty()){
				Edge tempE = tempES2.pop();
				//System.out.println(tempE.getTargetObject().getObjectID()+"="+focus+"?");
				if(tempE.getTargetObject().getObjectID().equals(focus)){
				//	System.out.println("FUCKUUUUUU==================================================================================================================");
					NFObject tempNFO = tempE.getPostingObject();
					String bod = tempNFO.getBody();
					if(bod == null){
						System.out.println("          " + tempE.getPostingObject().getObjectID() + " " + tempE.getPostingObject().getType() + " " + tempE.getPostingObject().getCreator() + " " + tempE.getPostingObject().getTimestamp());
					}
					else{
						System.out.println("          " + tempE.getPostingObject().getObjectID() + " " + tempE.getPostingObject().getType() + " " + tempE.getPostingObject().getCreator() + " " + bod + " " + tempE.getPostingObject().getTimestamp());
					}
				}
			}
		}
		if(current.hasRight()){//...then we go as far right as possible.
			printStupidComments(current.getRightChild(), focus);
		}
	}

	public int calcWeight(NFObject postingObject){
		int weight = 0; 
		if (postingObject.getType() == 0) //Calculate 'weight' for the 'score' calculation
			weight = 1;
		else if(postingObject.getType() == 1)
			weight = 4;
		else if(postingObject.getType() == 2)
			weight = 3;
		else if(postingObject.getType() == 4)
			weight = 4;
		else if(postingObject.getType() == 5)
			weight = 2;
		return weight;
	}

}

