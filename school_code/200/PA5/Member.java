import java.util.LinkedList;
import java.util.ListIterator;

/**
 * CS 200 Colorado State University, Fall 2011
 */

public class Member {

	private String userID;
	private String first;
	private String last;
	private EdgeStack edgeStack;
	private LinkedList<Friend> fList;
	private ListIterator<Friend> fListItr;
	
	public Member(){ //Default constructor
		userID = null;
		first = null;
		last = null;
		fList = new LinkedList<Friend>();
	}
	
	public Member(String _userID, String _first, String _last){ //Only constructor called (in InfoParser).
		userID = _userID;
		first = _first;
		last = _last;
		fList = new LinkedList<Friend>();
	}
	
	public String getUserID(){
		return userID;
	}

	public String getFirst(){
		return first;
	}
	
	public String getLast(){
		return last;
	}
	
	public EdgeStack getEdgeStack(){
		return edgeStack;
	}	
	
	public void setUserID(String _userID){
		userID = _userID;
	}
	
	public void setFirst(String _first){
		first = _first;
	}
	
	public void setLast(String _last){
		last = _last;
	}
	
	public void setEdgeStack(EdgeStack _edgeStack){
		edgeStack = _edgeStack;
	}
	
	public void sortScore(){//Overloaded method call. Allows us to call sortScore without needing a stack.
		edgeStack = sortScore(edgeStack);
	}
	
	public EdgeStack sortScore(EdgeStack _edgestack){//Sort implementing the Quicksort algorithm
		EdgeStack temp = new EdgeStack();
		EdgeStack less = new EdgeStack();
		EdgeStack greater = new EdgeStack();
		EdgeStack pivot = new EdgeStack();
		
		if(_edgestack.isEmpty()){//Base case return.
			return _edgestack;
		}
		pivot.push(_edgestack.pop()); //Takes the first element of the stack as the pivot.
		while(!_edgestack.isEmpty()){ //Iterate through the rest of the stack and sort into appropriate stacks
				if(_edgestack.peek().getEdgeRank() <= pivot.peek().getEdgeRank()){
					less.push(_edgestack.pop()); //Put all Edges less than or equal to pivot in 'less'
				}
				else {
					greater.push(_edgestack.pop());//And put all Edges greater than pivot in 'greater'
				}
		}
		
		less = sortScore(less); //recursive calls to sort the 'less' and 'greater' EdgeStacks
		greater = sortScore(greater);
		
		//Transfer Edges back to a single list to return
		while(!less.isEmpty()){//Load temp with the contents of 'less' first...
			temp.push(less.pop());
		}
		temp.push(pivot.pop()); //...followed by the pivot...
		while(!greater.isEmpty()){//... and finally the contents of 'greater'
			temp.push(greater.pop());
		}
		return temp;//Bada-bing!
		
	}
	
	//This method will take an object ID, and find all NFObjects targeting that ID, as well as all
	//of the NFObjects targeting those, and so on, until all related objects have been found and recorded.
	public int[] getRelated(int ID){//Decided to implement this by returning an int array with ID's
		int i = 0; //Indexing variable for 'indices'
		int[] indices = new int[100]; //Arbitrarily large array to store PostingObject ID's
		int[] inc; //Used to store the returned array from recursive calls
		for (int k = 0; k < 100; k++){//Initialize the array with sentinel value -1
			indices[k] = -1;
		}
		EdgeStack temp = edgeStack.klone();//Get a copy of the edgestack
		while (!temp.isEmpty()){//Iterate through the edgestack, looking for objects targeting 'ID'
			if (Integer.parseInt(temp.peek().getTargetObject().getObjectID()) == ID){
				indices[i] = Integer.parseInt(temp.peek().getPostingObject().getObjectID());
				i++;
				//Recursive call to find objects targeting this newly found related object. (below)
				inc = getRelated(Integer.parseInt(temp.pop().getPostingObject().getObjectID()));
				int j = 0; //Index variable for 'inc[]'
				while(inc[j] != -1){//Copy returned array to main array, until sentinel value is found.
					
					indices[i] = inc[j];
					i++;
					j++;
				}
			}
			else{
				temp.pop();
			}
		}
		return indices;//Once the list has been traversed, return what we found.
	}
	
	//Sums the individual scores of Edges related to one-another.
	public void bigCalc(){
		EdgeStack temp = edgeStack.klone(); //Get a copy of EdgeStack
		EdgeStack temp2 = new EdgeStack(); //Stack used to store popped edges from temp.
		while(!temp.isEmpty()){//Iterate through 'temp'
			Edge eTemp = temp.pop();//Get the top edge from the stack...
			temp2.push(eTemp);//... and store it in the 2nd stack
			int score = eTemp.getEdgeRank(); //Get the edge's score...
			int[] numbaz = getRelated(Integer.parseInt(eTemp.getPostingObject().getObjectID()));//...and the edge's related objects
			int i = 0;
			while (numbaz[i] != -1){//Iterate through the array of ID's related to our edge...
				EdgeStack temp3 = edgeStack.klone();//...get a new copy of EdgeStack to destroy...
				while (Integer.parseInt(temp3.peek().getPostingObject().getObjectID()) != numbaz[i]){
					temp3.pop();//...pop edges until we find a related edge...
				}
				score += temp3.peek().getEdgeRank();//... and add its score to the edge's.
				i++;//get the next index in 'numbaz'.
			}
			eTemp.setEdgeRank(score);//Set the edge's score.
		}
		while (!temp2.isEmpty()){//Copy the edges back to temp, preserving their original order...
			Edge bak = temp2.pop();
			temp.push(bak);
		}
		edgeStack = temp;//... and set this updated stack as the Member's edgeStack
	}
	
	public void printList(){ //Assistant method. Used to check the list our program assembles.
		System.out.println("=========BEGIN PRINTING EDGESTACK==========");
		EdgeStack fallGuy = edgeStack.klone();
		while (!fallGuy.isEmpty()){
			Edge t = fallGuy.pop();
			System.out.println(t.getEdgeRank());
		}
		System.out.println("=/=/=/=/=/=END PRINTING EDGESTACK/=/=/=/=/=/");
	}
	
//=======================PA5 ADDITIONS BELOW==========================================================================//
	
    public void addFriend(String memberID, int affinityScore){ //Method used to add friends
    	Friend buddy = new Friend(memberID, affinityScore);		//Make a new friend given the passed information...
    	fList.add(buddy);										//...the jam him/her in with the rest of 'em.
    }
    
    public void removeFriend(String friend_MemberID){	//Removes friend from the linked list of friends.
    	fListItr = fList.listIterator();	//First, let's get a iterator for our linked list.
    	Friend bebe = fListItr.next();		//Next, make a 'placeholder' friend object, and make it the first friend in the list
    	while (!bebe.getID().equals(friend_MemberID) && fListItr.hasNext()){	//While our placeholder isn't the one we don't like anymore...
    		bebe = fListItr.next(); 	//... iterate through the list
    	}
    	if (friend_MemberID.equals(bebe.getID())){//If we found the marked 'friend'...
    		int indx = fList.indexOf(bebe);	//...note the friend's index in the list...
    		fList.remove(indx);//...and knock 'em out of the picture.
    	}
    }

    public int getAffinityScore(String friend_MemberID){//Method used to get a friend's affinity score.
    	fListItr = fList.listIterator();//First, get an iterator for the linked list
    	Friend bebe = fListItr.next(); //Next, make a placeholder friend, set it equal to the first friend in the list.
    	while (!bebe.getID().equals(friend_MemberID) && fListItr.hasNext()){//While we haven't found our friend...
    		bebe = fListItr.next();//...iterate through the list looking for them.
    	}
    	if (friend_MemberID.equals(bebe.getID())){ //If we found the right friend...
    		return bebe.getAffinity(); //return the requested info.
    	}
    	return -1;
    }

    public void setAffinityScore(String friend_memberID, int affinityScore){//Method used to set a friend's affinity.
    	fListItr = fList.listIterator();//Get an iterator, per usual...
    	Friend bebe = fListItr.next();//Make a placeholder Friend object; set it to the first friend in the linked list.
    	while (!bebe.getID().equals(friend_memberID) && fListItr.hasNext()){//While we're not on the right friend...
    		bebe = fListItr.next();	//... iterate through them.
    	}
    	if(friend_memberID.equals(bebe.getID())){ //If we found our friend, 
    		int indx = fList.indexOf(bebe); //Note the friend's index...
    		fList.remove(bebe);//...remove the friend from the list...
    		bebe.setAffinity(affinityScore);//...set the friend's affinity score...
    		fList.add(indx, bebe);//...and add the modified friend to the linked list at the index the original was.
    	}
    }

    public String[] getListOfMyFriends(){ //Make an array of Strings which hold the names of the member's friends
    	fListItr = fList.listIterator();	//Get an iterator (NOWAY!)
    	String[] result = new String[fList.size() * 2]; //initialize the result String[] to twice the size of the LinkedList.
    	for (int i = 0; i < fList.size() * 2; i += 2){//While there are Friends in the list...
    		Friend bebe = fListItr.next(); //... get them... 
    		result[i] = bebe.getID();//... and add their MemberID to the array...
    		result[i + 1] = String.valueOf(bebe.getAffinity()); //...followed immediately by their affinity.
    	}
    	return result; //And we're off!
    }
}