/**
 * CS 200 Colorado State University, Fall 2011
 * This is the base class for Bonus PA. 
 * DO NOT MODYFY main(). Grading will be based on main() of this class.
 * For more information: http://www.cs.colostate.edu/~cs200/Fall11/assignments/BonusPA/BonusPA.html
 */
import java.util.LinkedList;
import java.util.List;

public class BonusPA{

	Parser p;
	Graph G;
	
    public BonusPA(String _input_file){
		
    	p = new Parser(_input_file);
    	G = p.parseFile();
    	//G.printMatrix();
    	
    }

	// print out all of the neighboring cities
    public void printNeighbors(String city){

		G.printNeighbors(city);
		
    }

	// print out all of the cities on the shortest path from 
	// city A to city B	
    public void printShortestPath(String cityA, String cityB){

		String[] temp = G.getShortestPath(cityA, cityB);
		System.out.println(temp[0]);
		for(int i = 1; i < temp.length; i++){
			if(!temp[i].equals("zyzzx") && !temp[i].equals(temp[i-1])){
				System.out.println(temp[i]);
			}
		}
    }

	// print out the total distance of the shortest path 
	// between city A and city B 
    public void printShortestDistance(String cityA, String cityB){

		G.getShortestDistance(cityA, cityB);
		
    }  

    //NOTE: DO NOT MODIFY main().
    public static void main(String args[]){
		if (args.length < 2){
			System.out.println("Please enter your input file and command.");
			System.out.println("e.g. java BonusPA [input_file][command1][command2]");
		}else{	    
			String input_file = args[0];
			String cmd = args[1];
	    

			BonusPA bonusPA = new BonusPA(input_file);
		
			if (cmd.equals("printNeighbors")){
				bonusPA.printNeighbors(args[2]);
			}else if(cmd.equals("printShortestPath")){
				bonusPA.printShortestPath(args[2],args[3]);
			}else if(cmd.equals("printShortestDistance")){
				bonusPA.printShortestDistance(args[2], args[3]);
			}	
	
		}	
    }
}
