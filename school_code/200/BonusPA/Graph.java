import java.util.Hashtable;


public class Graph {

	private int[][] Cities;
	private Hashtable<String, Integer> H;
	private Hashtable<Integer, String> K;
	private int len;
	
	public Graph(int _len, Hashtable<String, Integer> _H, Hashtable<Integer, String> _K){
		len = _len;
		Cities = new int[len][len];
		H = _H;
		K = _K;
	}
	
	public void addRelation(String a, String b, int x){
		Cities[H.get(a)][H.get(b)] = x;
		Cities[H.get(b)][H.get(a)] = x;
	}
	
	public boolean hasHash(String a){
		boolean result = false;
		if(H.containsKey(a)){
			result = true;
		}
		return result;
	}
	
	public void printNeighbors(String city){
		int i = H.get(city);
		for(int j = 0; j < H.size(); j++){
			if(Cities[i][j] != 0){
				System.out.println(K.get(j));
			}
		}
	}
	
	public String[] getShortestPath(String cityA, String cityB){
		String[][] result = new String[len][len];
		for (int i = 0; i < len; i++){
			for (int j = 0; j < len; j++){
				result[i][j] = "zyzzx";
			}
			result[i][0] = cityA;
		}
		boolean[] visited = new boolean[len];
		int[] dists = new int[len];
		for (int i = 0; i < len; i++){
			dists[i] = Integer.MAX_VALUE;
		}
		dists[H.get(cityA)] = 0;

		for (int i = 0; i < len; i++){  //Algorithm implementation derived from http://snippets.dzone.com/posts/show/4832
			int min = -1;

			for (int j = 0; j < len; j++){
				if(!visited[j] && ((min == -1) || (dists[j] < dists[min]))){
					min = j;
				}
			}
			
			if(min != -1){
				visited[min] = true;

				for (int j = 0; j < len; j++){
					if (min != j && Cities[min][j] > 0){
						if (dists[min] + Cities[min][j] < dists[j]){
							dists[j] = dists[min] + Cities[min][j];
							for (int l = 0; l < firstZyzz(result, min); l++){
									result[j][l] = result[min][l];
							}
							if(j > 0 && !result[j][firstZyzz(result, j-1)].equals(K.get(j))){
								result[j][firstZyzz(result, j)] = K.get(j);
							}
							else if (j == 0){
								result[j][firstZyzz(result, j)] = K.get(j);
							}
						}
					}
				}
			}
		}
		//printList(result);
		return result[H.get(cityB)];
	}
	
	public void getShortestDistance(String cityA, String cityB){
		boolean[] visited = new boolean[len];
		int[] dists = new int[len];
		for (int i = 0; i < len; i++){
			dists[i] = Integer.MAX_VALUE;
		}
		dists[H.get(cityA)] = 0;

		for (int i = 0; i < len; i++){  //Algorithm implementation derived from http://snippets.dzone.com/posts/show/4832
			int min = -1;

			for (int j = 0; j < len; j++){
				if(!visited[j] && ((min == -1) || (dists[j] < dists[min]))){
					min = j;
				}
			}
			
			if(min != -1){
				visited[min] = true;

				for (int j = 0; j < len; j++){
					if (min != j && Cities[min][j] > 0){
						if (dists[min] + Cities[min][j] < dists[j]){
							dists[j] = dists[min] + Cities[min][j];
						}
					}
				}
			}
		}
		System.out.println(dists[H.get(cityB)]);
	}
	
	public int findIndex(String[][] input, String city, int i){
		int result = -1;
		
		for(int j = 1; i < len; i++){
			if (input[i][j].equals(city) || input[i][j].equals("zyzzx")){
				result = j;
				break;
			}
		}
		
		return result;
	}
	
	public int firstZyzz(String[][] input, int i){
		int result = -1;
		
		for (int j = 0; j < input.length; j++){
			if(input[i][j].equals("zyzzx")){
				result = j;
				break;
			}
		}
		
		return result;
	}
	
	public int relevantPath(String[] testa, String city){
		int result = -1;
		
		for (int i = 0; i < testa.length; i++){
			if (testa[i].equals(city)){
				System.out.println(city + " found at index " + i);
				result = i;
				break;
			}
		}
		
		return result;
	}
	
	public String[] cleanList(String[] input, String city, int i){
		input[i] = city;
		for(i += 1; i < input.length; i++){
			input[i] = "zyzzx";
		}
		
		return input;
	}
	
	public void printMatrix(){
		for(int i = 0; i < Cities.length; i++){
			for(int j = 0; j < Cities.length; j++){
				System.out.print(Cities[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	public void printDij(int[] ary){
		for (int i = 0; i < ary.length; i++){
			System.out.println(ary[i]);
		}
	}

	public void printList(String[][] ary){
		for(int i = 0; i < ary.length; i++){
			for(int j = 0; j < ary.length; j++){
				System.out.print(ary[i][j] + " ");
			}
			System.out.println();
		}
	}

}