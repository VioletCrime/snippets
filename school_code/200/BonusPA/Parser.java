import java.io.File;
import java.io.FileNotFoundException;
import java.util.Hashtable;
import java.util.Scanner;


public class Parser {	
	
	private String inFile;
	private File input;
	private int len;
	private Hashtable<String, Integer> H;
	private Hashtable<Integer, String> K;
	private Graph G;
	
	public Parser(String inputFile){
		inFile = inputFile;
		input = new File (inFile);
		H = makeHashtable();
		K = makeRevHashtable();
		len = H.size();
		G = new Graph(len, H, K);
	}
	
	public Graph parseFile(){
		Scanner inny = null;
		try{
			inny = new Scanner(input);
		}
		catch(FileNotFoundException e){
		}
		
		while(inny.hasNextLine()){
			String[] temp = inny.nextLine().trim().split(" ");
			G.addRelation(temp[0], temp[1], Integer.parseInt(temp[2]));
		}
		
		return G;
	}
		
	private Hashtable<String, Integer> makeHashtable(){
		Scanner inny = null;
		try{
			inny = new Scanner(input);
		}
		catch(FileNotFoundException e){
		}
		
		Hashtable<String, Integer> h = new Hashtable<String, Integer>();
		int i = 0;
		
		while(inny.hasNextLine()){
			String[] temp = inny.nextLine().trim().split(" ");
			if(!h.containsKey(temp[0])){
				h.put(temp[0], i);
				i++;
			}
			if(!h.containsKey(temp[1])){
				h.put(temp[1], i);
				i++;
			}
		}
		return h;
	}
	
	private Hashtable<Integer, String> makeRevHashtable(){
		Scanner inny = null;
		try{
			inny = new Scanner(input);
		}
		catch(FileNotFoundException e){
		}
		
		Hashtable<Integer, String> h = new Hashtable<Integer, String>();
		int i = 0;
		
		while(inny.hasNextLine()){
			String[] temp = inny.nextLine().trim().split(" ");
			if(!h.contains(temp[0])){
				h.put(i, temp[0]);
				i++;
			}
			if(!h.contains(temp[1])){
				h.put(i, temp[1]);
				i++;
			}
		}
		return h;
	}
	
}
