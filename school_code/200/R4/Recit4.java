import java.util.*;
public class Recit4{
	
	Random random = new Random();
	
	//1; thinking it's a linear relationship, something to the effect of '3n'. I think this because there are 3 (non-nested) while loops running.
	public static double[] nextPermutation(double[] perm)
	{
		int n = perm.length -1;
		int j = n-1;
		
		while (perm[j] > perm[j+1]) j--;
		 int k = perm.length - 1;
		while (perm[j] > perm[k]) k--;
		
		//swap
		double temp = perm[k];
		perm[k]= perm[j];
		perm[j] = temp;
		
		int m = j+1;
		int s = k;
		 
		while(m<s)
		{
			//swap
			temp = perm[m];
			perm[m]= perm[s];
			perm[s] = temp;
			
			s--;
			m++;
		}
		return perm;
	}
	
	//2; Steeper linear relationship. it calls functions 1 and 3, each of which employ a linear algorithm.
	public static void permutationsFrom(double[] perm)
	{
		while(!isLastPermutation(perm = nextPermutation(perm)));
	}
	
	//3; Also linear: employing a single for loop that increases iterations linearly with increases in 'N'
	public static boolean isLastPermutation(double[] perm)
	{
		double tmp = Double.MAX_VALUE;
		for (int i = 0; i < perm.length; i++)
			if (perm[i] < tmp)
				tmp = perm[i];
			else return false;
		return true;
	}

	//4; Squared relation due to the nested 'for' loop, ensuring N^2 iterations.
	public static double[] selectionSort(double[] numbers)
	{
		int minNumIndex = -1;
		for(int j = 0; j < numbers.length; j++)
		{
			double minNumber = Double.MAX_VALUE;
			for(int i = j; i < numbers.length; i++)
			{
				if(numbers[i] < minNumber)
				{
					minNumber = numbers[i];
					minNumIndex = i;
				}
			}
			numbers[minNumIndex] = numbers[j];
			numbers[j] = minNumber;
		}
		return numbers;
	}
	
	//5; Also squared thanks to a 'while' loop nested in a 'for' loop
	public static double[] insertionSort(double[] numbers)
	{
		double number;
		for(int i = 0; i < numbers.length; i++)
		{
			number = numbers[i];
			int index = i;
			while (index > 0 && (numbers[index-1] > number)) 
				{
			        numbers[index] = numbers[index - 1];
			        index--;
			    }
			    numbers[index] = number;
		}
		return numbers;
	}
	
    private static void merge(double[] a, double[] aux, int lo, int mid, int hi) {
        int i = lo, j = mid;
        for (int k = lo; k < hi; k++) {
           if      (i == mid)                 aux[k] = a[j++];
           else if (j == hi)                  aux[k] = a[i++];
           else if (a[j] < a[i]) 			  aux[k] = a[j++];
           else                               aux[k] = a[i++];
        }

        // copy back
        for (int k = lo; k < hi; k++)
           a[k] = aux[k];
     }

    //7; Going to take forever, due to the double recursion, but the neither the recusrion nor the 'merge' call have nested loops, so it's likely linear.
    //Adapted from Princeton java library
    public static void mergesort(double[] a) 
    {
        int N = a.length;
        double[] aux = new double[N];
        sort(a, aux, 0, N);
    }
     public static void sort(double[] a, double[] aux, int lo, int hi) {

        // base case
        if (hi - lo <= 1) return;

        // sort each half, recursively
        int mid = lo + (hi - lo) / 2;
        sort(a, aux, lo, mid);
        sort(a, aux, mid, hi);

        // merge back together
        merge(a, aux, lo, mid, hi);
     }
     
     //9; linear: just one loop.
    public static int linearSearch(double[] n, double key)
    {
    	for (int i = 0; i < n.length; i++)
    		if (n[i]==key) return i;
    	return -1;
    }
	
	public static double getSetGo(double[] n, int m)
	{
		Random random = new Random();
		int idx = random.nextInt(n.length);
		
		long start = System.currentTimeMillis();
		
		switch (m) {
        case 1:  nextPermutation(n); break;
        case 3:  isLastPermutation(n); break;
        case 2:  permutationsFrom(n); break;
        case 4:  selectionSort(n); break;
        case 5:  insertionSort(n); break;
        case 6:  Arrays.sort(n); break;
        case 7:  mergesort(n); break;
        case 8:  Arrays.binarySearch(n, n[idx]); break;
        case 9:  linearSearch(n, n[idx]); break;
		}

		long stop = System.currentTimeMillis();
        double elapsed = (stop - start) / 1000.0;
        
        switch (m) {
        case 1:  System.out.println("nextPermutation(): " + elapsed + " seconds"); break;
        case 3:  System.out.println("isLastPermutation(): " + elapsed + " seconds"); break;
        case 2:  System.out.println("permutationsFrom(): " + elapsed + " seconds"); break;
        case 4:  System.out.println("selectionSort(): " + elapsed + " seconds"); break;
        case 5:  System.out.println("insertionSort(): " + elapsed + " seconds"); break;
        case 6:  System.out.println("Arrays.sort(): " + elapsed + " seconds"); break;
        case 7:  System.out.println("mergesort(): " + elapsed + " seconds"); break;
        case 8:  System.out.println("binarySearch(): " + elapsed + " seconds"); break;
        case 9:  System.out.println("linearSearch(): " + elapsed + " seconds"); break;
        }
        return elapsed;
	}
	
	public static double[] randomArray(int size)
	{
		double[] n = new double[size];
		for (int i = 0; i < size; i++)
			n[i] = Math.random();
		return n;
	}
	
	public static void printArray(double[] n)
	{
		for(double m : n)
		 System.out.print(m + " ");
		System.out.println();
	}
	
	public static void main(String[] args) 
	{
		//size of the input (think of this as n)
		int N = 9999999;
		//number of times you want to execute the function (f(n))
		int numTrials = 20;
	
		int lap = 0;
		double avg = 0;
		
		//read the assignment or switch statement above to see which int coresponds to which function
		while (lap++ < numTrials)
		 avg += getSetGo(randomArray(N),9);
		System.out.println("\nThe average runtime over uniformly random instances of size " + N + " was " + avg/numTrials + " seconds");
	}

}