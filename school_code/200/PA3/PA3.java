/**
 * CS 200 Colorado State University, Fall 2011
 * This is the base class for PA3. 
 * DO NOT MODYFY main(). Grading will be based on main() of this class.
 */

public class PA3{
    MemberTree tallBoy = new MemberTree();//The MemberTree for the MemberNodes.
    InformationParser ip = null;
    
    public PA3(String _input_file){
	// add your code here
    	ip = new InformationParser(_input_file);
   }

    public void build_tree(int option){
	// build a tree from the example data file, and
	// if the option is 1, print the user ids following the inorder traversal
	// algorithm. if the option is 2, print the user ids folloiwng the preorder
	// traverdal algorithm. if hte options is 3, print the user ids folloiwng 
	// the postorder traversal algorithm

	// add your code here
    	tallBoy = ip.readMembers();
    	switch (option){
    	case 1:
    		tallBoy.printMemberTreeInOrder();
    		break;
    	case 2:
    		tallBoy.printMemberTreePreOrder();
    		break;
    	case 3:
    		tallBoy.printMemberTreePostOrder();
    		break;
    	}
    }

    public void add_member(String member_info){
	// build a tree from the example data file, and
	// add a member to the tree (binery search tree)
	// print out the user's ids following the inorder traversal algorithm

	//add your code here
    	tallBoy = ip.readMembers();
    	String[] stuff = member_info.split(" ");
    	Member dude = new Member(stuff[0], stuff[1], stuff[2]);
    	MemberNode a = new MemberNode(dude, null, null);
    	tallBoy.insertMember(a);
    	tallBoy.printMemberTreeInOrder();
    }

    public void retrieve_memberInfo(String user_id){
	// build a tree from the example data file, and
	// retrieve information of a member from the tree (who has the specified user_id)
	// print first name and last name. (first name and last name are separated by a space)

	// add your code here
    	tallBoy = ip.readMembers();
    	MemberNode b = tallBoy.retrieveMemberInfo(user_id, tallBoy.getRoot());
    	Member c = b.getItem();
    	System.out.println(c.getFirst() + " " + c.getLast());
    }

    public void remove_member(String user_id){
	// build a tree from the example data file, and
	// delete a member from the tree (who has the specified user_id)
	// print your tree (user ids following the inorder traversal algorithm)

	// add your code here
    	tallBoy = ip.readMembers();
    	tallBoy.removeMember(user_id);
    	tallBoy.printMemberTreeInOrder();
    	
    }




    //NOTE: DO NOT MODIFY main().
    public static void main(String args[]){
	if (args.length < 3){
	    System.out.println("Please enter your input file and command.");
	    System.out.println("e.g. java PS3 [input_file][command1][command2]");
	}else{
	    
	    String input_file = args[0];
	    String cmd = args[1];
	    

	    PA3 pa3 = new PA3(input_file);
			
	   
	    if (cmd.equals("build_tree")){
			int cmd2 = Integer.parseInt(args[2]);
			pa3.build_tree(cmd2);
			}else if (cmd.equals("add_member")){		
				pa3.add_member(args[2]);
			}else if (cmd.equals("remove_member")){
				pa3.remove_member(args[2]);
			}else if(cmd.equals("retrieve_memberInfo")){
				pa3.retrieve_memberInfo(args[2]);
			}
	
		}
    }
}