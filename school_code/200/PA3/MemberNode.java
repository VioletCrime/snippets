/**
 * CS 200 Colorado State University, Fall 2011
 */

public class MemberNode{
    private Member item;
    private MemberNode leftchild;
    private MemberNode rightchild;
   
    public MemberNode(){
    	item = null;
    	leftchild = null;
    	rightchild = null;
    }  

    public MemberNode (Member _item, MemberNode _leftchild, MemberNode _rightchild) { 
    	item = _item;
    	leftchild = _leftchild;
    	rightchild = _rightchild;
    }	   

    public void setLeftChild(MemberNode _left){
    	leftchild = _left;
    }

    public void setRightChild(MemberNode _right){
    	rightchild = _right;
    }

    public void setItem(Member _item){
    	item = _item;
    }
    public MemberNode getLeftChild (){
    	return leftchild;
    }

    public MemberNode getRightChild(){
		return rightchild;
    }

    public Member getItem(){
		return item;
    }
    
    public boolean hasLeft(){ //Return true if 'leftchild' is not pointing to null
    	return leftchild != null;
    }
    
    public boolean hasRight(){ //Return true if 'rightchild' is not pointing to null
    	return rightchild != null;
    }



}
       	     