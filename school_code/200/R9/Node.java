
public class Node {
	int value = -7777;
	Node lchild = null;
	Node rchild = null;
	
	
	public Node(){//Constructor used to create root Node
		
	}
	public Node(int i){//Constructor used to create children
		value = i;
	}
	
	public void setValue(int a){//Never used....
		value = a;
	}
	
	public void setRight(Node a){
		rchild = a;
	}
	
	public void setLeft(Node a){
		lchild = a;
	}
	
	public int getValue(){//Never used....
		return value;
	}
	
	public Node getlchild(){//Never used...
		return lchild;
	}
	
	public Node getrchild(){//never used...
		return rchild;
	}
	
	public void insert(int i){
		if (value == -7777){//Only true with first value (root value), using -7777 as a sentinel
			value = i;
		}
		else if (i < value){
			if (this.hasLeft()){//if there is a left child, pass the value down...
				lchild.insert(i);
			}
			else lchild = new Node(i); //... if there isn't, make one and set it
		}
		else if (i > value){//if there is a right child, pass the value down...
			if (this.hasRight()){
				rchild.insert(i);
			}
			else rchild = new Node(i);//... if there isn't, make one and set it
		}
	}
	
	public boolean hasLeft(){//Test to see if a left child exists.
		return lchild != null;
	}
	
	public boolean hasRight(){//Test to see if a right child exists.
		return rchild != null;
	}
	
	public void traverse(){//Implementing in order traverse
		if (this.hasLeft()){//If there's stuff to the left...
			lchild.traverse();//... have it print first...
		}
		System.out.print(value + " ");//...followed by this value...
		if (this.hasRight()){//...and finally everything to the right.
			rchild.traverse();
		}
	}	
}
