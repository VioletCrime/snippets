import java.util.*;

public class BST {
	static Node root = new Node();
	public static void main(String[] args){
		
		root.insert(5);
		root.insert(3);
		root.insert(7);
		root.insert(2);
		root.insert(6);
		root.insert(4);
		root.insert(-1);
		root.insert(12);
		root.insert(18);
		root.insert(15);
		root.insert(-6);
		root.insert(-2);
		root.traverse();
		Node temp = find(6, root);
		System.out.println("\n===========================\nNode \"temp\" = " + temp.getValue() + "\n===========================");
		remove(6);
		root.traverse();
		System.out.println();
		System.out.println(height(root));
		System.out.println(isPerfect(root));
		System.out.println();
		BFS(root);
	}
	
	//CODE FOR WEEK 8 RECITATION FOLLOWS
	public static void remove(int x){ //'Remove' simply houses the algorithm to remove the node)
		
		Node rem = find(x, root); //First, find the node to delete...
		kill(x, root);//...delete it...
		
		if (rem.hasLeft()){
			reinsert(rem.getlchild()); //...and save its children for posterity...
		}
		if (rem.hasLeft()){
			reinsert(rem.getrchild());
		}
	}
	
	public static Node find(int x, Node in){//Finds the node we need to remove
		Node result = null; //Satisfying the 'Node' part of the method, even though this will never be returned...
		if (in.getValue() == x){ //If the node passed in is the chosen one, return it.
			return in;
		}
		else if (x > in.getValue()){ //If 'x' is larger than what we seek to kill...
			return find(x, in.getrchild()); //...chase it down the right branch.
		}
		else if (x < in.getValue()){//If 'x' is smaller than what we seek to kill...
			return find(x, in.getlchild());//...chase it down the left branch.
		}
		return result;//Never used.
	}
	
	public static void reinsert(Node x){//Reinserts values into root. VERY similar to Node.traverse().
		
		root.insert(x.getValue());
		if (x.hasLeft()){
			reinsert(x.getlchild());
		}
		if (x.hasRight()){
			reinsert(x.getrchild());
		}
		
		//Poor code; ensures resulting 'tree' from removed node will effectively be two linked lists branching to the right
		//Fixed by simply moving the present Node's insert call to the very beginning of the block.
		/*if (x.hasLeft()){ //Input the smaller values first...
			reinsert(x.getlchild());
		}
		root.insert(x.getValue()); //...followed by the node's value...
		if (x.hasRight()){//...then move onto larger values
			reinsert(x.getrchild());
		}*/
	}
	
	public static void kill(int x, Node weAt){//The executioner of the bunch.
		if (weAt.getlchild().getValue() == x){//If the left child is the marked node...
			weAt.setLeft(null);//...merc it...
			return;//...and bugger off to the pub.
		}
		if (weAt.getrchild().getValue() == x){//If the right child is marked...
			weAt.setRight(null);//...handle business...
			return;//...and leave before the fuzz arrives.
		}
		if (x < weAt.getValue()){ //By this point, we haven't tracked our target quite yet; do we go left...
			kill(x, weAt.getlchild());
		}
		else if (x > weAt.getValue()){ //... or right with our recursive call?
			kill(x, weAt.getrchild());
		}
	}
	
	
//=========WEEK 9 CODE FOLLOWS============================================
	public static int height(Node at){
		if (!at.hasLeft() && !at.hasRight()){
			return 1;
		}
		int luft = 0;
		int roght = 0;
		if (at.hasLeft()){
			luft = height(at.getlchild());
		}
		if (at.hasRight()){
			roght = height(at.getrchild());
		}
		if (luft > roght){
			return luft + 1;
		}
		else return roght + 1;
	}
	
	public static boolean isFull(Node at){
		if (at.hasLeft() && at.hasRight()){
			if (isFull(at.getlchild()) && isFull(at.getrchild())){
				return true;
			}
		}
		else if((!at.hasLeft() && at.hasRight()) || (at.hasLeft() && !at.hasRight())){
			return false;
		}
		else if (!at.hasLeft() && !at.hasRight()){
			return true;
		}
		return false;
	}
	
	public static boolean isPerfect(Node at){
		if (isFull(at)){
			if (at.hasLeft() && at.hasRight()){
				if (height(at.getlchild()) != height(at.getrchild())){
					return false;
				}
				else return (isPerfect(at.getlchild()) && isPerfect(at.getrchild()));
			}
			else return true;
		
			
		}
		return false;
	}
	
	public static void DFS(Node at){
		System.out.print(at.getValue() + " ");
		if(at.hasLeft()){
			DFS(at.getlchild());
		}
		if(at.hasRight()){
			DFS(at.getrchild());
		}
	}
	
	public static void BFS(Node at){
		Queue<Node> nooQ = new LinkedList<Node>();
		nooQ.add(root);
		Node temp;
		while (nooQ.size() > 0){
			temp = nooQ.remove();
			System.out.print(temp.getValue() + " ");
			if (temp.hasLeft()){
				nooQ.add(temp.getlchild());
			}
			if (temp.hasRight()){
				nooQ.add(temp.getrchild());
			}
		}
		
	}
}

