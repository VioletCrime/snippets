import java.util.Hashtable;

public class Relations {
	private static int[][] matrix;
	private static Hashtable<String, Integer> H;
	
	public static void main(String[] args){
		//Initialize matrix
		matrix = new int[5][5];
		
		H = new Hashtable<String, Integer>();
		H.put("a", 1);
		H.put("b", 0);
		H.put("c", 4);
		H.put("d", 3);
		H.put("e", 2);
		
		
		/*//Makes reflexive symmetric matrix
		matrix[0][0] = 1;
		matrix[1][1] = 1;
		matrix[2][2] = 1;
		matrix[3][3] = 1;
		matrix[4][4] = 1;
		matrix[0][1] = 1;
		matrix[1][0] = 1;
		matrix[1][2] = 1;
		matrix[2][1] = 1;*/
		
		//Makes transitive matrix
		/*matrix[0][2] = 1;
		matrix[0][3] = 1;
		matrix[0][4] = 1;
		matrix[1][0] = 1;
		matrix[1][2] = 1;
		matrix[1][3] = 1;
		matrix[1][4] = 1;
		matrix[2][3] = 1;*/

		matrix[1][1] = 1;
		matrix[1][2] = 1;
		matrix[2][1] = 1;
		matrix[2][2] = 1;
		matrix[3][3] = 1;
		matrix[4][4] = 1;
		
		//Output matrix
		for(int i = 0; i < matrix.length; i++){
			for(int j = 0; j < matrix.length; j++){
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		
		//Test subroutines.
		System.out.println("Is the matrix reflexive? " + isReflexive());
		System.out.println("Is the matrix symmetric? " + isSymmetric());
		System.out.println("Is the matrix transitive? " + isTransitive());
		System.out.println("Is \"b\" related to \"c\"? " + isRelated("b", "c"));
		
		/*int[] temp = getList(1);
		for(int k = 0; k < temp.length; k++){
			System.out.print(temp[k] + " ");
		}*/
	}//End main(String[] args)
	
	public static boolean isReflexive(){
		boolean result = true;
		for (int i = 0; i < matrix.length; i++){
			if (matrix[i][i] != 1){
				result = false;
			}
		}		
		return result;
	}//End isReflexive()
	
	public static boolean isSymmetric(){
		boolean result = true;
		for(int i = 0; i < matrix.length; i++){
			for(int j = 0; j < matrix.length; j++){
				if(matrix[i][j] == 1){
					if(matrix[j][i] != 1){
						result = false;
					}
				}
			}
		}
		return result;
	}//End isSymmetric()
	
	public static boolean isTransitive(){
		boolean result = true;
		for(int i = 0; i < matrix.length; i++){
			for(int j = 0; j < matrix.length; j++){
				if(matrix[i][j] == 1){
					for(int k = 0; k < matrix.length; k++){
						if(matrix[j][k] == 1){
							if(matrix[i][k] != 1){
								result = false;
							}
						}
					}
				}
			}
		}
		return result;
	}//End iStransitive()
	
	public static boolean isRelated(String a, String b){
		boolean result = false;
		if(matrix[H.get(a)][H.get(b)] == 1){
			result = true;
		}
		return result;
	}//End isRelated(String a, String b)
	
}
