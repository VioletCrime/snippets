/**
 * CS 200 Colorado State University, Fall 2011
 */


import java.util.LinkedList;

public class MemberInfo extends MemberTree{
    public MemberInfo(){
    }

    // return the list of memberIDs of user's (specified by memberID) friends.
    public String [] getListOfFriends(String memberID){
    	Member guy = retrieveMemberInfo(memberID,getRoot()).getItem(); //Get the member in question...
    	return guy.getListOfMyFriends(); //...and interrogate the object for a list of friends.
    }
    
    // return the affinity score of friend (specified by friend_memberID).
    // This value is maintained by the member (specified by memberID)

    public int getAffinityScore(String memberID, String friend_memberID){
    	Member guy = retrieveMemberInfo(memberID, getRoot()).getItem(); //Get the member in question...
    	return guy.getAffinityScore(friend_memberID); //...and interrogate the object for the friend's affinity, returning it.
    }


	// return a list of NFObjects posted by user (specified by memberID)
    // This list contains only objects with types specified in the type array.
    // type array can have 0 ~ 6 items based on your need.
    // type 0: visit
    // type 1: wall posting
    // type 2: comment
    // type 3: homepage
    // type 4: status change
    // type 5: like
    // All of the items in the list has timedecay that is less than or equal to the max_timedecay

    public LinkedList <NFObject> getListOfPostingNFObjects(String memberID, int [] type, int max_timedecay){
    	EdgeStack temp = retrieveMemberInfo(memberID, getRoot()).getItem().getEdgeStack().klone(); //Get the member's EdgeStack.
    	LinkedList<NFObject> returnMe = new LinkedList<NFObject>(); //Declare and initialize the list to be returned.
    	while (!temp.isEmpty()){ //While the EdgeStack has edges...
    		Edge eTemp = temp.pop(); //...pop them out one at a time...
    		if (max_timedecay >= eTemp.getPostingObject().getTimeDecay()){ //... if they're within the timeframe...
    			for (int i = 0; i < type.length; i++){//...check to make sure it's of a requested Type...
    				if (eTemp.getPostingObject().getType() == type[i]){
    					returnMe.add(eTemp.getPostingObject()); //... and if it is; add it to the list to be returned.
    				}
    			}
    		}
    	}
    	return returnMe; //End Act IV
    }


    // return a list of Edges posted by user(specified by memberID)'s friends
    public LinkedList <Edge> getFriendsEdges(String memberID){
    	LinkedList<Edge> returnMe = new LinkedList<Edge>(); //initialize a list to be returned.
    	String[] list = getListOfFriends(memberID); //Get the list of the member's friends.
    	for (int i = 0; i < list.length; i+=2){//For every friend in the list...
    		EdgeStack temp = retrieveMemberInfo(list[i], getRoot()).getItem().getEdgeStack().klone();//...get their EdgeStacks...
    		while (!temp.isEmpty()){
    			returnMe.add(temp.pop());//...and dump the contents into the list.
    		}
    	}
    	return returnMe; //Send the list on its way.
    }

    // return a list of NFObjects related to the specified NFObject(specified 
    // with the creator's ID and the object ID).
    // 
    // The currentNFObject can be any type of NFObject. 
    //
    // If currentNFObject is;
    // (1) wall posting, your homepage, or status change
    //     : it should return all of the postings those have the "nfobject" as their target posting.
    // (2) comment or like or visit
    //     : it should return the target object of this "nfobject" -- (a)
    //     : AND all the postings those have object (a) as their target posting
    //
    // In the case (2), there can be some postings published by a member who is not a friend of member(originally
    // specified by the "memberID" parameter). This method should return those postings as well.
    public LinkedList<NFObject> getRelatedNFObjects(NFObject currentObject){
    	
    	LinkedList<NFObject> returnMe = new LinkedList<NFObject>();//Declare and initialize the list to be returned.
    	String interest = null;
    	String EyeD = null;
    	
    	if (currentObject.getType() == 1 || currentObject.getType() == 3 || currentObject.getType() == 4){
    		interest = currentObject.getCreator();
    		EyeD = currentObject.getObjectID();
    	}
    	else{
    		EdgeStack temp = retrieveMemberInfo(currentObject.getCreator(), getRoot()).getItem().getEdgeStack().klone();
    		while(!temp.isEmpty()){
    			Edge eTemp = temp.pop();
    			if (eTemp.getPostingObject().getObjectID().equals(currentObject.getObjectID())){
    				interest = eTemp.getTargetObject().getCreator();
    				EyeD = eTemp.getTargetObject().getObjectID();
    				break;
    			}
    		}
    	}
    	
    	String[] frends = retrieveMemberInfo(interest, getRoot()).getItem().getListOfMyFriends();
    	for (int i = 0; i < frends.length; i+=2){
    		EdgeStack temp = retrieveMemberInfo(frends[i], getRoot()).getItem().getEdgeStack().klone();
    		while (!temp.isEmpty()){
    			Edge eTemp = temp.pop();
    			if (eTemp.getTargetObject().getCreator().equals(EyeD)){
    				returnMe.add(eTemp.getTargetObject());
    			}
    		}
    	}
    	
    	
    	
    	return returnMe;//Return the resulting linked list
    }

}
       	     
