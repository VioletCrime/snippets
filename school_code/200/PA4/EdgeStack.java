/**
 * CS 200 Colorado State University, Fall 2011
 */
import java.util.LinkedList;

public class EdgeStack {
	
	private LinkedList<Edge> llist = new LinkedList<Edge>();
	
	public EdgeStack(){
		
	}
	
	public EdgeStack(LinkedList<Edge> a){
		llist = a;
		
	}
	
	public boolean isEmpty(){
		return llist.size() == 0;
	}
		
	public void push(Edge e){
		llist.add(e);
	}

	public Edge pop(){
		return llist.remove();
	}
	
	public Edge peek(){
		return llist.peek();
	}
	
	public Edge peekLast(){
		return llist.peekLast();
	}
	
	public LinkedList<Edge> popAll(){
		llist.clear();
		return llist;
	}
	
	//Perhaps the single most useful helper method we made. Clones and returns 'this' EdgeStack object.
	@SuppressWarnings("unchecked")
	public EdgeStack klone(){ 
		LinkedList<Edge> terry = (LinkedList<Edge>) llist.clone();
		EdgeStack temp = new EdgeStack(terry);
		return temp;
	}
}