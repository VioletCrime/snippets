/**
 * CS 200 Colorado State University, Fall 2011
 * This is the base class for PA4. 
 * DO NOT MODYFY main(). Grading will be based on main() of this class.
 */
import java.util.LinkedList;
public class PA4{
    String input_file;
    MemberInfo myMemberInfo;
    InformationParser ip;

    public PA4(String _input_file){
    	ip = new InformationParser(_input_file);
    	myMemberInfo = ip.readMembers();
    }

    public void printListOfFriends(String memberID){
        String[] list = myMemberInfo.getListOfFriends(memberID); //Call MemberInfo's method for the String[]
        for(int i=0; i<list.length; i+=2){	//Every even entry is an ID, odd entries (i + 1) are affinities.
        	System.out.println(list[i]+":"+list[i+1]); //Put 'em on screen in the proper format! ^_^ b
        }
    }
    
    public void printAffinityScore(String memberID, String friendID){    	
       	System.out.println(myMemberInfo.getAffinityScore(memberID, friendID));//Just print what the MemberInfo's function returns. Easy!
    }
    
    public void printPostingNFObject(String memberID, int [] type, int timedecay ){
	// print postingNFObjects,
	// (1) posted by member with ID of "memberID" 
	// (2) with the NFObject type of one of the int in the "type" array
	// (3) posted within (less than or equal to) timedecay specified by "timedecay"
	
	// add your code here
    	LinkedList<NFObject> yadda = myMemberInfo.getListOfPostingNFObjects(memberID, type, timedecay);
    	for (int i = 0; i < yadda.size(); i++){
    		NFObject temp = yadda.get(i);
    		System.out.println("postingobject " + temp.getObjectID() + " " + temp.getType() + " " + temp.getCreator() + " " + temp.getBody() + " " + temp.getTimestamp());
    	}

    }

    public void printFriendsEdges(String memberID){
	// print Edges posted by friends of the member specified with "memberID" parameter
	// please follow the same format in the sample data file.
	//
	// edge [edgeID] [type]
	// postingobject [ID] [type] [creator] [message] [timedecay]
	// targetobject [ID] [type] [creator] [message] [timedecay]
	//
	// please make sure that each of the items are separated by space and each of the
	// lines are separated by linefeed character (\n).

	// add your code here
    	LinkedList<Edge> temp = myMemberInfo.getFriendsEdges(memberID);
    	for (int i = 0; i < temp.size(); i++){
    		Edge eTemp = temp.get(i);
    		System.out.println("edge " + eTemp.getEdgeID() + " " + eTemp.getEdgeType());
    		NFObject pTemp = eTemp.getPostingObject();
    		NFObject tTemp = eTemp.getTargetObject();
    		if (pTemp.getBody() == null){
    			System.out.println("postingobject " + pTemp.getObjectID() + " " + pTemp.getType() + " " + pTemp.getCreator() + " " + pTemp.getTimestamp());
    		}
    		else{
    			System.out.println("postingobject " + pTemp.getObjectID() + " " + pTemp.getType() + " " + pTemp.getCreator() + " " + pTemp.getBody() + " " + pTemp.getTimestamp());
    		}
    		System.out.println("targetobject " + tTemp.getObjectID() + " " + tTemp.getType() + " " + tTemp.getCreator() + " " + tTemp.getBody() + " " + tTemp.getTimestamp());
    		System.out.println();
    	}
	}

    public void printRelatedPostings(String memberID, int objectID, int objectType,
				     String message, int timedecay){
	// print posted NFObjects related to the object specified with
	// parameters, memberID, objectID, objectType, message, and source_timedecay
	//
	// The specified nfobject can be any type of NFObject. 
	//
	// If the spedified nfobject is;
	// (1) wall posting, your homepage, or status change
	//     : it should return all of the postings those have the "nfobject" as their target posting.
	// (2) comment or like or visit
	//     : it should return the target object of this "nfobject" -- (a)
	//     : AND all the postings those have object (a) as their target posting
	//
	// In the case (2), there can be some postings published by a member who is not a friend of member(originally
	// specified by the "memberID" parameter). This method should return those postings as well.

	// add your code here
    	NFObject random = new NFObject(Integer.toString(objectID), objectType, memberID, message, Long.parseLong(Integer.toString(timedecay)));
    	
	}




  //NOTE: DO NOT MODIFY main().
    public static void main(String args[]){
	if (args.length < 3){
	    System.out.println("Please enter your input file and command.");
	    System.out.println("e.g. java PA4 [input_file][command1][command2]");
	}else{
	    
	    String input_file = args[0];
	    String cmd = args[1];
	    

	    PA4 pa4 = new PA4(input_file);
	    int type_size = 0;
		
	    if (cmd.equals("printListOfFriends")){
		pa4.printListOfFriends(args[2]);

	    }else if (cmd.equals("printPostingNFObject")){
		int [] type1 = new int [args.length - 4]; 
		for (int i = 0 ; i <  args.length - 4; i++){
		    type1[i] = Integer.parseInt(args[3+i]);
		
		}
		pa4.printPostingNFObject(args[2], type1 ,Integer.parseInt(args[args.length-1]) );
	    }else if (cmd.equals("printAffinityScore")){
		pa4.printAffinityScore(args[2],args[3]);
	
	    }else if (cmd.equals("printFriendsEdges")){
		pa4.printFriendsEdges(args[2]);

	    }else if(cmd.equals("printRelatedPostings")){
		pa4.printRelatedPostings(args[4], Integer.parseInt(args[2]), Integer.parseInt(args[3]), 
					 args[5],Integer.parseInt(args[6]));
	    }
	
	    
	}
    }
}