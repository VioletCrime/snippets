/**
 * CS 200 Colorado State University, Fall 2011
 */

public class NFObject {
	
	private String objectID;
	private int type;
	private String creator;
	private String body;
	private long timestamp;
	
	public NFObject (String _objectID,int _type, String _creator, String _body,long _timestamp){//Usual constructor
		objectID = _objectID;
		type = _type;
		creator = _creator;
		body = _body;
		timestamp = _timestamp;
	}
	
	public NFObject (String _objectID,int _type, String _creator,long _timestamp){ //'Visit' constructor
		objectID = _objectID;
		type = _type;
		creator = _creator;
		timestamp = _timestamp;
	}
	
	public String getObjectID(){
		return objectID;
	}
	
	public int getType(){
		return type;
	}
	
	public String getCreator(){
		return creator;
	}
	
	public String getBody(){
		return body;
	}
	
	public long getTimestamp(){
		return timestamp;
	}
	
	public void setObjectID(String _objectID){
		objectID = _objectID;
	}
	
	public void setType(int _type){
		type = _type;
	}
	
	public void setCreator(String _creator){
		creator = _creator;
	}
	
	public void setBody(String _body){
		body = _body;
	}
	
	public void setTimestamp(long _timestamp){
		timestamp = _timestamp;
	}
	
	public int getTimeDecay(){
		int result = 0; //Used to hold the time decay value.
		int timeHours = (int) timestamp / 60; //Convert time to hours
		if (timeHours < 6 || (timeHours == 6 && timestamp % 60 == 0)) //If time is less than or equal to 6 hours...
			result = 6;
		else if (timeHours < 12 || (timeHours == 12 && timestamp % 60 == 0)) //If time is over 6 hours, but less than or equal to 12.
			result = 5;
		else if (timeHours < 18 || (timeHours == 18 && timestamp % 60 == 0))//" " " 12 hours, " " " 18.
			result = 4;
		else if (timeHours < 24 || (timeHours == 24 && timestamp % 60 == 0))//" " " 18 hours, " " " 24.
			result = 3;
		else if (timeHours < 48 || (timeHours == 48 && timestamp % 60 == 0))//" " " 24 hours, " " " 48.
			result = 2;
		else if (timeHours < 72 || (timeHours == 72 && timestamp % 60 == 0))//" " " 48 hours, " " " 72.
			result = 1;
		return result;
	}
}
