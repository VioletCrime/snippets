/**
 * CS 200 Colorado State University, Fall 2011
 */
import java.util.Scanner; //Hooray for free code! I love you, free code!
import java.io.File;
import java.io.FileNotFoundException;

public class InformationParser{
	private static InformationParser informationParser = null;

	protected InformationParser(){
	}
	
	public static InformationParser getInstance(){
		if (informationParser==null) {
			informationParser = new InformationParser();
		}
		return informationParser;
	}
	
	public Member parseFile(String file_loc){ //Time to kick ass and chew bubble gum, and I have plenty of gum.

		Member dude = null; //Resulting Member object
		EdgeStack es = new EdgeStack(); //Edgestack we'll be filling and giving to 'dude'
		Scanner inny = null; //Input scanner
		File inStuff = new File(file_loc); //File to input
		
		
		try{ //Trying to hand the file to the input scanner. doesn't seem to properly catch =-/
			inny = new Scanner(inStuff);
		}
		catch (FileNotFoundException e){
			System.out.println("File not found.");
		}
		
		//The class' tourist attraction; does most all the work
		while (inny.hasNextLine()){
			String[] temp = inny.nextLine().trim().split(" ");//Split the next input line by whitespaces, trimmed to avoid whitespace-only lines
			if(temp[0] != "\n"){//Enter this block only if the line contains something other than a newline char
				if (temp[0].equals("member")){//If the line has the member information, ascribe the info to 'dude'
					dude = new Member(temp[1], temp[2], temp[3]);
				}
				if (temp[0].equals("edge")){//If the line is an edge, do a bunch...
					Edge tempE = new Edge(temp[1], Integer.parseInt(temp[2])); //...starting with make a new edge...
					temp = inny.nextLine().split(" "); //...and get the next line housing the PostingObject data.
					NFObject from;
					if (Integer.parseInt(temp[2]) == 0){//If it's a 'visit', we call a special NFObject constructor.
						from = new NFObject(temp[1], 0, temp[3], Long.parseLong(temp[4]));
					}
					else{ //If it's not a 'visit', call the usual NFObject constructor
						from = new NFObject(temp[1], Integer.parseInt(temp[2]), temp[3], temp[4], Long.parseLong(temp[5]));
					}
					tempE.setPostingObject(from); //Set the NFObject 'from' as edge's PostingObject
					temp = inny.nextLine().split(" "); //Get the next line; we already know this has to be TargetObject.
					NFObject to = new NFObject(temp[1], Integer.parseInt(temp[2]), temp[3], temp[4], Long.parseLong(temp[5])); //Simple constructor call
					tempE.setTargetObject(to); 
					tempE.calcScore(); //Calculate the Edge's *individual* score. Cumulative score is calculated by Member.bigCalc()
					es.push(tempE);//Add edge to the edgestack
				}
			}
		}
		dude.setEdgeStack(es);//Attribute the edgestack to member 'dude'
		dude.bigCalc();//Sum the scores of all related edges for each edge
		dude.sortScore();//Sort the edgestack by score.
		
		return dude;
	}

}