/**
 * CS 200 Colorado State University, Fall 2011
 * This is the base class for PA2. 
 * DO NOT MODYFY main(). Grading will be based on main() of this class.
 */

public class PA2{
    String input_file;
    Member dude;
    
    public PA2(String _input_file){
		input_file = _input_file;	
		InformationParser ip = InformationParser.getInstance();
		dude = ip.parseFile(input_file);
    }

    public void get_TargetingObjects(int id){
    	
    	//Solution 1: prints only the NFObjects which DIRECTLY target the given ID. Seems to be what is wanted.
    	EdgeStack sourceStack = dude.getEdgeStack().klone(); //Get a copy of the EdgeStack
    	while(!sourceStack.isEmpty()){ //Loop popping Edges looking for the right ID's
    		Edge temp = sourceStack.pop(); //Pop's into a temporary Edge
    		if (Integer.parseInt(temp.getTargetObject().getObjectID()) == id){ //If this object targets our id...
    			System.out.print(Integer.parseInt(temp.getPostingObject().getObjectID()) + " ");//... put it on screen.
    		}
    	}
    	
    	
    	//Alternate solution: prints all the objects RELATED to the given ID
    	/*int[] r = dude.getRelated(id); //Puts all related NFObject ID's in int array 'r'
    	for(int i = 0; r[i] != -1; i++){ //... and outputs them.
    		System.out.print(r[i] + " ");
    	}*/
    	
    }
	
    public void get_EdgeRankScore(int id){
    	EdgeStack sourceStack = dude.getEdgeStack().klone(); //Get a copy of the EdgeStack to destroy
    	while(!sourceStack.isEmpty()){
    		Edge temp = sourceStack.pop();//Traverse through the stack until we find the correct id...
    		if (Integer.parseInt(temp.getPostingObject().getObjectID()) == id){
    			System.out.print(temp.getEdgeRank()); //...and print its rank score on screen.
    		}
    	}
    }
	
    public void get_NFObject_ranked_at(int rank){
    	
    	EdgeStack trash = dude.getEdgeStack().klone(); //Get a copy of the EdgeStack.
    	int length = 0;
    	while(!trash.isEmpty()){//Destroys the stack copy to figure its length.
    		trash.pop();
    		length++;
    	}
    	
    	int stop = length - rank; //Figuring out how many we have to pop to get the ith largest score
    	EdgeStack sourceStack = dude.getEdgeStack().klone(); //Blew'd up the last one...
    	while(stop > 0){ //Blow this one up until we come to 'our stop'.
    		sourceStack.pop();
    		stop--;
    	}
    	Edge temp = sourceStack.pop(); //Get the Edge we're looking for, and output it to the screen.
    	System.out.println(temp.getPostingObject().getObjectID() +" "+ dude.getUserID() +" "+ temp.getPostingObject().getBody());
    	
    }

    //NOTE: DO NOT MODIFY main().
    public static void main(String args[]){
	
		if (args.length < 3){
			System.out.println("Please enter your input file and command.");
			System.out.println("e.g. java PA2 [input_file][command1][command2]");
		}else{
	    
			String input_file = args[0];
			String cmd1 = args[1];
			int cmd2 = Integer.parseInt(args[2]);
			
			PA2 pa2 = new PA2(input_file);
			
			// test case 1,2: print the entire object IDs targeting NFObject 
			// cmd2 is the object id of the targeted NFObject
			// e.g. pa2.get_targetingObjects(2) will return all of the
			// object ids of NFObjects those have NFObject with id 2 as their
			// target object. For instance, if NFObject with the object ID 2 
			// is a status change posting, this method will return all of the 
			// IDs of comments, visit and like object added to this posting.
			if (cmd1.equals("get_TargetingObjects")){
				pa2.get_TargetingObjects(cmd2);
			}else if (cmd1.equals("get_EdgeRankScore")){
				// test case 3,4: print the EdgeRankScore of NFObject with 
				// specified id.
				pa2.get_EdgeRankScore(cmd2);
			}else if (cmd1.equals("get_NFObject_ranked_at")){
				// test case 5,6: Print ID, owner's userID, and body of NFObject 
				// that is ranked at cmd2 in the sorted list.
				// e.g. get_NFObject_ranked_at(1) is the NFObject with the biggest
				// EdgeRank score.
				pa2.get_NFObject_ranked_at(cmd2);
			}
		}	
	}
}