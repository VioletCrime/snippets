import java.util.LinkedList;
import java.util.Stack;
import java.util.TreeMap;
import java.util.Vector;


public class Graph {
	private static int numVertices, numEdges;
	private static Vector<TreeMap<Integer, Integer>> adjList;
	
	public Graph(int n){
		numVertices = n;
		numEdges = 0;
		adjList = new Vector<TreeMap<Integer, Integer>>();
		for (int i = 0; i < numVertices; i++){
			adjList.add(new TreeMap<Integer, Integer>());
		}
	}
	
	public void addEdge(Integer _v, Integer _w, int _weight){
		adjList.get(_v).put(_w, _weight);
		adjList.get(_w).put(_v, _weight);
		numEdges++;
	}
	
	public static void dfs(int _v){
		boolean[] visited = new boolean[numVertices];
		Stack<Integer> stk = new Stack<Integer>();
		stk.push(_v);
		visited[_v] = true;
		
		while(!stk.isEmpty()){
			if (getUnvisitedNeighbor(stk.peek(), visited) == -1){
				int boobs = stk.pop();
				System.out.println(boobs);
			}
			else{
				stk.push(getUnvisitedNeighbor(stk.peek(), visited));
				visited[stk.peek()] = true;
			}
		}
	}
	
	public void bfs(Integer _v){
		boolean[] visited = new boolean[numVertices];
		LinkedList<Integer> q = new LinkedList<Integer>();
		visited[_v] = true;
		
		while(!q.isEmpty()){
			int x = q.getLast();
			q.remove(q.size() - 1);
		}
	}
	
	public static int getUnvisitedNeighbor(int _v, boolean[] ary){
		int result = -1;
		TreeMap<Integer, Integer> tm = adjList.get(_v);
		for (int i = 0; i < numVertices; i++){
			if (i != _v){
				if (tm.containsKey(i) && ary[i] != true){
					result = i;
					break;
				}
			}
		}
		
		return result;
	}
	
	public static void main(String[] args){
		Graph g = new Graph(4);
		g.addEdge(0, 1, 1);
		g.addEdge(0, 3, 1);
		g.addEdge(3, 1, 1);
		g.addEdge(3, 2, 1);
		g.addEdge(1, 2, 1);
		dfs(0);
	}
}
