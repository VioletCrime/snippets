
public interface StackADT {
	
	public boolean isEmpty();
	public Object peek();
	public void push(Object item);
	public Object pop();
	public void popAll();

}