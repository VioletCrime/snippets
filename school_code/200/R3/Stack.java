
public class Stack implements StackADT{

	private LinkedList linka; 
	@Override
	public boolean isEmpty() {
		return linka.size() == 0;
	}

	@Override
	public Object peek() {
		return linka.getObject(); //Added this function to the LinkedList class. It simply returns the Object...
		// ... contained in the 'head' node of the class.
	}

	@Override
	public void push(Object item) {
		linka.add(item);
		
	}

	@Override
	public Object pop() {
		Object a = linka.getObject();
		linka.remove(a);
		return a;
	}

	@Override
	public void popAll() {
		linka.clear();		
	}

}
