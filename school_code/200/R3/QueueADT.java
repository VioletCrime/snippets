
public interface QueueADT {

	public void enqueue(Object item);
	public Object dequeue();
	public Object peek();
	
}