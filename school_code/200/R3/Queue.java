
public class Queue implements QueueADT{

	private LinkedList linka;
	int front = -1, back = 0;
	@Override
	public void enqueue(Object item) {
		linka.add(front+1, item);
		front++;
	}

	@Override
	public Object dequeue() {
		Object a = linka.getObject(); //Added this function to the LinkedList class. It simply returns the Object...
		// ... contained in the 'head' node of the class.
		linka.remove(back);
		back++;
		return a;
	}

	@Override
	public Object peek() {
		return linka.getObject();
	}

}
