
public class BST {
	public static void main(String[] args){
		Node root = new Node();
		root.insert(5);
		root.insert(3);
		root.insert(7);
		root.insert(2);
		root.insert(6);
		root.insert(4);
		root.insert(-1);
		root.insert(12);
		root.insert(18);
		root.insert(15);
		root.insert(-6);
		root.insert(-2);
		root.traverse();
	}
}
