import java.util.Scanner;
public class Hanoi {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Which Lucas number do you want?");
		Scanner inny = new Scanner(System.in);
		int usr = inny.nextInt();
		int lukuz = lucas(usr);
		System.out.println("The " + usr + " number in the Lucas sequence is " + lukuz + "\n\n");
		System.out.println("How many tiles on the towers of Hanoi?");
		int wat = inny.nextInt();
		solveHanoi(wat, 1, 2, 3);
	}
	
		
		
		

	public static int lucas(int in){
		if (in == 1)
			return 2;
		if (in == 2)
			return 1;
		return lucas(in-1) + lucas(in-2);
	}
	
	static void move(int fromPole, int toPole){
		System.out.println("Moving from pole " + fromPole + " to pole " + toPole + ".");
	}
	
	static void solveHanoi(int numTiles, int fromPole, int toPole, int withPole){
		if (numTiles == 1)
			move(fromPole, withPole);
		else{
			solveHanoi(numTiles - 1, fromPole, withPole, toPole);
			move(fromPole, withPole);
			solveHanoi(1, fromPole, toPole, withPole);
			move(fromPole, toPole);
			solveHanoi(numTiles-1, withPole, toPole, fromPole);
			move(withPole, toPole);
		}
	}
}
