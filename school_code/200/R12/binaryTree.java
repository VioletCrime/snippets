public class binaryTree {
	
	static int size;
	static int[] arrayy;
	
	public static void main(String[] args){
		size = 0;
		arrayy = new int[size];
		insert(5);
		insert(2);
		insert(6);
		insert(1);
		insert(8);
		insert(-2);
		printOut(arrayy);
		remove(0);
		printOut(arrayy);
		
	}
	
	public static void insert(int addit){//REALLY shitty optimization... 
		int[] temp = new int [size + 1];
		
		for(int i = 0; i < size; i++){
			temp[i] = arrayy[i];
		}
		temp[size] = addit;
		size++;
		arrayy = new int[size];
		for(int i = 0; i < size; i++){
			arrayy[i] = temp[i];
		}
		percolate(size-1);
	}
	
	public static int getLeftChild(int i){
		return 2 * i+1;
	}
	
	public static int getRightChild(int i){
		return 2*(i+1);
	}
	
	public static int getParent(int i){
		return (i-1)/2;
	}
	
	public static boolean hasLeft(int i){
		int j = 2 * i+1;
		return j <= (arrayy.length - 1);
		
	}
	
	public static boolean hasRight(int i){
		int j = 2*(i+1);
		return j <= (arrayy.length - 1);
	}
	
	public static void percolate(int place){
		if (getParent(place) >= 0 && place > 0){
			if(arrayy[place] < arrayy[getParent(place)]){
				int temp2 = arrayy[place];
				arrayy[place] = arrayy[getParent(place)];
				arrayy[getParent(place)] = temp2;
				percolate(getParent(place));
			}
			else return;
		}
		else return;
	}
	
	public static void printOut(int[] ary){
		if(ary.length > 0){
			System.out.print(ary[0]);
			for(int i = 1; i < size; i++){
				System.out.print(", ");
				System.out.print(ary[i]);
			}
			System.out.println();
		}
	}
	
	public static void remove(int index){
		size--;
		arrayy[index] = arrayy[arrayy.length - 1];
		arrayy = shrinkAry();
		remSort(index);
	}
	
	public static int[] shrinkAry(){
		int[] temp = new int[arrayy.length-1];
		for (int i = 0; i < temp.length; i++){
			temp[i] = arrayy[i];
		}
		return temp;
	}
	
	public static void remSort (int index){
		if(hasLeft(index) && hasRight(index)){
			if (arrayy[index] > arrayy[getParent(index)] && arrayy[index] < arrayy[getLeftChild(index)] && arrayy[index] < arrayy[getRightChild(index)]){
				return;
			}
			else if (arrayy[index] < arrayy[getParent(index)]){
				percolate(index);
				return;
			}
			else if (arrayy[index] > arrayy[getLeftChild(index)] || arrayy[index] > arrayy[getRightChild(index)]){
				sinker(index);
			}
		}
		else if (hasLeft(index)){
			if (arrayy[index] > arrayy[getParent(index)] && arrayy[index] < arrayy[getLeftChild(index)]){
				return;
			}
			else if (arrayy[index] < arrayy[getParent(index)]){
				percolate(index);
				return;
			}
			else if (arrayy[index] > arrayy[getLeftChild(index)]){
				sinker(index);
			}
		}
		else{
			if (arrayy[index] > arrayy[getParent(index)]){
				return;
			}
			else if (arrayy[index] < arrayy[getParent(index)]){
				percolate(index);
				return;
			}
		}
	}
	
	public static void sinker(int index){
		if (hasLeft(index) && hasRight(index)){
			if (arrayy[index] < arrayy[getLeftChild(index)] && arrayy[index] < arrayy[getRightChild(index)]){
				return;
			}
			else if (arrayy[index] > arrayy[getLeftChild(index)] && arrayy[index] > arrayy[getRightChild(index)]){
				if (arrayy[getLeftChild(index)] < arrayy[getRightChild(index)]){
					int temp = arrayy[index];
					arrayy[index] = arrayy[getLeftChild(index)];
					arrayy[getLeftChild(index)] = temp;
					sinker(getLeftChild(index));
				}
				else{
					int temp = arrayy[index];
					arrayy[index] = arrayy[getRightChild(index)];
					arrayy[getRightChild(index)] = temp;
					sinker(getRightChild(index));
				}
			}
			else if (arrayy[index] > arrayy[getLeftChild(index)]){
				int temp = arrayy[index];
				arrayy[index] = arrayy[getLeftChild(index)];
				arrayy[getLeftChild(index)] = temp;
				sinker(getLeftChild(index));
			}
			else if (arrayy[index] > arrayy[getRightChild(index)]){
				int temp = arrayy[index];
				arrayy[index] = arrayy[getRightChild(index)];
				arrayy[getRightChild(index)] = temp;
				sinker(getRightChild(index));
			}
		}
		
		else if (hasLeft(index)){
			if (arrayy[index] < arrayy[getLeftChild(index)]){
				return;
			}
			else{
				int temp = arrayy[index];
				arrayy[index] = arrayy[getLeftChild(index)];
				arrayy[getLeftChild(index)] = temp;
				sinker(getLeftChild(index));
			}
		}
		
		else{
			return;
		}
	}
	
	public static void decreaseKey(int i, int amount){
		arrayy[i] -= amount;
		remSort(i); 
	}
}