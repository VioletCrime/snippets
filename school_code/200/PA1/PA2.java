/**
 * CS 200 Colorado State University, Fall 2011
 * This is the base class for PA2. 
 * DO NOT MODYFY main(). Grading will be based on main() of this class.
 */

public class PA2{
    String input_file;
    Member dude;
    
    public PA2(String _input_file){
		input_file = _input_file;	
		InformationParser ip = InformationParser.getInstance();
		dude = ip.parseFile(input_file);
    }

    public void get_TargetingObjects(int id){
    	while(!dude.getEdgeStack().isEmpty()){
    		if(Integer.parseInt(dude.getEdgeStack().pop().getTargetObject().getObjectID()) == id)
    			System.out.print(Integer.parseInt(dude.getEdgeStack().pop().getPostingObject().getObjectID()) + " ");
    	}		
    }
	
    public void get_EdgeRankScore(int id){
		// Add your code here
		
    }
	
    public void get_NFObject_ranked_at(int rank){
		// This method prints the ID, owner's userID, and body of the NFobject that 
		// is ranked at the specified input parameter:rank. 
		// e.g. get_NFObject_with_biggest_ERscore(1) will return the ID of the NFObject
		// that has the biggest ERScore. get_NFObject_with_biggest_ERscore(2) will return
		// the ID of the NFObject that has second biggest ERScore.
		// Add your code here
		
    }


    //NOTE: DO NOT MODIFY main().
    public static void main(String args[]){
	
		if (args.length < 3){
			System.out.println("Please enter your input file and command.");
			System.out.println("e.g. java PA2 [input_file][command1][command2]");
		}else{
	    
			String input_file = args[0];
			String cmd1 = args[1];
			int cmd2 = Integer.parseInt(args[2]);
			
			PA2 pa2 = new PA2(input_file);
			
			// test case 1,2: print the entire object IDs targeting NFObject 
			// cmd2 is the object id of the targeted NFObject
			// e.g. pa2.get_targetingObjects(2) will return all of the
			// object ids of NFObjects those have NFObject with id 2 as their
			// target object. For instance, if NFObject with the object ID 2 
			// is a status change posting, this method will return all of the 
			// IDs of comments, visit and like object added to this posting.
			if (cmd1.equals("get_TargetingObjects")){
				pa2.get_TargetingObjects(cmd2);
			}else if (cmd1.equals("get_EdgeRankScore")){
				// test case 3,4: print the EdgeRankScore of NFObject with 
				// specified id.
				pa2.get_EdgeRankScore(cmd2);
			}else if (cmd1.equals("get_NFObject_ranked_at")){
				// test case 5,6: Print ID, owner's userID, and body of NFObject 
				// that is ranked at cmd2 in the sorted list.
				// e.g. get_NFObject_ranked_at(1) is the NFObject with the biggest
				// EdgeRank score.
				pa2.get_NFObject_ranked_at(cmd2);
			}
		}	
	}
}