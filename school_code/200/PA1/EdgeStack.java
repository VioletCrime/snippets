/**
 * CS 200 Colorado State University, Fall 2011
 */
import java.util.LinkedList;

public class EdgeStack {
	
	private LinkedList<Edge> llist = new LinkedList<Edge>();
	private int index;
	
	public EdgeStack(){
		index = -1;
	}
	
	public boolean isEmpty(){
		return index == -1;
	}
		
	public void push(Edge e){
		llist.add(e);
		index++;
	}

	public Edge pop(){
		index--;
		return llist.remove(index + 1);
	}
	
	public Edge peek(){
		return llist.get(index);
	}
	
	public LinkedList<Edge> popAll(){
		llist.clear();
		index = -1;
		return llist;
	}
}
