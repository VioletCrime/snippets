/**
 * CS 200 Colorado State University, Fall 2011
 */
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
public class InformationParser{
	private static InformationParser informationParser = null;

	protected InformationParser(){
	}
	
	public static InformationParser getInstance(){
		if (informationParser==null) {
			informationParser = new InformationParser();
		}
		return informationParser;
	}
	
	public Member parseFile(String file_loc){
		Member dude = null;
		EdgeStack es = new EdgeStack();
		Scanner inny = null;
		File inStuff = new File(file_loc);
		try{
			inny = new Scanner(inStuff);
		}
		catch (FileNotFoundException e){
			System.out.println("File not found.");
		}
		while (inny.hasNextLine()){
			String[] temp = inny.nextLine().split(" ");
			if(temp[0] != "\n"){
				if (temp[0].equals("member")){
					dude = new Member(temp[1], temp[2], temp[3]);
				}
				if (temp[0].equals("edge")){
					Edge tempE = new Edge(temp[1], Integer.parseInt(temp[2]));
					temp = inny.nextLine().split(" ");
					NFObject from;
					if (Integer.parseInt(temp[2]) == 0){
						from = new NFObject(temp[1], 0, temp[3], Long.parseLong(temp[4]));
					}
					else{
						from = new NFObject(temp[1], Integer.parseInt(temp[2]), temp[3], temp[4], Long.parseLong(temp[5]));
					}
					tempE.setPostingObject(from);
					temp = inny.nextLine().split(" ");
					NFObject to = new NFObject(temp[1], Integer.parseInt(temp[2]), temp[3], temp[4], Long.parseLong(temp[5]));
					tempE.setTargetObject(to);
					tempE.calcScore();
					es.push(tempE);
				}
			}
		}
		dude.setEdgeStack(es);
		return dude;
	}

}