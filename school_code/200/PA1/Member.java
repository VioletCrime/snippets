/**
 * CS 200 Colorado State University, Fall 2011
 */

public class Member {

	private String userID;
	private String first;
	private String last;
	private EdgeStack edgeStack;
	
	//constructor
	public Member(){	
		userID = null;
		first = null;
		last = null;
	}
	
	public Member(String _userID, String _first, String _last){
		userID = _userID;
		first = _first;
		last = _last;
	}
	
	public String getUserID(){
		return userID;
	}

	public String getFirst(){
		return first;
	}
	
	public String getLast(){
		return last;
	}
	
	public EdgeStack getEdgeStack(){
		return edgeStack;
	}	
	
	public void setUserID(String _userID){
		userID = _userID;
	}
	
	public void setFirst(String _first){
		first = _first;
	}
	
	public void setLast(String _last){
		last = _last;
	}
	
	public void setEdgeStack(EdgeStack _edgeStack){
		edgeStack = _edgeStack;
	}
	
	public EdgeStack sortScore(EdgeStack _edgestack){
		EdgeStack temp = new EdgeStack();
		EdgeStack less = new EdgeStack();
		EdgeStack greater = new EdgeStack();
		EdgeStack pivot = new EdgeStack();
		
		if(_edgestack.isEmpty())
			return _edgestack;
		pivot.push(_edgestack.pop());
		while(!_edgestack.isEmpty()){
			if(_edgestack.peek().getEdgeRank() <= pivot.peek().getEdgeRank())
				less.push(_edgestack.pop());
			else greater.push(_edgestack.pop());
		}
		
		sortScore(less);
		sortScore(greater);
		
		while(!less.isEmpty()){
			temp.push(less.pop());
		}
		temp.push(pivot.pop());
		while(!greater.isEmpty()){
			temp.push(greater.pop());
		}
		while(!temp.isEmpty()){
			System.out.println(temp.pop().getEdgeRank());
		}
		return temp;
		
	}
	
}
