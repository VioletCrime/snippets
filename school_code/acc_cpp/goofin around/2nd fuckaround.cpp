#include <iostream>
#include <cmath>
#include <iomanip>
#include <cctype>

using namespace std;

void defaultParam(int u, int v = 5, double z = 3.2);

int main()
{
    defaultParam(3, 0, 2.8);
    
    system("pause");
    return 0;
}

void defaultParam(int u, int v, double z)
{
     //cout << "u = " << u << ", v = " << v << ", z = " << z << endl;
     int a;
     u = u + static_cast<int> (2 * v + z);
     a = u + v * z;
     cout << "a = " << a << endl;
}
