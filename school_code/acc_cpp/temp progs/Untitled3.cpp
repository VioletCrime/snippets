#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main()
{
    int age;
    char ch;
    string name;
    
    cout << "Input from Ch3, Prob 8..." << endl;
    cin >> age;
    cin.get(ch);
    getline(cin, name);
    
    cout << "age = " << age << endl << "ch = " << ch << endl << "'name' = " << name << endl << endl;
    system("pause");
    
    return 0;
}
