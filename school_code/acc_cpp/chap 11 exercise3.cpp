/* Chapter 11 exerxise 3
 * Creator: Andrew Williams
 * date: april 30 2010
 * Purpose: 1
 */

#include <iostream>
#include <cstring>
using namespace std;

class Daytype  // Daytype class declaration
{
public:			// public functions of class
	Daytype();
	void set_day(int index1);
	void print_day();
	void get_day(char passday[6]);
	void get_tomorrow(char passday[6]);
	void get_yesterday(char passday[6]);
	void get_xdays(int index2,char passday[6]);
private:		// private variables of class
	int index;
	char days[8][6];
};
/* -----------------------------------------------------------------------------
 * Function definitions for running class Daytype
 *		Daytype will set the day of the week and tell the user the current day,
 *		previous day, next day, and the day that is x number of days ahead. 
 *		
 *	function set_day requires the input of a int variable from 1-7 to set the day 
 *		of the week to 1 = Sun, 2 = Mon, 7 = Sat.
 *	function print_day requires no input and will print the day that class Daytype
 *		is set to to the console, default for current day is Sun
 *	function get_day requires the input of a char [6] and will copy the day of the
 *		week into that char variable.
 *	function get_tomorrow requires the input of a char [6] and will copy the next 
 *		day into that char variable.
 *	function get_yesterday requires the input of a char[6] and will coppy the previous
 *		day into that char variable.
 *	funciton get_xdays reqires the input of an int which will be the number of days 
 *		forward you would like to calculate, and a char [6] variable to copy that calculatd 
 *		day into.
 *-----------------------------------------------------------------------------------*/

bool test(Daytype Daylist); // test functinon prototype

int main()
{
	int input1 = 0,		// declares input variables
		input2 = 0;
	char day[6];
	Daytype Daylist;	// declares instance of class Daytype
	cout << "Please select a day" << endl;	// promps user for input to set currnt day to
	cout << "1 = sunday\n2 = monday\n3 = Tuesday\n4 = Wednesday\n"
		<< "5 = Thursday\n6 = Friday\n7 = Saturday\n";

	cin >> input1;

	Daylist.set_day(input1); // calls Daytype funciton to set the day
	cout << "The day is ";
	Daylist.print_day();	// calls Daytype function to print current day to console
	cout << "\n\n";

	Daylist.get_day(day);	// calls Daytype function to copy current day into char day
	cout << "The day is " << day << endl;

	Daylist.get_tomorrow(day);	// calls Daytype function to copy tommorrow into char day
	cout << "Tommorrow is " << day << endl;

	Daylist.get_yesterday(day);	// calls Daytype function to copy yesterday into char day
	cout << "Yesterday is " << day << endl;

	cout << "How many days would you like to add? "; // promps user to select how many days ahead they would like to know
	cin >> input2;
	cout << input2 << " days away is ";
	Daylist.get_xdays(input2,day);	// calls Daytype function to copy day that is x days ahead into char day
	cout << day << endl;			
	
	if(test(Daylist)!= true) // bool test to check that Class Daytype is running correctly
		cout << "---- Error occured during test ---- /nPlease contact the software developer";
	else 
		cout << "\n ---- Test complete no errors ---- \n\n";
	return 0;
}

bool test(Daytype Daylist)
{
	char test1[6]= "Tues";	// declares and sets char strings to test class daytype against
	char test2[6]= "Wed";
	char test3[6]= "Mon";
	char test4[6]= "Sat";
	char day1[6];			// declares char srings for class Daytype to copy informatin into
	char day2[6];
	char day3[6];
	char day4[6];

	int input1,input2;
	input1 = 3;
	input2 = 11;

	Daylist.set_day(input1);	// calls functions of Class Daytype
	Daylist.get_day(day1);
	Daylist.get_tomorrow(day2);
	Daylist.get_yesterday(day3);
	Daylist.get_xdays(input2,day4);

	if(strcmp(test1, day1)!= 0)		// if else if statement to compare the returned value of class daytype to 
		return false;				// known correct values in test char

	else if(strcmp(test2, day2)!= 0)
		return false;

	else if(strcmp(test3, day3)!= 0)
		return false;

	else if(strcmp(test4, day4)!= 0)
		return false;
	else 
	return true;
}

Daytype::Daytype() // constructor which sets values into char array
{
index = 0;
strcpy (days[0],"Sun");
strcpy (days[1],"Mon");
strcpy (days[2],"Tues");
strcpy (days[3],"Wed");
strcpy (days[4],"Thurs");
strcpy (days[5],"Fri");
strcpy (days[6],"Sat");
strcpy (days[7],"Error");
}
void Daytype::set_day(int index1) // setday function which sets private valeu of the current day to user input
{
 index = index1 -1;
 if(index < 0 || index > 6)		// if input value out of range returns error
	 index = 7;
}

void Daytype::print_day()	// print day function prints the current day to console
{
	cout << days[index];
}

void Daytype::get_day(char passday[6])	// function which copies the value of curent day to outside char string
{
	strcpy(passday,days[index]);
}

void Daytype::get_tomorrow(char passday[6]) // function which copies the value of next day to outside char string
{
	int x = 0;
	x = index;
	if(index == 7)
	{
		strcpy(passday,days[7]);
		return;
	}
	x++;
	if (x > 6)
		x = 0;
	strcpy(passday,days[x]);
}
void Daytype::get_yesterday(char passday[6])// function which copies the value of previous day to outside char string
{
	int x = 0;
	x = index;
	if(index == 7)
	{
		strcpy(passday,days[7]);
		return;
	}
	x--;
	if (x < 0)
		x = 6;
	strcpy(passday,days[x]);
}
void Daytype::get_xdays(int index2,char passday[6]) // function which calculates the value x days ahead and copies
{													// that value into outside char string
	int x = 0;
	x = index;
	if(index == 7)
	{
		strcpy(passday,days[7]);
		return;
	}
	if(index2 < 0)
	{
		strcpy(passday,days[7]);
		return;
	}

	while(index2 > 0,index2--)
	{
		x++;
		if (x > 6)
		x = 0;
	}
	strcpy(passday,days[x]);
}