/* PRYME TYME by Kyle Smith, No-One's Productions
Made for CIS160 C11 Spring 2010 */

#include <iostream>   

using namespace std;

int main()
{
    int num;   //integer for user input
    int rewt;  //integer used to store the value of the square root of 'num'
    
    cout << "Enter a positive integer greater than 1: "; //Prompting the user
    cin >> num;  //YOU GOT WHAT I NEED!!!
    cout << endl << endl << "The number you entered is: " << num << endl;  //Reiterating
    
    rewt = num - 1; //Setting 'rewt' equal to 'num', subtracting 1 immediatly to avoid dividing by itself
    
    do  //Initiating a 'do...while' control structure to test, update, test, update, etc....
    {  //hajimaru
         if (rewt == 1) //If the rewt value falls to 1, it has not found a clean divisible number...
            cout << "It is a prime number" << endl << endl; //...and therefore 'num' is a prime!
            
         else if (num % rewt == 0) //If the user input divided by the 'rewt' integer leaves no remainder...
         {
                 cout << "It is not a prime number" << endl << endl; //...then it's not a prime number
                 break; //No use continuing once we've established 'num' to be non-prime
         } //ending the 'else if' statement
         rewt--; //modifying the control variable 
    }
    while (rewt >= 1); //Setting the condition for the 'do...while' control structure
    
    system("pause"); //Take a moment to absorb the sheer awesome that is your prime / non-prime
    return 0;  //Clean operation! ^_^
}  //Nothing left to see here, folks! Move along, now.
