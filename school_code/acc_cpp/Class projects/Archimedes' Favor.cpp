/* Archimedes' Favor by Kyle Smith, No-One's Productions
Made for CIS160 C11, Spring 2010*/

#include <iostream>   //Some have inny's, some have outy's
#include <iomanip>    //Making the output look purdy
#include <cmath>      //Going to require powers and roots to calculate the radius

using namespace std;  //It's like deja vu all over again

float radius(float xa, float xb, float ya, float yb); //declaring the radius funtion
float diameter(float r);  //declaring diameter function
float circumference(float r, float pi);  //delcaring circumference function
float area(float r, float pi);  // declaring area function

int main()   //Kick the tires and light the fires!!
{
    float xa, xb, ya, yb, r;  //r is for the radius, the x's and y's are here for coordinates
    const float pi = 3.1416;  //Declaring pi as a constant value
    
    cout << "Enter the x and y coordinates of the center of the circle: "; //Prompting for x1, y1...
    cin >> xa >> ya; //... = xa, ya respectively
    cout << endl << endl << "Enter the x and y coordinates of a point on the circle: "; //x2 and y2...
    cin >> xb >> yb; //... predicatbly = xb and yb
    cout << endl << endl;
    
    r = radius(xa, xb, ya, yb); //solving for the radius and storing it here simplifies the upcoming cout's
    cout << fixed << setprecision(2) << showpoint << left;  //Setting output left margin, 2 decimal places including 0's
    cout << setw(16) << "Radius =" << r << endl; //Already calculated r; this one's simple
    cout << setw(16) << "Diameter =" << diameter(r) << endl; //Calling the diameter function to manipulate r for diameter
    cout << setw(16) << "Circumference =" << circumference(r, pi) << endl; // "" circumference function ""  "" for circumference
    cout << setw(16) << "Area =" << area(r, pi) << endl << endl; //Calling area() to work with r... predicatbly for area
    
    system("pause");  //Hold on a minute, slick!
    return 0;  // Hooray!
}

float radius(float xa, float xb, float ya, float yb) //Time to define the radius() function
{
      return sqrt(pow((xb - xa), 2) + pow((yb - ya), 2)); //return the value given by the distance formula
} //short and sweet, no?

float diameter(float r) // Defining diameter() this time
{
      return (r * 2); // Doesn't come much simpler than this
}

float circumference(float r, float pi) //Defining circumference() function
{
      return (2 * pi * r); //Just a hair less simple...
}

float area(float r, float pi) //Defining the area() function
{
      return (pi * pow(r, 2));  //pi*r^2 
} 
