/*Chapter 3 Programming Exercise 1: It's effectively
"set up a basic program to open 2 files for reading and writing"*/

#include<iostream>
#include<fstream>
#include<iomanip>
#include<string>

using namespace std;

int main()
{
	char discard;
	ifstream inny;
	ofstream outty;
	inny.open("inData.txt");
	outty.open("outData.txt");
	float length, width, radius, balance, interest;
	string fName, lName;
	char rand;
	int age;
	const float pi = 3.1416;
	inny >> length >> width >> radius >> fName >> lName >> age >> balance >> interest >> rand;
	inny.close();
	outty << fixed << setprecision(2) << left; 
	outty << "Rectangle:\nLength = " << length << ", width = " << width << ", area = " << length * width << ", perimeter = " << 2*length + 2*width << endl << endl;
	outty << "Circle:\nRadius = " << radius << ", area = " << radius * radius * pi << ", circumference = " << 2*radius*pi << endl << endl;
	outty << "Name: " << fName << " " << lName << ", age: " << age << endl;
	outty << "Beginning balance = " << balance << ", interest rate = " << interest << endl;
	outty << "Balance at the beginning of the month = " << balance + balance * (interest*.01) << endl << endl;
	outty << "The character that comes after " << rand << " in the ASCII set is " << static_cast<char>(rand +1);
	outty.close();


}//end main()
