/*Chapter 3 Programming Exercise 6.
This program needs to open an input and output file, read employee names, salaries,
and % salary increase. Then increase the salary by the amount, and output to file.
Simple, right?*/

#include<iostream>
#include<fstream>
#include<iomanip>

using namespace std;

int main()
{
	string fName, lName;
	float salS, salF, inc;
	ifstream inny;
	ofstream outty;
	inny.open("Ch3_Ex6Data.txt");
	outty.open("Ch3_Ex6Output.txt");
	inny >> lName >> fName >> salS >> inc;
	salF = salS + salS * (inc * .01);
	outty << fixed << setprecision(2) << fName << " " << lName << " " << salF << endl;
	inny >> lName >> fName >> salS >> inc;
	salF = salS + salS * (inc * .01);
	outty << fName << " " << lName << " " << salF << endl;
	inny >> lName >> fName >> salS >> inc;
	salF = salS + salS * (inc * .01);
	outty << fName << " " << lName << " " << salF << endl;

	inny.close();
	outty.close();

	return 0;


}//End main()
