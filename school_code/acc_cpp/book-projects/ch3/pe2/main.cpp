/*Chapter 3 Programming Exercise 2... prompt for a decimal-laden number and round
to 2 places; assuming output of this value...*/

#include<iostream>
#include<iomanip>

using namespace std;

int main()
{
	float storage;
	cout << "Input a decimal-laden value:" << endl;
	cin >> storage;
	cout << fixed << setprecision(2) << endl << endl << "The value you entered rounded to 2 places is: " << storage << endl;


}//End main()
