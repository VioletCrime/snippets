/*Chapter 3 Programming Exercise 7
Get 2 inputs from user (mass and density), and output volume.

Oh noes?*/

#include<iostream>
#include<iomanip>

using namespace std;

int main()
{
	float mass, density, volume;
	cout << "Input the object's mass (g): ";
	cin >> mass;
	cout << endl << "Input the object's density (g/m^3): ";
	cin >> density;
	volume = mass / density;
	cout << endl << "The volume of the object is " << fixed << setprecision(2) << volume << " m^3." << endl << endl;

	return 0;


}
