/*Chapter 3 Programming Exercise 5
Rewrite programming exercise 22 from chapter 2 to incorporate the use of
full-line strings. PE22 wants me to receive 3 strings from user, and output
all 6 combinations of the 3 strings.

Output isn't terribly pretty, but it does what's been asked.*/

#include<iostream>
#include<string>

using namespace std;

int main()
{
	string a, b, c;
	cout << "Input 3 strings: ";
	getline(cin, a);
	getline(cin, b);
	getline(cin, c);

	cout << a << b << c << endl << endl;
	cout << a << c << b << endl << endl;
	cout << b << a << c << endl << endl;
	cout << b << c << a << endl << endl;
	cout << c << a << b << endl << endl;
	cout << c << b << a << endl << endl;


}//End main()
