/*Chapter 3, Programming exercise 3
The purpose is to create a program that will open a file containing
ticket sales information, and return total seats sold and sales.  
Assuming output to both screen and file.
Input "input.txt", output "output.txt"*/

#include<iostream>
#include<fstream>
#include<iomanip>

using namespace std;

int main()
{
	int boxP, sideP, premP, genP, totalSeats;
	double boxS, sideS, premS, genS, totalS;
	ifstream inny;
	ofstream outty;
	inny.open("input.txt");
	outty.open("output.txt");
	inny >> boxP >> boxS >> sideP >> sideS >> premP >> premS >> genP >> genS;
	inny.close();
	totalSeats = boxS + sideS + premS + genS;
	totalS = boxS*boxP + sideS*sideP + premS*premP + genS*genP;
	cout <<  "Total seats sold : " << totalSeats << fixed << setprecision(2) << "\nTotal sales : " << totalS << endl << endl;
	outty << "Total seats sold : " << totalSeats << fixed << setprecision(2) << "\nTotal sales : " << totalS;

	return 0;
	

}//End main()
