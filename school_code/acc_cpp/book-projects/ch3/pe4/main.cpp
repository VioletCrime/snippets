/*Chapter 3 Programming Exercise 4.
Gotta prompt user for name and gross monthly paycheck and output
taxes and deductions, finally printing net pay.*/

#include<iostream>
#include<iomanip>
#include<string>

using namespace std;

int main()
{
	string fName, lName;
	double grossPay, netPay, fT, sT, ssT, mT, pT, hT;
	cout << "Please input your first and last name: ";
	cin >> fName >> lName;
	cout << endl << "Please input gross pay: ";
	cin >> grossPay;
	fT = grossPay * .15;
	sT = grossPay * .035;
	ssT = grossPay * .0575;
	mT = grossPay * .0275;
	pT = grossPay * .05;
	hT = 75;
	netPay = grossPay - fT - sT - ssT - mT - pT - hT;
	cout << endl << fName << " " << lName << endl;
	cout << setw(26) << setfill('.') << fixed << setprecision(2) << left << "Gross Amount: " << " $" << setw(7) << setfill(' ') << right << grossPay << endl;
	cout << setw(26) << setfill('.') << left << "Federal Tax: " << " $" << setw(7) << setfill(' ') << right << fT << endl;
	cout << setw(26) << setfill('.') << left << "State Tax: " << " $" << setw(7) << setfill(' ') << right << sT << endl;
	cout << setw(26) << setfill('.') << left << "Social Security Tax: " << " $" << setw(7) << setfill(' ') << right << ssT << endl;
	cout << setw(26) << setfill('.') << left << "Medicare/Medicaid Tax: " << " $" << setw(7) << setfill(' ') << right << mT << endl;
	cout << setw(26) << setfill('.') << left << "Pension Plan: " << " $" << setw(7) << setfill(' ') << right << pT << endl;
	cout << setw(26) << setfill('.') << left << "Health Insurance: " << " $" << setw(7) << setfill(' ') << right << hT << endl;
	cout << setw(26) << setfill('.') << left << "Net Pay: " << " $" << setw(7) << setfill(' ') << right << netPay << endl << endl;


}//End main()


