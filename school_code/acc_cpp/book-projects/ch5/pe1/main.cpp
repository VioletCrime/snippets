/*Chapter 5 PE1
Write a program what takes an integer from the user, displays each character, and outputs the sum of the characters.

...could get interesting...?*/

#include<iostream>

using namespace std;

int main()
{
	int inny, sinny;
	int ch1, ch2;
	int total = 0;
	cout << endl << "Please enter an integer: ";
	cin >> inny;
	
	cout << endl << "The individual digits of " << inny << " are:";
	sinny = 0;

	while(inny > 0)
	{
		ch2 = inny % 10;
		sinny = sinny * 10 + ch2;
		inny = inny / 10;
	}//End while()

	while(sinny > 0)
	{
		ch2 = sinny % 10;
		total = total + ch2;
		sinny = sinny / 10;
		cout << " " << ch2;
	}//End while()

	cout << "\nAnd the sum of the digits is: " << total << endl << endl;
	
	return 0;


}//End main()
