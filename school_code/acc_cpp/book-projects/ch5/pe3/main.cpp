/*Chapter 5 PE16
Prompt user for the number of lockers in a school, run through a series of thinking stuff to close / open lockers
0 = closed 1 = open.*/

#include<iostream>
#include<iomanip>

using namespace std;

int main()
{
	int noStud, lc, ph, counter;
	int temp = 0;
	cout << endl << "Enter the number of students in school: ";
	cin >> noStud;
	cout << "The lockers which end open are: " << endl << endl;
	for(lc = 1; lc <= noStud; lc++)
	{
		counter = 0;
		for(ph = 1; ph <= lc; ph++)
		{
			if(lc % ph == 0)
				counter++;
		}
		if(counter % 2 == 1)
		{
			cout << "Locker #" << left << setw(5) << setfill(' ') << lc << "   ";
			temp++;
		}
		if(temp == 10)
		{
			cout << endl;
			temp = 0;
		}
	}
	cout << endl << endl;
	return 0;
}//End main()

