/*Chapter 5 PE 18
Same as CH5PE17, only this time with added features (one of which I already incorporated)*/

#include<iostream>
#include<iomanip>

using namespace std;

int main()
{
	double loan, interest, monthly, temp, intPaid;
	int lc;

	intPaid = 0;
	
	cout << "How much is your loan for? ";
	cin >> loan;
	cout << "And what is your interest rate? ";
	cin >> interest;
	temp = loan * ((interest / 12) * .01);
	cout << "Your minimum monthly payment is: " << setprecision(2) << fixed << temp;
	cout << ".\nHow much do you wish to pay monthly? ";
	cin >> monthly;
	while(temp >= monthly)
	{
		cout << endl << "You will never be able to pay off your loan at that montly payment rate; please decide on a\n";
		cout << "monthly payment greater than " << temp << ": ";
		cin >> monthly;
	}

	for(lc = 1; loan >= 0; lc++)
	{
		temp = loan * ((interest / 12) * .01);
		intPaid = intPaid + temp;
		if((monthly -temp) >= loan)
		{
			cout << endl << "It will take " << lc << " months to pay off your loan, after having paid $";
			cout << intPaid << " in interest." << endl;
			cout << "On the final month, you will owe $" << loan << ", and your final payment will be $";
			cout << loan + temp << ".";
		}
		loan = loan - (monthly - temp);
	}

	cout << endl << endl;
	return 0;
}//End main() 
