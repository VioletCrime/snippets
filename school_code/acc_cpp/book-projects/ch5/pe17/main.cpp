/*CHapter 5 PE 17
Take loan, interest and monthly payments; output # months to pay off*/

#include<iostream>

using namespace std;

int main()
{
	int lc;
	double interest, loan, monthly, temp;

	cout << endl << "So you wanna know how many months it will take to pay off your loan, eh?";
	cout << "\nFirst, we'll need to know your loan amount: ";
	cin >> loan;
	cout << "Next, we need the interest rate (%): ";
	cin >> interest;
	cout << "Finally, we need the monthly payments: ";
	cin >> monthly;

	if((loan * ((interest / 12) * .01)) >= monthly)
	{
		cout << endl << "You will never pay off this loan at your monthly payment rate!" << endl;
		cout << "You need to be making monthly payments of over " << loan * ((interest / 12) * .01) << " to make progress!" << endl << endl;
		return 0;
	}
	
	for(lc = 1; loan >= 0; lc++)
	{
		temp = loan * ((interest / 12) * .01);
		loan = loan - (monthly - temp);	
	}//End for()

	cout << endl << "It will take you " << lc << " months to pay off this loan.";	

	cout << endl << endl;
	return 0;
}//End main()
