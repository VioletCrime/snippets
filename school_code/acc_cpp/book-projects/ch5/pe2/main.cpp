/*Chapter 5 PE 2
Prompt for an integet, and reverse it (i.e. '12345' becomes '54321')
Did VERY similar for PE1*/

#include<iostream>

using namespace std;

int main()
{
	int inny, ch1;
	
	cout << endl << "Please enter an integer: ";
	cin >> inny;
	cout << endl << "Your integer in reverse order is: ";

	while(inny > 0)
	{
		ch1 = inny % 10;
		cout << ch1;
		inny = inny / 10;
	}//End while()

	cout << endl << endl;

	return 0;
}//End main()
