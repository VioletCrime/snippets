/*Chapter 7 PE8

Make a fucntion that takes a 'long' number, and returns # even and odd digits*/

#include<iostream>

using namespace std;

void longDong(long testr);

int main()
{
	long testr;
	cout << endl << "Enter an integer (max '2147483647'; 10 digits): ";
	cin >> testr;
	
	longDong(testr);

	return 0;
}

void longDong(long testr)
{
	int lc, temp;
	int even = 0;
	int odd = 0;
	int zero = 0;

	while(testr > 1)
	{
		cout << endl << "value is: " << testr << ", ";
		temp = testr % 10;
		cout << "temp is: " << temp;
		if(temp == 0)
			zero = zero + 1;
		else if (temp % 2 == 0)
			even = even + 1;
		else if (temp % 2 == 1)
			odd = odd + 1;
		testr = testr / 10;
	}
	cout << endl << even << " evens, " << odd << " odds, and " << zero << " zero's." << endl << endl;
}
