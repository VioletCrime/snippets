/*Chapter 7 PE5
Write a program that takes Month-Day-Year and returns the day # (of the year), checking for leap years.*/

#include<iostream>

using namespace std;

int main()
{
	int day, month, year, lc;
	int total = 0;
	cout << endl << "Gonna need the month (1-12): ";
	cin >> month;
	cout << endl << "The day: ";
	cin >> day;
	cout << endl << "And finally the year: ";
	cin >> year;

	for(lc = 1; lc < month; lc++)
	{
		if(lc % 2 == 1 && lc <= 7)
		{
			total = total + 31;
		}
		else if(lc % 2 == 1 && lc >=8)
		{
			total = total + 30;
		}
		else if(lc % 2 == 0 && lc != 2 && lc < 8)
		{
			total = total + 30;
		}
		else if(lc % 2 == 0 && lc >= 8)
		{
			total = total + 31;
		}
		else if(lc == 2)
		{
			total = total + 28;
			if((year % 4 == 0 && year % 100 != 0) || (year % 4 == 0 && year % 400 == 0))
			{
				total = total + 1;
			}
		}
	}
	
	total = total + day;

	cout << endl << "It is the " << total;

	if(total % 10 == 1)
		cout <<"st";
	else if (total % 10 == 2)
		cout << "nd";
	else if (total % 10 == 3)
		cout << "rd";
	else
		cout << "th";

	cout << " day of the year." << endl << endl;
	return 0;
}//End main()
