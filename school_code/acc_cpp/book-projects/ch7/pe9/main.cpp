/*Chapter 7 PE 9
A bit of a monster, this one; open a file, read data, output a BUNCH of data... no global variables =-/ */

#include<iostream>
#include<fstream>
#include<string>
#include<iomanip>

using namespace std;

void calculateAverage(int& average, int test1, int test2, int test3, int test4, int test5);
char calculateGrade(int average);

int main()
{
	string lName;
	char grade;
	int average, test1, test2, test3, test4, test5, lc;
	int classAvg = 0;
	ifstream inny;

	inny.open("input.txt");

	cout << endl << "Student     | Test 1 | Test 2 | Test 3 | Test 4 | Test 5 | Average | Grade |" << endl;
	cout << "----------------------------------------------------------------------------" << endl;


	for(lc = 0; lc <10; lc++)
	{
		inny >> lName >> test1 >> test2 >> test3 >> test4 >> test5;
		calculateAverage(average, test1, test2, test3, test4, test5);
		grade = calculateGrade(average);
		cout << setw(12) << setfill(' ') << left << lName << "|" << right << setw(8) << test1 << "|" << setw(8) << test2 << "|" << setw(8);
		cout << test3 << "|" << setw(8) << test4 << "|" << setw(8) << test5 << "|" << setw(9) << average << "|" << setw(7) << grade << "|" << endl;
		classAvg = classAvg + average;
	}
	cout << "----------------------------------------------------------------------------" << endl;
	cout << endl << "Class average = " << classAvg / 10 << endl << endl;

	return 0;
}


void calculateAverage(int& average, int test1, int test2, int test3, int test4, int test5)
{
	average = (test1 + test2 + test3 + test4 + test5) / 5;
}

char calculateGrade(int average)
{
	switch(average / 10)
	{
		case 10:
		case 9:
			return 'A';
		case 8:
			return 'B';
		case 7:
			return 'C';
		case 6:
			return 'D';
		case 5:
		case 4:
		case 3:
		case 2:
		case 1:
		case 0:
			return 'F';
		default:
			return 'U';
	}
}
