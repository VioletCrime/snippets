/*Chapter 6 PE 8
Write a program with 2 functions which calculate average and standard deviation of 5 user-values*/

#include<iostream>
#include<iomanip>
#include<cmath>

using namespace std;

double mean(double xa, double xb, double xc, double xd, double xe);
double deviation(double xa, double xb, double xc, double xd, double xe);

int main()
{
	double xa, xb, xc, xd, xe, mn, dv;

	cout << endl << "Enter 5 numbers seperated by either spaces or returns: ";
	cin >> xa >> xb >> xc >> xd >> xe;

	mn = mean(xa, xb, xc, xd, xe);
	dv = deviation(xa, xb, xc, xd, xe);

	cout << endl << setprecision(4) << "The mean (average) of your 5 numbers is " << mn << ", and the standard deviation is " << dv << ".";

	cout << endl << endl;
	return 0;
}

double mean(double xa, double xb, double xc, double xd, double xe)
{
	double result;
	result = (xa + xb + xc + xd + xe ) / 5;
	return result;

}//End mean()


double deviation(double xa, double xb, double xc, double xd, double xe)
{
	double result, avg;
	avg = (xa + xb + xc + xd + xe) / 5;
	result = sqrt((pow(xa-avg, 2) + pow(xb-avg, 2) + pow(xc - avg, 2) + pow(xd - avg, 2) + pow(xe - avg, 2)) / 5);
	return result;

}//End deviation()
