/*Chapter 6 PE 2

Write a program with function 'isVowel', which tests user input to see if.... well...

Not perfect, but it DOES do what the question asks... =-P*/

#include<iostream>

using namespace std;

bool isVowel(char een);

int main()
{
	char een;
	bool isit = false;
	cout << endl << "Please enter a character (A-Z): ";
	cin >> een;

	while(!cin)
	{
		cin.clear();
		cin.ignore(200, '\n');
		cout << endl << "It seems the input was not of the needed type. Please enter a character (A-Z): ";
		cin >> een;
	}//End while()
	
	een = tolower(een);

	isit = isVowel(een);

	if(isit == true)
		cout << endl << "The character you have entered is a vowel.";

	else
		cout << endl << "The character you have entered is not a vowel.";

	cout << endl << endl;
	return 0;
}//End main()	

bool isVowel(char een)
{
	if(een == 'a' || een == 'e' || een == 'i' || een == 'o' || een == 'u')
	{
		return true;
	}
	
	else
	{
		return false;
	}
}//End isVowel()
