/*Chapter 6 PE 3
Similar to PE2, only going to test a sequence of characters, and output the # of vowels*/

#include<iostream>

using namespace std;

bool isVowel(char een);

int main()
{
	int noChar, lc;
	int counter = 0;
	bool temp;
	char een;

	cout << endl << "All right, go ahead and start typing. If you want to exit, enter a \"~\":\n";
	
	while(cin)
	{
		cin.get(een);
		if(een == '~')
		{
			cout << endl << "Exiting: \"~\" was entered. At the point of termination, " << counter << " vowels were found." << endl << endl;
			return 0;
		}
		temp = isVowel(een);
		if(temp == true)
			counter++;
	}//End for()
}//End main()

bool isVowel(char een)
{
	een = tolower(een);
	if(een == 'a' || een == 'e' || een == 'i' || een == 'o' || een == 'u')
	{
		return true;
	}//End if()

	else
		return false;

}//End isVowel()
