/*Chapter 8 PE 1
Triangle work with enumerations... le sigh...*/

#include<iostream>

using namespace std;

enum triangleType {scalene, isosceles, equilateral, noTriangle};

triangleType triangleShape(double& side1, double& side2, double& side3);

int main()
{
	triangleType usersTriangle;
	double side1, side2, side3;
	cout << endl << "So, we're going to need the lengths of the 3 sides of the triangle: ";
	cin >> side1 >> side2 >> side3;
	
	usersTriangle = triangleShape(side1, side2, side3);

	cout << endl << "These three sides create ";

	switch (usersTriangle)
	{
		case scalene:
			cout << "a scalene triangle. ";
			break;
		case isosceles:
			cout << "an isosceles triangle. ";
			break;
		case equilateral:
			cout << "an equilateral triangle. ";
			break;
		case noTriangle:
			cout << "no triangle. ";
			break;
		default:
			cout << "an error(?). ";
	}
	cout << endl << endl;
}

triangleType triangleShape(double& side1, double& side2, double& side3)
{
	double temp;
	if (side1 == side2 && side2 == side3)
		return equilateral;
	else if (side1 == side2 || side2 == side3 || side1 == side3)
		return isosceles;
	else
	{
		if (side2 > side1)
		{
			temp = side1;
			side1 = side2;
			side2 = temp;
		}
		if (side3 > side1)
		{
			temp = side1;
			side1 = side3;
			side3 = temp;
		}
		if (side1 >= (side2 + side3))
			return noTriangle;
		if (side1 < (side2 + side3))
			return scalene;
	}
	
}//End triangleShape()
