/*Chapter 8 PE6
Prompt user for 2 sides (regardless of which), output the length of the third side.*/

#include<iostream>
#include<cmath>

using namespace std;

int main()
{
	double m, n, a, b, c;
	cout << endl << "All right, scrubs! I need you to input 2 numbers: ";
	cin >> m >> n;
	a = pow(m, 2) - pow(n, 2);
	b = 2 * m * n;
	c = pow(m, 2) + pow(n, 2);
	a = abs(a);
	cout << "Your pythagorean triple is " << a << ", " << b << ", and " << c << endl;
	cout << "This program was a pretty serious waste of time..." << endl << endl;
	return 0;

}//End main()
