/*Chapter 4 Programming Exercise 2
Take 3 numbers from the user, and output them in ascending order.

Not entirely complete; if there are 2 of the high number, nothing will be displayed*/

#include<iostream>

using namespace std;

int main()
{
	float a, b, c;
	cout << endl << "Enter 3 numbers: ";
	cin >> a >> b >> c;

	if(a > b && a > c)
	{
		cout << endl << a << " > ";
		if(b > c)
			cout << b << " > " << c << endl << endl;
		else
			cout << c << " > " << b << endl << endl;
	}//End if()

	else if(b > a && b > c)
	{
		cout << endl << b << " > ";
		if(a > c)
			cout << a << " > " << c << endl << endl;
		else
			cout << c << " > " << a << endl << endl;
	}//End if()

	else if(c > a && c > b)
	{
		cout << endl << c << " > ";
		if(a > b)
			cout << a << " > " << b << endl << endl;
		else 
			cout << b << " > " << a << endl << endl;
	}//End if()
	
	return 0;


}//End main()
