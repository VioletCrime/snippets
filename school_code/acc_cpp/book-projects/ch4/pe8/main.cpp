/*Ch4PE8* Make a 4-function calculator*/

#include<iostream>

using namespace std;

int main()
{
	float a, b;
	char d;
	cout << endl << "a (+, -, *, /) b\n\nEnter a: ";
	cin >> a;
	cout << "Enter b: ";
	cin >> b;
	cout << "Enter operation (+, -, * or /): ";
	cin >> d;
	if(d == '+')
		cout << endl << a << " + " << b << " = " << a + b << endl << endl;
	else if(d == '-')
		cout << endl << a << " - " << b << " = " << a - b << endl << endl;
	else if(d == '*')
		cout << endl << a << " * " << b << " = " << a * b << endl << endl;
	else if(d == '/')
	{
		if(b==0)
		{
			cout << endl << "Cannot divide by zero; exiting the program." << endl << endl;
			return 0;
		}//End if()
		cout << endl << a << " / " << b << " = " << a / b << endl << endl;
	}
	return 0;

}//End main()
