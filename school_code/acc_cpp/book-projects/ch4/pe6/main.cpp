/*Chapter 4 PE 6
ax^2+bx+c=0; prompt for a, b, and c. Determine type of roots, and
solve for real roots.*/

#include<iostream>
#include<cmath>

using namespace std;

int main()
{
	double a, b, c, disc;
	cout << endl << "ax^2+bx+c=0. Input the values for a, b, and c in that order: ";
	cin >> a >> b >> c;
	disc = b*b - 4*a*c;
	if(disc == 0)
		cout << endl << "There is a sinlge repeated root." << endl << endl;
	else if(disc < 0)
		cout << endl << "There are 2 complex roots." << endl << endl;
	else if(disc > 0)
	{
		cout << endl << "There are 2 real roots.\nThese roots are: " << (-b-pow(pow(b,2) - 4*a*c,0.5))/(2*a) << " and " << (-b+pow(pow(b,2)-4*a*c,0.5))/(2*a) << endl << endl;
	}//End else if()
	return 0;

}
