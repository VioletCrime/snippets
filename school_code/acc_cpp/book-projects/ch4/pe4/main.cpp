/*Chapter 4 Programming Exercise 4

Using input of 3 lengths, determine if they make a right triangle.
a^2+b^2=c^2 only works on right triangles...*/

#include<iostream>
#include<cmath>

using namespace std;

int main()
{
	double a, b, c, B, C;
	cout << endl << "Enter the 3 lengths of the sides of a triangle, hypotenuse (longest) first: ";
	cin >> a >> b >> c;
	B = b*b;
	C = c*c;
	if(a == sqrt(B + C))
		cout << endl << "It is a right triangle" << endl << endl;
	else
		cout << endl << "It is not a right triangle" << endl << endl;
	return 0;


}//End main()
