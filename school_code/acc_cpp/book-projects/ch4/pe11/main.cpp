/*Chapter 4 PR 11 Get bank account info from user, and output some stuff... */

#include<iostream>
#include<iomanip>

using namespace std;

int main()
{
	int account;
	char accType;
	double accBal, minBal;

	cout << endl << "Please enter your account number: ";
	cin >> account;
	cout << endl << "Is this a checking ('c') or savings ('s') account? ";
	cin >> accType;
	cout << endl << "What is the minimum balance? ";
	cin >> minBal;
	cout << endl << "What is the current balance? ";
	cin >> accBal;

	cout << endl << endl << "Account number: " << account;

	if(accType == 'c' || accType == 'C')
	{
		if(minBal > accBal)
		{
			cout << fixed << setprecision(2);
			cout << endl << "Your account is below minimum balance. A fee of $25.00 will be collected at the end of the month\n";
			cout << "and your balance will be $" << accBal - 25 << endl << endl; 
		}//End if()
		else if(minBal >= accBal - 5000)
		{
			cout << fixed << setprecision(2) << endl;
			cout << "Your account will accrue 3" << '%' << " interest, and your balance at the end of the month will be $" << accBal * 1.03 << endl << endl;
		}//End if()
		else if(minBal < accBal - 5000) 
		{
			cout << fixed << setprecision(2) << endl;
			cout << "Your account will accrue 5" << '%' << " interest, and your balance at the end of the month will be $" << accBal * 1.05 << endl << endl;
		}//End if()
	}//End if()

	else if(accType == 's' || accType == 'S')
	{
		if(minBal > accBal)
		{
			cout << fixed << setprecision(2);
			cout << endl << "Your account is below minimum balance. A fee of $10.00 will be collected at the end of the month\n";
			cout << "and your balance will be $" << accBal - 10 << endl << endl;
		}//End if()
		if(accBal > minBal)
		{
			cout << fixed << setprecision(2);
			cout << endl << "Your account will accrue 4" << '%' << " interest, and your balance at the end of the month will be $" << accBal *1.04 << endl << endl;
		}//End if()
	}//End if()

	else
		cout << endl << "Error reading your account type. Please try again." << endl << endl;

	return 0;


}//End main()


