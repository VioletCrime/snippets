/*CH4PE7 Get x and y coords from user, output location (quad, axis, origin)*/

#include<iostream>
	
using namespace std;

int main()
{
	float x, y;
	cout << endl << "Input x and y coordinates, respectively: ";
	cin >> x >> y;
	cout << endl << "(" << x << ", " << y << ") ";
	if(x == 0 && y == 0)
		cout << "is on the origin." << endl;
	else if(x == 0)
		cout << "is on the x-axis." << endl;
	else if(y == 0)
		cout << "is on the y-axis." << endl;
	else if(x > 0 && y > 0)
		cout << "is in the first quadrant." << endl;
	else if(x > 0 && y < 0)
		cout << "is in the fourth quadrant." << endl;
	else if(x < 0 && y > 0)
		cout << "is in the second quadrant." << endl;
	else if(x < 0 && y < 0)
		cout << "is in the third quadrant." << endl;

	return 0;

}//End main()
