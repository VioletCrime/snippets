/*Chapter 4 Programming Exercise 5
Prompt for the number of cookies, # cookies per box
and # boxes per container. Output number of containers,
remaining boxes and remaining cookies.*/

#include<iostream>
#include<cmath>

using namespace std;

int main()
{
	int cks, bxs, cntnrs, cksL, boxL, boxT, contT;
	cout << endl << "How many cookies do you have? ";
	cin >> cks;
	cout << "How many cookies go in a box? ";
	cin >> bxs;
	cout << "How many boxes go in a container? ";
	cin >> cntnrs;
	boxT = cks / bxs;
	cksL = cks % bxs;
	contT = boxT / cntnrs;
	boxL = boxT % cntnrs;
	cout << endl << "You have enough cookies for " << contT << " containers, with " << boxL << " boxes and " << cksL << " individual cookies remaining." << endl << endl;

	return 0;



}//End main()
