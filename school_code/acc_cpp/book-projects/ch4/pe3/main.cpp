/*Chapter 4 Programming Exercise 3
Prompt the user for a number between 0 and 35.
Return the number 0-9, return A-z on 10-35.*/

#include<iostream>

using namespace std;

int main()
{
	int getcha;
	cout << endl <<"Enter a number between 0 and 35: ";
	cin >> getcha;
	if(getcha >= 0 && getcha < 10)
		cout << endl << "Returned value is: " << getcha << endl << endl;
	else if(getcha >= 10 && getcha <= 35)
		cout << endl << "Returned value is: " << static_cast<char>(getcha + 55) << endl << endl;
	else
		cout << endl << "You don't seem to have entered a value in the needed range.\nPlease restart the application and try again." << endl << endl;
	return 0;


}//End main()
