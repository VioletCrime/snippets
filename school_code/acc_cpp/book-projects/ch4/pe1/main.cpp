/*Chapter 4 Programming Exercise 1
Ask the user for a number, output the number, as well as a statment indicating
whether the number is positive, negative, or zero.

How hard could it be?*/

#include<iostream>

using namespace std;

int main()
{
	float input;
	cout << endl << "Input a number, any number will do: ";
	cin >> input;
	cout << endl << endl << "The number you entered is " << input << "." << endl;
	if(input == 0)
		cout << "It is equivalent to zero." << endl << endl;
	if(input < 0)
		cout << "It is less than zero." << endl << endl;
	if(input > 0)
		cout << "It is greater than zero." << endl << endl;

	return 0;


}//End main()
