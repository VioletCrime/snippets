/*CHapter 9 PE4
INput grade data from file, categorize, output # students in each category.*/

#include<iostream>
#include<fstream>

using namespace std;

int main()
{
	int lc, temp;
	int beta[8];
	for (lc = 0; lc < 8; lc++)
		beta[lc] = 0;
	int alpha[26];
	ifstream inny;
	inny.open("input.txt");
	if(!inny)
	{
		cout << endl << "Could not find input file; please make sure it is in this program's directory\n";
		cout << "and named \"input.txt\"." << endl << endl;
		return 0;
	}
	for(lc = 0; lc < 26; lc++)
	{
		inny >> alpha[lc];
	}
	inny.close();
	for(lc = 0; lc < 26; lc++)
	{
		temp = alpha[lc] / 25;
		beta[temp] = beta[temp] + 1;
	}
	for (lc = 0; lc < 8; lc++)
		cout << endl << endl << "Group " << lc << " consists of " << beta[lc] << " students.";
	cout << endl << endl;
	return 0;
}
