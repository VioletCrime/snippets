/*Chapter 9 PE 1
Initialize an 50-iteration array, store various data on it, output 10 iterations per line.*/

#include<iostream>
#include<cmath>

using namespace std;

int main()
{
	int lc, lc2;
	int lc3 = 0;
	double alpha[50];
	cout << endl;
	for(lc = 0; lc < 50; lc++)
	{
		if(lc < 25)
			alpha[lc] = sqrt(lc);
		else if (lc >=25)
			alpha[lc] = 3*lc;
	}

	for (lc = 0; lc < 5; lc++)
	{
		for (lc2 = 0; lc2 < 10; lc2++)
		{
			cout << alpha[lc3] << "	";
			lc3++;
		}
		cout << endl;
	}
	cout << endl << endl;
	return 0;


}//End main()
