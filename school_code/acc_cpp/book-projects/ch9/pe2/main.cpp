/*CHapter 9 PE2
Write a program that finds the first occurance of the smallest element in an index (in a function, returning the index)*/

#include<iostream>
#include<fstream>

using namespace std;

int findSmallest(int alpha[25]);

int main()
{
	int lc, temp;
	int alpha[25];
	ifstream inny;
	inny.open("input.txt");
	if(!inny)
	{
		cout << endl << "Could not find input file; please ensure it is in the program's directory\n";
		cout << "and is named \"input.txt\"" << endl << endl;
		return 1;
	}
	for (lc = 0; lc < 25; lc++)
		inny >> alpha[lc];
	temp = findSmallest(alpha);
	cout << endl << "The smallest value is " << alpha[temp] << " and first occurs at index " << temp + 1 << endl << endl;
	

}//End Main


int findSmallest(int alpha[25])
{
	int lc;
	int loc = 0;
	int temp = alpha[0];
	for (lc = 0; lc < 25; lc++)
	{
		if (alpha[lc] < temp)
		{
			loc = lc;
			temp = alpha[lc];
		}
	}
	return loc;

}
