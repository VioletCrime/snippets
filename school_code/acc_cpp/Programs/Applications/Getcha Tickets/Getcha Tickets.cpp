/* "Getcha Tickets" by Kyle Smith, No-One's Productions.

This program is designed to open a premade file containing ticket sales and prices,
obtain that information, manipulate the data to figure the total number of tickets sold and
the gross ticket sales. The program will then save these new values to a new file.     */

#include <iostream>         
#include <iomanip>        //Going to need some advanced io functions
#include <fstream>        //Need this to work with the files

using namespace std;

int main()                //Let's get this party started
{
    ifstream inFile;      //Declaring in....
    ofstream outFile;     //and out string variables
    
    int numBox, numSideline, numPremium, numGenAdmission;  //Creating variables for the number of tickets sold of each type
    double priceBox, priceSideline, pricePremium, priceGenAdmission;  //Creating variables for the prices of each ticket type
    double numTotal, grossSales;      //Creating variables for total tickets sold and gross ticket sales
    
    inFile.open("ch3_ex3data.txt");    //Opening the input file
    outFile.open("ch3_ex3out.txt", ios::app);    //Creating an output file, and safeguardsing old data from overwriting
    
    cout << "Processing data, please wait" << endl;  //Akin to saying "Please wait 6 to 8 weeks for delivery"
    
    inFile >> priceBox >> numBox >> priceSideline >> numSideline >> pricePremium >> numPremium >> priceGenAdmission >> numGenAdmission;
    //^^ Getting the information contained in the included file for manipulation
    
    grossSales = priceBox * numBox + priceSideline * numSideline + pricePremium * numPremium + priceGenAdmission * numGenAdmission;
    //^^ Calculating gross sales by summing the product of each ticket type's price multiplied by it's cost
    
    numTotal = numBox + numSideline + numPremium + numGenAdmission;
    //^^ Generating the total number of tickets sold by summing the sales of each ticket type
    
    outFile << "Number of tickets sold = " << numTotal << endl;   //Outputting the total number of tickets to the output file, and starting a new line
    outFile << fixed << showpoint << setprecision(2);             //Defining output parameters to show 2 decimal places, including zero's
    outFile << "Sale amount = " << grossSales << endl << endl;            //Outputing gross sales to output file
    
    inFile.close();        //Closing the input and...
    outFile.close();       //output files.
    
    system("pause");       //Indicating (hopefuly successful) completion of task
    
    return 0;
}                          //That's all, folks!
    
    
    
    
