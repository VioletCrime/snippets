/* Makin the Grade by Kyle Smith, No-One's Productions
Made for CIS 160 Spring 2010 */

#include <iostream>  //Communications array online!
#include <iomanip>   //Let's make it pretty!
#include <fstream>   //Going to need this in order to call on secondary storage

using namespace std; //Found out how bad things get without this great function today.... took a few minutes to figure out

void openFiles(ifstream&, ofstream&); //This function handles the opening of files, and the formatting of numerical output
void initialize(ifstream&, ofstream&); //This function declares all the variables used with the exceptions of the ifstream and ofstream variables
void sumGrades(ifstream&, ofstream&, char&, float&, int&, int&, float&, float&); //This function reads the input file and tallies GPA's and genders
void averageGrade(int&, int&, float&, float&, float&, float&); // This function finds the average GPA of each gender
void printResults(ifstream&, ofstream&, int&, int&, float&, float&, float&, float&); //This function outputs the results to an output file

int main()  //Oh how I've missed you, 'main()'!
{
    ifstream in; //Setting ifstream variable; this links the variable 'inData' in openFiles() with the other functions
    ofstream out; //Setting ofstream variable; this links the variable 'outData' in openFiles() with the other functions
    
    cout << "Processing grades\n";  //Sit back, and relax; I got this
    
    openFiles(in, out);  //Calling the openFiles() function, unexpectedly, to open files for us
    
    if (!in) //This tests in for the input file; if it is true that 'in' has returned an error at this point, it can only be due to a missing input file
    {
        return 1;  //Danger Will Robinson!
    }
     
    initialize(in, out); //Calling the initialize() function, and sharing the if/of-stream variables. From here, main() is only used to close up shop
    
    in.close(); //Closing the input file
    out.close(); //Closing the output file
    cout << "Output data is in file Ch7_Ex4out.txt" << endl << endl;  //Indicating where the output file is at
    system("pause"); //Thank you for your patience.
    return 0; //And the party is only just beginning... weird...
}

void openFiles(ifstream& inData, ofstream& outData) //Share fstream variables, and let's get to defining!
{    
     outData << fixed << setprecision(2) << showpoint; //Setting all numerical fstream output to 2 decimal places including zero's
     
     inData.open("Ch7_Ex4Data.txt"); //Let's take a peek....
     
     if (!inData) //... and if nothing is wrong with the input file...
     {
                 cout << "\nUnable to find input file; please place it in the same directory as this \napplication, ensure it is named \"Ch7_Ex4Data.txt\", and try again.";
                 cout << endl << endl;
                 system("pause");
                 return;
     }
     outData.open("Ch7_Ex4out.txt"); //...we will document what we find.
}

void initialize(ifstream& inny, ofstream& outty) 
/* It is easy to argue that this function is the primary function of the program. Because all of the numerical
variables are declared in this body, they will only be usable in this body. Because there is no way to share these
values with other functions outside of this body without breaking the rules of the program design, the remainder 
of the functions are called from within this body*/
{
     char gender; //Used to store gender information gathered from the input file
     float temp; //Used to store GPA information gathered from the file.
     int countMale = 0; //Used to tally male students
     int countFemale = 0; //Used to tally female students
     float sumMale = 0; //This will keep the running score for the men's GPA
     float sumFemale = 0; //This will keep the running score for the women's GPA
     float avgMale = 0; //Storage for the average male GPA
     float avgFemale = 0; //Sotrage for the average female GPA
     
     sumGrades(inny, outty, gender, temp, countMale, countFemale, sumMale, sumFemale); //Calling sumGrades() function, sharing quite a bit
     
     averageGrade(countMale, countFemale, sumMale, sumFemale, avgMale, avgFemale); // Calling averageGrade() function, sharing a bit less
     
     printResults(inny, outty, countMale, countFemale, sumMale, sumFemale, avgMale, avgFemale); //Calling printResults(), what a list!
     
}

void sumGrades(ifstream& inny, ofstream& outty, char& gender, float& temp, int& countMale, int& countFemale, float& sumMale, float& sumFemale)
//It's hard to keep track of all these variables if you're not paying attention.
{
     while (!inny.eof()) //Constructing an End-of-File while loop
     {
           inny >> gender >> temp; //Reading 2 values in, the first for the char 'm' or 'f' value, the second for the GPA
           if (gender == 'f') //If it's a female...
           {
                      sumFemale = sumFemale + temp; // ... add her GPA to the running total...
                      countFemale++; //... and add a tick mark for the women.
           }
           else if (gender == 'm') //Similarly, if it's a male...
           {
                sumMale = sumMale + temp; // ... add his GPA to the pool...
                countMale++; //... and add a head to the list.
           }
           outty << gender << "   " << temp << endl; //While we're here, we may as well mirror the values we need to the output file while we have them stored
     }
}

void averageGrade(int& countMale, int& countFemale, float& sumMale, float& sumFemale, float& avgMale, float& avgFemale)
//It's all downhill from here...
{
     avgMale = sumMale / countMale; //Pretty straightforward averaging formula
     avgFemale = sumFemale / countFemale; //More of the same
} //If only they were all this easy

void printResults(ifstream& inny, ofstream& outty, int& countMale, int& countFemale, float& sumMale, float& sumFemale, float& avgMale, float& avgFemale)
//Defining the printResults() function. I suppose in retrospect I didn't need to include the ifstream parameter, but oh well
{
     outty << setw(24) << left << "Sum female GPA =" << sumFemale << endl; //These are pretty straightforward; just left-adjusting and setting
     outty << setw(24) << left << "Sum male GPA =" << sumFemale << endl;  //the width to 24 characters to make a uniform data sheet, and outputting
     outty << setw(24) << left << "Female count =" << countFemale << endl; //the various values with the corresponding markers. I suppose using 'fixed'
     outty << setw(24) << left << "Male count =" << countMale << endl; //would have saved me some copy-paste work, but things are always clearer in
     outty << setw(24) << left << "Average female GPA =" << avgFemale << endl; //the rearview mirror.
     outty << setw(24) << left << "Average male GPA =" << avgMale;
}
