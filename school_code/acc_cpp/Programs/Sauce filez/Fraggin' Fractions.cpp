/* Fraggin' Fractions by Kyle Smith, No-One's Productions
Created for CIS160 C11, Spring 2010   */

#include <iostream>

using namespace std;  //<3 this 

void menu(); //Prototype for the menu function
void getInput(int& n1, int& n2, int& d1, int& d2);
void addFractions();  //Prototype for the addFractions function
void subtractFractions (); //Prototype for the subtractFractions function
void multiplyFractions ();  //Prototype for the multiplyFractions function
void divideFractions ();  //Prototype for the divideFractions function

int main()  //Hajimeru
{ //Begin main()
    menu();  //I'm so excited, let's get main() started!!!
    return 0; //That's what I'm talking about! Bring it o.... wait! That's IT!?! lol
} //end main()

void menu () // Defining the menu() function. This funtion contains all of the central decisionmaking of the program
{  //start menu()
    int selector = 1; //Setting up a sentinel / selector for the upcoming while loop
    
    while (selector != 9) 
    /*This loop is the heart and soul of this program. All of the functions branch from within this structure
    and when the loop ends, the program in its entirety ends as well*/
    { //Start while loop
          cout << "This program performs operations on fractions. Enter" << endl; //Typing out the program's
          cout << "1 : To add fractions" << endl; //main menu, prompting the user to decide which action 
          cout << "2 : To subtract fractions" << endl; //to take
          cout << "3 : To multiply fractions" << endl;
          cout << "4 : To divide fractions" << endl;
          cout << "9 : To exit the program" << endl;
          cin >> selector; //Getting the user's input
          
          if (!cin)  // the 'default' selection in the switch structure is unable to stop the loop if cin enters an error state; this will handle the problem
          {//Begin if
                   cout << "Input is not valid, please reselect." << endl << endl; //... by alerting the user they messed up
                   cin.clear(); //.. and handling the problem
                   cin.ignore(200,'\n'); // ... one step at a time
          }//End if
          
          switch (selector)  //This structure calls upon different functions depending on the user's input
          {//Begin switch
                 case 1: //If the user entered '1'...
                      addFractions();  //... then we call on the addFractions function...
                      break; //... and end the switch structure
                 case 2: //If the user entered '2'...
                      subtractFractions(); //... then we call upon the subtractFractions function...
                      break; // and leave the switch structure upon completion
                 case 3:  //If the user entered '3'...
                      multiplyFractions();  // ... no surprises here
                      break;  // more of the same
                 case 4:  //User enters '4' to divide....
                      divideFractions();  //... and so we shall divide
                      break;
                 case 9:   //The user has opted to leave the program. When the switch structure terminates,
                      break;  //the variable will be tested, and the while loop will terminate, ending the program
                 default:   //None of the above?
                         cout << "Input is not valid, please reselect." << endl << endl;  //you screwed up, user!!
                         break;  //It's OK, though; we'll give you as many attempts as you need
          }  //end switch
    }  //end while (and the entire program)
} //end menu()

void getInput(int& n1, int& n2, int& d1, int& d2)
/*This function was created to drastically decrease the amount of code contained in this program.
The sole purpose of this function is to prompt the user for the numerators and denominators of 2
fractions. The safeguards against stream error states and 0-value denominators became quite extensive
and it was clear that in order to increase the readability of the program, this function should be 
created. 

There are 2 iterations of the same code found in this function. Because these iterations are virtually
identical, I will only be commenting on the first iteration of the following code*/

{ //Start getInput
           cout << "For fraction 1\nEnter the numerator : "; //Prompting for the numerator of the first fraction
         cin >> n1; //Receiving input
         if (!cin)  //If the user's input triggers an instream failstate...
         {//start if()
                  do //we need to set up a do...while structure to test again to ensure we have good input, and repeat ad infinitum until good input is received
                  {//start do...while
                        cin.clear(); //clear the input...
                        cin.ignore(200, '\n');  // ignore oncoming garbage...
                        cout << endl << "Error occurred, try again.\nFor fraction 1\nEnter the numerator : ";  //Let the user know something's wrong and prompt again
                        cin >> n1;  //Try it again...
                  }//end 'do' of do...while
                  while (!cin); //... we can be here as long as we need to be...
         }//end if()
         d1 = 0;  //Ensuring the following while loop is entered
         while (d1 == 0)
         {//start of while loop
                  cout << endl << "Enter the denominator : "; //Prompting for denominator of first fraction
                  cin >> d1; //Getting input...
                  if (d1 == 0 && cin)  //If it's still zero after user input and the input stream isn't in a fail state...
                      cout << "The denominator must be nonzero." << endl; //Then alert the user of the problem.
                  if (!cin) // If the stream enters fail state...
                     {//start if()
                           cin.clear(); //clear the cin...
                           cin.ignore(200,'\n');  //ignore the loads of trash...
                           d1 = 0; // make sure we enter the while() loop again...
                           cout << "Error occurred, try again.\n";  //and let the user know...
                     }//end if()
         }//end while loop: if the value is still 0, the user will come back
         
         /*This is the end of the prompt code for the first fraction; the second fraction's code is the same, 
         except for the variables involved*/
         
         cout << endl << endl << "For fraction 2\nEnter the numerator : "; //Prompting for the numerator of the second fraction
         cin >> n2;
         if (!cin)
         {
                  do
                  {
                        cin.clear();
                        cin.ignore(200, '\n');
                        cout << endl << "Error occurred, try again.\nFor fraction 2\nEnter the numerator : ";
                        cin >> n2;
                  }
                  while (!cin);
         }
         d2 = 0;
         while (d2 == 0)
         {
                  cout << endl << "Enter the denominator : ";
                  cin >> d2;
                  if (d2 == 0 && cin) 
                      cout << "The denominator must be nonzero." << endl;
                  if (!cin)
                     {
                           cin.clear();
                           cin.ignore(200,'\n');
                           d2 = 0;
                           cout << "Error occurred, try again.\n";
                     }
         }
}//end getInput function
          
void addFractions()
{//Begin addFractions function

         int numer1, numer2, numer3, denom1, denom2, denom3, temp;  //Going to need 3 sets of fraction variables and a loop placeholder
         
         getInput(numer1, numer2, denom1, denom2); // Time to get the user's values for the 2 fractions
         
         denom3 = denom1 * denom2; //Easiest way to sum is to multiply each fraction by the other's denominator...
         numer3 = (numer1 * denom2) + (numer2 * denom1); // Sum the product of each numerator with the other denominator
         
         cout << endl << numer1 << "/" << denom1 << " + " << numer2 << "/" << denom2 << " = " << numer3 << "/" << denom3 << endl << endl;
         //Output the results to the user. No need to system("pause"), as the menu will pause the system in a similar fashion
}//end addFractions()

void subtractFractions()
{//Begin subtractFractions function

         int numer1, numer2, numer3, denom1, denom2, denom3, temp;  //Going to need 3 sets of fraction variables
         
         getInput(numer1, numer2, denom1, denom2); // Time to get the user's values for the 2 fractions
         
         denom3 = denom1 * denom2; //Easiest way to sum is to multiply each fraction by the other's denominator...
         numer3 = (numer1 * denom2) - (numer2 * denom1); // Sum the product of each numerator with the other denominator
         
         cout << endl << numer1 << "/" << denom1 << " - " << numer2 << "/" << denom2 << " = " << numer3 << "/" << denom3 << endl << endl;
         //Output the results to the user. No need to system("pause"), as the menu will pause the system in a similar fashion
}//end subtractFractions()

void multiplyFractions()
{//Begin multiplyFractions function

         int numer1, numer2, numer3, denom1, denom2, denom3, temp;  //Going to need 3 sets of fraction variables
         
         getInput(numer1, numer2, denom1, denom2); // Time to get the user's values for the 2 fractions
         
         numer3 = numer1 * numer2;
         denom3 = denom1 * denom2;
         
         cout << endl << numer1 << "/" << denom1 << " * " << numer2 << "/" << denom2 << " = " << numer3 << "/" << denom3 << endl << endl;
         //Output the results to the user. No need to system("pause"), as the menu will pause the system in a similar fashion
}//end multiplyFractions()

void divideFractions()

/*Because the second numerator is used to calcluate the resulting denominator by cross-multiplying, the code from the getInput()
function has been copied to this function and altered in it's prompt for the second numerator. These alterations result in the 
prompt being equivalent to the input prompts for the donominators. Please refer to the 'getInput()' function for detailed code
commentary.*/

{//Begin divideFractions function

         int numer1, numer2, numer3, denom1, denom2, denom3, temp;  //Going to need 3 sets of fraction variables
         
         cout << endl << endl << "For fraction 1\nEnter the numerator : "; //Prompting for the numerator of the second fraction
         cin >> numer1;
         if (!cin)
         {
                  do
                  {
                        cin.clear();
                        cin.ignore(200, '\n');
                        cout << endl << "Error occurred, try again.\nFor fraction 1\nEnter the numerator : ";
                        cin >> numer1;
                  }
                  while (!cin);
         }
         denom1 = 0;
         while (denom1 == 0)
         {
                  cout << endl << "Enter the denominator : ";
                  cin >> denom1;
                  if (denom1 == 0 && cin) 
                      cout << "The denominator must be nonzero." << endl;
                  if (!cin)
                     {
                           cin.clear();
                           cin.ignore(200,'\n');
                           denom1 = 0;
                           cout << "Error occurred, try again.\n";
                     }
         }
         
         numer2 = 0;
         while (numer2 == 0)
         {
                  cout << endl << "Enter the numerator : ";
                  cin >> numer2;
                  if (numer2 == 0 && cin) 
                      cout << "This particular numerator must be nonzero." << endl;
                  if (!cin)
                     {
                           cin.clear();
                           cin.ignore(200,'\n');
                           numer2 = 0;
                           cout << "Error occurred, try again.\n";
                     }
         }
         denom2 = 0;
         while (denom2 == 0)
         {
                  cout << endl << "Enter the denominator : ";
                  cin >> denom2;
                  if (denom2 == 0 && cin) 
                      cout << "The denominator must be nonzero." << endl;
                  if (!cin)
                     {
                           cin.clear();
                           cin.ignore(200,'\n');
                           denom2 = 0;
                           cout << "Error occurred, try again.\n";
                     }
         }
         
         numer3 = numer1 * denom2;
         denom3 = denom1 * numer2;
         
         cout << endl << numer1 << "/" << denom1 << " / " << numer2 << "/" << denom2 << " = " << numer3 << "/" << denom3 << endl << endl;
         //Output the results to the user. No need to system("pause"), as the menu will pause the system in a similar fashion
}//end divideFractions()
