/* C++ Landscape, by Kyle Smith for CSC 160 C11 2/6/2010 */

#include <iostream>

using namespace std;

int main()
{
    cout << "CCCCCCCC        ++             ++" << endl;
    cout << "CC              ++             ++" << endl;
    cout << "CC         ++++++++++++   ++++++++++++" << endl;
    cout << "CC         ++++++++++++   ++++++++++++" << endl;
    cout << "CC              ++             ++" << endl;
    cout << "CCCCCCCC        ++             ++" << endl;       
    cout << " " << endl;
    system ("pause");                                 //Didn't realize it at the time, but this function adds the 'Press any key' prompt automatically!
    
    return 0;
}
