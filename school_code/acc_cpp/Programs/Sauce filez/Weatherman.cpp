//Weatherman by Kyle Smith, No-One's Productions
//Made for CIS160 C11, Spring 2010

#include <iostream>
#include <fstream>

using namespace std;  //Making sure my code isn't littered with std::'s 

void getData(int gMatrix[12][2], ifstream& inData);  // Prototype declarations incoming... their names 
int averageHigh(int hMatrix[12][2]); // seem self-explanitory...
int averageLow(int iMatrix[12][2]);
int indexHighTemp(int jMatrix[12][2]);
int indexLowTemp(int kMatrix[12][2]);  //... and that should just about do it for prototypes.

int main()  //Let's form the glue holding this together...
{ //Main
    int avgHigh, avgLow, indexHigh, indexLow;  // These will store the corresponding calculated values
    int wMatrix[12][2];  // Declaring a 24-variable array to hold the incoming data
    ifstream in;  // Declaring a fstream input variable 'in'
    getData(wMatrix, in); // Calling on the getData function, sharing the new array and the ifstream variable
    if(!in) // If the ifstream is in failstate...
           return 1; //... end the program
    
    avgHigh = averageHigh(wMatrix); //Calling averageHigh() and storing it's returned value
    avgLow = averageLow(wMatrix); //Same with averageLow...
    indexHigh = indexHighTemp(wMatrix); //A pattern is forming...
    indexLow = indexLowTemp(wMatrix); //Fin
    
    in.close();  //Closing the input file...
    
    cout << "Average high temperature: " << avgHigh << endl; //Defining the cout cluster used to
    cout << "Average low temperature: " << avgLow << endl; //output the resulting data
    cout << "Highest temperature: " << indexHigh << endl;
    cout << "Lowest temperature: " << indexLow << endl << endl;
    
    system("pause"); //Giving the user time...
    return 0; //All's well that ends well.
} //End main



void getData(int gMatrix[12][2], ifstream& inData)
{ //getData
     int loopOne, loopTwo;  //Just a couple of loop counters...
     inData.open("Ch9_Ex9Data.txt"); // Opening the input file...
     if(!inData) // If the ifstream enters a fail state, let the user know, and stop the function short.
     { //if()
                cout << "Input file could not be found. Please ensure the file is in the same directory";
                cout << "\nas this program, and is named \"Ch9_Ex9Data.txt\"" << endl << endl;
                system("pause");
                return;
     } //End if()
     
     for(loopOne = 0; loopOne < 12; loopOne++) //Setting up two loops to input data to the array
     { //for(loopOne)
                 for(loopTwo = 0; loopTwo < 2; loopTwo++)
                 {//for(loopTwo)
                             inData >> gMatrix[loopOne][loopTwo];
                 }//end for(loopTwo)
     } //end for(loopOne)
} //End getData



int averageHigh(int hMatrix[12][2])
{//averageHigh()
     cout << "Processing Data" << endl << endl;  //If we've made it this far, it's a-go!
     int loop, sum;   //loop placeholder, and running sum
     sum = 0;
     for(loop = 0; loop < 12; loop++) //This loop is designed to sum all the values of the first column
              sum = sum + hMatrix[loop][0];
     return sum / 12;  //returning the sum / 12 (average)
} //End averageHigh()



int averageLow(int iMatrix[12][2])
{ //averageLow()
    int loop, sum;  // same as with 'averageHigh()'
    sum = 0;
    for(loop = 0; loop < 12; loop++) //Only THIS loop will sum the entries in the second column...
        sum = sum + iMatrix[loop][1];
    return sum / 12;  //... and return the average of the second column
}//End averageLow()



int indexHighTemp(int jMatrix[12][2])
{//indexHighTemp()
    int highest, loop;  //loop placeholder, and a memory-variable
    highest = 0; //making sure...
    for(loop = 0; loop < 12; loop++) //This loop tests every entry in the first column to see if it's...
    {//for()
             if(highest < jMatrix[loop][0]) //... greater than the previously tested values...
                        highest = jMatrix[loop][0];
    }//End for()
    return highest;  //... and returns the highest value found in the first column.
}//End indexHighTemp()



int indexLowTemp(int kMatrix[12][2])
{//indexLowTemp()
    int lowest, loop;  //loop placeholder, and memory variable
    lowest = 0; //making sure...
    for(loop = 0; loop < 12; loop++)  //Similarly, this for loop tests every entry of the second column...
    {//for()
             if(lowest > kMatrix[loop][1]) //...against the lowest value found previously, and saves the...
                       lowest = kMatrix[loop][1]; //... lowest value for further testing...
    }//End for()
    return lowest; //... and returns the lowest found throughout the second column.
}//End indexLowTemp()
