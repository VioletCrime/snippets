/* Countin' Pennies by Kyle Smith for CSC 160 C11 2/6/2010 */

#include <iostream>

using namespace std;

int main()
{
    int quarters, dimes, nickels, pennies;
    
    cout << "Enter the number of quarters: ";
    cin >> quarters;
    cout << endl << endl;
    
    cout << "Enter the number of dimes: ";
    cin >> dimes;
    cout << endl << endl;
    
    cout << "Enter the number of nickels: ";
    cin >> nickels;
    cout << endl << endl;
    
    pennies = quarters * 25 + dimes * 10 + nickels * 5;
    
    cout << "The change in pennies = " << pennies << endl << endl << endl; //3 endl commands to make the program look cleaner
    
    system ("pause");
    
    return 0;
}
    
