/*City Wok Menu by Kyle Smith, No-one's Productions
Made for CIS160 C11, Spring 2010 */

#include <iostream> //User output is good... not so sure about the input... =-P
#include <fstream>  //Going to need to open a file...
#include <string>   //Just found out that having this really reduces compiler errors where strings are used... =-/
#include <iomanip>  //Making it pretty...

using namespace std;  //Never gonna give you up, never gonna let you down...

struct menuItemType  //Creating a structure for the different menu items
{
        char item[30]; //Stores the name of the item in question...
        double price; //... this stores the corresponding price....
        bool ordered; //...and this stores a purchase decision.
};  //End struct{}

void getData(ifstream& inny, menuItemType menuList[]);  // Function prototypes...
void showMenu(menuItemType menuList[], int& exitEarly);
void printCheck(menuItemType menuList[]);



int main()  //Again with the short main functions... 
{
    int exitEarly = 0; //Setting up a flag to end the program early if the user orders nothing.
    
    ifstream in; //Declaring an ifstream variable 'in'
    menuItemType menuList[8]; //Making an 8-iteration array of type menuItemType
    
    getData(in, menuList);    //Calling on the getData() function to fill in the blanks
    if(!in)  //double-checking to see if the program should quit on account of lack of input file
           return 1;
           
    showMenu(menuList, exitEarly);  //take what getData() has input into menuList, and throw it at showMenu() to display the menu, and prompt for the user's order
    if(exitEarly == 1) //If the user didn't even order a single thing, there's no use printing a check...
           return 0; //... so let's break early!
    
    printCheck(menuList); //Call for the printCheck() function again using menuList.
    
    return 0; //That about wraps it up.
} //End main()



void getData(ifstream& inny, menuItemType menuList[])  //This function opens the input file and copies it's information to the menuList array
{
       int lc; //Loop Controller
       char discard;  //Throw-away variable used to 'read past' newline characters
       inny.open("ch10_ex3data.txt"); //Opening the input file...
       
       if(!inny)  //... and testing to make sure the file was actually there.
       {
                cout << "Could not find the menu input file. Please ensure it is in this\nprogram's ";
                cout << "directory and is named \"ch10_ex3data\"." << endl << endl;
                system("pause");
                return;
       }//End if()
       
       for(lc = 0; lc < 8; lc++) //Setting up an 8-iteration loop to input the data from the file to the array
       {
              inny.get(menuList[lc].item, 30); //30 characters is probably way too high, but memory is cheap these days...
              inny.get(discard);  //Ignoring the newline character
              inny >> menuList[lc].price;
              inny.get(discard);
              menuList[lc].ordered = false; //Setting all entries for the ordered bool value to false while I'm here
              inny.clear();//Clears the inevitable error state caused by the end of file on the very last 'inny.get(discard)'; took 20 minutes to track this one down.
       }//End for()
       
       inny.close(); //Won't need the input file anymore; may as well clean this up.
}//End getData()



void showMenu(menuItemType menuList[], int& exitEarly)  
/*This function is the heavy lifter of the program. It displays the menu, prompts the user if they want to order,
what they want to order, and stores this quantity in the menuList array.*/
{
     int lc; //Loop Control variable
     char decide;  // Used to store the user's decision to order more food / drinks.
     int orderNum; // Used to store the menu value of what the user orders
     
     cout << "Welcome to Johnny's Restaurant\n----Today's Menu----" << fixed << left <<setprecision(2) << endl;  //First printed line of code
     
     for(lc = 0; lc < 8; lc++) //Loop outputting the menu, using lc as the loop placeholder and array iteration counter
     {
            cout << lc + 1 << ": " << setw(16) << menuList[lc].item << "$" << menuList[lc].price << endl;
     }//End for()
     
     cout << "You can make up to 8 single order selections" << endl; //Going the extra mile for that 'A' in this class
     cout << "Do you want to make a selection? Y/y <Yes>, N/n <No>: "; //Prompting for the desire to order food
     cin >> decide; //Receiving user's decision...
     
     if(decide == 'n' || decide == 'N') //If the user says 'no' now, they will have ordered nothing, so we may as well not print a check
     {
               exitEarly = 1;  //setting a flag for a main() if() expression to terminate the program early...
               return;   //...and getting to main() for exactly that with haste.
     }
     
     if((decide != 'y' && decide != 'Y') || !cin) //Loop designed to prompt until 1 of the 4 desired inputs is received
     {
               do
               {
                    cin.clear(); //Resetting input stream...
                    cin.ignore(200, '\n'); //... clearing the potential garbage...
                    cout << "Invalid input, please try again: ";  //...letting the user know...
                    cin >> decide; // ... and reprompting...
               }//End do()
               while((decide != 'y' && decide != 'Y') || !cin); //...as long as it takes to get it right.
     }//End if()
     
     while(decide == 'y' || decide == 'Y') 
     /*Is the ordering loop. It recurrs until the user select's 'no' when prompted if they wish to order
     more food.*/
     {
                  cout << endl << "Enter item number: ";  //Prompting for user's choice.
                  cin >> orderNum;  //Receiving input...
                  if(orderNum >= 9 || orderNum <= 0 || !cin) //Loop designed to prompt until 1 of the desired inputs is received
                  {
                       do
                       {
                              cin.clear(); //Resetting input stream...
                              cin.ignore(200, '\n'); //... clearing the potential garbage...
                              cout << "Invalid input, please try again: ";  //...letting the user know...
                              cin >> orderNum; // ... and reprompting...
                       }//End do()
                       while(orderNum >= 9 || orderNum <= 0 || !cin); //...as long as it takes to get it right.
               
                   }//End if()
                   menuList[orderNum - 1].ordered = true; //Commiting to memory what the user wants.
                  cout << endl << "Order another item? Y/y <Yes>, N/n <No>: ";  //Want more?
                  cin >> decide;
                  if((decide != 'y' && decide != 'Y' && decide != 'n' && decide != 'N') || !cin) //Exact same error failsafe loop as above.
                  {  //If no valid input as entered, or if the input stream entered a fail state...
                             do
                             {
                                    cin.clear(); //...clear the problems...
                                    cin.ignore(200, '\n'); //...clean up the mess...
                                    cout << "Invalid input, please try again: "; //...tell the user they're being dense...
                                    cin >> decide; //...and see what they want to do...
                             }//End do()
                  while((decide != 'y' && decide != 'Y' && decide != 'n' && decide != 'N') || !cin); //...until they get it right.
                  }//End if()
     }//End while()
} //End showMenu()



void printCheck(menuItemType menuList[])
{
     int lc;  //Loop control variable
     double subTotal, tax, totalDue;  //Need these to store tax, subTotal, and the final total
     
     cout << endl;
     cout << "|        Item        | Price |" << endl;  //Let's make it look pretty...
     cout << "|--------------------|-------|" << endl;  //... real pretty...
     for(lc = 0; lc < 8; lc++) //Loop designed to print array information
     {
            if(menuList[lc].ordered == true) //If an item was ordered, display its information, and add it's value to the subtotal
            {
                   cout << "| " << setw(19) << menuList[lc].item << "| $"; //Some pretty-making code...
                   cout << setw(5) << menuList[lc].price << "|" << endl;
                   subTotal = subTotal + menuList[lc].price; //Keeping a running (sub)total
            }//End if()
     }//End for()
     tax = subTotal * 0.05;   //Figuring tax
     totalDue = subTotal + tax;  //Totaling the check
     cout << "------------------------------" << endl;  //The last of the pretty-making code...
     cout << right << setw(24) << "Subtotal = $" << subTotal << endl; //Displaying the subtotal
     cout << right << setw(24) << "Tax (5%) = $" << tax << endl; //Displaying the tax
     cout << right << setw(24) << "Total Due = $" << totalDue << endl << endl; //Displaying the final amount due
     system("pause");  //Once this is passed, the program ends.
}//End printCheck()
