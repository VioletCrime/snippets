/* Calcul8, by Kyle Smith, No-One's Productions, for CIS-160 C11 2/28/10

The idea is to create a program that acts as a calculator. It will request 2 numbers, then one of the 4
basic arithmetic operators (+, -, *, /), perform the calculation, and display the result on the screen.
Also, the calculator must be able to handle floating-point bumbers, as well as 'survive' an attempt to
divide by zero. */

#include <iostream>
#include <iomanip>   //Needed to format the output

using namespace std; // S.S.D.D.

int main()          //Hajimaru
{
    float a, b;      // Declaring two variables with decimal places
    char o;          // Declaring a char for the operator to be stored in
    
    cout << "Enter two numbers: ";   // Requesting the values we'll be manipulating
    cin >> a >> b;                   // In they go
    cout << "Enter operator: + <addition>, - <subtraction>, * <multiplication>, / <division>: ";   // Requesting operator
    cin >> o;   // The more you know...
    cout << "\n";   // Tarting the old girl up
    cout << fixed << showpoint << setprecision(2); // Setting cout to show two decimal places including zeros
    
    switch (o)   // Initializing the switch structure to determine which function needs to be used
    {
           case '+':  // If this is the value of 'o', then the following cout performes the addition
                cout << a << " + " << b << " = " << a + b << endl << endl;
                break;  // Exit the stitch sctructure
           case '-':  // If this is the value of 'o', then the following cout performes the subtraction
                cout << a << " - " << b << " = " << a - b << endl << endl;
                break;  // Exit the stitch sctructure
           case '*':  // If this is the value of 'o', then the following cout performes the multiplication
                cout << a << " * " << b << " = " << a * b << endl << endl;
                break;  // Exit the stitch sctructure
           case '/':  // If this is the value of 'o', then the following cout performes the division
                cout << a << " / " << b << " = ";
                if (b == 0)  // Is the denominator zero? If so, inform the user of the error
                   cout << "ERROR" << endl << "Cannot divide by zero" << endl << endl;
                else  // It's not zero? Well then, let's get back to the math!!
                    cout << a / b << endl << endl;
                break;  // Exit the stitch sctructure 
           default:  // If none of the above 4 operators were input, the user is informed of the mistake, and the program closes having performed no calculations
                   cout << "Invalid operator. The program will now close." << endl << endl;
    }
    
    system("pause");   // Humans don't operate as fast as this super-fly calculator; gotta slow it down
    
    return 0;          //Everything went swimmingly
}                      //Adios, 'main()'!
                   
