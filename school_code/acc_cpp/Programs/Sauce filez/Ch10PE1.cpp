/*Struct Madness by Kyle Smith, No-One's Productions
Made for CIS160C11, Spring 2010*/

#include <iostream> //Communications complex online!
#include <string>   //Like a cat, I play with strings.
#include <fstream>  //Readin like Helen Keller... wait!
#include <iomanip>  //Makin it look pretty

using namespace std;  // Never gonna give you up, never gonna let you down...

struct studentType   //Structure used to store 4 pieces of student information
{
       string studentFName;  //Used to store the student's first name...
       string studentLName;  //... their last name...
       int testScore;  //... their test score...
       char grade;     //...and their grade.
}; //end struct()

void readData(studentType [], ifstream& inny);  //Function prototypes...
void assignGrade(studentType group[]);
void highScore(studentType group[]);
void printNames(studentType group[],int indexHigh);


main() 
{
       ifstream inny;  //Declaring an input variable
       studentType group[20];  //Creating an array 'class' with 20 entries, each of struct 'studentType'
       
       readData(group, inny); //Sending the array to the readData function, in order to fill it.
       
       if(!inny)  //If the ifstream is an a fail state...
                return 1;  //...end the program before the upcoming function calls
       
       assignGrade(group); //Sending the array to assignGrade to fill in 'studentType[xx].grade' values
       
       highScore(group);  //Sending the array to 'highScore' function to determine who has/shares the high score
             
      return 0;  //So long, and thanks for all the fish.
}//End main()


void readData(studentType group[], ifstream& inny)
{
     int lc;  //Loop Control variable
     
     inny.open("Ch10_Ex1Data.txt");  //Opening the input file
     
     if(!inny)  //If the ifstream enters a fail state at this point, the file was not present...
     {
              cout << "Input file could not be located. Please place it in this program's directory";
              cout << "\nand ensure it's named \"Ch10_Ex1Data.txt\"." << endl << endl;
              system("pause");
              return; //... so there's nothing we can really do... =-/
     }
     
     for(lc = 0; lc < 20; lc++) //This loop enters the file's information into the array
     {
            inny >> group[lc].studentFName >> group[lc].studentLName >> group[lc].testScore;
     }
     
     inny.close();  //Closing the input file...
     
}//End readData     


void assignGrade(studentType group[])
{
     int lc; //Loop Control variable
     for(lc = 0; lc < 20; lc++) //This loop simply tests every every 'testScore' entry in the array and assigns a letter grade
     {
            switch(group[lc].testScore / 10) //the '/10' expression also divides my work by a factor of 10 ^_^
            {
                    case 10:
                    case 9:
                         group[lc].grade = 'A';
                         break;
                    case 8:
                         group[lc].grade = 'B';
                         break;
                    case 7:
                         group[lc].grade = 'C';
                         break;
                    case 6:
                         group[lc].grade = 'D';
                         break;
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                    case 1:
                    case 0:
                         group[lc].grade = 'F';
                         break;
                    default:
                         group[lc].grade = 'U';
                         break;
            }//End switch
     }//End for
}//End assignGrade


void highScore(studentType group[])
{
     int indexHigh = 0; //Used to store the high score
     int lc; //Loop Control variable
     
     for(lc = 0; lc < 20; lc++)  //This loop determines the highest score
     {
            if(group[lc].testScore > indexHigh)
                  indexHigh = group[lc].testScore;
     }//End for
     
     printNames(group, indexHigh);  //Sending the array and indexHigh to highScore to determine who wins at academic life!
}//End highScore


void printNames(studentType group[], int indexHigh)
{
     int lc; //Loop Control variable
     
     cout << "The high score was " << indexHigh << ". The student(s) to earn this score were:"<< endl << endl;
     cout << fixed << left; //Setting the left-justification described in the program exercise
     for (lc = 0; lc < 20; lc++)  //This loop tests every testScore entry in the array to see if it equals the high score
     {
         if(group[lc].testScore == indexHigh) //... and if it does, the corresponding student's name is printed
              cout << group[lc].studentLName << ", " << group[lc].studentFName << endl;
     }//End for
     cout << endl; //God is in the details...
     system("pause");
     
}//End printNames
