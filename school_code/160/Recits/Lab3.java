import java.util.Scanner;
public class Lab3 {
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		double a, b;
		int c, d;
		System.out.println("Please enter 2 floating-point numbers, followed by 2 integers:");
		a = keyboard.nextDouble();
		b = keyboard.nextDouble();
		c = keyboard.nextInt();
		d = keyboard.nextInt();
		System.out.println("a = " + a + ", b = " + b + ", c = " + c + ", d = " + d);
		System.out.println("a + b = " + (a + b));
		System.out.println("a / b = " + (a / b));
		System.out.println("c / d = " + (c / d));
		System.out.println("a / d = " + (a / d));
		System.out.println("c % d = " + (c % d));
		System.out.println("a - b x c = " + (a - b * c));
		System.out.println("(a - b) x c = " + ((a - b) * c));
	}

}

