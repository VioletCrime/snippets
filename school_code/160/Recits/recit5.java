import java.util.Scanner;
public class recit5 {
	public static void main(String[] args){
		System.out.printf("Enter a number: ");
		Scanner inny = new Scanner(System.in);
		int usvar = inny.nextInt();
		if (usvar >= 0)
			System.out.println(usvar + " is positive");
		else if (usvar < 0)
			System.out.println(usvar + " is negative");
		
		if (usvar % 2 == 1 || usvar %2 == -1)
			System.out.println(usvar + " is odd");
		else if (usvar % 2 == 0)
			System.out.println(usvar + " is even");
		
		if (usvar % 10 == 0)
			System.out.println(usvar + " is divisible by 10");
		else
			System.out.println(usvar + " is not divisible by 10");
		
		if (usvar % 100 == 0)
			System.out.println(usvar + " is divisible by 100");
		else
			System.out.println(usvar + " is not divisible by 100");
		
		System.out.println(usvar % 10 + " is the last digit of the number " + usvar);
	}
}
