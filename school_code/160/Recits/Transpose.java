//Kyle Smith, for CS160 recitation (#9?) 11/30/10, (e-name: 'ksmitty', cs login: 'smithkyl')

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class Transpose {
	public static void main(String[] args){
		//Get the file open...
		Scanner inny = null;
		File input = new File(args[0]);
		try{
		inny = new Scanner(input);
		}
		catch(FileNotFoundException e){
			System.out.println("Cannot find the file " + args[0] + ". Program will now exit.");
			System.exit(0);	
		}
		
		//Declare variables, and cram the input file's data into the array...
		int i, j, n = inny.nextInt();
		int[][] home = new int[n][n];
		int[][] temp = new int[n][n];
		for(i = 0; i < n; i++){
			for(j = 0; j < n; j++){
				home[i][j] = inny.nextInt();
			}
		}
		
		//Time to transpose....
		for(i = 0; i < n; i++){
			for(j = 0; j < n; j++){
				temp[j][i] = home[i][j];
			}
		}
		
		//Time to output the finished product...
		for( i = 0; i < n; i++){
			for( j = 0; j < n; j++){
				System.out.print(temp[i][j] + " ");
			}
			System.out.println();
		}
		
		
	}
}
