import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.text.*;


public class Bleep {
		public static void main(String[] args){
			int i = 0, j = 0;
			double result = 0, swear = 0, total = 0;
			boolean period = false;
 
			String gosh = "gosh";
			String darn = "darn";
			String bum = "bum";
			String barking = "barking";

			File fileName = new File(args[0]);
		    Scanner input = null;
		    try
		    {
		    input = new Scanner(fileName);
		    }
		    catch(FileNotFoundException e)
		    {
		    e.printStackTrace();
		    System.exit(0);
		    }
		        
		    String line = input.nextLine();
		    String[] intext = line.split("[ ]");
		    total = Array.getLength(intext);
		    
		    for(i = 0; i < total; i++){
		    	String temp = intext[i];
		    	j = temp.length();
		    	if (temp.charAt(j-1) == '.'){
		    		temp = intext[i].substring(0, j-1);
		    		period = true;
		    	}
		    	if (temp.equalsIgnoreCase(gosh)){
		    		swear++;
		    		temp = temp.substring(0, 1) + "***";
		    	}
		    	if (temp.equalsIgnoreCase(bum)){
		    		swear++;
		    		temp = temp.substring(0, 1) + "**";
		    	}
		    	if (temp.equalsIgnoreCase(darn)){
		    		swear++;
		    		temp = temp.substring(0, 1) + "***";
		    	}
		    	if (temp.equalsIgnoreCase(barking)){
		    		swear++;
		    		temp = temp.substring(0, 1) + "******";
		    	}
		    	if(period == true){
		    		System.out.print(temp + ". ");
		    		period = false;
		    	}
		    	else 
		    		System.out.print(temp + " ");		    	
		    }
		    
		    result = swear / total * 100;
		    String fmt = "#.#";
		    DecimalFormat df = new DecimalFormat(fmt);
		    System.out.print("\n" + df.format(result) + "% of the words are inappropriate.\n\n");
		}

}
