/*'MyCourse', by Kyle Smith (ksmitty) on 9/8/10 for CS160*/

public class MyCourse
{
 public static void main(String[] args)
 {
     System.out.println("First Name: Kyle");
     System.out.println("Last Name:  Smith");
     System.out.println("Nickname:   Kyle");
     System.out.println("Course:     CS160");
     System.out.println("E-Name:     ksmitty");
     System.out.println("CS Login:   ksmitty");
     System.out.println("Head:       Ronald Reagan");
 }
}