// Kyle Smith (E-name 'ksmitty', CS 'smithkyl') Nov 22nd, 2010, CS160, HW6

import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Pic
{
    // Once we've read an image, it implicitly has this maximum value:
    static final int MAXVAL = 255;

    // This 2-d array contains the pixel values.
    private int[][] imageData = null;

    // Constructor for the class Pic
    public Pic(String path) throws Exception {
        imageData = readPGM(path);
    }

    // Read the picture from the given path.
    //
    // An ASCII PGM file looks like this:
    // P2
    // width height
    // maxval
    // one number per pixel

    private int[][] readPGM(String path) throws Exception {
	Scanner in = new Scanner(new File(path));
	String magic = in.next();
	if (!magic.equals("P2")) {
	    System.err.println("ERROR: "+path+" is not an ASCII PGM file");
	    System.exit(1);
	}
	int width = in.nextInt();
	int height = in.nextInt();
	int fileMaxval = in.nextInt();
	int[][] pic = new int[height][width];

	// Old, slow, readable code:
	for (int y=0; y<height; y++)
	    for (int x=0; x<width; x++)
	        pic[y][x] = in.nextInt();

	// New, fast, perplexing, code;
	// String content = in.useDelimiter("\\Z").next();
	// StringTokenizer st = new StringTokenizer(content);
	// int x=0, y=0;
	// while (st.hasMoreTokens()) {
	//     pic[y][x] = Integer.valueOf(st.nextToken());
	//     if (++x >= width) {
	// 	x = 0;
	// 	y++;
	//     }
	// }

	in.close();

	// We want our values to be in the range 0-MAXVAL.  If the file data
	// wasn't in that range, scale it up to be in that range.

	if (fileMaxval != MAXVAL)
	    for (int j=0; j<height; j++)
		for (int i=0; i<width; i++)
		    pic[j][i] = (pic[j][i] * MAXVAL)/fileMaxval;

	return pic;
    }


    // Negate the image.
    // That is, if a pixel has the value 0, replace it with MAXVAL.
    // If a pixel has the value 1, replace it with MAXVAL-1.
    // If a pixel has the value 7, replace it with MAXVAL-7, etc.

    public void negate() {
    	int i = 0, j = 0;
    	for (i = 0; i < imageData.length; i++){
    		for (j = 0; j < imageData[0].length; j++){
    			imageData[i][j] = MAXVAL - imageData[i][j];
    		}
    	}
    }


    // Flip the image horizontally.
    // If it's a person looking left, they'll end up looking right.

    public void horizontalFlip() {
    	int i, j, h = imageData.length, w = imageData[0].length;
    	int[][] temp = new int[h][w];
    	for (i = 0; i < h; i++){
    		for (j = 0; j < w; j++){
    			temp[i][j] = imageData[i][(w- 1) - j];
    		}
    	}
    	imageData = temp;
    }


    // Flip the image vertically.
    // A picture of a person will end up standing on their head.

    public void verticalFlip() {
    	int i, j, h = imageData.length, w = imageData[0].length;
    	int[][] temp = new int[h][w];
    	for (j = 0; j < w; j++){
    		for (i = 0; i < h; i++){
    			temp[i][j] = imageData[(h - 1) - i][j];
    		}
    	}
    	imageData = temp;
    }

    // Rotate the image right, i.e., 90 degrees clockwise.
    // A picture of a person will end up getting 
    // rotated right by 90 degrees.

    public void rotateRight() {
    	int i, j, h = imageData.length, w = imageData[0].length;
    	int[][] temp = new int [w][h];
    	for (i = 0; i < h; i++){
    		for (j = 0; j < w; j++){
    			temp[j][(h - 1) - i] = imageData[i][j];
    		}
    	}
    	
    	imageData = temp;
    }

    // Rotate the image left, i.e., 90 degrees counterclockwise.
    // A picture of a person will end up getting 
    // rotated left by 90 degrees.

    public void rotateLeft() {
    	int i, j, h = imageData.length, w = imageData[0].length;
    	int[][] temp = new int [w][h];
    	for (i = 0; i < h; i++){
    		for (j = 0; j < w; j++){
    			temp[(w - 1) - j][i] = imageData[i][j];
    		}
    	}
    	imageData = temp;
    }

    // Bleep the image using a white rectangle.

    public void bleep(int top_row, 
                      int top_col, 
                      int bottom_row, 
                      int bottom_col) {
    	int i, j, h = imageData.length, w = imageData[0].length;
    	for (i = 0; i < h; i++){
    		for (j = 0; j < w; j++){
    			if (i >= top_row && i <= bottom_row && j >= top_col && j <= bottom_col){
    				imageData[i][j] = MAXVAL;
    			}
    		}
    	}
    }


    // Write the picture to the given path.

    public void writePGM(String path) throws Exception {
	int height = imageData.length;
	int width = imageData[0].length;
	PrintStream out = new PrintStream(new FileOutputStream(path));
	out.println("P2");
	out.println(width+" "+height);
	out.println(MAXVAL);			// maximum pixel value
	for (int y=0; y<height; y++)
	    for (int x=0; x<width; x++)
		out.println(imageData[y][x]);	// Yeah, one pixel per line.  So?
	out.close();
    }

    public static void usage() {
	System.err.println("usage: java Pic <infile> <outfile> (h|v|n|rr|rl|b) ...");
	System.exit(1);
    }

    public static void main(String[] args) throws Exception {
	if (args.length < 2)
	    usage();

        Scanner keyboard = new Scanner(System.in);

        Pic picture = new Pic(args[0]);

	for (int i=2; i<args.length; i++) {
	    String op = args[i];
	    if (op.equals("h"))
		picture.horizontalFlip();
	    else if (op.equals("v"))
		picture.verticalFlip();
	    else if (op.equals("n"))
		picture.negate();
	    else if (op.equals("rr"))
		picture.rotateRight();
	    else if (op.equals("rl"))
		picture.rotateLeft();
            else if (op.equals("b")) {
                System.out.println("Enter four numbers for upper_left_row, upper_left_col, lower_right_row, and lower_right_col");
                int upperLeftRow = keyboard.nextInt();
                int upperLeftCol = keyboard.nextInt();
                int upperRightRow = keyboard.nextInt();
                int upperRightCol = keyboard.nextInt();
                picture.bleep(upperLeftRow, upperLeftCol, upperRightRow, upperRightCol);
            }
	    else
	    	System.err.println("Invalid operation \""+op+"\"");
	}

	picture.writePGM(args[1]);
    }
}


