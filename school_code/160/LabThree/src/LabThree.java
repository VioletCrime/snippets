import java.util.Scanner;
import java.io.File;
import java.io.IOException;
public class LabThree {
	public static void main(String[] args) throws IOException{
		Scanner sc = new Scanner(new File("input.txt"));
		double a = sc.nextDouble();
		double b = sc.nextDouble();
		int c = sc.nextInt();
		int d = sc.nextInt();
		System.out.println("a = " + a + ", b = " + b + ", c= " + c + ", d = " + d);
		System.out.println("a + b = " + (a + b));
		System.out.println("a / b = " + (a / b));
		System.out.println("c / d = " + (c / d));
		System.out.println("a / d = " + (a / d));
		System.out.println("c % d = " + (c % d));
		System.out.println("a - b x c = " + (a - b * c));
		System.out.println("(a - b) x c = " + ((a - b) * c));
	}

}
