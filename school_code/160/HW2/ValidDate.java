/*ValidDate by Kyle Smith (ksmitty) for CIS-160. 9/20/10*/


import java.util.Scanner;

public class ValidDate {

    public static void main(String[] args){

       System.out.println("Enter the date as: mm" + '/' + "dd" + '/' + "yyyy");
       Scanner inny = new Scanner(System.in);
       boolean mon = false, dai = false, yeer = false; //Bool's to remember user mistakes to later alert user of
       int lastDay = 0;  //Variable to hold number of days in the given month
       inny.useDelimiter("/|\n|\r");
       int month = inny.nextInt();
       int day = inny.nextInt();
       int year = inny.nextInt();
       
       
/*The following if() statements determine the number of days in the given month, culminating with a 
 * larger nested if-else control structure for February's interesting 29th day*/
       if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month ==10 || month == 12){
    	   	lastDay = 31;
       }
       if (month == 4 || month == 6 || month == 9 || month == 11){
    	   	lastDay = 30;
       }
       if (month == 2){
    	   if (year % 4 == 0){
    		   if (year % 100 == 0){
    			   if (year % 400 == 0){
    				   lastDay = 29;
    			   }
    			   else
    				   lastDay = 28;
    		   }
    		   else
    			   lastDay = 29;
    	   }
    	   else
    		   lastDay = 28;
       }
       

       
       
//Testing each of the user-defined inputs for validity, noting invalid entries
       if (month < 1 || month > 12){
          mon = true;
          lastDay = 31;
          }
       if (year < 1590 || year > 2400) {
          yeer = true;
         }       
       if (day < 0 || day > lastDay){
    	   dai = true;
       }

       
/*Output structure. Tests each output flag and outputs accordingly.*/
     if (mon || dai || yeer){
  	   System.out.printf(month + "/" + day + "/" + year + ": Invalid: ");
  	   if (mon){
  		   System.out.printf("Month");
  	   }
  	   if (mon && !dai && day > 28)
  		   System.out.printf(", *Possibly* Day");
  	   if (dai){
  		   if (mon)
  			   System.out.printf(", ");
  		   System.out.printf("Day");
  	   }
  	   if (yeer){
  		   if (mon || dai)
  			   System.out.printf(", ");
  		   System.out.printf("Year");
  	   }
 	   System.out.println(".");    	   
     }
     else
  	   System.out.println(month + "/" + day + "/" + year + ": Valid.");
       
       
       
// The following block serves to troubleshoot the program; didn't see fit to remove it, just in case...
       
//       System.out.println(month + "/" + day + "/" + year);
//       System.out.println("lastDay = " + lastDay);
//       System.out.println(mon);
//       System.out.println(dai);
//       System.out.println(yeer);
    }
}