/*Enc.java by Kyle Smith (CS: smithkyl, RamCT: ksmitty) for CS160 HW5, started 10/30/10 */

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Enc {
	
	//This method tests the command-line arguments to make sure they're up to snuff, returning a boolean marker.
	public static boolean checkArgs(String[] args){
		if(args.length == 3){
			if(args[0].equals("-e") || args[0].equals("-d"))return true;
			else{
				System.out.println("usage: -d|-e keyfile datafile");
				return false;
			}
		}
		else{
			System.out.println("Usage: -d|-e keyfile datafile");
			return false;
		}
	}
	
	
	//Encrypting method; takes the alphabet and key string arrays as well as a line from the input file, returning 
	//the encrypted version of the text
	public static String encrypt(String[] alphabet, String[] keyabet, String cipher){
		int i, j;
		String sub;
		String output = "";
		for(i = 0; i < cipher.length(); i++){
			sub = cipher.substring(i, i+1);
			if(cipher.charAt(i) >= 'a' && cipher.charAt(i) <= 'z'){
				for(j = 0; j < 26; j++){
					if(sub.equalsIgnoreCase(alphabet[j]))
						output += keyabet[j];
				}
			}
			else if(cipher.charAt(i) >= 'A' && cipher.charAt(i) <= 'Z'){
				for(j = 0; j < 26; j++){
					if(sub.equalsIgnoreCase(alphabet[j]))
						output += keyabet[j].toUpperCase();
				}
			}
			else
				output += cipher.charAt(i);
			
		}
		return output;
	}
	
	
	//Decryption method; virtually identical to the encryption method, but switching test and output arrays.
	public static String decrypt(String[] alphabet, String[] keyabet, String cipher){
		int i, j;
		String sub;
		String output = "";
		for(i = 0; i < cipher.length(); i++){
			sub = cipher.substring(i, i+1);
			if(cipher.charAt(i) >= 'a' && cipher.charAt(i) <= 'z'){
				for(j = 0; j < 26; j++){
					if(sub.equalsIgnoreCase(keyabet[j]))
						output += alphabet[j];
				}
			}
			else if(cipher.charAt(i) >= 'A' && cipher.charAt(i) <= 'Z'){
				for(j = 0; j < 26; j++){
					if(sub.equalsIgnoreCase(keyabet[j]))
						output += alphabet[j].toUpperCase();
				}
			}
			else
				output += cipher.charAt(i);
			
		}
		return output;
	}
	
	
	public static void main(String[] args){
		//If the args don't check out, break early in the main method.
		boolean test = checkArgs(args);
		if(!test)System.exit(0);
		
		//Everything must check out on the user end; time to declare stuff...
		String[] alphabet = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
		String[] keyabet = new String[26];
		
		//Set up file inputs, first for the key file, second for the input file
		Scanner keyScan = null;
		File key = new File(args[1]);
		try{
		keyScan = new Scanner(key);
		}
		catch(FileNotFoundException e){
			System.out.println("Cannot find the file " + args[0] + ". Program will now exit.");
			System.exit(0);	
		}
		
		
		Scanner inScan = null;
		File input = new File(args[2]);
		try{
		inScan = new Scanner(input);
		}
		catch(FileNotFoundException e){
			System.out.println("Cannot find the file " + args[0] + ". Program will now exit.");
			System.exit(0);	
		}
		
		
		//Jam the key file into a string array... found out the hard way how much harder a char array is to manipulate.
		String temp = keyScan.nextLine();
		keyabet = temp.split(" ");
		
		
		//The breadwinner structure. Reads a line of input, and based on args[0], calls appropriate method.
		do{
			String cipher = inScan.nextLine();
			if(args[0].equalsIgnoreCase("-e")){
				cipher = encrypt(alphabet, keyabet, cipher);
				System.out.println(cipher);
			}
			else{
				cipher = decrypt(alphabet, keyabet, cipher);
				System.out.println(cipher);
			}
		}while(inScan.hasNextLine());
		
	}
}
