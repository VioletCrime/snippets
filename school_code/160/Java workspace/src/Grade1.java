import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Grade1 {
	public static void main(String[] args){
		int[] grades = new int[5];
		int i = 0;
		int j =0;
		double butt = 0;
		double[] fromFile = new double[100];
		int butts = 0;
		File inFile = new File(args[0]);
		Scanner inny = null;
		
		try
		{
			inny = new Scanner(inFile);
		}
		catch(FileNotFoundException e)
		{
			System.out.println("Error opening the file " + inFile + ".");
		    e.printStackTrace();
		    System.exit(0);
		}
		
		for (i=0; i< 100; i++){
			fromFile[i] = inny.nextDouble();
			if (fromFile[i] == -999)
				break;
			j++;
		}
		i = 0;
		do{
			butt = fromFile[i];
			butts = (int) (butt / 10);
			switch (butts){
			case 10:
			case 9:
				grades[0]++;
				break;
			case 8:
				grades[1]++;
				break;
			case 7:
				grades[2]++;
				break;
			case 6:
				grades[3]++;
				break;
			default:
				grades[4]++;
				break;
			}
			i++;
		}while(fromFile[i] != -999);
		System.out.print("A: " + grades[0]);
		if(grades[0] >= 10)
			System.out.print(" ");
		else
			System.out.print("  ");
		for (i = 1; i <= grades[0]; i++)
			System.out.print("#");
		System.out.print("\nB: " + grades[1]);
		if(grades[1] >= 10)
			System.out.print(" ");
		else
			System.out.print("  ");
		for (i = 1; i <= grades[1]; i++)
			System.out.print("#");
		System.out.print("\nC: " + grades[2]);
		if(grades[2] >= 10)
			System.out.print(" ");
		else
			System.out.print("  ");
		for (i = 1; i <= grades[2]; i++)
			System.out.print("#");
		System.out.print("\nD: " + grades[3]);
		if(grades[3] >= 10)
			System.out.print(" ");
		else
			System.out.print("  ");
		for (i = 1; i <= grades[3]; i++)
			System.out.print("#");
		System.out.print("\nF: " + grades[4]);
		if(grades[4] >= 10)
			System.out.print(" ");
		else
			System.out.print("  ");
		for (i = 1; i <= grades[4]; i++)
			System.out.print("#");
		System.out.println("\n");
		
	}
}
