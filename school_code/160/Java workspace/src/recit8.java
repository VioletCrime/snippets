import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class recit8 {
	public static void main(String[] args){
		int noFiles = args.length;
		if(noFiles == 0){
			System.out.println("You must specify an input file. Program will now exit.");
			System.exit(0);
}
		if(noFiles >= 2){
			System.out.println("Too many input files specified. Trying the first.\n");
		}
		int i = 0, j = 0;
		char a, b;
		boolean palin= true;
		String temp, input;
		File inFile = new File(args[0]);
		Scanner inny = null;
		try{
			inny = new Scanner(inFile);
		}
		catch(FileNotFoundException e){
			System.out.println("Cannot find the file " + args[0] + ". Program will now exit.");
			System.exit(0);			
		}		
		while(inny.hasNextLine()){
			palin = true;
			input = inny.nextLine();
			temp = input.toLowerCase();
			if(temp.length() == 0){
				System.out.println("The file is unreadable, program will now exit.");
				System.exit(0);
			}
			
			for(i = 0, j = temp.length() - 1; i < j; i++, j--){
				while(!((temp.charAt(i) <= 'z' && temp.charAt(i) >= 'a'))){
					i++;
					if(i >= temp.length()/2){
						i = j;
						break;
					}
				}
				while(!((temp.charAt(j) <= 'z' && temp.charAt(j) >= 'a'))){
					j--;
					if(j <= temp.length()/2){
						i = j;
						break;
					}
				}
				a = temp.charAt(i);
				b = temp.charAt(j);
				if(a != b)
					palin = false;
			}
			if(palin == false)
				System.out.println("Not a palindrome: " + input);
			else
				System.out.println("Palindrome: " + input);
		}
	}
}
