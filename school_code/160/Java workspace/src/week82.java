import java.util.Scanner;
import java.io.IOException;
public class week82 {
	public static void main(String[] args) throws IOException{
		Scanner inny = new Scanner(System.in);
		System.out.printf("Enter your date (mm/dd/yyyy), enter 0/0/0 to exit: ");
		inny.useDelimiter("/|\n|\r");
		int month = inny.nextInt(), day = inny.nextInt(), year = inny.nextInt();
		while (month != 0 && day != 0 && year != 0)
		{
			switch (month)
			{
			case 1:
				System.out.printf("January ");
				break;
			case 2:
				System.out.printf("February " );
				break;
			case 3:
				System.out.printf("March ");
				break;
			case 4:
				System.out.printf("April ");
				break;
			case 5:
				System.out.printf("May ");
				break;
			case 6:
				System.out.printf("June ");
				break;
			case 7:
				System.out.printf("July ");
				break;
			case 8:
				System.out.printf("August ");
				break;
			case 9:
				System.out.printf("September ");
				break;
			case 10:
				System.out.printf("October ");
				break;
			case 11:
				System.out.printf("November ");
				break;
			case 12:
				System.out.printf("December  ");
				break;
			default:
				System.out.printf("Unknown month ");
				break;
			}
			if (day % 10 == 1 && day != 11)
				System.out.printf(day + "st, ");
			else if (day % 10 == 2 && day != 12)
				System.out.printf(day + "nd, ");
			else if (day % 10 == 3 && day != 13)
				System.out.printf(day + "rd, ");
			else
				System.out.printf(day + "th, ");
			System.out.println(year);
			System.out.println("");
			System.out.printf("Enter your date (mm/dd/yyyy), enter 0/0/0 to exit: ");
			month = inny.nextInt();
			day = inny.nextInt();
			year = inny.nextInt();
		}
	}
}
