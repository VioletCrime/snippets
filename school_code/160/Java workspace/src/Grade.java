import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.lang.Math;
import java.text.*;

public class Grade {

	public static void main(String[] args){
		
		//Set up the imput file
		Scanner inny = null;
		File key = new File(args[0]);
		try{
		inny = new Scanner(key);
		}
		catch(FileNotFoundException e){
			System.out.println("Cannot find the file " + args[0] + ". Program will now exit.");
			System.exit(0);	
		}
		
		//Declare a metric fuckton of variables
		int i = 0, j = 0;
		double max = 0, min = 100, sum = 0, mean = 0, temp = 0;
		int m = inny.nextInt();
		int n = inny.nextInt();
		double[][] stats = new double[m][n];
		double[][] storea = new double[4][n];//[0] = min, [1] = max, [2] = mean, [3] = std. dev.
		double[][] stores = new double[4][m];//ditto
		
		//Read data from file into array
		for (i = 0; i < m; i++){
			for (j = 0; j < n; j++){
				stats[i][j] = inny.nextInt();
			}
		}
		
		//Test and calculate assignment values, feeding them into a storage array awaiting output
		for(i = 0; i < n; i++){
			for(j = 0; j < m; j++){
				if (stats[j][i] < min) min = stats[j][i];
				if (stats[j][i] > max) max = stats[j][i];
				sum += stats[j][i];				
			}
			storea[0][i] = min;
			storea[1][i] = max;
			mean = (sum / m);
			storea[2][i] = (sum / m);
			min = 100;
			max = 0;
			sum = 0;
			for(j = 0; j < m; j++){
				temp = stats[j][i] - mean;
				sum = sum + (temp * temp);
			}
			sum = Math.sqrt(sum / (m - 1));
			storea[3][i] = sum;
		}
		
		//Test and calculate student values, feeding them into a different storage array awaiting output
		for(i = 0; i < m; i++){
			for(j = 0; j < n; j++){
				if (stats[i][j] < min) min = stats[i][j];
				if (stats[i][j] > max) max = stats[i][j];
				sum += stats[i][j];				
			}
			stores[0][i] = min;
			stores[1][i] = max;
			mean = (sum / m);
			stores[2][i] = (sum / m);
			min = 100;
			max = 0;
			sum = 0;
			for(j = 0; j < n; j++){
				temp = stats[i][j] - mean;
				sum = sum + (temp * temp);
			}
			sum = Math.sqrt(sum / (n - 1));
			stores[3][i] = sum;
		}
		
		//Time for the massive output block...
		NumberFormat formatter = new DecimalFormat("#0.00");
		System.out.println("ASSIGNMENT VALUES");
		System.out.print("Min: ");
		for (i = 0; i < n; i++){
			System.out.print((int) storea[0][i] + " ");
		}
		System.out.print("\nMax: ");
		for (i = 0; i < n; i++){
			System.out.print((int) storea[1][i] + " ");
		}
		System.out.print("\nMean: ");
		for (i = 0; i < n ; i++){
			System.out.print(formatter.format(storea[2][i]) + " ");
		}
		System.out.print("\nStandard Deviation: ");
		for (i = 0; i < n; i++){
			System.out.print(formatter.format(storea[3][i]) + " ");
		}
		
		System.out.println("\n\nSTUDENT VALUES");
		System.out.print("Min: ");
		for (i = 0; i < m; i++){
			System.out.print((int) stores[0][i] + " ");
		}
		System.out.print("\nMax: ");
		for (i = 0; i < m; i++){
			System.out.print((int) stores[1][i] + " ");
		}
		System.out.print("\nMean: ");
		for (i = 0; i < m ; i++){
			System.out.print(formatter.format(stores[2][i]) + " ");
		}
		System.out.print("\nStandard Deviation: ");
		for (i = 0; i < m; i++){
			System.out.print(formatter.format(stores[3][i]) + " ");
		}
		
	}
}
