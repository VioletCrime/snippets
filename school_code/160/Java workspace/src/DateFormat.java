import java.util.Scanner;
import java.io.IOException;
public class DateFormat {
	public static void main(String[] args) throws IOException{
		Scanner inny = new Scanner(System.in);
		int lastDay = 0;
		System.out.printf("Enter your date (mm/dd/yyyy), enter 0/0/0 to exit: ");
		inny.useDelimiter("/|\n|\r");
		int month = inny.nextInt(), day = inny.nextInt(), year = inny.nextInt();
		while (month != 0 && day != 0 && year != 0)
		{
			 if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month ==10 || month == 12){
		    	   	lastDay = 31;
		       }
		       if (month == 4 || month == 6 || month == 9 || month == 11){
		    	   	lastDay = 30;
		       }
		       if (month == 2){
		    	   if (year % 4 == 0){
		    		   if (year % 100 == 0){
		    			   if (year % 400 == 0){
		    				   lastDay = 29;
		    			   }
		    			   else
		    				   lastDay = 28;
		    		   }
		    		   else
		    			   lastDay = 29;
		    	   }
		    	   else
		    		   lastDay = 28;
		       }
		    if ((month < 1 || month > 12) || (day < 1 || day > lastDay))
		    	System.out.print("There was no ");
		       
			switch (month)
			{
			case 1:
				System.out.printf("January ");
				break;
			case 2:
				System.out.printf("February " );
				break;
			case 3:
				System.out.printf("March ");
				break;
			case 4:
				System.out.printf("April ");
				break;
			case 5:
				System.out.printf("May ");
				break;
			case 6:
				System.out.printf("June ");
				break;
			case 7:
				System.out.printf("July ");
				break;
			case 8:
				System.out.printf("August ");
				break;
			case 9:
				System.out.printf("September ");
				break;
			case 10:
				System.out.printf("October ");
				break;
			case 11:
				System.out.printf("November ");
				break;
			case 12:
				System.out.printf("December  ");
				break;
			default:
				System.out.printf("Unknown month ");
				break;
			}
			if (day % 10 == 1 && day != 11)
				System.out.printf(day + "st, ");
			else if (day % 10 == 2 && day != 12)
				System.out.printf(day + "nd, ");
			else if (day % 10 == 3 && day != 13)
				System.out.printf(day + "rd, ");
			else
				System.out.printf(day + "th, ");
			System.out.print(year + "\n\n");
			System.out.printf("Enter your date (mm/dd/yyyy), enter 0/0/0 to exit: ");
			month = inny.nextInt();
			day = inny.nextInt();
			year = inny.nextInt();
		}
	}
}
