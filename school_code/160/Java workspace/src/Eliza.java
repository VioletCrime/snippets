import java.util.Scanner;
public class Eliza {
public static void main(String[] args){
	
	//Declaring variables
	int lc = 0, total = 0, slen = 0, lIndex = 0, lMax = 0;
	String input, temp;
	
	//Prompting the user for input, initializing a keyboard scanner.
	System.out.println("What would you like to talk about?");
	Scanner inny = new Scanner(System.in);
	input = inny.nextLine();
	
	//Meat and potatoes loop. User stays in until they type 'thanks'
	while (!input.equalsIgnoreCase("Thanks")){
		lMax = 0; //otherwise lMax would stay the same as it was in the last loop
		String[] sentence = input.split("[ ]"); //Splitting the input into individual words, placing in array 'sentence'
		total = sentence.length; //finding the number of iterations in the array
		
		//This series of loops test for punctuation at the end of the each word, and finds the longest word in the array
		for(lc = 0; lc < total; lc++){
			temp = sentence[lc];
			slen = temp.length();
			if(temp.charAt(slen - 1) == '?' || temp.charAt(slen - 1) == '!' || temp.charAt(slen - 1) == ',' || temp.charAt(slen - 1) == '.'){
				sentence[lc] = temp.substring(0, slen - 1);
				slen--;
			}
			if(slen > lMax){
				lIndex = lc;
				lMax = slen;
			}
		}
		
		//If-else structure outputs according to the largest word in the user input, prompting for the next line.
		if(lMax == 4)
			System.out.println("Tell me more about " + sentence[lIndex].toLowerCase() + ".");
		else if(lMax == 5)
			System.out.println("Why do you think " + sentence[lIndex].toLowerCase() + " is important?");
		else if (lMax > 5)
			System.out.println("Now we are getting somewhere. How does " + sentence[lIndex].toLowerCase() + " affect you the most?");
		else
			System.out.println("Maybe we should move on. Is there something else you would like to talk about?");
		input = inny.nextLine();
	}		
	
	//User will have opted to quit to see this goodbye message.
	System.out.println("Goodbye.");
	}
}
