#!/bin/bash

# You'll need to run the following statement to get the required permissions.
# GRANT ALL PRIVILEGES ON csadb.* TO 'root'@'localhost' IDENTIFIED BY 'stack';
# FLUSH PRIVILEGES;

set -e

OUTPUT_FILE="database_summary.txt"
if [ -f $OUTPUT_FILE ]; then
    rm $OUTPUT_FILE
fi

LIST_OF_TABLES=`mysql -B -N -D "csadb" --password="stack" --execute="show tables;"`
for TABLE in $LIST_OF_TABLES; do
    TABLE_FIELDS=`mysql -B -N -D "csadb" --password="stack" --execute="SELECT column_name FROM information_schema.columns WHERE table_schema = 'csadb' AND table_name = '$TABLE';"`
    TABLE_HEAD=`mysql -B -N -D "csadb" --password="stack" --execute="SELECT * FROM $TABLE LIMIT 1;"`

    echo $TABLE >> $OUTPUT_FILE
    echo $TABLE_FIELDS >> $OUTPUT_FILE
    echo "EXAMPLE:    ===================================" >> $OUTPUT_FILE
    echo $TABLE_HEAD >> $OUTPUT_FILE
    echo "-----------------------------------------------" >> $OUTPUT_FILE
    echo '' >> $OUTPUT_FILE
done
