#!/bin/bash

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $))
readonly ARGS="$@"

validate_session_and_os(){
    if [[ ! $(whoami) == "root" ]]; then
        echo "ERROR: This script must be run as root! Exiting."
        exit 2
    fi

    if [[ ! -e /ci/bin/wait-for-cic ]]; then
        version_error
    fi
}

identify_system(){   
    if service csa status 2>&1 | grep "csa .* is running..." > /dev/null; then
        readonly DB="csa"
        readonly DB_USER="csa"
    elif
        service hp-oo status 2>&1 | grep 'HP OO is running' > /dev/null; then
        readonly DB="hp_oo"
        readonly DB_USER="postgres"
    else
        version_error
    fi
}

version_error(){
    echo "ERROR: This script only works on CloudSystem 8.X Foundation and Enterprise appliances. Exiting."
    exit 1
}

run_psql_cmd(){
    psql -U$DB_USER -c "$@" $DB
}

get_table_list(){
    run_psql_cmd "\d+" | grep 'table' | cut -d '|' -f2 | tr -d ' '
}

clean_psql_output(){
    echo $(echo $@ | cut -d '(' -f1 | rev | cut -d '-' -f1 | rev | tr -d ' ')
}

estimate_max_list_size(){
    local highest_size_per_row=0
    for table in $(get_table_list); do
        local size=$(clean_psql_output $(run_psql_cmd "SELECT pg_total_relation_size('$table');"))
        local row_count=$(clean_psql_output $(run_psql_cmd "SELECT COUNT(*) FROM $table;"))
        if [[ $row_count -gt 0 ]]; then
            local size_per_row=$(($size / $row_count))
            if [[ $size_per_row -gt $highest_size_per_row ]]; then
                local highest_size_per_row=$size_per_row
            fi
        fi
    done
    echo $(($((2**30*5))/$highest_size_per_row))
}

main() {
    validate_session_and_os
    identify_system
    echo "The value for max query / list sizes should be: $(estimate_max_list_size)"
    echo "(Lines 94 and 108 of /opt/stack/venvs/csmigrate/lib/python2.7/site-packages/csmigrate/steps/cs8_5/online_migrate.py on ea1)"
}

main
