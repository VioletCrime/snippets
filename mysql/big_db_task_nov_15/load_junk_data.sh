#!/bin/bash

function csa_variables(){
    #CSA
    BASE_ITERATIONS=87382
    PSQL_DB=csa
    PSQL_USER=csa
    PSQL_PASS=csa
    TABLE_ONE=junk_data_twogig
    TABLE_TWO=junk_data_fourgig
    TABLE_TRE=junk_data_sixgig
}

function oo_variables(){
    #OO
    BASE_TERATIONS=873814
    PSQL_DB=hp_oo
    PSQL_USER=postgres
    PSQL_PASS=postgres
    TABLE_ONE=junk_data_twentygig
    TABLE_TWO=junk_data_fourtygig
    TABLE_TRE=junk_data_sixtygig
}

csa_variables
#oo_variables

bigtext=$(cat alpine.txt)

ITERATIONS_MULTIPLY=1
for table in $TABLE_ONE $TABLE_TWO $TABLE_TRE; do
    ITERATION=1
    let TOTAL_ITERATIONS=$ITERATIONS_MULTIPLY*$BASE_ITERATIONS

    while [ $ITERATION -lt $TOTAL_ITERATIONS ]; do
        let ITERATION=$ITERATION+1
        if [[ $(($ITERATION % 100)) -eq 0 ]]; then
            echo "Iteration $ITERATION of $TOTAL_ITERATIONS"
        fi
        psql -U$PSQL_USER -c "insert into $table (entrynum, data) values ($ITERATION, '$bigtext');" $PSQL_DB
    done
    let ITERATIONS_MULTIPLY=$ITERATIONS_MULTIPLY+1
done
