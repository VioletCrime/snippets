#!/bin/bash

VMS=`vagrant status | grep 'running' | cut -d ' ' -f 1`
for VM in $VMS
do
    IP=`vagrant ssh $VM -c 'ifconfig' | grep 'inet addr' | head -n1 | cut -d ':' -f 2 | cut -d ' ' -f 1`
    echo $VM is at: $IP
done
