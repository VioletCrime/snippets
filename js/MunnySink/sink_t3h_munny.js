
// Select all text nodes on the page
//const allTextNodes = document.createTreeWalker(document.body, NodeFilter.SHOW_TEXT);

// Regular expression to match dollar amounts
//const dollarAmountRegex = /\$\d+(\.\d{1,2})?/g;  // GPT's first attempt
//const dollarAmountRegex = '/^\$?\d+(,\d{3})*\.?[0-9]?[0-9]?$/i';  // My first attempt
//const dollarAmountRegex = /\$[\d,]+(?:\.\d{1,2})?/g;

// Loop through all text nodes on the page
//while (allTextNodes.nextNode()) {
//  const textNode = allTextNodes.currentNode;
  
  // Check if the text node contains a dollar amount
//  if (dollarAmountRegex.test(textNode.nodeValue)) {
    // Replace the dollar amount with an empty string
//    textNode.nodeValue = textNode.nodeValue.replace(dollarAmountRegex, '');
//  }
//}

(function() {
    'use strict';

    const dollarAmountRegex = /\$[\d,]+(?:\.\d{1,2})?/g;
    const priceDescriptorsSelector = 'shopify-product-form';

    function removeDollarAmountsAndPriceDescriptors() {
        // Remove dollar amounts
        const allTextNodes = document.createTreeWalker(document.body, NodeFilter.SHOW_TEXT);
        while (allTextNodes.nextNode()) {
            const textNode = allTextNodes.currentNode;
            let newText = textNode.nodeValue;
            let match;
            while ((match = dollarAmountRegex.exec(newText)) !== null) {
                newText = newText.replace(match[0], '');
            }
            textNode.nodeValue = newText;
        }

        // Remove elements with ID "price-descriptors"
        //const priceDescriptorsElements = document.querySelectorAll(priceDescriptorsSelector);
        //for (let i = 0; i < priceDescriptorsElements.length; i++) {
        //    const element = priceDescriptorsElements[i];
        //    element.parentNode.removeChild(element);
        //}
		// Get the span element with the ID 'shopify-installments-content'
		const spanToRemove = document.getElementById('shopify-installments-content');

		// Check if the element exists on the page
		if (spanToRemove) {
		  // Remove the element from the DOM
 		 spanToRemove.remove();
		}
    }

    // Initial run of the script
    removeDollarAmountsAndPriceDescriptors();

    // Monitor the page for changes using a MutationObserver
    const observer = new MutationObserver(removeDollarAmountsAndPriceDescriptors);
    observer.observe(document.body, { childList: true, subtree: true });
})();