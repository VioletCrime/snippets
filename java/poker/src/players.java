
public class players {
	player humie;
	player dealer;
	public players(int num_bots, int starting_funds){
		humie = new human(starting_funds);
		dealer = humie;
		player _bot = new bot(starting_funds);
		_bot.setNextPlayer(humie);
		_bot.setPreviousPlayer(humie);
		humie.setNextPlayer(_bot);
		humie.setPreviousPlayer(_bot);
		
		for(int i = 0; i < num_bots-1; i++){
			player _bot1 = new bot(starting_funds);
			_bot1.setPreviousPlayer(humie.getPreviousPlayer());
			_bot1.setNextPlayer(humie);
			humie.setPreviousPlayer(_bot1);
			
		}
	}
}
