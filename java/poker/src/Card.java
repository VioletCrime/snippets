
public class Card {
	
	private suits suit = null;
	private value val = null;
	private double shuffle_val = -1;
	
    public Card(suits st, value v, double d){
    	suit = st;
    	val = v;
    	shuffle_val = d;
    }
    
    public suits getSuit(){
    	return suit;
    }
    
    public value getValue(){
    	return val;
    }
    
    public double getShuffleVal(){
    	return shuffle_val;
    }
    
    public String toString(){
    	String result = val + " of " + suit + " (shuffle_val = " + shuffle_val + ").";
    	return result;
    }
}
