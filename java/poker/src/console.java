import java.util.Scanner;
public class console {
	public static void main(String[] args){
		Scanner inny = new Scanner(System.in);
		manager m = new manager();
		
		System.out.println("Welcome to Poker v0.01");
		System.out.print("Please enter the number of players (including yourself): ");
		int num_bots = inny.nextInt() - 1;
		System.out.print("How much money should each player start with?: ");
		int starting_dough = inny.nextInt();
		m.create_table(num_bots, starting_dough);
		
		m.play_hand();
	}
}
