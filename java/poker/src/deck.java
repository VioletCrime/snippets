import java.util.Random;

public class deck {
	
	private Card[] deck = new Card[52];
	private int deck_position = 0;
	
	public deck(){
		int i = 0;
		Random rand = new Random();
		for (suits s : suits.values()){
			for (value v : value.values()){
				Card c = new Card(s, v, rand.nextDouble());
				deck[i] = c;
				i++;
			}
		}
		sort_deck();
	}
	
	public Card deal_card(){
		if (deck_position == 52){
			return null;
		}
		else{
			deck_position++;
			return deck[deck_position-1];
		}
	}
	
	public void burn_card(){
		deck_position++;
	}
	
	private void sort_deck(){
		deck = merge_sort(deck);
	}
	
	private Card[] merge_sort(Card[] c){
		if (c.length <= 1){
			return c;
		}
		else{
			Card[] result = new Card[c.length];
			Card[] left = merge_sort(splitLeft(c));
			Card[] right = merge_sort(splitRight(c));
			int i = 0;
			int j = 0;
			while(i < left.length || j < right.length){
				if(i == left.length){
					result[i+j] = right[j];
					j++;
				}
				else if(j == right.length){
					result[i+j] = left[i];
					i++;
				}
				else if(left[i].getShuffleVal() < right[j].getShuffleVal())
				{
					result[i+j] = left[i];
					i++;
				}
				else{
					result[i+j] = right[j];
					j++;
				}
			}
			return result;
		}
	}
	
	private Card[] splitLeft(Card[] c){
		int start = 0;
		Card[] result = new Card[c.length/2];
		while (start < result.length){
			result[start] = c[start];
			start++;
		}
		return result;
	}
	
	private Card[] splitRight(Card[] c){
		int start = c.length/2;
		Card[] result;
		if(c.length%2 == 1){
			result = new Card[c.length/2+1];
		}
		else{
			result = new Card[c.length/2];
		}
		int i = 0;
		while(start < c.length){
			result[i] = c[start];
			start++;
			i++;
		}
		return result;
	}
	
	public void printDeck(){
		for (int i = 0; i < deck.length; i++){
			System.out.println(deck[i]);
		}
	}
}
