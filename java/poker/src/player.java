
public abstract class player {
	protected player left_player = null;
	protected player right_player = null;
	protected Card cardOne;
	protected Card cardTwo;
	boolean busted;
	boolean human;
	int balance;
	
	public player(int bal, boolean hum){
		balance = bal;
		human = hum;
	}
	
	public Card getCardOne(){
		return cardOne;
	}
	
	public Card getCardTwo(){
		return cardTwo;
	}
	
	public player getNextPlayer(){
		if(right_player.busted){
			return right_player;
		}
		else{
			return right_player.getNextPlayer();
		}
	}
	
	public player getPreviousPlayer(){
		if(left_player.busted){
			return left_player;
		}
		else{
			return left_player.getPreviousPlayer();
		}
	}
	
	public void setCardOne(Card c){
		cardOne = c;
	}
	
	public void setCardTwo(Card c){
		cardTwo = c;
	}
	
	public void setNextPlayer(player next){
		right_player = next;
	}
	
	public void setPreviousPlayer(player prev){
		left_player = prev;
	}
}
