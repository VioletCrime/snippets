export GOPATH=$(git rev-parse --show-toplevel)/go
export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOBIN
