package main

import "fmt"

func main() {
	var age int = 40

	var favNum float64 = 1.16180339

	fmt.Println(age, " ", favNum)

	fmt.Println(age, favNum)

	const pi float64 = 3.14159265368979323846

	var myName string = "Kyle Smith"

	fmt.Println("Length of my name is", len(myName), "characters long.")

	var ( // multiple var declarations at once- seems useless?
		boolTrue  bool = true
		boolFalse bool = false
	)

	fmt.Println("true && false =", boolTrue && boolFalse)
	fmt.Println("true || false =", boolTrue || boolFalse)
	fmt.Println("!true || false =", !boolTrue && boolFalse)
	fmt.Println("!(true && false) =", !(boolTrue && boolFalse))

	fmt.Printf("pi: %f\npi to 3 deicmal places: %.3f\nType: %T\nisBool?: %t\nSome int: %d\n", pi, pi, pi, boolTrue, age)

	fmt.Printf("Binary of 100: %b\n", 100)
	fmt.Printf("Character of 44: %c\n", 44)
	fmt.Printf("Hex of 18: %x\n", 18)
	fmt.Printf("Sci. Notation of pi: %e\n", pi)

}
