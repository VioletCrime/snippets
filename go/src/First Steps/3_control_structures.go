package main

import "fmt"

func main() {

	// For-loops
	i := 1

	for i <= 10 {
		fmt.Println(i)
		i++
	}

	for j := 0; j < 5; j++ {
		fmt.Println(j)
	}

	// If statements

	yourAge := 18

	if yourAge >= 16 {
		fmt.Println("You can drive.")
	} else if yourAge >= 18 {
		fmt.Println("You can vote.")
	} else {
		fmt.Println("You can't do shit.")
	}

	// Switch statements
	switch yourAge {
	case 16:
		fmt.Println("You can drive")
	case 18:
		fmt.Println("You can vote")
	default:
		fmt.Println("You can't do shit")
	}

}
