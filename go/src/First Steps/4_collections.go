package main

import "fmt"

func main() {
	var favNums [5]float64

	favNums[0] = 163
	favNums[1] = 178557
	favNums[2] = 691
	favNums[3] = 3.141
	favNums[4] = 1.618

	fmt.Println("favNums[2]:", favNums[2])

	firstFive := [5]int{1, 2, 3, 4, 5}
	fmt.Println("firstFive:", firstFive)

	for i, value := range firstFive {
		fmt.Printf("firstFive[%d]: %d, ", i, value)
	}
	fmt.Println("")
	for _, value := range firstFive { // Must use '_' if not using index in the for loop
		fmt.Printf("%d ", value)
	}

}
