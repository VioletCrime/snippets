package main

import (
	"fmt"
	"os"
)

func main() {
	environs := os.Environ()
	for _, environ := range environs {
		fmt.Println(environ)
	}
}
