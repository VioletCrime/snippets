package main

import "fmt"

func do_sum(a uint8, b uint16, c uint32, d uint64) uint64 {
	d += uint64(c)
	d += uint64(b)
	d += uint64(a)
	return d
}

func main() {
	var a uint8 = 1
	var b uint16 = 2
	var c uint32 = 3
	var d uint64 = 4

	fmt.Println(do_sum(a, b, c, d))
}
