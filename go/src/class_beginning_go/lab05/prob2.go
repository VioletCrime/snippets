package main

import (
	"bufio"
	"fmt"
	"os"
)

func print_bikes(bikes ...string) {
	for _, bike := range bikes {
		fmt.Println(bike)
	}
	//fmt.Println(bikes)
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	var bikes []string
	var moto string
	for moto != "q" {
		fmt.Print("Input Motorcycle (q to quit): ")
		scanner.Scan()
		moto = scanner.Text()
		if moto != "q" {
			bikes = append(bikes, moto)
		}
	}
	print_bikes(bikes[:]...)
}
