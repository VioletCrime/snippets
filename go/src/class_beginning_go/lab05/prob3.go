package main

import "fmt"

func ItalianFilter(s string) bool {
	italian_bikes := [...]string{"Ducati", "Aprilia", "Moto", "Guzzi"}
	for _, ib := range italian_bikes {
		if s == ib {
			return true
		}
	}
	return false
}

func BritishFilter(s string) bool {
	british_bikes := [...]string{"BSA", "Triumph", "Norton"}
	for _, bb := range british_bikes {
		if s == bb {
			return true
		}
	}
	return false
}

func print_bikes(filter func(string) bool, bikes ...string) {
	for _, bike := range bikes {
		fmt.Println(filter(bike))
	}
}

func main() {
	print_bikes(ItalianFilter, "Ducati", "Triumph", "Moto", "Norton")
	print_bikes(BritishFilter, "Ducati", "Triumph", "Moto", "Norton")
}
