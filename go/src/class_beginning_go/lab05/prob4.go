package main

import "fmt"

func ItalianFilter(s string) bool {
	italian_bikes := [...]string{"Ducati", "Aprilia", "Moto", "Guzzi"}
	for _, ib := range italian_bikes {
		if s == ib {
			return true
		}
	}
	return false
}

func BritishFilter(s string) bool {
	british_bikes := [...]string{"BSA", "Triumph", "Norton"}
	for _, bb := range british_bikes {
		if s == bb {
			return true
		}
	}
	return false
}

func print_bikes(filter func(string) bool, bikes ...string) {
	for _, bike := range bikes {
		fmt.Println(filter(bike))
	}
}

func main() {
    fmt.Println("Italian:")
	print_bikes(ItalianFilter, "Ducati", "Triumph", "Moto", "Norton")
    fmt.Println("\nBritish:")
	print_bikes(BritishFilter, "Ducati", "Triumph", "Moto", "Norton")
    fmt.Println("\nGerman:")
    print_bikes(func(s string) bool { german_bikes := [...]string{"BMW", "MZ", "Sommer"}; for _, bike := range german_bikes{ if bike == s{ return true; } } return false; }, "BMW", "BSA", "MZ", "Triumph")
}
