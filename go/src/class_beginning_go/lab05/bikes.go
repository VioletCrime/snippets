package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	var moto string
	for moto != "q" {
		fmt.Print("Input Motorcycle (q to quit): ")
		scanner.Scan()
		moto = scanner.Text()
		if moto != "q" {
			fmt.Println("Motorcycle entered: ", moto)
		}
	}
}
