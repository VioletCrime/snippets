package main

import (
	"encoding/json"
	"os"
)

type Car struct {
	Make  string
	Model string
	Odo   int32
	Year  int16
	Color string
}

func main() {
	mycar := Car{"Ford", "Fiesta ST", 6560, 2016, "White"}
	//fmt.Println(mycar)
	data, _ := json.Marshal(mycar)
	//fmt.Printf("%s\n", data)
	buf := []byte(data)
	f, _ := os.Create("myCar.txt")
	f.Write(buf[:])
	f.Close()
}
