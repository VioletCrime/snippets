package main

import "os"

func main() {
	buf := []byte(`[{"title":"Hello", "subject":"world"}]`)
	f, _ := os.Create("output.txt")
	f.Write(buf[:])
	f.Close()
}
