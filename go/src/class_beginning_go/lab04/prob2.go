package main

import "fmt"

type Car struct {
	Make  string
	Model string
	Odo   int32
	Year  int16
	Color string
}

func main() {
	mycar := Car{"Ford", "Fiesta ST", 6560, 2016, "White"}
	fmt.Println(mycar)
	var myOtherCar Car
	myOtherCar.Make = "Eagle"
	myOtherCar.Model = "Talon"
	myOtherCar.Odo = 118000
	myOtherCar.Year = 1995
	myOtherCar.Color = "Green"
	fmt.Println(myOtherCar)
}
