package main

import "fmt"

type Car struct {
	year  int16
	Make  string
	Model string
}

func main() {
	oldCarOne := Car{1969, "Chevy", "Corvette"}
	oldCarTwo := Car{1969, "Dodge", "Challenger"}
	modernCarOne := Car{2011, "Ferarri", "458"}
	modernCarTwo := Car{2011, "Nissan", "GTR"}
	newCarOne := Car{2015, "McLaren", "P1"}
	newCarTwo := Car{2015, "Porsche", "Cayman GT4"}

	//yearMap := map[int16][]Car{
	//    oldCarOne.year: [oldCarOne, oldCarTwo],
	//    modernCarOne.year: [modernCarOne, modernCarTwo],
	//    newCarOne.year: [newCarOne, newCarTwo],
	//}
	yearMap := make(map[int16][]Car)
	yearMap[oldCarOne.year] = append(yearMap[oldCarOne.year], oldCarOne)
	yearMap[oldCarTwo.year] = append(yearMap[oldCarTwo.year], oldCarTwo)
	yearMap[modernCarOne.year] = append(yearMap[modernCarOne.year], modernCarOne)
	yearMap[modernCarTwo.year] = append(yearMap[modernCarTwo.year], modernCarTwo)
	yearMap[newCarOne.year] = append(yearMap[newCarOne.year], newCarOne)
	yearMap[newCarTwo.year] = append(yearMap[newCarTwo.year], newCarTwo)

	fmt.Println(yearMap[1969])
	fmt.Println(yearMap[2011])
	fmt.Println(yearMap[2015])

	fmt.Println("=======================================")

	makeMap := make(map[string][]Car)
	makeMap[oldCarOne.Make] = append(makeMap[oldCarOne.Make], oldCarOne)
	makeMap[oldCarTwo.Make] = append(makeMap[oldCarTwo.Make], oldCarTwo)
	makeMap[modernCarOne.Make] = append(makeMap[modernCarOne.Make], modernCarOne)
	makeMap[modernCarTwo.Make] = append(makeMap[modernCarTwo.Make], modernCarTwo)
	makeMap[newCarOne.Make] = append(makeMap[newCarOne.Make], newCarOne)
	makeMap[newCarTwo.Make] = append(makeMap[newCarTwo.Make], newCarTwo)

	fmt.Println(makeMap[oldCarOne.Make])
	fmt.Println(makeMap[oldCarTwo.Make])
	fmt.Println(makeMap[modernCarOne.Make])
	fmt.Println(makeMap[modernCarTwo.Make])
	fmt.Println(makeMap[newCarOne.Make])
	fmt.Println(makeMap[newCarTwo.Make])

}
