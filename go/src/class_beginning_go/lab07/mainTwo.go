package main

import (
	"fmt"
	"time"
)

func main() {
	f := func() int {
		fmt.Println("before")
		time.Sleep(10 * time.Second)
		fmt.Println("after")
		return 0
	}
	f()
	c := make(chan int)
	go func() {
		c <- f()
	}()
	fmt.Println("do some other work before we block on <-")
	returnVal := <-c
	fmt.Println("return value of", returnVal)
}
