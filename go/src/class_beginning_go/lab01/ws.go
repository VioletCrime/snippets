package main

import "net/http"

func main() {
	http.Handle("/", http.HandlerFunc(hi))
	http.ListenAndServe("localhost:9999", nil)
}

func hi(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("Hello World!\n"))
}
