package main

import "fmt"

type Moto struct {
	Make  string
	Model string
	MPG   int32
	Price int32
}

func (m Moto) dump() {
	fmt.Println(m)
}

func main() {
	m := Moto{"Honda", "CBR900RR", 30, 42000}
	m.dump()
}
