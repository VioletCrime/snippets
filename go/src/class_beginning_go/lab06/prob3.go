package main

import "fmt"

type Moto struct {
	Make  string
	Model string
	MPG   float32
	Price float64
}

func (m Moto) dump() {
	fmt.Println(m)
}

func (m *Moto) discount() {
	m.Price *= .9
}

func main() {
	m := Moto{"Honda", "CBR900RR", 30.0, 42000.0}
	m.dump()
	m.discount()
	m.dump()
	m.discount()
	m.dump()
}
