package main

import "fmt"

type Moto struct {
	Make  string
	Model string
	MPG   float32
	Price float64
}

func (m Moto) dump() {
	fmt.Println(m)
}

func (m *Moto) discount() {
	m.Price *= .9
}

func x(a []func()) {
	for _, f := range a {
		f()
	}
}

func main() {
	m := Moto{"Honda", "CBR900RR", 30.0, 42000.0}
	mm := Moto{"BWM", "Hoopty", 17.4, 999999999.0}
	var a [2]func()
	a[0] = func() { m.dump() }
	a[1] = func() { mm.dump() }
	x(a[:])
}
