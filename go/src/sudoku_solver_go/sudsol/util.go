package sudsol

import (
	"os"
	"strconv"
)

func valIsPossible(s []string, v string) bool {
	// Takes a squares possible values (pvals) and a val we want to check.
	// Returns true if the value exists in the slice, false otherwise.
	for _, elem := range s {
		if elem == v {
			return true
		}
	}
	return false
}

func check_err(err error) {
	if err != nil {
		panic(err)
	}
}

// http://stackoverflow.com/questions/10510691/how-to-check-whether-a-file-or-directory-denoted-by-a-path-exists-in-golang
func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

func intToStr(v int) string {
	retval := strconv.Itoa(v)
	//check_err(err)
	return retval
}

func strToInt(s string) int {
	retval, err := strconv.Atoi(s)
	check_err(err)
	return retval
}

func getSubGrid(i int, j int) (startI int, endI int, startJ int, endJ int) {
	startI = (i / 3) * 3
	endI = startI + 2
	startJ = (j / 3) * 3
	endJ = startJ + 2
	return
}

func indexOfValue(s []string, val string) int {
	retval := -1 // false case
	for i, elem := range s {
		if elem == val {
			retval = i
			break
		}
	}
	return retval
}

func strSliceContains(s []string, val string) bool {
	return indexOfValue(s, val) > -1
}
