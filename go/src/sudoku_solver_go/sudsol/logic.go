package sudsol

func (b *board) isolatedPossibilities() bool {
  b.logger.log(4, 2, "Entering board.isolatedPossibilities().")
  retval := false
  for i := 0; i < 9; i++ {
    for j := 0; j < 9; j++ {
      for k := 1; k < 10; k++ {
        if strSliceContains(b.brd[i][j].pvals, intToStr(k)){
          if b.isIsolatedPossibility(i, j, k) {
            retval = true
            newMove := []int{i, j, k}
            b.knownMoves = append(b.knownMoves, newMove)
          }
        }
      }
    }
  }
  b.logger.log(3, -2, "Exiting board.isolatedPossibilities() returning %v.", retval)
  return retval
}


func (b *board) isIsolatedPossibility(i int, j int, k int) bool {
  b.logger.log(4, 2, "Entering isIsolatedPossibility(%v, %v, %v).", i, j, k)
  retval := b.isIsolatedOnRow(i, j, k) ||  b.isIsolatedOnCol(i, j, k) || b.isIsolatedInSubgrid(i, j, k)
  b.logger.log(3, -2, "Exiting isIsolatedPossibility(%v, %v, %v) returning %v.", i, j, k, retval)
  return retval
}

func (b *board) isIsolatedOnRow(i int, j int, k int) bool {
  b.logger.log(4, 2, "Entering board.isIsolatedOnRow(%v, %v, %v)", i, j, k)
  retval := true
  for col := 0; col < 9; col++ {
    if col == j {
      continue
    }
    if strSliceContains(b.brd[i][col].pvals, intToStr(k)) {
      b.logger.log(4, 0, "Found another square (%v, %v) with %v as a possibility.", i, col, k)
      retval = false
      break
    }
  }
  b.logger.log(3, -2, "Exiting board.isIsolatedOnRow(%v, %v, %v) returning %v.", i, j, k, retval)
  return retval
}

func (b *board) isIsolatedOnCol(i int, j int, k int) bool {
  b.logger.log(4, 2, "Entering board.isIsolatedOnRow(%v, %v, %v).", i, j, k)
  retval := true
  for row := 0; row < 9; row++ {
    if row == i {
      continue
    }
    if strSliceContains(b.brd[row][j].pvals, intToStr(k)) {
      b.logger.log(4, 0, "Found another square (%v, %v) with %v as a possibility.", row, j, k)
      retval = false
      break
    }
  }
  b.logger.log(3, -2, "Exiting board.isIsolatedOnCol(%v, %v, %v) returning %v.", i, j, k, retval)
  return retval
}

func (b *board) isIsolatedInSubgrid(i int, j int, k int) bool {
  b.logger.log(4, 2, "Entering board.isIsolatedInSubgrid(%v, %v, %v).", i, j, k)
  retval := true
  startI, endI, startJ, endJ := getSubGrid(i, j)
  for row := startI; row <= endI; row++{
    for col := startJ; col <= endJ; col++ {
      if row == i && col == j{
        continue
      }
      if strSliceContains(b.brd[row][col].pvals, intToStr(k)) {
        b.logger.log(4, 0, "Found another square (%v, %v) with %v as a possibility.", row, col, k)
        retval = false
        break
      }
    }
  }
  b.logger.log(3, -2, "Exiting board.isIsolatedInSubgrid(%v, %v, %v) returning %v.", i, j, k, retval)
  return retval
}

func (b *board) nakedTwins() bool {
  return false  // TODO
}
