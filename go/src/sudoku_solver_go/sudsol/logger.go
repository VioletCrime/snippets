package sudsol

import (
	"fmt"
	"os"
	"strings"
	"time"
)

type logger struct {
	logFile  string
	logLevel int8
	toScreen bool
	offset   int
    skipLog  bool
}

func (l *logger) log(level int, offset int, s string, args ...interface{}) {
	if offset > 0 {
		l.offset += offset
	}
	pad := strings.Repeat(" ", l.offset)
	var msg string = pad + fmt.Sprintf(s, args...)
	t := time.Now()
	ts := fmt.Sprintf("%02d:%02d:%02d.%d", t.Hour(), t.Minute(), t.Second(), t.Nanosecond())
	loglevel := fmt.Sprintf("[%s] - ", loglvl2str[level])
	output := fmt.Sprintf("%s %10s%s", ts, loglevel, msg)
	if int8(level) <= l.logLevel {
        if !l.skipLog {
            f, err := os.OpenFile(l.logFile, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0660)
            defer f.Close()
            check_err(err)
            f.WriteString(output)
            f.WriteString("\n")
        }
		if l.toScreen {
			fmt.Println(output)
		}
	}
	if offset < 0 {
		l.offset += offset
		if l.offset < 0 {
			l.offset = 0
		}
	}
}

var defaultLogDir string = "./.logs/"
var loglvl2str = map[int]string{
	0: "ERROR",
	1: "WARN",
	2: "INFO",
	3: "DEBUG",
	4: "XTREM",
}

func createLogger(logFile string, logLevel int8, toScreen bool, skipLog bool) logger {
    if logFile == "" {
		exist, err := exists(defaultLogDir)
		if err != nil {
			panic(err)
		}
		if !exist {
			err := os.Mkdir(defaultLogDir, 0775)
			check_err(err)
		}
		//logFile = defaultLogDir + time.Now().Format(time.RFC3339)
		t := time.Now()
		logFile = fmt.Sprintf("%s%d%02d%02d_%02d:%02d:%02d", defaultLogDir, t.Year(), t.Month(), t.Day(),
			t.Hour(), t.Minute(), t.Second())
	}
	logr := logger{logFile, logLevel, toScreen, 0, skipLog}
	logr.log(2, 0, "Logger initialized! logFile: %v. logLevel: %v. toScreen: %v, skipLog: %v", logr.logFile, logr.logLevel, logr.toScreen, logr.skipLog)
	return logr
}
