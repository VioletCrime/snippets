package sudsol

import (
    //"fmt"
    "testing"
    )


func TestValidateRowsAndColsTrue(t *testing.T){
    logger := getLogger(false)
    board := buildBoardFromString(complete_board, logger)
    rowColResult := board.validateRowsAndCols()
    if !rowColResult {
        t.Error("TestValidateRowsAndColsTrue - Expected true, got", rowColResult)
    }
}

func TestValidateRowsAndColsFalse(t *testing.T){
    logger := getLogger(false)
    board := buildBoardFromString(incomplete_board, logger)
    rowColResult := board.validateRowsAndCols()
    if rowColResult {
        t.Error("TestValidateRowsAndColsFalse - Expected false, got", rowColResult)
    }
}

func TestValidateSubgridTrue(t *testing.T){
    logger := getLogger(false)
    board := buildBoardFromString(complete_board, logger)
    subgridResult := board.validateSubgrid()
    if !subgridResult {
        t.Error("TestValidateSubgridTrue - Expected true, got", subgridResult)
    }
}

func TestValidateSubgridFalse(t *testing.T){
    logger := getLogger(false)
    board := buildBoardFromString(incomplete_board, logger)
    subgridResult := board.validateSubgrid()
    if subgridResult {
        t.Error("TestValidateSubgridFalse - Expected false, got", subgridResult)
    }
}

func TestUpdatePossibleValues(t *testing.T){
    logger := getLogger(false)
    board := buildBoardFromString(zeros_board, logger)
    board.processBoardState()
    board.brd[0][0].value = "1"
    board.brd[0][0].pvals = []string {}
    board.updatePossibleVals(0, 0, "1")
    //fmt.Println(board.formatBoardPretty(0))
    //fmt.Println(board.formatPossibilities(0))

    for i := 0; i < 9; i++ {
        for j := 0; j < 9; j++ {
            if i == 0 || j == 0 || ( ( i >= 0 && i <= 2) && ( j >= 0 && j <= 2) ){
                for _, pval := range(board.brd[i][j].pvals){
                    if pval == "1" {
                        t.Error("Found '1' as a possible value where we shouldn't have ([", i, "][", j, "])")
                    }
                }
            } else {
                found := false
                for _, pval := range(board.brd[i][j].pvals) {
                    if pval == "1" {
                        found = true
                        break
                    }
                }
                if !found {
                    t.Error("Did not find '1' as a possilbe value where we should have ([", i, "][", j, "])")
                }
            }
        }
    }
}

func TestCopyBoard(t *testing.T){
    logger := getLogger(false)
    board := buildBoardFromString(zeros_board, logger)
    if board.brd[0][0].value != "0" {
        t.Error("Test setup incorrect!")
    }
    if len(board.brd[0][0].pvals) != 0 {
        t.Error("Test setup (pvals) incorrect!")
    }
    board_copy := board.copyBoard()
    board_copy.brd[0][0].value = "1"
    board_copy.brd[0][0].pvals = []string {"1"}
    if board.brd[0][0].value != "0" {
        t.Error("Original board was updated!")
    }
    if len(board.brd[0][0].pvals) != 0 {
        t.Error("Original board was updated (pvals)!")
    }
    if board_copy.brd[0][0].value != "1" {
        t.Error("Copy update did not work!")
    }
    if len(board_copy.brd[0][0].pvals) != 1 {
        t.Error("Copy update (pvals) did not work!")
    }
}
