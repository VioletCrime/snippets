package main

import (
    "os/exec"
    "bufio"
    "fmt"
    "log"
)

// Waits 10 seconds and lets input build up. After sleep exits, dump all input from stdin to stdout
// Written by Daniel "Goodest" Suhr, c. 2021-3-12
func main() {
    cmd := exec.Command("sh", "-c", "sleep 10")
    stdout, _ := cmd.StdoutPipe()
    cmd.Start()
    scanner := bufio.NewScanner(stdout)
    for scanner.Scan() {
        m := scanner.Text()
        fmt.Println(m)
        log.Printf(m)
    }
    cmd.Wait()
}
