package main

import (
	"flag"
	"fmt"
)

func test_passing(inputStr string, file string) {
	if inputStr != "" && file != "" {
		fmt.Println("both values are specified at the command line!\n")
	}
	if inputStr != "" {
		fmt.Printf("Input string: %s\n", inputStr)
	} else {
		fmt.Println("Input string is nil!")
	}
	if file != "" {
		fmt.Printf("file: %s\n", file)
	} else {
		fmt.Println("file is nil!")
	}
}

func main() {
	inputStr := flag.String("board", "", "lol")
	file := flag.String("file", "", "refl")
	flag.Parse()
	test_passing(*inputStr, *file)
}
