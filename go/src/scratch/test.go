package main

import "fmt"

func main() {
	var lol [9][9]int
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			lol[i][j] = i + j
			fmt.Printf("%d ", lol[i][j])
		}
		fmt.Printf("\n")
	}
	lol[4][4] = 42
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			fmt.Printf("%d ", lol[i][j])
		}
		fmt.Printf("\n")
	}

	fmt.Printf("TYPE: %T\n", lol)

	test_two := [4]int8{1, 2, 3, 4}
	fmt.Printf("%d, %d, %d, %d\n", test_two[0], test_two[1], test_two[2], test_two[3])
	test_two[3] = 5
	fmt.Printf("%d, %d, %d, %d\n", test_two[0], test_two[1], test_two[2], test_two[3])

}
